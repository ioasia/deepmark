# Deepmark

# How to use

 - Make a forked from this repository. https://gitlab.com/serdao/deepmark.git
 - git clone https://gitlab.com/{username}/deepmark.git
 - git remote add upstream https://gitlab.com/serdao/deepmark.git
 - `composer install`
 - `sass --update style.scss ../css/style.css`
 - Update parameter with right information
 - Execute
 ```
php bin/console doctrine:migrations:diff --no-interaction
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
php bin/console cache:clear --env=dev
php bin/console cache:clear --env=prod
```
 - For PHP server `php bin/console server:run`

## vHost

 - Point to /web
 - sudo a2enmod rewrite
 - sudo a2enmod headers
 - sudo a2enmod expires
 - sudo /etc/init.d/apache2 restart
 - domain.local

## Previous document

## Setup DB

* Install mysql

* Install & configure postgres (not used anymore)

> sudo apt-get install postgresql postgresql-client

> sudo apt-get install php-pgsql

> sudo su - postgres 

> createuser deepmark --pwprompt

> createdb -O deepmark deepmark

## Useful command

* Start dev server

> php bin/console server:run

* Run update.sh to update db tables and clear cache

> ./update.sh

* Update DB from schema

> php bin/console doctrine:schema:update --dump-sql

> php bin/console doctrine:schema:update --force

* Data fixtures

By default the load command purges the database, removing all data from every table. To append your fixtures' data add the --append option.

> php bin/console doctrine:fixtures:load --append

* User management 

> php bin/console fos:user:create testuser test@example.com p@ssword

> php bin/console fos:user:create testuser

> php bin/console fos:user:create adminuser --super-admin

> php bin/console fos:user:create testuser --inactive

> php app/console fos:user:activate testuser

> php app/console fos:user:deactivate testuser

> php bin/console fos:user:promote testuser ROLE_ADMIN

> php bin/console fos:user:promote testuser --super

> php bin/console fos:user:demote testuser ROLE_ADMIN

> php bin/console fos:user:demote testuser --super

> php bin/console fos:user:change-password testuser newp@ssword

## Frontend 

> sass --update style.scss ../css/style.css

## Cron job 
https://crontab.guru/examples.html
Minute	Hour	Day	    Month	Weekday
0	    0	    */2	    *	    *
Setup this command line below into the crontab
> 0 1 * * * php bin/console alerts:alerts
> 0 0 */2 * * php bin/console alerts:next-session 48 0 your_domain
> */10 * * * * php bin/console alerts:next-session 0 10 your_domain
> 0 2 */15 * * php bin/console alerts:learner-not-start-courses your_domain
> 0 0 * * 0 php bin/console alerts:learner-not-complete-courses 30 your_domain
> 0 1 * * * php bin/console alerts:update-planning 30 your_domain
> 0 1 * * * php bin/console alerts:alerts your_domain

## AWS S3
 // get S3 storage in the controller
 $storage = $this->container->get('storage');
 // Upload a file with the content "content text" and the MIME-Type text/plain
 $result = $storage->upload('test2.txt', 'content text', ['contentType' => 'text/plain']);
 // Upload a local existing File and let the service automatically detect the mime type.
 $base_path = $this->container->getParameter('kernel.root_dir') . '/../';
 $path = $base_path . 'web/files/InterventionFiles/S3/';
 $result = $storage->uploadFile($path . 'excel_testing.xlsx');
 // get URL of a file
 $plainUrl = $storage->getObjectUrl('excel_testing.xlsx');
 
 # NodeJS Env
 
 ## Install NVM
 
 ```
 wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
 ```
 
 ```
 source ~/.bashrc
 ```
 
 ## Install NodeJS LTS
 
 ```
 nvm install --lts
 ```
 
 ## Install Yarn
 
 ```
 curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
 
 echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
 
 sudo apt update && sudo apt install yarn
 ```
 
 ## Install PM2 Global
 
 ```
 npm install pm2 -g
 ```
 
 ## Add certificate to project
 
 ```
 cd web/js/lib/streaming-server
 sudo cp /etc/letsencrypt/live/matchlearning.net/fullchain.pem ./public.pem
 sudo cp /etc/letsencrypt/live/matchlearning.net/privkey.pem ./private.pem
 sudo chmod -R 644 ./private.pem
 sudo chmod -R 644 ./public.pem
 ```
 
 ## Run Streaming Server
 
 ```
 # Navigation to the streaming server
 cd web/js/lib/streaming-server
 # Install vendor
 yarn
 # Start the streaming server
 pm2 start index.js
 # Check the logs of the streaming server
 pm2 logs index
 ```