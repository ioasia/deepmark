$(document).ready(function() {
    moment.locale($('#locate').val())
    // JS Translations
    jsTranslations = {
        module_expired: $('#jsTranslations').data('module_expired'),
        session_view: $('#jsTranslations').data('session_view'),
        session_rejoin: $('#jsTranslations').data('session_rejoin'),
        attendance_title: $('#jsTranslations').data('attendance_title'),
        attendance_text: $('#jsTranslations').data('attendance_text'),
        attendance_error: $('#jsTranslations').data('attendance_error'),
        attendance_signature_updated: $('#jsTranslations').data('attendance_signature_updated'),
        trainer_alerts_reports_not_upload: $('#jsTranslations').data('trainer_alerts_reports_not_upload'),
        global_warning: $('#jsTranslations').data('global_warning'),
        successfully_title: $('#jsTranslations').data('successfully_title'),
        successfully_learner_removed_text: $('#jsTranslations').data('successfully_learner_removed_text'),
        cannot_remove_learner_text: $('#jsTranslations').data('cannot_remove_learner_text'),
        successfully_trainer_removed_text: $('#jsTranslations').data('successfully_trainer_removed_text'),
        select_one_learner: $('#jsTranslations').data('select_one_learner'),
        send_email_to_learner_success: $('#jsTranslations').data('send_email_to_learner_success'),
        select_one_trainer: $('#jsTranslations').data('select_one_trainer'),
        select_one_report: $('#jsTranslations').data('select_one_report'),
        send_email_to_trainer_success: $('#jsTranslations').data('send_email_to_trainer_success'),
        warning_title: $('#jsTranslations').data('warning_title'),
        success_title: $('#jsTranslations').data('success_title'),
        assign_learner_to_course_success: $('#jsTranslations').data('assign_learner_to_course_success'),
        assign_learner_to_group_success: $('#jsTranslations').data('assign_learner_to_group_success'),
        cancellation_session_success: $('#jsTranslations').data('cancellation_session_success'),
        alert_checked: $('#jsTranslations').data('alert_checked'),
        alert_has_been_sent: $('#jsTranslations').data('alert_has_been_sent'),
        signature: $('#jsTranslations').data('signature'),
        open_signature_title: $('#jsTranslations').data('open_signature_title'),
        open_signature_text: $('#jsTranslations').data('open_signature_text'),
        close_signature_title: $('#jsTranslations').data('close_signature_title'),
        close_signature_text: $('#jsTranslations').data('close_signature_text'),
        save_signature_title: $('#jsTranslations').data('save_signature_title'),
        save_signature_text: $('#jsTranslations').data('save_signature_text'),
        please_sign_text: $('#jsTranslations').data('please_sign_text'),
        warning_quit_session_title: $('#jsTranslations').data('warning_quit_session_title'),
        warning_quit_session_digital_confirm: $('#jsTranslations').data('warning_quit_session_digital_confirm'),
        warning_quit_session_importation_confirm: $('#jsTranslations').data('warning_quit_session_importation_confirm'),
        buttons_confirm: $('#jsTranslations').data('buttons_confirm'),
        buttons_cancel: $('#jsTranslations').data('buttons_cancel'),
        global_from: $('#jsTranslations').data('global_from'),
        global_to: $('#jsTranslations').data('global_to'),
        global_submit: $('#jsTranslations').data('global_submit'),
        record: $('#jsTranslations').data('record'),
        restart_recording: $('#jsTranslations').data('restart_recording'),
    };
    randomUserColor();

    let activeLang = $('#dropdownLanguage .active').html();
    $('#switchLanguage').html(activeLang);
    $("#switchLanguage").append('<i class="fas fa-chevron-down cursorPointer marg-t-10 color-header font-size-15"></i>')
    $('[data-action="assignLearner"]').on('click', function() {
        localStorage.setItem('assignLearnerModuleId', $('#interventionId').val());
    });

    // Expend/collapse the "Description"
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).prev().find('.fa-minus').show();
        $(this).prev().find('.fa-plus').hide();
    })
    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).prev().find('.fa-minus').hide();
        $(this).prev().find('.fa-plus').show();
    })

    // destroy all instances of the CKEDITOR
    $.each( CKEDITOR.instances, function( key, value ) {
        let hasTextToFill = key.includes("text_to_fill");
        if (!hasTextToFill) {
            value.destroy();
        }
    });
});

function randomUserColor() {
    let randomColor = 'hsl(' + Math.floor(Math.random() * 360) + ',100%,35%)';
    $('.sidebar .user .photo').css('background-color', randomColor);
}

// function initMinimize() {
//     let btn = $('[data-action="minimize-sidebar"]');
//     let main = $('#main');
//     let sidebar = $('#sidebar');
//     if (btn.length) {
//         btn.on('click', function () {
//             main.toggleClass('full');
//             sidebar.toggleClass('hidden');
//         })
//     }
// }

function cloneTemplate(templateNode, id) {
    const clone = templateNode.clone();
    clone.css({ visibility: 'visible', position: 'relative' });
    clone.attr('id', id);
    return clone;
}

function showCourse(id) {
    document.location = '/learner/courses/' + id + '/show';
}

function makeRequest(url, payload, successText = '' , elementToDisable) {
    $.ajax({
        method: 'POST',
        url:url,
        data: payload,
        success: function () {
            swal(jsTranslations.success_title, successText, 'success', { button: 'ok' });
            $("#leanerGrade").modal("hide");
            if (typeof elementToDisable != 'undefined'
                && Array.isArray(elementToDisable)
                && elementToDisable.length >= 1) {
                elementToDisable.forEach(function (element) {
                    $(element).prop('disabled', true);
                });
            }
            location.reload();
        },
        failure: function () {
            swal('Error', '', 'error', { button: 'ok' });
        }
    });
}

