
function bindNavigationCircles() {
    let scrollPosition = 0;
    let numModule = $('.div-module').length;
    let widthLine = $("#circlesOuterWrapper").width() - 40;
    let widthModule = widthLine/5;
    let widthLeftRight = ($(".last-step").width() - 40)/2;
    $(".display-text").css('cssText', 'width: ' + widthModule + 'px!important');
    $("#courseStepsMiddleLine").css('cssText','margin-left: ' + widthLeftRight + 'px; margin-right: ' + widthLeftRight + 'px');
    function scrollCircles(direction) {

        return function () {
            if (scrollPosition + direction < 0){
                return;
            } else if (scrollPosition + direction == 0) {
                $("#previousModule").addClass('cursorPointer');
            } else if (scrollPosition + 5 + direction >= numModule) {
                $("#nextModule").addClass('cursorPointer');
            }

            if (scrollPosition + 5 + direction < numModule) {
                $("#nextModule").removeClass('cursorPointer');
            }

            if (scrollPosition + direction > 0){
                $("#previousModule").removeClass('cursorPointer');
            }

            const slideSize = widthModule;
            const circles = $('#circlesInnerWrapper');
            const spaceLeft = circles.children().length * widthModule - circles.width() - 40 - 2 * widthModule;
            if ((scrollPosition + direction - 1) * slideSize > spaceLeft) return;

            scrollPosition += direction;
            const scroll = scrollPosition * slideSize * -1 + 'px';
            circles.css({ transform: 'translateX('+scroll+')'});
        }
    }

    $('#nextModule').on('click', scrollCircles(1));
    $('#previousModule').on('click', scrollCircles(-1));

    let currentModuleId = $("#currentModuleId").val();
    if (currentModuleId != 0 && currentModuleId >= 6){
        let nextNumber = currentModuleId - 3;
        for(let i = 0; i < nextNumber; i++ ){
            $('#nextModule').click();
        }
    } else {
        if(numModule >= 6) {
            $("#previousModule").addClass('cursorPointer');
        }
    }
}

$(document).ready(function() {
    bindNavigationCircles();
    $(window).resize(function() {
        bindNavigationCircles();
    });
    let btn = $('[data-action="minimize-sidebar"]');
    if (btn.length) {
        btn.on('click', function () {
            setTimeout(function() {
                bindNavigationCircles();
            }, 300);
        })
    }
});
