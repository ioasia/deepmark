/**
 * Initializes file upload on element
 * @param selector - jquery selector for element
 * @param url - url for the upload
 * @param options - can have following optional properties:
 *    success(data, element) - callback when upload succeeds
 *    failure(data, element) - callback for failure
 *    formData(data, element) - function that returns form data to send with the file
 */
function initFileUploader(selector, url, options = {}) {

    if (!selector) throw new Error('No selector specified for file uploader');
    if (!url) throw new Error('No url specified for file uploader');

    const textTemplateNode = $('#upload-file-modal-text');

    selector.fileupload({
        url: url,
        dropzone: '.upload-area',
        dataType: 'json',
        autoUpload: false

    }).on('fileuploadadd', function (e, data) {

        const swalOptions = {
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        };

        swal(
            textTemplateNode.data('progress-header'),
            cloneTemplate($('#progressbar-upload-template'), 'progressbar-upload').prop('outerHTML'),
            "info",
            swalOptions
        );

        return data.submit();

    }).on('fileuploadprogress', function (e, data) {

        $('#progressbar-upload').width(Math.round((data.loaded * 100) / data.total) + '%');

    }).on('fileuploaddone', function (e, data) {
        swal(textTemplateNode.data('success-header'), textTemplateNode.data('success-comment'), "success", {
            button: "Ok",
        });

        options.success && options.success(data, this);

        if( options.hasOwnProperty("isTrainer")  && options.isTrainer == 1 ){
            var path = data.result.file_name;
            var type = data.result.uploaded;
            if(type == 'avatar'){
                $('#trainer-avatar').empty();
                $('#trainer-avatar').append('<img  style="border-radius: 50%;" width="150" height="150" src="'+path+'" >');
            } else if (type == 'video'){
                $("#trainer-video").attr("href", path);
                location.reload();
            } else if (type == 'cv'){
                $("#trainer-cv").attr("href", path);
                location.reload();
            }

        }
        if( options.hasOwnProperty("isFileTrainerReportTemplate")  && options.isFileTrainerReportTemplate == 1 ) {
            $('.fileTrainerReportTemplate').prop('required',false);
        }

        }).on('fileuploadfail', function (e, data) {
        let errorMessage = ''
        if(data.jqXHR.responseText){
            errorMessage = data.jqXHR.responseText.replace(/"/g, '');
        }else{
            errorMessage = data.messages.uploadedBytes?textTemplateNode.data('failure-uploaded-bytes'):textTemplateNode.data('failure-comment');
        }
        swal(
            textTemplateNode.data('failure-header'),
            errorMessage,
            "error",
            { button: 'Ok' }
        );

        options.failure && options.failure(data, this);
    });
}

function hasId(element){
    return typeof element.id != 'undefined';
}

function getFileExtension(filename) {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
}