const GREEN = '#28c612';
const CALENDAR = $('#learnerDashboardCalendar');
const HEADER_OPTIONS = {
    left: 'agendaWeek',
    center: 'prev,title,next',
    right: 'month'
};
function get_calendar_height() {
    return $(window).height() - 300;
}
$(document).ready(function() {
    $(window).resize(function() {
        $('#learnerDashboardCalendar').fullCalendar('option', 'height', get_calendar_height());
    });

    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''

    let isAdding = false;
    let isDeleting = false;
    let locate = $('#locate').val();

    $('#learnerDashboardCalendar').fullCalendar({
        locale: locate,
        header: HEADER_OPTIONS,
        selectable: true,
        firstDay: 1,
        eventLimit: 2,
        height: get_calendar_height(),
        defaultView: 'agendaWeek',
        allDaySlot: false,
        eventOverlap: false,
        slotEventOverlap : false,
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '24:00:00',
        fixedWeekCount: false,
        eventColor: GREEN,
        views: {
            week: {
                columnFormat: 'ddd D/M' // set format for week here
            },
        },
        eventClick: function (calEvent, jsEvent, view) {
            if (calEvent.title) {
                $('#dashboard-calendar-modal').modal();
                $('#modal-title').text(calEvent.displayType);
                $('#modal-intervention').text(calEvent.intervention);
                $('#modal-entity').text(calEvent.entity);
                $('#modal-type').text(calEvent.displayType);
                $('#modal-start-date').text(calEvent.startEndDate);
                $('#modal-duration').text(calEvent.duration);
                $('#modal-booked-status').text(calEvent.bookingStatus);
                $('#modal-trainer-display-name').text(calEvent.trainerName);
                $('#modal-session-date-new').text(moment(calEvent.start).format('dddd D MMMM'));
                $('#timeDuration').text(calEvent.startEndDate);
                $('#modal-mobile-phone').text(calEvent.trainerMobile);
                $('#modal-other-email').text(calEvent.trainerEmail);
                $('#modal-address').text(calEvent.moduleAddress);
                $("#modal-title-module").text(calEvent.moduleName);
                if(calEvent.type == 9 ){
                    $(".divForClassroom").show();
                }
                else{
                    $(".divForClassroom").hide();
                }
                if (!calEvent.available) {
                    $('#modal-book-online-session').text(jsTranslations.module_expired);
                    $('#modal-book-online-session').removeClass('btn-deepmark');
                    $(".booked").hide();
                    $(".expired").show();
                    $(".divStatus").css('border','1px solid grey')
                } else {
                    $('#modal-book-online-session').text(jsTranslations.session_rejoin);
                    $('#modal-book-online-session').addClass('btn-deepmark');
                    $(".booked").show();
                    $(".expired").hide();
                    $(".divStatus").css('border','1px solid #28c612')
                }

                $('#modal-book-online-session').off('click').click(function () {
                    window.location.href = '/learner/courses/' + calEvent.id + '/module';
                });
            }
        },
        viewRender: function (view, element) {
            $(element).find('.fc-scroller').perfectScrollbar();
            $('.fc-center').after($('.calendar-legend'));
            updateCalendar(view.start.format(), view.end.format());
        },
        eventRender: function (event, element) {
            if(!event.available){
                element.find('.fc-content').parent().addClass('event-expired');
            }
            if(event.type == 4 || event.type == 5){
                return false;
            }
            var eventDate = moment(event.start).format("YYYY-MM-DD");
            let today = new Date();
            today = moment(today).format("YYYY-MM-DD");
            // console.log(event);
            if (eventDate == today) {
                element.find('.fc-content').parent().removeClass('fc-event');
                element.find('.fc-content').parent().addClass('fc-event-today');
            }
            element.find('.fc-title').html(" <strong>" + event.displayType + "</strong> <p class='no-margin'>" + event.startEndDate + "</p>");
            // check AM/PM
            let time = event.start.format("HH");
            if (time >= 12 ) {
                element.find('.fc-content').parent().addClass('border-pm');
            } else {
                element.find('.fc-content').parent().addClass('border-am');
            }
        }
    });

    function updateCalendar(start, end) {
        let type = $("#typeModule").val();
        return new Promise((resolve, reject) => {

            const selectorCalendar = '#learnerDashboardCalendar';

            let url = lang + '/learner/calendars';

            if (start && end) {
                url += '?dateStart=' + start + '&dateEnd=' + end + '&typeModule=' + type;
            }
            $.ajax({
                'url': url,
                'method': 'GET',
            }).done(function (response) {
                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', response);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(response);
            });
        });
    }


    $('body').on('click', function(e) {
        if ( !CALENDAR.is(e.target) && CALENDAR.has(e.target).length === 0 ) {
            CALENDAR.removeClass('cursor-add cursor-clear');
            $('.fc-addAvailability-button').removeClass('fc-state-active');
            $('.fc-removeAvailability-button').removeClass('fc-state-active');
            isAdding = false;
            isDeleting = false;
        }
    });
    //day activities
    $('#nextDayActivities').on('click', function () {
        var start = $('#activitiesDaySearch').val();
        var nextDay = moment(start).add(1, 'days');
        $.ajax({
            url: '/learner/dayActivities?day=' + nextDay.format('YYYY-MM-DD'),
            method: "GET",
            success: function (data) {
                $('#activitiesDaySearch').val(nextDay.format('YYYY-MM-DD'));
                $('#dayRangeDateActivities').html('<span>'+ nextDay.format('DD/MM/YYYY') + '</span>');
                $('#activitiesDayTable table tbody').html(data);
            }
        })
    });

    $('#prevDayActivities').on('click', function () {
        var start = $('#activitiesDaySearch').val();
        var prevDay = moment(start).subtract(1, 'days');
        $.ajax({
            url: '/learner/dayActivities?day=' + prevDay.format('YYYY-MM-DD'),
            method: "GET",
            success: function (data) {
                $('#activitiesDaySearch').val(prevDay.format('YYYY-MM-DD'));
                $('#dayRangeDateActivities').html('<span>'+ prevDay.format('DD/MM/YYYY') + '</span>');
                $('#activitiesDayTable table tbody').html(data);
            }
        })
    });
    //end day activities
    $('#prevWeekActivities').on('click', function () {
        var start = $('#activitiesWeekStart').val();
        var activitiesWeekStart = GetLastWeekStart(start);
        var activitiesWeekEnd = GetLastWeekEnd(start);
        $.ajax({
            url: '/learner/activitiesByRange',
            method: "POST",
            data : {
                'startDate': activitiesWeekStart.format('YYYY-MM-DD'),
                'endDate': activitiesWeekEnd.format('YYYY-MM-DD')
            },
            success: function (data) {
                $('#activitiesWeekStart').val(activitiesWeekStart.format('YYYY-MM-DD'));
                $('#activitiesWeekEnd').val(activitiesWeekEnd.format('YYYY-MM-DD'));
                $('#weekRangeDateActivities').html('<span>'+ activitiesWeekStart.format('DD/MM/YYYY') + '</span>–<span>'+ activitiesWeekEnd.format('DD/MM/YYYY') +'</span>');
                $('#activitiesWeekTable table tbody').html(data);
            }
        })
    });
    $('#nextWeekActivities').on('click', function () {
        var start = $('#activitiesWeekEnd').val();
        var activitiesWeekStart = GetNextWeekStart(start);
        var activitiesWeekEnd = GetNextWeekEnd(start);
        $.ajax({
            url: '/learner/activitiesByRange',
            method: "POST",
            data : {
                'startDate': activitiesWeekStart.format('YYYY-MM-DD'),
                'endDate': activitiesWeekEnd.format('YYYY-MM-DD')
            },
            success: function (data) {
                $('#activitiesWeekStart').val(activitiesWeekStart.format('YYYY-MM-DD'));
                $('#activitiesWeekEnd').val(activitiesWeekEnd.format('YYYY-MM-DD'));
                $('#weekRangeDateActivities').html('<span>'+ activitiesWeekStart.format('DD/MM/YYYY') + '</span>–<span>'+ activitiesWeekEnd.format('DD/MM/YYYY') +'</span>');
                $('#activitiesWeekTable table tbody').html(data);
            }
        })
    });

    $('[data-action="addWorkingHours"]').on('click', function(event) {
        event.preventDefault();

        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';
            let end = '', start = '';
            if ( $(selector).find('.form-check-input').is(':checked') ) {
                start = $(selector).find('[data-role="startTime"]').val();
                end = $(selector).find('[data-role="endTime"]').val();

            }
            days.push({
                dayName: day,
                start: start,
                end: end,
            });
        }

        let lunch_start_time = $("#lunch_start_time").val();
        let lunch_end_time = $("#lunch_end_time").val();

        $.ajax({
            'url': '/learner/working/hours',
            'method': 'POST',
            'data': {'lunchStartTime' : lunch_start_time, 'lunchEndTime' : lunch_end_time,
                'days': days,
            }
        }).done(function (response) {
            location.reload();
        }).fail(function (response) {
            console.log('err0r')
        });
    });

    $('#repeatTime').on('click', function(event) {
        event.preventDefault();
        repeatTime();
    });

    function GetNextWeekStart(today) {
        today = moment(today);

        return today.add(1, 'days');
    }

    function GetNextWeekEnd(today) {
        var nextMonday = GetNextWeekStart(today);

        return nextMonday.add(6, 'days');
    }

    function GetLastWeekStart(today) {
        today = moment(today);

        return today.subtract(7, 'days');
    }

    function GetLastWeekEnd(today) {
        var lastMonday = GetLastWeekStart(today);
        return lastMonday.add(6, 'days');
    }




});

function dateFormat(date) {
    return date.format();
}
function repeatTime() {
    let startTime = $('#startTime-Monday').val();
    let endTime = $('#endTime-Monday').val();

    let startTimeAfternoon = $('#startTimeAfternoon-Monday').val();
    let endTimeAfternoon = $('#endTimeAfternoon-Monday').val();
    $('[id^="startTime-"]').each(function () {
        $(this).val(startTime);
    });

    $('[id^="endTime-"]').each(function () {
        $(this).val(endTime);
    });

    $('[id^="startTimeAfternoon-"]').each(function () {
        $(this).val(startTimeAfternoon);
    });

    $('[id^="endTimeAfternoon-"]').each(function () {
        $(this).val(endTimeAfternoon);
    })
}