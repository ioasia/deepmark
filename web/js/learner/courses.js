$(document).ready(function() {
    bindRatings();
    bindCalendar();
    bindCourseStart();
    bindNavigationCircles();
    $(window).resize(function() {
        bindNavigationCircles();
    });
    let btn = $('[data-action="minimize-sidebar"]');
    if (btn.length) {
        btn.on('click', function () {
            setTimeout(function() {
                bindNavigationCircles();
            }, 300);
        })
    }
});

function bindCalendar() {

    const headerOptions = {
        left: 'month, agendaWeek',
        center: 'prev,title,next',
        right: 'addAvailability, removeAvailability'
    };

    const calendar = $('#learnerCourseCalendar');
    const interventionId = $('#interventionId').val();

    if (!calendar) return;

    calendar.fullCalendar({
        header: headerOptions,
        selectable: true,
        contentHeight: 600,
        displayEventTime : false,
        firstDay: 1,
        defaultView: 'month',
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        maxTime: '21:00:00',
        fixedWeekCount: false,
        eventColor: 'green',
        select: function (start, end, jsEvent, view) {},
        eventClick: function (calEvent, jsEvent, view) {},
        viewRender: function (view, element) {
            $.ajax({
                'url': '/learner/courses/calendars/bookings/'+ interventionId,
                'method': 'GET',
            }).done(function (response) {
                calendar.fullCalendar('removeEvents');
                calendar.fullCalendar('addEventSource', response);
                calendar.fullCalendar('rerenderEvents' );
            });
        }
    });
}

function bindRatings() {

    $('.ratings-widget i').on('click', function() {

        $('#evaluationBottomRow').css({ height: '50px' });

        $(this).removeClass('fal');
        $(this).addClass('fas');

        $(this).prevAll('i').each(function() {
            $(this).removeClass('fal');
            $(this).addClass('fas');
        });

        $(this).nextAll('i').each(function() {
            $(this).removeClass('fas');
            $(this).addClass('fal');
        });
    });

    let moduleEvaluation = document.getElementById('module-evaluation');
    if (moduleEvaluation) {
        if ( $('#module-evaluation').is(':hidden')) {
            $('[data-action="nextModule"]').one('click', function (e) {
                e.preventDefault();
                $('#module-evaluation').modal("show");

                // update status is done
                let id = $(this).attr('module-id');
                $.ajax({
                    'url': '/learner/courses/done',
                    'method': 'POST',
                    'data' : {
                        'module_id': id
                    }
                }).done(function (response) {
                    // show information here
                }).fail(function (response) {
                    if (response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('message')) {
                        swal('Error',response.responseJSON.message, 'error');
                    }
                });

            });
            $('[data-action="backDetail"]').one('click', function (e) {
                e.preventDefault();
                $('#module-evaluation').modal("show");
            });
        }
    } else {
        $('[data-action="backDetail"]').one('click', function(e) {
            let id = $(this).attr('module-id');
            window.location.href = "/learner/courses/" + id + "/module";
        });
        $('[data-action="nextModule"]').one('click', function(e) {
            e.preventDefault();
            let nextModule = $(this).attr('next-module-id');
            let id = $(this).attr('module-id');
            if (nextModule) {
                window.location.href = "/learner/courses/" + nextModule + "/module?doneModule=" + id;
            } else {
                window.location.href = "/learner/courses/finish/" + id;
            }
        });
    }

    $('[data-action="evaluate"]').on('click', function() {
        evaluation($(this).attr('data-mod'));
    });
}

function evaluation(id) {
    let learnedTime = '';
    let notation = $('.evaluation-form').find('.fas').length;
    let notationTrainer = $('.evaluation-trainer-form').find('.fas').length;
    $('.loader').show();
    var isGradeTrainer = $('#isGradeTrainer').val();
    var designationTrainer = isGradeTrainer == 1 ? $('#observationsTrainerArea')[0].value: null;
    var startTime = $('#startTime').text();
    var learnedEchoTime = Number($('#timeEchoInSeconds').text());
    if (learnedEchoTime) {
        learnedTime = learnedEchoTime;
    } else {
        learnedTime = Number($('#timeInSeconds').text());
    }
    let learnedTimeSCORM = Number($('#timeInSecondsSCORM').text());

    $.ajax({
        'url': '/learner/courses/notation',
        'method': 'POST',
        'data' : {
            'module_id': id,
            'designation': $('#observationsArea')[0].value,
            'designationTrainer': designationTrainer,
            'notation': notation,
            'notationTrainer': notationTrainer,
            'startTime': startTime,
            'learnedTime': learnedTime + learnedTimeSCORM
        }
    }).done(function (response) {
        $('.loader').hide();
        let nextModule = $('input:hidden[name=nextModule]').val()
        if (nextModule) {
            window.location.href = "/learner/courses/" + nextModule + "/module?doneModule=" + id;
        } else {
            window.location.href = "/learner/courses/finish/" + id;
        }
    }).fail(function (response) {
        $('.loader').hide();
        if (response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('message')) {
            swal('Error',response.responseJSON.message, 'error');
        }
    });
}

function bindNavigationCircles() {
    let scrollPosition = 0;
    let numModule = $('.div-module').length;
    let widthLine = $("#circlesOuterWrapper").width() - 40;
    let widthModule = widthLine/5;
    let widthLeftRight = ($(".last-step").width() - 40)/2;
    $(".display-text").css('cssText', 'width: ' + widthModule + 'px!important');
    $("#courseStepsMiddleLine").css('cssText','margin-left: ' + widthLeftRight + 'px; margin-right: ' + widthLeftRight + 'px');
    function scrollCircles(direction) {

        return function () {

            if (scrollPosition + direction < 0){
                return;
            } else if (scrollPosition + direction == 0) {
                $("#previousModule").addClass('cursorPointer');
            } else if (scrollPosition + 5 + direction >= numModule) {
                $("#nextModule").addClass('cursorPointer');
            }
            if (scrollPosition + 5 + direction < numModule) {
                $("#nextModule").removeClass('cursorPointer');
            }
            if (scrollPosition + direction > 0){
                $("#previousModule").removeClass('cursorPointer');
            }

            const slideSize = widthModule;
            const circles = $('#circlesInnerWrapper');
            const spaceLeft = circles.children().length * widthModule - circles.width() - 40 - 2 * widthModule;
            if ((scrollPosition + direction - 1) * slideSize > spaceLeft) return;

            scrollPosition += direction;
            const scroll = scrollPosition * slideSize * -1 + 'px';
            circles.css({ transform: 'translateX('+scroll+')'});
        }
    }
    $('#nextModule').on('click', scrollCircles(1));
    $('#previousModule').on('click', scrollCircles(-1));
    let currentModuleId = $("#currentModuleId").val();
    if (currentModuleId != 0 && currentModuleId >= 6){
        let nextNumber = currentModuleId - 3;
        for(let i = 0; i < nextNumber; i++ ){
            $('#nextModule').click();
        }
    } else {
        if(numModule >= 6) {
            $("#previousModule").addClass('cursorPointer');
        }
    }
}

function bindCourseStart() {
    $('.start-course-button').on('click', function () {
        startCourse($(this).data('intervention'));
    });
}

function startCourse(id) {
    $.ajax({
        'url': '/learner/courses/start/' + id,
        'method': 'PUT',
        'data': {}
    }).done(function (response){
        document.location = '/learner/courses/' + response + '/show';
    }).fail(function (response) {
        console.log(response);
    })
}

function exportHTML(){
    let header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
        "xmlns:w='urn:schemas-microsoft-com:office:word' "+
        "xmlns='http://www.w3.org/TR/REC-html40'>"+
        "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    let footer = "</body></html>";
    let data = $("#editor").val();
    let sourceHTML = header+data+footer;

    let source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    let fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'note.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
}