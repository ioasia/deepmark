const HEADER_OPTIONS = {
    left: '',
    center: 'prev, title, next',
    right: 'cancelBooking',

};

$(document).ready(function() {
    const CALENDAR = $('#onlineSessionCalendar'),
          DASHBOARD = $('#onlineSessionModule');
    let locate = $('#locate').val();
    let morning = 'Morning';
    let afternoon = 'Afternoon';
    if(locate == 'fr'){
        morning = 'Matin';
        afternoon = 'Après midi';
    }

    let moduleId = DASHBOARD.attr('data-id'),
        dateStart = DASHBOARD.attr('data-start'),
        dateEnd = DASHBOARD.attr('data-end');

    $('[data-action="calendar"]').on('click', function() {
        $('[data-target="calendar"]').removeClass('hidden');
        CALENDAR.fullCalendar('option', 'height', 800);
    });

    function booking(moduleId, bookingDate, trainerId, learnerId) {
        $.ajax({
            'url': '/learner/courses/booking/online',
            'method': 'POST',
            'data': {
                'module_id': moduleId,
                'booking_date': bookingDate,
                'live_resource': trainerId,
                'learner_id': learnerId
            }
        }).done(function(response) {
            let view = $('#onlineSessionCalendar').fullCalendar('getView');
            updateOnlineCalendar(moduleId, dateStart, dateEnd, view);
            window.location.reload();
        }).fail(function(response) {
            console.log(response);
        });
    }

    function cancelBooking(moduleId, learnerId) {
        $.ajax({
            'url': '/learner/courses/unbooking',
            'method': 'POST',
            'data': {
                'module_id': moduleId,
                'learner_id': learnerId
            }
        }).done(function(response) {
            let view = $('#onlineSessionCalendar').fullCalendar('getView');
            updateOnlineCalendar(moduleId, dateStart, dateEnd, view);
            window.location.reload();
        }).fail(function(response) {
            console.log(response);
        });
    }

    $('#calendar-tab-link').on('click', function () {
        let view = $('#onlineSessionCalendar').fullCalendar('getView');
        updateOnlineCalendar(moduleId, dateStart, dateEnd, view);
    });

    $('#cancel-booking').off('click').on('click', function () {
        let learnerElement = document.getElementById('learner-id');
        if (learnerElement) {
            cancelBooking(moduleId, $('#learner-id').val());
        } else {
            cancelBooking(moduleId, null);
        }

    });

    $('#cancel-booking-info').off('click').on('click', function () {
        $('#cancel-confirmation-modal').modal();
    });

    $("#assignments").change(function() {
        let trainerId = $(this).val();
        if (trainerId) {
            $(".selectTrainer").hide();
        }
        else {
            $(".selectTrainer").show();
        }

        let view = $('#onlineSessionCalendar').fullCalendar('getView');
        updateOnlineCalendar(moduleId, dateStart, dateEnd, view, trainerId);
    });

    $('#button-trainer').on('click','button', function () {
        let trainerId = $(this)[0].getAttribute('data-trainer-id');
        let view = $('#onlineSessionCalendar').fullCalendar('getView');
        updateOnlineCalendar(moduleId, view.start.format(), view.end.format(), view, trainerId);
    });

    CALENDAR.fullCalendar({
        header: HEADER_OPTIONS,
        eventLimit: 3,
        selectable: true,
        firstDay: 1,
        defaultView: 'month',
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        aspectRatio: 2.2,
        minTime: '08:00:00',
        maxTime: '21:00:00',
        displayEventTime: false,
        contentHeight: 'auto',
        locale: locate,

        /*validRange: {
            start: dateStart,
            end: dateEnd,
        },*/
        eventColor: COLORS.green,
        customButtons: {
            cancelBooking: {
                text: $('#cancel-booking-text').html(),
                click: function () {
                    $('#cancel-confirmation-modal').modal();
                }
            }
        },
        select: function (start, end, jsEvent, view) {},
        eventClick: function (calEvent, jsEvent, view) {
            if (!calEvent.sessions || calEvent.sessions.length === 0) return;
            let isChecked = false;
            let isAfternoon = false;
            let disable = false;

            let optionMorningArray = [];
            let disabledMorningArray = [];
            let optionAfternoonArray = [];
            let disabledAfternoonArray = [];
            let selectedFlag = false;

            $('#online-calendar-modal').modal();

            const options = Object.keys(calEvent.sessions).map((date, index) => {
                disable = false;
                if (jQuery.isEmptyObject(calEvent.sessions[date].trainers)) {
                    disable = true;
                }
                let checked = '';
                if (!disable && !isChecked) {
                    checked = 'checked';
                    isChecked = true;
                }

                let title = '';
                let endDate = calEvent.sessions[date].endDate;
                let startDate = date.split(' ')[1];
                if (index == 0 && endDate < "12:00" && startDate < "12:00") {
                    title = title + morning+': <br />';
                } else if (endDate >= "12:00" && startDate >= "12:00" && !isAfternoon) {
                    isAfternoon = true;
                    title = title + '<br /><br />'+afternoon+': <br />';
                }
                return `${title}<label class="dm-radio-btn"><input type="radio" data-afternoon="${isAfternoon}" name="modal-hour-select" ${ disable ? 'disabled' : ''} ${checked} value="${date}"/><span>${date.split(' ')[1]} - ${endDate}</span></label>`
            });

            $('#modal-hour-select').empty().append(options);

            $("input[name=modal-hour-select]").each(function(i,e){
                if ($(this).attr('data-afternoon') == 'true') {
                    optionAfternoonArray[i] = $(this);
                } else {
                    optionMorningArray[i] = $(this);
                }
            })

            for ( var i = 0, l = optionMorningArray.length; i < l; i++ ) {
                //console.log('========Start morning=========')
                //console.log(i)
                //console.log($.inArray( i, disabledMorningArray ))
                //console.log($( optionMorningArray[i]).attr('disabled'))

                if (!selectedFlag && $( optionMorningArray[i]).attr('disabled') != 'disabled') {
                    $( optionAfternoonArray[i] ).prop( "checked", true );
                    selectedFlag = true;
                }

                if ($( optionMorningArray[i]).attr('disabled') == 'disabled' && $.inArray( i, disabledMorningArray ) === -1) {
                    //console.log('Not found')
                    //console.log(i)
                    //console.log(optionMorningArray[i])

                    if ($( optionMorningArray[i-1]).attr('disabled') != 'disabled') {
                        $( optionMorningArray[i-1] ).prop( "disabled", true );
                        disabledMorningArray[i-1] = i-1;
                    }

                    if ($( optionMorningArray[i+1]).attr('disabled') != 'disabled') {
                        //console.log($( optionMorningArray[i+1]).attr('checked'))
                        $( optionMorningArray[i+1] ).prop( "disabled", true );
                        disabledMorningArray[i+1] = i+1;
                    }
                    //console.log('End Not found')
                }
                //console.log('========End morning=========')
            }

            for ( var i = 0, l = optionAfternoonArray.length; i < l; i++ ) {
                //console.log('========Start Afternoon=========')
                //console.log(i)
                //console.log($.inArray( i, disabledAfternoonArray ))
                //console.log($( optionAfternoonArray[i]).attr('disabled'))

                if (!selectedFlag && $( optionMorningArray[i]).attr('disabled') != 'disabled') {
                    $( optionAfternoonArray[i] ).prop( "checked", true );
                    selectedFlag = true;
                }

                if ($( optionAfternoonArray[i]).attr('disabled') == 'disabled' && $.inArray( i, disabledAfternoonArray ) === -1) {
                    //console.log('Not found')
                    //console.log(i)
                    //console.log(optionAfternoonArray[i])

                    if ($( optionAfternoonArray[i-1]).attr('disabled') != 'disabled') {
                        $( optionAfternoonArray[i-1] ).prop( "disabled", true );
                        disabledAfternoonArray[i-1] = i-1;
                    }

                    if ($( optionAfternoonArray[i+1]).attr('disabled') != 'disabled') {
                        //console.log($( optionAfternoonArray[i+1]).attr('checked'))
                        $( optionAfternoonArray[i+1] ).prop( "disabled", true );
                        disabledAfternoonArray[i+1] = i+1;
                    }
                    //console.log('Not found')
                }
                //console.log('========End Afternoon=========')
            }

            function addTrainerOptions() {
                const hourValue = $('input[name="modal-hour-select"]:checked').val();
                const trainerIds = Object.keys(calEvent.sessions[hourValue]['trainers'] || {});

                const trainerOptions = trainerIds.map(id =>
                    `<option value="${id}">${calEvent.sessions[hourValue]['trainers'][id]}</option>`
                );

                $('#modal-trainer-select').empty().append(trainerOptions);
                $("#modal-trainer-select").selectpicker("refresh");
            }

            addTrainerOptions();
            $('input[name="modal-hour-select"]').off('change').change(addTrainerOptions);

            $('#book-online-session').off('click').click(function () {
                let learnerElement = document.getElementById('learner-id');
                if (learnerElement) {
                    booking(moduleId, $('input[name="modal-hour-select"]:checked').val(), $('#modal-trainer-select').val(), $('#learner-id').val());
                } else {
                    booking(moduleId, $('input[name="modal-hour-select"]:checked').val(), $('#modal-trainer-select').val(), null);
                }
            });
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            $(".fc-agendaWeek-button").prop('disabled', true);
            $(".fc-agendaWeek-button").addClass('fc-state-disabled');

            updateOnlineCalendar(moduleId, dateStart, dateEnd, view);
        },
        eventRender: function (event, element) {
            // Don't render events on the weekends
            /*if ( (event.start).weekday() === 5 || (event.start).weekday() === 6) {
                return false;
            }*/
            if (event.color === COLORS.blue) {
                element.find(".fc-title").prepend('<i class="fa fa-calendar-check"></i>');
                element.find(".fc-title").append(`<span class="calendar-trainer-name">${event.trainer}</span>`);
            }
        }
    });
});

function updateOnlineCalendar(moduleId, dateStart, dateEnd, view, trainerId = null) {
    // fixed performance load 1 month of the view instead of load from start date to end date of the module
    let viewStart = moment(view.start.toString()).format('YYYY-MM-DD 00:00:00');
    let viewEnd = moment(view.end.toString()).format('YYYY-MM-DD 00:00:00');

    console.log('Start date of view: ' + viewStart + ' || End date of view: ' + viewEnd);
    console.log('Start date of module: ' + dateStart + ' || End date of module: ' + dateEnd);

    if (viewStart < dateStart) {
        viewStart = dateStart;
    }

    if (viewEnd > dateEnd) {
        viewEnd = dateEnd;
    }

    let aDateStart = viewStart.split(' ');
    let aDateEnd = viewEnd.split(' ');

    let url = '/learner/courses/calendars/online/' + moduleId + '?dateStart=' + aDateStart[0] + '&dateEnd=' + aDateEnd[0];
    if (trainerId)
        url = '/learner/courses/calendars/online/' + moduleId + '?dateStart=' + aDateStart[0] + '&dateEnd=' + aDateEnd[0] + '&trainer=' + trainerId;

    $.ajax({
        'url': url,
        'method': 'GET',
    }).done(function (response) {

        const events = response.sessions.map(day => {

            const sessions = Object.keys(day.sessions);
            day.title = $('#book-session-text').html();
            if (sessions.length === 0) day.color = COLORS.red;
            else if (day.sessionsRemoved > sessions.length) day.color = COLORS.orange;

            return day;
        });

        if (response.booked) {
            let event = events.find(event => moment(event.start).isSame(moment(response.booked.booking_date), 'day'));
            if (!event) {
                event = {
                    start: response.booked.booking_date,
                    end: response.booked.booking_date,
                };
                events.push(event);
            }

            event.title = moment(response.booked.booking_date).format('HH:mm') + '\n';
            event.color = COLORS.blue;
            event.trainer = response.booked.trainer.first_name + ' ' + response.booked.trainer.last_name;
        }

        $('#onlineSessionCalendar').fullCalendar('removeEvents');
        $('#onlineSessionCalendar').fullCalendar('addEventSource', events);
        $('#onlineSessionCalendar').fullCalendar('rerenderEvents');
    });

}

$('#module_information').find(".show-calendar").click(function (el) {
    $(".calendar-hidden").toggle();
    $(".show-calendar").each(function (key, value) {
        $(value).toggle();
    });
});
