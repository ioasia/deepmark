$(window).resize(function() {
    let quizPlayedId = $("#quizUserPlayedId").val();
    setTimeout( function() {
        $('.user-answer'+quizPlayedId).each(function(index){
            let height_user_ans = $(this).height();
            let height_correct_ans = $(".correct-answer"+quizPlayedId).eq(index).height();
            if (height_user_ans != 0 && height_correct_ans != 0) {
                // console.log(height_user_ans +" "+ height_correct_ans);
                if (height_user_ans > height_correct_ans) {
                    $(".correct-answer"+quizPlayedId).eq(index).height(height_user_ans);
                    $(this).height(height_user_ans);
                } else {
                    $(".correct-answer"+quizPlayedId).eq(index).height(height_correct_ans);
                    $(this).height(height_correct_ans);
                }
            } else {
                $(this).css("height", "auto");
                $(".correct-answer"+quizPlayedId).eq(index).css("height", "auto");

            }
        });
    },3000 );
});
$(document).ready(function () {
    // Init
    quizService.initQuiz();

    $(document).on('click', ".learn-more-btn", function() {
        $('.swal2-modal').toggleClass('flipped');
        setTimeout( function() {
            $("#contentOld").hide();
            $("#newContent").show();
        }, 500 );
    });

    $(document).on('click', ".backBtn", function() {
        $('.swal2-modal').toggleClass('flipped');
        setTimeout( function() {
            $("#contentOld").show();
            $("#newContent").hide();
        }, 500 );
    });
});

// Vars
let currStep = 0;
let timerCount = null;
let timerToSave = 0;
let timerLast = 0;
let answers = [];
let play_id = null;
let hasOpenQuestion = false;
let isSurvey = false;
// Translations
translations = {
    too_late_title: $('#translations').data('too_late_title'),
    quiz_complete: $('#translations').data('quiz_complete'),
    quiz_test: $('#translations').data('quiz_test'),
    open: $('#translations').data('open'),
    waiting: $('#translations').data('waiting'),
    servey: $('#translations').data('servey'),
    too_late_description: $('#translations').data('too_late_description'),
    good_answer: $('#translations').data('good_answer'),
    good_answer_p: $('#translations').data('good_answer_p'),
    in_review_title: $('#translations').data('in_review_title'),
    in_review_description: $('#translations').data('in_review_description'),
    in_review_step_title: $('#translations').data('in_review_step_title'),
    in_review_step_question: $('#translations').data('in_review_step_question'),
    in_review_quiz_title: $('#translations').data('in_review_quiz_title'),
    in_review_quiz_sub_title: $('#translations').data('in_review_quiz_sub_title'),
    survey_title: $('#translations').data('survey_title'),
    survey_description: $('#translations').data('survey_description'),
    survey_step_title: $('#translations').data('survey_step_title'),
    survey_step_question: $('#translations').data('survey_step_question'),
    survey_quiz_title: $('#translations').data('survey_quiz_title'),
    survey_quiz_sub_title: $('#translations').data('survey_quiz_sub_title'),
    add_template: $('#translations').data('add_template'),
    add_document: $('#translations').data('add_document'),
    loading: $('#translations').data('loading'),
    file_sended: $('#translations').data('file_sended'),
    error_unknown: $('#translations').data('error_unknown'),
    upload_complete: $('#translations').data('upload_complete'),
    restart_recording: $('#translations').data('restart_recording'),
    data_point: $('#translations').data('point'),
    no_answer_found: $('#translations').data('no_answer_found'),
    wrong_format_answer: $('#translations').data('wrong_format_answer'),
    attention: $('#translations').data('attention'),
    pending_score: $('#translations').data('pending_score'),
    question_pending: $('#translations').data('question_pending'),
    title_servey: $('#translations').data('title_servey'),
    learn_more: $('#translations').data('learn_more'),
    learn_more_btn: $('#translations').data('learn_more_btn'),
};

// QuizService
let quizService = {
    // Init actions
    initQuiz() {
        quizService.bindTabs();
        quizService.navSteps();
        quizService.validateQuestion();
        quizService.startQuiz();
        quizService.showResults();
    },
    // Duration timer
    timer() {
        const start = new Date.parse('1970-01-01 00:00:00');
        const end = new Date.parse(quiz_duration);
        let diff = Math.round((end.getTime() - start.getTime()) / 1000);
        timerCount = window.setInterval(function () {
            var seconds = Math.floor(diff);
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours %= 24;
            minutes %= 60;
            seconds %= 60;
            $('#quiz_timer').text((hours > 0 ? (hours.toString().length == 1 ? '0' + hours : hours) + "H" : '00:') + (minutes > 0 ? (minutes.toString().length == 1 ? '0' + minutes : minutes) + ":" : '') + (seconds.toString().length == 1 ? '0' + seconds : seconds));
            diff -= 1;
            if (diff <= -1) {
                // Hide All
                swal(translations.too_late_title, translations.too_late_description, "error", {});
                $('#quiz-step-resume, #quiz-start, .quiz-content').hide();
                quizService.finished();
            }
        }, 1000);
    },
    // Duration timer Global
    timerGlobal() {
        timerCount = window.setInterval(function () {
            timerToSave += 1;
        }, 1000);
    },
    // Start quiz 
    startQuiz() {
        $('.startQuiz').unbind('click');
        $('.startQuiz').click(function (e) {
            e.preventDefault();
            // Create play ID
            quizService.startQuizProcess();
            $('#quiz-start').hide();
            $('#quiz-start #quiz-error').hide();
            // Timer 3,2,1
            $('#quiz-header').show();
            $('#quiz-start-timer').show();
            $('#quiz-start-timer .timer-3').fadeIn().delay(700).fadeOut().delay(700).hide(0, function() {
                $('#quiz-start-timer .timer-2').fadeIn().delay(700).fadeOut().delay(700).hide(0, function() {
                    $('#quiz-start-timer .timer-1').fadeIn().delay(700).fadeOut().delay(700).hide(0, function() {
                        // Show steps & question 1
                        $('#quiz-start-timer').hide();
                        $('#quiz-header').hide();
                        $('#quiz-left-1').hide('fade', {});
                        $('#quiz-left-2').show('fade', {}, function () {
                            $(this).css('position', 'unset');
                        });
                        $('.quiz-content').show('fade', {});
                        // quiz-left step 1 default
                        $('.title-step').html($('.step-quiz tr:eq(0)').attr('data-title'));
                        $('.total-question-step').html($('.step-quiz tr:eq(0)').attr('data-total-question'));
                        $('.point-question-step').html($('.step-quiz tr:eq(0)').attr('data-point-step'));

                        quizService.timerGlobal();
                        if (quiz_duration != false) {
                            setTimeout(quizService.timer());
                        }
                    });
                });
            });
        })
    },
    startQuizProcess() {
        $.ajax({
            'url': '/learner/quiz/savePlay',
            'method': 'POST',
            'data': {
                module_id: $('#module_id').val(),
                quiz_id: $('#quiz_id').val()
            },
        }).done(function (res) {
            if (res.status == 'success') {
                play_id = res.play_id;
            } else {
                quizService.showError();
            }
        }).fail(function () {
            quizService.showError();
        });
    },
    showError() {
        $('#quiz-start-timer').hide();
        $('.quiz-content').hide();
        $('#quiz-start #quiz-error').show();
        $('#quiz-start').show();
    },
    // Validate question
    validateQuestion() {


        $('.validateQuestion').unbind('click');
        $('.validateQuestion').click(function () {
            let element = $(this).parent().parent().parent();
            if (play_id != null) {
                // Stop song and videos
                quizService.stopSongsAndVideos($(this).data('question_id'));
                
                // Const
                const id_result = $(this).data('id_result');
                const question_id = $(this).data('question_id');
                const checkLearnMore = $("#checkLearnMore"+question_id).val();
                const type = $(this).data('type');
                const msg_ok = $(this).data('msg_ok');
                const sub_msg_ok = $(this).data('sub_msg_ok');
                const msg_nok = $(this).data('msg_nok');
                const sub_msg_nok = $(this).data('sub_msg_nok');
                const file_descriptor = $('#' + id_result).data('file_descriptor');
                // Get answer (specific by question type)
                const answer = eval('quizService.' + type + '.getAnswer("' + question_id + '")');
                if (answer == false) {
                    swal({
                        html: '<h2 class="bold">' +  translations.attention + '<h4>' + '<h4>' +  translations.no_answer_found + '<h4>',
                        // title: translations.no_answer_found,
                    });
                } else if (answer == 'wrong format') {
                    swal({
                        html: '<h2 class="bold">' +  translations.attention + '<h4>' + '<h4>' +  translations.wrong_format_answer + '<h4>',
                        // title: translations.no_answer_found,
                    });
                } else {
                    // Change question
                    const toShow = $(this).data('show');
                    const object = $(this).parent().parent().parent();
                    // $(this).parent().parent().parent().hide('slide', {'direction': 'left'}, 500, function () {
                    //     $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                    // });
                    // Params Ajax
                    const params = {
                        play_id: play_id,
                        module_id: $('#module_id').val(),
                        quiz_id: $('#quiz_id').val(),
                        question_id: question_id,
                        answer: answer,
                        file_descriptor: file_descriptor,
                        timer_total: timerToSave,
                        timer: timerToSave - timerLast
                    }
                    // Ajax
                    $.ajax({
                        'url': '/learner/quiz/validateQuestion',
                        'method': 'POST',
                        'data': params,
                    }).done(function (res) {
                        if (res.status == 'error') {
                            // Error -> Timer finished or other impossible errors if try to cheat
                            quizService.finished();
                        } else {
                            timerLast = timerToSave;
                            if (type == 'open') {
                                hasOpenQuestion = true;
                            }
                            if (res.is_survey == 1) {
                                isSurvey = true;
                            }
                            // Answers if type = learning
                            if (res.answers != false) {
                                text = `<div class="row">
                                            <div class="col-md-12 pad-t-10 pad-b-10 text-center btn-deepmark-green">
                                                <span class="c-white bold">${translations.good_answer}</span>
                                            </div>
                                        </div>`;
                                
                                switch (type) {
                                    case 'qcu' :
                                        text += `<div class="row pad-b-10" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);">`;
                                        res.answers.map((answer, key) => {
                                            let optionTitle = '';
                                            let optionChecked = '';
                                            if (res.answers[key]['correct'] === undefined) {
                                                optionTitle = res.answers[key]['fail'];
                                                optionChecked = '';
                                                console.log(res.answers[key]['fail'])
                                            } else {
                                                optionTitle = res.answers[key]['correct'];
                                                optionChecked = 'checked';
                                                console.log(res.answers[key]['correct'])
                                            }
                                            text += `<div class="col-md-2 pad-t-10">
                                                    <div class="quiz-float-left text-right">
                                                        <input disabled type="radio" class="marg-t-2 border-radius-1 radio-custom-green" value="${optionTitle}" ${optionChecked} style="width: 30px; height: 30px" />
                                                    </div>
                                                </div>
                                                <div class="col-md-8 pad-t-10">
                                                    <div class="col-md-12 text-center pad-5 btn-deepmark-green-light">
                                                        <p class="no-margin">${optionTitle}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 pad-t-10"></div>`;
                                        });
                                        text += `</div>`;
                                        break;

                                    case 'qcm' :
                                        text += `<div class="row">
                                                    <div class="col-md-12 pad-15 font-size-13 qcmContainer" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);">
                                                        <div class="qcmOptions">`;
                                        
                                        res.answers.map((answer, key) => {
                                            let optionTitle = '';
                                            let optionChecked = '';
                                            if (res.answers[key]['correct'] === undefined) {
                                                optionTitle = res.answers[key]['fail'];
                                                optionChecked = '';
                                                console.log(res.answers[key]['fail'])
                                            } else {
                                                optionTitle = res.answers[key]['correct'];
                                                optionChecked = 'checked';
                                                console.log(res.answers[key]['correct'])
                                            }

                                            text += `<div class="row checkbox-false">
                                                        <div class="col-md-1 no-padding">
                                                            <label class="form-check-label">
                                                                <input type="checkbox" disabled="" class="marg-t-2 border-radius-0 radio-custom-green" ${optionChecked} />
                                                            </label>
                                                        </div>
                                                        <div class="col-md-10 no-padding-right">
                                                            <div class="col-md-12 text-center pad-5 btn-deepmark-green-light">
                                                                <p class="no-margin">${optionTitle}</p>
                                                            </div>
                                                        </div>
                                                    </div>`;
                                        });
                                        text += `   </div>
                                                </div>
                                            </div>`;
                                        break;

                                    case 'ordering' :
                                        text += `<div class="row">
                                                    <div class="col-md-12 pad-15 font-size-13 qcmContainer" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);">
                                                            <ul class="orderingOptions">`;

                                        res.answers.map((answer, key) => {
                                            text += `<li style="border: none;" class="ui-state-default bg-white">
                                                        <div class="row" style="align-items: center; justify-content: space-between">
                                                            <div class="good-answer-list-style">
                                                                <span style="margin-top: 4px; margin-left: -4px;">${key + 1}</span>
                                                            </div>
                                                            <div style="flex: 1; background-color: #cee5d0" class="quiz-ordering-value text-center pad-5">
                                                                ${answer}
                                                            </div>
                                                        </div>
                                                    </li>`
                                        });
                                        text += `   </ul>
                                                </div>
                                            </div>`;
                                        break;
                                    case 'drag_and_drop' :
                                        text += `<div class="row">
                                                    <div class="col-md-12 no-padding dragAndDropContainer">
                                                        <div class="dragAndDropOptions pad-15 font-size-13" style="box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);">
                                                            <ul class="flex--center">`;
                                        res.answers.map((answer) => {
                                                    text += `
                                                                <li class="match row" style="width: 100%!important; height: 110px!important; margin-top: 25px!important;">
                                                                    <div class="col-md-4 no-padding marg-t-0">
                                                                        <div class="uploadShow upload-area marg-t-0 quiz-float-right" style="width: 100%!important; max-width: 150px!important; height: 110px!important;">
                                                                            <img style="height: 100%; width: 100%;" src='/files/quiz/${question_id}/drag-and-drop/${answer[1]}' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 no-padding flex--center">
                                                                        <svg style="margin-left: -8px; margin-right: -19px; z-index: 9999; ">
                                                                            <rect x="0" y="55" width="92%" height="2" fill="#22aa73" />
                                                                            <circle cx="8" cy="55" r="8" stroke="" stroke-width=1 fill="#22aa73" />
                                                                            <circle cx="92%" cy="55" r="8" stroke="" stroke-width="1" fill="#22aa73" />
                                                                        </svg>
                                                                    </div>
                                                                    <div class="col-md-4 no-padding upload-area marg-t-0 droppable quiz-float-right" id="upload-id" style="position:relative; width: 100%!important; max-width: 150px!important; height: 110px!important;">`;
                                                        switch (answer[0]) {
                                                            case 'image_to_image' :
                                                                text += `   <div class="uploadShow">
                                                                                <img style="height: 100%; width: 100%;" src='/files/quiz/${question_id}/drag-and-drop/${answer[2]}' />
                                                                            </div>`;
                                                                break;
                                                            case 'image_to_text' :
                                                                text += `<span class="translate badge badge-info">${answer[2]}</span>`;
                                                                break;
                                                        }
                                                    text += `        </div>
                                                                </li>`;
                                        })
                                        text += `            </ul>
                                                        </div>
                                                    </div>
                                                </div>`;
                                        break;
                                    case 'category' :
                                        text += `<div class="row">
                                                    <div class="col-md-12 no-padding categoryContainer">
                                                        <div class="col-md-12 categoryOptions pad-15 font-size-13" style="box-shadow:  0px 1px 1px 1px rgba(0, 0, 0, 0.1);">`;
                                            res.answers.map((answer, key) => {
                                                text += `<div class="row marg-b-10">
                                                            <div class="col-md-8 text-left no-padding flex--center">
                                                                <div class="btn-deepmark-green-light pad-5 col-md-12">
                                                                   ${answer[0]}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 no-padding-right flex--center">
                                                                <div class="col-md-12 text-center pad-5 c-white btn-deepmark-green">
                                                                    ${answer[1]}
                                                                </div>
                                                            </div>
                                                        </div>`;
                                            });
                                         text += `</div>
                                                </div>
                                            </div>`;
                                        break;
                                    case 'text_to_fill' :
                                        text += `<div class="row">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-md-12 text-left text-justify textToFillOptions pad-15 font-size-13" style="box-shadow:  0px 1px 1px 1px rgba(0, 0, 0, 0.1);">`;
                                        text += res.answers_question.replace(/"marker"/g, '"badge border-radius-0 badge-success"');
                                        text += `        </div>
                                                    </div>
                                                </div>`;

                                        break;
                                    case 'scoring' :
                                        switch (res.type) {
                                            case 'free':
                                                text += `<div class="row">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-md-12 text-left text-justify scoringOptions pad-15 font-size-13" style="box-shadow:  0px 1px 1px 1px rgba(0, 0, 0, 0.1);">`;
                                                text += `<span class="aw-green">${res.answers}</span>`;
                                                text += `        </div>
                                                            </div>
                                                        </div>`;
                                                break;
                                            case 'stars':
                                                text += `<div class="row marg-t-15">
                                                            <div class="col-3 text-right flex-end-center">
                                                                <h3 class="no-margin bold c-red">${res.specifics.min}</h3>
                                                            </div>
                                                        <div class="col-6 no-padding-left no-padding-right text-center display-flex-center" style="justify-content: center;">`;
                                                            for (let i = 0; i < parseInt(res.answers); i++) {
                                                                text += `<i class="fas fa-star quiz-scoring-star"></i>`;
                                                            }
                                                            for (let i = 0; i < parseInt(res.specifics.val) - parseInt(res.answers); i++) {
                                                                text += `<i class="far fa-star quiz-scoring-star"></i>`;
                                                            }
                                                text += `</div>
                                                    <div class="col-3 flex-start-center">
                                                        <h3 class="no-margin bold c-green">${res.specifics.max}</h3>
                                                    </div></div>`;
                                                break;
                                            case 'bubbles':
                                                let cols;
                                                const answers = parseInt(res.answers);
                                                const val = parseInt(res.specifics.val);
                                                console.log(answers);
                                                if(val == 9) {
                                                    cols = [2, 8]
                                                } else if(val == 7) {
                                                    cols = [3, 6]
                                                } else {
                                                    cols = [4, 4]
                                                }
                                                text += `<div class="row marg-t-15 marg-b-15">
                                                            <div class="col-${cols[0]} justify-content-end c-red quiz-scoring-bubbles-text flex--center"><h3 class="bold no-margin">${res.specifics.min}</h3></div>
                                                            <div class="col-${cols[1]} no-padding-left no-padding-right text-center">`
                                                            for (let i = 1; i < val/2; i++) {
                                                                if(parseInt(res.answers) == i) {
                                                                    text += `<i class="c-red fas fa-check-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-${Math.ceil(val/2 - i)}"></i>`
                                                                } else {
                                                                    text += `<i class="c-red far fa-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-${Math.ceil(val/2 - i)}"></i>`
                                                                }
                                                            }
                                                        
                                                        if(parseInt(res.answers) == Math.ceil(val/2)) 
                                                            text += `<i class="c-lightgray fas fa-check-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-0"></i>`
                                                        else
                                                            text += `<i class="c-lightgray far fa-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-0"></i>`
        
                                                        for (let i = 1; i < val/2; i++) {
                                                            if(parseInt(res.answers) - Math.ceil(val/2) == i)
                                                                text += `<i class="c-green fas fa-check-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-${i}"></i>`
                                                            else 
                                                                text += `<i class="c-green far fa-circle quiz-scoring-bubbles quiz-scoring-bubbles-pos-${i}"></i>`
                                                        }
                                                    text += `</div>
                                                        <div class="col-${cols[0]} c-green quiz-scoring-bubbles-text flex--center justify-content-start"><h3 class="bold no-margin">${res.specifics.max}</h3></div>`
                                                break;
                                            case 'slider': 
                                                text += `<div class="row marg-t-15 marg-b-15">
                                                            <div class="col-12 text-center pad-t-20">
                                                                <div class="quiz-scoring-slider-left font-size-30 color-header">
                                                                    <button class="btn-score">${res.specifics.min}</button>
                                                                </div>
                                                                <div class="quiz-scoring-slider-center" id="slider">
                                                                    <div style="background-color: #0076fe; color: white;" id="quiz-scoring-slider-handle" class="ui-slider-handle quiz-scoring-slider-handle"></div>
                                                                </div>
                                                                <div class="quiz-scoring-slider-right font-size-30 color-header">
                                                                    <button class="btn-score">${res.specifics.max}</button>
                                                                </div>
                                                            </div>
                                                        </div>`;
                                                break;
                                        }
                                        break;
                                }
                            } else {
                                text = '';
                            }
                            // Confirmation message
                            if (res.is_survey == 1) {
                                object.hide('slide', {'direction': 'left'}, 500, function () {
                                    $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                });
                            } else if (res.status == 'wait') {
                                swal({
                                    customClass : "popup-quiz",
                                    title: translations.in_review_title,
                                    html: '<div class="marg-t-20 marg-b-20">' + translations.in_review_description + '</div>',
                                    type: "warning",
                                    showConfirmButton: true,
                                    confirmButtonText: "Next",
                                    timer: undefined,
                                    allowOutsideClick: false,
                                }).then(function() {
                                    object.hide('slide', {'direction': 'left'}, 500, function () {
                                        $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                    });
                                });
                                if (checkLearnMore != 0) {
                                    let content = $(".learn-more"+question_id).html();
                                    let contentOld =  $(".swal2-modal").html();
                                    $(".swal2-modal").empty();
                                    $(".swal2-modal").prepend('<div class="div-flip scroll-custom" id="newContent" style="display: none"></div><div class="div-flip" id="contentOld"></div>');
                                    $("#contentOld").html(contentOld);
                                    $("#newContent").html(content);
                                    $(".swal2-buttonswrapper").prepend('<button type="button" class="learn-more-btn swal2-styled badge-learn-more" aria-label="" style="display: inline-block;">'+ translations.learn_more_btn + ' +</button>');


                                    $(document).on('click', ".swal2-confirm", function() {
                                        object.hide('slide', {'direction': 'left'}, 500, function () {
                                            $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                        });
                                        swal.close();
                                    });

                                }

                            } else if (res.status == 'success') {
                                swal({
                                    // title: '',
                                    customClass : "popup-quiz",
                                    html: '<h2 class="point">' + res.points + ' ' + msg_ok + '</h2><div class="marg-t-20 marg-b-20">' + text + '</div>' + '<div class="message color-blue">' + sub_msg_ok + '</div>',
                                    type: "success",
                                    showConfirmButton: true,
                                    confirmButtonText: "Next",
                                    timer: undefined,
                                    allowOutsideClick: false,
                                }).then(function() {
                                    object.hide('slide', {'direction': 'left'}, 500, function () {
                                        $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                    });
                                });
                                
                                if ($('#slider').html() != undefined) {
                                    const handle = $("#quiz-scoring-slider-handle");
                                    $('#slider').slider({
                                        value: parseInt(res.answers),
                                        step: parseInt(res.specifics.step),
                                        min: parseInt(res.specifics.min),
                                        max: parseInt(res.specifics.max),
                                        create: function () {
                                            handle.text($(this).slider("value"));
                                        },
                                    });
                                    $("#slider").on("slide", function (event, ui) { return false; }); 
                                }
                                if (checkLearnMore != 0) {
                                    let content = $(".learn-more"+question_id).html();
                                    let contentOld =  $(".swal2-modal").html();
                                    $(".swal2-modal").empty();
                                    $(".swal2-modal").prepend('<div class="div-flip scroll-custom" id="newContent" style="display: none"></div><div class="div-flip" id="contentOld"></div>');
                                    $("#contentOld").html(contentOld);
                                    $("#newContent").html(content);
                                    $(".swal2-buttonswrapper").prepend('<button type="button" class="learn-more-btn swal2-styled badge-learn-more" aria-label="" style="display: inline-block;">'+ translations.learn_more_btn + ' +</button>');
                                    $(document).on('click', ".swal2-confirm", function() {
                                        object.hide('slide', {'direction': 'left'}, 500, function () {
                                            $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                        });
                                        swal.close();
                                    });
                                }
                            } else {
                                swal({
                                    // title: '',
                                    customClass : "popup-quiz",
                                    html: '<h2 class="point">' + res.points + ' ' + msg_nok + '</h2><div class="marg-t-20 marg-b-20">' + text + '</div>' + '<div class="message color-blue">' + sub_msg_nok + '</div>',
                                    type: "error",
                                    showConfirmButton: true,
                                    confirmButtonText: "Next",
                                    timer: undefined,
                                    allowOutsideClick: false,
                                }).then(function() {
                                    object.hide('slide', {'direction': 'left'}, 500, function () {
                                        $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                    });
                                });
                                if ($('#slider').html() != undefined) {
                                    const handle = $("#quiz-scoring-slider-handle");
                                    $('#slider').slider({
                                        value: parseInt(res.answers),
                                        step: parseInt(res.specifics.step),
                                        min: parseInt(res.specifics.min),
                                        max: parseInt(res.specifics.max), 
                                        create: function () {
                                            handle.text($(this).slider("value"));
                                        },
                                    });
                                    $("#slider").on("slide", function (event, ui) { return false; }); 
                                }
                                if (checkLearnMore != 0) {
                                    let content = $(".learn-more"+question_id).html();
                                    let contentOld =  $(".swal2-modal").html();
                                    $(".swal2-modal").empty();
                                    $(".swal2-modal").prepend('<div class="div-flip scroll-custom" id="newContent" style="display: none"></div><div class="div-flip" id="contentOld"></div>');
                                    $("#contentOld").html(contentOld);
                                    $("#newContent").html(content);
                                    $(".swal2-buttonswrapper").prepend('<button type="button" class="learn-more-btn swal2-styled badge-learn-more" aria-label="" style="display: inline-block;">'+ translations.learn_more_btn + ' +</button>');
                                    $(document).on('click', ".swal2-confirm", function() {
                                        object.hide('slide', {'direction': 'left'}, 500, function () {
                                            $('#' + toShow).show('slide', {'direction': 'right'}, 500);
                                        });
                                        swal.close();
                                    });
                                }
                            }
                            // Add answers in global tab
                            if (answers[currStep] == undefined) {
                                answers[currStep] = [];
                            }
                            res.type = type;
                            answers[currStep].push(res);
                            if (answers[currStep].length == quiz_steps[currStep].questions_count) {
                                let checkOpenTemplate = 0;
                                // Question list
                                object.hide();
                                $('#quiz-step-resume .questionList').html('');
                                const question_title = $('#quiz-step-resume .questionList').data('question_title');
                                let totalPointsNeeded = 0;
                                let totalPointsEarned = 0;
                                answers[currStep].map((question, index) => {
                                    if (question.type == 'open') {
                                        checkOpenTemplate = 1;
                                        let badgeColor = '';
                                        if (isSurvey == true) {
                                            badgeColor = 'badge-success';
                                            $('#quiz-step-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4 class="no-margin">' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 flex--center text-right"><span class="badge ' + badgeColor + '">' + translations.survey_step_question + '</span></div> <div class="col-md-2"></div> </div>');
                                        } else {
                                            badgeColor = 'badge-deepmark-orangelight';
                                            $('#quiz-step-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4 class="no-margin">' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 flex--center text-right"><span class="badge ' + badgeColor + '">' + translations.in_review_step_question + '</span></div> <div class="col-md-2"></div> </div>');
                                        }
                                   
                                        
                                        // $('#quiz-step-resume .questionList').append('<div class="row"><div class="col-md-12 text-center"><h4>' + question_title + ' ' + (index + 1) + '<small>/' + answers[currStep].length + '</small> <span class="marg-l-30 badge badge-deepmark-orangelight">' + translations.in_review_step_question + '</span></h4></div></div>');
                                    } else if (isSurvey == true) {
                                        $('#quiz-step-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4 class="no-margin">' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 flex--center text-right"><span class="badge badge-success">' + translations.survey_step_question + '</span></div> <div class="col-md-2"></div></div>');
                                    } else {
                                        $('#quiz-step-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4 class="no-margin">' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 flex--center text-right"><span class="marg-l-30">' + (question.status == 'success' ? '<i class="fas fa-check text-success"></i>' : '<i class="fas fa-times text-danger"></i>') + '</span></div> <div class="col-md-2"></div></div>');
                                    }

                                    totalPointsNeeded += question.points_max;
                                    totalPointsEarned += question.points;
                                });

                                // Calcul score %
                                const score = Math.round((totalPointsEarned / totalPointsNeeded) * 100);
                                // Question header msg
                                if (isSurvey == true) {
                                    $('#quiz-step-resume .header-quiz').addClass('bg-success');
                                    $('#quiz-step-resume .questionHeader h3').removeClass('text-danger').addClass('text-success');
                                    $('#quiz-step-resume .questionHeader h3').html(translations.survey_step_title);
                                } else if (score >= quiz_score) {
                                    $('#quiz-step-resume .header-quiz').addClass('bg-success').removeClass('bg-danger');
                                    
                                    // $('#quiz-step-resume .questionHeader h1').removeClass('text-danger').addClass('text-success');
                                    $('#quiz-step-resume .questionHeader h3').html(quiz_steps[currStep].msg_ok);
                                } else {
                                    // $('#quiz-step-resume .questionHeader h1').addClass('text-danger').removeClass('text-success');
                                    $('#quiz-step-resume .header-quiz').addClass('bg-danger');
                                    $('#quiz-step-resume .questionHeader h3').html(quiz_steps[currStep].msg_ok);
                                }
                                if (checkOpenTemplate == true) {
                                    $('#quiz-step-resume .questionHeader h3').html(translations.in_review_step_title);
                                    $('#quiz-step-resume .header-quiz').addClass('bg-deepmark');
                                    $('#quiz-step-resume .questionHeader h1').html(translations.pending_score + '<p class="font-size-14 text-initial text-white no-margin">' + translations.question_pending + '</p>');
                                    $('#quiz-step-resume .questionHeader h1').addClass('text-white');

                                    // $('#quiz-step-resume .questionHeader h1, #quiz-step-resume .questionHeader h2').hide();
                                } else {
                                    $('#quiz-step-resume .header-quiz').removeClass('bg-deepmark');
                                    $('#quiz-step-resume .questionHeader h1').html(totalPointsEarned + ' / ' + totalPointsNeeded + ' ' + translations.data_point);
                                    $('#quiz-step-resume .questionHeader h1').addClass('text-white');
                                }
                                if(isSurvey == true){
                                    $('#quiz-step-resume .questionHeader h1').html(translations.title_servey)
                                }
                                // Slide resume
                                $('#quiz-step-resume').show('slide', {'direction': 'right'}, 500, function () {
                                    $('#quiz-step-resume .questionHeader h1').hide().show('pulsate', {}, 3000);
                                });

                                // End quiz
                                if (currStep + 1 == quiz_steps.length) {
                                    $('.nextStep').hide();
                                    $('.finishQuiz').show();
                                    quizService.finishQuiz();
                                }


                                // get title nextstep
                                let nextstep = currStep + 1;
                                let title = $('.step-quiz tr:eq(' + nextstep + ')').attr('data-title');
                                if (currStep + 1 >= quiz_steps.length) {

                                    $('#quiz-step-resume .next-title').html();
                                } else {
                                    $('#quiz-step-resume .next-title').html('Étape ' + (nextstep + 1) + '. ' + title);

                                }

                            }

                            // Open - Recording option -> Save file in background & update answer in DB
                            // If the recording-validate-[question_id] == 1 then save the video or video
                            console.log($('#recording-validate-' + question_id).val())
                            if ($('#recording-type-' + question_id).data('type') != undefined && $('#recording-validate-' + question_id).val() == 1) {
                                quizService.open.saveRecord($('#quiz_id').val(), question_id, play_id);
                            } else if ($('#recording-type-' + question_id).data('type') != undefined && $('#recording-validate-' + question_id).val() == 2) {
                                quizService.open.saveScreenRecord($('#quiz_id').val(), question_id, play_id);
                            }
                        }
                    }).fail(function (res) {
                        quizService.showError();
                    });
                }
            } else {
                quizService.showError();
            }
        });
    },
    // Finish Quiz
    finishQuiz() {
        $('.finishQuiz').click(function () {
            $('#quiz-step-resume').hide('slide', {'direction': 'left'}, 500, function () {
                quizService.finished();
            });
        });
    },
    // Finish quiz process
    finished() {
        // Clear interval timer
        window.clearInterval(timerCount);
        // Clear Html (not necessary)
        $('#quiz-finish-resume .questionList').html('');
        // Init 
        const step_title = $('#quiz-finish-resume .questionList').data('step_title');

        const question_title = $('#quiz-finish-resume .questionList').data('question_title');
        const msg_ok = $('#quiz-finish-resume .questionList').data('msg_ok');
        const msg_nok = $('#quiz-finish-resume .questionList').data('msg_nok');
        let totalPointsNeeded = 0;
        let totalPointsEarned = 0;
        let totalSuccess = 0;
        let totalQuiz = 0;

        // Each step in this quiz
        quiz_steps.map((step, currStep) => {
            $('#quiz-finish-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h3>' + step_title + ' ' + (currStep + 1) + '</h3></div><div class="col-md-4"></div><div class="col-md-2"></div></div>');
            // Each question in this quiz
            step.questions.map((question, index) => {

                if (question.status == 'wait') {
                    // If answer wait correction
                    question.points = 0;
                    question.status = 'wait';
                } else if (answers != undefined && answers[currStep] != undefined && answers[currStep][index] != undefined) {
                    // If answer exists
                    question.status = answers[currStep][index].status;
                    question.points = answers[currStep][index].points;
                } else {
                    // If not exists, 0 point and fail
                    question.points = 0;
                    question.status = 'fail';
                }
                // Set view
                if (question.status == 'wait') {
                    $('#quiz-finish-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4>' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 text-right"><span class="marg-l-30 badge badge-deepmark-orangelight">' + translations.in_review_step_question + '</span></div> <div class="col-md-2"></div></div>');
                } else if (question.is_survey == 1) {
                    $('#quiz-finish-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4>' + question_title + ' ' + (index + 1) + '</h4></div><div class="col-md-4 text-right"><span class="marg-l-30 badge badge-success">' + translations.survey_step_question + '</span></div> <div class="col-md-2"></div></div>');
                } else {
                    $('#quiz-finish-resume .questionList').append('<div class="row"><div class="col-md-2"></div><div class="col-md-4 no-padding"><h4>' + question_title + ' ' + (index + 1) + step.questions.length + ' </h4></div><div class="col-md-4 text-right"><span class="marg-l-30 badge badge-' + (question.status == 'success' ? 'success' : 'danger') + '"><strong>' + question.points + '</strong>/' + question.score_to_pass + '</span></div> <div class="col-md-2"></div></div>');

                }
                if (question.status == 'success') {
                    totalSuccess += 1;
                }
                totalQuiz += 1;
                totalPointsNeeded += question.score_to_pass;
                totalPointsEarned += question.points;
            });
        })

        // Calcul score %
        const score = Math.round((totalPointsEarned / totalPointsNeeded) * 100);
        // Question header msg
        if (hasOpenQuestion == true) {
            $('#quiz-finish-resume .questionHeader h1').removeClass('text-danger').removeClass('text-success').addClass('c-orangelight'); //#quiz-finish-resume .questionHeader h3, #quiz-finish-resume .questionHeader h2, 
            
            // $('#quiz-finish-resume .questionHeader h2').html(translations.quiz_complete);
            $('#quiz-finish-resume .questionHeader h3').html(translations.quiz_test);
            // add icon waits result last step
            $('#quiz-finish-resume .questionHeader .result-icon').html('<i class="c-orangelight icon--xl fas fa-clock" aria-hidden="true"></i>');

        } else if (isSurvey == true) {
            $('#quiz-finish-resume .questionHeader h1').removeClass('text-danger').addClass('text-success'); // #quiz-finish-resume .questionHeader h2,
            $('#quiz-finish-resume .questionHeader h3').html(translations.survey_quiz_sub_title);
        } else if (score >= quiz_score) {
            $('#quiz-finish-resume .questionHeader h1').removeClass('text-danger').addClass('text-success'); // #quiz-finish-resume .questionHeader h2, 
            $('#quiz-finish-resume .questionHeader h3').html(msg_ok);
            // add icon success result last step
            $('#quiz-finish-resume .questionHeader .result-icon').html('<i class="c-red icon--xl fa fa-times" aria-hidden="true"></i>');
        } else {
            $('#quiz-finish-resume .questionHeader h1').addClass('text-danger').removeClass('text-success'); // #quiz-finish-resume .questionHeader h2, 
            // $('#quiz-finish-resume .questionHeader h3').html(msg_nok);
            // add icon error result last step
            $('#quiz-finish-resume .questionHeader .result-icon').html('<i class="c-red icon--xl fa fa-times" aria-hidden="true"></i>');
        }

        if (hasOpenQuestion == true) {

            $('#quiz-finish-resume .questionHeader h2:eq(0)').html(translations.quiz_complete);
            $('#quiz-finish-resume .questionHeader h1:eq(0)').hide(); //, #quiz-finish-resume .questionHeader h2:eq(1)
            $('#quiz-finish-resume .open_question').html(translations.open);
            $('#quiz-finish-resume .waiting').html(translations.waiting);

            $('#quiz-finish-resume .status').hide();
        } else {
            $('#quiz-finish-resume .questionHeader h1:eq(0)').html(score + ' %');
            $('#quiz-finish-resume .questionHeader h2:eq(1)').html(totalPointsEarned + 'pts <strong>/ ' + totalPointsNeeded + '</strong>');
            $('#quiz-finish-resume .total').html(totalQuiz);
            $('#quiz-finish-resume .total-success').html(totalSuccess);

        }
        if (isSurvey == true) {
            $('#quiz-finish-resume .status').show();
            $('#quiz-finish-resume .questionHeader h2:eq(0)').html(translations.survey_quiz_title);
            $('#quiz-finish-resume .waiting').html(translations.servey);
            $('#quiz-finish-resume .questionHeader h1:eq(0), #quiz-finish-resume .questionHeader h2:eq(1)').hide();
            $('#quiz-finish-resume .total').html(totalQuiz + " questions") ;

            
            
            // $('#quiz-finish-resume .open_question').hide();
        } 
        


        // Slide resume
        $('#quiz-finish-resume').show('slide', {'direction': 'right'}, 500, function () {
            $('#quiz-finish-resume .questionHeader h1').hide().show('pulsate', {}, 3000);
        });

    },
    // Tabs step
    navSteps() {

        $('.nextStep').click(function () {
            currStep++;

            $('.title-step').html($('.step-quiz tr:eq(' + currStep + ')').attr('data-title'));
            $('.total-question-step').html($('.step-quiz tr:eq(' + currStep + ')').attr('data-total-question'));
            $('.point-question-step').html($('.step-quiz tr:eq(' + currStep + ')').attr('data-point-step'));
            $('.present-step').html(currStep + 1);
            $("#presentTitle").html($('.step-quiz tr:eq(' + currStep + ')').attr('data-title'));
            $('.stepList .nav-tabs li a').removeClass('active');
            $('.stepList .nav-tabs li a:eq(' + currStep + ')').addClass('active');
            $('.stepList .tab-content .tab-pane').removeClass('active');
            // Hide step resume, show next question
            $('#quiz-step-resume').hide('slide', {'direction': 'left'}, 500, function () {
                $('.stepList .tab-content .tab-pane:eq(' + currStep + ')').addClass('show active');
                $('.stepList .tab-content .tab-pane:eq(' + currStep + ') .question-boxe:eq(0)').hide().show('slide', {'direction': 'right'}, 500);
            });
        });
    },
    // Next step check
    checkNextButton() {
        if ($('.step-pos-' + (currStep + 1)).html() == undefined) {
            $('.nextStep').html($('.nextStep').data('finish_text'));
        } else {
            $('.nextStep').html($('.nextStep').data('base_text'));
        }
    },
    // Bind Tabs
    bindTabs() {
        // change to click icon eyes

        $('.quiz-result-list .nav-tabs a').unbind('click');
        $('.quiz-result-list .nav-tabs a').click(function () {
            let id = $(this).attr('data-id');
            let title = $(this).html();
            let quizPlayedId = $(this).attr('data-play-id');
            $("#quizUserPlayedId").val(quizPlayedId);
            $('#step_number').html($(this).attr('data-number'));
            $('#modal-title-popup').html($(this).attr('data-title'));
            step = $(this).attr('aria-controls').replace('step-', '');
            currStepResult = step;
            $('.quiz-result-list .nav-link').removeClass('active').removeClass('modal-active');
            $('.quiz-result-list .tab-pane.active').removeClass('active').removeClass('show').removeClass('modal-active');
            $('.quiz-result-list #step-nav-' + step).addClass('show active modal-active');
            $('.quiz-result-list #step-' + step).addClass('show active modal-active');

            currStepResult = $(this).data('step');
            let content = $('#results-'+ id + ' .tab-content.quiz-content .modal-active').html();
            $('#detail-quiz-modal .modal-body').html(content);
            $("#detail-quiz-modal").modal("show");

            $('.user-answer'+quizPlayedId).each(function(index){
                let height_user_ans = $(this).height();
                let height_correct_ans = $(".correct-answer"+quizPlayedId).eq(index).height();
                if (height_user_ans != 0 && height_correct_ans != 0) {
                    // console.log(height_user_ans +" "+ height_correct_ans);
                    if (height_user_ans > height_correct_ans) {
                        $(".correct-answer"+quizPlayedId).eq(index).height(height_user_ans);
                        $(this).height(height_user_ans);
                    } else {
                        $(".correct-answer"+quizPlayedId).eq(index).height(height_correct_ans);
                        $(this).height(height_correct_ans);
                    }
                } else {
                    $(this).css("height", "auto");
                    $(".correct-answer"+quizPlayedId).eq(index).css("height", "auto");

                }
            });
        });
    },
    // Stop songs
    stopSongsAndVideos(question_id) {
        // Songs
        let audios = document.getElementsByTagName('audio');
        for (var i = 0, len = audios.length; i < len; i++) {
            audios[i].pause();
        }
        // Videos
        let videos = document.getElementsByTagName('video');
        for (var i = 0, len = videos.length; i < len; i++) {
            videos[i].pause();
        }
        // Youtube / Vimeo
        $('.yvideo[data-question_id="' + question_id + '"]').attr('src', '');
    },

    /**
     * ######### RESULTS
     */
    showResults() {
        $('.showResults').click(function () {
            $('#results-' + $(this).data('id')).slideDown();
            $(this).hide();
            $(this).next('a').show();
        });
        $('.hideResults').click(function () {
            $('#results-' + $(this).data('id')).slideUp();
            $(this).hide();
            $(this).prev('a').show();
        });
    }
}