// QCU
quizService.qcu = {
    // Init
    init(question_id) {
    },
    // Get Answer
    getAnswer(question_id) {
        value = $('#question-' + question_id + ' input[type="radio"]:checked').val();

        if (value) {
            return {value: value};
        } else {
            return false;
        }
    }
}