// Scoring
let stars_timer = false;
quizService.scoring = {
    // Init
    init(question_id, slider_step) {
        quizService.scoring.initSlider(question_id, slider_step);
        quizService.scoring.initStars(question_id);
        quizService.scoring.initBubbles(question_id);
    },
    // Init slider
    initStars(question_id) {
        $('#question-' + question_id + ' .quiz-scoring-star').unbind('hover');
        $('#question-' + question_id + ' .quiz-scoring-star').hover(function () {
            if (stars_timer == false) {
                const currPosition = $(this).index();
                $('.quiz-scoring-star', $(this).parent()).each(function (i) {
                    if (i <= currPosition) {
                        $(this).removeClass('far').removeClass('clicked').addClass('fas');
                    }
                });
            }
        }, function () {
            if (stars_timer == false) {
                if ($('.quiz-scoring-star.clicked', $(this).parent()).html() == undefined) {
                    $('.quiz-scoring-star', $(this).parent()).removeClass('fas').addClass('far');
                }
            }
        });


        $('#question-' + question_id + ' .quiz-scoring-star').unbind('click');
        $('#question-' + question_id + ' .quiz-scoring-star').click(function () {
            const currPosition = $(this).index();
            $('.quiz-scoring-star', $(this).parent()).each(function (i) {
                if (i <= currPosition) {
                    $(this).removeClass('far').addClass('fas').addClass('clicked');
                }
            });
            // Set value
            $('.question-' + question_id + '-data').val(currPosition + 1);
            // Timer 1 second before can change again
            stars_timer = true;
            setTimeout(function () {
                stars_timer = false;
            }, 1000);
        });
    },
    // Init bubbles
    initBubbles(question_id) {
        $('.quiz-scoring-bubbles').hover(function () {
            if (!$(this).hasClass('clicked')) {
                $(this).removeClass('far').addClass('fas');
            }
        }, function () {
            $('.quiz-scoring-bubbles', $(this).parent()).each(function () {
                if (!$(this).hasClass('clicked')) {
                    $(this).removeClass('fas').addClass('far');
                }
            });
        });


        $('#question-' + question_id + ' .quiz-scoring-bubbles').click(function () {
            $('#question-' + question_id + ' .quiz-scoring-bubbles').removeClass('fas').addClass('far').removeClass('fa-check-circle').addClass('fa-circle').removeClass('clicked');
            $(this).removeClass('far').addClass('fas').addClass('clicked').removeClass('fa-circle').addClass('fa-check-circle');
            // Set value
            $('#question-' + question_id + ' .question-' + question_id + '-data').val($(this).index() + 1);
        });
    },
    // Init slider
    initSlider(question_id, slider_step) {
        if ($('#slider-' + question_id).html() != undefined) {
            const handle = $("#quiz-scoring-slider-handle-" + question_id);
            $('#slider-' + question_id).slider({
                step: slider_step,
                min: $('#question-' + question_id + ' .question-' + question_id + '-data').data('min'),
                max: $('#question-' + question_id + ' .question-' + question_id + '-data').data('max'),
                create: function () {
                    handle.text($(this).slider("value"));
                },
                slide: function (event, ui) {
                    handle.text(ui.value);
                    $('#question-' + question_id + ' .question-' + question_id + '-data').val(ui.value);
                }
            }); 
        }
    },
    // Get Answer
    getAnswer(question_id) {
        values = {};
        $('.question-' + question_id + '-data').each(function () {
            if ($(this).val() != '') {
                switch ($(this).data('type')) {
                    case 'free':
                        const val = $(this).val();
                        // console.log("getAnswer -> val", val)
                        // console.log("getAnswer -> min", $(this).prop('min'))
                        // console.log("getAnswer -> max", $(this).prop('max'))
                        // console.log("getAnswer -> dec", $(this).data('dec'))
                        if(Number(val) < Number($(this).prop('min')) || Number(val) > Number($(this).prop('max'))) {
                            values = 'wrong format';
                            break;
                        }
                        const decimal = val.split('.')[1];
                        if(!decimal && Number($(this).data('dec')) != 0) {
                            values = 'wrong format';
                            break;
                        }
                        if(decimal && decimal.length != Number($(this).data('dec'))) {
                            values = 'wrong format';
                            break;
                        }
                        values.value = val;
                        break;
                    case 'stars':
                        values.value = $(this).val();
                        break;
                    case 'bubbles':
                        values.value = $(this).val();
                        break;
                    case 'slider':
                        values.value = $(this).val();
                        break;
                }
            } else {
                values = false;
            }
        });
        
        return values;
    }
}