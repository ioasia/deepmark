// Text_to_fill
let textToFill = [];
let textToFillInitialParents = [];
// Text_to_fill
quizService.text_to_fill = {
    // Init
    init(question_id) {
        textToFill[question_id] = [];
        quizService.text_to_fill.bindDraggable(question_id);
    },
    bindDraggable(question_id) {
        $('#question-' + question_id + ' .badge').draggable({
            revert : function(event, ui) {
                $(this).data("uiDraggable").originalPosition = {
                    top : 0,
                    left : 0
                };
                return !event;
            }
        });
        $('#question-' + question_id + ' .marker').droppable({
            drop: function (event, ui) {
                // Current dragged item
                $item = ui.draggable;
                // Get values matched
                const toMatch = $(this).data('pos');
                const match = $item.data('match');
                // Check if element move from place to place
                textToFill[question_id].map((item, i) => {
                    if (item[1] == match) {
                        textToFill[question_id].splice(i, 1);
                    }
                });
                // Check if element replace another element in a place
                textToFill[question_id].map((item, i) => {
                    if (item[0] == toMatch) {
                        // Revert in view
                        item[2].attr('style', '');
                        item[3].append(item[2]);
                        item[2].draggable("destroy");
                        item[2].draggable({revert: "invalid"});
                        // Delete in array
                        textToFill[question_id].splice(i, 1);
                    }
                });

                // Add to array
                if(textToFillInitialParents[$item.attr('id')] == undefined) {
                    // First time, we keep the parent element for next time replacement
                    textToFill[question_id].push([toMatch, match, $item, $item.parent(), $item.parent().attr('class')]);
                    textToFillInitialParents[$item.attr('id')] = $item.parent();
                } else {
                    // other time, we re-add previous defined parent (<li>) to be sure element go back to good place
                    textToFill[question_id].push([toMatch, match, $item, textToFillInitialParents[$item.attr('id')], textToFillInitialParents[$item.attr('id')].attr('class')]);
                }

                // Re-Position in place view of new element
                $(this).append(ui.draggable);
                $(ui.draggable).css({"left": "0", "top": "0", "margin-top": '0'});
            }
        });
    },
    // Get Answer
    getAnswer(question_id) {
        let lengthBadge = $('#question-' + question_id + ' .textToFillOptions .badge').length;
        let markers = $('#question-' + question_id + ' .textToFillContent .marker').length;
        let markerBadges = $('#question-' + question_id + ' .textToFillContent .marker .badge').length;
        // return markers;
        if (textToFill[question_id].length > 0) {
            // if(parse(markers) == 0) {
            //     return {value : ""};
            // }
            values = [];
            textToFill[question_id].map((item) => {
                values.push([item[0], item[1]]);
            });
            // console.log(markers)
            // console.log(markerBadges)
            if(markerBadges < markers){
                return false;
            } 
            return {value: values};
        } else {
            if(parseInt(markers) == 0) {
                return {value : ""};
            } else {
                return false;
            }
            
        }
    }
}  