// Open
let openFree = [];
const openDocumentTypes = [
  ["xls", "excel", ".xls,.xlsx"],
  ["doc", "word", ".doc,.docx"],
  ["ppt", "powerpoint", ".ppt,.pptx"],
  ["pdf", "pdf", ".pdf"],
  ["img", "image", "image/*"],
  ["video", "video", "video/*"],
  ["audio", "audio", "audio/*"],
];
let mediaSources = [];
let myMediaRecorders = [];
let recordedBlobs = [];
let sourceBuffers = [];
let constraints = [];
let audioRecords = [];
let recorded = [];

// record desktop screen
const start = document.getElementById("start");
const stop = document.getElementById("stop");
const replay = document.getElementById("replay");
const video = document.getElementById("screencast");
let preCountingRec,
  screenRecorder,
  screenStream,
  mediaStream,
  composedStream,
  completeBlob = [];
const screenOptions = { video: true, audio: true };
let micNumber = 0;
let reRec = false;

// Meter class that generates a number correlated to audio volume.
// The meter class itself displays nothing, but it makes the
// instantaneous and time-decaying volumes available for inspection.
// It also reports on the fraction of samples that were at or near
// the top of the measurement range.
function SoundMeter(context) {
  this.context = context;
  this.instant = 0.0;
  this.slow = 0.0;
  this.clip = 0.0;
  this.script = context.createScriptProcessor(2048, 1, 1);
  var that = this;
  this.script.onaudioprocess = function (event) {
    var input = event.inputBuffer.getChannelData(0);
    var i;
    var sum = 0.0;
    var clipcount = 0;
    for (i = 0; i < input.length; ++i) {
      sum += input[i] * input[i];
      if (Math.abs(input[i]) > 0.99) {
        clipcount += 1;
      }
    }
    that.instant = Math.sqrt(sum / input.length);
    that.slow = 0.95 * that.slow + 0.05 * that.instant;
    that.clip = clipcount / input.length;
  };
}

SoundMeter.prototype.connectToSource = function (stream, callback) {
  console.log("SoundMeter connecting");
  try {
    this.mic = this.context.createMediaStreamSource(stream);
    this.mic.connect(this.script);
    // necessary to make sample run, but should not be.
    this.script.connect(this.context.destination);
    if (typeof callback !== "undefined") {
      callback(null);
    }
  } catch (e) {
    console.error(e);
    if (typeof callback !== "undefined") {
      callback(e);
    }
  }
};
SoundMeter.prototype.stop = function () {
  this.mic.disconnect();
  this.script.disconnect();
};

const handleRecTimer = (handleType) => {
  const start = new Date.parse("1970-01-01 00:00:00");
  const end = new Date.parse("1970-01-01 00:10:00");
  let diff = Math.round((end.getTime() - start.getTime()) / 1000);
  let timerInt;
  if (handleType == "start") {
    timerInt = setInterval(() => {
      let seconds = Math.floor(diff);
      let minutes = Math.floor(seconds / 60);

      minutes %= 60;
      seconds %= 60;

      $("#timer").text(
        (minutes > 0
          ? (minutes.toString().length == 1 ? "0" + minutes : minutes) + ":"
          : "") + (seconds.toString().length == 1 ? "0" + seconds : seconds)
      );
      diff -= 1;
      if (diff <= -1) {
        clearInterval(timerInt);
        $("#stop").trigger("click");
        $("#timer").hide();
      }
    }, 1000);
  } else {
    clearInterval(timerInt);
    $("#timer").hide();
  }
};

const defineSharingBuffer = () => {
  return navigator.mediaDevices
    .getDisplayMedia(screenOptions)
    .then((stream) => {
      return navigator.mediaDevices
        .enumerateDevices()
        .then((devices) => {
          devices.forEach((device) => {
            if (device.kind == "audioinput") {
              micNumber++;
            }
          });

          if (micNumber == 0) {
            return stream;
          }

          mediaStream = navigator.mediaDevices
            .getUserMedia({ audio: true })
            .then((micStream) => {
              composedStream = new MediaStream();
              stream.getVideoTracks().forEach((videoTrack) => {
                composedStream.addTrack(videoTrack);
              });

              //if system audio has been shared
              if (stream.getAudioTracks().length > 0) {
                //merge the system audio with the mic audio
                const context = new AudioContext();
                let audioDestination = context.createMediaStreamDestination();

                const systemSource = context.createMediaStreamSource(stream);
                const systemGain = context.createGain();
                systemGain.gain.value = 1.0;
                systemSource.connect(systemGain).connect(audioDestination);
                console.log("added system audio");

                if (micStream && micStream.getAudioTracks().length > 0) {
                  const micSource = context.createMediaStreamSource(micStream);
                  const micGain = context.createGain();
                  micGain.gain.value = 1.0;
                  micSource.connect(micGain).connect(audioDestination);
                  console.log("added mic audio");
                }

                audioDestination.stream
                  .getAudioTracks()
                  .forEach(function (audioTrack) {
                    composedStream.addTrack(audioTrack);
                  });
              } else {
                //add just the mic audio
                micStream.getAudioTracks().forEach(function (micTrack) {
                  composedStream.addTrack(micTrack);
                });
              }

              return composedStream;
            })
            .catch((err) => {
              console.log("getUserMedia err -> ", err);
              throw err;
            });
          return mediaStream;
        })
        .catch((err) => {
          console.log("enumerateDevices err -> ", err);
          throw err;
        });
    })
    .catch((err) => {
      console.log("getDisplayMedia err -> ", err);
      throw err;
    });
};

const stopStream = () => {
  screenStream.getTracks().forEach((track) => track.stop());
  composedStream.getTracks().forEach((track) => track.stop());
  $("#start").show();
  $("#replay").show();
  $("#stop").hide();
  handleRecTimer("stop");
};

const startRecording = async () => {
  screenStream = null;
  composedStream = null;

  screenStream = await defineSharingBuffer();
  screenRecorder = new MediaRecorder(screenStream);

  const x = document.getElementById("counting-sound");
  x.play();

  let chunks = [];
  screenRecorder.ondataavailable = (e) => chunks.push(e.data);
  screenRecorder.onstop = (e) => {
    completeBlob = new Blob(chunks, { type: "video/webm" });
    video.src = URL.createObjectURL(completeBlob);
    chunks = [];
  };
  screenStream.getVideoTracks()[0].onended = () => {
    $("#stop").trigger("click");
  };

  $("#start").hide();
  $("#counting").show();

  start.setAttribute("disabled", true);
  stop.removeAttribute("disabled");

  preCountingRec = 3;
  $("#counting").html(
    '<h1 class="no-margin"><span class="circle-time"><b>' +
      preCountingRec +
      "</b></span></h1>"
  );

  const countingInt = setInterval(() => {
    preCountingRec--;
    if (preCountingRec == 1) {
      $("#counting").html(
        '<h1 class="no-margin"><span class="circle-time"><b class="pad-5">' +
          preCountingRec +
          "</b></span></h1>"
      );
    } else {
      $("#counting").html(
        '<h1 class="no-margin"><span class="circle-time"><b>' +
          preCountingRec +
          "</b></span></h1>"
      );
    }
    if (preCountingRec == 0) {
      clearInterval(countingInt);
      handleRecTimer("start");
      $("#counting").text("");
      $("#stop").show();
      $("#counting").hide();
    }
  }, 1000);

  setTimeout(() => {
    screenRecorder.start();
  }, (preCountingRec + 2) * 1000);
};

if (start && stop) {
  start.addEventListener("click", () => {
    startRecording();
  });

  stop.addEventListener("click", () => {
    let question_id = $("#stop").attr("data-question-id");
    $("#recording-validate-" + question_id).val(2);
    stop.setAttribute("disabled", true);
    replay.removeAttribute("disabled");
    start.removeAttribute("disabled");

    if (!reRec) {
      reRec = true;
      $("#start").html(
        `<i class="fa fa-video"></i> ${jsTranslations.restart_recording}`
      );
    }

    screenRecorder.stop();
    stopStream();
  });

  replay.addEventListener("click", () => {
    video.play();
  });
}
// record desktop screen

quizService.open = {
  // Init
  init(question_id) {
    quizService.open.bindEditor(question_id);
    // Init uploads
    quizService.open.initUpload(question_id);
    // Init recording
    if ($("#recording-type-" + question_id).data("type") != undefined) {
      quizService.open.initRecording(question_id);
      $("#stop").hide();
      $("#replay").hide();
      $("#counting").hide();
    }
  },
  // Bind plugins
  bindEditor(question_id) {
    if ($("#editor-" + question_id).html() != undefined) {
      // openFree[question_id] = CKEDITOR.replace('editor-' + question_id);
      openFree[question_id] = new Jodit(`#editor-${question_id}`, {
        toolbarAdaptive: false,
        buttons:
          "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview",
      });
    }
  },
  initUpload(question_id) {
    // Upload documents
    openDocumentTypes.map((type) => {
      if ($(".add-" + type[0]).html() != undefined) {
        $(".add-" + type[0]).unbind("click");
        $(".add-" + type[0]).click(function () {
          question_id = $(this).data("question-id");
          // Dropzone
          uploadService.addUpload(
            "quizUpload" + type[0],
            type[0] + "-" + question_id,
            type[0] + "-" + question_id + "-preview",
            "/learner/quiz/uploadFile",
            "fa fa-file-" + type[1],
            translations.add_template,
            type[2],
            {
              quiz_id: $("#quiz_id").val(),
              type: "quiz_" + type[0],
              question_id: question_id,
            },
            true,
            $($(this).data("target") + " .upload-area").width(),
            $($(this).data("target") + " .upload-area").height(),
            "add-" + type[0],
            true
          );
        });
      }
    });

    // Upload template
    if ($(".addTemplate").html() != undefined) {
      $(".addTemplate").unbind("click");
      $(".addTemplate").click(function () {
        question_id = $(this).data("question-id");
        // Dropzone
        uploadService.addUpload(
          "quizUploadTemplate",
          "template-" + question_id,
          "template-" + question_id + "-preview",
          "/learner/quiz/uploadFile",
          "fa fa-file",
          translations.add_template,
          ".pdf,.pptx,.docx,.xlsx,.txt",
          {
            quiz_id: $("#quiz_id").val(),
            type: "quiz_template",
            question_id: question_id,
          },
          true,
          $($(this).data("target") + " .upload-area").width(),
          $($(this).data("target") + " .upload-area").height(),
          "add-template",
          true
        );
      });
    }
  },
  // Get Answer
  getAnswer(question_id) {
    values = {};
    let validate = false;
    $(".question-" + question_id + "-data").each(function () {
      switch ($(this).data("type")) {
        case "free":
          // console.log(openFree[question_id].value);
          if (
            openFree[question_id].value == "" ||
            openFree[question_id].value == "<p></p>"
          ) {
            values.free = false;
          } else {
            values.free = openFree[question_id].value;
          }
          break;
        case "document":
          values.documents = [];
          values.documentList = false;
          openDocumentTypes.map((type) => {
            //console.log('#' + type[0] + '-' + question_id);
            //console.log($('#' + type[0] + '-' + question_id).val());
            if ($("#" + type[0] + "-" + question_id).val() != undefined) {
              values.documents.push([
                type[0],
                $("#" + type[0] + "-" + question_id).val(),
                $("#" + type[0] + "-" + question_id).data("file_descriptor"),
              ]);
              if ($("#" + type[0] + "-" + question_id).val()) {
                values.documentList = true;
              }
            }
          });
          break;
        case "template":
          values.template = $("#template-" + question_id).val();
          break;
        case "record":
          let val = $("#recording-validate-" + question_id).val();
          if (val == 1 || val == 2) {
            values.record = "record available";
          } else {
            values.record = false;
          }
          break;
      }
    });

    for (var key in values) {
      console.log("key: " + key + "\n" + "value: " + values[key]);
      if (values[key] != "" && key != "documents") {
        validate = true;
      }
    }
    return validate ? values : validate;
  },

  // Recording
  initRecording(question_id) {
    mediaSources[question_id] = new MediaSource();
    mediaSources[question_id].addEventListener(
      "sourceopen",
      quizService.open.handleSourceOpen,
      false
    );
    myMediaRecorders[question_id];
    sourceBuffers[question_id];
    $("#accept-recording-" + question_id).click(async function () {
      if ($("#recording-type-" + question_id).data("type") == "audio") {
        constraints[question_id] = {
          audio: true,
        };
      } else {
        constraints[question_id] = {
          audio: true,
          video: true,
        };
        $("#video-recorded-" + question_id).show();
        $("#play-recording-" + question_id).show();
      }
      await quizService.open.initRecordingProcess(
        constraints[question_id],
        question_id
      );
    });
  },
  async initRecordingProcess(constraints, question_id) {
    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      quizService.open.handleSuccess(stream, question_id);
    } catch (e) {
      console.error("navigator.getUserMedia error:", e);
      $("#error-recording-" + question_id).html(
        `navigator.getUserMedia error:${e.toString()}`
      );
    }
  },
  startRecord(question_id) {
    $("#start-recording-" + question_id).click(function () {
      $("#play-recording-" + question_id).prop("disabled", true);
      if (
        document.querySelector(
          $("#recording-type-" + question_id).data("type") +
            "#video-recorded-" +
            question_id
        ).src != undefined
      ) {
        document
          .querySelector(
            $("#recording-type-" + question_id).data("type") +
              "#video-recorded-" +
              question_id
          )
          .pause();
      }
      $("#stop-recording-" + question_id).show();
      $("#start-recording-" + question_id).hide();
      if ($("#recording-type-" + question_id).data("type") == "audio") {
        $("#video-recording-" + question_id).hide();
        $("#audio-recording-" + question_id).show();
        audioRecords[question_id] = setInterval(
          "quizService.open.startAudioRecord(" + question_id + ")",
          1500
        );
      }
      quizService.open.start_timer(question_id);
      quizService.open.startRecordProcess(question_id);
    });
  },
  startRecordProcess(question_id) {
    recordedBlobs = [];
    let options = { mimeType: "video/webm" };
    if ($("#recording-type-" + question_id).data("type") == "audio") {
      options = { mimeType: "audio/webm" };
    } else {
      options = { mimeType: "video/webm" };
      /*let options = {mimeType: 'video/webm;codecs=vp9'};
             console.log(options);
             if (!MediaRecorder.isTypeSupported(options.mimeType)) {
             console.error(`${options.mimeType} is not Supported`);
             $('#error-recording-' + question_id).html(`${options.mimeType} is not Supported`);
             options = {mimeType: 'video/webm;codecs=vp8'};
             if (!MediaRecorder.isTypeSupported(options.mimeType)) {
             console.error(`${options.mimeType} is not Supported`);
             $('#error-recording-' + question_id).html(`${options.mimeType} is not Supported`);
             options = {mimeType: 'video/webm'};
             if (!MediaRecorder.isTypeSupported(options.mimeType)) {
             console.error(`${options.mimeType} is not Supported`);
             $('#error-recording-' + question_id).html(`${options.mimeType} is not Supported`);
             options = {mimeType: 'video/webm'};
             } else {
             options = {mimeType: 'video/webm'};
             }
             }
             }*/
    }
    console.log(options);

    try {
      myMediaRecorders[question_id] = new MediaRecorder(window.stream, options);
    } catch (e) {
      console.error("Exception while creating MediaRecorder:", e);
      $("#error-recording-" + question_id).html(
        `Exception while creating MediaRecorder: ${JSON.stringify(e)}`
      );
      return;
    }

    console.log(
      "Created MediaRecorder",
      myMediaRecorders[question_id],
      "with options",
      options
    );

    myMediaRecorders[question_id].onstop = (event) => {
      console.log("Recorder stopped: ", event);
      console.log("Recorded Blobs: ", recordedBlobs);
    };
    myMediaRecorders[question_id].ondataavailable =
      quizService.open.handleDataAvailable;
    myMediaRecorders[question_id].start(10); // collect 10ms of data
    console.log("MediaRecorder started", myMediaRecorders[question_id]);
  },
  startAudioRecord(question_id) {
    $("#audio-recording-" + question_id + " i")
      .fadeOut(900)
      .delay(300)
      .fadeIn(800);
  },
  stopRecord(question_id) {
    $("#stop-recording-" + question_id).click(function () {
      $("#recording-validate-" + question_id).val(1);
      $("#stop-recording-" + question_id).hide();
      $("#start-recording-" + question_id)
        .html('<i class="fa fa-play"></i> ' + translations.restart_recording)
        .show();
      $("#play-recording-" + question_id).show();
      $("#play-recording-" + question_id).prop("disabled", false);
      $("#video-recorded-" + question_id).show();
      if ($("#recording-type-" + question_id).data("type") == "audio") {
        clearInterval(audioRecords[question_id]);
        $("#video-recording-" + question_id).hide();
        $("#audio-recording-" + question_id).hide();
      }

      quizService.open.stop_timer(question_id);
      quizService.open.stopRecordProcess(question_id);
    });
  },
  stopRecordProcess(question_id) {
    myMediaRecorders[question_id].stop();
  },
  playRecord(question_id) {
    $("#play-recording-" + question_id).click(function () {
      let superBuffer;
      if ($("#recording-type-" + question_id).data("type") == "audio") {
        superBuffer = new Blob(recordedBlobs, { type: "audio/webm" });
      } else {
        superBuffer = new Blob(recordedBlobs, { type: "video/webm" });
      }
      $("#modal").removeClass("modal-lg");
      $("#modal").addClass("resize-modal");

      $("#divVideo").removeClass("col-12");
      $("#divVideo").addClass("col-6");

      recorded[question_id] = document.querySelector(
        $("#recording-type-" + question_id).data("type") +
          "#video-recorded-" +
          question_id
      );
      recorded[question_id].src = null;
      recorded[question_id].srcObject = null;
      recorded[question_id].src = window.URL.createObjectURL(superBuffer);
      recorded[question_id].controls = true;
      recorded[question_id].play();

      // quizService.open.saveRecord(question_id);
    });
  },
  saveRecord(quiz_id, question_id, play_id) {
    const blob = new Blob(recordedBlobs, { type: "video/webm" });
    var fd = new FormData();
    fd.append("fname", "test.mp4");
    fd.append("file", blob);
    fd.append("quiz_id", quiz_id);
    fd.append("play_id", play_id);
    fd.append("question_id", question_id);
    fd.append("type", "quiz_recording");
    $.ajax({
      type: "POST",
      url: "/learner/quiz/uploadFile",
      data: fd,
      processData: false,
      contentType: false,
    }).done(function (data) {
      console.log(data);
    });
  },
  saveScreenRecord(quiz_id, question_id, play_id) {
    var fd = new FormData();
    fd.append("fname", "test.mp4");
    fd.append("file", completeBlob);
    fd.append("quiz_id", quiz_id);
    fd.append("play_id", play_id);
    fd.append("question_id", question_id);
    fd.append("type", "quiz_recording");
    $.ajax({
      type: "POST",
      url: "/learner/quiz/uploadFile",
      data: fd,
      processData: false,
      contentType: false,
    }).done(function (data) {
      console.log(data);
    });
  },
  handleSourceOpen(event) {
    console.log("MediaSource opened");
    sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
    console.log("Source buffer: ", sourceBuffer);
  },
  handleDataAvailable(event, question_id) {
    // console.log('handleDataAvailable', event);
    if (event.data && event.data.size > 0) {
      recordedBlobs.push(event.data);
    }
  },
  handleSuccess(stream, question_id) {
    console.log("getUserMedia() got stream:", stream);
    window.stream = stream;

    const gumVideo = document.querySelector(
      $("#recording-type-" + question_id).data("type") +
        "#video-recording-" +
        question_id
    );
    gumVideo.srcObject = stream;

    $("#video-recording-" + question_id).show();
    $("#video-recorded" + question_id).show();
    $("#accept-recording-" + question_id).hide();
    $("#start-recording-" + question_id).show();

    quizService.open.startRecord(question_id);
    quizService.open.stopRecord(question_id);
    quizService.open.playRecord(question_id);
  },
  // Timer recording
  start_timer(question_id) {
    $("#video-timer-" + question_id).show();
    const start = new Date.parse("1970-01-01 00:00:00");
    const end = new Date.parse("1970-01-01 00:10:00");
    let diff = Math.round((end.getTime() - start.getTime()) / 1000);
    timerCount = window.setInterval(function () {
      var seconds = Math.floor(diff);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;
      $("#video-timer-" + question_id).text(
        (hours > 0
          ? (hours.toString().length == 1 ? "0" + hours : hours) + "H"
          : "") +
          (minutes > 0
            ? (minutes.toString().length == 1 ? "0" + minutes : minutes) + ":"
            : "") +
          (seconds.toString().length == 1 ? "0" + seconds : seconds)
      );
      diff -= 1;
      if (diff <= -1) {
        $(".stopRecording").trigger("click");
      }
    }, 1000);
  },
  stop_timer(question_id) {
    $("#video-timer-" + question_id).text("10:00");
    $("#video-timer-" + question_id).hide();
    window.clearInterval(timerCount);
  },
};
