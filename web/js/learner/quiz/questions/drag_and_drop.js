// Drag_and_drop
let dragAndDrop = [];
let dragAndDropInitialParents = [];
quizService.drag_and_drop = {
  // Init
  init(question_id) {
    dragAndDrop[question_id] = [];
    quizService.drag_and_drop.bindDraggable(question_id);
  },
  // Bind draggable
  bindDraggable(question_id) {
    $("#question-" + question_id + " .draggable")
      .draggable({
        revert: function (event, ui) {
          $(this).data("uiDraggable").originalPosition = {
            top: 0,
            left: 0,
          };
          return !event;
        },
      })
      .uniqueId();
    $("#question-" + question_id + " .droppable").droppable({
      drop: function (event, ui) {
        // Current dragged item
        $item = ui.draggable;
        // Get values matched
        const toMatch = $(this).parent().data("to_match");
        const match = $item.data("match");
        // Check if element move from place to place
        dragAndDrop[question_id].map((item, i) => {
          if (item[1] == match) {
            dragAndDrop[question_id].splice(i, 1);
          }
        });
        // Check if element replace another element in a place
        dragAndDrop[question_id].map((item, i) => {
          if (item[0] == toMatch) {
            // Revert in view
            item[2].attr("style", "");
            item[3].append(item[2]);
            item[2].draggable("destroy");
            item[2].draggable({ revert: "invalid" });
            // Delete in array
            dragAndDrop[question_id].splice(i, 1);
          }
        });

        // Add to array
        if (dragAndDropInitialParents[$item.attr("id")] == undefined) {
          // First time, we keep the parent element for next time replacement
          dragAndDrop[question_id].push([
            toMatch,
            match,
            $item,
            $item.parent(),
            $item.parent().attr("class"),
          ]);
          dragAndDropInitialParents[$item.attr("id")] = $item.parent();
        } else {
          // other time, we re-add previous defined parent (<li>) to be sure element go back to good place
          dragAndDrop[question_id].push([
            toMatch,
            match,
            $item,
            dragAndDropInitialParents[$item.attr("id")],
            dragAndDropInitialParents[$item.attr("id")].attr("class"),
          ]);
        }

        // Re-Position in place view of new element
        $(this).append(ui.draggable);
        $(ui.draggable).css({
          left: "0",
          top: "0",
          "margin-top": "0",
          border: "none",
        });
      },
    });
  },
  // Get Answer
  getAnswer(question_id) {
    let lengthDragAndDrop = $("#question-" + question_id + " .draggable")
      .length;

    if (dragAndDrop[question_id].length > 0) {
      values = [];
      dragAndDrop[question_id].map((item) => {
        values.push([item[0], item[1]]);
      });
      if (lengthDragAndDrop != dragAndDrop[question_id].length) {
        return false;
      }

      return { value: values };
    } else {
      return false;
    }
  },
};
