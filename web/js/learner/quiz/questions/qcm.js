// QCM
quizService.qcm = {
    // Init
    init(question_id) {
    },
    // Get Answer
    getAnswer(question_id) {
        values = [];
        $('#question-' + question_id + ' input[type="checkbox"]:checked').each(function () {
            values.push($(this).val())
        });

        if (values.length > 1) { 
            return {value: values};
        } else {
            return false;
        }
    }
}