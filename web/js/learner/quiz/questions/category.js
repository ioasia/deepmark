// Category
quizService.category = {
    // Init
    init(question_id) {
        quizService.category.bindSelectpicker(question_id);
    },
    // Bind plugins
    bindSelectpicker(question_id) {
        $('#'+question_id+' .selectpicker').selectpicker();
    },
    // Get Answer
    getAnswer(question_id) {
        values = [];
        valuesToValidate = 0;
        $('#question-' + question_id + ' select').each(function () {
            valuesToValidate++;
            if($(this).val() != 'not-selected') {
                values.push($(this).val());
            } 
        });

        console.log(valuesToValidate);
        console.log(values.length);
        if (values.length > 0 && valuesToValidate == values.length) {
            return {value: values};
        } else {
            return false;
        }
    }
}