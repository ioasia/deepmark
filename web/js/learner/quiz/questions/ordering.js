// Ordering
quizService.ordering = {
    // Init
    init(question_id) {
        quizService.ordering.bindSortable(question_id);
    },
    // Bind plugins
    bindSortable(question_id) {
        $('#question-'+question_id + ' .orderingOptions').sortable();
        $('#question-'+question_id + ' .orderingOptions').disableSelection();
    },
    // Get Answer
    getAnswer(question_id) {
        values = [];
        $('#question-' + question_id + ' .quiz-ordering-value').each(function () {
            values.push($(this).data('value'))
        });

        if (values.length > 0) {
            return {value: values};
        } else {
            return false;
        }
    }
}