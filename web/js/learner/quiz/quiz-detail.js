function detailQuiz(e) {

   	$.ajax({
        'url': '/learner/quiz/detailQuiz',
        'method': 'POST',
        'data': {
            module_id: $('#module_id').val(),
            quiz_id: $('#quiz_id').val(),
            play_id : $(e).attr('data-play-id'),
            title : $(e).attr('data-title'),
        },
        dataType: "html",
    }).done(function (res) {

    	$('#modals-popup').html(res);
    	$("#detail-quiz-modal").modal()

    }).fail(function () {
        
    });
}