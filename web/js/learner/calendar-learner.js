const GREEN = '#28c612';
const CALENDAR = $('#trainerPlanningCalendar');
const HEADER_OPTIONS = {
    left: 'agendaWeek,addAvailability',
    center: 'prev,title,next',
    right: 'removeAvailability,month'
};
function get_calendar_height() {
    return $(window).height() - 300;
}
$(document).ready(function () {
    $(window).resize(function() {
        $('#trainerPlanningCalendar').fullCalendar('option', 'height', get_calendar_height());
    });
    let isAdding = false;
    let isDeleting = false;
    let locate = $('#locate').val();
    let default_lang = $('#default_locale').val();
    let lang = (locate == default_lang) ? '' : '/' + locate;
    $('#trainerPlanningCalendar').fullCalendar({
        locale: locate,
        header: HEADER_OPTIONS,
        selectable: true,
        eventLimit: 3,
        firstDay: 1,
        height: get_calendar_height(),
        contentHeight: 'auto',
        defaultView: 'agendaWeek',
        allDaySlot: false,
        eventOverlap: false,
        slotEventOverlap : false,
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '24:00:00',
        fixedWeekCount: false,
        eventColor: GREEN,
        customButtons: {
            addAvailability: {
                text: 'Add availability',
                click: function () {
                    CALENDAR.removeClass('cursor-clear');
                    isDeleting = false;

                    if (isAdding) {
                        isAdding = false;
                        $(this).removeClass('fc-state-active');
                        CALENDAR.removeClass('cursor-add');
                    } else {
                        isAdding = true;
                        $(this).addClass('fc-state-active');
                        CALENDAR.addClass('cursor-add');
                    }
                    $('.fc-removeAvailability-button').removeClass('fc-state-active');
                }
            },
            removeAvailability: {
                text: 'Remove availability',
                click: function () {
                    CALENDAR.removeClass('cursor-add');
                    isAdding = false;
                    if (isDeleting) {
                        isDeleting = false;
                        $(this).removeClass('fc-state-active');
                        CALENDAR.removeClass('cursor-clear');
                    } else {
                        isDeleting = true;
                        $(this).addClass('fc-state-active');
                        CALENDAR.addClass('cursor-clear');
                    }
                    $('.fc-addAvailability-button').removeClass('fc-state-active');
                }
            }
        },
        select: function (start, end, jsEvent, view) {
            if (isAdding && view.name === 'month' || isAdding && view.name === 'agendaWeek') {
                jQuery('#addAvailabilityModal .modal-body .errors').empty();
                $('#addAvailabilityModal').modal('show');
                $('#addAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    let startTime = $('#startTimeModal').val(),
                        endTime = $('#endTimeModal').val(),
                        startTimeAfternoon = $('#startTimeAfternoonModal').val(),
                        endTimeAfternoon = $('#endTimeAfternoonModal').val();

                    if (!startTime || !endTime || !startTimeAfternoon || !endTimeAfternoon) return;

                    $.ajax({
                        'url': '/learner/planning/add',
                        'method': 'POST',
                        'data': {
                            'beginning': start.format(),
                            'ending': end.format(),
                            'startTime': startTime,
                            'endTime': endTime,
                            'startTimeAfternoon': startTimeAfternoon,
                            'endTimeAfternoon': endTimeAfternoon
                        }
                    }).done(function (response) {
                        $('#addAvailabilityModal').modal('hide');
                        updateCalendar(view.start.format(), view.end.format());
                    }).fail(function (response) {
                        jQuery('#addAvailabilityModal .modal-body .errors').empty().append('<div class="alert alert-danger" role="alert">' + response.responseJSON.message + '</div>');
                    })
                })
            } else if (isDeleting && view.name === 'month' || isDeleting && view.name === 'agendaWeek') {
                $('#removeAvailabilityModal').modal('show');
                $('#removeAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    let startTime = $('#startTimeModal').val() + ':00',
                        endTime = $('#endTimeModal').val() + ':00';
                    $.ajax({
                        'url': '/learner/planning/delete',
                        'method': 'POST',
                        'data': {
                            'beginning': start.format(),
                            'ending': end.format(),
                            'startTime': startTime,
                            'endTime': endTime
                        }
                    }).done(function (response) {
                        console.log(response);
                        $('#removeAvailabilityModal').modal('hide');
                        updateCalendar(view.start.format(), view.end.format());
                        swal("Info", response.status, "info");
                    }).fail(function (response) {
                        console.log(response);
                    });
                })
            } else if (isAdding) {
                $.ajax({
                    'url': '/learner/planning/add',
                    'method': 'POST',
                    'data': {
                        'beginning': start.format(),
                        'ending': end.format(),
                    }
                }).done(function (response) {
                    console.log(start.format(), end.format());
                    updateCalendar(view.start.format(), view.end.format());
                }).fail(function (response) {
                    console.log('err0r')
                })
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            if (isDeleting) {
                $('#removeAvailabilityModal').modal('show');
                $('#removeAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    $.ajax({
                        'url': '/learner/planning/delete',
                        'method': 'POST',
                        'data': {
                            'beginning': calEvent.start.format(),
                            'ending': calEvent.end.format(),
                        }
                    }).done(function (response) {
                        console.log(response);
                        $('#removeAvailabilityModal').modal('hide');
                        $('#trainerPlanningCalendar').fullCalendar('removeEvents', [calEvent.id]);
                    }).fail(function (response) {
                        console.log(response);
                    });
                })
            } else {
                if (calEvent.title) {
                    $('#dashboard-calendar-modal').modal();

                    $('#modal-title').text(calEvent.displayType);
                    $('#modal-intervention').text(calEvent.intervention);
                    $('#modal-entity').text(calEvent.entity);
                    $('#modal-type').text(calEvent.displayType);
                    $('#modal-start-date').text(calEvent.startEndDate);
                    $('#modal-duration').text(calEvent.duration);
                    $('#modal-booked-status').text(calEvent.bookingStatus);
                    $('#modal-trainer-display-name').text(calEvent.trainerName);
                    $('#modal-session-date-new').text(moment(calEvent.start).format('dddd D MMMM'));
                    let title = calEvent.startEndDate.split(" from");
                    $('#timeDuration').text(title[1]);

                    $('#modal-mobile-phone').text(calEvent.trainerMobile);
                    $('#modal-other-email').text(calEvent.trainerEmail);
                    $('#modal-address').text(calEvent.moduleAddress);
                    $("#modal-title-module").text(calEvent.moduleName);
                    if(calEvent.type == 9 ){
                        $(".divForClassroom").show();
                    }
                    else{
                        $(".divForClassroom").hide();
                    }
                    if (!calEvent.available) {
                        $('#modal-book-online-session').text(jsTranslations.module_expired);
                        $('#modal-book-online-session').removeClass('btn-deepmark');
                        $(".booked").hide();
                        $(".expired").show();
                        $(".divStatus").css('border','1px solid grey')
                    } else {
                        $('#modal-book-online-session').text(jsTranslations.session_rejoin);
                        $('#modal-book-online-session').addClass('btn-deepmark');
                        $(".booked").show();
                        $(".expired").hide();
                        $(".divStatus").css('border','1px solid #28c612')
                    }
                    $('#modal-book-online-session').off('click').click(function () {
                        window.location.href = '/learner/courses/' + calEvent.id + '/module';
                    });
                }
            }
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            $(element).find('.fc-scroller').perfectScrollbar();
            updateCalendar(view.start.format(), view.end.format());
        },
        eventRender: function (event, element) {
            if (event.type != "") {
                if (!event.available) {
                    element.find('.fc-content').parent().addClass('event-expired');
                }
            } else {
                element.find('.fc-content').parent().addClass('event-availability');
            }
            // Don't render events on the weekends
            // if ((event.start).weekday() === 5 || (event.start).weekday() === 6) {
            //     return false;
            // }
            // check AM/PM
            let time = event.start.format("HH");
            if (time >= 12 ) {
                element.find('.fc-content').parent().addClass('border-pm');
            } else {
                element.find('.fc-content').parent().addClass('border-am');
            }
        }
    });

    function updateCalendar(start, end) {
        console.log(start, end);
        return new Promise((resolve, reject) => {

            const selectorCalendar = '#trainerPlanningCalendar';

            let url = lang + '/learner/planning/calendars';

            if (start && end) {
                url += '?dateStart=' + start + '&dateEnd=' + end;
            }
            $.ajax({
                'url': url,
                'method': 'GET',
            }).done(function (response) {
                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', response);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(response);
            });
        });
    }


    $('body').on('click', function (e) {
        if (!CALENDAR.is(e.target) && CALENDAR.has(e.target).length === 0) {
            CALENDAR.removeClass('cursor-add cursor-clear');
            $('.fc-addAvailability-button').removeClass('fc-state-active');
            $('.fc-removeAvailability-button').removeClass('fc-state-active');
            isAdding = false;
            isDeleting = false;
        }
    });

    $('[for="availability-day"]').on('click', function () {
        if (!$('#availability-day').is(':checked')) {
            $('#dayPlanning').show();
            $('#rangePlanning').hide();
        }
    });

    $('[for="availability-range"]').on('click', function () {
        if (!$('#availability-range').is(':checked')) {
            $('#dayPlanning').hide();
            $('#rangePlanning').show();
        }
    });

    $('[data-action="addRangeAvailability"]').on('click', function (event) {
        event.preventDefault();
        let startDate = moment($('#startDate').val(), 'DD/MM/YYYY'),
            endDate = moment($('#endDate').val(), 'DD/MM/YYYY');

        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';

            if ($(selector).find('.form-check-input').is(':checked')) {
                let start = $(selector).find('[data-role="startTime"]').val(),
                    end = $(selector).find('[data-role="endTime"]').val(),
                    start_afternoon = $(selector).find('[data-role="startTimeAfternoon"]').val(),
                    end_afternoon = $(selector).find('[data-role="endTimeAfternoon"]').val();

                days.push({
                    dayName: day,
                    start: start,
                    end: end,
                    start_afternoon: start_afternoon,
                    end_afternoon: end_afternoon,
                });
            }
        }
        if ($('#startDate').val() && $('#endDate').val() && days.length > 0) {
            $.ajax({
                'url': '/learner/planning/calendars/custom',
                'method': 'POST',
                'data': {
                    'beginning': startDate.format('YYYY-MM-DD'),
                    'ending': endDate.format('YYYY-MM-DD'),
                    'days': days,
                }
            }).done(function (response) {
                const startMonth = startDate.startOf('month');
                const endMonth = endDate.endOf('month');
                updateCalendar(startMonth.format('YYYY-MM-DD'), endMonth.format('YYYY-MM-DD'))
                    .then(() => $('#recurringPopup').modal('hide'));
                //.then(() => $('[href="#monthPlanning"]').tab('show'));
            }).fail(function (response) {
                swal(
                    "Error", response.responseJSON.message, "error",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            });
        }
    });

    $('[data-action="deleteRangeAvailability"]').on('click', function (event) {
        event.preventDefault();
        let startDate = moment($('#startDate').val(), 'DD/MM/YYYY'),
            endDate = moment($('#endDate').val(), 'DD/MM/YYYY');

        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';

            if ($(selector).find('.form-check-input').is(':checked')) {
                let start = $(selector).find('[data-role="startTime"]').val(),
                    end = $(selector).find('[data-role="endTime"]').val(),
                    start_afternoon = $(selector).find('[data-role="startTimeAfternoon"]').val(),
                    end_afternoon = $(selector).find('[data-role="endTimeAfternoon"]').val();

                days.push({
                    dayName: day,
                    start: start,
                    end: end,
                    start_afternoon: start_afternoon,
                    end_afternoon: end_afternoon,
                });
            }
        }
        if ($('#startDate').val() && $('#endDate').val() && days.length > 0) {
            $.ajax({
                'url': '/learner/planning/delete/custom',
                'method': 'POST',
                'data': {
                    'beginning': startDate.format('YYYY-MM-DD'),
                    'ending': endDate.format('YYYY-MM-DD'),
                    'days': days,
                }
            }).done(function (response) {
                const startMonth = startDate.startOf('month');
                const endMonth = endDate.endOf('month');
                updateCalendar(startMonth.format('YYYY-MM-DD'), endMonth.format('YYYY-MM-DD'))
                    .then(() => $('#recurringPopup').modal('hide'));
                //.then(() => $('[href="#monthPlanning"]').tab('show'));
                swal("Info", response.status, "info");
            }).fail(function (response) {
                swal(
                    "Error", response.responseJSON.message, "error",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            });
        }
    });

    $('[data-action="addWorkingHours"]').on('click', function (event) {
        event.preventDefault();
        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';
            let end = '', start = '';
            if ($(selector).find('.form-check-input').is(':checked')) {
                start = $(selector).find('[data-role="startTime"]').val();
                end = $(selector).find('[data-role="endTime"]').val();

            }
            days.push({
                dayName: day,
                start: start,
                end: end,
            });
        }

        let lunch_start_time = $("#lunch_start_time").val();
        let lunch_end_time = $("#lunch_end_time").val();

        $.ajax({
            'url': '/learner/planning/working/hours',
            'method': 'POST',
            'data': {
                'days': days,'lunchStartTime' : lunch_start_time, 'lunchEndTime' : lunch_end_time,
            }
        }).done(function (response) {
            location.reload();
        }).fail(function (response) {
            console.log('err0r')
        });
    });

    $('[data-action="addSingleAvailability"]').on('click', function (event) {

        if ($('#singleDate').val()) {
            let singleDate = moment($('#singleDate').val(), 'DD/MM/YYYY'),
                endSingleDate = moment($('#singleDate').val(), 'DD/MM/YYYY').add(1, 'days'),
                startTime = $('#startTime').val(),
                endTime = $('#endTime').val();
            $.ajax({
                'url': '/learner/planning/add',
                'method': 'POST',
                'data': {
                    'beginning': singleDate.format(),
                    'ending': endSingleDate.format(),
                    'startTime': startTime,
                    'endTime': endTime
                }
            }).done(function (response) {
                const startMonth = singleDate.startOf('month');
                const endMonth = endSingleDate.endOf('month');
                updateCalendar(startMonth.format('YYYY-MM-DD'), endMonth.format('YYYY-MM-DD'))
                    .then(() => $('[href="#monthPlanning"]').tab('show'));
            }).fail(function (response) {
                console.log('err0r')
            })
        }
    });


    $('#repeatTime').on('click', function (event) {
        event.preventDefault();
        repeatTime();
    });

    $('#add-availability').on('click', function () {
        var singleDate = $('#singleDate').val();
        var startTime = $('#startTime').val();
        var endTime = $('#endTime').val();

        startTime = moment(singleDate + ' ' + startTime, 'DD/MM/YYYY HH:mm');
        endTime = moment(singleDate + ' ' + endTime, 'DD/MM/YYYY HH:mm');
        $.ajax({
            'url': '/learner/planning/add',
            'method': 'POST',
            'data': {
                'beginning': startTime.format(),
                'ending': endTime.format(),
            }
        }).done(function (response) {
            console.log(response);
        }).fail(function (response) {
            console.log(response);
        })
    });
    $(".fc-addAvailability-button").addClass('d-none');
    $(".fc-removeAvailability-button").addClass('d-none');
});

function dateFormat(date) {
    return date.format();
}

function repeatTime() {
    let startTime = $('#startTime-Monday').val();
    let endTime = $('#endTime-Monday').val();

    let startTimeAfternoon = $('#startTimeAfternoon-Monday').val();
    let endTimeAfternoon = $('#endTimeAfternoon-Monday').val();
    $('[id^="startTime-"]').each(function () {
        $(this).val(startTime);
    });

    $('[id^="endTime-"]').each(function () {
        $(this).val(endTime);
    });

    $('[id^="startTimeAfternoon-"]').each(function () {
        $(this).val(startTimeAfternoon);
    });

    $('[id^="endTimeAfternoon-"]').each(function () {
        $(this).val(endTimeAfternoon);
    })
}

function initRemoveModal() {
    $('#removeAvailabilityModal .btn-cancel').on('click', function () {
        $('#removeAvailabilityModal').modal('hide');
    });
}
