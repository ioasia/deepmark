const HEADER_OPTIONS = {
    left: '',
    center: 'prev, title, next',
    right: 'cancelBooking'
};
const CALENDAR = $('#virtualClassCalendar');
$(document).ready(function() {
    $('[data-action="calendar"]').on('click', function() {
        $('[data-target="calendar"]').removeClass('hidden');
        CALENDAR.fullCalendar('option', 'height', 800);
    });
    let language = $('#locate').val();
    let card = $('.card-pane'),
        moduleId =  card.data('id'),
        dateStart = card.data('start'),
        dateEnd =   card.data('end');

    $('#cancel-booking-info').off('click').on('click', function () {
        $('#cancel-confirmation-modal').modal();
    });

    function booking(moduleId, sessionId, learnerId) {
        $.ajax({
            'url': '/learner/courses/booking/session',
            'method': 'POST',
            'data': {
                'module_id': moduleId,
                'session_id': sessionId,
                'learner_id': learnerId,
            }
        }).done(function(response) {
            // updateVclassCalendar(moduleId, dateStart, dateEnd);
            window.location.reload();
        }).fail(function(response) {
            console.log(response);
        });
    }

    function cancelBooking(moduleId, learnerId) {
        $.ajax({
            'url': '/learner/courses/unbooking',
            'method': 'POST',
            'data': {
                'module_id': moduleId,
                'learner_id': learnerId
            }
        }).done(function(response) {
            // updateVclassCalendar(moduleId, dateStart, dateEnd);
            window.location.reload();
        }).fail(function(response) {
            console.log(response);
        });
    }

    $('#cancel-booking').off('click').on('click', function () {
        let learnerElement = document.getElementById('learner-id');
        if (learnerElement) {
            cancelBooking(moduleId, $('#learner-id').val());
        } else {
            cancelBooking(moduleId, null);
        }
    });

    $('#virtualClassCalendar').fullCalendar({
        header: HEADER_OPTIONS,
        eventLimit: 3,
        selectable: true,
        defaultView: 'month',
        defaultDate: $('#next-session-date').text() != "" ? $('#next-session-date').text() : moment(),
        firstDay: 1,
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '21:00:00',
        contentHeight: 'auto',
        locale: language,
        /*validRange: {
            start: dateStart,
            end: dateEnd,
        },*/
        eventColor: '#378006',
        customButtons: {
            cancelBooking: {
                text: $('#cancel-booking-text').html(),
                click: function () {
                    $('#cancel-confirmation-modal').modal();
                }
            }
        },
        select: function (start, end, jsEvent, view) {
            // console.log(start, end);
        },
        eventClick: function (calEvent, jsEvent, view) {


            if (!calEvent.session_id) return;
            if (calEvent.color === COLORS.red || calEvent.color === COLORS.grey) return;
            $('#virtual-calendar-modal').modal();

            const options = `<option value="${calEvent.session_id}">${moment(calEvent.session_date).format('D/MM/YYYY')}</option>`;
            $('#timeStart').text(calEvent.startTime);
            $('#timeEnd').text(calEvent.endTime);
            $('#selectVirtualClass').empty().append(options);
            $("#selectVirtualClass").selectpicker("refresh");
            $('#modal-session-date').text(moment(calEvent.session_date).format('D/MM/YYYY') + ' at ' + moment(calEvent.start).format('HH:mm'));
            $('#modal-session-date-new').text(moment(calEvent.session_date).format('dddd D MMMM'));
            $('#modal-duration').text(calEvent.duration);
            $('#modal-trainer-display-name').text(calEvent.displayName);
            $('#modal-mobile-phone').text(calEvent.mobile);
            $('#modal-other-email').text(calEvent.email);
            $('#modal-address').text(calEvent.address);

            $('#book-session').off('click').click(function () {
                let learnerElement = document.getElementById('learner-id');
                if (learnerElement) {
                    booking(moduleId, $('#selectVirtualClass').val(), $('#learner-id').val());
                } else {
                    booking(moduleId, $('#selectVirtualClass').val(), null);
                }
            });
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            updateVclassCalendar(moduleId, dateStart, dateEnd);
        },
        eventRender: function (event, element) {
            // Don't render events on the weekends
            /*if ( (event.start).weekday() === 5 || (event.start).weekday() === 6) {
                return false;
            }*/
            if (event.color === COLORS.blue) {
                element.find(".fc-title").prepend('<i class="fa fa-calendar-check"></i>');
            }
        }
    });

    $('[data-action="confirmBooking"]').on('click', function() {

    });
});

function updateVclassCalendar(moduleId, dateStart, dateEnd) {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $.ajax({
        'url': lang + '/learner/courses/calendars/sessions/' + moduleId + '?dateStart=' + dateStart + '&dateEnd=' + dateEnd,
        'method': 'GET',
    }).done(function (response) {

        const events = response.map(day => {
            let percent = (day.participantsHasBooked / day.participants) * 100;
            console.log(day.participantsHasBooked, day.participants, percent);
            var title = day.title;
            day.title = title + $('#book-session-text').html();

            if (day.hasBooked) {
                day.title = title;
                day.color = COLORS.blue;
            } else if ( percent <= 70 ) {
                day.color = COLORS.green;
            } else if ( percent > 70 && percent < 100 ) {
                day.color = COLORS.orange;
            } else {
                day.color = COLORS.red;
            }
            if (!day.canBook)
                day.color = COLORS.grey;
            return day;
        });
        $('#virtualClassCalendar').fullCalendar('removeEvents');
        $('#virtualClassCalendar').fullCalendar('addEventSource', events);
        $('#virtualClassCalendar').fullCalendar('rerenderEvents');
    });
}

$('#module_information').find(".show-calendar").click(function (el) {
    $(".calendar-hidden").toggle();
    $(".show-calendar").each(function (key, value) {
        $(value).toggle();
    });
});
