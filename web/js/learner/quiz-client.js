$(document).ready(function () {

  const moduleId = $('#quiz-done-resolver').attr('moduleId');
  const resultsCount = $('#quiz-done-resolver').attr('resultsCount');
  const retry = $('#quiz-done-resolver').attr('retry');
  const isCompleted = $('#quiz-done-resolver').attr('isCompleted');

  if (parseInt(resultsCount)  < parseInt(retry)){
    toggleQuizVisible(true);
  }

  if (parseInt(resultsCount) > 0 && isCompleted) {
      $('#finishQuiz').show();
  } else {
      $('#finishQuiz').hide();
  }

  $('#quiz-submit-button').click(function () {
    const quizAnswersFinal = {};
    const quizId = $(this).attr('quiz-id');
    $('.question-checkbox').each(function(i, obj) {
        const questionId = $(this).attr('question-id');
        const answerId = $(this).attr('answer-id');

        let isChecked = $(this).prop('checked');
        if (isChecked) {
            if ($(this).attr('question-type') === 'single_answer') {
                quizAnswersFinal[questionId + ""] = [answerId];
            } else {
                if (quizAnswersFinal[questionId]){
                    quizAnswersFinal[questionId + ""].push(answerId);
                } else {
                    quizAnswersFinal[questionId + ""] = [answerId];
                }
            }
        }
    });

    submitResults(quizAnswersFinal, quizId, moduleId)
      .then(() => location.reload())
      .catch(err => console.log(err));
  });

    $('.plusBtn').on('click', function () {
        var id = $(this).attr("data-id");
        $("#plusBtn"+id).addClass('d-none');
        $("#minusBtn"+id).removeClass('d-none');
        $("#divDes"+id).removeClass('d-none');

    });
    $('.minusBtn').on('click', function () {
        var id = $(this).attr("data-id");
        $("#plusBtn"+id).removeClass('d-none');
        $("#minusBtn"+id).addClass('d-none');
        $("#divDes"+id).addClass('d-none');

    });
});

function submitResults(quizAnswers, quizId, moduleId) {

  return new Promise((resolve, reject) => {
    $.ajax({
      method: 'POST',
      url: '/learner/quiz/countResults',
      data: JSON.stringify({
              "answer": quizAnswers,
              "quizId" : quizId,
              "moduleId": moduleId,
          }),
      contentType: 'application/json',
      dataType:"json",
      success: function (result) {
        console.log(resolve);
        window.location.href = "/learner/courses/"+moduleId+"/module?complete=1";
        // resolve();
      },
      failure: function () {
        reject();
      }
    });
  });
}

function toggleQuizVisible(visible) {
  $('#quiz-wrapper').css({ display: visible ? 'block' : 'none' });
  $('#quiz-done-message').css({ display: visible ? 'none' : 'grid' });
}
