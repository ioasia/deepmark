$(document).ready(function () {
    bindLinkingLearner();
    bindAssignGroupLearner();
});

function bindLinkingLearner() {

    $('#btnLinkLearner').on('click', function () {
        let nameBtn = $(this).text();
        const intervention = $('#selectIntervention').val();
        const checkInput = $('input[type="checkbox"]');
        const selectedProfiles = [];
        checkInput.each(function () {
            if(this.checked && this.value !== ""){
                selectedProfiles.push(this.value);
            }
        });
        if (!selectedProfiles.length) {
            // one or more checkboxes are
            swal(jsTranslations.warning_title, jsTranslations.select_one_learner, 'warning');
            return;
        }

        $.ajax({
            'url': '/admin/learners/attachment',
            'method': 'POST',
            'data' : {
                'intervention': intervention,
                'selectedProfiles': selectedProfiles
            },
            beforeSend: function() {
                $("#btnLinkLearner").empty().html('<i class="fa fa-spinner fa-spin"></i> ' + nameBtn);
            },
        }).done(function (response) {
            swal(jsTranslations.success_title, jsTranslations.assign_learner_to_course_success, 'success');
            $("#btnLinkLearner").empty().html(nameBtn);

        }).fail(function (response) {
            console.log(response)
        })
    });

    $('#btnLinkSendLearner').on('click', function () {
        let nameBtn = $(this).text();
        const template = $('#selectTemplate')[0].value;
        const checkInput = $('input[type="checkbox"]');
        const selectedProfiles = [];

        checkInput.each(function () {
            if(this.checked && this.value !== ""){
                selectedProfiles.push(this.value);
            }
        });

        if (!selectedProfiles.length) {
            // one or more checkboxes are
            swal(jsTranslations.warning_title, jsTranslations.select_one_learner, 'warning');
            return;
        }

        $.ajax({
            'url': '/admin/learners/sendAgain',
            'method': 'POST',
            'data' : {
                'template': template,
                'selectedProfiles': selectedProfiles
            },
            beforeSend: function() {
                $("#btnLinkSendLearner").empty().html('<i class="fa fa-spinner fa-spin"></i> ' + nameBtn);
            },
        }).done(function (response) {
            swal(jsTranslations.success_title, jsTranslations.send_email_to_learner_success, 'success');
            $("#btnLinkSendLearner").empty().html(nameBtn);
        }).fail(function (response) {
            console.log(response)
        })
    });

    let moduleId = localStorage.getItem('assignLearnerModuleId');
    if (moduleId) {
        $('#selectIntervention').val( moduleId);//$('#selectIntervention').val(moduleId);
        $('#selectTemplate').val(moduleId);
        $("#assignCourse").addClass('btn-border');
        $("#assignCourse").addClass('btn-deepmark');
        $(".divActionCourse").removeClass("d-none");
        $("#interventionActive"+moduleId).addClass('active-attach');
        setTimeout(function () {
            $("#search_intervention").val($("#interventionActive"+moduleId).text().toLowerCase());
            $("#search_intervention").trigger('keyup');
        }, 3000);

        localStorage.removeItem('assignLearnerModuleId');
    }
}

function bindAssignGroupLearner() {

    $('#btnAssignLearner').on('click', function () {
        let nameBtn = $(this).text();
        const group = $('#selectGroup').val();
        const checkInput = $('input[type="checkbox"]');
        const selectedProfiles = [];

        checkInput.each(function () {
            if(this.checked && this.value !== ""){
                selectedProfiles.push(this.value);
            }
        });

        if (!selectedProfiles.length) {
            // one or more checkboxes are
            swal(jsTranslations.warning_title, jsTranslations.select_one_learner, 'warning');
            return;
        }

        $.ajax({
            'url': '/admin/learners/assign/group',
            'method': 'POST',
            'data' : {
                'group': group,
                'selectedProfiles': selectedProfiles
            },
            beforeSend: function() {
                $("#btnAssignLearner").empty().html('<i class="fa fa-spinner fa-spin"></i> ' + nameBtn);
            },
        }).done(function (response) {
            swal(jsTranslations.success_title, jsTranslations.assign_learner_to_group_success, 'success');
            $("#btnAssignLearner").empty().html(nameBtn);
        }).fail(function (response) {
            console.log(response)
        })
    });
}

