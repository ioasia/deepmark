$(document).ready(function() {
    bindSignature();
});

function bindSignature() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''

    // Draw image
    let signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(0, 0, 0)'
    });
    let saveButton = document.getElementById('save');
    let cancelButton = document.getElementById('clear');

    saveButton.addEventListener('click', function (event) {
        console.log(signaturePad.isEmpty())
        if (!signaturePad.isEmpty()) {
            let signature = signaturePad.toDataURL('image/png');
            let bookingId = $('#signature-image').find('input[name="bookId"]').val();
            let signatureType = $('#signature-image').find('input[name="signatureType"]').val();
            let signatureName = $('#signature-image').find('input[name="signatureName"]').val();

            // Send data to server instead...
            $.ajax({
                'url': '/admin/entities/save_signature',
                'method': 'POST',
                'data': {
                    'signature': signature,
                }
            }).done(function (response) {
                $(".organisation-signature").empty().append('<img id="signaturePreview" style="border-radius: 10px;" width="100" height="100" src="'+response.filePath+'" alt="">')
                $('input[name="signatureFile"]').val(response.filePath);

                swal({
                    type: 'info',
                    allowOutsideClick: false,
                    title: jsTranslations.save_signature_title,
                    text: jsTranslations.save_signature_text,
                }).then(
                    function () {
                        $('#signature-image').modal('hide');
                    },
                )
            }).fail(function (response) {
                console.log(response)
            });
        } else {
            swal(
                "Warning", jsTranslations.please_sign_text, "warning",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        }
    });

    cancelButton.addEventListener('click', function (event) {
        signaturePad.clear();
    });

    // Keep booking ID and signatureType at the signature-image modal
    $('#signature-image').on('show.bs.modal', function(e) {
        $("#clear").trigger("click");
    });
}