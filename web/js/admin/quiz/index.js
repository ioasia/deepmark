$(document).ready(function(){
    // Init
    quizIndexService.init();
});
function leadingZerosForHour(input) {
    if(!isNaN(input.value)) {
        if (input.value.length === 1) {
            input.value = '0' + input.value;
            return;
        } else if (input.value.length === 3){
            input.value = parseInt(input.value);
        } else {

        }
    }
}
function leadingZeros(input) {
    if(!isNaN(input.value)) {
        if (input.value.length === 1) {
            input.value = '0' + input.value;
            return;
        } else if (input.value.length === 3){
            input.value = parseInt(input.value);
        } else {

        }
    }
}
let quizIndexService = {
    init() {
        quizIndexService.swal();
        quizIndexService.intervention_checkTimer();
    },
    /**
     * ALL - Swak on dom element
     * Define this to use : 
     * <a href="#" data-toggle="swal" data-url="{url}" data-msg="{msg}" data-type="{type}">LINK</a>
     * Type = Types used by sweet alert : error / success / info / question ...
     */
    swal() {
        $('[data-toggle="swal"]').unbind('click');
        $('[data-toggle="swal"]').click(function(e){
            e.preventDefault();
            const type = $(this).data('type');
            const msg = $(this).data('msg');
            const url = $(this).data('url');
            swal({
                title: msg,
                type: type,
                showConfirmButton: true,
                showCancelButton: true,
                timer: undefined
            }).then((willDelete) => {
                window.location.href = url;
            });
        });
    },
    // INTERVENTION - Check timer 
    intervention_checkTimer() {
        $('.quiz_timer_check').unbind('click');
        $('.quiz_timer_check').click(function(){
            if($(this).val() == 1) {
                $('.quiz_timer_show').slideDown();
            } else {
                $('.quiz_timer_show').slideUp();
            }
        });
    },
    // Init courses
    initCourses(unique_id) {
        $('.showHideQuiz'+unique_id).addClass('d-none');
        $('#quiz_'+unique_id).on("change", function (e) {
            const datas = $('#quiz_'+unique_id+' option[value="'+$(this).val()+'"]').data();
            // Change duration
            $('input[name="module['+unique_id+'][durationHours]"]').val(0);
            $('input[name="module['+unique_id+'][correctionDurationHours]"]').val(0);
            $('input[name="module['+unique_id+'][correctionDurationMaxHours]"]').val(0);
            $('input[name="module['+unique_id+'][durationMinutes]"]').val(datas.duration);
            $('input[name="module['+unique_id+'][correctionDurationMinutes]"]').val(datas.correction_duration);
            $('input[name="module['+unique_id+'][correctionDurationMaxMinutes]"]').val(datas.correction_duration);
            $('input[name="module['+unique_id+'][durationActive]"]').filter('[value="'+datas.timer_type+'"]').click();
            // Change skills
            let skills;
            if(typeof datas.skills == 'string' && datas.skills.indexOf(',') != -1) {
                skills = datas.skills.split(',');
            } else {
                skills = [datas.skills];
            }
            $('#skillSelect_'+unique_id+'').val(skills).trigger("change");

            if (datas.quiz_type == 1) {
                $('#skillSelect_'+unique_id).select2().removeAttr('required');
                $('.showHideQuiz'+unique_id).addClass('d-none');
            } else {
                $('#quiz_'+unique_id+'_type').trigger("change");
                $('.showHideQuiz'+unique_id).removeClass('d-none');
                if (datas.open_question == 0) {
                    $('#skillSelect_'+unique_id).select2().removeAttr('required');
                } else {
                    $('#skillSelect_'+unique_id).select2().attr('required', true);
                }
            }
            leadingZeros(document.getElementById('hourDuration['+unique_id+']'));
            leadingZeros(document.getElementById('minDuration['+unique_id+']'));
            leadingZerosForHour(document.getElementById('hourDuration['+unique_id+']'));
            leadingZerosForHour(document.getElementById('minDuration['+unique_id+']'));
            $(".iterations"+unique_id).removeClass('d-none');
        });

        $('#quiz_'+unique_id+'_type').on("change", function (e) {
            let type = $(this).val();
            if (type == 1) {
                $('input[name="module['+unique_id+'][durationActive]"]').filter('[value="1"]').click();
            } else {
                $('input[name="module['+unique_id+'][durationActive]"]').filter('[value="0"]').click();
            }
        });
        $('#quiz_'+unique_id).trigger("change");
    }
}