$(document).ready(function () {
    // Quiz init
    quizService.updateType();
    quizService.addStep();
    quizService.addQuestion();
    quizService.saveQuiz();

    $('#quiz_skills').select2();

    // Quiz add first step
    if (edit == false) {
        $('.addStep').trigger('click');
    }

    // Modals init
    quizService.initCSVUpload();
    quizService.initImageUpload();
    quizService.initSelectLearMore();
    quizService.initDocUpload();
    quizService.initSongUpload();
    quizService.initVideoUpload();
    quizService.initImageGoodUpload();
    quizService.initDocGoodUpload();
    quizService.initSongGoodUpload();
    quizService.initVideoGoodUpload();
    quizService.initInProgress();
    quizService.bindSortable();

    // Imports
    quizImportService.initTable();
    quizImportService.addHistoryProcess();
    quizImportService.filterHistory();
});
// Init
let isSurvey = false;
let stepCount = 0;
let currStep = 0;
let questionCount = [];
let currQuestion = 0;
let editors = [];
let saveAll = false;
// Init Imports
let historyChoosed = [];
let typeHistory = 'all';

let quizService = {
    // Get is survey
    getIsSurvey() {
        return isSurvey;
    },
    // Set is survey
    setIsSurvey(type) {
        isSurvey = type;
    },
    // Add Step process
    addStepProcess(datas = null) {
        // Step count
        stepCount++;

        // Clone
        let menu_html = quizService.selectTemplateById('menuStep');
        let container_html = quizService.selectTemplateById('containerStep');

        // Edit ids
        menu_html = menu_html.html().replace(/step-x/gi, 'step-' + stepCount);
        container_html = container_html.html().replace(/step-x/gi, 'step-' + stepCount);

        // Add to view
        $('.stepList ul:eq(0)').append(menu_html);
        $('.stepList .tab-content').append(container_html);

        // Select step
        quizService.selectLastStep();

        // Question count
        questionCount[currStep] = 0;

        // Set params
        if (datas) {
            $('#step-' + stepCount + '-id').val(datas.id);
            $('#step-' + stepCount + '-title').val(datas.title);
            $('#step-' + stepCount + '-msg_ok').val(datas.params.msg_ok);
            $('#step-' + stepCount + '-msg_nok').val(datas.params.msg_nok);
            $('#step-' + stepCount + '-questions_to_show').val(datas.params.questions_to_show);
            $('#step-' + stepCount + '-ordering option[value="' + datas.ordering + '"]').attr('selected', 'selected');
            $('.selectpicker').selectpicker('refresh');
        }

        // End actions
        $('.stepBoxe').show();
        quizService.addQuestion();

        quizService.initInProgress();
        quizService.deleteStep(currStep);
        quizService.bindSortable();

    },
    // Add Steps to quiz
    addStep() {
        $('.addStep').click(function (e) {
            e.preventDefault();
            quizService.addStepProcess();
        });
    },
    // Delete step
    deleteStep(currStep) {
        // Show button
        if (currStep > 1) {
            $('#step-' + stepCount + ' .deleteStep').show();
        }
        // Delete step 
        $('.deleteStep').unbind('click');
        $('.deleteStep').click(function () {
            swal({
                title: translations.confirmation_delete_step,
                type: 'warning',
                showConfirmButton: true,
                showCancelButton: true,
                timer: undefined
            }).then((willDelete) => {
                if ($(this).data('stepid') != 'step-1') {
                    const step_id = $(this).data('stepid');
                    // TODO - Delete Ajax
                    if ($('#' + step_id + '-id').val() != 'tmp') {
                        $.ajax({
                            'url': '/admin/quiz/api/update-status/step/-1/' + $('#' + step_id + '-id').val() + '/true',
                            'method': 'GET',
                        }).fail(function (res) {
                            console.log('Update step status failed !');
                        });
                    }

                    // Remove in view
                    $('#' + step_id).remove();
                    $('#' + step_id + '-tab').parent().remove();
                    quizService.selectLastStep();
                    quizService.bindTabs();
                    // Check Points Weight
                    quizService.checkPointsWeight();
                }
            });

        });
    },
    // Add question Process
    addQuestionProcess(type, datas = null) {
        // Question count
        questionCount[currStep]++;

        // Clone container
        let question = quizService.selectTemplateById('containerQuestion');

        // Active specific section per question
        const active_section = eval('quizService.' + type + '.active_section()');
        if (active_section) {
            active_section.map(i => {
                $('[data-active_section="' + i + '"]', question).removeClass('hide-element');
            });
        }

        // Replace in template
        question = question.html()
                .replace(/step-N-q-y/gi, 'step-' + currStep + '-q-' + questionCount[currStep])
                .replace(/step-N/gi, 'step-' + currStep)
                .replace(/question-y/g, questionCount[currStep])
                .replace(/step-x/g, currStep);

        // Add container to view
        $('#step-' + currStep + ' .questionList').append(question);

        // Set type
        $('#step-' + currStep + '-q-' + questionCount[currStep] + '-type').val(type);

        // Clone & Add question template
        question_content = quizService.selectTemplateById('question-' + type);
        question_content = question_content.html()
                .replace(/question-z/g, 'step-' + currStep + '-q-' + questionCount[currStep])
                .replace(/step-x/gi, currStep)
                .replace(/question-y/g, questionCount[currStep]);
        $('#step-' + currStep + '-q-' + questionCount[currStep] + ' .questionContent').html(question_content);

        // Init question specific JS
        eval('quizService.' + type + '.init("' + currStep + '", "' + questionCount[currStep] + '")');

        // CKEDITOR Question
        // editors['step-' + currStep + '-q-' + questionCount[currStep]] = CKEDITOR.replace('step-' + currStep + '-q-' + questionCount[currStep] + '-question');
        // editors['step-' + currStep + '-q-' + questionCount[currStep]+ '-question-good'] = CKEDITOR.replace('step-' + currStep + '-q-' + questionCount[currStep] + '-question-good');

        // Jodit Question
        editors['step-' + currStep + '-q-' + questionCount[currStep]] = new Jodit(`#step-${currStep}-q-${questionCount[currStep]}-question`, {
            toolbarAdaptive: false,
            buttons: "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview"
        });
        editors['step-' + currStep + '-q-' + questionCount[currStep]+ '-question-good'] = new Jodit(`#step-${currStep}-q-${questionCount[currStep]}-question-good`, {
            toolbarAdaptive: false,
            uploader: {
                insertImageAsBase64URI: true,
                url: '/api/upload-editor-file',
                pathVariableName: "path",
                filesVariableName: function(e){return "file"},
                isSuccess: function (resp) {
                    return resp;
                },
                process: function (resp) {
                    let directory = resp.directory.split("../web");
                    return {
                        files: resp,
                        path: `${directory[1]}/${resp.path}`,
                        baseurl: `${directory[1]}/${resp.path}`,
                        error: resp.uploaded ? "success" : "failed",
                        message: resp.uploaded ? "success" : "failed"
                    }
                },
                defaultHandlerSuccess: function (data) {
                    console.log("data", data)
                    // var i, field = 'files';                      
                    // if (data[field] && data[field].length) {
                    //     for (i = 0; i < data[field].length; i += 1) {
                    //         this.selection.insertImage(data.baseurl + data[field][i]);
                    //     }
                    // }
                    this.selection.insertImage(`${data.baseurl}`);
                },
            },
            filebrowser: {
                ajax: {
                    url: '/api/editor-files'
                }
            }
        });


        // CKEDITOR Trainer's default correction
        if (active_section.indexOf('default_correction') != -1) {
            // editors['step-' + currStep + '-q-' + questionCount[currStep] + '-default_correction'] = CKEDITOR.replace('step-' + currStep + '-q-' + questionCount[currStep] + '-default_correction');
            editors['step-' + currStep + '-q-' + questionCount[currStep] + '-default_correction'] = new Jodit('#step-' + currStep + '-q-' + questionCount[currStep] + '-default_correction', {
                toolbarAdaptive: false,
                buttons: "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview"
            });
        }

        // PUT datas for this question
        if (datas) {
            //editors['step-' + currStep + '-q-' + questionCount[currStep]].setData(datas.question);
            editors['step-' + currStep + '-q-' + questionCount[currStep]].value = datas.question;
            if (datas.history_id != undefined) {
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-history_id').val(datas.history_id);
            }
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-id').val(datas.id);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-doc').val(datas.params.doc);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-song').val(datas.params.song);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-video').val(datas.params.video);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-points').val(datas.score_to_pass);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-msg_ok').val(datas.params.msg_ok);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-msg_nok').val(datas.params.msg_nok);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-answer_type').val(datas.params.answer_type);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-is_learn_more').val(datas.is_learn_more);
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-image-good-download').val(datas.params.image_good_download);
            if (datas.params.image_good_download == 1) {
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-image-good-download').prop('checked', true);
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-image-good-download-icon').removeClass('text-gray');
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-image-good-download-icon').addClass('color-header');
            }
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-doc-good-download').val(datas.params.doc_good_download);
            if (datas.params.doc_good_download == 1) {
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-doc-good-download').prop('checked', true);
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-doc-good-download-icon').removeClass('text-gray');
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-doc-good-download-icon').addClass('color-header');
            }
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-song-good-download').val(datas.params.song_good_download);
            if (datas.params.song_good_download == 1) {
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-song-good-download').prop('checked', true);
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-song-good-download-icon').removeClass('text-gray');
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-song-good-download-icon').addClass('color-header');
            }
            $('#step-' + currStep + '-q-' + questionCount[currStep] + '-video-good-download').val(datas.params.video_good_download);
            if (datas.params.video_good_download == 1) {
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-video-good-download').prop('checked', true);
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-video-good-download-icon').removeClass('text-gray');
                $('#step-' + currStep + '-q-' + questionCount[currStep] + '-video-good-download-icon').addClass('color-header');
            }
            editors['step-' + currStep + '-q-' + questionCount[currStep]+ '-question-good' ].value = datas.params.question_good;

            // Files
            const imageId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-image';
            $('#' + imageId).val(datas.params.image);
            const docId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-doc';
            $('#' + docId).val(datas.params.doc);
            const songId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-song';
            $('#' + songId).val(datas.params.song);
            const videoId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-video';
            $('#' + videoId).val(datas.params.video);
            if (datas.params.image) {
                $('#' + imageId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + imageId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/picture/' + datas.params.image + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.doc) {
                $('#' + docId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + docId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/doc/' + datas.params.doc + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.song) {
                $('#' + songId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + songId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/song/' + datas.params.song + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.video) {
                $('#' + videoId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + videoId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/video/' + datas.params.video + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }

            // Files good
            const imageGoodId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-image-good';
            $('#' + imageGoodId).val(datas.params.image_good);
            const docGoodId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-doc-good';
            $('#' + docGoodId).val(datas.params.doc_good);
            const songGoodId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-song-good';
            $('#' + songGoodId).val(datas.params.song_good);
            const videoGoodId = 'step-' + currStep + '-q-' + questionCount[currStep] + '-video-good';
            $('#' + videoGoodId).val(datas.params.video_good);
            if (datas.params.image_good) {
                $('#' + imageId + '-preview-good').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + imageId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/picture/' + datas.params.image_good + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.doc_good) {
                $('#' + docId + '-preview-good').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + docId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/doc/' + datas.params.doc_good + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.song_good) {
                $('#' + songId + '-preview-good').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + songId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/song/' + datas.params.song_good + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }
            if (datas.params.video_good) {
                $('#' + videoId + '-preview-good').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + videoId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + datas.id + '/video/' + datas.params.video_good + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
            }

            uploadService.removeFiles();
            uploadService.noPreview();

            // Set items in question
            eval("quizService." + type + ".setItems('" + currStep + "', '" + questionCount[currStep] + "', " + JSON.stringify(datas.items) + ")");

            // Footer step
            quizService.setQuestionToShow('step-' + currStep, datas.step.params.questions_to_show);
            quizService.checkPointsWeight();

            // Is survey
            if (datas.is_survey == "1") {
                $('.typeQuestion[data-question_id="step-' + currStep + '-q-' + questionCount[currStep] + '"][data-type="survey"]').trigger('click');
            }
        } else {
            // Update step footer
            quizService.setQuestionToShow('step-' + currStep, $('#step-' + currStep + '-questions_to_show').val());
        }

        // Bind plugins

        quizService.bindSelectpicker();
        quizService.initImageGoodUpload();
        quizService.initDocGoodUpload();
        quizService.initSongGoodUpload();
        quizService.initVideoGoodUpload();
        quizService.initImageUpload();
        quizService.initSelectLearMore();
        quizService.initDocUpload();
        quizService.initSongUpload();
        quizService.initVideoUpload();

        quizService.initBadgesActionable();
        if (active_section.indexOf('default_correction') != -1) {
            quizService.initImageCorrectionUpload();
            quizService.initDocCorrectionUpload();
            quizService.initSongCorrectionUpload();
            quizService.initVideoCorrectionUpload();
        }
        if (active_section.indexOf('personalized_correction') != -1) {
            quizService.initTemplateCorrectionUpload();
        }
        if (!datas) {
            quizService.bindCollapse(currStep, questionCount[currStep]);
        }
        // Hide First Question in all cases
        $('#step-' + currStep + '-accordion .firstQuestion').hide();
        // Save questions
        quizService.saveQuestions();

        // Bind Plugins
        quizService.initInProgress();
        quizService.deleteQuestion();

        // Check type
        quizService.updateTypeProcess();
    },
    // Add a question to quiz
    addQuestion() {
        $('[data-action="addQuestion"]').unbind('click');
        $('[data-action="addQuestion"]').click(function (e) {
            e.preventDefault();
            // Type
            const type = $(this).data('type');
            quizService.addQuestionProcess(type);
        });
    },
    // Delete question
    deleteQuestion() {
        $('.deleteQuestion').unbind('click');
        $('.deleteQuestion').click(function () {
            swal({
                title: translations.confirmation_delete_question,
                type: 'warning',
                showConfirmButton: true,
                showCancelButton: true,
                timer: undefined
            }).then((willDelete) => {
                // Question_id
                const question_id = $(this).data('question_id');

                // Update in DB
                if ($('#' + question_id + '-id').val() != 'tmp') {
                    $.ajax({
                        'url': '/admin/quiz/api/update-status/question/-1/' + $('#' + question_id + '-id').val() + '/true',
                        'method': 'GET',
                    }).fail(function (res) {
                        console.log('Update status position failed !');
                    });
                }

                // Remove
                $('#' + question_id).parent().remove();
                // Show first question add button if 0 question in step (=1 because hidden .firstQuestion)
                if ($('#step-' + currStep + ' .questionList .card').length == 1) {
                    $('#step-' + currStep + '-accordion .firstQuestion').show();
                }

                // Reset question and update count
                questionCount[currStep]--;
                for (i = 1; i <= questionCount[currStep]; i++) {
                    $('#step-' + currStep + ' .questionList .card:eq(' + i + ') .questionPos').text(i);
                }

                // Update all positions
                quizService.setPositions();
                // Check Points Weight
                quizService.checkPointsWeight();
            });
        });
    },
    // Delete items in questions
    deleteItems() {
        $('.quizOptionDelete').unbind('click');
        $('.quizOptionDelete').click(function () {
            const to_delete = $(this).data('to_delete');
            $('#' + to_delete).remove();
        });
    },
    // Save question process
    saveQuestionProcess(step_id, question_id, confirm = true) {
        
        return new Promise(function (resolve, reject) {
            // Type
            const type = $('#step-' + step_id + '-q-' + question_id + '-type').val();
            const step_name = $('#step-' + step_id + '-title').val();
            const question_name = $('#step-' + step_id + '-q-' + question_id + ' .questionName').html().replace('<span class="questionPos">', '').replace('</span>', '');
            const saveQuestionBtn = $('#step-' + step_id + '-q-' + question_id + ' .saveQuestion');
            saveQuestionBtn.html('<i class="fas fa-spinner fa-pulse"></i>');
            const is_survey = quizService.getIsSurvey() == true ? 1 : 0;
            const isLearnMore = is_survey == 1 ? null : $('#step-' + step_id + '-q-' + question_id + '-is_learn_more').val();
            // Get Global Params
            let params = {
                quiz_tmp_id: $('#quiz_tmp_id').val(),
                quiz_id: $('#quiz_id').val(),
                step_id: $('#step-' + step_id + '-id').val(),
                question_id: $('#step-' + step_id + '-q-' + question_id + '-id').val(),
                quiz: {
                    title: $('#quiz_title').val(),
                    duration: $('#quiz_duration').val(),
                    skills_or_and: $('#quiz_skills_or_and').val(),
                    correction_duration: $('#quiz_correction_duration').val(),
                    skills: $('#quiz_skills').val(),
                    is_survey: is_survey,
                },
                step: {
                    title: $('#step-' + step_id + '-title').val(),
                    ordering: $('#step-' + step_id + '-ordering').val(),
                    params: {
                        msg_ok: $('#step-' + step_id + '-msg_ok').val(),
                        msg_nok: $('#step-' + step_id + '-msg_nok').val(),
                        questions_to_show: $('#step-' + step_id + '-questions_to_show').val(),
                        position: $('#step-' + step_id + '-pos').val()
                    }
                },
                question: {
                    history_id: $('#step-' + step_id + '-q-' + question_id + '-history_id').val(),
                    // question: editors['step-' + step_id + '-q-' + question_id].getData(),
                    question: editors['step-' + step_id + '-q-' + question_id].value,
                    type: type,
                    score_to_pass: $('#step-' + step_id + '-q-' + question_id + '-points').val(),
                    is_survey: is_survey,
                    is_learn_more: isLearnMore,
                    params: {
                        image: $('#step-' + step_id + '-q-' + question_id + '-image').val(),
                        doc: $('#step-' + step_id + '-q-' + question_id + '-doc').val(),
                        song: $('#step-' + step_id + '-q-' + question_id + '-song').val(),
                        video: $('#step-' + step_id + '-q-' + question_id + '-video').val(),
                        msg_ok: $('#step-' + step_id + '-q-' + question_id + '-msg_ok').val(),
                        msg_nok: $('#step-' + step_id + '-q-' + question_id + '-msg_nok').val(),
                        answer_type: $('#step-' + step_id + '-q-' + question_id + '-answer_type').val(),
                        position: $('#step-' + step_id + '-q-' + question_id + '-pos').val(),
                        image_good: $('#step-' + step_id + '-q-' + question_id + '-image-good').val(),
                        doc_good: $('#step-' + step_id + '-q-' + question_id + '-doc-good').val(),
                        song_good: $('#step-' + step_id + '-q-' + question_id + '-song-good').val(),
                        video_good: $('#step-' + step_id + '-q-' + question_id + '-video-good').val(),
                        image_good_download: $('#step-' + step_id + '-q-' + question_id + '-image-good-download').is(':checked') ? 1 : 0,
                        doc_good_download: $('#step-' + step_id + '-q-' + question_id + '-doc-good-download').is(':checked') ? 1 : 0,
                        song_good_download: $('#step-' + step_id + '-q-' + question_id + '-song-good-download').is(':checked') ? 1 : 0,
                        video_good_download: $('#step-' + step_id + '-q-' + question_id + '-video-good-download').is(':checked') ? 1 : 0,
                        question_good: editors['step-' + step_id + '-q-' + question_id + '-question-good'].value
                    }
                }
            };
            // Validation
            params.question.items = eval('quizService.' + type + '.getItems("' + step_id + '", "' + question_id + '")');

            let validation = eval('quizService.' + type + '.validate("' + step_name + '", "' + question_name + '", ' + JSON.stringify(params.question.items) + ', "' + step_id + '", "' + question_id + '",+ isSurvey)');

            if (validation.status == 'error') {
                swal({
                    title: translations.error,
                    html: validation.error,
                    type: "error",
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                saveQuestionBtn.html(saveQuestionBtn.data('base_text'));
                resolve('fail');
            } else {
                // Save 
                $.ajax({
                    'url': '/admin/quiz/api/save-question',
                    'method': 'POST',
                    'data': params
                }).done(function (res) {
                    // Ok
                    $('#quiz_id').val(res.quiz_id);
                    $('#step-' + step_id + '-id').val(res.step_id);
                    $('#step-' + step_id + '-q-' + question_id + '-id').val(res.question_id);
                    $('#step-' + step_id + '-q-' + question_id + '-history_id').val('');

                    saveQuestionBtn.html(saveQuestionBtn.data('base_text'));


                    // Set position for step & questions
                    quizService.setPositions().then(function () {
                        if (confirm === true) {
                            swal({
                                title: translations.saved,
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                closeOnConfirm: false,
                                closeOnCancel: false,
                                timer: 1500
                            });
                        }
                        resolve('done');
                    });

                    // Collapse
                    quizService.bindCollapseOnSave(step_id, question_id);

                    // Check Points Weight
                    quizService.checkPointsWeight();

                    // Set preview files
                    if (params.question.params.image != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-image-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/picture/' + params.question.params.image);

                    }
                    if (params.question.params.doc != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-doc-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/doc/' + params.question.params.doc);

                    }
                    if (params.question.params.song != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-song-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/song/' + params.question.params.song);

                    }
                    if (params.question.params.video != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-video-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/video/' + params.question.params.video);

                    }

                    // set for find more
                    if (params.question.params.image_good != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-image-preview-good .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/picture/' + params.question.params.image_good);

                    }
                    if (params.question.params.doc_good != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-doc-preview-good .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/doc/' + params.question.params.doc_good);

                    }
                    if (params.question.params.song_good != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-song-preview-good .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/song/' + params.question.params.song_good);

                    }
                    if (params.question.params.video_good != '') {
                        $('#step-' + step_id + '-q-' + question_id + '-video-preview-good .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/video/' + params.question.params.video_good);

                    }

                    // Set files in items (open question)
                    if (params.question.items) {
                        params.question.items.map(item => {
                            if (item.type == 'open' || item.type == 'default_correction' || item.type == 'personalized_correction') {
                                switch (item.name) {
                                    // Open template & Personalized_correction template
                                    case 'template' :
                                        $('#step-' + step_id + '-q-' + question_id + '-template' + (item.type == 'personalized_correction' ? '-correction' : '') + '-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/doc/' + item.specifics.file);
                                        break;
                                        // Default correction
                                    case 'default_correction' :
                                        if (item.specifics && item.specifics.image != '') {
                                            $('#step-' + step_id + '-q-' + question_id + '-image-correction-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/picture/' + item.specifics.image);
                                        }
                                        if (item.specifics && item.specifics.doc != '') {
                                            $('#step-' + step_id + '-q-' + question_id + '-doc-correction-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/doc/' + item.specifics.doc);
                                        }
                                        if (item.specifics && item.specifics.song != '') {
                                            $('#step-' + step_id + '-q-' + question_id + '-song-correction-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/song/' + item.specifics.song);
                                        }
                                        if (item.specifics && item.specifics.video != '') {
                                            $('#step-' + step_id + '-q-' + question_id + '-video-correction-preview .noPreview').removeClass('noPreview').addClass('previewLoaded').attr('target', '_blank').attr('href', '/files/quiz/' + res.question_id + '/video/' + item.specifics.video);
                                        }
                                        break;
                                }

                            }
                        });
                    }

                    // Set dragAndDrop
                    if (params.question.type == 'drag_and_drop' && params.question.history_id != '') {
                        params.question.items.map((item, key) => {
                            $('#step-' + step_id + '-q-' + question_id + ' .dragAndDropOptions .match:eq(' + key + ') .upload-area:eq(0)').html('<div class="uploadShow"><img data-dz-thumbnail="" alt="" src="/files/quiz/' + res.question_id + '/drag-and-drop/' + item.value + '"></div>');
                            if (item.name == 'image_to_image') {
                                $('#step-' + step_id + '-q-' + question_id + ' .dragAndDropOptions .match:eq(' + key + ') .upload-area:eq(1)').html('<div class="uploadShow"><img data-dz-thumbnail="" alt="" src="/files/quiz/' + res.question_id + '/drag-and-drop/' + item.specifics.match + '"></div>');
                            }
                        });
                        $('#step-' + step_id + '-q-' + question_id + ' .saveFirst').hide();
                    }

                    uploadService.noPreview();

                }).fail(function (res) {
                    if (confirm === true) {
                        // Error
                        swal({
                            title: translations.error,
                            type: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        });
                    }

                    saveQuestionBtn.html(saveQuestionBtn.data('base_text'));
                });
            }
        });
    },
    // Save Questions
    saveQuestions() {
        $('.saveQuestion').unbind('click');
        $('.saveQuestion').click(function (e) {
            e.preventDefault();

            // Step Id, Question Id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Save
            quizService.saveQuestionProcess(step_id, question_id, true);
        });
    },
    // Save all quiz
    saveQuiz() {
        $('.saveQuiz').click(function (e) {
            e.preventDefault();
            if ($('#quiz_id').val().indexOf('tmp') === -1) {
                saveAll = true;
                const url = $(this).data('url');
                quizService.loading();
                $('.saveQuiz').hide();
                questionToSave = $('.saveQuestion').length;
                questionSaved = 0;
                setTimeout(function () {
                    // Save each question
                    $('.saveQuestion').each(function () {
                        // Step Id, Question Id
                        const step_id = $(this).data('step-id');
                        const question_id = $(this).data('question-id');
                        // Save
                        quizService.saveQuestionProcess(step_id, question_id, false).then(function (res) {
                            if (res == 'done') {
                                questionSaved++;
                                if (questionSaved == questionToSave) {
                                    if ($('#quiz_id').val().indexOf('tmp') == -1) {
                                        $.ajax({
                                            'url': '/admin/quiz/api/update-status/quiz/1/' + $('#quiz_id').val() + '/true',
                                            'method': 'GET',
                                        }).done(function (res) {
                                            window.location.href = url;
                                            $('.saveQuiz').hide();
                                        }).fail(function (res) {
                                            $('.saveQuiz').hide();
                                            swal(translations.error, "", "error", {});
                                        });
                                    }
                                }
                            } else {
                                // If fail
                                $('.saveQuiz').show();
                            }
                        });
                    });
                }, 500);

            } else {
                swal(translations.save_question_first_title, translations.save_question_first_description, "info", {});
            }
        });

    },
    // Set element positions
    setPositions() {
        return new Promise(function (resolve, reject) {
            if (saveAll == false) {
                quizService.loading();
            }
            let steps = [];
            let questions = [];
            // Step list
            $('.stepList .tab-pane').each(function (i) {
                // Set in view
                $('.posStep', $(this)).val(i);
                // Save for DB update
                const step_id = $('#' + $(this).attr('id') + '-id').val();
                if (step_id != 'tmp') {
                    steps.push({id: step_id, position: i});
                }
                // Question list
                $('.posQuestion', $(this)).each(function (j) {
                    // Set in view
                    $(this).val(j);
                    $('.questionPos', $(this).parent()).text(j + 1);
                    // Save for DB update
                    const question_id = $('.question-id', $(this).parent()).val();
                    if (question_id != 'tmp') {
                        questions.push({id: question_id, position: j});
                    }
                });
            });


            // Update in DB
            if (steps && questions) {
                $.ajax({
                    'url': '/admin/quiz/api/update-positions',
                    'method': 'POST',
                    'data': {quiz_id: $('#quiz_id').val(), steps: steps, questions: questions}
                }).done(function () {
                    resolve('done');
                    if (saveAll == false) {
                        quizService.stopLoading();
                    }
                }).fail(function (res) {
                    console.log('Update Step position failed !');
                    if (saveAll == false) {
                        quizService.stopLoading();
                    }
                });
            }
        });
    },
    // Set question to show
    setQuestionToShow(step_id, value = false) {
        let questionsToShow = 0;
        $('#' + step_id + ' .posQuestion').each(function (i) {
            questionsToShow++;
        });

        let options = '<option value="0">' + $('#' + step_id + '-questions_to_show').data('first_opt') + '</option>';
        for (i = 1; i <= questionsToShow; i++) {
            options += '<option value="' + i + '" ' + (value !== false && value == i ? 'selected' : '') + '>' + i + '</option>';
        }
        $('#' + step_id + '-questions_to_show').html(options);
        $('#' + step_id + '-questions_to_show').selectpicker('refresh');

        $('#' + step_id + '-questions_to_show.selectpicker').on('change', function () {
            var selected = $('#' + step_id + '-questions_to_show.selectpicker option:selected').val();
            if (selected != 0) {
                // Ask him if change points or not
                swal({
                    title: translations.confirm_partial_step_title,
                    text: translations.confirm_partial_step_description,
                    type: 'warning',
                    showConfirmButton: true,
                    showCancelButton: true,
                    timer: undefined
                }).then((e) => {
                    $('.question-points').val(1);
                    quizService.checkPointsWeight();
                }).catch(error => {
                    $('#' + step_id + '-questions_to_show').val(0);
                    $('#' + step_id + '-questions_to_show').selectpicker('refresh');
                });
            }
        });
    },
    // Calcul points weight
    checkPointsWeight() {
        totalPoints = 0;
        $('.resumeStepPoints').show();
        // Calcul total points
        let steps = [];
        $('.question-points').each(function (i) {
            // Total points
            totalPoints += parseInt($(this).val());
            // Step points
            step_id = parseInt($(this).data('step-id').toString().replace('step-', ''));
            if (steps[step_id] == undefined) {
                steps[step_id] = parseInt($(this).val());
            } else {
                steps[step_id] += parseInt($(this).val());
            }
        });

        // Set informations in footer
        $('.resumePoints').html('');
        $('.question-points').each(function (i) {
            // Step Id / Question Id
            const step_id = $(this).data('step-id');
            let question_id = $(this).data('question-id');
            question_id = 'step-' + step_id + '-q-' + question_id;

            // Calcul pourcent
            const pourcent = Math.round((parseInt($(this).val()) / totalPoints) * 100);

            // Add to view
            $('#step-' + step_id + ' .resumePoints').append('<h4 class="marg-r-30">' + $('#' + question_id + '-titleJS').val() + ' ' + $('#' + question_id + ' .questionPos').html() + ' <span class="badge badge-deepmark-orangelight"><strong>' + pourcent + '%</strong> <small>' + $(this).val() + ' /' + totalPoints + '</small></span> </h4>');
        });
        // Set informations for steps
        if (totalPoints > 0) {
            $('.resumeStep').each(function () {
                step_id = parseInt($(this).data('step-id').toString().replace('step-', ''));
                if (steps[step_id] != undefined) {
                    $('#' + $(this).data('step-id') + ' .resumeStep').show();
                    pourcentStep = Math.round((parseInt(steps[step_id]) / totalPoints) * 100);
                    $('#' + $(this).data('step-id') + ' .resumeStep').html('<h3><span class="badge badge-deepmark-lightblue"><strong>' + pourcentStep + '%</strong> <small class="c-white">' + steps[step_id] + ' /' + totalPoints + '</small></span></h3>');
                }
            });
        } else {
            $('.resumeStep').each(function () {
                $('#' + $(this).data('step-id') + ' .resumeStep').hide();
            });
        }
    },

    // --- COMMON
    // Update question type
    updateType() {
        $('.typeQuestion').unbind('click');
        $('.typeQuestion').click(function () {
            let errors = false;
            if ($(this).data('type') == 'survey') {
                $('.question-type').each(function () {
                    if ($(this).val() == 'text_to_fill') {
                        swal({
                            title: translations.change_type_error_title,
                            text: translations.change_type_error_description,
                            type: 'warning',
                            timer: undefined,
                            allowEscapeKey: true,
                            allowOutsideClick: false,
                            showConfirmButton: true
                        });
                        errors = true;
                    }
                });
                $("#learn-more-div").hide();
            } else {
                $("#learn-more-div").show();
            }

            if (errors == false) {
                $('.typeQuestion').removeClass('btn-info').addClass('btn-outline-info');
                $(this).removeClass('btn-outline-info').addClass('btn-info');

                switch ($(this).data('type')) {
                    case 'question':
                        quizService.setIsSurvey(false);
                        break;
                    case 'survey' :
                        quizService.setIsSurvey(true);
                        break;
                }

                quizService.updateTypeProcess();
            }
        });
    },
    updateTypeProcess() {
        if (quizService.getIsSurvey() == true) {
            $('.answer_needed').hide();
            $('.answer_not_needed').show();
            $('.qcmOptions input[type="checkbox"]').attr('disabled', 'disabled');
            $('.qcmOptions input[type="checkbox"]').removeAttr('checked');
            $('.qcuContainer input[type="radio"]').attr('disabled', 'disabled');
            $('.qcuContainer input[type="radio"]').removeAttr('checked');
        } else {
            $('.answer_needed').show();
            $('.answer_not_needed').hide();
            $('.qcmOptions input[type="checkbox"]').removeAttr('disabled');
            $('.qcmOptions input[type="checkbox"]:first').attr('checked', 'checked');
            $('.qcuContainer input[type="radio"]').removeAttr('disabled');
            $('.qcuContainer input[type="radio"]:first').attr('checked', 'checked');
        }
    },
    // Loading
    loading() {
        swal({
            title: translations.loading_title,
            text: translations.loading_description,
            type: 'warning',
            timer: undefined,
            allowEscapeKey: false,
            allowOutsideClick: false,
            showConfirmButton: false
        });
    },
    stopLoading() {
        swal.close();
    },
    // Select step
    selectLastStep() {
        let maxStep = $('.stepList .tab-content .tab-pane').length - 1;
        currStep = $('.stepList .tab-content .tab-pane:eq(' + maxStep + ')').attr('id').replace('step-', '');
        $('.stepList .active, .stepList .active').removeClass('active').removeClass('show');
        $('.stepList li a:eq(' + maxStep + ')').addClass('active').attr('aria-selected', true);
        $('.stepList .tab-pane:eq(' + maxStep + ')').addClass('show active');
        quizService.bindSelectpicker();
        quizService.bindTabs();
    },
    // Select and clone template
    selectTemplateById(id) {
        const node = $('template').prop('content');
        return $(node).find(`[template-id='${id}']`).clone(true);
    },
    // Generate random ID
    generateId() {
        this.length = 8;
        this.timestamp = +new Date;

        var _getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };

        this.generate = function () {
            var ts = this.timestamp.toString();
            var parts = ts.split("").reverse();
            var id = "";

            for (var i = 0; i < this.length; ++i) {
                var index = _getRandomInt(0, parts.length - 1);
                id += parts[index];
            }

            return id;
        };

        return  this.generate();
    },

    // --- INIT PLUGINS
    // Tabs step
    bindTabs() {
        $('.stepList li a').unbind('click');
        $('.stepList li a').click(function () {
            step = $(this).attr('aria-controls').replace('step-', '');
            currStep = step;
            $('.stepList .tab-pane.active').removeClass('active').removeClass('show');
            $('.stepList #' + $(this).attr('aria-controls')).addClass('show active');
        });
    },
    // Select Picker
    bindSelectpicker() {
        $('.selectpicker').selectpicker();
    },
    // Collapse questions
    bindCollapse(step_id, question_id) {
        $('#' + step_id + ' .collapse').collapse();
        $('#step-' + step_id + '-q-' + question_id).collapse('toggle');
    },
    // Collapse questions on save
    bindCollapseOnSave(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id).removeClass('show').addClass('hide');
        $('#step-' + step_id + ' .firstQuestion').show();
        $('html,body').animate({
            scrollTop: $(".firstQuestion").offset().top
        }, 'slow');
    },
    // Bind Sortable
    bindSortable() {
        $(".sortable").sortable({
            cancel: ".collapse",
            revert: true,
            start: function (event, ui) {
                var id_textarea = ui.item.find(".collapse").attr("id");
                if(id_textarea != undefined) {
                    var id_textarea_textToFill = id_textarea.replace('step-', '').replace('-q-', '-');
                    editors[id_textarea].destruct();
                    if (textToFill[id_textarea_textToFill] != undefined) {
                        textToFill[id_textarea_textToFill].destruct();
                    }
                }
            },
            stop: function (event, ui) {
                var id_textarea = ui.item.find(".collapse").attr("id");
                if(id_textarea != undefined) {
                    var id_textarea_textToFill = id_textarea.replace('step-', '').replace('-q-', '-');
                    // editors[id_textarea] = CKEDITOR.replace(id_textarea + '-question');
                    editors[id_textarea] = new Jodit('#'+ id_textarea + '-question', {
                        toolbarAdaptive: false,
                        buttons: "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview"
                    });
                    if (textToFill[id_textarea_textToFill] != undefined) {
                        // textToFill[id_textarea_textToFill] = CKEDITOR.replace(id_textarea + '-text_to_fill');
                        textToFill[id_textarea_textToFill] = new Jodit('#'+ id_textarea + '-text_to_fill', {
                            toolbarAdaptive: false,
                            buttons: "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview"
                        });
                    }
                    quizService.setPositions();
                }
            }
        });
    },
    // Image upload for modal
    initSelectLearMore() {
        $('.islearnMore').unbind('change');
        $('.islearnMore').on('change', function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            let isLearnMore = $(this).val();
            if (isLearnMore == 1) {
                $('#step-' + step_id + '-q-' + question_id + '-learn_more_body').show();
            } else {
                $('#step-' + step_id + '-q-' + question_id + '-learn_more_body').hide();
            }
            $(this).selectpicker('refresh');
        });
        $('.islearnMore').trigger('change');

        $('.is-download').unbind('click');
        $('.is-download').on('click', function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');
            const type = $(this).data('type');

            if ($(this).is(':checked')) {
                $('#step-' + step_id + '-q-' + question_id + '-' + type + '-good-download-icon').removeClass('text-gray');
                $('#step-' + step_id + '-q-' + question_id + '-' + type + '-good-download-icon').addClass('color-header');
            } else {
                $('#step-' + step_id + '-q-' + question_id + '-' + type + '-good-download-icon').addClass('text-gray');
                $('#step-' + step_id + '-q-' + question_id + '-' + type + '-good-download-icon').removeClass('color-header');
            }
        });
    },
    initImageUpload() {
        $('.addImage').unbind('click');
        $('.addImage').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');
            // Dropzone
            uploadService.addUpload('quizUploadImage', 'step-' + step_id + '-q-' + question_id + '-image', 'step-' + step_id + '-q-' + question_id + '-image-preview', '/admin/quiz/api/upload', 'fa fa-image', translations.add_image, 'image/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_image',
                step_id: step_id,
                question_id: question_id
            },
                    false, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-image', true);

        });
    },

    // Image good upload for modal
    initImageGoodUpload() {
        $('.addImageGood').unbind('click');
        $('.addImageGood').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadImage', 'step-' + step_id + '-q-' + question_id + '-image-good', 'step-' + step_id + '-q-' + question_id + '-image-preview-good', '/admin/quiz/api/upload', 'fa fa-image', translations.add_image, 'image/*', {
                    quiz_id: $('#quiz_id').val(),
                    type: 'quiz_image',
                    step_id: step_id,
                    question_id: question_id
                },
                false, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-image', true);

        });
    },
    // Doc upload for modal
    initDocUpload() {
        $('.addDoc').unbind('click');
        $('.addDoc').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadDoc', 'step-' + step_id + '-q-' + question_id + '-doc', 'step-' + step_id + '-q-' + question_id + '-doc-preview', '/admin/quiz/api/upload', 'fa fa-file', translations.add_doc, '.pdf,.pptx,.docx,.xlsx,.txt', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_doc',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-doc', true);

        });
    },
    // Doc good upload for modal
    initDocGoodUpload() {
        $('.addDocGood').unbind('click');
        $('.addDocGood').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadDoc', 'step-' + step_id + '-q-' + question_id + '-doc-good', 'step-' + step_id + '-q-' + question_id + '-doc-preview-good', '/admin/quiz/api/upload', 'fa fa-file', translations.add_doc, '.pdf,.pptx,.docx,.xlsx,.txt', {
                    quiz_id: $('#quiz_id').val(),
                    type: 'quiz_doc',
                    step_id: step_id,
                    question_id: question_id
                },
                true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-doc-good', true);

        });
    },
    // Song upload for modal
    initSongUpload() {
        $('.addSong').unbind('click');
        $('.addSong').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadSong', 'step-' + step_id + '-q-' + question_id + '-song', 'step-' + step_id + '-q-' + question_id + '-song-preview', '/admin/quiz/api/upload', 'fa fa-music', translations.add_song, 'audio/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_song',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-song', true);

        });
    },
    // Song good upload for modal
    initSongGoodUpload() {
        $('.addSongGood').unbind('click');
        $('.addSongGood').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadSong', 'step-' + step_id + '-q-' + question_id + '-song-good', 'step-' + step_id + '-q-' + question_id + '-song-preview-good', '/admin/quiz/api/upload', 'fa fa-music', translations.add_song, 'audio/*', {
                    quiz_id: $('#quiz_id').val(),
                    type: 'quiz_song',
                    step_id: step_id,
                    question_id: question_id
                },
                true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-song-good', true);

        });
    },
    // Video upload for modal
    initVideoUpload() {
        $('.addVideo').unbind('click');
        $('.addVideo').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadVideo', 'step-' + step_id + '-q-' + question_id + '-video', 'step-' + step_id + '-q-' + question_id + '-video-preview', '/admin/quiz/api/upload', 'fa fa-video', translations.add_video, 'video/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_video',
                step_id: $(this).data('step-id'),
                question_id: $(this).data('question-id')
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-video', true);

            // Youtube Vimeo
            $('.quiz_video_type_2 input').val('');
            $('.quiz_video_type_2 input').on('keyup', function () {
                $('#video-link-error').hide();
                $('#video-link-viewer').hide();
                let video = $('.quiz_video_type_2 input').val();

                if (video.indexOf('youtube') != -1) {
                    video = video.replace('/watch?v=', '/embed?');
                    $('#video-link-viewer').attr('src', video).show();
                } else if (video.indexOf('vimeo') != -1) {
                    video = 'https://player.vimeo.com/video/' + video.replace('https://vimeo.com/', '');
                    $('#video-link-viewer').attr('src', video).show();
                } else {
                    $('#video-link-error').show();
                }
            });
            $('.quiz_video_type_2_confirm').unbind('click');
            $('.quiz_video_type_2_confirm').click(function () {
                const video = $('.quiz_video_type_2 input').val();
                $('#video-link-viewer').hide();
                $('#video-link-error').hide();
                if (video.indexOf('youtube') != -1 || video.indexOf('vimeo') != -1) {
                    $('#step-' + step_id + '-q-' + question_id + '-video').val(video);
                    $('#step-' + step_id + '-q-' + question_id + '-video-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="step-' + step_id + '-q-' + question_id + '-video"></i> <span class="text-info">' + translations.video_link_sended + '</span>');
                    uploadService.removeFiles();
                }
            });
        });
    },
    // Video good upload for modal
    initVideoGoodUpload() {
        $('.addVideoGood').unbind('click');
        $('.addVideoGood').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadVideo', 'step-' + step_id + '-q-' + question_id + '-video-good', 'step-' + step_id + '-q-' + question_id + '-video-preview-good', '/admin/quiz/api/upload', 'fa fa-video', translations.add_video, 'video/*', {
                    quiz_id: $('#quiz_id').val(),
                    type: 'quiz_video',
                    step_id: $(this).data('step-id'),
                    question_id: $(this).data('question-id')
                },
                true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-video-good', true);

            // Youtube Vimeo
            $('.quiz_video_type_2 input').val('');
            $('.quiz_video_type_2 input').on('keyup', function () {
                $('#video-link-error').hide();
                $('#video-link-viewer').hide();
                let video = $('.quiz_video_type_2 input').val();

                if (video.indexOf('youtube') != -1) {
                    video = video.replace('/watch?v=', '/embed?');
                    $('#video-link-viewer').attr('src', video).show();
                } else if (video.indexOf('vimeo') != -1) {
                    video = 'https://player.vimeo.com/video/' + video.replace('https://vimeo.com/', '');
                    $('#video-link-viewer').attr('src', video).show();
                } else {
                    $('#video-link-error').show();
                }
            });
            $('.quiz_video_type_2_confirm').unbind('click');
            $('.quiz_video_type_2_confirm').click(function () {
                const video = $('.quiz_video_type_2 input').val();
                $('#video-link-viewer').hide();
                $('#video-link-error').hide();
                if (video.indexOf('youtube') != -1 || video.indexOf('vimeo') != -1) {
                    $('#step-' + step_id + '-q-' + question_id + '-video-good').val(video);
                    $('#step-' + step_id + '-q-' + question_id + '-video-preview-good').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="step-' + step_id + '-q-' + question_id + '-video-good"></i> <span class="text-info">' + translations.video_link_sended + '</span>');
                    uploadService.removeFiles();
                }
            });
        });
    },
    // CSV upload for modal
    initCSVUpload() {
        $('.importCSV').unbind('click');
        $('.importCSV').click(function () {
            // Dropzone
            uploadService.addUpload('quizUploadCSV', false, 'import_csv', '/admin/quiz/api/upload', 'fa fa-file', 'Add your CSV (Max 8mb)', '.csv', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_csv'
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-video', true);
        });
    },
    // Image upload for modal
    initImageCorrectionUpload() {
        $('.addImageCorrection').unbind('click');
        $('.addImageCorrection').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadImage', 'step-' + step_id + '-q-' + question_id + '-image-correction', 'step-' + step_id + '-q-' + question_id + '-image-correction-preview', '/admin/quiz/api/upload', 'fa fa-image', translations.add_image, 'image/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_image',
                step_id: step_id,
                question_id: question_id
            },
                    false, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-image', true);

        });
    },
    // Doc upload for modal
    initDocCorrectionUpload() {
        $('.addDocCorrection').unbind('click');
        $('.addDocCorrection').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadDoc', 'step-' + step_id + '-q-' + question_id + '-doc-correction', 'step-' + step_id + '-q-' + question_id + '-doc-correction-preview', '/admin/quiz/api/upload', 'fa fa-file', translations.add_doc, '.pdf,.pptx,.docx,.xlsx,.txt', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_doc',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-doc', true);

        });
    },
    // Song upload for modal
    initSongCorrectionUpload() {
        $('.addSongCorrection').unbind('click');
        $('.addSongCorrection').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadSong', 'step-' + step_id + '-q-' + question_id + '-song-correction', 'step-' + step_id + '-q-' + question_id + '-song-correction-preview', '/admin/quiz/api/upload', 'fa fa-music', translations.add_song, 'audio/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_song',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-song', true);

        });
    },
    // Video upload for modal
    initVideoCorrectionUpload() {
        $('.addVideoCorrection').unbind('click');
        $('.addVideoCorrection').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadVideo', 'step-' + step_id + '-q-' + question_id + '-video-correction', 'step-' + step_id + '-q-' + question_id + '-video-correction-preview', '/admin/quiz/api/upload', 'fa fa-video', translations.add_video, 'video/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_video',
                step_id: $(this).data('step-id'),
                question_id: $(this).data('question-id')
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-video', true);

            // Youtube Vimeo
            $('.quiz_video_type_2 input').val('');
            $('.quiz_video_type_2_confirm').unbind('click');
            $('.quiz_video_type_2_confirm').click(function () {
                const video = $('.quiz_video_type_2 input').val();
                $('#step-' + step_id + '-q-' + question_id + '-video').val(video);
            });
        });
    },
    // Template upload
    initTemplateUpload() {
        $('.addTemplate').unbind('click');
        $('.addTemplate').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadDoc', 'step-' + step_id + '-q-' + question_id + '-template', 'step-' + step_id + '-q-' + question_id + '-template-preview', '/admin/quiz/api/upload', 'fa fa-file', translations.add_doc, '.pdf,.pptx,.docx,.xlsx,.txt', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_doc',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-doc', true);

        });
    },
    // Template correction upload
    initTemplateCorrectionUpload() {
        $('.addTemplateCorrection').unbind('click');
        $('.addTemplateCorrection').click(function () {
            // Step_id / Question_id
            const step_id = $(this).data('step-id');
            const question_id = $(this).data('question-id');

            // Dropzone
            uploadService.addUpload('quizUploadDoc', 'step-' + step_id + '-q-' + question_id + '-template-correction', 'step-' + step_id + '-q-' + question_id + '-template-correction-preview', '/admin/quiz/api/upload', 'fa fa-file', translations.add_doc, '.pdf,.pptx,.docx,.xlsx,.txt', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_doc',
                step_id: step_id,
                question_id: question_id
            },
                    true, ($($(this).data('target') + ' .upload-area').width()), ($($(this).data('target') + ' .upload-area').height()), 'add-doc', true);

        });
    },
    // In progress
    initInProgress() {
        $('.inProgress').unbind('click');
        $('.inProgress').click(function () {
            swal(translations.error, "", "error", {});
        });
    },
    // Badge actionable
    initBadgesActionable() {
        $('.badge-actionable').unbind('click');
        $('.badge-actionable').click(function () {
            // Get datas to check action to do
            const actionable = $(this).data('actionable');
            const type = $(this).data('actionable_type');
            const name = $(this).data('actionable_name');
            const value = $(this).data('actionable_value');
            // Switch action (add input or toggle div)
            switch (type) {
                case 'input-multi':
                    let add = true;
                    // Change active
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        add = false;
                    } else {
                        $(this).addClass('active');
                        add = true;
                    }
                    // Datas
                    let input_multi = $('input[name="' + actionable + '"]', $(this).parent());
                    if (input_multi.val()) {
                        let inputVal = JSON.parse(input_multi.val());
                        if (add == true) {
                            inputVal.push(name);
                        } else {
                            inputVal.splice(inputVal.indexOf(name), 1);
                        }
                        input_multi.val(JSON.stringify(inputVal));
                    } else {
                        $(this).parent().append('<input type="hidden" name="' + actionable + '" class="' + actionable + '" value=\'' + JSON.stringify([name]) + '\' />');
                    }
                    break;
                case 'input' :
                    // Change active
                    $('.badge-actionable[data-actionable="' + actionable + '"]').removeClass('active');
                    $(this).addClass('active');

                    // Datas
                    let input = $('input[name="' + actionable + '"]', $(this).parent());
                    if (input.val()) {
                        input.val(JSON.stringify([name]));
                    } else {
                        $(this).parent().append('<input type="hidden" name="' + actionable + '" class="' + actionable + '" value=\'' + JSON.stringify([name]) + '\' />');
                    }
                    break;
                case 'div' :
                    const target = $(this).data('actionable_target');
                    // Change active
                    $('.badge-actionable[data-actionable="' + actionable + '"]').removeClass('active');
                    $(this).addClass('active');
                    // Show div
                    if (value == 1) {
                        $('.' + target).show();
                    } else {
                        $('.' + target).hide();
                    }
                    break;
            }
        });
    }
};


let quizImportService = {
    initTable() {
        addFromHistory = $('#addFromHistory').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": '/admin/quiz/api/get-history/' + typeHistory,
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<i class="historyIcon fa fa-plus"></i>'
                },
                {"data": "quiz"},
                {"data": "steps_count"},
                {"data": "questions_count"},
                {"data": "created"},
                {
                    "visible": false,
                    "searchable": true,
                    "data": "step_hidden"
                },
                {
                    "visible": false,
                    "searchable": true,
                    "data": "question_hidden"
                },
                {
                    "visible": false,
                    "searchable": true,
                    "data": "type_hidden"
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#addFromHistory tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = addFromHistory.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $('.historyIcon', tr).addClass('fa-plus').removeClass('fa-minus');
            } else {
                // Open this row
                row.child(quizImportService.format(row.data())).show();
                tr.addClass('shown');
                quizImportService.selectStep();
                quizImportService.addHistory();
                $('.historyIcon', tr).removeClass('fa-plus').addClass('fa-minus');
            }
        });
    },
    format(d) {
        // Create array for choosing steps & questions
        table = '<table class="table">';
        d.steps.map((step) => {
            table += '<tr class="step">';
            table += '<td><input type="checkbox" class="historyStep" data-step_id="' + step.id + '" /></td>';
            table += '<td>' + step.name + '</td>';
            table += '</tr>';
            step.questions.map((question) => {
                if (typeHistory == 'all' || typeHistory == question.type) {
                    table += '<tr class="question">';
                    table += '<td><input type="checkbox" class="marg-l-20 historyQuestion" data-step_id="' + step.id + '" data-question_id="' + question.id + '" /></td>';
                    table += '<td>' + question.name + '</td>';
                    table += '</tr>';
                }
            });
        });

        table += '</table>';

        return table;
    },
    selectStep() {
        $('.historyStep').unbind('click');
        $('.historyStep').click(function () {
            const step_id = $(this).data('step_id');
            if ($(this).is(':checked')) {
                $('.historyQuestion[data-step_id="' + step_id + '"]').prop("checked", true).change();
                // Add all questions
                $('.historyQuestion[data-step_id="' + step_id + '"]').each(function () {
                    const question_id = $(this).data('question_id');
                    if (historyChoosed.indexOf(question_id) == -1) {
                        historyChoosed.push(question_id);
                    }
                });
            } else {
                $('.historyQuestion[data-step_id="' + step_id + '"]').prop("checked", false).change();
                // Remove all questions
                $('.historyQuestion[data-step_id="' + step_id + '"]').each(function () {
                    const question_id = $(this).data('question_id');
                    if (historyChoosed.indexOf(question_id) != -1) {
                        historyChoosed.splice(historyChoosed.indexOf(question_id), 1)
                    }
                });
            }
        });
    },
    addHistory() {
        $('.historyQuestion').unbind('click');
        $('.historyQuestion').click(function () {
            const step_id = $(this).data('step_id');
            const question_id = $(this).data('question_id');
            if ($(this).is(':checked')) {
                if (historyChoosed.indexOf(question_id) === -1) {
                    historyChoosed.push(question_id);
                }
            } else {
                const index = historyChoosed.indexOf(question_id);
                if (index !== -1) {
                    historyChoosed.splice(index, 1);
                }
            }
        });
    },
    addHistoryProcess() {
        $('.addHistory').unbind('click');
        $('.addHistory').click(function () {
            swal({
                title: translations.add_history_title,
                text: translations.add_history_description,
                type: 'warning',
                showConfirmButton: true,
                showCancelButton: true,
                timer: undefined
            }).then((willDelete) => {
                quizService.loading();
                $.ajax({
                    'url': '/admin/quiz/api/get-questions',
                    'data': {questions: historyChoosed},
                    'method': 'POST',
                }).done(function (res) {
                    if (res.status == 'success') {
                        res.questions.map((question) => {
                            // TODO : Clone image drag and drop !!! -> Ajax

                            quizService.addQuestionProcess(question.type, {
                                'history_id': question.history_id,
                                'id': question.id,
                                'question': question.question,
                                'score_to_pass': question.score_to_pass,
                                'params': question.params,
                                'items': question.items,
                                'step': question.step
                            });
                        });
                        $('#add-from-history').modal('hide');
                    }
                    quizService.stopLoading();
                }).fail(function (res) {
                    $('#add-from-history').modal('hide');
                    quizService.stopLoading();
                    console.log('Loading question failed !');
                });
            });

        });
    },
    filterHistory() {
        $('.chooseHistoryType').change(function () {
            typeHistory = $(this).val();
            addFromHistory.ajax.url('/admin/quiz/api/get-history/' + typeHistory).load();
            addFromHistory.ajax.reload();
        });
    }
}