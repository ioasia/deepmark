let textToFill = [];
let textToFillOptions = [];
let textToFillOptionsFree = [];
// Stock all words, deleted or not, to keep all positions
let textToFillOptions_full = [];

quizService.text_to_fill = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.text_to_fill.initTextarea(step_id, question_id);
        quizService.text_to_fill.addOption(step_id, question_id);
    },
    initTextarea(step_id, question_id) {
        textToFill[step_id + '-' + question_id] = CKEDITOR.replace('step-' + step_id + '-q-' + question_id + '-text_to_fill');
        // textToFill[step_id + '-' + question_id] = new Jodit(`#step-${step_id}-q-${question_id}-text_to_fill`, {
        //     toolbarAdaptive: false,
        //     buttons: "bold,strikethrough,underline,italic,eraser,|,ul,ol,|,outdent,indent,|,font,fontsize,brush,paragraph,copyformat,preview"
        // });
        textToFillOptions[step_id + '-' + question_id] = [];
        textToFillOptionsFree[step_id + '-' + question_id] = [];
        textToFillOptions_full[step_id + '-' + question_id] = [];
    },
    // Add an option
    addOption(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillAddOption-free-add').click(function (e) {
            e.preventDefault();
            const selectedText = $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillAddOption-free').val();
            textToFillOptionsFree[step_id + '-' + question_id].push(selectedText);

            // Select container
            const text_to_fillContrainer = $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillOptions');
            // Add badge to view
            text_to_fillContrainer.append('<span class="badge badge-info marg-r-10">' + selectedText + ' <i class="fa fa-times deleteOptionFree" data-val="' + selectedText + '"></i></span>');

            // Bind Plugins
            quizService.text_to_fill.deleteOption(step_id, question_id);
        });
        $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillAddOption').click(function (e) {
            e.preventDefault();
            // Select container
            const text_to_fillContrainer = $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillOptions');
            const editor = textToFill[step_id + '-' + question_id];
            // Get selection
            const selectedText = editor.getSelection().getSelectedText();
            if (selectedText != '') {
                // Add badge option
                textToFillOptions[step_id + '-' + question_id].push('');
                textToFillOptions_full[step_id + '-' + question_id].push('');
                const currPos = textToFillOptions[step_id + '-' + question_id].length - 1;
                const currPos_full = textToFillOptions_full[step_id + '-' + question_id].length - 1;
                textToFillOptions[step_id + '-' + question_id][currPos] = selectedText;
                textToFillOptions_full[step_id + '-' + question_id][currPos_full] = selectedText;
                // Add badge to view
                text_to_fillContrainer.append('<span class="badge badge-info marg-r-10">' + selectedText + ' <i class="fa fa-times deleteOption" data-opt_id="' + currPos + '" data-opt_id_full="' + currPos_full + '"></i></span>');
                // Replace text in ckeditor
                editorContent = $('<textarea />').html(editor.getData()).text();
                const replaced_text = editorContent.replace(selectedText, '<span class="marker">' + textToFillOptions[step_id + '-' + question_id][currPos] + '</span>');
                editor.setData(replaced_text);

                // Bind Plugins
                quizService.initInProgress();
                quizService.text_to_fill.deleteOption(step_id, question_id);
            }
        });
    },
    // Delete option
    deleteOption(step_id, question_id) {

        $('.text_to_fillOptions .deleteOptionFree').unbind('click');
        $('.text_to_fillOptions .deleteOptionFree').click(function (e) {
            // Remove badge
            $(this).parent().remove();
            const valToCheck = $(this).data('val');
            textToFillOptionsFree[step_id + '-' + question_id] = textToFillOptionsFree[step_id + '-' + question_id].filter(function (value, index, arr) {
                return value != valToCheck;
            });
        });
        $('.text_to_fillOptions .deleteOption').unbind('click');
        $('.text_to_fillOptions .deleteOption').click(function (e) {
            // Remove badge
            $(this).parent().remove();
            // Remove in editor
            const editor = textToFill[step_id + '-' + question_id];
            const textOpt = textToFillOptions_full[step_id + '-' + question_id][parseInt($(this).data('opt_id_full'))];
            editorContent = $('<textarea />').html(editor.getData()).text();
            const replaced_text = editorContent.replace(textOpt, '');
            editor.setData(replaced_text);
            // remove in array
            //textToFillOptions[step_id + '-' + question_id].splice(parseInt($(this).data('opt_id')), 1);
            const opt_id = parseInt($(this).data('opt_id'));
            textToFillOptions[step_id + '-' + question_id] = textToFillOptions[step_id + '-' + question_id].filter(function (value, index, arr) {
                return index != opt_id;
            });
        });
    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        items.push({
            type: 'text_to_fill',
            name: 'text_to_fill',
            value: textToFill[step_id + '-' + question_id].getData(),
            specifics: {options: textToFillOptions[step_id + '-' + question_id], options_free: textToFillOptionsFree[step_id + '-' + question_id]}
        });
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        if (items[0].specifics.options.length < 2) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_min_options + '2'};
        } else if (items[0].specifics.options.length > 20) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_max_options + '20'};
        } else {
            let status = {status: 'success'};
            items.map(item => {
                if(item.value == '' || item.value == '<p></p>') {
                    status = {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error};
                }
            })
            return status;
        }
    },
    // Set items
    setItems(step_id, question_id, datas) {
        // Set text
        if (datas[0] != undefined) {
            const editor = textToFill[step_id + '-' + question_id];
            editor.setData(datas[0].value);
            // Add badges option
            if (datas[0].specifics != undefined && datas[0].specifics.options != undefined) {
                datas[0].specifics.options.map((option) => {
                    textToFillOptions[step_id + '-' + question_id].push(option);
                    textToFillOptions_full[step_id + '-' + question_id].push(option);
                    const currPos = textToFillOptions[step_id + '-' + question_id].length - 1;
                    const currPos_full = textToFillOptions_full[step_id + '-' + question_id].length - 1;
                    option = option.replace('<span class="marker">', '').replace('</span>', '');
                    // Add badge to view
                    $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillOptions').append('<span class="badge badge-info marg-r-10">' + option + ' <i class="fa fa-times deleteOption" data-opt_id="' + currPos + '" data-opt_id_full="' + currPos_full + '"></i></span>');
                    // Replace text in ckeditor
                    const replaced_text = editor.getData().replace('<span class="marker" data-pos="' + currPos + '"></span>', '<span class="marker" data-pos="' + currPos + '">' + textToFillOptions[step_id + '-' + question_id][currPos] + '</span>');
                    editor.setData(replaced_text);
                    // Bind Delete
                    quizService.text_to_fill.deleteOption(step_id, question_id);
                });
            }
            
            
            // Add badges option free
            if (datas[0].specifics != undefined && datas[0].specifics.options_free != undefined) {
                datas[0].specifics.options_free.map((option) => {
                    textToFillOptionsFree[step_id + '-' + question_id].push(option);
                    // Add badge to view
                    $('#step-' + step_id + '-q-' + question_id + ' .text_to_fillOptions').append('<span class="badge badge-info marg-r-10">' + option + ' <i class="fa fa-times deleteOptionFree" data-val="' + option + '"></i></span>');
                    // Bind Delete
                    quizService.text_to_fill.deleteOption(step_id, question_id);
                });
            }
        }
    }
}