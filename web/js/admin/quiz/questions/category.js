let options = '';

quizService.category = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.category.addOption(step_id, question_id);
        quizService.category.addConfigurationOption(step_id, question_id);
        quizService.category.checkOptionChanged(step_id, question_id);

        quizService.category.bindSelectpicker(step_id, question_id);
    },
    // Add an option process
    addOptionProcess(step_id, question_id, datas = null) {
        // Select container
        const categoryContrainer = $('#step-' + step_id + '-q-' + question_id + ' .categoryContainer');
        option = quizService.selectTemplateById('question-category-option');
        option = option.html()
                .replace(/question-x-uniqid/g, 'step-' + currStep + '-q-' + questionCount[currStep] + '-' + quizService.generateId())
                .replace(/question-z/g, 'step-' + step_id + '-q-' + question_id);
        // Add option
        $('.categoryOptions', categoryContrainer).append(option).ready(function () {
            if (datas) {
                $('#step-' + step_id + '-q-' + question_id + ' .categoryOptions input[type="text"]:eq(' + datas.i + ')').val(datas.name);
            }
        });
        // BindPlugins
        quizService.initInProgress();
        quizService.category.checkOptionChanged(step_id, question_id);
        quizService.deleteItems();
    },
    // Add an option
    addOption(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .categoryAddOption').click(function (e) {
            e.preventDefault();
            quizService.category.addOptionProcess(step_id, question_id);
        });
    },
    // Add an option (configuration) process
    addConfigurationOptionProcess(step_id, question_id, datas = null) {
        // Select container
        const categoryContrainer = $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationContainer');
        option = quizService.selectTemplateById('question-category-configuration-option');
        option = option.html().replace(/question-z/g, 'step-' + step_id + '-q-' + question_id).replace(/{options_to_fill}/, options);
        // Add option
        $('.categoryConfigurationOptions', categoryContrainer).append(option).ready(function () {
            if (datas) {
                $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationOptions input[type="text"]:eq(' + datas.i + ')').val(datas.name);
                $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationOptions select:eq(' + datas.i + ')').html(datas.options);

                quizService.category.bindSelectpicker(step_id, question_id);
            }
        });
        // BindPlugins
        quizService.initInProgress();
        quizService.category.bindSelectpicker(step_id, question_id);
    },
    // Add an option (configuration)
    addConfigurationOption(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationAddOption').click(function (e) {
            e.preventDefault();
            quizService.category.addConfigurationOptionProcess(step_id, question_id);
        });
    },
    // Fill select when option changed
    checkOptionChanged(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .categoryOptions input').focusout(function () {
            options = '';
            $('#step-' + step_id + '-q-' + question_id + ' .categoryOptions input').each(function () {
                options += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
            });
            $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationContainer select').html(options);

            quizService.category.bindSelectpicker(step_id, question_id);
        });
    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        let config = [];
        // Options config
        $('input[name^="step-' + step_id + '-q-' + question_id + '-config"]').each(function () {
            config.push($(this).val());
        });

        $('input[name^="step-' + step_id + '-q-' + question_id + '-options"]').each(function () {
            items.push({
                type: 'category',
                name: $(this).val(),
                value: $('.step-' + step_id + '-q-' + question_id + '-value select', $(this).parent().parent().parent()).val(),
                specifics: {options: config}
            });
        });
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        if (items.length < 2) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_min_options + '2'};
        } else if (items.length > 20) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_max_options + '20'};
        } else {
            let status = {status: 'success'};
            items.map(item => {
                if(item.name == '') {
                    status = {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error};
                }
            })
            return status;
        }
    },
    // Set items
    setItems(step_id, question_id, datas) {
        if (datas) {
            let options = '';
            // Each "question"
            datas.map((item, i) => {
                options = '';

                // Each options defined
                item.specifics.options.map((opt, k) => {
                    // Configuration (only first question because same options for each question)
                    if (i == 0) {
                        // 2 first options = line already visible, other we create new lines
                        if (k < 2) {
                            // First question
                            $('#step-' + step_id + '-q-' + question_id + ' .categoryOptions input[type="text"]:eq(' + k + ')').val(opt);
                        } else {
                            // Other questions
                            quizService.category.addOptionProcess(step_id, question_id, {i: k, name: opt});
                        }
                    }
                    // Options for current line
                    options += '<option value="' + opt + '" ' + (opt == item.value ? 'selected' : '') + '>' + opt + '</option>';
                });

                if (i < 2) {
                    // First questions
                    $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationOptions input[type="text"]:eq(' + i + ')').val(item.name);
                    $('#step-' + step_id + '-q-' + question_id + ' .categoryConfigurationOptions select:eq(' + i + ')').html(options);

                    quizService.category.bindSelectpicker(step_id, question_id);
                } else {
                    // Other questions
                    quizService.category.addConfigurationOptionProcess(step_id, question_id, {i: i, name: item.name, options: options});
                }
            });
        }
    },
    // Bind plugins
    bindSelectpicker(step_id, question_id) {
        quizService.bindSelectpicker();
        $('#step-' + step_id + '-q-' + question_id + ' .selectpicker').selectpicker('refresh');
    }
}