quizService.open = {
    active_section() {
        return ['default_correction', 'personalized_correction'];
    },
    init(step_id, question_id) {
        quizService.initTemplateUpload(step_id, question_id);
    },
    // Get items
    getItems(step_id, question_id) {
        // Define
        let document = $('input[name^="learners_answer_document-step-' + step_id + '-q-' + question_id + '"]');
        let template = $('#step-' + step_id + '-q-' + question_id + '-template');
        let record = $('input[name^="learners_answer_record-step-' + step_id + '-q-' + question_id + '"]');
        let items = [];
        
        // Learner's answer
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-free"]').is(':checked')) {
            items.push({
                type: 'open',
                name: 'free',
                value: '1',
            });
        }
        
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-document"]').is(':checked') && document.val() != undefined && document.val() != "") {
            items.push({
                type: 'open',
                name: 'document',
                value: '1',
                specifics: {types: JSON.parse($('input[name^="learners_answer_document-step-' + step_id + '-q-' + question_id + '"]').val())}
            });
        }
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-template"]').is(':checked') && template.val() != undefined && template.val() != "") {
            items.push({
                type: 'open',
                name: 'template',
                value: '1',
                specifics: {file: $('#step-' + step_id + '-q-' + question_id + '-template').val(),
                    file_descriptor: $('#step-' + step_id + '-q-' + question_id + '-template-file-descriptor').val()}
            });
        }
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-record"]').is(':checked') && record.val() != undefined && record.val() != "") {
            items.push({
                type: 'open',
                name: 'record',
                value: '1',
                specifics: {types: JSON.parse($('input[name^="learners_answer_record-step-' + step_id + '-q-' + question_id + '"]').val())}
            });
        }
        
        // Trainer's default correction
        let default_correction = 0;
        if($('.default_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').hasClass('active')) {
            default_correction = 1;
            
            items.push({
                type: 'default_correction',
                name: 'default_correction',
                value: '1',
                specifics: {
                    text: editors['step-' + step_id + '-q-' + question_id+'-default_correction'].value,
                    image: $('#step-' + step_id + '-q-' + question_id + '-image-correction').val(),
                    doc: $('#step-' + step_id + '-q-' + question_id + '-doc-correction').val(),
                    song: $('#step-' + step_id + '-q-' + question_id + '-song-correction').val(),
                    video: $('#step-' + step_id + '-q-' + question_id + '-video-correction').val(),
                    file_descriptor: {
                        image: $('#step-' + step_id + '-q-' + question_id + '-image-correction-file-descriptor').val(),
                        doc: $('#step-' + step_id + '-q-' + question_id + '-doc-correction-file-descriptor').val(),
                        song: $('#step-' + step_id + '-q-' + question_id + '-song-correction-file-descriptor').val(),
                        video: $('#step-' + step_id + '-q-' + question_id + '-video-correction-file-descriptor').val(),
                    }
                }
            });
        }
        
        // Trainer's personalized correction
        let personalized_correction = 0;
        if($('.personalized_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').hasClass('active')) {
            personalized_correction = 1;
            // Define
            document = $('input[name^="personalized_correction_document-step-' + step_id + '-q-' + question_id + '"]');
            template = $('#step-' + step_id + '-q-' + question_id + '-template-correction');
            record = $('input[name^="personalized_correction_record-step-' + step_id + '-q-' + question_id + '"]');
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-free"]').is(':checked')) {
                items.push({
                    type: 'personalized_correction',
                    name: 'free',
                    value: '1',
                });
            }
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-document"]').is(':checked') && document.val() != undefined && document.val() != "") {
                items.push({
                    type: 'personalized_correction',
                    name: 'document',
                    value: '1',
                    specifics: {types: JSON.parse($('input[name^="personalized_correction_document-step-' + step_id + '-q-' + question_id + '"]').val())}
                });
            }
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-template"]').is(':checked') && template.val() != undefined && template.val() != "") {
                items.push({
                    type: 'personalized_correction',
                    name: 'template',
                    value: '1',
                    specifics: {file: $('#step-' + step_id + '-q-' + question_id + '-template-correction').val(),
                        file_descriptor: $('#step-' + step_id + '-q-' + question_id + '-template-correction-file-descriptor').val()}
                });
            }
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-record"]').is(':checked') && record.val() != undefined && record.val() != "") {
                items.push({
                    type: 'personalized_correction',
                    name: 'record',
                    value: '1',
                    specifics: {types: JSON.parse($('input[name^="personalized_correction_record-step-' + step_id + '-q-' + question_id + '"]').val())}
                });
            }
        }
        
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        // Define
        let document = $('input[name^="learners_answer_document-step-' + step_id + '-q-' + question_id + '"]');
        let template = $('#step-' + step_id + '-q-' + question_id + '-template');
        let record = $('input[name^="learners_answer_record-step-' + step_id + '-q-' + question_id + '"]');
        // Check
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-document"]').is(':checked') && (document.val() == undefined || document.val() == "")) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_1};
        }
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-template"]').is(':checked') && (template.val() == undefined || template.val() == "")) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_2};
        }
        if($('input[name^="step-' + step_id + '-q-' + question_id + '-record"]').is(':checked') && (record.val() == undefined || record.val() == "")) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_3};
        }
        
        // Trainer's default correction
        let default_correction = 0;
        if($('.default_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').hasClass('active')) {
            default_correction = 1;
        }
        
        // Trainer's personalized correction
        let personalized_correction = 0;
        if($('.personalized_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').hasClass('active')) {
            personalized_correction = 1;
            // Define
            document = $('input[name^="personalized_correction_document-step-' + step_id + '-q-' + question_id + '"]');
            template = $('#step-' + step_id + '-q-' + question_id + '-template-correction');
            record = $('input[name^="personalized_correction_record-step-' + step_id + '-q-' + question_id + '"]');
            // Check
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-document"]').is(':checked') && (document.val() == undefined || document.val() == "")) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_1};
            }
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-template"]').is(':checked') && (template.val() == undefined || template.val() == "")) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_2};
            }
            if($('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-record"]').is(':checked') && (record.val() == undefined || record.val() == "")) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_3};
            }
        }
        
        if(default_correction == 0 && personalized_correction == 0 && quizService.getIsSurvey() == false) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_open_4};
        }
        return {status: 'success'}
    },
    // Set items 
    setItems(step_id, question_id, datas) {
        if (datas) {
            datas.map((item, i) => {
                // Switch item type
                switch(item.type) {
                    // Open = Question
                    case 'open' :
                        // Switch item name
                        switch(item.name) {
                            case 'free' :
                                if(item.value == '1') {
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-free"]').attr('checked', 'checked');
                                }
                                break;
                            case 'document' :
                                if(item.value == '1') {
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-document"]').attr('checked', 'checked');
                                }
                                if(Array.isArray(item.specifics.types)) {
                                    // Add input
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-document"]').parent().append('<input type="hidden" name="learners_answer_document-step-'+step_id + '-q-' + question_id+'" class="learners_answer_document-step-'+step_id + '-q-' + question_id+'" value=\''+JSON.stringify(item.specifics.types)+'\' />');
                                    // Active badged
                                    item.specifics.types.map(type => {
                                        $('.learners_answer_document-step-'+step_id + '-q-' + question_id+'[data-actionable_name="'+type+'"]').addClass('active');
                                    });
                                }
                                break;
                            case 'template' :
                                if(item.value == '1') {
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-template"]').attr('checked', 'checked');
                                    // Show file
                                    $('#step-' + step_id + '-q-' + question_id + '-template').val(item.specifics.file);
                                    $('#step-' + step_id + '-q-' + question_id + '-template-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="step-' + step_id + '-q-' + question_id + '-template-preview"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/doc/' + item.specifics.file + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                                }
                                break;
                            case 'record' :
                                if(item.value == '1') {
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-record"]').attr('checked', 'checked');
                                }
                                if(Array.isArray(item.specifics.types)) {
                                    // Add input
                                    $('input[name^="step-' + step_id + '-q-' + question_id + '-record"]').parent().append('<input type="hidden" name="learners_answer_record-step-'+step_id + '-q-' + question_id+'" class="learners_answer_record-step-'+step_id + '-q-' + question_id+'" value=\''+JSON.stringify(item.specifics.types)+'\' />');
                                    // Active badged
                                    item.specifics.types.map(type => {
                                        $('.learners_answer_record-step-'+step_id + '-q-' + question_id+'[data-actionable_name="'+type+'"]').addClass('active');
                                    });
                                }
                                break;
                        }
                    break;
                    // default_correction = trainer's default correction
                    case 'default_correction' :
                        editors['step-' + step_id + '-q-' + question_id+'-default_correction'].value=item.specifics.text;
                        // Active div
                        $('.default_correction-step-' + step_id + '-q-' + question_id).removeClass('active');
                        $('.default_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').addClass('active');
                        $('.default_correction-step-' + step_id + '-q-' + question_id+'-div').show();
                        // Show files
                        const imageId = 'step-' + step_id + '-q-' + question_id + '-image-correction';
                        $('#' + imageId).val(item.specifics.image);
                        const docId = 'step-' + step_id + '-q-' + question_id + '-doc-correction';
                        $('#' + docId).val(item.specifics.doc);
                        const songId = 'step-' + step_id + '-q-' + question_id + '-song-correction';
                        $('#' + songId).val(item.specifics.song);
                        const videoId = 'step-' + step_id + '-q-' + question_id + '-video-correction';
                        $('#' + videoId).val(item.specifics.video);
                        if (item.specifics.image) {
                            $('#' + imageId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + imageId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/picture/' + item.specifics.image + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                        }
                        if (item.specifics.doc) {
                            $('#' + docId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + docId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/doc/' + item.specifics.doc + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                        }
                        if (item.specifics.song) {
                            $('#' + songId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + songId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/song/' + item.specifics.song + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                        }
                        if (item.specifics.video) {
                            $('#' + videoId + '-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + videoId + '"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/video/' + item.specifics.video + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                        }
                        uploadService.removeFiles();
                        uploadService.noPreview();
                        break;
                    // personalized_correction = trainer's personalized correction
                    case 'personalized_correction' :
                        // Active div
                        $('.personalized_correction-step-' + step_id + '-q-' + question_id).removeClass('active');
                        $('.personalized_correction-step-' + step_id + '-q-' + question_id+'[data-actionable_value="1"]').addClass('active');
                        $('.personalized_correction-step-' + step_id + '-q-' + question_id+'-div').show();
                        // Switch item name
                        switch(item.name) {
                            case 'free' :
                                if(item.value == '1') {
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-free"]').attr('checked', 'checked');
                                }
                                break;
                            case 'document' :
                                if(item.value == '1') {
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-document"]').attr('checked', 'checked');
                                }
                                if(Array.isArray(item.specifics.types)) {
                                    // Add input
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-document"]').parent().append('<input type="hidden" name="personalized_correction_document-step-'+step_id + '-q-' + question_id+'" class="personalized_correction_document-step-'+step_id + '-q-' + question_id+'" value=\''+JSON.stringify(item.specifics.types)+'\' />');
                                    // Active badged
                                    item.specifics.types.map(type => {
                                        $('.personalized_correction_document-step-'+step_id + '-q-' + question_id+'[data-actionable_name="'+type+'"]').addClass('active');
                                    });
                                }
                                break;
                            case 'template' :
                                if(item.value == '1') {
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-template"]').attr('checked', 'checked');
                                    // Show file
                                    $('#step-' + step_id + '-q-' + question_id + '-template-correction').val(item.specifics.file);
                                    $('#step-' + step_id + '-q-' + question_id + '-template-correction-preview').html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="step-' + step_id + '-q-' + question_id + '-template-correction-preview"></i> <a target="_blank" ' + (datas.history_id != undefined ? 'class="noPreview" href="#"' : 'href="/files/quiz/' + item.question_id + '/doc/' + item.specifics.file + '" class="previewLoaded"') + '><span class="text-info">' + translations.file_sended + '</span></a>');
                                }
                                break;
                            case 'record' :
                                if(item.value == '1') {
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-record"]').attr('checked', 'checked');
                                }
                                if(Array.isArray(item.specifics.types)) {
                                    // Add input
                                    $('input[name^="personalized_correction-step-' + step_id + '-q-' + question_id + '-record"]').parent().append('<input type="hidden" name="personalized_correction_record-step-'+step_id + '-q-' + question_id+'" class="personalized_correction_record-step-'+step_id + '-q-' + question_id+'" value=\''+JSON.stringify(item.specifics.types)+'\' />');
                                    // Active badged
                                    item.specifics.types.map(type => {
                                        $('.personalized_correction_record-step-'+step_id + '-q-' + question_id+'[data-actionable_name="'+type+'"]').addClass('active');
                                    });
                                }
                                break;
                        }
                        break;
                }
            });
        }
    }
}