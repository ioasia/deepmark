quizService.drag_and_drop = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.drag_and_drop.addOptionOnModal(step_id, question_id);
        quizService.drag_and_drop.addOption(step_id, question_id);
    },
    // Add an option process
    addOptionProcess(step_id, question_id, type, datas = null) {
        let uploadIds = [];
        // Switch type
        switch (type) {
            case 'imageToImage':
                uploadIds.push(uploadService.generateId());
                uploadIds.push(uploadService.generateId());
                break;
            case 'imageToText' :
                uploadIds.push(uploadService.generateId());
                break;
        }
        // Select container
        const dragAndDropContrainer = $('#step-' + step_id + '-q-' + question_id + ' .dragAndDropContainer');
        option = quizService.selectTemplateById('question-dragAndDrop-option-' + type);
        option = option.html();
        // Replace upload ids
        uploadIds.map((id) => {
            option = option
                    .replace(/question-x-uniqid/g, 'step-' + currStep + '-q-' + questionCount[currStep] + '-' + quizService.generateId())
                    .replace('upload-result-step-N-q-x', id + '-result')
                    .replace('upload-id', id);
        });

        // Add option
        $('.dragAndDropOptions', dragAndDropContrainer).append(option).ready(function () {
            if (datas) {
                // Fill left image
                if (question_id.indexOf('tmp_') == -1 && $('#step-' + step_id + '-q-' + question_id + '-history_id').val() != '') {
                    $('#step-' + step_id + '-q-' + question_id + ' .saveFirst').show();
                    $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-left:eq(' + datas.i + ') .upload-area').html('<div class="dz-default dz-message uploadContent"><span><h1 class="text-center c-orangelight"><i class="fa fa-save"></i></h1></span></div>');
                } else {
                    $('#step-' + step_id + '-q-' + question_id + ' .saveFirst').hide();
                    $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-left:eq(' + datas.i + ') .upload-area').html('<div class="uploadShow"><img data-dz-thumbnail="" alt="" src="/files/quiz/' + datas.item.question_id + '/drag-and-drop/' + datas.item.value + '"></div>');
                }
                $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-left:eq(' + datas.i + ') .upload-result').val(datas.item.value);
                // Fill right option
                switch (datas.item.name) {
                    case 'image_to_image':
                        if (question_id.indexOf('tmp_') == -1 && $('#step-' + step_id + '-q-' + question_id + '-history_id').val() != '') {
                            $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-right:eq(' + datas.compt_iti + ') .upload-area').html('<div class="dz-default dz-message uploadContent"><span><h1 class="text-center c-orangelight"><i class="fa fa-save"></i></h1></span></div>');
                        } else {
                            $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-right:eq(' + datas.compt_iti + ') .upload-area').html('<div class="uploadShow"><img data-dz-thumbnail="" alt="" src="/files/quiz/' + datas.item.question_id + '/drag-and-drop/' + datas.item.specifics.match + '"></div>');
                        }
                        $('#step-' + step_id + '-q-' + question_id + ' .drag_and_drop-option-right:eq(' + datas.compt_iti + ') .upload-result').val(datas.item.specifics.match);
                        break;
                    case 'image_to_text' :
                        $('#step-' + step_id + '-q-' + question_id + ' .option-text:eq(' + datas.compt_itt + ')').val(datas.item.specifics.match);
                        break;
                }
            }
        });

        // Init Dropzone
        uploadIds.map((id) => {
            uploadService.addUpload(id, id + '-result', false, '/admin/quiz/api/upload', 'fa fa-image', '', 'image/*', {
                quiz_id: $('#quiz_id').val(),
                type: 'quiz_q_drad_and_drop',
                step_id: step_id,
                question_id: question_id
            },
                    false);
        });

        // BindPlugins
        quizService.initInProgress();
        quizService.deleteItems();
    },
    addOptionOnModal(step_id, question_id) {
        $('.addOptionOnModal').click(function () {
            curr_step_id = $(this).data('step_id');
            curr_question_id = $(this).data('question_id');
        });
    },
    // Add an option
    addOption(step_id, question_id) {
        $('.dradAndDropAddOption').unbind('click');
        $('.dradAndDropAddOption').click(function (e) {
            e.preventDefault();
            const type = $(this).data('type');
            quizService.drag_and_drop.addOptionProcess(curr_step_id, curr_question_id, type);
            quizService.drag_and_drop.preventPoints();
        });

    },
    // We check points in learner parts, so can't add points in ITT here
    preventPoints() {
        $('[data-name="image_to_text"] input').unbind('keypress');
        $('[data-name="image_to_text"] input').on('keypress', function (e) {
            if (e.which == 46) {
                return false;
            }
        });
    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        // Get each lines
        $('#step-' + step_id + '-q-' + question_id + ' .dragAndDropOptions .match').each(function () {
            // Action name
            const name = $(this).data('name');
            let value;
            let specifics;
            switch (name) {
                case 'image_to_image':
                    value = $('.upload-result:eq(0)', $(this)).val();
                    specifics = {match: $('.upload-result:eq(1)', $(this)).val(), match_type: 'image'};
                    break;
                case 'image_to_text' :
                    value = $('.upload-result:eq(0)', $(this)).val();
                    specifics = {match: $('input:eq(1)', $(this)).val(), match_type: 'text'};
                    break;
            }
            items.push({
                type: 'drag_and_drop',
                name: name,
                value: value,
                specifics: specifics
            });
        });

        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        if (items.length < 2) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_min_options + '2'};
        } else if (items.length > 8) {
            return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_max_options + '8'};
        } else {
            let status = {status: 'success'};
            items.map(item => {
                if(item.name == '' || item.value == '') {
                    status = {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error};
                }
            })
            return status;
        }
    },
    // Set items
    setItems(step_id, question_id, datas) {
        if (datas) {
            let compt_iti = -1;
            let compt_itt = -1;
            datas.map((item, i) => {
                let type = '';
                switch (item.name) {
                    case 'image_to_image':
                        compt_iti++;
                        type = 'imageToImage';
                        break;
                    case 'image_to_text':
                        compt_itt++;
                        type = 'imageToText';
                        break;
                }
                quizService.drag_and_drop.addOptionProcess(step_id, question_id, type, {compt_iti: compt_iti, compt_itt: compt_itt, i: i, item: item});
                quizService.drag_and_drop.preventPoints();
            });
        }
    }
}