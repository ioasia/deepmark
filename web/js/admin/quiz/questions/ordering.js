quizService.ordering = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.ordering.addOption(step_id, question_id);
        quizService.ordering.bindSortable(step_id, question_id);
    },
    
    // Add an option process
    addOptionProcess(step_id, question_id, datas = null) {
        // Select container
        const orderingContrainer = $('#step-' + step_id + '-q-' + question_id + ' .orderingContainer');
        option = quizService.selectTemplateById('question-ordering-option');
        option = option.html()
                .replace(/question-x-uniqid/g, 'step-' + currStep + '-q-' + questionCount[currStep]+'-'+quizService.generateId())
                .replace(/question-z/g, 'step-' + step_id + '-q-' + question_id)
                .replace(/option-pos/, $('li', orderingContrainer).length+1);
        // Add option
        $('.orderingOptions', orderingContrainer).append(option).ready(function () {
           if(datas) {
               $('#step-' +step_id+'-q-'+question_id+' input[type="text"]:eq('+datas.i+')').val(datas.name);
           } 
        });

        // BindPlugins
        quizService.initInProgress();
        quizService.ordering.bindSortable(step_id, question_id);
        quizService.deleteItems();
    },
    // Add an option
    addOption(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .orderingAddOption').click(function (e) {
            e.preventDefault();
            quizService.ordering.addOptionProcess(step_id, question_id);
        });
    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        $('input[name^="step-' + step_id + '-q-' + question_id + '-options"]').each(function () {
            items.push({
                type: 'ordering',
                name: $(this).val(),
                value: $('input[type="checkbox"]', $(this).parent().parent()).is(':checked') ? 1 : 0,
                specifics: {}
            });
        });
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        if(items.length < 2) {
            return {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error_min_options+'2'};
        } else if(items.length > 20) {
            return {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error_max_options+'20'};
        } else {
            let status = {status: 'success'};
            items.map(item => {
                if(item.name == '') {
                    status = {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error};
                }
            })
            return status;
        }
    },
    // Set items
    setItems(step_id, question_id, datas) {
         if(datas) {
            datas.map((item, i) => {
                if(i < 2) {
                    // First questions
                    $('#step-' +step_id+'-q-'+question_id+' input[type="text"]:eq('+i+')').val(item.name);
                } else {
                    // Other questions
                    quizService.ordering.addOptionProcess(step_id, question_id, {i : i,  name: item.name});
                }
            });
        }
    },

    // Bind plugins
    bindSortable(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .orderingOptions').sortable();
        $('#step-' + step_id + '-q-' + question_id + ' .orderingOptions').disableSelection();
    }
}