quizService.qcu = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.qcu.addOption(step_id, question_id);
    },
    // Add an option process
    addOptionProcess(step_id, question_id, datas = null) {
        // Select container
        const qcuContrainer = $('#step-' +step_id+'-q-'+question_id+' .qcuContainer');
        option = quizService.selectTemplateById('question-qcu-option');
        option = option.html()
                .replace(/question-x-uniqid/g, 'step-' + currStep + '-q-' + questionCount[currStep]+'-'+quizService.generateId())
                .replace(/question-z/g, 'step-' +step_id+'-q-'+question_id);
        // Add option
        $('.qcuOptions', qcuContrainer).append(option).ready(function () {
           if(datas) {
               $('#step-' +step_id+'-q-'+question_id+' input[type="text"]:eq('+datas.i+')').val(datas.name);
                if(datas.value == 1) {
                    $('#step-' +step_id+'-q-'+question_id+' input[type="radio"]:eq('+datas.i+')').attr('checked', 'checked');
                }
           } 
           // Is survey or not
           if(quizService.getIsSurvey() == true) {
               $('#step-' +step_id+'-q-'+question_id+' input[type="radio"]').attr('disabled', 'disabled');
           }
        });

        // BindPlugins
        quizService.initInProgress();
        quizService.deleteItems();
    },
    // Add an option
    addOption(step_id, question_id) {
        $('#step-' + step_id + '-q-' + question_id + ' .qcuAddOption').click(function(e){
            e.preventDefault();
            quizService.qcu.addOptionProcess(step_id, question_id);
        });
    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        $('input[name^="step-'+step_id+'-q-'+question_id+'-options"]').each(function() {
            items.push({
                type: 'qcu',
                name: $(this).val(),
                value: $('input[type="radio"]', $(this).parent().parent()).is(':checked') ? 1 : 0,
                specifics: {}
            });
        });
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id) {
        if(items.length < 2) {
            return {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error_min_options+'2'};
        } else if(items.length > 20) {
            return {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error_max_options+'20'};
        } else {
            let status = {status: 'success'};
            items.map(item => {
                if(item.name == '') {
                    status = {status: 'error', error: '<b>'+step_name+' > '+question_name+' : </b><br />'+translations.error};
                }
            })
            return status;
        }
    },
    // Set items
    setItems(step_id, question_id, datas) {
        if(datas) {
            datas.map((item, i) => {
                if(i < 2) {
                    // First questions
                    $('#step-' +step_id+'-q-'+question_id+' input[type="text"]:eq('+i+')').val(item.name);
                    if(item.value == 1) {
                        $('#step-' +step_id+'-q-'+question_id+' input[type="radio"]:eq('+i+')').attr('checked', 'checked');
                    }
                } else {
                    // Other questions
                    quizService.qcu.addOptionProcess(step_id, question_id, {i : i,  name: item.name, value: item.value});
                }
            });
        }
    }
}