/**
 * - UPLOADS w/ Dropzone -
 * 
 * Define this in your Html :
 * <div class="upload-area dropzone" id="quizUploadVideo"></div>
 * <input type="hidden" id="quizUploadVideo-result" data-path="" />
 * 
 * Call this in your JS :
 * 
 * uploadService.addUpload('your_element_id', '/admin/quiz/api/upload', 'fa fa-image, 'Your Text', 'image/*', {your_params_for_php_treatment});
 *  
 * 
 * 
 * 
 * No autodiscover, declare your own zone for each upload :
 * <div class="upload-area" id="{$id}"></div>
 */
Dropzone.autoDiscover = false;

// Upload Instances
let uploadsBoxes = [];

// Upload Service
let uploadService = {
    /**
     *  Create dropzone instance and show uploaded image
     * @param {int} id id of element
     * @param {int} id id of element to put the result
     * @param {int} id id of element to put the preview loading & url
     * @param {string} url URL of treatment
     * @param {string} icon ex : fa fa-image
     * @param {string} text ex : add an image
     * @param {boolean} iconProcess if you want to show icons process (true) or image preview (false)
     * @param {int} widthThumb width thumbnail
     * @param {int} heightThumb height thumbnail
     */
    addUpload(id, id_result, id_preview, url, icon = 'fa fa-image', text = '', accepted = 'image/*', params = {}, iconProcess = true, widthThumb = 100, heightThumb = 100, modal_to_close = false, addSwal = false) {
        if (uploadsBoxes[id] != undefined) {
            uploadsBoxes[id].destroy();
        }
        uploadsBoxes[id] = new Dropzone("div#" + id, {
            url: url,
            params: params,
            addRemoveLinks: false,
            maxFilesize: 100,
            maxFiles: 1,
            acceptedFiles: accepted,
            thumbnailWidth: widthThumb,
            thumbnailHeight: heightThumb,
            dictDefaultMessage: '<h1 class="text-center c-orangelight"><i class="' + icon + '"></i></h1><h4 class="text-center c-orangelight">' + text + '</h4>',
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><img data-dz-thumbnail /></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div>',
            clickable: '#' + id + '.dropzone',
            // Init text
            init: function (file) {

                // IconProcess
                if (iconProcess === true) {
                    $('#' + id).html('<div class="dz-default dz-message uploadContent"><span><h1 class="text-center c-orangelight"><i class="' + icon + '"></i></h1><h4 class="text-center c-orangelight">' + text + '</h4></span></div>');
                    //$('#' + id).html('<div class="uploadContent"><h1 class="text-center c-orangelight"><i class="' + icon + '"></i></h1><h4 class="text-center c-orangelight">' + text + '</h4></div>');
                    // Server response when uploaded
                    this.on("success", function (file, res) {
                        switch (res.status) {
                            case 'success' :
                                $('#' + id).html('<div class="uploadContent"><h1 class="text-center c-green"><i class="fa fa-check"></i></h1></div>');
                                break;
                            case 'fail' :
                                $('#' + id).html('<div class="uploadContent"><h1 class="text-center c-red"><i class="fa fa-times"></i></h1><p class="text-center c-red">'+translations.error_unknown+'</p></div>');
                                break;
                        }
                    });
                }
            },
            // Error
            error: function (file, errors) {
                if (params.step_id && params.question_id) {
                    $('#step-' + params.step_id + '-q-' + params.question_id + ' .saveQuestion').show();
                }
                // IconProcess
                $('#' + id).html('<div class="uploadContent"><h1 class="text-center c-red"><i class="fa fa-times"></i></h1><p class="text-center c-red">' + errors + '</p></div>');
                if (id_preview != false) {
                    $('#' + id_preview).html('<span class="c-red"><i class="fa fa-times cursorPointer removeFile" data-result="' + id_result + '"></i> ' + errors + '</span>');
                    uploadService.removeFiles();
                }
            },
            // Uploading...
            sending: function (file) {
                if (params.step_id && params.question_id) {
                    $('#step-' + params.step_id + '-q-' + params.question_id + ' .saveQuestion').hide();
                }
                // Edit mode
                $('#' + id + ' .uploadShow').remove();
                // IconProcess
                if (iconProcess === true) {
                    $('#' + id).html('<div class="uploadContent"><h1 class="text-center c-orangelight"><i class="fas fa-spinner fa-pulse"></i></h1></div>');
                } else {
                    $('.uploadContent').remove();
                    $('#' + id + " .dz-default").remove();
                    $('#' + id + " div").css('margin-top', 0);
                }

                if (id_preview != false) {
                    $('.modal').modal('hide');
                    $('#' + id_preview).html('<span class="c-orangelight"><i class="fa fa-fas fa-spinner fa-pulse"></i> ' + translations.loading + '</span>');
                }
            },
            accept: function (file, done) {
                done();
            },
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            success: function (file, res) {
                if (res.status == 'success') {
                    // Status OK
                    
                    // Show save question
                    if (params.step_id && params.question_id) {
                        $('#step-' + params.step_id + '-q-' + params.question_id + ' .saveQuestion').show();
                    }
                    // Replace filename in hidden field
                    $('#' + id_result).val(res.file.path);
                    $('#' + id_result + '-file-descriptor').val(JSON.stringify(res.file));
                    // Close modal
                    $('#' + modal_to_close).modal('hide');
                    if (addSwal === true) {
                        // SweetAlert Confirm
                        uploadService.confirmAlert();
                    }
                    
                    if (id_preview != false && id_preview != 'import_csv') {
                        // Create preview if defined
                        $('.modal').modal('hide');
                        $('#' + id_preview).html('<span><i class="fa fa-times c-red cursorPointer removeFile" data-result="' + id_result + '"></i> <a href="#" class="noPreview"><span class="text-info">' + translations.file_sended + '</span></a>');
                        uploadService.removeFiles();
                        uploadService.noPreview();
                    } else if (id_preview == 'import_csv') {
                        // Import CSV -> generate questions
                        res.questions.map((question) => {
                            quizService.addQuestionProcess(question.type, {
                                'id': question.id,
                                'question': question.question,
                                'score_to_pass': question.score_to_pass,
                                'params': question.params,
                                'items': question.items,
                                'step': question.step
                            });
                        });
                        $('.modal').modal('hide');
                    }
                } else {
                    // Status Error
                    if (params.step_id && params.question_id) {
                        $('#step-' + params.step_id + '-q-' + params.question_id + ' .saveQuestion').show();
                    }
                    // IconProcess
                    $('#' + id).html('<div class="uploadContent"><h1 class="text-center c-red"><i class="fa fa-times"></i></h1><p class="text-center c-red">' + res.errors + '</p></div>');
                    if (id_preview != false) {
                        $('#' + id_preview).html('<span class="c-red"><i class="fa fa-times cursorPointer removeFile" data-result="' + id_result + '"></i> ' + res.errors + '</span>');
                        uploadService.removeFiles();
                    }
                }


            }
        });
    },
    generateId() {
        return 'upload-' + quizService.generateId();
    },
    // Sweet Alert
    confirmAlert() {
        swal({
            title: translations.upload_complete,
            type: 'success',
            timer: 1500,
            showConfirmButton: false
        });
    },
    removeFiles() {
        $('.removeFile').unbind('click');
        $('.removeFile').click(function () {
            $('#' + $(this).data('result')).val('');
            $(this).parent().remove();
        });
    },
    noPreview() {
        $('.previewLoaded').unbind('click');
        $('.noPreview').unbind('click');
        $('.noPreview').click(function(e){
            e.preventDefault();
            swal(translations.no_preview_title, translations.no_preview_description, "info", {});
        });
    }
};

