$(document).ready(function() {
    bindDayBadges();
    bindCalendar();
    $('#switch-sm').on('click', function () {
        if ($("#switch-sm").is(':checked')) {
            $('#divCalendar').removeClass('hidden');
            $('#guidelines').removeClass('hidden');
        }
        else{
            $('#divCalendar').addClass('hidden');
            $('#guidelines').addClass('hidden');
        }
    });
    if ($("#switch-sm").is(':checked')) {
        $('#divCalendar').removeClass('hidden');
        $('#guidelines').removeClass('hidden');
    }
});


function bindCalendar() {
    const headerOptions = {
        left: '',
        center: 'prev,title,next',
        right: 'addAvailability, removeAvailability'
    };
    let locate = $('#locate').val();
    const calendar = $('#calendar');
    today = new Date();
    y = today.getFullYear();
    m = today.getMonth();
    d = today.getDate();
    calendar.fullCalendar({
        locale: locate,
        editable: true,
        selectOverlap: false,
        eventOverlap: false,
        firstDay: 1,
        header: headerOptions,
        selectable: true,
        eventLimit: true,
        select: function (start, end) {
            let eventData = {
                title: '',
                start: start,
                end: end
            };
            let chosenRanges = [];
            let weeksExclude = $('#weeks_exclude').val();
            weeksExclude.split(",").forEach(function (item) {
                if (item.length) {
                    chosenRanges.push(item);
                }
            });

            chosenRanges.push(start.format() + '|' + end.format());

            $('#weeks_exclude').val(chosenRanges);
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
            $('#calendar').fullCalendar('unselect');
        },
        contentHeight: 600,
        defaultView: 'month',
        eventColor: 'red',
        eventClick: function (calEvent, jsEvent, view) {
            $("#deleteModal").modal('show');

            $('#deleteBtn').off('click').click(function () {
                let startDate = moment(new Date(calEvent.start._d)).format("YYYY-MM-DD");
                let endDate = moment(new Date(calEvent.end._d)).format("YYYY-MM-DD");
                let week = startDate + '|' + endDate;
                let weeksExclude = $('#weeks_exclude').val();
                let arrWeek = weeksExclude.split(",");
                let filteredItems = arrWeek.filter(item => item !== week);
                $('#weeks_exclude').val(filteredItems);
                $('#calendar').fullCalendar('removeEvents', calEvent._id);

                $("#deleteModal").modal('hide');
            });
        },
        viewRender: function (view, element) {
            let weeksExclude = $('#weeks_exclude').val();

            if (weeksExclude.length) {
                updateCalendar(view.start.format(), view.end.format(), weeksExclude);
            }
        }
    });
}

function updateCalendar(start, end, weeksExclude) {
    return new Promise((resolve, reject) => {
        const selectorCalendar = '#calendar';
        let url = '/admin/courses/api/calendars-constraints';

        if (start && end) {
            url += '?dateStart=' + start + '&dateEnd=' + end + '&excludeWeeks=' + weeksExclude;
        }
        $.ajax({
            'url': url,
            'method': 'GET',
        }).done(function (response) {
            $(selectorCalendar).fullCalendar('removeEvents');
            $(selectorCalendar).fullCalendar('addEventSource', response);
            $(selectorCalendar).fullCalendar('rerenderEvents');

            resolve(response);
        });
    });
}

function bindDayBadges() {

    const chosenDays = [];

    $('.badge-day.active').each(function () {
        chosenDays.push(parseInt($(this).data('day')));
        let nameDay = $(this).attr("data-name");
        let dataDay = $(this).attr("data-day");
        let dataStartMorningTime = $(this).attr("data-start-morning-time");
        let dataEndMorningTime = $(this).attr("data-end-morning-time");
        let dataStartAfternoonTime = $(this).attr("data-start-afternoon-time");
        let dataEndAfternoonTime = $(this).attr("data-end-afternoon-time");
        let data = generateTime(nameDay, dataDay, dataStartMorningTime, dataEndMorningTime, dataStartAfternoonTime, dataEndAfternoonTime, $("#jsTranslations").data("global_from"), $("#jsTranslations").data("global_to"));
        $("#divDays").append(data);
    });

    $('#days_open_for_session').val(chosenDays);

    $('.badge-day').on('click', function () {
        let nameDay = $(this).attr("data-name");
        let dataDay = $(this).attr("data-day");
        const day = parseInt($(this).data('day'));

        if (chosenDays.includes(day)) {
            chosenDays.splice(chosenDays.indexOf(day), 1);
            $(this).removeClass('active');
            $("#div"+nameDay).remove();

        } else {
            chosenDays.push(day);
            $(this).addClass('active');
            let dataStartMorningTime = $(this).attr("data-start-morning-time");
            let dataEndMorningTime = $(this).attr("data-end-morning-time");
            let dataStartAfternoonTime = $(this).attr("data-start-afternoon-time");
            let dataEndAfternoonTime = $(this).attr("data-end-afternoon-time");
            let data = generateTime(nameDay, dataDay, dataStartMorningTime, dataEndMorningTime, dataStartAfternoonTime, dataEndAfternoonTime, $("#jsTranslations").data("global_from"), $("#jsTranslations").data("global_to"));
            $("#divDays").append(data);

        }

        $('#days_open_for_session').val(chosenDays);
    });
}

function generateTime(nameDay, dataDay, dataStartMorningTime, dataEndMorningTime, dataStartAfternoonTime, dataEndAfternoonTime, txtFrom, txtTo) {
    let data = ' <div class="row"><div id="div'+nameDay+'" class="col-md-12 open-time">\
                    <div class="from-to-time">\
                        <span class="day-label col-form-label text-left" style="width: 104px"><b>'+nameDay+':</b> </span>\
                        <span class="col-form-label"> '+txtFrom+'</span>\
                        <div class="form-group timepicker col-md-1">\
                            <input name="startMorningTime'+dataDay+'" type="text" class="form-control timepicker" value="'+dataStartMorningTime+'">\
                        </div>\
                        <span class="col-form-label">'+txtTo+'</span>\
                        <div class="form-group timepicker col-md-1">\
                            <input name="endMorningTime'+dataDay+'" type="text" class="form-control timepicker" value="'+dataEndMorningTime+'">\
                        </div>\
                        <span class="col-form-label"><i class="fas fa-utensils-alt color-header inputMonday iconMonday"></i></span>\
                        <span class="col-form-label">'+txtFrom+'</span>\
                        <div class="form-group timepicker col-md-1">\
                            <input name="startAfternoonTime'+dataDay+'" type="text" class="form-control timepicker" value="'+dataStartAfternoonTime+'">\
                        </div>\
                        <span class="col-form-label">'+txtTo+'</span>\
                        <div class="form-group timepicker col-md-1">\
                            <input name="endAfternoonTime'+dataDay+'"  type="text" class="form-control timepicker" value="'+dataEndAfternoonTime+'">\
                        </div>\
                    </div>\
                </div></div>';
    return data;
}
