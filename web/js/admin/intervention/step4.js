const HEADER_OPTIONS = {
    left: '',
    center: 'prev, title, next',
    right: ''
};

$(document).ready(function () {

    let sessionId = $('.card-intervention-add')[0].getAttribute('data-id');
    let trainerList = {}, chosenTrainerIds = {};
    let vclassModalData = {};
    let otherBookingModalData = [];

    let modules = $('.module');

    modules.each((module) => {
        var moduleId = modules[module].getAttribute('data-id');
        var appId = parseInt(modules[module].getAttribute('data-appId'));

        if (appId === 6) {
            getOnlineAvailableTrainer(sessionId, moduleId);
        } else if (appId === 7 || appId === 8 || appId === 9) {
            var minDate = modules[module].getAttribute('data-minDate');
            var maxDate = modules[module].getAttribute('data-maxDate');
            // createCalendar(moduleId, minDate, maxDate);
            getOtherBooking(moduleId, minDate, maxDate);
        }
    });

    function getOnlineAvailableTrainer(sessionId, moduleId) {
        $.ajax({
            'url': '/admin/trainers/available/module/' + sessionId + '?moduleId=' + moduleId,
            'method': 'GET',
            statusCode: {
                404: function () {
                    swal("Error", "We couldn't process this request", "error", {
                        button: "Ok",
                    });
                },
                500: function () {
                    swal("Error", "We couldn't process this request. Please contact IT.", "error", {
                        button: "Ok",
                    });
                }
            },
        }).done(function (response) {

            if (response.done) markModuleDone(moduleId);

            trainerList[moduleId] = response.results;
            chosenTrainerIds[moduleId] = [];
            const template = getOnlineTrainerTemplate();

            response.results.forEach((data) => {
                const trainerData = {
                    name: data.trainer.first_name + ' ' + data.trainer.last_name,
                    id: data.trainer.id,
                    hours: data.hours,
                    start: moment(data.dateStart).format('l'),
                    end: moment(data.dateEnd).format('l'),
                    booked: data.booked
                };

                let trainerItem = bindTrainerItem(template.clone(), trainerData, moduleId, response.hourQuotaNeeded);
                $(`#trainers-${moduleId}`).append(trainerItem);
            });
            updateRemainingHours(moduleId, response.hourQuotaNeeded);
        });
    }

    function updateRemainingHours(moduleId, hourQuotaNeeded, popup = false) {
        const total = parseInt($(`#intervention-hours-total-${moduleId}`).data('duration')) / 3600;
        const occupied = trainerList[moduleId]
            .filter(tr => chosenTrainerIds[moduleId].includes(tr.trainer.id))
            .reduce((prev, current) => prev + current.hours, 0);
        const remainingHours = total - occupied < 0 ? 0 : total - occupied;
        console.log(total, occupied, remainingHours, hourQuotaNeeded);
        const submitButton = $(`.intervention-online-trainers-submit[moduleId='${moduleId}']`);

        if (remainingHours === 0) {
            submitButton.prop('disabled', false);
        } else {
            submitButton.prop('disabled', true);
        }

        const remainingMinutes = Math.floor(remainingHours * 60);
        let remainingText = '';
        if (remainingMinutes > 60) {
            remainingText = Math.floor(remainingMinutes / 60) + 'h ';
        }
        remainingText = remainingText + Math.floor(remainingMinutes % 60) + 'm';

        if (occupied < hourQuotaNeeded && popup) {
            swal('Hours checked', 'Please assign more trainers to cover the remaining ' + remainingText + ' for this module.');
        }

        $(`#intervention-hours-remaining-${moduleId}`).empty().html(remainingText);
    }

    function getOnlineTrainerTemplate() {
        const template = $('#trainer-item-template').clone();
        template.css({display: 'grid'});
        template.attr('id', '');
        return template;
    }

    function getOtherBookingTemplate() {
        const template = $('#other-booking-item-template').clone();
        template.css({display: 'grid'});
        template.attr('id', '');
        return template;
    }

    function bindTrainerItem(item, data, moduleId, hourQuotaNeeded) {

        item.find('.trainer-name').empty().html(data.name);
        item.find('.trainer-item-hours').empty().html(data.hours);
        item.find('.hour-quota-needed').empty().html(hourQuotaNeeded);
        item.find('.trainer-date-from').empty().html(data.start);
        item.find('.trainer-date-to').empty().html(data.end);
        item.find('.dm-checkbox').attr('id', data.id + moduleId);
        item.find('.dm-label').attr('for', data.id + moduleId);

        if (data.booked) {
            item.find('.dm-checkbox').prop('checked', true);
            chosenTrainerIds[moduleId].push(data.id);
        }

        item.find('.dm-checkbox').change(function () {

            if ($(this).is(':checked')) chosenTrainerIds[moduleId].push(data.id);
            else chosenTrainerIds[moduleId].splice(chosenTrainerIds[moduleId].indexOf(data.id), 1);

            updateRemainingHours(moduleId, hourQuotaNeeded, true);
        });

        return item;
    }

    function bindOtherBookingItem(item, data, moduleId) {
        let sessionsData = moment(data.date).format('DD/MM/YYYY') + '<br />';
        let myVar = '';
        let sessionNumber = 0;
        data.sessions.forEach((session, key) => {
            if (key > 0 && key < data.sessions.length) {
                myVar += '<br />';
            }
            sessionNumber = key + 1;
            myVar += '<strong>Session ' + sessionNumber + '</strong>: ' + moment(session.start).tz(session.userTimeZone).format('HH:mm');
            if (typeof session.address === 'string' || session.address instanceof String) {
                myVar += '<br />' + session.address;
            }
            if (session.chosen_trainer) {
                myVar += '<br /><b>Chosen trainer: </b>' + session.chosen_trainer.first_name + ' ' + session.chosen_trainer.last_name;
            } else {
                myVar += '<br /><b>Chosen trainer: None</b>';
            }
            myVar += '<br />';
        });

        item.find('.booking-title').empty().html(sessionsData + myVar);
        item.find('.sessionBooking').attr("moduleId", moduleId);

        item.find('.sessionBooking').click(function () {
            $('#step4-vclass-modal').modal();
            updateVclassModal(otherBookingModalData[moduleId + '_' + data.date], moduleId);

        });
        return item;
    }

    function assignTrainersOnline(moduleId) {
        $.ajax({
            url: '/admin/trainers/assign/online/' + sessionId,
            method: 'POST',
            data: {
                trainersIds: chosenTrainerIds[moduleId],
                moduleId: moduleId,
            },
            success: function (response) {
                if(response.length > 0){
                    $('[data-days='+moduleId+']').removeClass('hidden');
                    let htmlSLot = '<ul>';
                    $.each(response, function( index, value ) {
                        htmlSLot += "<li>" + value + "</li>";
                    });
                    htmlSLot += '</ul>';
                    $('#daysSlot_'+moduleId).html(htmlSLot);
                    $('#viewSlots_'+moduleId).modal('show');
                }else {
                    swal('Trainers validated');
                }
            },
            error: function (response) {
                swal('Error', response.responseJSON, 'error');
            }
        })
    }

    $('.intervention-online-trainers-submit').off('click').on('click', function () {
        assignTrainersOnline($(this).attr('moduleId'));
    });

    function getOtherBooking(moduleId, minDate, maxDate) {

        updateModule(moduleId);
        $(`#collapse_${moduleId}`).removeClass('show');

        $('.fc-prev-button').on('click', function () {
            $('#collapse_' + moduleId).addClass('show');
        });

        $('.fc-next-button').on('click', function () {
            $('#collapse_' + moduleId).addClass('show');
        });
    }

    function updateModule(moduleId) {
        return new Promise((resolve, reject) => {
            $.ajax({
                'url': '/admin/trainers/available/module/' + sessionId + '?moduleId=' + moduleId,
                'method': 'GET',
            }).done(function (response) {
                if (response.done) markModuleDone(moduleId);
                let number_of_session = 0;
                const events = response.results.map(day => {
                    day.start = day.date;
                    day.end = day.date;
                    number_of_session += day.sessions.length;
                    const chosenCount = chosenTrainersCount(day.sessions);
                    day.title = `${chosenCount} / ${day.sessions.length}`;
                    day.color = chosenCount === day.sessions.length ? COLORS.blue : COLORS.green;
                    const template = getOtherBookingTemplate();
                    let bookingItem = bindOtherBookingItem(template.clone(), day, moduleId);
                    $(`#other-booking-${moduleId}`).append(bookingItem);
                    otherBookingModalData[moduleId + '_' + day.date] = day;
                });

                if (number_of_session) {
                    $(`#quantity_session_${moduleId}`).html(number_of_session);
                }
                resolve(events);
            });
        });
    }

    function createCalendar(moduleId, minDate, maxDate) {

        const selectorCalendar = '#' + moduleId;

        $(selectorCalendar).fullCalendar({
            fixedWeekCount: false,
            header: HEADER_OPTIONS,
            firstDay: 1,
            selectable: true,
            defaultView: 'month',
            slotLabelFormat: 'HH:mm',
            timeFormat: 'H:mm',
            minTime: '08:00:00',
            maxTime: '21:00:00',
            eventColor: COLORS.green,
            customButtons: {},
            validRange: {
                start: minDate,
                end: maxDate,
            },
            select: function (start, end, jsEvent, view) {
            },
            eventClick: function (calEvent, jsEvent, view) {

                $('#step4-vclass-modal').modal();
                vclassModalData = {};
                updateVclassModal(calEvent, moduleId);
            },
            dayRender: function (date, cell) {
            },
            viewRender: function (view, element) {

                updateCalendar(moduleId);
                $(`#collapse_${moduleId}`).removeClass('show');

                $('.fc-prev-button').on('click', function () {
                    $('#collapse_' + moduleId).addClass('show');
                });

                $('.fc-next-button').on('click', function () {
                    $('#collapse_' + moduleId).addClass('show');
                });
            }
        });
    }

    function updateCalendar(moduleId) {

        return new Promise((resolve, reject) => {

            const selectorCalendar = '#' + moduleId;

            $.ajax({
                'url': '/admin/trainers/available/module/' + sessionId + '?moduleId=' + moduleId,
                'method': 'GET',
            }).done(function (response) {

                if (response.done) markModuleDone(moduleId);

                const events = response.results.map(day => {
                    day.start = day.date;
                    day.end = day.date;

                    const chosenCount = chosenTrainersCount(day.sessions);
                    day.title = `${chosenCount} / ${day.sessions.length}`;
                    day.color = chosenCount === day.sessions.length ? COLORS.blue : COLORS.green;
                    return day;
                });

                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', events);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(events);
            });
        });
    }

    function assignTrainerVclass(moduleId, sessionHash, trainerId) {

        return new Promise((resolve, reject) => {
            $.ajax({
                url: '/admin/trainers/assign/session/' + sessionId,
                method: 'POST',
                data: {
                    trainerId,
                    moduleId,
                    sessionHash: sessionHash.toString()
                }
            }).done(function (response) {
                resolve();
            });
        });
    }

    function updateVclassModal(calEvent, moduleId) {
        let key = 1;
        const countText = `(${$('#chosen-text').html()} ${chosenTrainersCount(calEvent.sessions)} / ${calEvent.sessions.length})`;
        $('#assigned-trainer-counter').empty().html(countText);

        const sessionOptions = calEvent.sessions.map(session =>
            `<option value="${session.sessionId}">Session ${key++} - ${moment(session.start).tz(session.userTimeZone).format('DD/MM/YYYY')} - ${moment(session.start).tz(session.userTimeZone).format('HH:mm')}</i></option>`
        );

        $('#modal-session-select').empty().append(sessionOptions);
        $("#modal-session-select").selectpicker("refresh");

        if (vclassModalData.currentSession) {
            $('#modal-session-select').val(vclassModalData.currentSession);
        }

        function chooseSession(id) {
            const session = calEvent.sessions.find(session => session.sessionId == id);
            $("#modal-session-select").selectpicker("refresh");
            loadVclassTrainers(session, moduleId, calEvent.date);
        }

        $('#modal-session-select').off('change').on('change', function () {
            console.log($(this).val())
            chooseSession($(this).val());
        });
        chooseSession($('#modal-session-select').val());
    }

    function getAvatarTrainer(avatar) {
        let srcAvatar = '/img/default-avatar.png'
        if (avatar) {
            srcAvatar = avatar;
        }
        return '<img style=" border-radius: 50%;"  width="40" height="40" src="' + srcAvatar + '">';
    }

    function getRankingTrainer(ranking) {
        let ratingHtml = '';
        for (let grade = 1; grade <= 5; grade++) {
            if (grade <= ranking) {
                ratingHtml += '<i class="fas fa-star"></i>';
            } else {
                ratingHtml += '<i class="fal fa-star"></i>';
            }

        }
        return ratingHtml;
    }

    function loadVclassTrainers(session, moduleId, date) {

        let trainersList = session.trainers;
        let trainerOptions = [];
        let city = '';
        if (session.chosen_trainer) {
            //exclude current selected trainer
            let trainer = session.chosen_trainer;
            let avatar = getAvatarTrainer(trainer.avatar);
            let ratingHtml = getRankingTrainer(trainer.ranking);
            if (typeof trainer.city !== 'undefined') {
                city = trainer.city;
            }

            trainerOptions.push(`<div class="form-check">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios${trainer.id}" value="${trainer.id}" checked>
              <label style="width: 100%" for="exampleRadios${trainer.id}" class="form-check-label col-md-12">
                  <div class="user row ">
                         <div class="photo col-md-2" >
                            ${avatar}
                        </div>
                        <div class="user-info col-md-10">
                            ${trainer.first_name} ${trainer.last_name} ${city}<br>
                            <div class="ratings-widget disabled"> ${ratingHtml} </div>
                        </div>
                    </div>
              </label>
            </div>`);
        }

        for (let index = 0; index < trainersList.length; index++) {
            let trainer = trainersList[index];
            let avatar = getAvatarTrainer(trainer.avatar);
            let ratingHtml = getRankingTrainer(trainer.ranking);

            trainerOptions.push(`<div class="form-check">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios${trainer.id}" value="${trainer.id}">
              <label style="width: 100%" for="exampleRadios${trainer.id}" class="form-check-label col-md-12">
                  <div class="user row ">
                         <div class="photo col-md-2" >
                            ${avatar}
                        </div>
                        <div class="user-info col-md-10">
                            ${trainer.first_name} ${trainer.last_name} ${city}<br>
                            <div class="ratings-widget disabled"> ${ratingHtml} </div>
                        </div>
                    </div>
              </label>
            </div>`);
        }

        if (trainersList.length < 1)
            trainerOptions.unshift($('#no-trainers-session'));


        $('#modal-vclass-trainer-select').empty().append(trainerOptions);

        //mark selected trainer if it exist
        if (session.chosen_trainer) {
            $('#exampleRadios' + session.chosen_trainer.id).val(session.chosen_trainer.id);
        }

        //save trainer each time selected
        $('.form-check-input').off('change').on('click', function () {

            vclassModalData.currentSession = session.sessionId;

            if (trainersList.length > 0) {
                assignTrainerVclass(moduleId, session.sessionId, $(this).val())
                    .then(() => updateCalendar(moduleId))
                    .then(events => {
                        const event = events.find(ev => ev.date === date);
                        updateVclassModal(event, moduleId);
                    })
                    .then(() => {
                        swal('Success', 'You have assigned the trainer successful!', 'success');
                        location.reload();
                    })

            }
        });
    }

    function markModuleDone(moduleId) {
        $('#heading_' + moduleId).css({background: COLORS.green});
        $('#assign-trainers-' + moduleId).hide();
    }

    function chosenTrainersCount(sessions) {
        return sessions.filter(session => !!session.chosen_trainer).length;
    }
});

