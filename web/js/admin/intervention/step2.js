$(document).ready(function () {
    bindFileUploaders();
    bindShowFiles();
    bindSessionManagement();
    bindDatepickers();
    bindModulesSelect();
    bindOnSubmitForm();
});


function bindOnSubmitForm(){

    $('#step2-submit').click(async function() {
        let isValid = true;
        let hasPopup = false;
        $('.isCompleted').each(function(i, v) {
            let currentElement = $(this);
            let elementName = currentElement.attr('name');
            let elementId = currentElement.attr('id');
            let sessionId = elementId.split(/_(.+)/)[1];
            let expandElement = $('[aria-controls=sessions_'+sessionId+']');
            if(expandElement.attr('aria-expanded') === 'false'){
                expandElement.trigger('click');
            }
            // do not allow checked NO for all conditions isCompleted, scoreToPass, timeSpentCheck
            let scoreToPassElement = elementName.replace("isCompleted", "scoreToPass");
            let timeSpentCheckElement = elementName.replace("isCompleted", "timeSpentCheck");
            if(currentElement.is(':checked') && currentElement.val() == 0){
                if($('[name="'+scoreToPassElement+'"]:checked').val() == 0 && $('[name="'+timeSpentCheckElement+'"]:checked').val() == 0){
                    swal('Warning', $('#condition-all-no-warning').text(), 'warning').then(result => {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: currentElement.offset().top - 30
                            }, 2000);
                        }
                    );
                    isValid = false
                    hasPopup = true
                }
            }
            if (!currentElement.is(':checked') && !$('[name="'+scoreToPassElement+'"]').is(':checked') && !$('[name="'+timeSpentCheckElement+'"]').is(':checked')){
                swal('Warning', $('#condition-all-no-warning').text(), 'warning').then(result => {
                        $([document.documentElement, document.body]).animate({
                            scrollTop: currentElement.offset().top - 30
                        }, 2000);
                    }
                );
                hasPopup = true
                isValid = false;
            }
        });

        const urlPromises = [];
        // URL validation
        $('.autocompleteUrl').each(async function(i, v) {
            let currElement = $(this);
            let validationValue = currElement.attr('data-validation');
            if (validationValue == 0) {
                let moduleId = $(this).attr("data-id")
                urlPromises.push($.ajax({
                    url: '/admin/courses/api/embed-checking',
                    type: 'get',
                    dataType: "json",
                    data: {
                        url: $('#url_' + moduleId).val()
                    },
                }).then(data => {
                    if (data.status === 1) {
                        console.log('#url_' + moduleId)
                        $('#url_' + moduleId).attr('data-validation' , 1);
                        $('#invalidUrl_' + moduleId).removeClass('color-red');
                        $('#messageEmbed_' + moduleId).addClass('d-none');
                    } else if (data.status === 0) {
                        $('#url_' + moduleId).attr('data-validation' , 0);
                        $('#invalidUrl_' + moduleId).removeClass('color-red');
                        $('#messageEmbed_' + moduleId).removeClass('d-none');
                    } else if (data.status === -1) {
                        $('#url_' + moduleId).attr('data-validation' , 0);
                        $('#invalidUrl_' + moduleId).addClass('color-red');
                        $('#messageEmbed_' + moduleId).addClass('d-none');
                    }
                    if (data.status === -1) {
                        isValid = false;
                        return false
                    }
                }));
            }
        });

        await Promise.all(urlPromises);
        const correctionDurationMax = $("input[id^='correctionDurationMax']").val();
        if (correctionDurationMax == "") {
            isValid = false;
            swal('Warning', $('#correction-deadline').text(), 'warning').then(result => {
                /*$([document.documentElement, document.body]).animate({
                    scrollTop: currElement.offset().top - 30
                }, 2000);*/
            });
            hasPopup = true;
        }

        console.log(isValid)
        
        if (!isValid && !hasPopup) {
            swal('Warning', $('#url-warning').text(), 'warning').then(result => {
                    /*$([document.documentElement, document.body]).animate({
                        scrollTop: currElement.offset().top - 30
                    }, 2000);*/
                }
            );
        }

        hasPopup = false;

        // after show element we check html5 validation
        // because validation of html5 can not run with hidden elements
        if(isValid) {
            $('[name=form_2]')[0].reportValidity();
            if ($('[name=form_2]')[0].checkValidity()) {
                $('[name=form_2]').submit();
            }
        }

    });
}
function bindModulesSelect(){
    var modulesSelected = [];
    $(document).on('focus', '[data-select-module]', function () {
        $(this).data('pre', $(this).val());
        modulesSelected = [];
        $('[data-select-module]').each(function () {
            if(this.value) {
                modulesSelected.push(this.value);
            }
        })
    });
    $(document).on('change', '[data-select-module]', function() {
        var before_change = $(this).data('pre');//get the pre data
        var $this = this;
        modulesSelected.forEach(function(moduleId) {
            if(moduleId === $this.value && $this.value !== before_change){
                swal('', $('#module-picked').text(), 'warning');
                $this.value = before_change;
            }
        });
        $(this).data('pre', $(this).val());//update the pre data
        $(this).blur();
    });
}

function bindFileUploaders() {

    const uploadOptions = {
        formData: function (data, element) {

            let triggerField = element.fileInput[0];
            let moduleId = $(triggerField).attr('data-module-id');
            let fileFor = $(triggerField).attr('data-person-type');

            return [{name: 'moduleId', value: moduleId}, {name: 'role', value: fileFor}];
        },
        isFileTrainerReportTemplate: 1

    };

    const uploadOption = {
        formData: function (data, element) {

            let triggerField = element.fileInput[0];
            let moduleId = $(triggerField).attr('data-module-id');
            let fileFor = $(triggerField).attr('data-person-type');

            return [{name: 'moduleId', value: moduleId}, {name: 'role', value: fileFor}];
        }
    };


    initFileUploader($('.inputUploadPublicFile'), '/admin/courses/file_upload/media', uploadOptions);
    initFileUploader($('.inputUploadFile'), '/admin/courses/file_upload', uploadOption);
}

function bindShowFiles() {

    function showFiles(moduleId, role) {
        let lang = $('#locate').val();
        let default_lang = $('#default_locale').val();
        lang = (lang == default_lang) ? '' : '/' + lang;
        const url = role ? lang + '/admin/courses/file_upload/get_view' : lang + '/admin/courses/get/files/all/module/' + moduleId;
        const data = role ? {data: {module_id: moduleId, person_role: role}} : undefined;
        const method = role ? 'POST' : 'GET';
        $.ajax({
            url,
            method,
            data,
            success: function (data) {

                const listModal = $('.uploaded-files-list');
                listModal.html(data);
                listModal.modal({ show: true, toggle: true });
                bindDeleteFiles();
            },
            error: function (data) {

                const headerText = $('#no-files-text').text();
                const commentText = $('#try-uploading-text').text();
                swal(headerText, commentText, "info", { button: "Ok" });
            },
            beforeSend: function () {
                $('.uploaded-files-list').modal('hide').data('bs.modal', null);
            }
        });
    }

    $('.show-files > .pointer, .file-icons > i').on('click', function () {
        showFiles($(this).data('module-id'), $(this).data('person-type'));
    });

    $('.btnListingUpload').on('click', function () {
       showFiles($(this).data('module-id'));
    });

}

function bindDeleteFiles() {

    $('.uploaded-files-list').find('.fa-minus-circle').off('click').on('click', function () {

        const el = $(this);
        const destinationUrl = el.data('reference-path');

        $.ajax({
            url: '/admin/courses/file_upload/delete' + destinationUrl,
            method: "DELETE",
            success: function (data) {
                $(el).closest('tr').remove();
                swal($('#success-text').text(), $('#file-removed-text').text(), "success", { button: "Ok" });
            },
            error: function (data) {
                swal($('#failure-text').text(), $('#cant-remove-text').text(), "error", { button: "Ok" });
            },
            beforeSend: function () {
                $('.uploaded-files-list').modal('hide').data('bs.modal', null);
            }
        })
    });
}

function bindSessionManagement() {

    $('.btn-add-session').off('click').on('click', function () {
       addSession(this);
    });

    $('.btn-add-session-assessment').off('click').on('click', function () {
        addSessionAssessment(this);
    });

    $('.btn-remove-session').off('click').on('click', function () {
       removeSession(this);
    });
}

function bindDatepickers() {

    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0, 6],
        icons: {
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right'
        }
    });


    $('.timepicker').datetimepicker({
        format: 'HH:mm',
        icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
        }
    });
}


function addSessionAssessment(triggerEl) {

    const toClone = $(triggerEl).closest('.card');
    const cloned = toClone.clone();
    const sessionsCount = getSessionCount(triggerEl);
    $("#numberCondition").text(sessionsCount+1);
    let idIcon = '';
    let idCollapse = '';
    cloned.find('input').each(function () {
        $(this).attr('name', $(this).attr('name').replace(/\[session\]\[\d+?\]/, `[session][${sessionsCount}]`));
        $(this).attr('id', $(this).attr('id') + `_${sessionsCount}`);
        $(this).attr('session-value', `${sessionsCount}`);
    });

    cloned.find('[class*=operation-one-block]').each(function(){
        $(this).removeClass(function (index, css) {
            return (css.match (/\boperation-one-block-\S+/g) || []).join(' '); // removes anything that starts with "operation-one-block-"
        });
        $(this).addClass('operation-one-block-'+ `${sessionsCount}`);
        $(this).find('input.operationOne').prop('checked',false);
    });

    cloned.find('[class*=operation-two-block]').each(function(){
        $(this).removeClass(function (index, css) {
            return (css.match (/\boperation-two-block-\S+/g) || []).join(' '); // removes anything that starts with "operation-two-block-"
        });
        $(this).addClass('operation-two-block-'+ `${sessionsCount}`);
        $(this).find('input.operationTwo').prop('checked',false);
    });

    cloned.find('[class*=time-spent-number]').each(function(){
        $(this).removeClass(function (index, css) {
            return (css.match (/\btime-spent-number-\S+/g) || []).join(' '); // removes anything that starts with "time-spent-number"
        });
        $(this).addClass('time-spent-number-'+ `${sessionsCount}`);
        $(this).val(1);
    });

    cloned.find('[class*=isCompleted]').each(function(){
       $(this).prop('checked',false);
    });

    cloned.find('[class*=scoreToPass]').each(function(){
        $(this).prop('checked',false);
    });

    cloned.find('[class*=timeSpent').each(function(){
        $(this).prop('checked',false);
    });

    cloned.find('[id^=weightNumber]').val(1);

    cloned.find('[data-select-module]').prop("selectedIndex", 0)

    cloned.find('select').each(function () {
        $(this).attr('name', $(this).attr('name').replace(/\[session\]\[\d+?\]/, `[session][${sessionsCount}]`));
        $(this).attr('id', $(this).attr('id') + `_${sessionsCount}`);
    });
    var numSession = sessionsCount+1;
    cloned.find('b').each(function() {
        $(this).text(numSession);
    })
    cloned.find('label').each(function() {
        $(this).attr('for', $(this).attr('for') + `_${sessionsCount}`);
    });
    cloned.find('.collapse').each(function() {
        idCollapse = $(this).attr('id') + `_${sessionsCount}`;
        $(this).attr('id', idCollapse);
    });
    cloned.find('i').each(function() {
        idIcon = $(this).attr('id') + `_${sessionsCount}`;
        $(this).attr('id',idIcon );
    });
    cloned.find('a').each(function() {
        $(this).attr('href', $(this).attr('href') + `_${sessionsCount}`);
        $(this).attr('aria-controls', $(this).attr('aria-controls') + `_${sessionsCount}`);
    });
    cloned.find('[data-reference="unique-session-identifier"]').val(new Date().getTime() + Math.floor(Math.random() * 100));
    cloned.find('script').each(function() {
        $(this).remove();
    });
    var scriptAutoComplete = '<script>'+
        "$('#"+idCollapse+"').on('hidden.bs.collapse', function () { " +
            "$('#"+idIcon+"').addClass('fa-angle-down');"+
            "$('#"+idIcon+"').removeClass('fa-angle-up');"+
        "});"+

    "$('#"+idCollapse+"').on('shown.bs.collapse', function () {"+
        "$('#"+idIcon+"').removeClass('fa-angle-down');"+
        "$('#"+idIcon+"').addClass('fa-angle-up');"+
    "});";
        '</script>';
    toClone.parent().append(cloned);
    toClone.parent().append(scriptAutoComplete);

    // hide elements
    $('.operation-one-block-'+`${sessionsCount}`).hide();
    $('.operation-two-block-'+`${sessionsCount}`).hide();
    $('.time-spent-number-'+`${sessionsCount}`).hide();
    bindSessionManagement();
    bindDatepickers();
}

function addSession(triggerEl) {

    const toClone = $(triggerEl).closest('.card');
    const cloned = toClone.clone();
    const sessionsCount = getSessionCount(triggerEl);
    var idString = '';
    var idValueString = '';
    var idValueHiddenString = '';
    cloned.find('input').each(function () {
        $(this).attr('name', $(this).attr('name').replace(/\[session\]\[\d+?\]/, `[session][${sessionsCount}]`));
        $(this).attr('id', $(this).attr('id') + `_${sessionsCount}`);
        if($(this).attr('data-id-string') != undefined){
            idString = $(this).attr('id');
        }
        if($(this).attr('data-id-value-string') != undefined){
            idValueString = $(this).attr('id');
        }
        if($(this).attr('data-id-value-hidden-string') != undefined){
            idValueHiddenString = $(this).attr('id');
        }
    });
    var numSession = sessionsCount+1;
    cloned.find('b').each(function() {
        $(this).text(numSession);
    })
    cloned.find('label').each(function() {
        $(this).attr('for', $(this).attr('for') + `_${sessionsCount}`);
    });
    cloned.find('script').each(function() {
        $(this).remove();
    });

    cloned.find('[data-reference="unique-session-identifier"]').val(new Date().getTime() + Math.floor(Math.random() * 100));
    var idUnique = $('#uniqueIdNumber').val();
    var scriptAutoComplete = '<script>'+
        'var placesAutocomplete = places({'+
        'appId: "plCXL3O26KYC",'+
        'apiKey: "dfbcd0ff009d87b963b11e5abbdb2d9d",'+
        'container: document.querySelector("#'+ idString + '")'+
        '});'+
        'var address = document.querySelector("#'+ idValueString + '");'+
        'placesAutocomplete.on("change", function(e) {'+
        '    $("#'+ idValueString + '").val(e.suggestion.value);'+
        '    $("#'+ idValueHiddenString + '").val(e.suggestion.administrative);'+

        '});'+

        'placesAutocomplete.on("clear", function() {'+
        '   address.value = "none";'+
        '});'+
        '</script>';
    toClone.parent().append(cloned);
    // need to check for each module
    toClone.parent().append(scriptAutoComplete);
    bindSessionManagement();
    bindDatepickers();
}

function removeSession(triggerEl) {
    if (getSessionCount(triggerEl) === 1) return;
    $(triggerEl).closest('.card').remove();
}

function getSessionCount(triggerEl) {
    return $(triggerEl).closest('.card').parent().closest('.card').find('[data-reference="unique-session-identifier"]').length;
}

