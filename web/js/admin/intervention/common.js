$(document).ready(function () {
    bindNavigation();
    bindSaveButton();
});

function bindNavigation() {

    $('.pills-link').on('click', function () {

        const targetStep = parseInt($(this).data('step')) - 1;

        saveStep(function() {
            goToStep(targetStep);
        });
    });
}

function bindSaveButton() {
    $('#save_unpublished').on('click', saveDraft);
}

function saveDraft() {
    const sessionId = $('[name="sessionId"]').val();
    const form = $("form[name^='form_']");
    $(form).attr("action", "/admin/courses/save/step/session/" + sessionId).submit();
}

function saveStep(callback) {

    const form = $('form[data-role="intervention-form"]');
    const currentStep = parseInt($('[name="step"]').val());
    const sessionId = $('[name="sessionId"]').val();

    $.ajax({
        url: '/admin/courses/api/step',
        method: "POST",
        data: {current: currentStep, session_id: sessionId, data: form.serialize()},
        success: callback
    });
}

function nextStep() {

    const nextStep = parseInt($('[name="step"]').val());

    saveStep(function() {
        goToStep(nextStep);
    });
}

function goToStep(step) {
    const newStep = window.location.href.includes('session') ? `step/${step}` : `step/${step}/session`;
    window.location.href = window.location.href.replace(/step\/\d/, newStep);
}

