var rowClick = $('.price-row').length;
var chapterRowClick = $('.chapter-row').length;

$(document).ready(function () {
    bindAddModuleButtons();
    bindModuleRows();
    bindDateValidation();

    $('#logo-upload').on('change', function () {
        readURL(this);
    });
    bindUploadIcon();

    //add chapter
    $('.btn-add-chapter').off('click').on('click', function () {
        rowClick++;
        chapterRowClick++;
        const moduleDetails = {
            step: 1,
            app_id: $(this).attr('data-id'),
            color: $(this).attr('data-color'),
            title: $(this).attr('data-title'),
            icon: $(this).attr('data-icon'),
            width: $(this).attr('data-width'),
            height: $(this).attr('data-height'),
            quantity: 0,
            duration: 0,
            unitPrice: 0,
            totalPrice:0,
            time_param: 0,
            index: rowClick
        };
        fetchModule(this, rowClick, chapterRowClick);
    });
});

function bindAddModuleButtons() {

    $('.btn-addmod').off('click').on('click', function () {
        rowClick++;
        const moduleDetails = {
            step: 1,
            app_id: $(this).attr('data-id'),
            color: $(this).attr('data-color'),
            title: $(this).attr('data-title'),
            icon: $(this).attr('data-icon'),
            width: $(this).attr('data-width'),
            height: $(this).attr('data-height'),
            quantity: 0,
            duration: 0,
            unitPrice: 0,
            totalPrice:0,
            time_param: 0,
            index: rowClick
        };
        fetchModule(this, rowClick, chapterRowClick);
    });
}

function bindModuleRows() {

    bindSelectpicker();
    bindDeleteModuleButtons();
    bindRowsSorting();

    $('.sortable-rows').children().each(function () {
        bindPriceInputs($(this));
    })
}

function bindDeleteModuleButtons() {

    $('.row-delete-btn').off('click').on('click', function () {
        let row = $(this).attr('data-row');
        $(this).parent().parent().remove();
        let e = "#row"+row;

        $('#quotationTable').DataTable().destroy();
        var table = $('#quotationTable').DataTable({
            dom: 'Bfrtip',
            "paging": false,
            "ordering": true,
            "order": [[ 0, "asc" ]],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    title: 'Quotation',
                    footer: true
                }, {
                    extend: 'excel',
                    title: 'Quotation',
                    footer: true
                }]
        });
        table.row( $(e) )
            .remove()
            .draw();

        $('.sortable-rows').children().each(function (key) {
            bindPriceInputs($(this));
            $(this).attr('data-order', key);
            let id =  $(this).attr('data-row');
            $("#col"+id).text(key+1);
        })

        $('#quotationTable').DataTable().destroy();
        var table = $('#quotationTable').DataTable({
            dom: 'Bfrtip',
            "paging": false,
            "ordering": true,
            "order": [[ 0, "asc" ]],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    title: 'Quotation',
                    footer: true
                }, {
                    extend: 'excel',
                    title: 'Quotation',
                    footer: true
                }]
        });
        table
            .rows()
            .invalidate()
            .draw();

        reOrderNumberModuleRow();
    });
}

function bindSelectpicker() {
    $('.selectpicker').selectpicker();
}

function bindRowsSorting() {

    $('.sortable-rows').sortable({
        axis : 'y',
        stop: function( event, ui ) {
            // console.log("Moved to new position: " + ui.item.index());
            $('.sortable-rows').children().each(function (key) {
                $(this).attr('data-order', key);
                let id =  $(this).attr('data-row');
                $("#col"+id).text(key+1);
            })
            $('#quotationTable').DataTable().destroy();
            var table = $('#quotationTable').DataTable({
                dom: 'Bfrtip',
                "paging": false,
                "ordering": true,
                "order": [[ 0, "asc" ]],
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        title: 'Quotation',
                        footer: true
                    }, {
                        extend: 'excel',
                        title: 'Quotation',
                        footer: true
                    }]
            });
            table
                .rows()
                .invalidate()
                .draw();
        }
    });
    $('.sortable-rows').disableSelection();

    $('.sortable-cards').sortable();
    $('.sortable-cards').disableSelection();
}

function bindDateValidation() {

    let today = moment(new Date());
    today.add(1, 'days');
    $('#startDate').data('DateTimePicker').defaultDate(moment().millisecond(0).second(0).minute(0).hour(0));
    $('#startDate').data('DateTimePicker').minDate(moment().millisecond(0).second(0).minute(0).hour(0));
    $('#endDate').data('DateTimePicker').minDate(today);

    $('#startDate').datetimepicker().on('dp.change', function (e) {
        let incrementDay = moment(new Date(e.date));
        incrementDay.add(1, 'days');
        $('#endDate').data('DateTimePicker').minDate(incrementDay);
        $(this).data("DateTimePicker").hide();
    });
}

function bindUploadIcon() {

    const uploadOptions = {
        success: function (data) {
            $('#file-name').val(data.result.fileName);
            $('#logo-table-name').text(data.result.fileName);
            $('#logo-path').val(data.result.filePath);
            // $('#logoPreview').attr('src', data.result.filePath);
            // $('#logoPreview').show();
            $('#logo-file-icon').removeClass('hidden');
        },
        failure: function (data) {
            $('#logoFileIcon').addClass('hidden');
        }
    };

    initFileUploader($('#logo-upload'), '/admin/courses/logo_upload', uploadOptions);

    $('#logo-file-icon').on('click', function () {
        $('.listDataFileAlreadyUploadModalNew').modal({
            show: true,
            toggle: true
        });
    });
}

function fetchModule(moduleDetailsNode,rowClick, chapterRowClick) {
    const moduleDetails = {
        rowClick: rowClick,
        step: 1,
        app_id: $(moduleDetailsNode).attr('data-id'),
        color: $(moduleDetailsNode).attr('data-color'),
        title: $(moduleDetailsNode).attr('data-title'),
        icon: $(moduleDetailsNode).attr('data-icon'),
        width: $(moduleDetailsNode).attr('data-width'),
        height: $(moduleDetailsNode).attr('data-height'),
        quantity: 0,
        index: $(moduleDetailsNode).attr('data-id') != 12 ? rowClick : chapterRowClick
    };
    let locate = $('#locate').val();
    let default_locate = $('#default_locale').val();
    locate = (locate == default_locate) ? '' : '/' + locate;
    $.ajax({
        url: locate + '/admin/courses/get_content',
        data: {module_detail: moduleDetails},
        method: "POST",
        success: addModuleRow,
        error: function (data) {
            console.log('Error adding module ' + data);
        }
    })
}

function addModuleRow(data) {
    $('.sortable-rows').append(data);
    const node = $('.sortable-rows').children().last();
    bindPriceInputs(node);
    bindSelectpicker();
    bindDeleteModuleButtons();
    reOrderNumberModuleRow();
}

function reOrderNumberModuleRow(){
    $('.sequenceNumber').each(function (k){
        $(this).html(k + 1);
    })
}

function bindPriceInputs(moduleNode) {
    const totalInput = moduleNode.find('.totalField');
    const priceInput = moduleNode.find('.priceField');
    const quantityInput = moduleNode.find('.qtyField');

    function updateRowTotal() {
        totalInput.val(parseInt(quantityInput.val()) * parseFloat(priceInput.val()));
        updateTotalBudget();

    }

    priceInput.off('keyup').on('keyup', updateRowTotal);
    quantityInput.off('keyup').on('keyup', updateRowTotal);
    updateRowTotal();
    bindGenerateQuotation();

}

function updateTotalBudget() {

    let total = 0;

    $('.totalField').each(function () {
        total += parseFloat($(this).val());
    });

    $('.budgetPrice').text(total + ' €');
    $('.totalPriceCourse').text(total+ ' €');
    $(".perPriceCourse").text(total % $("#nbParticipant").val());
}

function bindGenerateQuotation() {

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#logoPreview').css("background-image", "url("+e.target.result+")");
        }

        reader.readAsDataURL(input.files[0]);
    }
}