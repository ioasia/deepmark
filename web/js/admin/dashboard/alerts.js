var alert_functions = {
    clearRow: function(object){
        $(object).closest('tr').remove();
    },

    makeSimpleGetRequest: function(url, data) {
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
        })
    },

    decreaseActionCountPlaning: function ($data = 'action_count_planning') {
        $('[data-role="' + $data + '"]').each(
            function(){
                let counter = $(this);
                let currentNumber = parseInt(counter.html());
                counter.html(currentNumber-1);
            }
        );
    },
}

$(document).ready(function () {
    $('[data-action="replace_status_change"]').click(function(){
        let url = $(this).data('url');
        let data = []
        $('[data-action="replacement-checkbox"]').each(function() {
            if(this.checked) {
                data.push($(this).val());
                alert_functions.clearRow(this);
                alert_functions.decreaseActionCountPlaning('action_count_planning_replacement');
            }
        });
        if(data.length > 0) {
            alert_functions.makeSimpleGetRequest(url, {data: data});
        }else{
            swal('Warning', 'Select at least one request', 'warning');
        }
    });

    $('[data-action="skill_status_change"]').click(function(){
        let url = $(this).data('url');
        let data = []
        $('[data-action="checkbox"]').each(function() {
            if(this.checked) {
                data.push($(this).val());
                alert_functions.clearRow(this);
                alert_functions.decreaseActionCountPlaning();
            }
        });

        alert_functions.makeSimpleGetRequest(url, {data: data});
    });

    $('[data-action="validated_report_change"]').click(function(){
        let url = $(this).data('url');
        let data = []
        $('[data-action="reports_un_validated"]').each(function() {
            if(this.checked) {
                data.push($(this).val());
                alert_functions.clearRow(this);
            }
        });

        alert_functions.makeSimpleGetRequest(url, {data: data});
    });

    $('[data-action="rejected_report_change"]').click(function(){
        let url = $(this).data('url');
        let data = []
        $('[data-action="reports_un_validated"]').each(function() {
            if(this.checked) {
                data.push($(this).val());
                alert_functions.clearRow(this);
            }
        });

        alert_functions.makeSimpleGetRequest(url, {data: data});
    });

    $('.re-assign-trainer').click(function(){
        $('#online-calendar-modal').modal();

        let moduleId = $(this).attr('data-module-id');
        let trainerId = $(this).attr('data-module-trainer-id');
        let currentDate = $(this).attr('data-booking-date');

        $('#modal-title').text($(this).attr('data-module-type'));
        $('#modal-booked-status').text($(this).attr('data-booking-status'));
        $('#modal-start-date').text($(this).attr('data-booking-duration-title'));
        $('#modal-duration').text($(this).attr('data-booking-duration'));
        $('#modal-trainer-display-name').text($(this).attr('data-module-trainer'));
        $('#modal-mobile-phone').text($(this).attr('data-module-mobile'));
        $('#modal-other-email').text($(this).attr('data-module-email'));
        $('#modal-module-id').val($(this).attr('data-module-id'));
        $('#modal-booking-id').val($(this).attr('data-booking-id'));

        $('#modal-book-online-session').removeClass('hidden');
        $('#modal-book-online-assign').addClass('hidden');
        $('#modal-cancel-booking').addClass('hidden');

        $('#modal-book-online-session').off('click').click(function () {
            getAvailableTrainers(moduleId, trainerId, currentDate);
        });
    });

    function getAvailableTrainers(moduleId, trainerId, currentDate) {
        let url = '/admin/trainers/available/module/' + moduleId + '/' + trainerId;
        let trainerOptions = '', hasTrainer = false;
        $.ajax({
            'url': url,
            'method': 'GET',
        }).done(function (response) {
            let selectList = "<select name='trainers' id='trainers' class='form-control'>";
            response.results.forEach((data) => {
                if (data.date) {
                    if (data.date == currentDate) {
                        data.sessions.forEach((session) => {
                            session.trainers.forEach((trainer) => {
                                selectList += '<option value="' + trainer.id + '">' + trainer.first_name + ' ' + trainer.last_name + '</option>';
                                hasTrainer = true;
                            });
                        });
                    }
                } else {
                    selectList += '<option value="' + data.trainer.id + '">' + data.trainer.first_name + ' ' + data.trainer.last_name + '</option>';
                    hasTrainer = true;
                }
            });
            selectList += "</select>";
            $('#modal-trainer-display-name').html(selectList);
            $('#modal-book-online-session').addClass('hidden');
            $('#modal-book-online-assign').removeClass('hidden');
            if (!hasTrainer) {
                $('#modal-cancel-booking').removeClass('hidden');
                $('#modal-book-online-assign').addClass('hidden');
            }
        });
    }

    $('#modal-book-online-assign').on('click', function () {
        let trainerId = $('#trainers').val();
        let bookingId = $('#modal-booking-id').val();

        $.ajax({
            'url': '/admin/trainers/assign/trainer',
            'method': 'POST',
            'data': {
                'bookingId': bookingId,
                'profileId': trainerId,
                'replacement': 1
            }
        }).done(function (response) {
            swal({
                title: "Success",
                text: "Re-assign the trainer has successfully!",
                type: "success",
                confirmButtonClass: "btn-primary",
                confirmButtonText: "OK",
                allowOutsideClick: false
            }).then(function() {
                location.reload();
            })
        }).fail(function (response) {
            swal('Warning', response.responseJSON.titre);
            console.log(response);
        })
    });
});