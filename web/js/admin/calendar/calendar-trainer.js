const GREEN = 'rgba(34,170,115,0.8)';
const CALENDAR = $('#trainerPlanningCalendar');
const HEADER_OPTIONS = {
    left: '',
    center: 'prev,title,next',
    right: 'addAvailability, removeAvailability'
};

$(document).ready(function () {
    let isAdding = false;
    let isDeleting = false;
    let trainerId = $('#trainerId').val();
    let start_date = null;
    let end_date = null;

    $('#trainerPlanningCalendar').fullCalendar({
        header: HEADER_OPTIONS,
        selectable: true,
        eventLimit: 4,
        firstDay: 1,
        contentHeight: 'auto',
        defaultView: 'month',
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '21:00:00',
        fixedWeekCount: false,
        eventColor: GREEN,
        customButtons: {
            addAvailability: {
                text: 'Add availability',
                click: function () {
                    CALENDAR.removeClass('cursor-clear');
                    isDeleting = false;

                    if (isAdding) {
                        isAdding = false;
                        $(this).removeClass('fc-state-active');
                        CALENDAR.removeClass('cursor-add');
                    } else {
                        isAdding = true;
                        $(this).addClass('fc-state-active');
                        CALENDAR.addClass('cursor-add');
                    }
                    $('.fc-removeAvailability-button').removeClass('fc-state-active');
                }
            },
            removeAvailability: {
                text: 'Remove availability',
                click: function () {
                    CALENDAR.removeClass('cursor-add');
                    isAdding = false;
                    if (isDeleting) {
                        isDeleting = false;
                        $(this).removeClass('fc-state-active');
                        CALENDAR.removeClass('cursor-clear');
                    } else {
                        isDeleting = true;
                        $(this).addClass('fc-state-active');
                        CALENDAR.addClass('cursor-clear');
                    }
                    $('.fc-addAvailability-button').removeClass('fc-state-active');
                }
            }
        },
        select: function (start, end, jsEvent, view) {
            if (isAdding && view.name === 'month') {
                jQuery('#addAvailabilityModal .modal-body .errors').empty();
                $('#addAvailabilityModal').modal('show');
                $('#addAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    let startTime = $('#startTimeModal').val() + ':00',
                        endTime = $('#endTimeModal').val() + ':00';
                    $.ajax({
                        'url': '/trainer/planning/calendars',
                        'method': 'POST',
                        'data': {
                            'beginning': start.format(),
                            'ending': end.format(),
                            'startTime': startTime,
                            'endTime': endTime,
                            'profileId': trainerId
                        }
                    }).done(function (response) {
                        $('#addAvailabilityModal').modal('hide');
                        updateCalendar(view.start.format(), view.end.format());
                    }).fail(function (response) {
                        jQuery('#addAvailabilityModal .modal-body .errors').empty().append('<div class="alert alert-danger" role="alert">' + response.responseJSON.message + '</div>');
                    })
                })
            } else if (isDeleting && view.name === 'month') {

                $('#removeAvailabilityModal').modal('show');
                $('#removeAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    let startTime = $('#startTimeModal').val() + ':00',
                        endTime = $('#endTimeModal').val() + ':00';
                    $.ajax({
                        'url': '/trainer/planning/calendars',
                        'method': 'DELETE',
                        'data': {
                            'beginning': start.format(),
                            'ending': end.format(),
                            'startTime': startTime,
                            'endTime': endTime,
                            'profileId': trainerId
                        }
                    }).done(function (response) {
                        console.log(response);
                        $('#removeAvailabilityModal').modal('hide');
                        updateCalendar(view.start.format(), view.end.format());
                    }).fail(function (response) {
                        console.log(response);
                    });
                })
            } else if (isAdding) {
                $.ajax({
                    'url': '/trainer/planning/calendars',
                    'method': 'POST',
                    'data': {
                        'beginning': start.format(),
                        'ending': end.format(),
                        'profileId': trainerId
                    }
                }).done(function (response) {
                    console.log(start.format(), end.format());
                    updateCalendar(view.start.format(), view.end.format());
                }).fail(function (response) {
                    console.log('err0r')
                })
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            if (isDeleting) {
                $('#removeAvailabilityModal').modal('show');
                $('#removeAvailabilityModal [data-confirm="modal"]').off('click').on('click', function () {
                    $.ajax({
                        'url': '/trainer/planning/calendars',
                        'method': 'DELETE',
                        'data': {
                            'beginning': calEvent.start.format(),
                            'ending': calEvent.end.format(),
                            'profileId': trainerId
                        }
                    }).done(function (response) {
                        console.log(response);
                        $('#removeAvailabilityModal').modal('hide');
                        $('#trainerPlanningCalendar').fullCalendar('removeEvents', [calEvent.id]);
                    }).fail(function (response) {
                        console.log(response);
                    });
                })
            } else {
                if (calEvent.heading) {
                    $('#online-calendar-modal').modal();

                    $('#modal-title').text(calEvent.heading);
                    $('#modal-booked-status').text(calEvent.bookingStatus);
                    $('#modal-start-date').text(calEvent.startEndDate);
                    $('#modal-duration').text(calEvent.duration);
                    $('#modal-trainer-display-name').text(calEvent.trainer);
                    $('#modal-mobile-phone').text(calEvent.mobile);
                    $('#modal-other-email').text(calEvent.email);
                    $('#modal-module-id').val(calEvent.id);
                    $('#modal-booking-id').val(calEvent.bookingId);

                    $('#modal-book-online-session').removeClass('hidden');
                    $('#modal-book-online-assign').addClass('hidden');
                    $('#modal-cancel-booking').addClass('hidden');

                    $('#modal-book-online-session').off('click').click(function () {
                        getAvailableTrainers(calEvent.id, trainerId, calEvent.start.format('YYYY-MM-DD'));
                    });
                }
            }
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            start_date = view.start.format();
            end_date = view.end.format();
            updateCalendar(view.start.format(), view.end.format());
        },
        eventRender: function (event, element) {
            // Don't render events on the weekends
        }
    });

    function updateCalendar(start, end) {
        return new Promise((resolve, reject) => {

            const selectorCalendar = '#trainerPlanningCalendar';

            let url = '/admin/trainers/calendars/' + trainerId;

            if (start && end) {
                url += '?dateStart=' + start + '&dateEnd=' + end;
            }
            $.ajax({
                'url': url,
                'method': 'GET',
            }).done(function (response) {
                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', response);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(response);
            });
        });
    }

    function getAvailableTrainers(moduleId, trainerId, currentDate) {
        let url = '/admin/trainers/available/module/' + moduleId + '/' + trainerId;
        let trainerOptions = '', hasTrainer = false;
        $.ajax({
            'url': url,
            'method': 'GET',
        }).done(function (response) {
            var selectList = "<select name='trainers' id='trainers' class='form-control'>";
            response.results.forEach((data) => {
                if (data.date) {
                    if (data.date == currentDate) {
                        data.sessions.forEach((session) => {
                            session.trainers.forEach((trainer) => {
                                selectList += '<option value="' + trainer.id + '">' + trainer.first_name + ' ' + trainer.last_name + '</option>';
                                hasTrainer = true;
                            });
                        });
                    }
                } else {
                    selectList += '<option value="' + data.trainer.id + '">' + data.trainer.first_name + ' ' + data.trainer.last_name + '</option>';
                    hasTrainer = true;
                }
            });
            selectList += "</select>";
            $('#modal-trainer-display-name').html(selectList);
            $('#modal-book-online-session').addClass('hidden');
            $('#modal-book-online-assign').removeClass('hidden');
            if (!hasTrainer) {
                $('#modal-cancel-booking').removeClass('hidden');
                $('#modal-book-online-assign').addClass('hidden');
            }
        });
    }

    $('body').on('click', function (e) {
        if (!CALENDAR.is(e.target) && CALENDAR.has(e.target).length === 0) {
            CALENDAR.removeClass('cursor-add cursor-clear');
            $('.fc-addAvailability-button').removeClass('fc-state-active');
            $('.fc-removeAvailability-button').removeClass('fc-state-active');
            isAdding = false;
            isDeleting = false;
        }
    });

    $('[for="availability-day"]').on('click', function () {
        if (!$('#availability-day').is(':checked')) {
            $('#dayPlanning').show();
            $('#rangePlanning').hide();
        }
    });

    $('[for="availability-range"]').on('click', function () {
        if (!$('#availability-range').is(':checked')) {
            $('#dayPlanning').hide();
            $('#rangePlanning').show();
        }
    });

    $('[data-action="addRangeAvailability"]').on('click', function (event) {
        event.preventDefault();
        let startDate = moment($('#startDate').val(), 'DD/MM/YYYY'),
            endDate = moment($('#endDate').val(), 'DD/MM/YYYY');

        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';

            if ($(selector).find('.form-check-input').is(':checked')) {
                let start = $(selector).find('[data-role="startTime"]').val(),
                    end = $(selector).find('[data-role="endTime"]').val();

                days.push({
                    dayName: day,
                    start: start,
                    end: end,
                });
            }
        }
        if ($('#startDate').val() && $('#endDate').val() && days.length > 0) {
            $.ajax({
                'url': '/trainer/planning/calendars/custom',
                'method': 'POST',
                'data': {
                    'beginning': startDate.format(),
                    'ending': endDate.format(),
                    'days': days,
                }
            }).done(function (response) {
                const startMonth = startDate.startOf('month');
                const endMonth = endDate.endOf('month');
                updateCalendar(startMonth.format('YYYY-MM-DD'), endMonth.format('YYYY-MM-DD'))
                    .then(() => $('[href="#monthPlanning"]').tab('show'));
            }).fail(function (response) {
                console.log('err0r')
            });
        }
    });

    $('[data-action="addWorkingHours"]').on('click', function (event) {
        event.preventDefault();
        const allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
        const days = [];
        for (let day of allDays) {
            let selector = '[data-role="day-' + day + '"]';
            let end = '', start = '';
            if ($(selector).find('.form-check-input').is(':checked')) {
                start = $(selector).find('[data-role="startTime"]').val();
                end = $(selector).find('[data-role="endTime"]').val();

            }
            days.push({
                dayName: day,
                start: start,
                end: end,
            });
        }
        $.ajax({
            'url': '/trainer/planning/working/hours',
            'method': 'POST',
            'data': {
                'days': days,
            }
        }).done(function (response) {
            location.reload();
        }).fail(function (response) {
            console.log('err0r')
        });
    });

    $('[data-action="addSingleAvailability"]').on('click', function (event) {

        if ($('#singleDate').val()) {
            let singleDate = moment($('#singleDate').val(), 'DD/MM/YYYY'),
                endSingleDate = moment($('#singleDate').val(), 'DD/MM/YYYY').add(1, 'days'),
                startTime = $('#startTime').val(),
                endTime = $('#endTime').val();
            $.ajax({
                'url': '/trainer/planning/calendars',
                'method': 'POST',
                'data': {
                    'beginning': singleDate.format(),
                    'ending': endSingleDate.format(),
                    'startTime': startTime,
                    'endTime': endTime,
                    'profileId': trainerId
                }
            }).done(function (response) {
                const startMonth = singleDate.startOf('month');
                const endMonth = endSingleDate.endOf('month');
                updateCalendar(startMonth.format('YYYY-MM-DD'), endMonth.format('YYYY-MM-DD'))
                    .then(() => $('[href="#monthPlanning"]').tab('show'));
            }).fail(function (response) {
                console.log('err0r')
            })
        }
    });


    $('#repeatTime').on('click', function (event) {
        event.preventDefault();
        repeatTime();
    });

    $('#add-availability').on('click', function () {
        var singleDate = $('#singleDate').val();
        var startTime = $('#startTime').val();
        var endTime = $('#endTime').val();

        startTime = moment(singleDate + ' ' + startTime, 'DD/MM/YYYY HH:mm');
        endTime = moment(singleDate + ' ' + endTime, 'DD/MM/YYYY HH:mm');

        $.ajax({
            'url': '/trainer/planning/calendars',
            'method': 'POST',
            'data': {
                'beginning': startTime.format(),
                'ending': endTime.format(),
                'profileId': trainerId
            }
        }).done(function (response) {
            console.log(response);
        }).fail(function (response) {
            console.log(response);
        })
    });

    $('#modal-book-online-assign').on('click', function () {
        let trainerId = $('#trainers').val();
        let bookingId = $('#modal-booking-id').val();

        $.ajax({
            'url': '/admin/trainers/assign/trainer',
            'method': 'POST',
            'data': {
                'bookingId': bookingId,
                'profileId': trainerId
            }
        }).done(function (response) {
            updateCalendar(start_date, end_date)
                .then(() => $('[href="#monthPlanning"]').tab('show'));
        }).fail(function (response) {
            console.log(response);
        })
    });


    $('#modal-cancel-booking').on('click', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this booking!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "CONFIRM",
            cancelButtonText: "CANCEL",
            closeOnConfirm: false,
            closeOnCancel: false
        })
        .then((willDelete) => {
            if (willDelete) {
                let bookingId = $('#modal-booking-id').val();
                $.ajax({
                    'url': '/admin/trainers/cancel/booking',
                    'method': 'POST',
                    'data': {
                        'bookingId': bookingId
                    }
                }).done(function (response) {
                    updateCalendar(start_date, end_date)
                        .then(() => $('[href="#monthPlanning"]').tab('show'));
                }).fail(function (response) {
                    console.log(response);
                })
            } else {
                swal("Your booking is safe!");
            }
        });
    });
});

function dateFormat(date) {
    return date.format();
}

function repeatTime() {
    let startTime = $('#startTime-Monday').val();
    let endTime = $('#endTime-Monday').val();

    let startTimeAfternoon = $('#startTimeAfternoon-Monday').val();
    let endTimeAfternoon = $('#endTimeAfternoon-Monday').val();
    $('[id^="startTime-"]').each(function () {
        $(this).val(startTime);
    });

    $('[id^="endTime-"]').each(function () {
        $(this).val(endTime);
    });

    $('[id^="startTimeAfternoon-"]').each(function () {
        $(this).val(startTimeAfternoon);
    });

    $('[id^="endTimeAfternoon-"]').each(function () {
        $(this).val(endTimeAfternoon);
    })
}