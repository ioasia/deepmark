const GREEN = 'rgba(34,170,115,0.8)';
const CALENDAR = $('#learnerPlanningCalendar');
const HEADER_OPTIONS = {
    left: '',
    center: 'prev,title,next',
    right: ''
};

$(document).ready(function() {

    let isAdding = false;
    let isDeleting = false;

    $('#learnerPlanningCalendar').fullCalendar({
        header: HEADER_OPTIONS,
        selectable: true,
        firstDay: 1,
        eventLimit: 3,
        contentHeight: 'auto',
        defaultView: 'month',
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '21:00:00',
        fixedWeekCount: false,
        eventColor: GREEN,
        eventClick: function (calEvent, jsEvent, view) {
            if (calEvent.title) {
                $('#dashboard-calendar-modal').modal();

                $('#modal-title').text(calEvent.displayType);
                $('#modal-intervention').text(calEvent.intervention);
                $('#modal-entity').text(calEvent.entity);
                $('#modal-type').text(calEvent.displayType);
                $('#modal-start-date').text(calEvent.startEndDate);
                $('#modal-duration').text(calEvent.duration);
                $('#modal-booked-status').text(calEvent.bookingStatus);
                $('#modal-trainer-display-name').text(calEvent.trainerName);
                $('#modal-session-date-new').text(moment(calEvent.session_date).format('dddd D MMMM'));
                let title = calEvent.startEndDate.split(" from");
                $('#timeDuration').text(title[1]);

                $('#modal-mobile-phone').text(calEvent.trainerMobile);
                $('#modal-other-email').text(calEvent.trainerEmail);
                $('#modal-address').text(calEvent.moduleAddress);
                $("#modal-title-module").text(calEvent.moduleName);
                if(calEvent.type == 9 ){
                    $(".divForClassroom").show();
                }
                else{
                    $(".divForClassroom").hide();
                }
                if (!calEvent.available) {
                    $('#modal-book-online-session').text(jsTranslations.module_expired);
                    $('#modal-book-online-session').removeClass('btn-deepmark');
                    $(".booked").hide();
                    $(".expired").show();
                    $(".divStatus").css('border','1px solid grey')
                } else {
                    $('#modal-book-online-session').text(jsTranslations.session_rejoin);
                    $('#modal-book-online-session').addClass('btn-deepmark');
                    $(".booked").show();
                    $(".expired").hide();
                    $(".divStatus").css('border','1px solid #28c612')
                }
                $('#modal-book-online-session').off('click').click(function () {
                    if (calEvent.available) {
                        window.location.href = '/learner/courses/' + calEvent.id + '/module';
                    }
                });
            }
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            $(".fc-agendaWeek-button").prop('disabled', true);
            $(".fc-agendaWeek-button").addClass('fc-state-disabled');
            updateCalendar(view.start.format(), view.end.format());
        },
        eventRender: function (event, element) {

        }
    });

    function updateCalendar(start, end) {
        let learnerId = $('#learnerId').val();
        return new Promise((resolve, reject) => {

            const selectorCalendar = '#learnerPlanningCalendar';

            let url = '/learner/calendars';

            if (start && end) {
                url += '?dateStart=' + start + '&dateEnd=' + end + '&learnerId=' + learnerId;
            }
            $.ajax({
                'url': url,
                'method': 'GET',
            }).done(function (response) {
                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', response);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(response);
            });
        });
    }


    $('body').on('click', function(e) {
        if ( !CALENDAR.is(e.target) && CALENDAR.has(e.target).length === 0 ) {
            CALENDAR.removeClass('cursor-add cursor-clear');
            $('.fc-addAvailability-button').removeClass('fc-state-active');
            $('.fc-removeAvailability-button').removeClass('fc-state-active');
            isAdding = false;
            isDeleting = false;
        }
    });

    $('#repeatTime').on('click', function(event) {
        event.preventDefault();
        repeatTime();
    });
});

function dateFormat(date) {
    return date.format();
}

function repeatTime() {
    let startTime = $('#startTime-Monday').val();
    let endTime = $('#endTime-Monday').val();

    let startTimeAfternoon = $('#startTimeAfternoon-Monday').val();
    let endTimeAfternoon = $('#endTimeAfternoon-Monday').val();
    $('[id^="startTime-"]').each(function () {
        $(this).val(startTime);
    });

    $('[id^="endTime-"]').each(function () {
        $(this).val(endTime);
    });

    $('[id^="startTimeAfternoon-"]').each(function () {
        $(this).val(startTimeAfternoon);
    });

    $('[id^="endTimeAfternoon-"]').each(function () {
        $(this).val(endTimeAfternoon);
    })
}