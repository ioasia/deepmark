// Document loaded
$(document).ready(function () {
    // init
    statFiltersService.init();
});

// StatService
let statFiltersService = {
    // Init
    init() {
        statFiltersService.changeQuizType();
    },
    // change quiz type
    changeQuizType() {
        $("#quizIsSurvey").change(function () {
            // clear select
            $("#quizId").html('');
            $('#entityId').selectpicker('refresh');
            // get is survey val
            const is_survey = $(this).val();
            // get corresponding quizes
            $.ajax({
                type: "POST",
                url: '/admin/stats/quizes',
                data: {
                    "is_survey": is_survey,
                },
                success: function (data) {
                    $("#quizId").append($("<option />").val('all').text(translations.all));
                    $.each(data, function (i, data) {
                        $("#quizId").append($("<option />").val(data.id).text(data.title));
                    });
                    $('#quizId').selectpicker('refresh');
                }
            });
        });
    }
}
