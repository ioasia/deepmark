// Vars
let language = $('#locate').val();
let languageCalendar = $('#locate').val();

// Document loaded
$(document).ready(function () {
    // Stat init
    statService.init();
});
 
// StatService
let statService = {
    // Init
    init() {
        // Bind
        statService.bindTab();
        statService.bindPointer();
        $(".firstTimeBtn").click();
    },

    // Get current date
    getCurrentDate() {
        $("#activitiesWeekStart").val(weekStart);
        $("#activitiesWeekEnd").val(weekEnd);
        $('#weekRangeDateActivities').html('<span>' + moment(weekStart).format('DD/MM/YYYY') + '</span> – <span>' + moment(weekEnd).format('DD/MM/YYYY') + '</span>');
        $("#activitiesMonthStart").val(monthStart);
        $("#activitiesMonthEnd").val(monthEnd);
        $('#monthRangeDateActivities').html('<span>' + moment(monthStart).locale(language).format('MMMM YYYY') + '</span>');
        $("#activitiesYearStart").val(yearStart);
        $("#activitiesYearEnd").val(yearEnd);
        $('#yearRangeDateActivities').html('<span>' + moment(yearStart).format('YYYY') + '</span>');
    },

    // Bind tab
    bindTab() {
        $('.change_tab').click(function () {
            switch ($(this).data('action')) {
                case 'global':
                    $('.quiz-filter-only').hide();
                    $("#organizationId").trigger('change');
                    $("#entityId").trigger('change');
                    $(".stat").addClass('d-none');
                    $(".stat-data").addClass('d-none');
                    $("#global").removeClass('d-none');
                    $(".filter").addClass('d-none');
                    $(".global-filter").removeClass('d-none');
                    $("#typeReport").val(9).change();
                    $("#typeStat").val('global');
                    $(".f-search").removeClass("col-md-6");
                    $(".f-search").removeClass("col-md-2");
                    $(".f-search").removeClass("col-md-3");
                    $(".f-search").addClass("col-md-4");
                    $(".firstTimeBtn").click();
                    statService.getCurrentDate();
                    $(".next").attr("disabled", true);
                    break;
                case 'course' :
                    $('.quiz-filter-only').hide();
                    $("#organizationId").trigger('change');
                    $("#entityId").trigger('change');
                    $(".stat").addClass('d-none');
                    $("#course").removeClass('d-none');
                    $(".stat-data").addClass('d-none');
                    $(".filter").addClass('d-none');
                    $(".course-filter").removeClass('d-none');
                    $("#typeReport").val(1).change();
                    $(".period-filter").addClass('d-none');
                    $("#typeStat").val('course');
                    $(".f-search").removeClass('col-md-2');
                    $(".f-search").addClass('col-md-3');
                    $(".f-search").removeClass('col-md-4');
                    
                    // TypeReport
                    $('#typeReport option').show();
                    $('#typeReport .quiz-filter-val').hide();
                    $('#typeReport').selectpicker('refresh');
                    
                    statService.getCurrentDate();
                    // Configure exports
                    statExportsService.initConfigure('course');
                    break;
                case 'activity' :
                    $('.quiz-filter-only').hide();
                    $("#organizationId").trigger('change');
                    $("#entityId").trigger('change');
                    $(".firstTimeBtn").click();
                    $(".stat").addClass('d-none');
                    $("#typeStat").val('activity');
                    $("#activity").removeClass('d-none');
                    $(".filter").removeClass('d-none');
                    $(".stat-data").addClass('d-none');
                    $(".exportAll").addClass('d-none');
                    $(".globalExportDiv").addClass('d-none');
                    $(".timeActive").click();
                    $(".roundCount").addClass('d-none');
                    $(".typeStatDiv").addClass('d-none');
                    $(".f-search").removeClass('col-md-2');
                    $(".f-search").removeClass('col-md-3');
                    $(".f-search").removeClass('col-md-4');
                    $(".f-1").addClass('col-md-2');
                    $(".f-2").addClass('col-md-3');
                    $("#typeReport").val(7).change();
                    statService.getCurrentDate();
                    $(".next").attr("disabled", false);
                    break;
                case 'quiz' :
                    $("#organizationId").trigger('change');
                    $("#entityId").trigger('change');
                    $(".stat").addClass('d-none');
                    $(".stat-data").addClass('d-none');
                    $("#typeStat").val('quiz');
                    $("#quiz").removeClass('d-none');
                    $(".filter").addClass('d-none');
                    $(".quiz-filter").removeClass('d-none');
                    $(".f-search").removeClass('col-md-2');
                    $(".f-search").removeClass('col-md-3');
                    $(".f-search").removeClass('col-md-4');
                    $(".f-1").addClass('col-md-2');
                    $(".f-2").addClass('col-md-3');
                    $('.typeStatDiv').addClass('col-md-3');
                    
                    $('.quiz-filter-only').removeClass('d-none');
                    $('.quiz-filter-only').show();
                    
                    // TypeReport
                    $('#typeReport option').hide();
                    $('#typeReport .quiz-filter-val').show();
                    $("#typeReport").val(11).change();
                    $('#typeReport').selectpicker('refresh');
                    
                    statService.getCurrentDate();
                    $(".next").attr("disabled", false);
                    
                    // Configure exports
                    statExportsService.initConfigure('quiz');
                    break;
            }
        });

        // First loading
        $("#globalBtn").trigger('click');
    },
    // Bind pointer
    bindPointer() {
        $(".pointer").click(function () {
            let typeTime = $(this).attr('typeTime');
            let type = $("#typeStat").val();
            $(".calendar-custom").addClass('d-none');
            $(".pointer").removeClass('timeActive');
            $(".period-filter").addClass('d-none');
            $(this).addClass('timeActive');
            if (type == 'global') {
                $(".f-search").removeClass("col-md-4");
                $(".f-search").removeClass("col-md-6");
                $(".f-search").removeClass("col-md-2");
                $(".f-search").removeClass("col-md-3");

                $(".f-search").addClass("col-md-4");

            } else if (type == 'activity') {
                $(".f-search").removeClass('col-md-2');
                $(".f-search").removeClass('col-md-3');
                $(".f-search").removeClass('col-md-4');
                $(".f-1").addClass('col-md-2');
                $(".f-2").addClass('col-md-3');
            }

            if (typeTime == 'week') {
                $("#week-tab").removeClass('d-none');
                $('#startDate').val(moment($("#activitiesWeekStart").val()).format('DD/MM/YYYY'));
                $('#endDate').val(moment($("#activitiesWeekEnd").val()).format('DD/MM/YYYY'));

            } else if (typeTime == 'month') {
                $("#month-tab").removeClass('d-none');
                $('#startDate').val(moment($("#activitiesMonthStart").val()).format('DD/MM/YYYY'));
                $('#endDate').val(moment($("#activitiesMonthEnd").val()).format('DD/MM/YYYY'));
                if (languageCalendar == 'fr') {
                    let day = moment($('#activitiesMonthStart').val());
                    $("#monthTxt").text(day.locale(languageCalendar).format('MMMM YYYY'));
                }

            } else if (typeTime == 'year') {
                $("#year-tab").removeClass('d-none');
                $('#startDate').val(moment($("#activitiesYearStart").val()).format('DD/MM/YYYY'));
                $('#endDate').val(moment($("#activitiesYearEnd").val()).format('DD/MM/YYYY'));

            } else if (typeTime == 'period') {
                $("#period-tab").removeClass('d-none');
                $(".period-filter").removeClass('d-none');
                $('#startDate').val(moment().format('DD/MM/YYYY'));
                $("#startDateTxt").text(moment().format('DD/MM/YYYY'));
                $('#endDate').val(moment().format('DD/MM/YYYY'));
                $("#endDateTxt").text(moment().format('DD/MM/YYYY'));
                if (type == 'global') {
                    $(".f-search").removeClass("col-md-4");
                    $(".period-filter").removeClass("col-md-6");
                    $(".period-filter").removeClass("col-md-4");
                    $(".period-filter").addClass("col-md-2");
                    $(".organizationId").addClass("col-md-2");
                    $(".entityId").addClass("col-md-3");
                    $(".courseId").addClass("col-md-3");

                } else if (type == 'activity') {
                    $(".period-filter").addClass("col-md-6");
                }

            }
        });
    }
}