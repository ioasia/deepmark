// Translations
translations = {
    all: $('#translations').data('all'),
    saved: $('#translations').data('saved'),
    error: $('#translations').data('error'),
};