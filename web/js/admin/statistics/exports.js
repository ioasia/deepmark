// Document loaded
configureExports = false;
$(document).ready(function () {
    // init
    statExportsService.init();
});

// statExportsService
let statExportsService = {
    // Init
    init() {
    },
    // init configure
    initConfigure(type) {
        if(configureExports != false) {
            configureExports.destroy();
        }
        configureExports = $('#configureExports').DataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false,
            "ajax": '/admin/stats/configureExports/' + type,
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<input type="checkbox" class="line_icon" />'
                },
                {"data": "tab_title"}
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return  '<span class="configure_export_tab_title c-orangelight">' + data + '</span>';
                    },
                    "targets": 1
                }
            ],
            "initComplete": function (settings, json) {
                // Add event listener for opening and closing details
                $('#configureExports tbody').unbind('click');
                $('#configureExports tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = configureExports.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                        $('.line_icon', tr).prop('checked', false);
                        $('.configure_export_tab_title', tr).removeClass('c-green').addClass('c-orangelight');
                    } else {
                        // Open this row
                        row.child(statExportsService.format(row.data())).show();
                        tr.addClass('shown');
                        $('.line_icon', tr).prop('checked');
                        $('.configure_export_tab_title', tr).addClass('c-green').removeClass('c-orangelight');
                    }
                });

                // Check selected
                if (json.data) {
                    json.data.map((tab, i) => {
                        if (tab.selected == true) {
                            $('.line_icon:eq(' + i + ')').trigger('click');
                        }
                    });
                }
            }
        });



        // Init saving
        statExportsService.saveExportConfiguration(type);
    },
    format(d) {
        // Create array for choosing steps & questions
        table = '<table class="table configure_cols_table ml-3">';
        d.cols.map((col) => {
            table += '<tr class="configure_col">';
            table += '<td><input type="checkbox" class="configuration_col_id" id="configuration_col_id_' + col.template_id + '" data-configuration_col_id="' + col.template_id + '" ' + (col.selected == true ? 'checked' : '') + ' /></td>';
            table += '<td><label for="configuration_col_id_' + col.template_id + '">' + col.col_title + '</label></td>';
            table += '</tr>';
        });

        table += '</table>';

        return table;
    },
    saveExportConfiguration(type) {
        $('.saveExportConfiguration').unbind('click');
        $('.saveExportConfiguration').click(function () {
            let cols = [];
            $('input.configuration_col_id').each(function () {
                if ($(this).is(':checked')) {
                    cols.push(parseInt($(this).data('configuration_col_id')));
                }
            });
            $.ajax({
                type: "POST",
                url: '/admin/stats/configureExportsSave/' + type,
                data: {
                    "cols": cols,
                },
                success: function (data) {
                    if (data.status == 'success') {
                        $('#configure-perso').modal('hide');
                        swal({
                            title: translations.saved,
                            text: "",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            closeOnConfirm: false,
                            closeOnCancel: false,
                            timer: 1500
                        });
                    }
                }
            });
        });
    }
}
