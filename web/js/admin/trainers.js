$(document).ready(function () {
    bindLinkingTrainer();
});

function bindLinkingTrainer() {
    $(".btnActionTrainer").click(function() {
        let type = $(this).attr("data-type");
        $(".btnActionTrainer").addClass("btn-border");
        $(".btnActionTrainer").removeClass("btn-deepmark");
        $(this).addClass("btn-deepmark");
        $(this).removeClass("btn-border");
        $(".text-action").removeClass('text-white');
        $(".text-action").addClass('color-header');
        $(".text-action").removeClass('d-none');
        $(".divAction").addClass('d-none');
        if (type == "email") {
            $(".text-email").removeClass('color-header');
            $(".text-email").addClass('text-white');
            $(".divActionEmail").removeClass("d-none");
        }
    });

    $(".email-name").click(function() {
        $(".email-name").removeClass("active-attach");
        let emailId = $(this).attr("data-id");
        $("#selectTemplate").val(emailId);
        $(this).addClass("active-attach");

    });

    $('#btnLinkSendTrainer').on('click', function () {
        let nameBtn = $(this).text();
        const template = $('#selectTemplate')[0].value;
        const checkInput = $('input[type="checkbox"]');
        const selectedProfiles = [];

        checkInput.each(function () {
            if(this.checked && this.value !== ""){
                selectedProfiles.push(this.value);
            }
        });

        if (!selectedProfiles.length) {
            // one or more checkboxes are
            swal(jsTranslations.warning_title, jsTranslations.select_one_trainer, 'warning');
            return;
        }

        $.ajax({
            'url': '/admin/learners/sendAgain',
            'method': 'POST',
            'data' : {
                'template': template,
                'selectedProfiles': selectedProfiles
            },
            beforeSend: function() {
                $("#btnLinkSendTrainer").empty().html('<i class="fa fa-spinner fa-spin"></i> ' + nameBtn);
            },
        }).done(function (response) {
            swal(jsTranslations.success_title, jsTranslations.send_email_to_trainer_success, 'success');
            $("#btnLinkSendTrainer").empty().html(nameBtn);
        }).fail(function (response) {
            console.log(response)
        })
    });

    let moduleId = localStorage.getItem('assignLearnerModuleId');
    if (moduleId) {
        $('#selectIntervention').val( moduleId);//$('#selectIntervention').val(moduleId);
        $('#selectTemplate').val(moduleId);
        $("#assignCourse").addClass('btn-border');
        $("#assignCourse").addClass('btn-deepmark');
        $(".divActionCourse").removeClass("d-none");
        $("#interventionActive"+moduleId).addClass('active-attach');
        setTimeout(function () {
            $("#search_intervention").val($("#interventionActive"+moduleId).text().toLowerCase());
            $("#search_intervention").trigger('keyup');
        }, 3000);

        localStorage.removeItem('assignLearnerModuleId');
    }
}

