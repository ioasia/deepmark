$(document).ready(function() {

	$('.planif-progress-widget').each(function() {
		var progressBar = $(this).find('.progress');
		var percent = progressBar.attr('data-progress');

		progressBar.css({
			'background': 'linear-gradient(90deg, #22aa73 ' + percent + '%,#ffad17 50%, #e11c24)'
		});
	});

	/**
	 *	Check all btn
	*/
	$('.check-all-input').change(function() {
		if ($(this).is(':checked')) {
			$(this).closest('.table').find('.form-check-input').prop('checked', true);
		} else {
			$(this).closest('.table').find('.form-check-input').prop('checked', false);
		}
	});

	/**
	 *	Listing btn
	 */

	$('.send-msg-btn').click(function() {
		var checked = false;

		$(this).parent().parent().parent().find('.table-responsive').find('.form-check-input').each(function() {
			if ( this.checked ) {
				checked = true;
			}
		});

		if (checked == true) {
			$(this).closest('.send-msg-wrapper').find('.send-msg-area').toggle();
		}

		return false;
	});

	$('.link-to-interv-btn').click(function() {
		$('.link-to-interv-area').toggle();
		return false;
	});

	$('.link-to-email-btn').click(function() {
		$('.link-to-email-area').toggle();
		return false;
	});
});
