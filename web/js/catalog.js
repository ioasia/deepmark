function myFavorite() {
	$('.my-favorite').click(function(event) {
		if(!event.detail || event.detail == 1){
			let heart = $(this);
			let interventionId = heart.data("intervention-id");
			let favoriteId = heart.data("favorite-id");
			$.ajax({
				'url': '/api/catalog/add-favorite',
				'method': 'POST',
				'data' : {
					'interventionId': interventionId,
					'favoriteId': favoriteId,
				},
				beforeSend: function() {
					//$(this).empty().html('<i class="fa fa-spinner fa-spin"></i> ');
				},
			}).done(function (response) {
				if (response.status) {
					if (response.message == 'Added') {
						heart.removeClass('far fa-heart');
						heart.addClass('fas fa-heart');
						heart.data('favorite-id', response.favoriteId);
					} else if (response.message == 'Removed') {
						heart.removeClass('fas fa-heart');
						heart.addClass('far fa-heart');
						heart.data('favorite-id', 0);
					}
				}

			}).fail(function (response) {
				console.log(response)
			})
			return true;
		} else {
			console.log('false')
			return false;
		}
	});
}