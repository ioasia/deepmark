var COLORS = {
    red: '#e11c24',
    orange: '#ffad17',
    green: '#22aa73',
    blue: '#68d2f1',
    grey: '#a7a7a7'
};