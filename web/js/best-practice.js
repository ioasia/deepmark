function myFavorite() {
	$('.my-favorite').click(function(event) {
		if(!event.detail || event.detail == 1){
			console.log('true')
			let heart = $(this);
			let bestPracticeId = heart.data("best-practice-id");
			let favoriteId = heart.data("favorite-id");
			$.ajax({
				'url': '/api/bestPractices/add-favorite',
				'method': 'POST',
				'data' : {
					'bestPracticeId': bestPracticeId,
					'favoriteId': favoriteId,
				},
				beforeSend: function() {
					//$(this).empty().html('<i class="fa fa-spinner fa-spin"></i> ');
				},
			}).done(function (response) {
				if (response.status) {
					if (response.message == 'Added') {
						heart.removeClass('text-gray');
						heart.addClass('c-red');
						heart.data('favorite-id', response.favoriteId);
					} else if (response.message == 'Removed') {
						heart.addClass('text-gray');
						heart.removeClass('c-red');
						heart.data('favorite-id', 0);
					}
				}

			}).fail(function (response) {
				console.log(response)
			})
			return true;
		} else {
			console.log('false')
			return false;
		}
	});
}
function bindRating(){
	$('.ratings-widget i').on('click', function() {

		$('#evaluationBottomRow').css({ height: '50px' });

		$(this).removeClass('fal');
		$(this).addClass('fas');

		$(this).prevAll('i').each(function() {
			$(this).removeClass('fal');
			$(this).addClass('fas');
		});

		$(this).nextAll('i').each(function() {
			$(this).removeClass('fas');
			$(this).addClass('fal');
		});
	});
}

function getUserInfo(e) {
	let $this = $(e);
	let profileId = $(e).attr('data-profile');
	$.ajax({
		'url': "/api/bestPractices/user-info",
		'method': 'POST',
		'data' : {
			'profileId': profileId
		}
	}).done(function (response) {
		$this.find('.dropdown-menu').html(response);
	});
}