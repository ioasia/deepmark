$(document).ready(function() {
    bindAttendance();
    bindAlert();
});

function bindAttendance() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $('#attendance-sheet, #attend-absent, #room-management').on('show.bs.modal', function (e) {
        //e.preventDefault();
        //console.log($(e.relatedTarget).data('target'))
        let absent_data = $(e.relatedTarget).data('absent_data');
        console.log(absent_data)
        $.ajax({
            'url': lang + '/trainer/exercices/get-attendance-content',
            'method': 'POST',
            'data': {
                'id': $('#bookingId').val(),
                'absent': absent_data != 'undefined' ? absent_data : 0,
                'method': 1,
            },
            beforeSend: function() {
                if ($(e.relatedTarget).data('target') == '#attendance-sheet') {
                    $("#content-attendance-sheet").empty().html('<i class="fa fa-spinner fa-spin"></i>');
                } else if ($(e.relatedTarget).data('target') == '#attend-absent') {
                    $("#content-attend-absent").empty().html('<i class="fa fa-spinner fa-spin"></i>');
                } else {
                    $("#content-room-management").empty().html('<i class="fa fa-spinner fa-spin"></i>');
                }
            },
        }).done(function (response) {
            if ($(e.relatedTarget).data('target') == '#attendance-sheet') {
                $("#content-attendance-sheet").html(response);
            } else if ($(e.relatedTarget).data('target') == '#attend-absent') {
                $("#content-attend-absent").html(response);
            } else {
                $("#content-room-management").html(response);
            }
            $($(e.relatedTarget).data('target')).modal('show');

            $('.check-all-input').change(function() {
                if ($(this).is(':checked')) {
                    $(this).closest('.table').find('.form-check-input').prop('checked', true);
                } else {
                    $(this).closest('.table').find('.form-check-input').prop('checked', false);
                }
            });
            $('#signatureMethod').val(1);
            $("#importSignature").removeClass('btn-deepmark-orangelight');
            $("#importSignature").addClass('btn-deepmark-default');
            $("#importSignature").removeAttr('data-toggle');
            $("#importSignature").removeAttr('data-target');
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });

    $('#attendance-sheet-download').on('show.bs.modal', function (e) {
        $.ajax({
            'url': lang + '/trainer/exercices/save-signature-method',
            'method': 'POST',
            'data': {
                'id': $('#bookingId').val(),
                'method': 0,
            },
        }).done(function (response) {
            $('#signatureMethod').val(0);
            $("#digitalSignature").removeClass('btn-deepmark-orangelight');
            $("#digitalSignature").addClass('btn-deepmark-default');
            $("#digitalSignature").removeAttr('data-toggle');
            $("#digitalSignature").removeAttr('data-target');
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });

    $('[data-action="addAttendanceSheetDownload"]').on('click', function(event) {
        event.preventDefault();
        const attendances = [];

        $('.attendance-input-download').each(function(){
            if($(this).is(':checked')){
                attendances.push({
                    bookingId: $(this).val(),
                    isAttendance: 1,
                });
            } else {
                attendances.push({
                    bookingId: $(this).val(),
                    isAttendance: 0,
                });
            }
        });

        $.ajax({
            'url': '/trainer/exercices/save_attendance',
            'method': 'POST',
            'data': {
                'attendances': attendances,
            }
        }).done(function (response) {
            swal({
                type: 'info',
                allowOutsideClick: false,
                title: jsTranslations.attendance_title,
                text: jsTranslations.attendance_text,
            }).then(
                function () {
                    $('#attendance-sheet').modal('hide');
                },
            )
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });

    // Draw image
    let signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(0, 0, 0)'
    });
    let saveButton = document.getElementById('save');
    let cancelButton = document.getElementById('clear');

    saveButton.addEventListener('click', function (event) {
        console.log(signaturePad.isEmpty())
        if (!signaturePad.isEmpty()) {
            let signature = signaturePad.toDataURL('image/png');
            let bookingId = $('#signature-image').find('input[name="bookId"]').val();
            let signatureType = $('#signature-image').find('input[name="signatureType"]').val();
            let signatureName = $('#signature-image').find('input[name="signatureName"]').val();
            let isSessionSignature = $('#signature-image').find('input[name="sessionSignature"]').val();

            if(isSessionSignature == 1){
                $.ajax({
                    'url': '/trainer/exercices/save_signature',
                    'method': 'POST',
                    'data': {
                        'signature': signature,
                        'bookingId': bookingId,
                        'signatureType': signatureType,
                        'sessionSignature': 1
                    }
                }).done(function (response) {
                    swal({
                        type: 'info',
                        allowOutsideClick: false,
                        title: jsTranslations.save_signature_title,
                        text: "",
                    }).then(
                        function () {
                            $('#signature-image').modal('hide');
                            loadAttendanceSheet(lang)
                        },
                    )
                }).fail(function (response) {
                    swal(
                        "Error", jsTranslations.attendance_error, "error",
                        {
                            showCancelButton: true,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true
                        }
                    );
                });
            }else {

                // Send data to server instead...
                $.ajax({
                    'url': '/trainer/exercices/save_signature',
                    'method': 'POST',
                    'data': {
                        'signature': signature,
                        'bookingId': bookingId,
                        'signatureType': signatureType,
                    }
                }).done(function (response) {
                    if (response.disabledButton) {
                        console.log('signature-button')
                        $('.signature-button-' + bookingId).attr('disabled', 'disabled');
                    }

                    swal({
                        type: 'info',
                        allowOutsideClick: false,
                        title: jsTranslations.save_signature_title,
                        text: '',
                    }).then(
                        function () {
                            $('#signature-image').modal('hide');
                            loadAttendanceSheet(lang)
                        },
                    )
                }).fail(function (response) {
                    swal(
                        "Error", jsTranslations.attendance_error, "error",
                        {
                            showCancelButton: true,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true
                        }
                    );
                });
            }
        } else {
            swal(
                "Warning", jsTranslations.please_sign_text, "warning",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        }
        // window.open(data);
    });

    cancelButton.addEventListener('click', function (event) {
        signaturePad.clear();
    });

    // Keep booking ID and signatureType at the signature-image modal
    $('#signature-image').on('show.bs.modal', function(e) {
        $(e.currentTarget).find('input[name="sessionSignature"]').val("");
        if($(e.relatedTarget).attr('session-signature')){
            let bookId = $(e.relatedTarget).attr('signature-data');
            let signatureType = $(e.relatedTarget).attr('signature-type');
            let signatureName = $(e.relatedTarget).attr('signature-name');
            let profileName = $(e.relatedTarget).attr('profile-name');
            $(e.currentTarget).find('input[name="bookId"]').val(bookId);
            $(e.currentTarget).find('input[name="signatureType"]').val(signatureType);
            $(e.currentTarget).find('input[name="signatureName"]').val(signatureName);
            $(e.currentTarget).find('span[id="signatureNumber"]').text(signatureName);
            $(e.currentTarget).find('p[id="learnerName"]').text(profileName);
            $(e.currentTarget).find('input[name="sessionSignature"]').val(1);
        }else {
            let bookId = $(e.relatedTarget).attr('signature-data');
            $(e.currentTarget).find('input[name="bookId"]').val(bookId);

            let signatureType = $(e.relatedTarget).attr('signature-type');
            let signatureName = $(e.relatedTarget).attr('signature-name');
            let learnerName = $(e.relatedTarget).attr('learner-name');
            $(e.currentTarget).find('input[name="signatureType"]').val(signatureType);
            $(e.currentTarget).find('input[name="signatureName"]').val(signatureName);
            $(e.currentTarget).find('span[id="signatureNumber"]').text(signatureName);
            $(e.currentTarget).find('p[id="learnerName"]').text(learnerName);
        }
        $("#clear").trigger("click");
    });
}

function bindAlert() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $('#applyAttendanceAction').on('click', function(event) {
        event.preventDefault();
        let typeAction = $('#actionAttendanceSelected').val();

        if (typeAction == 'absence' || typeAction == 'presence') {
            const attendances = [];
            $('.attendance-input').each(function(){
                if($(this).is(':checked')){
                    attendances.push({
                        bookingId: $(this).val(),
                        isAttendance: 1,
                    });
                } else {
                    attendances.push({
                        bookingId: $(this).val(),
                        isAttendance: 0,
                    });
                }
            });
            $.ajax({
                'url': '/trainer/exercices/save_attendance',
                'method': 'POST',
                'data': {
                    'attendances': attendances,
                    'type': typeAction
                }
            }).done(function (response) {
                swal({
                    type: 'info',
                    allowOutsideClick: false,
                    title: jsTranslations.attendance_title,
                    text: jsTranslations.attendance_text,
                }).then(
                    function () {
                        $('#attendance-sheet').modal('hide');
                        loadAlertAttendance(lang)
                    },
                )
            }).fail(function (response) {
                swal(
                    "Error", jsTranslations.attendance_error, "error",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            });
        } else {
            const alerts = [];
            $('.attendance-input').each(function(){
                if($(this).is(':checked')){
                    alerts.push({
                        bookingId: $(this).val(),
                        isAlert: 1,
                    });
                } else {
                    alerts.push({
                        bookingId: $(this).val(),
                        isAlert: 0,
                    });
                }
            });

            $.ajax({
                'url': '/trainer/exercices/video/virtual_class_alerts',
                'method': 'POST',
                'data': {
                    'alerts': alerts,
                    'late': typeAction,
                }
            }).done(function (response) {
                swal(
                    jsTranslations.alert_checked, jsTranslations.alert_has_been_sent, "info",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                ).then(function(isConfirm) {
                    $('#virtualClassAlert').modal('hide');
                });
            }).fail(function (response) {
                swal(
                    "Error", "There is an error in your alert", "error",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            });
        }
    });
}

function bindAllowAttendanceSheet() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $('.closeOpenSignature').on('click', function(event) {
        $("#digitalSignatureFirst").val(0);
        event.preventDefault();
        const signatures = [];
        let typeSignature = $('#typeSignature').val();
        let id = $(this).attr("data-id");
        let bookingId = $('#bookingId').val();
        let title = ''
        let text = ''
        if (id == 1) {
            title = jsTranslations.open_signature_title + ' : ' + $("#typeSignature option:selected").text();
            text = jsTranslations.open_signature_text;
        } else {
            title = jsTranslations.close_signature_title + ' : ' + $("#typeSignature option:selected").text();
            text = jsTranslations.close_signature_text;
        }
        $('.signature-input').each(function(){
            if($(this).is(':checked')){
                signatures.push({
                    bookingId: $(this).val(),
                    allowSignature: id,
                });
            }
        });

        $.ajax({
            'url': '/trainer/exercices/allow-signatures',
            'method': 'POST',
            'data': {
                'signatures': signatures,
                'typeSignature': typeSignature,
                'bookingId': bookingId,
                'isOpen': id,
            }
        }).done(function (response) {
            swal({
                type: 'info',
                allowOutsideClick: false,
                html: '<b style="font-size: 30px;">'+title+'</b><br><br>'+text,
            }).then(
                function () {
                    $('#attend-absent').modal('hide');
                    loadAttendanceSheet(lang);
                    $('#typeSignature').val(typeSignature);
                },
            )
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });
}

function loadAttendanceSheet(lang) {
    $.ajax({
        'url': lang + '/trainer/exercices/get-attendance-content',
        'method': 'POST',
        'data': {
            'id': $('#bookingId').val(),
            'absent': 0,
        },
        beforeSend: function() {
            $("#content-attendance-sheet").empty().html('<i class="fa fa-spinner fa-spin"></i>');
        },
    }).done(function (response) {
        $("#content-attendance-sheet").html(response);
        $('#attendance-sheet').modal('show');

        $('.check-all-input').change(function() {
            if ($(this).is(':checked')) {
                $(this).closest('.table').find('.form-check-input').prop('checked', true);
            } else {
                $(this).closest('.table').find('.form-check-input').prop('checked', false);
            }
        });
    }).fail(function (response) {
        swal(
            "Error", jsTranslations.attendance_error, "error",
            {
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true
            }
        );
    });
}

function loadAlertAttendance(lang) {
    $.ajax({
        'url': lang + '/trainer/exercices/get-attendance-content',
        'method': 'POST',
        'data': {
            'id': $('#bookingId').val(),
            'absent': 1,
        },
        beforeSend: function() {
            $("#content-attend-absent").empty().html('<i class="fa fa-spinner fa-spin"></i>');
        },
    }).done(function (response) {
        $("#content-attend-absent").html(response);

        $('.check-all-input').change(function() {
            if ($(this).is(':checked')) {
                $(this).closest('.table').find('.form-check-input').prop('checked', true);
            } else {
                $(this).closest('.table').find('.form-check-input').prop('checked', false);
            }
        });
    }).fail(function (response) {
        swal(
            "Error", jsTranslations.attendance_error, "error",
            {
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true
            }
        );
    });
}

function loadRoomManagement(lang) {
    $.ajax({
        'url': lang + '/trainer/exercices/get-attendance-content',
        'method': 'POST',
        'data': {
            'id': $('#bookingId').val(),
            'absent': 2,
        },
        beforeSend: function() {
            $("#content-room-management").empty().html('<i class="fa fa-spinner fa-spin"></i>');
        },
    }).done(function (response) {
        $("#content-room-management").html(response);

        $('.check-all-input').change(function() {
            if ($(this).is(':checked')) {
                $(this).closest('.table').find('.form-check-input').prop('checked', true);
            } else {
                $(this).closest('.table').find('.form-check-input').prop('checked', false);
            }
        });
    }).fail(function (response) {
        swal(
            "Error", jsTranslations.attendance_error, "error",
            {
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true
            }
        );
    });
}