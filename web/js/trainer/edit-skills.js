$(document).ready(function () {
    bindFileUploaders();
    let type = 'trainer';
    createSkillsForm(type);
});

function loadSkillList(skillType) {
    let id = $('#divAddSkill').attr('data-id');
    $.ajax({
        url: '/trainer/profile/ajax-skills-list',
        type: 'GET',
        data: {
            'type': skillType,
            'id' : id,
        },
        success: function (response) {

            if (skillType == 1) {
                $('#mentor_skill_list').html(response);
            } else if (skillType == 2) {
                $('#trainer_skill_list').html(response);
            }
        }
    })
}


function createSkillsForm(skillType) {
    let id = $('#divAddSkill').attr('data-id');
    if (id != -1) {
        $.ajax({
            url: '/trainer/profile/ajax-available-skills',
            type: 'GET',
            data: {
                'id' : id,
                'type': skillType,

            },
            success: function (response) {

                let optionsString = '';
                let i = 0;
                let border = '';

                for (let index in response) {
                    border = i == 0 ? 'first-row' : "border-top-header";
                    optionsString = '<div data-id-row-first="'+index+'" id="skill'+index+'" class="skill'+index+' skill-row row pad-10 '+border+'">\
                                        <div class="col-md-8 no-padding">\
                                            <span data-id-row="'+index+'"  class="skill-name skills-list__item-title c-black">'+response[index]['name']+'</span>\
                                        </div>\
                                        <div class="col-md-4 text-right no-padding text-center">\
                                             <a data-id="'+index+'" class="color-header addSkill pointer">'+jsTranslations.global_submit+'\
                                            </a>\
                                        </div>\
                                    </div>';
                    $("#divAddSkill").append(optionsString);
                    i++;
                }
                let scriptAdd = '<script>\
                function loadSkillList(skillType) {\
                    let id = $("#divAddSkill").attr("data-id");\
                    $.ajax({\
                        url: "/trainer/profile/ajax-skills-list",\
                        type: "GET",\
                        data: {\
                            "type": skillType,\
                            "id" : id,\
                        },\
                        success: function (response) {\
                            if (skillType == 1) {\
                                $("#mentor_skill_list").html(response);\
                            } else if (skillType == 2) {\
                                $("#trainer_skill_list").html(response);\
                            }\
                        }\
                    })\
                }\
                $(document).ready(function () {\
                    $("#search_skill").keyup(function() {\
                        let text = $(this).val();\
                        let indexOf = [];\
                        $(".skill-row").hide();\
                        let position = 0;\
                        $(".skill-row .skill-name").each(function(){\
                            if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ){\
                                $(this).closest(".skill-row").show();\
                                if(position == 0){\
                                    $(this).closest(".skill-row").removeClass("border-top-header");\
                                }\
                                position++;\
                            }\
                        });\
                        if(text == ""){\
                        let count = 0; \
                            $(".skill-row").each(function(){ \
                            if(count != 0 && !$(this).hasClass( "border-top-header" )){\
                              $(this).addClass("border-top-header");\
                            }\
                            count++;\
                            });\
                        }\
                    });\
                    $(".addSkill").on("click", function () {\
                        let url = $("#actionSkill").val();\
                        let id = $(this).attr("data-id");\
                        let idUser = $("#divAddSkill").attr("data-id");\
                        $.ajax({\
                            type: "POST",\
                            url: url,\
                            data: {"skill": id, "type": "trainer" , "idUser" : idUser},\
                            success: function (response) {\
                                if (response["status"] != "success") {\
                                    return false;\
                                }\
                                loadSkillList(response["type"]);\
                                $("#skill"+id).remove();\
                            }\
                        })\
                    });\
                });\
            <\/script>';
                $("#divAddSkill").append(scriptAdd);
            }
        })
    } else {
        $.ajax({
            url: '/trainer/profile/ajax-available-skills',
            type: 'GET',
            data: {
                'id' : id,
                'type': skillType,

            },
            success: function (response) {

                let optionsString = '';
                let i = 0;
                let border = '';

                for (let index in response) {

                    border = i == 0 ? 'first-row' : "border-top-header";
                    optionsString = '<div data-id-row-first="'+index+'" id="skill'+index+'" class="skill'+index+' skill-row row pad-10 '+border+'">\
                                        <div class="col-md-8 no-padding">\
                                            <span data-id-row="'+index+'"  class="skill-name skills-list__item-title c-black">'+response[index]['name']+'</span>\
                                        </div>\
                                        <div class="col-md-4 text-right no-padding text-center">\
                                             <a data-title="'+response[index]['name']+'" data-id="'+index+'" class="color-header addSkill pointer">'+jsTranslations.global_submit+'\
                                            </a>\
                                        </div>\
                                    </div>';
                    $("#divAddSkill").append(optionsString);
                    i++;
                }
                let scriptAdd = '<script>\
                $(document).ready(function () {\
                    $("#search_skill").keyup(function() {\
                        let text = $(this).val();\
                        let indexOf = [];\
                        $(".skill-row").hide();\
                        let position = 0;\
                        $(".skill-row .skill-name").each(function(){\
                            if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ){\
                                $(this).closest(".skill-row").show();\
                                if(position == 0){\
                                    $(this).closest(".skill-row").removeClass("border-top-header");\
                                }\
                                position++;\
                            }\
                        });\
                        if(text == ""){\
                        let count = 0; \
                            $(".skill-row").each(function(){ \
                            if(count != 0 && !$(this).hasClass( "border-top-header" )){\
                              $(this).addClass("border-top-header");\
                            }\
                            count++;\
                            });\
                        }\
                    });\
                    $(".addSkill").on("click", function () {\
                        let txt = $(this).attr("data-title");;\
                        let idItem = $(this).attr("data-id");\
                        let character= "/";\
                        let div = \'<div id="skillItemDiv\'+idItem+\'" class="skills-list__item row no-padding"><div class="col-md-2 text-center no-padding"><span><label class="status-validate"></label></span></div><div class="col-md-8 no-padding"><input type="hidden" name="newSkills[]" value="\'+idItem+\'"><span class="skills-list__item-title">\'+txt+\'</span> </div> <div class="col-md-2 no-padding text-center"> <a data-id="\'+idItem+\'" class="deleteSkill pointer">  <i class="fa fa-times color-header"></i> </a> </div></div> \';\
                        div = div + \' <script>$(document).ready(function () { $(".deleteSkill").click(function () { let idDelete = $(this).attr("data-id"); $("#skillItemDiv"+idDelete).remove();   $("#skill"+idDelete).show(); }); }); <\'+character+\'script>\'; \
                        $("#skill"+idItem).hide();\
                        $("#trainer_skill_list").append(div);\
                    });\
                });\
            <\/script>';
                $("#divAddSkill").append(scriptAdd);
            }
        })
    }
}

function bindFileUploaders() {
    const uploadOptions = {
        formData: function (data, element) {
            let triggerField = element.fileInput[0];
            let profileId = $(triggerField).attr('data-profile-id');
            let dataType = $(triggerField).attr('data-type');
            return [{name: 'profileId', value: profileId}, {name: 'dataType', value: dataType}];
        },
        isTrainer : 1
    };
    initFileUploader($('.inputUploadPublicFile'), '/trainer/profile/ajax/file_upload/media', uploadOptions);
}
