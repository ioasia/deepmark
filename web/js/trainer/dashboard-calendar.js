const GREEN = '#28c612';
const CALENDAR = $('#trainerDashboardCalendar');
const HEADER_OPTIONS = {
    left: 'agendaWeek',
    center: 'prev,title,next',
    right: 'month'
};

$(document).ready(function() {
    let isAdding = false;
    let isDeleting = false;
    let locate = $('#locate').val();
    let default_lang = $('#default_locale').val();
    let lang = (locate == default_lang) ? '' : '/' + locate;
    $('#trainerDashboardCalendar').fullCalendar({
        locale: locate,
        header: HEADER_OPTIONS,
        selectable: true,
        firstDay: 1,
        eventLimit: 2,
        contentHeight: 'auto',
        defaultView: 'agendaWeek',
        allDaySlot: false,
        eventOverlap: false,
        slotEventOverlap : false,
        slotLabelFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        displayEventTime: false,
        maxTime: '24:00:00',
        fixedWeekCount: false,
        eventColor: GREEN,
        views: {
            week: {
                columnFormat: 'ddd D/M' // set format for week here
            },
        },
        eventClick: function (calEvent, jsEvent, view) {
            console.log(calEvent);
            if (calEvent.heading) {
                $('#dashboard-calendar-modal').modal();
                $("#numberLearner").text(calEvent.numberLearner);
                $('#modal-title').text(calEvent.heading);
                $('#modal-intervention').text(calEvent.intervention);
                $('#modal-entity').text(calEvent.entity);
                $('#modal-type').text(calEvent.heading);
                $('#modal-booked-status').text(calEvent.bookingStatus);
                $('#modal-start-date').text(calEvent.startEndDate);
                $('#modal-duration').text(calEvent.duration);
                $('#modal-trainer-display-name').text(calEvent.trainer);
                $('#modal-session-date-new').text(moment(calEvent.start).format('dddd D MMMM'));
                $('#timeDuration').text(calEvent.startEndDate);
                $('#modal-mobile-phone').text(calEvent.mobile);
                $('#modal-other-email').text(calEvent.email);
                $('#modal-address').text(calEvent.moduleAddress);
                $("#modal-title-module").text(calEvent.moduleName);
                $('.entity').text(calEvent.entity);
                if(calEvent.type == 9 ){
                    $(".divForClassroom").show();
                }
                else{
                    $(".divForClassroom").hide();
                }
                if(calEvent.type == 6 ){
                    $('#learnerName').text(calEvent.learnerName);
                    $('#learnerMobile').text(calEvent.learnerMobile);
                    $('#learnerEmail').text(calEvent.learnerEmail);
                    $('#learnerEntity').text(calEvent.entity);
                    $(".divForRemote").show();
                }
                else{
                    $(".divForRemote").hide();
                }
                if(calEvent.type == 7 ){
                    $(".divForVirtual").show();
                }
                else{
                    $(".divForVirtual").hide();
                }

                if (!calEvent.available) {
                    $('#modal-book-online-session').text(jsTranslations.module_expired);
                    $('#modal-book-online-session').removeClass('btn-deepmark');
                    $(".booked").hide();
                    $(".expired").show();
                    $(".divStatus").css('border','1px solid grey')
                } else {
                    $('#modal-book-online-session').text(jsTranslations.session_rejoin);
                    $('#modal-book-online-session').addClass('btn-deepmark');
                    $(".booked").show();
                    $(".expired").hide();
                    $(".divStatus").css('border','1px solid #28c612')
                }
                $('#modal-book-online-session').off('click').click(function () {
                    window.location.href = '/trainer/exercices/'+ calEvent.id +'/video/bookingAgenda/' + calEvent.bookingId;
                });
            }
        },
        viewRender: function (view, element) {
            $('.fc-center').after($('.calendar-legend'));
            updateCalendar(view.start.format(), view.end.format());
        },
        eventRender: function (event, element) {
            if(!event.available){
                element.find('.fc-content').parent().addClass('event-expired');
            }
            // check AM/PM
            let time = event.start.format("HH");
            if (time >= 12 ) {
                element.find('.fc-content').parent().addClass('border-pm');
            } else {
                element.find('.fc-content').parent().addClass('border-am');
            }

        },
        eventAfterAllRender: function (view) {
            // Count events
            var quantity = $('.fc-event').length;
            $("#numberBooking").text(quantity);
        }
    });

    function updateCalendar(start, end) {
        console.log(start, end);
        return new Promise((resolve, reject) => {

            const selectorCalendar = '#trainerDashboardCalendar';

            let url = lang+'/trainer/calendars';

            if (start && end) {
                url += '?dateStart=' + start + '&dateEnd=' + end;
            }
            $.ajax({
                'url': url,
                'method': 'GET',
            }).done(function (response) {
                $(selectorCalendar).fullCalendar('removeEvents');
                $(selectorCalendar).fullCalendar('addEventSource', response);
                $(selectorCalendar).fullCalendar('rerenderEvents');

                resolve(response);
            });
        });
    }


    $('body').on('click', function(e) {
        if ( !CALENDAR.is(e.target) && CALENDAR.has(e.target).length === 0 ) {
            CALENDAR.removeClass('cursor-add cursor-clear');
            $('.fc-addAvailability-button').removeClass('fc-state-active');
            $('.fc-removeAvailability-button').removeClass('fc-state-active');
            isAdding = false;
            isDeleting = false;
        }
    });
    //day activities
    $('#nextDayActivities').on('click', function () {
        var start = $('#activitiesDaySearch').val();
        var nextDay = moment(start).add(1, 'days');
        $.ajax({
            url: '/trainer/dayActivities?day=' + nextDay.format('YYYY-MM-DD'),
            method: "GET",
            success: function (data) {
                $('#activitiesDaySearch').val(nextDay.format('YYYY-MM-DD'));
                $('#dayRangeDateActivities').html('<span>'+ nextDay.format('DD/MM/YYYY') + '</span>');
                $('#activitiesDayTable table tbody').html(data);
            }
        })
    });

    $('#prevDayActivities').on('click', function () {
        var start = $('#activitiesDaySearch').val();
        var prevDay = moment(start).subtract(1, 'days');
        $.ajax({
            url: '/trainer/dayActivities?day=' + prevDay.format('YYYY-MM-DD'),
            method: "GET",
            success: function (data) {
                $('#activitiesDaySearch').val(prevDay.format('YYYY-MM-DD'));
                $('#dayRangeDateActivities').html('<span>'+ prevDay.format('DD/MM/YYYY') + '</span>');
                $('#activitiesDayTable table tbody').html(data);
            }
        })
    });
    //end day activities
    //week action for dashboard
    $('#prevWeekActivities').on('click', function () {
        var start = $('#activitiesWeekStart').val();
        var activitiesWeekStart = GetLastWeekStart(start);
        var activitiesWeekEnd = GetLastWeekEnd(start);
        $.ajax({
            url: '/trainer/activitiesByRange',
            method: "POST",
            data : {
                'startDate': activitiesWeekStart.format('YYYY-MM-DD'),
                'endDate': activitiesWeekEnd.format('YYYY-MM-DD')
            },
            success: function (data) {
                $('#activitiesWeekStart').val(activitiesWeekStart.format('YYYY-MM-DD'));
                $('#activitiesWeekEnd').val(activitiesWeekEnd.format('YYYY-MM-DD'));
                $('#weekRangeDateActivities').html('<span>'+ activitiesWeekStart.format('DD/MM/YYYY') + '</span>–<span>'+ activitiesWeekEnd.format('DD/MM/YYYY') +'</span>');
                $('#activitiesWeekTable table tbody').html(data);
            }
        })
    });

    $('#nextWeekActivities').on('click', function () {
        var start = $('#activitiesWeekEnd').val();
        var activitiesWeekStart = GetNextWeekStart(start);
        var activitiesWeekEnd = GetNextWeekEnd(start);
        $.ajax({
            url: '/trainer/activitiesByRange',
            method: "POST",
            data : {
                'startDate': activitiesWeekStart.format('YYYY-MM-DD'),
                'endDate': activitiesWeekEnd.format('YYYY-MM-DD')
            },
            success: function (data) {
                $('#activitiesWeekStart').val(activitiesWeekStart.format('YYYY-MM-DD'));
                $('#activitiesWeekEnd').val(activitiesWeekEnd.format('YYYY-MM-DD'));
                $('#weekRangeDateActivities').html('<span>'+ activitiesWeekStart.format('DD/MM/YYYY') + '</span>–<span>'+ activitiesWeekEnd.format('DD/MM/YYYY') +'</span>');
                $('#activitiesWeekTable table tbody').html(data);
            }
        })
    });

    function GetNextWeekStart(today) {
        today = moment(today);

        return today.add(1, 'days');
    }

    function GetNextWeekEnd(today) {
        var nextMonday = GetNextWeekStart(today);

        return nextMonday.add(6, 'days');
    }

    function GetLastWeekStart(today) {
        today = moment(today);

        return today.subtract(7, 'days');
    }

    function GetLastWeekEnd(today) {
        var lastMonday = GetLastWeekStart(today);
        return lastMonday.add(6, 'days');
    }
    //end week action for dashboard
});

function dateFormat(date) {
    return date.format();
}