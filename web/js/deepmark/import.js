(function (window, jQuery) {
    let importUsers = {
        el: {
            uploadFile: '#import-upload-file',
            console: '#console .card-text ul.list-unstyled'
        },
        sign: null,

        /**
         * Generate ajax URL
         * @returns {string}
         */
        getAjaxUrl() {
            return '/admin/' + importType + '/ajaxUpload'
        },

        /**
         *
         * @param msg
         * @param type
         */
        console: function (msg, type) {
            if (typeof type == 'undefined') {
                type = 'info';
            }
            jQuery(importUsers.el.console).append('<li class="text text-' + type + '">' + msg + '</li>');
        },

        /**
         * Reset everything
         */
        reset: function () {
            jQuery(importUsers.el.uploadFile).val('');
            jQuery(importUsers.el.uploadFile).removeClass('hidden d-none');
            jQuery('#import-step-mapping').addClass('hidden d-none');
            jQuery(importUsers.el.console).html('');
        },

        step: function (step) {

        },

        /**
         * Init import process
         * @param el
         * @returns {boolean}
         */
        init: function (el) {
            if (!importUsers.validateFile(el)) {
                importUsers.console('Invalid file validated', 'warning');
                importUsers.reset();
                return false;
            }

            importUsers.console('File is validated <i class="fas fa-check"></i>', 'success');

            // Process upload
            importUsers.upload();
        },

        /**
         * Validate file
         * @todo Move to separate validators
         * @param fileEl
         * @returns {boolean}
         */
        validateFile: function (fileEl) {
            let uploadFile = jQuery(fileEl)[0].files[0];
            let fileExt = uploadFile.name.split('.').pop().toLowerCase();
            let fileSize = uploadFile.size / 1024 / 1024;

            let allowedExt = ['csv', 'xls', 'xlsx'];

            if (allowedExt.indexOf(fileExt) < 0) {
                importUsers.console('Invalid file extension', 'warning');
                return false;
            }

            importUsers.console('Validated file extension <i class="fas fa-check"></i>', 'success');

            if (fileSize > 15) {
                importUsers.console('Invalid file size <i class="fas fa-check"></i>', 'warning');
                return false;
            }

            importUsers.console('Validated file size <i class="fas fa-check"></i>', 'success');

            jQuery(fileEl).addClass('hidden d-none');

            return true;
        },

        /**
         * Process upload file and start chain
         */
        upload: function () {
            let delimiter = jQuery("#delimiter").val();
            let formData = new FormData();

            formData.append('file', jQuery(importUsers.el.uploadFile)[0].files[0]);
            formData.append('task', 'uploadFile');
            formData.append('role', jQuery('#user-role option:selected').text())
            formData.append('delimiter', delimiter);

            jQuery.ajax({
                url: importUsers.getAjaxUrl(),
                type: 'POST',
                data: formData,
                beforeSend: function () {
                    swal(
                        "Uploading progress",
                        "Your file is being uploaded",
                        "info",
                        {
                            showCancelButton: true,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true
                        }
                    );
                    importUsers.console('File uploading...', 'secondary')
                },
                cache: false,
                contentType: false,
                processData: false
            })
                /**
                 * Upload succeed
                 */
                .done(function (response) {
                    importUsers.console('File uploaded <i class="fas fa-check"></i>', 'success')
                    let importHeaders = response.data.headers;
                    importUsers.sign = response.data.sign;

                    jQuery.each(importHeaders, function (index, importHeader) {
                        jQuery('select.mapping-fields').append('<option value="' + importHeader + '">' + importHeader + '</option>');
                    });

                    jQuery('.selectpicker').selectpicker('refresh');
                    jQuery('#mapping').removeClass('hidden d-none')

                    swal("File is uploaded", 'Please choose mapping field and press Validate', "success", {
                        button: "Ok",
                    });

                    jQuery('#import-step-mapping').removeClass('hidden d-none');
                })
                .fail(function (respond) {
                    swal("Error", respond.msg, "error", {
                        button: "Ok",
                    });
                });
        },

        /**
         * Validate mapped fields
         */
        validateFields: function () {
            let isValidated = true;
            jQuery.each(mappingFields, function (key, value) {
                let selectedValue = jQuery('#map-field-' + key + ' option:selected').text();
                if (value == true && selectedValue === '--No data--') {
                    alert(key + ' is required');

                    isValidated = false;
                }

                mappingFields[key] = selectedValue;
            });

            if (!isValidated) {
                return false;
            }

            importUsers.console('Validated mapping <i class="fas fa-check"></i>', 'success');
            jQuery('#import-step-mapping').addClass('hidden d-none');

            importUsers.import();
        },
        import: function () {
            jQuery('.loader').show();
            jQuery.ajax({
                url: importUsers.getAjaxUrl(),
                type: 'POST',
                data: {
                    task: 'importUsers',
                    userType: importType,
                    sign: importUsers.sign,
                    mappedFields: mappingFields,
                    delimiter: jQuery("#delimiter").val()
                },
            })
            .done(function (response) {
                if (response.console) {
                    jQuery.each(response.console, function (key, value) {
                        importUsers.console(value['message'], value['type']);
                    });
                }
                jQuery('.loader').hide();

                swal(
                    "Import succeed",
                    "Done",
                    "info",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            })
            .fail(function () {
                importUsers.reset();
            })
        }
    };

    jQuery(document).ready(function () {
        jQuery('body').on('change', importUsers.el.uploadFile, function (event) {
            importUsers.init(this);
        });
        jQuery('body').on('click', '#import-step-mapping', function (event) {
            if (importUsers.validateFields() === false) {
                event.preventDefault();
            }
        })
    });

    window.deepmark.importUsers = importUsers;

})(window, jQuery.noConflict());