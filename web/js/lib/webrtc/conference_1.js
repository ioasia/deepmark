// Last updated On: January 18, 2019

// Muaz Khan      - www.MuazKhan.com
// MIT License    - www.WebRTC-Experiment.com/licence
// Documentation  - github.com/muaz-khan/WebRTC-Experiment/tree/master/Pluginfree-Screen-Sharing

var isbroadcaster = false;
var createRoomInterval;
var conference = function(config) {
    if (typeof adapter === 'undefined' || typeof adapter.browserDetails === 'undefined') {
        // https://webrtc.github.io/adapter/adapter-latest.js
        console.warn('adapter.js is recommended.');
    } else {
        window.adapter = {
            browserDetails: {
                browser: 'chrome'
            }
        };
    }

    var self = {
            userToken: uniqueToken() // receive token form server socket
        },
        channels = '--',
        isGetNewRoom = true,
        participants = 0,
        defaultSocket = {};
    console.log("This user token", self.userToken);
    console.log("conference -> defaultSocket", defaultSocket)

    var sockets = [];

    function openDefaultSocket() {
        defaultSocket = config.openSocket({ // 2
            onmessage: defaultSocketResponse,
            callback: function(socket) {
                defaultSocket = socket;
            }
        }, 1);
        
    }

    function defaultSocketResponse(response) { 
        console.log("defaultSocketResponse -> response", response)
        console.log("defaultSocketResponse -> self.userToken", self.userToken)
        console.log("defaultSocketResponse -> channels", channels)
        console.log("defaultSocketResponse -> isGetNewRoom", isGetNewRoom)
        
        if (response.userToken == self.userToken) return;

        if(!isbroadcaster && response.roomToken !== self.roomToken && response.broadcaster === self.broadcaster) {
            response.reJoinRoom = true;
            // response.userToken = uniqueToken();
            self.userToken = uniqueToken();
            console.log("Broadcaster re-share");
            config.onRoomFound(response);
        }

        if (isGetNewRoom && response.roomToken && response.broadcaster) {
            console.log("In found room")
            config.onRoomFound(response);
        }

        if (response.newParticipant) {
            console.log("In new participant")
            onNewParticipant(response.newParticipant)
        };

        // trainer
        if (response.userToken && response.joinUser == self.userToken && response.participant && (channels.indexOf(response.userToken) == -1 || response.reJoinRoom === true)) {
            console.log("In open subsocket");
            channels = '--';
            channels += response.userToken + '--';
            console.log("Open subsocket", channels);
            openSubSocket({
                isofferer: true,
                channel: response.channel || response.userToken,
                closeSocket: true
            });
        }
    }

    function openSubSocket(_config) {
        if (!_config.channel) return;
        console.log("openSubSocket -> _config", _config);
        // return;

        var socketConfig = {
            channel: _config.channel,
            onmessage: socketResponse,
            onopen: function() {
                if (isofferer && !peer) {
                    console.log("openSubSocket -> peer", peer)
                    console.log("openSubSocket -> isofferer", isofferer)
                    // return;
                    
                    initPeer()
                };
                sockets[sockets.length] = socket;
            }
        };

        socketConfig.callback = function(_socket) {
            socket = _socket;
            this.onopen();
        };

        var socket = config.openSocket(socketConfig, 2),
            isofferer = _config.isofferer,
            gotstream,
            htmlElement = document.createElement('video'),
            inner = {},
            peer;

        var peerConfig = {
            oniceconnectionstatechange: function(p) {
                if (!isofferer) return;

                if (p && p.iceConnectionState) {
                    config.oniceconnectionstatechange(p.iceConnectionState);
                }
            },
            attachStream: config.attachStream,
            onICE: function(candidate) {
                socket && socket.send({
                    userToken: self.userToken,
                    candidate: {
                        sdpMLineIndex: candidate.sdpMLineIndex,
                        candidate: JSON.stringify(candidate.candidate)
                    }
                });
            },
            onRemoteStream: function(stream) {
                console.log("LEARNER RECEIVE THE STREAM")
                $(".status").addClass("text-success").html("Active");
                if (isbroadcaster) return;

                try {
                    htmlElement.setAttributeNode(document.createAttribute('autoplay'));
                    htmlElement.setAttributeNode(document.createAttribute('playsinline'));
                    htmlElement.setAttributeNode(document.createAttribute('controls'));
                    htmlElement.style.width = "inherit";

                } catch (e) {
                    htmlElement.setAttribute('autoplay', true);
                    htmlElement.setAttribute('playsinline', true);
                    htmlElement.setAttribute('controls', true);
                    htmlElement.style.width = "inherit";
                }
                htmlElement.srcObject = stream;

                _config.stream = stream;
                afterRemoteStreamStartedFlowing();
            }, 
            onRemoteStreamEnded: function(stream) {
                console.log("onRemoteStreamEnded -> stream", stream)
                
            }

        };

        function initPeer(offerSDP) {
            console.log("initPeer -> offerSDP", offerSDP)
            if (!offerSDP) { // trainer
                peerConfig.onOfferSDP = sendsdp
            }
            else { //learner
                peerConfig.offerSDP = offerSDP;
                peerConfig.onAnswerSDP = sendsdp;
            }
            peer = RTCPeerConnectionHandler(peerConfig);
            console.log("initPeer -> peer", peer)
        }

        function afterRemoteStreamStartedFlowing() {
            console.log("afterRemoteStreamStartedFlowing")
            gotstream = true;

            config.onRemoteStream({
                video: htmlElement
            });

            /* closing sub-socket here on the offerer side */
            if (_config.closeSocket) socket = null;
        }

        function sendsdp(sdp) {
            console.log("sendsdp -> sdp", sdp)
            sdp = JSON.stringify(sdp);
            var part = parseInt(sdp.length / 3);

            var firstPart = sdp.slice(0, part),
                secondPart = sdp.slice(part, sdp.length - 1),
                thirdPart = '';

            if (sdp.length > part + part) {
                secondPart = sdp.slice(part, part + part);
                thirdPart = sdp.slice(part + part, sdp.length);
            }
            console.log("sendsdp -> firstPart", firstPart)
            console.log("sendsdp -> secondPart", secondPart)
            console.log("sendsdp -> thirdPart", thirdPart)            

            socket && socket.send({
                userToken: self.userToken,
                firstPart: firstPart
            });

            socket && socket.send({
                userToken: self.userToken,
                secondPart: secondPart
            });

            socket && socket.send({
                userToken: self.userToken,
                thirdPart: thirdPart
            });
        }

        function socketResponse(response) {
            console.log("socketResponse -> response", response)
            if (response.userToken == self.userToken) return;
            if (response.firstPart || response.secondPart || response.thirdPart) {
                if (response.firstPart) {
                    inner.firstPart = response.firstPart;
                    if (inner.secondPart && inner.thirdPart) selfInvoker();
                }
                if (response.secondPart) {
                    inner.secondPart = response.secondPart;
                    if (inner.firstPart && inner.thirdPart) selfInvoker();
                }

                if (response.thirdPart) {
                    inner.thirdPart = response.thirdPart;
                    if (inner.firstPart && inner.secondPart) selfInvoker();
                }
            }

            if (response.candidate && !gotstream) {
                console.log("HAVE CANDIDATE AND NO STREAM")
                // return
                peer && peer.addICE({
                    sdpMLineIndex: response.candidate.sdpMLineIndex,
                    candidate: JSON.parse(response.candidate.candidate)
                });
            }

            if (response.left) {
                participants--;
                // isGetNewRoom = true;
                clearInterval(createRoomInterval);
                if (isofferer && config.onNewParticipant) config.onNewParticipant(participants);

                if (peer && peer.peer) {
                    peer.peer.close();
                    peer.peer = null;
                }
            }
        }

        var invokedOnce = false;

        function selfInvoker() {
            console.log("selfInvoker -> invokedOnce", invokedOnce);

            if (invokedOnce) return;

            invokedOnce = true;

            inner.sdp = JSON.parse(inner.firstPart + inner.secondPart + inner.thirdPart);
            console.log("selfInvoker -> inner.sdp", inner.sdp)

            if (isofferer && inner.sdp.type == 'answer') {
                peer.addAnswerSDP(inner.sdp);
                participants++;
                if (config.onNewParticipant) config.onNewParticipant(participants);
            } else {
                initPeer(inner.sdp)
            };
        }
    }

    function leave() {
        console.log("LEAVING")
        var length = sockets.length;
        for (var i = 0; i < length; i++) {
            var socket = sockets[i];
            if (socket) {
                socket.send({
                    left: true,
                    userToken: self.userToken
                });
                delete sockets[i];
            }
        }

        // if owner leaves; try to remove his room from all other users side
        if (isbroadcaster) {
            console.log("broadcaster out")
            defaultSocket.send({
                left: true,
                userToken: self.userToken,
                roomToken: self.roomToken
            });
        }

        if (config.attachStream) config.attachStream.stop();

    }

    window.addEventListener('beforeunload', function() {
        leave();
    }, false);

    // window.addStreamStopListener(stream, function () {
    //     console.log("onGettingSteam -> stream", stream)
    //     leave();
    // }); 

    window.addEventListener('keyup', function(e) {
        if (e.keyCode == 116)
            leave();
    }, false);

    function startBroadcasting() { // 3
        // if(defaultSocket) {console.log(defaultSocket);}
        defaultSocket && defaultSocket.send({
            roomToken: self.roomToken,
            roomName: self.roomName,
            broadcaster: self.userToken
        });
        console.log("AFTER EMIT BROADCASTER")
        setTimeout(startBroadcasting, 3000);
    }

    function onNewParticipant(channel) {
        if (!channel || channels.indexOf(channel) != -1 || channel == self.userToken) return;
        channels += channel + '--';

        var new_channel = uniqueToken();
        openSubSocket({
            channel: new_channel,
            closeSocket: true
        });

        defaultSocket.send({
            participant: true,
            userToken: self.userToken,
            joinUser: channel,
            channel: new_channel
        });
    }

    function uniqueToken() {
        return Math.random().toString(36).substr(2, 35);
    }

    openDefaultSocket(); // 1
    return {
        createRoom: function(_config) {
            self.roomName = _config.roomName || 'Anonymous';
            self.roomToken = uniqueToken();
            console.log("This room name", self.roomName);
            console.log("This room token", self.roomToken);

            isbroadcaster = true;
            isGetNewRoom = false;
            startBroadcasting();
            // createRoomInterval = setInterval(function() {
            //     startBroadcasting();
            // }, 3000);
        },
        joinRoom: function(_config) {
            console.log("joinRoom -> _config", _config)
            self.roomToken = _config.roomToken;
            self.broadcaster = _config.joinUser;
            isGetNewRoom = false;

            openSubSocket({
                channel: self.userToken
            });
            defaultSocket.send({
                participant: true,
                userToken: self.userToken,
                joinUser: _config.joinUser,
                reJoinRoom: _config.reJoinRoom
            });
            console.log("AFTER EMIT TO JOIN ROOM")
        }
    };
};

// Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RTCPeerConnection
// RTCPeerConnection-v1.5.js

var iceServers = [];

if (typeof IceServersHandler !== 'undefined') {
    iceServers = IceServersHandler.getIceServers();
}

iceServers = {
    iceServers: iceServers,
    iceTransportPolicy: 'all',
    bundlePolicy: 'max-bundle',
    iceCandidatePoolSize: 0
};

if (adapter.browserDetails.browser !== 'chrome') {
    iceServers = {
        iceServers: iceServers.iceServers
    };
}

var dontDuplicateOnAddTrack = {};

function RTCPeerConnectionHandler(options) {
    console.log("RTCPeerConnectionHandler -> options", options)
    var w = window,
        PeerConnection = w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection,
        SessionDescription = w.RTCSessionDescription || w.mozRTCSessionDescription,
        IceCandidate = w.RTCIceCandidate || w.mozRTCIceCandidate;

    var peer = new PeerConnection(iceServers);

    peer.onicecandidate = function(event) {
        console.log("peer.onicecandidate -> event", event)
        // return;
        if (event.candidate)
            options.onICE(event.candidate);
    };

    // attachStream = MediaStream;
    if (options.attachStream) {
        if ('addStream' in peer) {
            console.log("RTCPeerConnectionHandler -> addStream")
            peer.addStream(options.attachStream);
        } else if ('addTrack' in peer) {
            options.attachStream.getTracks().forEach(function(track) {
                peer.addTrack(track, options.attachStream);
            });
        } else {
            throw new Error('WebRTC addStream/addTrack is not supported.');
        }
    }

    // attachStreams[0] = audio-stream;
    // attachStreams[1] = video-stream;
    // attachStreams[2] = screen-capturing-stream;
    if (options.attachStreams && options.attachStream.length) {
        var streams = options.attachStreams;
        for (var i = 0; i < streams.length; i++) {
            var stream = streams[i];

            if ('addStream' in peer) {
                peer.addStream(stream);
            } else if ('addTrack' in peer) {
                stream.getTracks().forEach(function(track) {
                    peer.addTrack(track, stream);
                });
            } else {
                throw new Error('WebRTC addStream/addTrack is not supported.');
            }
        }
    }

    if ('addStream' in peer) {
        console.log("addStream in peer")
        peer.onaddstream = function(event) {
            var remoteMediaStream = event.stream;
            console.log("peer.onaddstream -> remoteMediaStream", remoteMediaStream)
            // return

            // onRemoteStreamEnded(MediaStream)
            addStreamStopListener(remoteMediaStream, function() {
                if (options.onRemoteStreamEnded) options.onRemoteStreamEnded(remoteMediaStream);
            });

            // onRemoteStream(MediaStream)
            if (options.onRemoteStream) options.onRemoteStream(remoteMediaStream);

            console.debug('on:add:stream', remoteMediaStream);
        };
    } else if ('addTrack' in peer) {
        console.log("addTrack")
        peer.ontrack = peer.onaddtrack = function(event) {
            event.stream = event.streams[event.streams.length - 1];

            if (dontDuplicateOnAddTrack[event.stream.id] && adapter.browserDetails.browser !== 'safari') return;
            dontDuplicateOnAddTrack[event.stream.id] = true;


            var remoteMediaStream = event.stream;

            // onRemoteStreamEnded(MediaStream)
            addStreamStopListener(remoteMediaStream, function() {
                if (options.onRemoteStreamEnded) options.onRemoteStreamEnded(remoteMediaStream);
            });

            // onRemoteStream(MediaStream)
            if (options.onRemoteStream) options.onRemoteStream(remoteMediaStream);

            console.debug('on:add:stream', remoteMediaStream);
        };
    } else {
        throw new Error('WebRTC addStream/addTrack is not supported.');
    }

    var sdpConstraints = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    if (isbroadcaster) {
        sdpConstraints = {
            OfferToReceiveAudio: false,
            OfferToReceiveVideo: false
        };
    }

    // onOfferSDP(RTCSessionDescription)

    function createOffer() { // trainer
        if (!options.onOfferSDP) return;
        console.log("TRAINER CREATE OFFER")


        peer.createOffer(sdpConstraints).then(function(sessionDescription) {
            sessionDescription.sdp = setBandwidth(sessionDescription.sdp);
            peer.setLocalDescription(sessionDescription).then(function() {
                options.onOfferSDP(sessionDescription); // TRAINER sendsdp HERE
            }).catch(onSdpError);
        }).catch(onSdpError);
    }

    // onAnswerSDP(RTCSessionDescription)

    function createAnswer() { //learner
        if (!options.onAnswerSDP) return;
        console.log("LEARNER CREATE ANSWER")

        //options.offerSDP.sdp = addStereo(options.offerSDP.sdp);
        peer.setRemoteDescription(new SessionDescription(options.offerSDP)).then(function() {
            peer.createAnswer(sdpConstraints).then(function(sessionDescription) {
                sessionDescription.sdp = setBandwidth(sessionDescription.sdp);
                peer.setLocalDescription(sessionDescription).then(function() {
                    options.onAnswerSDP(sessionDescription); // LEARNER sendsdp HERE
                }).catch(onSdpError);
            }).catch(onSdpError);
        }).catch(onSdpError);
    }

    function setBandwidth(sdp) {
        if (adapter.browserDetails.browser === 'firefox') return sdp;
        if (adapter.browserDetails.browser === 'safari') return sdp;
        if (isEdge) return sdp;

        // https://github.com/muaz-khan/RTCMultiConnection/blob/master/dev/CodecsHandler.js
        if (typeof CodecsHandler !== 'undefined') {
            sdp = CodecsHandler.preferCodec(sdp, 'vp9');
        }

        // https://github.com/muaz-khan/RTCMultiConnection/blob/master/dev/BandwidthHandler.js
        if (typeof BandwidthHandler !== 'undefined') {
            window.isFirefox = adapter.browserDetails.browser === 'firefox';

            var bandwidth = {
                screen: 512, // 300kbits minimum
                video: 512 // 256kbits (both min-max)
            };
            var isScreenSharing = false;

            sdp = BandwidthHandler.setApplicationSpecificBandwidth(sdp, bandwidth, isScreenSharing);
            sdp = BandwidthHandler.setVideoBitrates(sdp, {
                min: bandwidth.video,
                max: bandwidth.video
            });
            return sdp;
        }

        // removing existing bandwidth lines
        sdp = sdp.replace(/b=AS([^\r\n]+\r\n)/g, '');

        // "300kbit/s" for screen sharing
        sdp = sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:300\r\n');

        return sdp;
    }

    peer.isConnected = false;
    peer.oniceconnectionstatechange = peer.onsignalingstatechange = function() {
        if(peer && peer.isConnected && peer.iceConnectionState == 'failed') return;
        options.oniceconnectionstatechange(peer);
    };

    createOffer();
    createAnswer();

    function onSdpError(e) {
        console.error('sdp error:', JSON.stringify(e, null, '\t'));
    }

    return {
        addAnswerSDP: function(sdp) {
            console.log('setting remote description', sdp.sdp);
            peer.setRemoteDescription(new SessionDescription(sdp)).catch(onSdpError).then(function() {
                peer.isConnected = true;
            });
        },
        addICE: function(candidate) {
            console.log('adding candidate', candidate.candidate);

            peer.addIceCandidate(new IceCandidate({
                sdpMLineIndex: candidate.sdpMLineIndex,
                candidate: candidate.candidate
            }));
        },

        peer: peer
    };
}

var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);

// getUserMedia
var video_constraints = {
    mandatory: {},
    optional: []
};

function getUserMedia(options) {
    function streaming(stream) {
        if (typeof options.onsuccess === 'function') {
            options.onsuccess(stream);
        }

        media = stream;
    }

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(options.constraints || {
            audio: false,
            video: video_constraints
        }).then(streaming).catch(options.onerror || function(e) {
            console.error(e);
        });
        return;
    }

    var n = navigator,
        media;
    n.getMedia = n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia;
    n.getMedia(options.constraints || {
        audio: true,
        video: video_constraints
    }, streaming, options.onerror || function(e) {
        console.error(e);
    });

    return media;
}

function addStreamStopListener(stream, callback) {
    console.log("addStreamStopListener")
    stream.addEventListener('ended', function() {
        callback();
        callback = function() {};
    }, false);
    stream.addEventListener('inactive', function() {
        callback();
        callback = function() {};
    }, false);
    stream.getTracks().forEach(function(track) {
        track.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);
        track.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);
    });
}
