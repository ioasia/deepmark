/* Import packages */
const fs = require("fs");

/* Key & certificate */
const options = {
  //key: fs.readFileSync('./fake-keys/privatekey.pem'),
  //cert: fs.readFileSync('./fake-keys/certificate.pem'),
  key: fs.readFileSync('./private.pem'),
  cert: fs.readFileSync('./public.pem'),
  // key: fs.readFileSync('/etc/letsencrypt/live/webrtcweb.com/privkey.pem'),
  // cert: fs.readFileSync('/etc/letsencrypt/live/webrtcweb.com/fullchain.pem')
};

/* Http app*/
const app = require("https").createServer(options, (request, response) => {
  response.writeHead(200, {
    "Content-Type": "text/html",
  });
  response.statusCode = 200;
  response.write(JSON.stringify("Hello Bro!"));
  response.end();
});

/* Socket.io */
const io = require("socket.io").listen(app, {
  log: true,
  origins: "*:*",
});

io.set("transports", [
  // 'websocket',
  "xhr-polling",
  "jsonp-polling",
]);

let channels = {};

io.sockets.on("connection", (socket) => {
  let initiatorChannel = "";
  if (!io.isConnected) {
    io.isConnected = true;
  }

  socket.on("new-channel", (data) => {
    if (!channels[data.channel]) {
      initiatorChannel = data.channel;
    }

    channels[data.channel] = data.channel;
    onNewNamespace(data.channel, data.sender);
  });

  socket.on("presence", (channel) => {
    const isChannelPresent = !!channels[channel];
    socket.emit("presence", isChannelPresent);
  });

  socket.on("disconnect", (channel) => {
    if (initiatorChannel) {
      delete channels[initiatorChannel];
    }
  });
});

const onNewNamespace = (channel, sender) => {
  io.of("/" + channel).on("connection", (socket) => {
    let username;
    if (io.isConnected) {
      io.isConnected = false;
      socket.emit("connect", true);
    }

    socket.on("message", (data) => {
      if (data.sender == sender) {
        if (!username) username = data.data.sender;

        socket.broadcast.emit("message", data.data);
      }
    });
    
    socket.on("sharescreen", (data) => {
      socket.broadcast.emit("sharescreen", data.isShared);
    })

    socket.on("toggleJitsi", (data) => {
      // console.log("onNewNamespace -> data", data);
      socket.broadcast.emit("toggleJitsi", data.data);
    })

    socket.on("changeRoom", (data) => {
      console.log("onNewNamespace -> data", data);
      socket.broadcast.emit("changeRoom", data.data);
    })

    socket.on("disconnect", () => {
      if (username) {
        socket.broadcast.emit("user-left", username);
        username = null;
      }
    });
  });
};

/* App run */
app.listen(process.env.PORT || 9559);

process.on("unhandledRejection", (reason, promise) => {
  process.exit(1);
});

console.log(
  "Please open SSL URL: https://localhost:" + (process.env.PORT || 9559) + "/"
);
