$(document).ready(function() {

	$('.tr--withmodal').click(function(e) {
		var target = $(e.target);

		var elemToCheck = target;
		if (target.hasClass('fa')) {
			elemToCheck = target.parent().parent().parent();
		} else if (target.hasClass('candlestick-nc') || target.hasClass('candlestick-off') || target.hasClass('candlestick-on') || target.hasClass('candlestick-toggle')) {
			elemToCheck = target.parent().parent();
		} else if (target.hasClass('candlestick-bg')) {
			elemToCheck = target.parent();
		}

		if (elemToCheck.hasClass('candlestick-wrapper')) {
			return false;
		}

		var id = $(this).attr('data-id');
		$('.tr-modal-' + id).toggleClass('active');
	});

});
