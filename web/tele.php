<?php

use Symfony\Bundle\FrameworkBundle\Console\Application;

// if you don't want to setup permissions the proper way, just uncomment the following PHP line
// read https://symfony.com/doc/current/setup.html#checking-symfony-application-configuration-and-setup
// for more information
//umask(0000);

set_time_limit(0);

require __DIR__ . '/../vendor/autoload.php';


$kernel      = new AppKernel('dev', 1);
$application = new Application($kernel);
$application->setAutoExit(false);

$input = new \Symfony\Component\Console\Input\ArrayInput([
    'command'   => 'clean:default',
    // (optional) pass options to the command
    '--keyword' => $_REQUEST['keyword'],
]);

// You can use NullOutput() if you don't need the output
$output = new \Symfony\Component\Console\Output\BufferedOutput();
$application->run($input, $output);

// return the output, don't use if you used NullOutput()
$content = $output->fetch();

echo '<pre>';
echo $content;