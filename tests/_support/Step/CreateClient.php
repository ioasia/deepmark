<?php

namespace Step;

/**
 * Trait CreateClient
 * @package Step
 */
trait CreateClient
{
    public function createClient(
        $firstName,
        $lastName,
        $entityId,
        $timezoneId,
        $mobile,
        $position,
        $fixedPhone,
        $email,
        $password
    )
    {
        $I = $this;

        $I->click('Clients');
        $I->see('Admin list', 'h2.marg-b-30');

        $I->seeLink('List');
        $I->seeLink('New');
        $I->seeLink('Upload');

        $I->click('New');

        $I->see('Create new client');
        $I->fillField('input#profile_client_firstName', $firstName);
        $I->fillField('input#profile_client_lastName', $lastName);
        $I->selectOption('select#profile_client_entity', $entityId);
        $I->selectOption('select#profile_client_person_timezone', $timezoneId);
        $I->fillField('input#profile_client_mobilePhone', $mobile);
        $I->fillField('input#profile_client_position', $position);
        $I->fillField('input#profile_client_fixedPhone', $fixedPhone);
        $I->fillField('input#profile_client_person_email', $email);
        $I->fillField('input#profile_client_person_plainPassword_first', $password);
        $I->fillField('input#profile_client_person_plainPassword_second', $password);
        $I->click('Create');
    }
}
