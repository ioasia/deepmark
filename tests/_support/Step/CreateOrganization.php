<?php

namespace Step;

/**
 * Trait CreateOrganizations
 * @package Step
 */
trait CreateOrganization
{
    /**
     *
     */
    public function createOrganizations($designation, $street, $zipCode, $country, $city)
    {
        $I = $this;

        $I->click('Organizations');
        $I->seeLink('Create a new organisation');

        $I->click('Create a new organisation');
        $I->see('Create');

        $I->fillField('input#appbundle_organisation_designation', $designation);
        $I->fillField('input#appbundle_organisation_street', $street);
        $I->fillField('input#appbundle_organisation_zipcode', $zipCode);
        $I->fillField('input#appbundle_organisation_country', $country);
        $I->fillField('input#appbundle_organisation_city', $city);
        $I->click('Create');
    }
}
