<?php

namespace Step;

/**
 * Trait CreateEntity
 * @package Step
 */
trait CreateEntity
{
    /**
     *
     */
    public function createEntity($designation, $street, $organizationId)
    {
        $I = $this;

        $I->click('Entities');
        $I->seeLink('Create a new entity');

        $I->click('Create a new entity');
        $I->see('Create', 'button.btn-deepmark-orangelight');

        $I->fillField('input#appbundle_entity_designation', $designation);
        $I->fillField('input#appbundle_entity_street', $street);
        $I->selectOption('select#appbundle_entity_organisation', $organizationId);
        $I->click('//*[@id="main"]/div/div/div/div/form/button');
    }
}
