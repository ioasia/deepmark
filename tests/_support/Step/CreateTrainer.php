<?php

namespace Step;

trait CreateTrainer
{
    /**
     * @param $email
     */
    public function createTrainer($firstName, $lastName, $entityId, $timeZoneId, $mobile, $position, $fixedPhone, $email, $password)
    {
        $I = $this;

        $I->click('Trainers');
        $I->click('New');
        $I->see('Create new trainer');

        $I->fillField('input#appbundle_profile_firstName', $firstName);
        $I->fillField('input#appbundle_profile_lastName', $lastName);
        $I->selectOption('select#appbundle_profile_entity', $entityId);
        $I->selectOption('select#appbundle_profile_person_timezone', $timeZoneId);
        $I->fillField('input#appbundle_profile_mobilePhone', $mobile);
        $I->fillField('input#appbundle_profile_position', $position);
        $I->fillField('input#appbundle_profile_fixedPhone', $fixedPhone);
        $I->fillField('input#appbundle_profile_person_email', $email);
        $I->fillField('input#appbundle_profile_person_plainPassword_first', $password);
        $I->fillField('input#appbundle_profile_person_plainPassword_second', $password);
        $I->click('Create');
    }
}