<?php

namespace Step\Acceptance;

use AcceptanceTester;
use Codeception\Scenario;

/**
 * Class Trainer
 * @package Step\Acceptance
 */
class Leaner extends AcceptanceTester
{
    /**
     * SuperAdmin constructor.
     * @param Scenario $scenario
     */
    public function __construct(Scenario $scenario)
    {
        parent::__construct($scenario);

        //$this->sidebarLinks = ['dashboard', 'My planning', 'My exercices', 'Statistics', 'My library', 'Logout'];

        $this->login('sadmin', $this->password);

        $faker          = $this->getFaker();
        $firstName      = $faker->firstName;
        $lastName       = $faker->lastName;
        $mobile         = $faker->phoneNumber;
        $position       = $faker->word;
        $fixedPhone     = $faker->phoneNumber;
        $this->username = $faker->email;

        $this->createLearner($firstName, $lastName, 1, 1, $mobile, $position, $fixedPhone, $this->username, $this->password);
    }
}
