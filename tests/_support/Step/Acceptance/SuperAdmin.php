<?php

namespace Step\Acceptance;

use AcceptanceTester;
use Codeception\Scenario;
use Step\CreateClient;
use Step\CreateEntity;
use Step\CreateOrganization;
use Step\CreateTrainer;

/**
 * Class SuperAdmin
 * @package Step\Acceptance
 */
class SuperAdmin extends AcceptanceTester
{
    use CreateOrganization;
    use CreateEntity;
    use CreateClient;
    use CreateTrainer;

    /**
     * SuperAdmin constructor.
     * @param Scenario $scenario
     */
    public function __construct(Scenario $scenario)
    {
        parent::__construct($scenario);

        $this->sidebarLinks = array_merge(
            $this->sidebarLinks,
            ['Clients', 'Organizations', 'Entities', 'Supervisors', 'Learners', 'Trainers', 'Courses', 'quiz', 'Statistics', 'Skills', 'Library']
        );

        $this->username = 'sadmin';
    }

    public function doLogin()
    {
        $this->login($this->username, $this->password);
    }
}