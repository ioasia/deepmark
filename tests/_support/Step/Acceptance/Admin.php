<?php

namespace Step\Acceptance;

use AcceptanceTester;
use Codeception\Scenario;
use Step\CreateClient;
use Step\CreateEntity;
use Step\CreateOrganization;
use Step\CreateTrainer;

/**
 * Class Admin
 * @package Step\Acceptance
 */
class Admin extends AcceptanceTester
{
    use CreateOrganization;
    use CreateEntity;
    use CreateClient;
    use CreateTrainer;

    /**
     * SuperAdmin constructor.
     * @param Scenario $scenario
     */
    public function __construct(Scenario $scenario)
    {
        parent::__construct($scenario);

        $this->sidebarLinks = array_merge(
            $this->sidebarLinks,
            ['Organizations', 'Entities', 'Supervisors', 'Learners', 'Trainers', 'Courses', 'quiz', 'Statistics', 'Skills', 'Library']
        );

        $this->login('sadmin', $this->password);

        $faker          = $this->getFaker();
        $firstName      = $faker->firstName;
        $lastName       = $faker->lastName;
        $mobile         = $faker->phoneNumber;
        $position       = $faker->word;
        $fixedPhone     = $faker->phoneNumber;
        $this->username = $faker->email;

        $this->createClient($firstName, $lastName, 1, 1, $mobile, $position, $fixedPhone, $this->username, $this->password);
    }

    public function doLogin()
    {
        $this->login($this->username, $this->password);
    }
}
