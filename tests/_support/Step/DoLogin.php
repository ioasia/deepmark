<?php

namespace Step;

use Page\Login;

/**
 * Trait Login
 */
trait DoLogin
{
    /**
     * @param  $username
     * @param  $password
     */
    protected function login($username, $password)
    {
        $I = $this;
        $I->amOnPage(Login::$URL);
        $I->seeInTitle(Login::$title);
        $I->fillField(Login::$usernameField, $username);
        $I->fillField(Login::$passwordField, $password);
        $I->click(Login::$loginButton);
    }
}
