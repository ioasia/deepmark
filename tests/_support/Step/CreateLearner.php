<?php

namespace Step;

/**
 * Trait CreateLearner
 * @package Step
 */
trait CreateLearner
{
    /**
     * @param $email
     */
    public function createLearner($firstName, $lastName, $entityId, $timeZoneId, $mobile, $position, $fixedPhone, $email, $password)
    {
        $I = $this;

        $I->click('Learners');
        $I->click('New');
        $I->see('Create new learner');

        $I->fillField('input#profile_learner_firstName', $firstName);
        $I->fillField('input#profile_learner_lastName', $lastName);
        /**
         * @TODO Only for Super Administrator
         */
        //$I->selectOption('select#appbundle_profile_entity', $entityId);
        $I->selectOption('select#profile_learner_person_timezone', $timeZoneId);
        $I->fillField('input#profile_learner_mobilePhone', $mobile);
        $I->fillField('input#profile_learner_position', $position);
        $I->fillField('input#profile_learner_fixedPhone', $fixedPhone);
        $I->fillField('input#profile_learner_person_email', $email);
        $I->fillField('input#profile_learner_person_plainPassword_first', $password);
        $I->fillField('input#profile_learner_person_plainPassword_second', $password);
        $I->click('Create');
    }
}
