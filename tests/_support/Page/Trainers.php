<?php


namespace Page;


class Trainers extends BasePage
{
    public static $URL = '/trainers';
    public static $URL_NEW = '/trainers/new';

    public static $FIELD_FIRSTNAME = 'input#appbundle_profile_firstName';
    public static $FIELD_LASTNAME = 'input#appbundle_profile_lastName';
    public static $FIELD_ENTITY = 'input#appbundle_profile_entity';
    public static $FIELD_TIMEZONE = 'input#appbundle_profile_person_timezone';
    public static $FIELD_MOBILE = 'input#appbundle_profile_mobilePhone';
    public static $FIELD_POSITION = 'input#appbundle_profile_position';
    public static $FIELD_FIXEDPHONE = 'input#appbundle_profile_fixedPhone';
    public static $FIELD_EMAIL = 'input#appbundle_profile_person_email';
    public static $FIELD_PASSWORD = 'input#appbundle_profile_person_plainPassword_first';
    public static $FIELD_CONFIRMPASSWORD = 'input#appbundle_profile_person_plainPassword_second';
}
