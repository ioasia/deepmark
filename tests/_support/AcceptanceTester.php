<?php

use Codeception\Actor;
use Faker\Factory;
use Step\CreateClient;
use Step\CreateEntity;
use Step\CreateLearner;
use Step\CreateOrganization;
use Step\CreateTrainer;
use Step\DoLogin;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends Actor
{
    use _generated\AcceptanceTesterActions;

    use DoLogin;
    use CreateOrganization;
    use CreateEntity;
    use CreateClient;
    use CreateTrainer;
    use CreateLearner;

    protected $sidebarLinks = ['Dashboard', 'Logout'];

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password = 'Vietnam';

    /**
     * Define custom actions here
     */

    /**
     * @return \Faker\Generator
     */
    public function getFaker()
    {
        return Factory::create();
    }

    /**
     *
     */
    public function doLogin()
    {
        $this->login($this->username, $this->password);
    }

    /**
     *
     */
    public function checkSidebar()
    {
        $I = $this;

        foreach ($this->sidebarLinks as $link) {
            $I->seeLink($link);
            $I->click($link);
        }
    }
}
