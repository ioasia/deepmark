<?php

use Step\Acceptance\Trainer;

/**
 * Class TrainerCest
 */
class TrainerCest
{
    /**
     * @param Trainer $I
     */
    public function checkLogin(Trainer $I)
    {
        //$I->checkLogin();
    }

    /**
     * @param Trainer $I
     */
    public function checkProfile(Trainer $I)
    {
        //$I->checkProfile();
    }

    /**
     * @param Trainer $I
     */
    public function checkEditProfile(Trainer $I)
    {
        //$I->checkEditProfile();
    }
}
