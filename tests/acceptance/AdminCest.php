<?php

use Step\Acceptance\Admin;
use Step\Acceptance\SuperAdmin;

/**
 * Class AdminCest
 */
class AdminCest
{
    protected $organizationId;

    protected $entityId;

    protected $clientId;

    /**
     * @param Admin $I
     */
    public function checkLogin(Admin $I)
    {
        $I->doLogin();
        $I->cantSee('Register');
        $I->cantSee('Log in');
    }

    /**
     * @param SuperAdmin $I
     */
    public function checkOrganisations(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $designation = implode(',', $faker->words);
        $street      = implode(',', $faker->words);
        $city        = $faker->city;

        $I->createOrganizations(
            $designation,
            $street,
            $faker->numerify(),
            $faker->country,
            $city
        );

        $this->organizationId = (int)$I->grabTextFrom('//*[@id="main"]/div/div/div/div/table/tbody/tr[1]/td');

        $I->amOnPage('/admin/organisations/');
        $I->canSee($designation);
        $I->canSee($street);
        $I->canSee($city);
    }

    public function checkEntities(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $designation = implode(',', $faker->words);
        $street      = implode(',', $faker->words);

        $I->createEntity($designation, $street, $this->organizationId);
        $this->entityId = (int)$I->grabTextFrom('//*[@id="main"]/div/div/div/div/table/tbody/tr[1]/td');

        $I->amOnPage('/admin/entities/');
        $I->canSee($designation);
        $I->canSee($street);
    }

    public function checkTrainers(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $firstName  = $faker->firstName;
        $lastName   = $faker->lastName;
        $mobile     = $faker->phoneNumber;
        $position   = $faker->word;
        $fixedPhone = $faker->phoneNumber;
        $email      = $faker->email;
        $password   = $faker->password;

        $I->createTrainer($firstName, $lastName, $this->entityId, 1, $mobile, $position, $fixedPhone, $email, $password);

        $I->amOnPage('/admin/trainers/');
        $I->canSee($firstName);
        $I->canSee($lastName);
    }
}
