<?php

use Step\Acceptance\SuperAdmin;

/**
 * Class AdminCest
 */
class SuperAdminCest
{
    protected $organizationId;

    protected $entityId;

    protected $clientId;

    /**
     * @param SuperAdmin $I
     */
    public function checkLogin(SuperAdmin $I)
    {
        $I->doLogin();
        $I->cantSee('Register');
        $I->cantSee('Log in');
    }

    /**
     * @param SuperAdmin $I
     */
    public function checkOrganisations(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $designation = implode(',', $faker->words);
        $street      = implode(',', $faker->words);
        $city        = $faker->city;

        $I->createOrganizations(
            $designation,
            $street,
            $faker->numerify(),
            $faker->country,
            $city
        );

        $this->organizationId = (int)$I->grabTextFrom('//*[@id="main"]/div/div/div/div/table/tbody/tr[1]/td');

        $I->amOnPage('/admin/organisations/');
        $I->canSee($designation);
        $I->canSee($street);
        $I->canSee($city);
    }

    public function checkEntities(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $designation = implode(',', $faker->words);
        $street      = implode(',', $faker->words);

        $I->createEntity($designation, $street, $this->organizationId);
        $this->entityId = (int)$I->grabTextFrom('//*[@id="main"]/div/div/div/div/table/tbody/tr[1]/td');

        $I->amOnPage('/admin/entities/');
        $I->canSee($designation);
        $I->canSee($street);
    }

    public function checkClients(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $firstName  = $faker->firstName;
        $lastName   = $faker->lastName;
        $mobile     = $faker->phoneNumber;
        $position   = $faker->word;
        $fixedPhone = $faker->phoneNumber;
        $email      = $faker->email;
        $password   = $faker->password;

        $I->createClient($firstName, $lastName, $this->entityId, 1, $mobile, $position, $fixedPhone, $email, $password);
        $this->clientId = (int)$I->grabTextFrom('//*[@id="main"]/div/div/div/div[2]/div[1]/div/dl/dd[1]');

        $I->amOnPage('/admin/clients/');
        $I->canSee($position);
        $I->canSee($email);
    }

    public function checkTrainers(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $firstName  = $faker->firstName;
        $lastName   = $faker->lastName;
        $mobile     = $faker->phoneNumber;
        $position   = $faker->word;
        $fixedPhone = $faker->phoneNumber;
        $email      = $faker->email;
        $password   = $faker->password;

        $I->createTrainer($firstName, $lastName, $this->entityId, 1, $mobile, $position, $fixedPhone, $email, $password);

        $I->amOnPage('/admin/trainers/');
        $I->canSee($firstName);
        $I->canSee($lastName);
    }

    /**
     * @param SuperAdmin $I
     */
    public function checkLearners(SuperAdmin $I)
    {
        $faker = $I->getFaker();

        $I->doLogin();
        $I->amOnPage('/admin/');

        $firstName  = $faker->firstName;
        $lastName   = $faker->lastName;
        $mobile     = $faker->phoneNumber;
        $position   = $faker->word;
        $fixedPhone = $faker->phoneNumber;
        $email      = $faker->email;
        $password   = $faker->password;

        $I->createLearner($firstName, $lastName, $this->entityId, 1, $mobile, $position, $fixedPhone, $email, $password);

        $I->amOnPage('/admin/learners/');
        $I->canSee($firstName);
        $I->canSee($lastName);
    }
}
