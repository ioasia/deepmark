<?php

use Step\Acceptance\Leaner;

/**
 * Class LearnerCest
 */
class LearnerCest
{
    /**
     * @param Leaner $I
     */
    public function checkLogin(Leaner $I)
    {
        $I->doLogin();
        $I->cantSee('Register');
        $I->cantSee('Log in');
    }

    /**
     * @param Leaner $I
     */
    public function checkSidebar(Leaner $I)
    {
        $I->doLogin();
        $I->checkSidebar();
    }
}
