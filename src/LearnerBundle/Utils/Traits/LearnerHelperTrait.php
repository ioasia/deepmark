<?php
/**
 * Created by PhpStorm.
 * User: hlsu
 * Date: 10/06/2019
 * Time: 14:14
 */

namespace LearnerBundle\Utils\Traits;

use ApiBundle\Entity\ModuleNotes;

trait LearnerHelperTrait
{
    abstract protected function getEntityManager();

    public function saveLearnerNotes($data) {
        $moduleNotes = new ModuleNotes();
        $moduleNotes->setLearnerId($data['learnerId']);
        $moduleNotes->setTrainerId($data['trainerId']);
        $moduleNotes->setModuleId($data['moduleId']);
        $moduleNotes->setLearnerNotes($data['notes']);
        $moduleNotes->setSessionId($data['bookingAgendaId']);
        $em = $this->getEntityManager();
        $em->persist($moduleNotes); //persist the object
        $em->flush(); //save it to the db
        return $moduleNotes;
    }
}