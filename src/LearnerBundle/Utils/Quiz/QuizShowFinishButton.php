<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 13.03.19
 * Time: 14:47.
 */

namespace LearnerBundle\Utils\Quiz;

use ApiBundle\Entity\NativeQuizAnswer;
//use ApiBundle\Entity\NativeQuizResult;
use Doctrine\Common\Collections\ArrayCollection;

class QuizShowFinishButton
{
    private $quizResults;

    public function __construct(array $quizResults)
    {
        $this->quizResults = $quizResults;
    }

    public function __invoke(): bool
    {
        $canNext = false;

        if (!empty($this->quizResults && $this->quizResults[0] instanceof NativeQuizResult)) {
            /** @var ArrayCollection $questions */
            $questions = $this->quizResults[0]->getQuiz()->getQuestions()->first();

            /** @var NativeQuizAnswer|null $answerCollection */
            $answerCollection = $questions->getAnswers();

            /** @var NativeQuizAnswer $answer */
            foreach ($answerCollection as $answer) {
                if (!is_null($answer->getPoints())) {
                    $canNext = true;
                    break;
                }
            }
        }

        return $canNext;
    }
}
