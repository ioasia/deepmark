<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class documentsController
 * @package LearnerBundle\Controller
 */
class documentsController extends BaseController
{
    public function indexAction()
    {
        $profile = $this->getCurrentProfile();

        $documents = $this->getDoctrine()->getRepository('ApiBundle:EducationalDocument')->findBy(array(
            'profile' => $profile,
        ));

        return $this->render('LearnerBundle:documents:index.html.twig', array(
            'documents' => $documents,
        ));
    }

    public function newAction(Request $request)
    {
        $profile = $this->getCurrentProfile();

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file           = $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);
            $document->setProfile($profile);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('learner_documents_index'));
        }

        return $this->render('LearnerBundle:documents:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
