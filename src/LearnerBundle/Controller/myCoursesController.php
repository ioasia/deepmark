<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\BookingSignature;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleAssessments;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleNotes;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\ModuleTemplate;
use ApiBundle\Entity\NativeQuizAnswer;
use ApiBundle\Entity\NativeQuizQuestion;
use ApiBundle\Entity\NativeQuizResult;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Service\Quiz\Native;
use ApiBundle\Service\QuizServiceAPI;
use Controller\BaseController;
use LearnerBundle\Utils\Traits\LearnerHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Traits\HasAttendance;
use Traits\IsAvailable;
use Traits\IsScorm;
use PhpOffice\PhpWord\Element\Field;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Element\Link;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;

class myCoursesController extends BaseController
{

    use Noticeable;
    use LearnerHelperTrait;
    use HelperTrait;
    use IsAvailable;
    use IsScorm;
    use HasAttendance;

    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getLearnerInterventionRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention');
    }

    private function getInterventionRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Intervention');
    }

    private function getModuleRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Module');
    }

    public function getTrainerAction(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $trainerId = $request->request->get('trainer_id');
        $trainer   = $em->getRepository('ApiBundle:Profile')->find($trainerId);
        $result    = array("phone" => $trainer->getMobilePhone(), 'mail' => $trainer->getPerson()->getEmail());
        $response  = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * return the profile object for the trainer user.
     *
     * @return Profile
     */
    private function getTrainerUser($uid)
    {
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['id' => $uid]);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    public function indexAction(Request $request)
    {
        $person  = $this->getCurrentUser();
        $interventionsCreated = $this->getLearnerInterventionRepository()->findBy(array(
            'learner' => $person,
            'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::LEARNER_INTERVENTION_CREATED,
            )),
        ));

        $interventionsInProgress = $this->getLearnerInterventionRepository()->findBy(array(
            'learner' => $person,
            'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS,
            )),
        ));

        $interventionsFinished = $this->getLearnerInterventionRepository()->findBy(array(
            'learner' => $person,
            'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::LEARNER_INTERVENTION_FINISHED,
            )),
        ));

        $today                 = date('Y-m-d');
        $coursesCreated        = [];
        foreach ($interventionsCreated as $interventionCreated) {
            if (strtotime($interventionCreated->getIntervention()->getEnding()->format('Y-m-d')) >= strtotime($today)) {

                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $interventionCreated->getIntervention()->getId()));
                $times         = [];
                foreach ($courseModules as $module) {
                    if (!$date = $module->getDuration()) {
                        continue;
                    }
                    $times[] = $date->format('H:i');
                }
                $totalDuration    = $this->AddPlayTime($times);
                $coursesCreated[] = array(
                    'totalDuration'       => $totalDuration,
                    'learnerIntervention' => $interventionCreated,
                );
            } else {
                $interventionsFinished[] = $interventionCreated;
            }
        }

        $coursesInProgress = [];

        foreach ($interventionsInProgress as $interventionInProgress) {
            if (strtotime($interventionInProgress->getIntervention()->getEnding()->format('Y-m-d')) >= strtotime($today)) {

                $learnersModulesNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)->getResultsByLearnerIntervention(
                    $interventionInProgress->getIntervention()->getId(),
                    $interventionInProgress->getLearner()->getId()
                );

                $total     = 0;
                $notations = 0;
                foreach ($learnersModulesNotation as $learnerModuleNotation) {
                    $notations += $learnerModuleNotation->getNotation();
                    ++$total;
                }

                $stars = $total ? $notations / $total : 0;

                $interventionInProgress->setInterventionNotation($stars);

                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $interventionInProgress->getIntervention()->getId()));
                $times         = [];
                foreach ($courseModules as $module) {
                    if (!$date = $module->getDuration()) {
                        continue;
                    }
                    $times[] = $module->getDuration()->format('H:i');
                }
                $totalDuration       = $this->AddPlayTime($times);
                $coursesInProgress[] = array(
                    'totalDuration'       => $totalDuration,
                    'learnerIntervention' => $interventionInProgress,
                );
            } else {
                $interventionsFinished[] = $interventionInProgress;
            }
        }
        $coursesFinished = [];
        foreach ($interventionsFinished as $interventionFinished) {
            $learnersModulesNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)->getResultsByLearnerIntervention(
                $interventionFinished->getIntervention()->getId(),
                $interventionFinished->getLearner()->getId()
            );

            $total     = 0;
            $notations = 0;
            foreach ($learnersModulesNotation as $learnerModuleNotation) {
                $notations += $learnerModuleNotation->getNotation();
                ++$total;
            }

            $stars = $total ? $notations / $total : 0;

            $interventionFinished->setInterventionNotation($stars);

            $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $interventionFinished->getIntervention()->getId()));
            $times         = [];
            foreach ($courseModules as $module) {
                if (!$date = $module->getDuration()) {
                    continue;
                }
                $times[] = $module->getDuration()->format('H:i');
            }
            $totalDuration     = $this->AddPlayTime($times);
            $coursesFinished[] = array(
                'totalDuration'       => $totalDuration,
                'learnerIntervention' => $interventionFinished,
            );
        }

        return $this->render('LearnerBundle:myCourses:index.html.twig', array(
            'interventions_created'     => $coursesCreated,
            'interventions_in_progress' => $coursesInProgress,
            'interventions_finished'    => $coursesFinished,
            'nextSession'               => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($person),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxAction(Request $request)
    {
        $fileId     = $request->get('id');
        $fileEntity = $this->getDoctrine()->getRepository(FileDescriptor::class)->findOneBy(['id' => $fileId]);

        $zip = new \ZipArchive();
        $res = $zip->open($fileEntity->getDirectory() . DIRECTORY_SEPARATOR . $fileEntity->getPath());

        if ($res === true) {
            $dir = $fileEntity->getDirectory() . '/scorm/' . $fileId;
            $zip->extractTo($dir);
            $zip->close();

            return $this->json([
                'iframe' => '<iframe frameBorder="0" id="scorm"  src="/files/scorm/' . $fileId . '/story_html5.html"><iframe>',
            ]);
        }
    }

    /**
     * return the intervention which the user want.
     *
     * @param $id
     * @param $quizServiceAPI
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, QuizServiceAPI $quizServiceAPI)
    {
        $profile = $this->getCurrentUser();

        $intervention        = $this->getInterventionRepository()->find($id);
        $learnerIntervention = $this->getLearnerInterventionRepository()->findOneBy(array(
            'learner'      => $profile,
            'intervention' => $intervention,
        ));

        //if (!$learnerIntervention) return $this->redirectToRoute('learner_courses_index');

        $learnersModulesNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)->getResultsByLearnerIntervention(
            $intervention->getId(),
            $profile->getId()
        );

        $total     = 0;
        $notations = 0;
        foreach ($learnersModulesNotation as $learnerModuleNotation) {
            $notations += $learnerModuleNotation->getNotation();
            ++$total;
        }

        $stars = $total ? $notations / $total : 0;

        if ($learnerIntervention) $learnerIntervention->setInterventionNotation($stars);

        $moduleAssessment = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
                        'intervention' => $intervention,
                        'storedModule' => StoredModule::ASSESSMENT,
                    ));

        $arrayChapters    = [];
        $tempModules      = [];
        $score            = null;
        $moduleIterations = [];
        $counter          = 0;

        $quizzes    = [];
        $moduleNext = null;
        $times      = [];
        $assessmentTimes = [];
        $chapters = [];
        $totalDuration = $scormData = null;

        $i = 0;
        foreach ($intervention->getChapters() as $item) {
            $chapters[] = $item;
        }
        usort($chapters, function($first,$second){
            return strcmp($first->getPosition(), $second->getPosition());
        });

        foreach ($chapters as $k => $chapter) {
            $courses          = [];
            $arrayChapters[$k]['chapter'] = $chapter;
            $chapterModules = $this->getDoctrine()->getRepository('ApiBundle:ChapterModule')->findByChapter($chapter);
            $totalDurationOfChapter = $numOfModule = 0;
            $timesOfChapter         = [];
            foreach ($chapterModules as $j => $chapterModules) {
                $i++;
                $module = $chapterModules->getModule();
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $numOfModule++;
                $timesOfChapter[]       = $module->getDuration()->format('H:i');
                $totalDurationOfChapter = $this->AddPlayTime($timesOfChapter);

                $times[]       = $module->getDuration()->format('H:i');
                $totalDuration = $this->AddPlayTime($times);
                ++$counter;
                //set status or checking status
                $this->updateStatusModule($profile, $module);
                $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));
                $status          = $moduleIteration->getStatus();
                //end set status or checking status
                $isDone = $this->isModuleDone($profile, $module);
                $module->setDone($isDone);

                $bookingModule = $this->getDoctrine()
                    ->getRepository('ApiBundle:BookingAgenda')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));

                $moduleReport = $this->getDoctrine()
                    ->getRepository('ApiBundle:ModuleReports')
                    ->findOneBy(array(
                        'module' => $module,
                    ));

                $moduleResponse = $this->getDoctrine()
                    ->getRepository('ApiBundle:ModuleResponse')
                    ->findOneBy(array(
                        'module'  => $module,
                        'learner' => $profile,
                    ));

                $moduleNote = $this->getDoctrine()
                    ->getRepository('ApiBundle:ModuleNotes')
                    ->findOneBy(array(
                        'module'  => $module,
                        'learner' => $profile,
                    ));

                if ($module->getQuiz()) {
                    $quizResults = $quizServiceAPI->getResults($module, $module->getQuiz(), $this->getUser());
                    $quizTimeSpent = $this->getTimeSpentQuiz($quizResults, $quizServiceAPI);
                    array_push($quizzes, array(
                        'quiz'   => $module->getQuiz(),
                        'result' => '-', //$result ? $result->getScore() : '-',
                        'toPass' => $module->getScoreQuiz(),
                        'score'  => 0//$score,
                    ));
                } else {
                    if ($bookingModule) {
                        $score = $bookingModule->getLearnerGrade();
                    }
                }

                // for SCORM data
                if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId() && !empty($module->getMediaDocument())
                    && !empty($module->getMediaDocument()->getFileDescriptor())
                    && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    // Scrom synthesis
                    $minScore = $module->getisNeedScoreToPass() && $module->getMinScore() ? $module->getMinScore() : null;
                    $scormData = $this->getScormSynthesis($module->getId(), $moduleIteration, $minScore);
                }

                $hasAssessment = $this->getDoctrine()->getRepository('ApiBundle:ModuleAssessments')
                    ->findOneBy(array(
                        'assessment' => $module,
                        'module'  => $moduleAssessment,
                    ));
                if($hasAssessment){
                    $assessmentTimes[] = $module->getDuration()->format('H:i');
                }

                $notation = $this->getDoctrine()
                    ->getRepository('ApiBundle:ModuleResponse')
                    ->findOneBy(['module' => $module, 'learner' => $this->getCurrentUser()]);

                $courses[$j] = [
                    'intervention'  => $intervention,
                    'module'        => $module,
                    'counter'       => $counter,
                    'booking'       => $bookingModule,
                    'status'        => $status,
                    'rating'        => $moduleResponse,
                    'iteration'     => $moduleIteration,
                    'note'          => $moduleNote,
                    'score'         => $score,
                    'report'        => $moduleReport,
                    'scorm'         => $scormData,
                    'hasAssessment' => $hasAssessment ? 1 : 0,
                    'quizTimeSpent' => isset($quizTimeSpent) ? $quizTimeSpent : null,
                    'notation'      => $notation,
                    'i' => $i
                ];

                array_push($tempModules, ['module' => $module]);
            }
            $arrayChapters[$k]['duration'] = $totalDurationOfChapter;
            $arrayChapters[$k]['numOfModule'] = $numOfModule;
            $arrayChapters[$k]['courses'] = $courses;
        }

        return $this->render('LearnerBundle:myCourses:show.html.twig', array(
            'intervention'        => $intervention,
            'learnerIntervention' => $learnerIntervention,
            'modules'             => $arrayChapters,
            'tempModules'         => $tempModules,
            'modules_iterations'  => $moduleIterations,
            'isCourses'           => true,
            'quizzes'             => $quizzes,
            'totalDuration'       => $totalDuration,
            'nextSession'         => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
            'moduleAssessment'    => $moduleAssessment,
            'totalAssessmentDuration' => $this->AddPlayTime($assessmentTimes),
            'scormData'           => $scormData
        ));
    }

    public function videoAction($id, Request $request)
    {
        $bookedModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo   = $bookedModule->getModule();
        /** @var Module $moduleToDo */
        $profile = $this->getCurrentUser();

        $intervention = $moduleToDo->getIntervention();

        $modules = $this->getDoctrine()
            ->getRepository('ApiBundle:Module')
            ->findBy(array('intervention' => $intervention->getId()));

        $courses          = [];
        $moduleIterations = [];
        $counter          = 0;
        $isNext           = false;
        $isNextSet        = false;
        $moduleNext       = null;
        $moduleAssessment = null;

        $notationModule = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleResponse')
            ->findOneBy(['module' => $moduleToDo, 'learner' => $profile]);

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                if (StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()) {
                    $moduleAssessment = $module;
                }
                continue;
            }
            if ($isNext && !$isNextSet) {
                $moduleNext = $module;
                $isNextSet  = true;
            }

            ++$counter;
            if ($module->getId() === $moduleToDo->getId()) {
                $isNext = true;
            }
            //set status or checking status
            $this->updateStatusModule($profile, $module);
            $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));
            $status          = $moduleIteration->getStatus();
            //end status

            $isDone = $this->isModuleDone($profile, $module);
            $module->setDone($isDone);

            if (!$isDone) {
                array_push($moduleIterations, ['module' => $module, 'counter' => $counter]);
            }

            $this->checkIfModuleBooked($module, $profile);

            $notation = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleResponse')
                ->findOneBy(['module' => $module, 'learner' => $this->getCurrentUser()]);

            array_push($courses, ['module' => $module, 'notation' => $notation, 'status' => $status]);
        }

        $moduleWording = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleWording')
            ->findOneBy(['module' => $moduleToDo]);

        $visio    = null;
        $isBooked = $this->checkIfModuleBooked($moduleToDo, $profile);

        $bookingModule = null;
        if (in_array($moduleToDo->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            $visio = $this->getDoctrine()->getRepository('ApiBundle:Visio')
                ->findOneBy(array(
                    'module' => $moduleToDo,
                ));

            $isBooked      = false;
            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $moduleToDo,
                ));

            if ($bookingModule) {
                $isBooked = true;
                $this->setStatusModuleIteration($profile, $moduleToDo, Status::LEARNER_INTERVENTION_IN_PROGRESS, true);
            }
        }

        $moduleNotes = $this->getDoctrine()->getRepository('ApiBundle:ModuleNotes')->findOneBy(array(
            'session' => $bookingModule,
            'module'  => $moduleToDo,
            'learner' => $profile,
        ));

        return $this->render('LearnerBundle:myCourses:video.html.twig', array(
            'intervention'           => $intervention,
            'notation'               => $notationModule,
            'modules'                => $courses,
            'module'                 => $moduleToDo,
            'next_modulenext_module' => $moduleNext,
            'visio'                  => $visio,
            'observation'            => $moduleWording,
            'isBooked'               => $isBooked,
            'isLearned'              => true,
            'booked'                 => $bookingModule,
            'isCourses'              => true,
            'canBeNotated'           => $this->canModuleBeNotated($moduleToDo, $profile),
            'notes'                  => $moduleNotes ? $moduleNotes->getLearnerNotes() : null,
            'moduleAssessment'       => $moduleAssessment,
            'nextSession'            => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
        ));
    }

    public function saveNotesAction($id, Request $request)
    {
        $data    = $request->request->all();
        $learner = $this->getCurrentUser();
        $trainer = $this->getTrainerUser($data['trainerId']);

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $data['moduleId'],
        ));

        /** @var BookingAgenda $bookingAgenda */
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $data['bookingAgendaId'],
        ));

        $moduleNotes = $this->getDoctrine()->getRepository('ApiBundle:ModuleNotes')->findOneBy(array(
            'session' => $bookingAgenda,
            'module'  => $module,
            'learner' => $learner,
        ));

        $moduleNotes = $moduleNotes ? $moduleNotes : new ModuleNotes();
        $moduleNotes->setLearner($learner);
        $moduleNotes->setTrainer($trainer);
        $moduleNotes->setModule($module);
        $moduleNotes->setSession($bookingAgenda);
        $moduleNotes->setLearnerNotes($data['notes']);
        $em = $this->getEntityManager();
        $em->persist($moduleNotes);
        $em->flush($moduleNotes);

        $response = new Response(json_encode($moduleNotes ? $moduleNotes->getId() : false));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function createModuleIteration(Profile $profile, Module $module)
    {
        $em = $this->getDoctrine()->getManager();

        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));

        if (!isset($moduleIteration)) {
            $moduleIteration = new ModuleIteration();
            $moduleIteration->setLearner($profile);
            $moduleIteration->setModule($module);
            $moduleIteration->setExecutionDate(new \DateTime());
        }

        $intervention        = $module->getIntervention();
        $interventionLearner = $em->getRepository('ApiBundle:LearnerIntervention')
            ->findOneBy(['learner' => $profile, 'intervention' => $intervention]);

        if (Status::LEARNER_INTERVENTION_CREATED == $interventionLearner->getStatus()->getAppId()) {
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);

            $interventionLearner->setStatus($status);
        }

        $progress = $this->countLearnerModuleByInterventionId($intervention, $profile);

        if (100 === $progress) {
            $interventionLearner->setStatus(
                $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::LEARNER_INTERVENTION_FINISHED,
                ))
            );
        }
        $interventionLearner->setProgression($progress);

        $em->persist($moduleIteration);
        $em->persist($interventionLearner);
        $em->flush();
    }

    private function countLearnerModuleByInterventionId($course, $learner)
    {
        $em             = $this->getDoctrine()->getManager();
        $countIteration = 0;
        $nbModule       = $em->getRepository('ApiBundle:Module')
            ->findBy(array(
                'intervention' => $course,
            ));
        foreach ($nbModule as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));
            $countIteration  = isset($moduleIteration) && !empty($moduleIteration->getStatus()) && $moduleIteration->getStatus()->getAppId() == Status::WITH_BOOKING_DONE ? $countIteration + 1 : $countIteration;
        }

        return ($countIteration / count($nbModule)) * 100;
    }

    private function checkBookingModuleDone(Profile $profile, Module $module)
    {
        $duration      = clone $module->getDuration();
        $now           = new \DateTime();
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));
        if ($bookingModule) {
            $end     = clone $bookingModule->getBookingDate();
            $hours   = intval($duration->format('H'));
            $minutes = intval($duration->format('i'));

            $interval = new \DateInterval('PT' . $hours . 'H' . $minutes . 'M');
            $end->add($interval);
            if ($now >= $end) {
                $this->createModuleIteration($profile, $module);
            }
        }
    }

    private function shouldUpdateIteration(Profile $profile, Module $module)
    {
        $moduleIterationExist = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));
        if (!$moduleIterationExist) {
            $moduleAppId = $module->getStoredModule()->getAppId();
            switch ($moduleAppId) {
                case StoredModule::ONLINE:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                default:
                    $this->createModuleIteration($profile, $module);
                    break;
            }
        }
    }

    private function updateStatusModule($profile, $module, $notShow = false)
    {
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));

        if (!isset($moduleIteration)) {
            $moduleIteration = new ModuleIteration();
            $moduleIteration->setLearner($profile);
            $moduleIteration->setModule($module);
            $moduleIteration->setLearnedTime(1);
            $moduleIteration->setExecutionDate(new \DateTime());
        }

        if (empty($moduleIteration->getStatus())) {
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_CREATED]);
            $moduleIteration->setStatus($status);
        }

        if ($notShow) {
            $moduleAppId = $module->getStoredModule()->getAppId();
            if ($moduleAppId == StoredModule::ELEARNING || $moduleAppId == StoredModule::QUIZ || $moduleAppId == StoredModule::EVALUATION) {
                //set expired status for module
                $endTime = $module->getEnding()->format('Y-m-d');
                if (date('Y-m-d') > $endTime) {
                    $status = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_EXPIRED]);
                    $moduleIteration->setStatus($status);
                } elseif ($moduleIteration->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $status = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);
                    $moduleIteration->setStatus($status);
                }
                //end set expired status for module
            }
        }

        if ($moduleIteration) {
            $em->persist($moduleIteration);
            $em->flush($moduleIteration);
        }

        return $moduleIteration;
    }

    private function isModuleDone(Profile $profile, Module $module)
    {
        $isDone          = false;
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')->findOneBy(array(
            'learner' => $profile,
            'module'  => $module,
        ));
        $appIdStatus     = $moduleIteration->getStatus()->getAppId();

        if ($appIdStatus == Status::WITH_BOOKING_DONE || $appIdStatus == Status::LEARNER_INTERVENTION_EXPIRED) {
            $isDone = true;
        } else {
            $this->shouldUpdateIteration($profile, $module);
        }
        //update status course
        $intervention        = $module->getIntervention();
        $interventionLearner = $em->getRepository('ApiBundle:LearnerIntervention')
            ->findOneBy(['learner' => $profile, 'intervention' => $intervention]);

        if(!$interventionLearner){
            return $isDone;
        }
        if (Status::LEARNER_INTERVENTION_CREATED == $interventionLearner->getStatus()->getAppId()) {
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);

            $interventionLearner->setStatus($status);
        }

        $progress = $this->countLearnerModuleByInterventionId($intervention, $profile);

        if (100 === $progress) {
            $interventionLearner->setStatus(
                $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::LEARNER_INTERVENTION_FINISHED,
                ))
            );
        }
        $interventionLearner->setProgression($progress);

        $em->persist($interventionLearner);
        $em->flush($interventionLearner);

        //end update status course
        return $isDone;
    }

    public function finishAction($id)
    {
        $profile    = $this->getCurrentUser();
        $moduleDone = $this->getModuleRepository()->find($id);

        $this->shouldUpdateIteration($profile, $moduleDone);
        return $this->redirect($this->generateUrl('learner_courses_show', ['id' => $moduleDone->getIntervention()->getId()], 0));
    }

    public function scormLmsAction($id)
    {
        $initializeCache = $this->initializeSCO($id);

        return $this->render('LearnerBundle:myCourses:scorm_lms.html.twig', array(
            'SCOInstanceID'   => $id,
            'initializeCache' => $initializeCache,
        ));
    }

    private function getMaxScoresQuiz($quizResults)
    {
        $maxScores = 0;
        foreach ($quizResults as $quizResult) {
            $scores = $quizResult->getScores();
            if($scores > $maxScores){
                $maxScores = $scores;
            }
        }

        return $maxScores;
    }

    private function getTimeSpentQuiz($quizResults, QuizServiceAPI $quizServiceAPI)
    {
        $totalTime = 0;
        foreach ($quizResults as $play) {
            $answers = $quizServiceAPI->getAnswers($play);
            if(is_array($answers['answers'])){
                foreach ($answers['answers'] as $answer) {
                    if(isset($answer->getParams()->timer)) {
                        $totalTime += $answer->getParams()->timer;
                    }
                }
            }
        }

        return $totalTime;
    }

    public function moduleAction($id, Request $request, QuizServiceAPI $quizServiceAPI)
    {
        $isCompleted = $request->query->get('complete');
        /** @var Module $moduleToDo */
        $moduleToDo = $this->getModuleRepository()->find($id);
        $profile    = $this->getCurrentUser();
        $intervention = $moduleToDo->getIntervention();
        // get moduleIteration before change status
        $moduleIterationBefore = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $moduleToDo,
            ));
        $isFirstAccess = false;
        if($moduleIterationBefore){
            $isFirstAccess = $moduleIterationBefore->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED ? true : false;
        }
        $moduleTodoIteration = $this->updateStatusModule($profile, $moduleToDo, true);

        $courseHasAssessment = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'intervention' => $intervention,
            'storedModule' => StoredModule::ASSESSMENT,
        ));

        // save the activity of profile
        $profileActivity = $this->getDoctrine()->getRepository('ApiBundle:Activities')
            ->findOneBy(array(
                'profile'      => $profile,
                'activityName' => Activities::LATEST_LEARNED_MODULE,
            ));
        if (!isset($profileActivity)) {
            $profileActivity = new Activities();
            $profileActivity->setProfile($profile);
            $profileActivity->setActivityName(Activities::LATEST_LEARNED_MODULE);
            $profileActivity->setActivityValue($moduleToDo->getId());
        } else {
            $profileActivity->setActivityValue($moduleToDo->getId());
        }
        $this->getDoctrine()->getManager()->persist($profileActivity);
        $this->getDoctrine()->getManager()->flush($profileActivity);

        if ($request->query->has('doneModule')) {
            /** @var Module $moduleDone */
            $moduleDone = $this->getDoctrine()
                ->getRepository('ApiBundle:Module')
                ->find($request->query->get('doneModule'));
            $this->shouldUpdateIteration($profile, $moduleDone);

            // set status as Done when user click next module
            $moduleIterationMustDone = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $moduleDone,
                ));

            if (isset($moduleIterationMustDone)) {
                $statusDone = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_DONE,
                ));
                $moduleIterationMustDone->setStatus($statusDone);
                $moduleIterationMustDone->setExecutionDate(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($moduleIterationMustDone);
                $em->flush($moduleIterationMustDone);
            }
        }

        $moduleAssessment = null;
        if (StoredModule::ASSESSMENT === $moduleToDo->getStoredModule()->getAppId()) {
            $moduleAssessment = $moduleToDo;
        }
        $allowEmbed = true;
        if (StoredModule::ELEARNING === $moduleToDo->getStoredModule()->getAppId() && !empty($moduleToDo->getMediaDocument())
            && !empty($moduleToDo->getMediaDocument()->getFileDescriptor())
            && $moduleToDo->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
            $fileId     = $request->get('id');
            $fileEntity = $this->getDoctrine()->getRepository(FileDescriptor::class)->findOneBy(['id' => $moduleToDo->getMediaDocument()->getFileDescriptor()->getId()]);

            $zip = new \ZipArchive();
            $res = $zip->open($fileEntity->getDirectory() . DIRECTORY_SEPARATOR . $fileEntity->getPath());
            $dir = $fileEntity->getDirectory() . '/scorm/' . $fileId;
            if ($res === true && !is_dir($dir)) {
                $zip->extractTo($dir);
                $zip->close();

            }

            if (file_exists($dir . '/imsmanifest.xml')) {
                $profileId = $this->getCurrentUser()->getId();
                $SCOdata = $this->readIMSManifestFile($dir . '/imsmanifest.xml');
                $SCOdata = reset($SCOdata);
                // updating the adlcp:masteryscore
                $this->writeElement($id, 'adlcp:masteryscore', $SCOdata['masteryscore']);
                $src = '/files/scorm/' . $fileId . '/' . $SCOdata['href'];

                $db = $this->getDoctrine()->getManager();
                // Write your raw SQL
                $query = "select VarName,VarValue from scormvars where (SCOInstanceID=$id) and (profileID = $profileId)";
                // Prepare the query from $db
                $statementDB = $db->getConnection()->prepare($query);
                // Execute both queries
                $statementDB->execute();
                $currentScormData = $statementDB->fetchAll();
            } else {
                $currentScormData = $SCOdata = null;
            }
            // Scrom synthesis
            $minScore = $moduleToDo->getisNeedScoreToPass() && $moduleToDo->getMinScore() ? $moduleToDo->getMinScore() : null;
            $scormSynthesis = $this->getScormSynthesis($id, $moduleTodoIteration, $minScore);
        } else if (StoredModule::ELEARNING === $moduleToDo->getStoredModule()->getAppId() && ($moduleToDo->getMediaDocument() && $moduleToDo->getMediaDocument()->getName() == 'Site')) {
            $allowEmbed = $this->allowEmbed($moduleToDo->getMediaDocument()->getUrl());
        }

        $notationModule = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleResponse')
            ->findOneBy(['module' => $moduleToDo, 'learner' => $this->getCurrentUser()]);

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findModulesByIntervention($intervention->getId());

        $courses          = [];
        $moduleIterations = [];
        $modulesAssessment = [];
        $counter          = 0;
        $condition        = null;
        $isNext           = false;
        $isNextSet        = false;
        $moduleNext       = null;
        $hasAssessmentModule = null;

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                if (StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()) {
                    $hasAssessmentModule = $module;
                }
                continue;
            }
            if ($isNext && !$isNextSet) {
                $moduleNext = $module;
                $isNextSet  = true;
            }

            ++$counter;
            if ($module->getId() === $moduleToDo->getId()) {
                $isNext = true;
            }

            $this->updateStatusModule($profile, $module);
            $isDone = $this->isModuleDone($profile, $module);
            $module->setDone($isDone);

            $this->checkIfModuleBooked($module, $profile);

            $notation = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleResponse')
                ->findOneBy(['module' => $module, 'learner' => $this->getCurrentUser()]);

            $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));

            // is Assessment module
            if ($moduleAssessment) {
                $score             = 0;
                $scoreToPassOfQuiz = 0;
                $isCompleted = $scoreToPass = $timeSpent = 1;

                $status = $moduleIteration->getStatus();
                $bookingModule = $this->getDoctrine()
                    ->getRepository('ApiBundle:BookingAgenda')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));

                if ($module->getQuiz()) {
                    // to do: implement the new quiz
                    //$score = $scoreToPass = 0;
                    /*if ('NATIVE' === $module->getQuiz()->getType()) {
                        $result = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                            ->findOneBy([
                                'student' => $this->getUser(),
                                'quiz'    => $module->getQuiz(),
                            ]);
                        $score = $result ? round(($result->getScore() / $module->getQuiz()->getTotalPoints()) * 100,2) : 0;
                        $scoreToPass = !empty($result) && $score >= $result->getScore() ? 1 : 0;
                    }*/
                    $quizResults = $quizServiceAPI->getResults($module, $module->getQuiz(), $this->getUser());
                    $score = $this->getMaxScoresQuiz($quizResults);
                    $quizTimeSpent = $this->getTimeSpentQuiz($quizResults, $quizServiceAPI);
                } else {
                    if ($bookingModule) {
                        $score = $bookingModule->getLearnerGrade();
                    }
                }

                // for SCORM data
                $scormData  = null;
                if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId() && !empty($module->getMediaDocument())
                    && !empty($module->getMediaDocument()->getFileDescriptor())
                    && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    // Scrom synthesis
                    $minScore = $module->getisNeedScoreToPass() && $module->getMinScore() ? $module->getMinScore() : null;
                    $scormData = $this->getScormSynthesis($module->getId(), $moduleIteration, $minScore);
                    if ($scormData) {
                        $score = $scormData['score'];
                    }
                }

                $condition = $this->getConditions($module, $moduleAssessment);
                $timeSpentRequire = 0;
                if ($condition) {
                    // check completed for all modules
                    if (!empty($condition->getCompleted()) && $condition->getCompleted() == 1) {
                        $isCompleted = $moduleIteration->getStatus()->getId() == Status::DONE ? 1 : 0;
                    }
                    // check score to pass for Online, Quiz and SCORM module
                    if (!empty($condition->getScoreToPass()) && $condition->getScoreToPass() == 1) {
                        if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                            $scoreToPass = $score >=  $module->getMinScore() ? 1 : 0;
                        } elseif ($module->getQuiz()) {
                            if($module->getQuiz()->getIsSurvey()){
                                $scoreToPass = 1;
                            }else{
                                $scoreToPass = $score >= $module->getScoreQuiz() ? 1 : 0;
                            }
                        } elseif (!empty($scormData)) {
                            $scoreToPass = $scormData['lessonStatus'] == 'passed' ? 1 : 0;
                        }
                    }
                    // check spent time
                    if (!empty($condition->getTimeSpent())) {
                        if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId() ||
                            StoredModule::EVALUATION === $module->getStoredModule()->getAppId()) {
                            $hour    = $module->getDuration()->format('G');
                            $minutes = $module->getDuration()->format('i');
                            $timeSpentRequire = ($condition->getTimeSpent()/100 * ($minutes*60 + $hour*360));
                            if ($moduleIteration->getLearnedTime() >= $timeSpentRequire) {
                                $timeSpent = 1;
                            } else {
                                $timeSpent = 0;
                            }
                        }
                    }
                }

                if ($condition && !empty($condition->getOperationOne())) {
                    $result = $condition->getOperationOne() == 'and' ? $isCompleted && $scoreToPass : $isCompleted || $scoreToPass;
                } else {
                    $result = $isCompleted || $scoreToPass;
                }
                // time spent was checked yes
                if($condition && !empty($condition->getTimeSpentCheck())) {
                    if (!empty($condition->getOperationTwo())) {
                        $result = $condition->getOperationTwo() == 'and' ? $result && $timeSpent : $result || $timeSpent;
                    } else {
                        $result = $result || $timeSpent;
                    }
                }

                // in case quiz module has timeSpent and score is passed -> validated
                $exceptionPass = 0;
                if ($module->getQuiz() && !$module->getQuiz()->getIsSurvey()) {
                    $passScore = $score >= $module->getScoreQuiz() ? 1 : 0;
                    if(!empty($quizTimeSpent) && $passScore == 1){
                        $exceptionPass = 1;
                    }
                }

                $hasAssessment = $this->getDoctrine()->getRepository('ApiBundle:ModuleAssessments')
                    ->findOneBy(array(
                        'assessment' => $module,
                        'module'  => $moduleAssessment,
                    ));
                $dataModule = [
                    'module'        => $module,
                    'booking'       => $bookingModule,
                    'status'        => $status,
                    'iteration'     => $moduleIteration,
                    'score'         => $score,
                    'condition'     => $condition ? $condition : null,
                    'result'        => $result,
                    'quizTimeSpent' => isset($quizTimeSpent) ? $quizTimeSpent : null,
                    'timeSpentRequire' => $timeSpentRequire,
                    'exceptionPass' => $exceptionPass
                ];
                if($hasAssessment) {
                    $dataModule['weight'] = $hasAssessment->getWeight();
                    array_push($modulesAssessment, $dataModule);
                }

                array_push($moduleIterations, $dataModule);
            }

            array_push($courses, ['module' => $module, 'notation' => $notation, 'status' => $moduleIteration->getStatus()]);
        }
        $moduleWording = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleWording')
            ->findOneBy(['module' => $moduleToDo]);

        $visio    = null;
        $isBooked = $this->checkIfModuleBooked($moduleToDo, $profile);

        $bookingModule = null;
        if (in_array($moduleToDo->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            $visio = $this->getDoctrine()->getRepository('ApiBundle:Visio')
                ->findOneBy(array(
                    'module' => $moduleToDo,
                ));

            $isBooked      = false;
            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $moduleToDo,
                ));

            if ($bookingModule) {
                $isBooked = true;
                if ($moduleToDo->getStoredModule()->getAppId() == StoredModule::PRESENTATION_ANIMATION) {
                    $this->setStatusModuleIteration($profile, $moduleToDo, Status::LEARNER_INTERVENTION_IN_PROGRESS,
                        true);
                }
            }
        }
        $quizResultsAndAnswers = null;
        $quizResults           = null;
        $quizSteps             = null;
        $quizResolver          = null;
        $canNext               = false;
        $canBeNavigation       = true;
        $programs              = $organizationId = $applicationId = $token = null;

        if ($moduleToDo->getQuiz()) {
            $quizSteps   = $quizServiceAPI->sanitizeQuiz($moduleToDo->getQuiz(), true);
            $quizResults = $quizServiceAPI->getResults($moduleToDo, $moduleToDo->getQuiz(), $this->getUser());
            if ($quizResults) {
                foreach ($quizResults as $play) {
                    $answers                 = $quizServiceAPI->getAnswers($play);
                    $quizResultsAndAnswers[] = array(
                        'quizResult'      => $play,
                        'steps'           => $answers['steps'],
                        'questions'       => $answers['questions'],
                        'answers'         => $answers['answers'],
                        'answers_status'  => $answers['status'],
                        'scores'          => $answers['scores'],
                        'total_points'    => $answers['total_points'],
                        'total_questions' => $answers['total_questions'],
                    );
                }
                $canNext = true;
            }
        } else {
            if ($moduleToDo->getEchoModule()) {
                try {
                    $organizationId = $this->getParameter('echo_organization_id');
                    $applicationId  = $this->getParameter('echo_application_id');

                    // example to get token: https://integration.surge9.com/api/integration/C6DB6CE0-4609-4BC5-9BF9-AC40C1D78963/token/login
                    $url    = $this->getParameter('echo_url') . '/api/integration/' . $organizationId . '/token/login';
                    $client = new \GuzzleHttp\Client([
                        'headers' => [
                            'x-api-key'    => $this->getParameter('echo_api_key'),
                            'Content-Type' => 'application/json',
                        ],
                    ]);

                    $response = $client->request('POST', $url, [
                        'body' => '{"email": "' . $profile->getPerson()->getEmail() . '","firstName": "' . $profile->getFirstName() . '","lastName": "' . $profile->getLastName() . '"}',
                    ]);

                    $token = json_decode($response->getBody(), true);

                    // Get Program: https://integration.surge9.com/api/integration/C6DB6CE0-4609-4BC5-9BF9-AC40C1D78963/4/programs?programId=70587823
                    $urlProgram      = $this->getParameter('echo_url') . '/api/integration/' . $organizationId . '/' . $applicationId . '/programs?programId=' . $moduleToDo->getEchoModule()->getEchoId();
                    $responseProgram = $client->request('GET', $urlProgram);
                    $programs        = json_decode($responseProgram->getBody(), true);
                    // if the status of module is not done then show show the next module button
                    /* if (isset($moduleIteration) && ($moduleIteration->getStatus()->getAppId() != Status::WITH_BOOKING_DONE)) {
                      $canBeNavigation = false;
                      } */
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        $assignments = $this->getAssignments($moduleToDo);
        $isCompleted = isset($isCompleted) && $isCompleted != '' ? true : false;
        // Set course status
        $isLearned    = false;
        $emailTrainer = $isAvailable = $notationTrainer = $moduleReport = $moduleNote = $moduleAttendanceReport = null;
        if ($bookingModule) {
            $today   = new \DateTime();
            $module  = $bookingModule->getModule();
            $begin   = $bookingModule->getBookingDate();
            $end     = clone $bookingModule->getBookingDate();
            $hour    = $module->getDuration()->format('G') + $bookingModule->getBookingDate()->format('G');
            $minutes = $module->getDuration()->format('i') + $bookingModule->getBookingDate()->format('i');
            $end->setTime($hour, $minutes, 0);
            $emailTrainer = $bookingModule->getTrainer()->getPerson()->getEmail();

            if ($today < $begin) {
                $isLearned = false;
            } else {
                $isLearned = ($bookingModule->getStatus()->getAppId() == Status::WITH_BOOKING_DONE) ? true : false;
            }

            // the module will be expired when the today - the booking date + the duration time > 24 hrs
            $isAvailable = $this->isAvailableBooking($moduleToDo, $bookingModule);

            $notationTrainer = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleTrainerResponse')
                ->findOneBy(['module' => $moduleToDo, 'trainer' => $bookingModule->getTrainer()]);
            $moduleReport    = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')
                ->findOneBy(['module' => $moduleToDo, 'trainer' => $bookingModule->getTrainer(), 'status' => ModuleReports::STATUS_APPROVED]);
            $moduleAttendanceReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleAttendanceReport')->findOneBy(array(
                'module' => $moduleToDo, 'trainer' => $bookingModule->getTrainer()
            ));
        } else {
            $isAvailable = $this->isAvailableModule($moduleToDo);
        }

        // is Assessment module
        $certification = true;
        $reportAssessments = true;
        if ($moduleAssessment) {
            if ($modulesAssessment) {
                $totalDuration = 0;
                $averageScore = 0;
                $totalModuleWithWeight = 0;
                foreach ($modulesAssessment as $item) {
                    $weight = 1;
                    if(!empty($item['weight'])){
                        $weight = $item['weight'];
                    }
                    $totalModuleWithWeight += $weight;
                    if (empty($item['condition']->getScoreToPass())){// no require score to pass
                        $item['score'] = 100; // %
                    }
                    $averageScore += $item['score'] * $weight;
                    if($item['module']->getDuration()){
                        $hour    = $item['module']->getDuration()->format('G');
                        $minutes = $item['module']->getDuration()->format('i');
                        $totalDuration += ($hour + $minutes/60);
                    }
                    if (!($item['status']->getAppId() == Status::WITH_BOOKING_DONE && $item['result'] == 1)) {
                        $certification = false;
                        $reportAssessments = false;
                    }
                }
                $averageScore = $averageScore/$totalModuleWithWeight;

                if ($certification) {
                    // the default the template
                    $template = 'certification_sample_1.docx';
                    $dirTemplate = __DIR__ . '/../../../web/files/assessment/certification_templates/';
                    $dir = __DIR__ . '/../../../web/files/';

                    // Template processor instance creation
                    $moduleTemplate = $this->getDoctrine()->getRepository('ApiBundle:ModuleTemplate')->findOneBy(array(
                        'module' => $moduleToDo, 'appId' => ModuleTemplate::CERTIFICATE_TEMPLATE
                    ));

                    $assessment = $moduleAssessment->getAssessments()[0];
                    // get template from selection
                    if(!empty($assessment->getCertificationTemplate())){
                        $template = $assessment->getCertificationTemplate().'.docx';
                    }
                    else if ($moduleTemplate) { //get template uploaded
                        $template = $moduleTemplate->getfileDescriptor()->getPath();
                        $dir = $moduleTemplate->getfileDescriptor()->getDirectory();
                        $dirTemplate = $dir;
                    }

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($dirTemplate . $template);
                    $fontSize = 13;
                    $fullName = new TextRun();
                    $fullName->addText($profile->getDisplayName(), ['bold' => true, 'size'=> $fontSize]);
                    $templateProcessor->setComplexBlock('fullName', $fullName);

                    $courseName = new TextRun();
                    $courseName->addText($intervention->getDesignation(), array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('courseName', $courseName);

                    $beginning = new TextRun();
                    $beginning->addText($intervention->getBeginning()->format('d/m/Y'), array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('beginning', $beginning);

                    $ending = new TextRun();
                    $ending->addText($intervention->getEnding()->format('d/m/Y'), array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('ending', $ending);

                    $hours = new TextRun();
                    $hours->addText(round($totalDuration, 2), array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('hours', $hours);

                    $score = new TextRun();
                    $score->addText($averageScore."%", array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('score', $score);

                    $today = new TextRun();
                    $today->addText(date("d/m/Y"), array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('today', $today);

                    $signName = new TextRun();
                    $signName->addText('Jimmy SOUVANNARATH', array('bold' => true, 'size'=> $fontSize));
                    $templateProcessor->setComplexValue('signName', $signName);

                    $signTitle = new TextRun();
                    $signTitle->addText('Directeur Général', array('bold' => true, 'size' => $fontSize));
                    $templateProcessor->setComplexValue('signTitle', $signTitle);

                    $signature = $profile->getEntity()->getSignature();
                    if ($signature != '' && file_exists(__DIR__ . '/../../../web'.$signature)){
                        $templateProcessor->setImageValue('sign', array('path' => __DIR__ . '/../../../web'.$signature, 'width' => 140, 'ratio' => true));
                    }else{
                        $sign = new TextRun();
                        $sign->addText('', array('bold' => true));
                        $templateProcessor->setComplexValue('sign', $sign);
                    }

                    $logoUrl = $profile->getEntity()->getOrganisation()->getLogo();
                    if(!file_exists(__DIR__ . '/../../../web'.$logoUrl)){
                        $logoUrl = '/img/LogoIO.jpg';
                    }

                    $templateProcessor->setImageValue('logo', array('path' => __DIR__ . '/../../../web'.$logoUrl, 'width' => 120, 'height' => 120, 'ratio' => true));

//                    $link = new Link($this->generateUrl('learner_courses_show', ['id' => $intervention->getId()], 0));
//                    $templateProcessor->setComplexValue('link', $link);
                    $fileName = str_replace(['(', ')', '/'], '_', 'Certification_' . $profile->getDisplayName().'_'.$intervention->getDesignation());
                    $outputFileName =  $fileName. '.' . pathinfo($template, PATHINFO_EXTENSION);
                    $outputPDFFileName = $fileName . '.pdf';
                    $templateProcessor->saveAs($dir . $outputFileName);

                    $outputPdfName = $this->convertToPDF($dir . $outputFileName, $dir);
                    $this->pushCertificateToS3($outputPdfName);
                }
            }
        }

        $nextAvailableSessionDate = "";
        if($moduleToDo->getSessions()) {
            $currentDateTime = new \DateTime();
            foreach ($moduleToDo->getSessions() as $session) {
                $start = $session->getSession()->getSessionDate();
                if ($start > $currentDateTime) {
                    $nextAvailableSessionDate = $start->format('Y-m-d');
                    break;
                }
            }
        }

        $moduleNote      = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleNotes')
            ->findOneBy(['module' => $moduleToDo, 'learner' => $profile]);

        // Start chapter bar
        $arrayChapters = $tempModules = [];
        $i = 0;
        $chapters = [];
        foreach ($intervention->getChapters() as $item) {
            $chapters[] = $item;
        }
        if ($chapters) {
            usort($chapters, function($first,$second){
                return strcmp($first->getPosition(), $second->getPosition());
            });
        }

        foreach ($chapters as $k => $chapter) {
            $list          = [];
            $arrayChapters[$k]['chapter'] = $chapter;
            $chapterModules = $this->getDoctrine()->getRepository('ApiBundle:ChapterModule')->findByChapter($chapter);
            foreach ($chapterModules as $j => $chapterModules) {
                $i++;
                $module = $chapterModules->getModule();
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }

                $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));
                $status          = $moduleIteration->getStatus();

                $list[$j] = [
                    'intervention'  => $intervention,
                    'module'        => $module,
                    'status'        => $status,
                    'i' => $i
                ];

                array_push($tempModules, ['module' => $module]);
            }
            $arrayChapters[$k]['courses'] = $list;
        }
        // end chapter

        return $this->render('LearnerBundle:myCourses:module.html.twig', array(
            'intervention'          => $intervention,
            'src'                   => isset($src) ? $src : null,
            'currentScormData'      => isset($currentScormData) ? $currentScormData : null,
            'SCOdata'               => isset($SCOdata) ? $SCOdata : null,
            'scormSynthesis'        => isset($scormSynthesis) ? $scormSynthesis : null,
            'chapters'              => $arrayChapters,
            'modules'               => $courses,
            'status'                => $moduleTodoIteration->getStatus(),
            'isFirstAccess'         => $isFirstAccess,
            'moduleIteration'       => $moduleTodoIteration,
            'module'                => $moduleToDo,
            'expired'               => !$isAvailable,
            'next_module'           => $moduleNext,
            'notation'              => $notationModule,
            'notationTrainer'       => $notationTrainer,
            'report'                => $moduleReport,
            'attendanceReportFile'  => $moduleAttendanceReport ? $moduleAttendanceReport : null,
            'notes'                 => $moduleNote ? $moduleNote->getLearnerNotes() : null,
            'visio'                 => $visio,
            'observation'           => $moduleWording,
            'isBooked'              => $isBooked,
            'isLearned'             => $isLearned,
            'booked'                => $bookingModule,
            'emailTrainer'          => $emailTrainer,
            'quizResolver'          => $quizResolver,
            'isCourses'             => true,
            'isCompleted'           => $isCompleted,
            'quizSteps'             => $quizSteps,
            'quizResults'           => $quizResults,
            'quizResultsAndAnswers' => $quizResultsAndAnswers,
            'assignments'           => $assignments,
            'canNext'               => $canNext,
            'canBeNotated'          => $this->canModuleBeNotated($moduleToDo, $profile),
            'echoUrl'               => isset($programs['programs'][0]['url']) ? $programs['programs'][0]['url'] : '',
            'echoToken'             => isset($token['token']) ? $token['token'] : null,
            'echoLoginUrl'          => $this->getParameter('echo_login_url') . '/user/api/' . $organizationId . '/auth/' . $applicationId,
            'canBeNavigation'       => $canBeNavigation,
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
            'moduleAssessment'      => $hasAssessmentModule,
            'modules_iterations'    => $moduleIterations,
            'modulesAssessment'     => $modulesAssessment,
            'outputFileName'        => isset($outputPDFFileName) ? $outputPDFFileName : null,
            'reportAssessments'     => $reportAssessments || false,
            'courseHasAssessment'   => $courseHasAssessment,
            'allowEmbed'            => $allowEmbed,
            'tempModules'           => $tempModules,
            'nextSessionDate'       => $nextAvailableSessionDate
        ));
    }

    /**
     * push file to s3 when generate pdf completed
     * @param $outputFileName
     */
    private function pushCertificateToS3($outputFileName)
    {
        $times = 0;
        while ($times < 10) {
            if(file_exists($outputFileName)){
                try {
                    $storage = $this->container->get('storage');
                    $storage->uploadFile($outputFileName);
                } catch (\Exception $e) {
                    break;
                }
                break;
            }
            $times++;
        };

    }

    private function convertToPDF($inputFile, $outputPath)
    {
        exec('export HOME=' . $outputPath . ' && soffice --headless --convert-to pdf "' . $inputFile . '" --outdir ' . $outputPath);
        $inputFile = str_replace('.docx', '.pdf', $inputFile);

        if (file_exists($outputPath.'/'.'.config')) {
            exec('rm -rf ' .$outputPath.'/'.'.config');
        }
        return $inputFile;
    }

    private function getConditions(Module $module, Module $assessment)
    {
        $return = $this->getDoctrine()->getRepository(ModuleAssessments::class)
            ->findOneBy(['module' => $assessment, 'assessment' => $module]);

        return $return;
    }

    private function getAssignments(Module $module)
    {
        $return = null;
        if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
            $return = $this->getDoctrine()->getRepository(AssigmentResourceSystem::class)
                ->findBy(array(
                    'module' => $module,
                ));
        }

        return $return;
    }

    private function checkIfModuleBooked(Module $module, $profile)
    {
        if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));

            if ($bookingModule) {
                $module->setIsBooked(true);
            } else {
                $module->setIsBooked(false);
            }
        }
    }

    function AddPlayTime($times)
    {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours   = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    public function saveAttendanceAction(Request $request)
    {
        $attendances = $request->request->get('attendances');
        $type = $request->request->get('type');
        $this->saveAttendance($attendances, $type);

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function saveSignatureAction(Request $request)
    {
        return $this->saveSignature($request);
    }

    public function checkSignatureAction(Request $request)
    {
        $allow = 0;
        $signatureType = null;
        $bookingId      = $request->request->get('bookingId');
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $bookingId,
        ));
        // find the signature parameters
        $params = $bookingAgenda->getSignatureParams();
        if ($params) {
            foreach ($params as $k => $param) {
                if ($param == 1) {
                    $allow = 1;
                    $signatureType = $k;
                }
            }
        }

        // find the before signed
        $signatures = $bookingAgenda->getBookingSignatures();
        if ($signatures) {
            foreach ($signatures as $signature) {
                if ($allow && $signatureType) {
                    $allow = $signature->getSignatureType() ==  $signatureType ? 0 : 1;
                }
            }
        }
        $moduleSignatureParams = $bookingAgenda->getModule()->getSignatureParameters();

        $response = new Response(json_encode(array(
            'bookId' => $bookingId,
            'allow' => $allow,
            'signatureType' => $signatureType,
            'learnerName' => $bookingAgenda->getLearner()->getDisplayName(),
            'signatureName' => $signatureType ? $moduleSignatureParams[$signatureType-1]['signatureName'] : ''
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function checkNewRoomAction(Request $request)
    {
        $bookingId      = $request->request->get('bookingId');
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $bookingId,
        ));

        $response = new Response(json_encode(array(
            'bookId' => $bookingAgenda->getId(),
            'roomId' => $bookingAgenda->getRoom()->getId(),
            'roomName' => $bookingAgenda->getRoom()->getRoomName()
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getAttendanceContentAction(Request $request)
    {
        $id = $request->request->get('id');
        $absent = $request->request->get('absent');
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo    = $bookingModule->getModule();
        $profile       = $this->getCurrentUser();

        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookingModule);
        } else {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $moduleToDo,
                'learner' => $profile
            ));
        }

        return $this->render('LearnerBundle:myCourses:ajax-attendance-sheet.html.twig', array(
            'module'                 => $moduleToDo,
            'booked'                 => $bookingModule,
            'bookingAgendas'         => $bookingAgendas,
            'absent'                 => $absent,
        ));
    }
}
