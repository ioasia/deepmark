<?php

namespace LearnerBundle\Controller;

use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CatalogController
 * @package LearnerBundle\Controller
 */
class CatalogController extends BaseController
{
    public function indexAction(Request $request)
    {
        $tagsQuery = $request->query->get('tags') ? explode(",", mb_strtoupper($request->query->get('tags'))) : null;
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // prepare the search parameters
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        // get entities
        $organization = $profile->getEntity()->getOrganisation();
        $searchParam['organization'] = $organization;
        $entities = $this->getDoctrine()->getRepository('ApiBundle:Entity')->findBy(array('organisation' => $organization));
        $searchParam['entities'] = $entities;

        // get tags
        $searchParam['tags'] = [];
        $tags         = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        foreach ($tags as $tag){
            $searchParam['tags'][$tag->getId()] = $tag->getTagName();
        }

        return $this->render('LearnerBundle:Catalog:index.html.twig', array(
            'searchParam' => $searchParam,
            'tagsQuery' => $tagsQuery
        ));
    }
}
