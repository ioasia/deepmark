<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class myPlanningController extends Controller
{
    public function indexAction()
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        /*$workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);*/

        return $this->render('LearnerBundle:myPlanning:index.html.twig', array(
            //'workingHours'  => $workingHours,
            'profile'       => $profile,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }
}
