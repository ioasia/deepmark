<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\StoredModule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Service\QuizServiceAPI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExercicesController extends Controller
{
    public function indexAction()
    {
        return $this->render('LearnerBundle:exercices:index.html.twig', array(
            'interventions' => [],
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id, Request $request, QuizServiceAPI $quizServiceAPI)
    {
        $person = $this->getUser();
        /** @var Profile $profile */
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($id);
        $questionItems = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'personalized_correction');
        $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
        $items = [];
        foreach ($questionItems as $k => $questionItem) {
            $items[$k]['item'] = $questionItem;
            $mark = $this->getDoctrine()->getRepository('ApiBundle:Marks')
                ->findBy(array(
                    'exercises' => $answer->getExercises(),
                    'questionsItem' => $questionItem,
                ));
            $items[$k]['mark'] = $mark;
        }

        // get step and total step
        $quizSteps = $quizServiceAPI->sanitizeQuiz($answer->getPlay()->getQuiz());
        $step = 1;
        foreach ($quizSteps as $k => $quizStep) {
            foreach ($quizStep['questions'] as $question) {
                if ($question->getId() == $answer->getQuestion()->getId()) {
                    $step = $k + 1;
                    break;
                }
            }
        }
        $tags = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findAll();

        return $this->render('TrainerBundle:myExercices:mark.html.twig', array(
            'totalStep'             => count($quizSteps),
            'step'                  => $step,
            'answer'                => $answer,
            'isSubmitted'           => 1,
            'questionMarkItems'     => $items,
            'defaultCorrection'     => $defaultCorrection,
            'templateFile'          => null,
            'reportFile'            => null,
            'tags'                  => $tags,
            'nextSession'         => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
        ));
    }

    /**
     * @return Response
     */
    public function myExercicesAction()
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // get validated answers
        $exercisesValidateds = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatusAndProfile('open', Exercises::STATUS_APPROVED, $person);
        $exercisesToValidatedUsers = [];
        foreach ($exercisesValidateds as $exercisesValidated) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesValidated->getPlay()->getUser_id()));
            $exercisesToValidatedUsers[$exercisesValidated->getId()] = $player;
        }

        return $this->render('LearnerBundle:exercices:my-exercices.html.twig', array(
            'exercisesValidateds'          => $exercisesValidateds,
            'exercisesToValidatedUsers'    => $exercisesToValidatedUsers,
            'nextSession'                  => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile)
        ));
    }

    /**
     * @param $id
     * @return Response
     * print attendance sheet for individual learner
     */
    public function printAttendanceSheetAction($id)
    {
        $person       = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo    = $bookingModule->getModule();
        $trainer = $bookingModule->getTrainer();
        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookingModule);
            $trainerSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                'profile'  => $trainer
            ), array('signatureType' => 'ASC'));
        } else {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $moduleToDo,
                'trainer' => $trainer,
                'learner' => $profile,
                'moduleSession' => $bookingModule->getModuleSession()
            ));
            $trainerSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                'profile'  => $trainer,
                'session' => $bookingModule->getModuleSession()->getSession()
            ), array('signatureType' => 'ASC'));
        }

        return $this->render('LearnerBundle:exercices:print-attendance-sheet.html.twig', array(
            'booked'         => $bookingModule,
            'module'         => $moduleToDo,
            'bookingAgendas' => $bookingAgendas,
            'trainerSignatures' => $trainerSignatures
        ));
    }
}
