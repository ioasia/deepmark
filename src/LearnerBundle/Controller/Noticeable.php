<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;

trait Noticeable
{
    protected function canModuleBeNotated(Module $module, Profile $profile)
    {
        $intervention = $module->getIntervention();
        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy([
            'intervention' => $intervention,
        ]);
        $sequent = $intervention->getSequential();
        /** @var Module $moduleToMark */
        if ($sequent) {
            foreach ($modules as $moduleToMark) {
                if ($moduleToMark->getId() >= $module->getId() || $this->isNotDisplayedModule($moduleToMark)) {
                    continue;
                }

                $moduleStatus = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')->findOneBy([
                    'module' => $moduleToMark, 'learner' => $profile,
                ]);
                if($moduleStatus->getStatus()->getId() != Status::DONE) {
                    return false;
                }
                /*$isMark = $this->getDoctrine()->getRepository('ApiBundle:ModuleResponse')->findOneBy([
                    'module' => $moduleToMark, 'learner' => $profile,
                ]);
                if ($isMark == null) {
                    return false;
                }*/

            }
        }
        return true;
    }

    private function isNotDisplayedModule(Module $module)
    {
        return in_array(
            $module->getStoredModule()->getAppId(),
            [StoredModule::CONCEPTION, StoredModule::PILOTAGE, StoredModule::TEAM_PREPARATION]
        );
    }
}
