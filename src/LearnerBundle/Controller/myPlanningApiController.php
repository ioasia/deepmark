<?php

namespace LearnerBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\AvailableCalendar;
use AppBundle\Entity\Person;
use AppBundle\Service\CalendarApi;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Traits\IsAvailable;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;

class myPlanningApiController extends RestController
{
    use IsAvailable;
    /**
     * @Rest\Get("calendars")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     *
     * @throws Exception
     * @Rest\QueryParam(name="dateStart", nullable=true, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=true, description="Date End")
     * @Rest\QueryParam(name="learnerId", nullable=true, description="Learner Id")
     * @Rest\QueryParam(name="typeModule", nullable=true, description="Module Type")
     */
    public function getAvailabilitiesLearnerAction(ParamFetcher $paramFetcher)
    {
        $now = new DateTime();
        $learnerId = $paramFetcher->get('learnerId') ? $paramFetcher->get('learnerId') : null;
        $moduleType = $paramFetcher->get('typeModule') ? $paramFetcher->get('typeModule') : null;
        $calendar_array = $calendar_my_booking = [];
        if ($paramFetcher->get('dateStart')) {
            $dateStart = new DateTime($paramFetcher->get('dateStart'));
        } else {
            // first day of current month
            $dateStart = clone $now;
            $dateStart = $dateStart->modify('first day of this month');
        }
        if ($paramFetcher->get('dateEnd')) {
            $dateEnd = new DateTime($paramFetcher->get('dateEnd'));
        } else {
            // last day of month
            $dateEnd = clone $now;
            $dateEnd = $dateEnd->modify('last day of this month');
        }

        $calendarApi = $this->get(CalendarApi::class);
        $em = $this->getDoctrine()->getManager();
        $person  = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        if (!$profile) {
            return $this->errorHandler();
        }

        $status = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findBy(['appId' => [
                Status::LEARNER_INTERVENTION_CREATED,
                Status::LEARNER_INTERVENTION_IN_PROGRESS,
            ]]);

        $roles   = $person->getRoles();
        // The dashboard for admin part: If user as Admin role then the calendar will show all session of all learners with Created and In Progress status
        if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array(
                'status' => $status,
            ));
        } else {
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array(
                'learner' => $profile,
                'status' => $status,
            ));
        }
        $moduleIds = [];
        foreach ($learnerInterventions as $learnerIntervention) {
            $intervention = $learnerIntervention->getIntervention();
            $modules = $em->getRepository('ApiBundle:Module')->findBy(array(
                'intervention' => $intervention,
            ));
            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                if(!in_array($module->getId(),$moduleIds)){
                    $moduleIds[] = $module->getId();
                }
            }
        }
        $modulesMonth = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByRange($moduleIds, $dateStart, $dateEnd);
        foreach ($modulesMonth as $module) {
            if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                $booking = $calendarApi->getMyBookingsByModule($profile, $module, clone $dateStart, clone $dateEnd);
                if($moduleType){
                    if ($module->getStoredModule()->getAppId() == $moduleType) {
                        if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
                            if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                                $calendar_my_booking = array_merge($calendar_my_booking, $booking);
                            } else {
                                $calendar_array = $calendar_array + $booking;
                            }
                        } else {
                            $calendar_array = array_merge($calendar_array, $booking);
                        }
                    }

                } else {
                    // The dashboard for admin part: If user as Admin role then the calendar will show duplication session. So will process it to avoid duplicate session
                    if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
                        if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                            $calendar_my_booking = array_merge($calendar_my_booking, $booking);
                        } else {
                            $calendar_array = $calendar_array + $booking;
                        }
                    } else {
                        $calendar_array = array_merge($calendar_array, $booking);
                    }
                }

            }
        }

        //$calendar_array = $calendarApi->getBookingsByTrainer($profile, $dateStart, $dateEnd);
        $calendar_array = array_merge($calendar_my_booking, $calendar_array);
        $calendar_array = array_merge($calendar_array,
            $calendarApi->getAvailabilityByTrainer($profile, $dateStart, $dateEnd));

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @Rest\Post("add")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="startTimeAfternoon", nullable=true, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="endTimeAfternoon", nullable=true, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="startTime", nullable=true, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="endTime", nullable=true, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function addCalendarsLearnerAction(ParamFetcher $paramFetcher)
    {
        /** @var Person $user */
        $user = $this->getUser();

        $beginning = new DateTime($paramFetcher->get('beginning'), $user->getDateTimeZone());
        $ending    = new DateTime($paramFetcher->get('ending'), $user->getDateTimeZone());

        $startTime = $paramFetcher->get('startTime');
        $endTime   = $paramFetcher->get('endTimeAfternoon');

        $freezeTime = $paramFetcher->get('endTime') . '|' . $paramFetcher->get('startTimeAfternoon');

        $profileId = $paramFetcher->get('profileId') ?: null;

        if ($profileId) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'id' => $profileId,
            ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'person' => $user->getId(),
            ));
        }

        $availableCalendarExist = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($profile, $beginning, $ending);

        if ($availableCalendarExist) {
            return $this->alreadyExist('availability already exist');
        }

        $period = new DatePeriod(
            $beginning,
            DateInterval::createFromDateString('1 day'),
            $ending
        );

        $availableCalendars = [];

        foreach ($period as $value) {
            /** @var DateTime $dayBegin */
            $dayBegin = clone $value;
            list($hours, $minutes) = explode(':', $startTime);

            $dayBegin->add(DateInterval::createFromDateString("$hours hours $minutes minutes"));

            /** @var DateTime $dayEnd */
            $dayEnd = clone $value;
            list($hours, $minutes) = explode(':', $endTime);

            $dayEnd->add(DateInterval::createFromDateString("$hours hours $minutes minutes"));

            $availableCalendar = new AvailableCalendar();
            $availableCalendar->setBeginning($dayBegin);
            $availableCalendar->setEnding($dayEnd);
            $availableCalendar->setFreezeTime($freezeTime);
            $availableCalendar->setProfile($profile);

            $availableCalendars[] = $availableCalendar;
        }

        $view       = View::create();
        $returnData = [];
        if (count($availableCalendars) > 0) {
            $em = $this->getDoctrine()->getManager();
            foreach ($availableCalendars as $availableCalendar) {
                $em->persist($availableCalendar);
                $em->flush();

                array_push($returnData, array(
                    'id'          => $availableCalendar->getId(),
                    'title'       => '',
                    'start'       => $availableCalendar->getBeginning(),
                    'end'         => $availableCalendar->getEnding(),
                    'type'        => '',
                    'displayType' => 'availability',
                    'duration'    => 0,
                    'with'        => '',
                    'withLink'    => '#',
                ));
            }
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de l\'ajout de votre disponibilité, veuillez réessayer', []);
        }

        return $this->handleView($view);
    }

    /**
     * DELETE AvailableCalendar.
     *
     * @Rest\Post("delete")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     *
     * @return Response
     */
    public function deleteCalendarsLearnerAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $user      = $this->getUser();
        $profileId = $paramFetcher->get('profileId') ?: null;

        if ($profileId) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'id' => $profileId,
            ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'person' => $user->getId(),
            ));
        }

        $availablesCalendar = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($profile, $beginning, $ending);
        $view               = View::create();
        $errors             = $this->get('validator')->validate($profile);
        $messages           = [];
        if (0 == count($errors)) {
            // find all module session which assigned to this learner
            $em = $this->getDoctrine()->getManager();
            $moduleSessionArray = $this->findModuleSessionForLearner($profile, $em);

            foreach ($availablesCalendar as $availableCalendar) {
                if ($this->checkAvailabilityDeleteForLearner($profile, $availableCalendar->getBeginning(), $availableCalendar->getEnding(), $moduleSessionArray)) {
                    $em->remove($availableCalendar);
                } else {
                    $messages[]= $availableCalendar->getBeginning()->format('d-m-Y');
                }
            }
            $em->flush();
            $returnData = ['status' => $messages ? $translator->trans('global.available_calendar_warning_delete', ['%days%' => implode(", ", $messages)]) : $translator->trans('global.available_calendar_delete_success')];
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de la supression de vos disponibilités, veuillez réessayer', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @Rest\Post("calendars/custom")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning availability.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     *
     * @return Response
     */
    public function postCalendarsCustomLearnerAction(ParamFetcher $paramFetcher)
    {
        /** @var Person $user */
        $user = $this->getUser();
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $days      = $paramFetcher->get('days');

        $interval = DateInterval::createFromDateString('1 day');
        $ending->add($interval);

        $period = new DatePeriod($beginning, $interval, $ending);

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));

        $availableCalendars = [];
        foreach ($period as $dayCurrent) {
            $dayName = date('l', $dayCurrent->getTimeStamp());

            foreach ($days as $day) {
                $dayAdded = $day['dayName'];

                if ($dayAdded === $dayName) {
                    $freezeTime = null;
                    $dayBegin = clone $dayCurrent;
                    $dayEnd   = clone $dayCurrent;

                    if ((!$day['start'] || !$day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        continue;
                    } else if ((!$day['start'] || !$day['end']) && ($day['start_afternoon'] && $day['end_afternoon'])) {
                        $start              = explode(':', $day['start_afternoon']);
                        $end                = explode(':', $day['end_afternoon']);

                    } else if (($day['start'] && $day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end']);
                    } else {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end_afternoon']);
                        $freezeTime         = $day['end'] . '|' . $day['start_afternoon'];
                    }

                    $beginTime = $dayBegin->setTime($start[0], $start[1]);
                    $endTime   = $dayEnd->setTime($end[0], $end[1]);

                    // Check if exist already
                    $existCalendar = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
                        ->findAvailabilityByLiveResource($profile, $beginTime, $endTime);

                    if (!$existCalendar) {
                        $availableCalendar = new AvailableCalendar();
                        $availableCalendar->setBeginning($beginTime);
                        $availableCalendar->setEnding($endTime);
                        $availableCalendar->setFreezeTime($freezeTime);
                        $availableCalendar->setProfile($profile);
                        $availableCalendars[] = $availableCalendar;
                    }
                }
            }
        }

        $view       = View::create();
        $returnData = [];
        if (count($availableCalendars) > 0) {
            $em = $this->getDoctrine()->getManager();
            foreach ($availableCalendars as $availableCalendar) {
                $em->persist($availableCalendar);
                $em->flush();

                array_push($returnData, array(
                    'id'          => $availableCalendar->getId(),
                    'title'       => '',
                    'start'       => $availableCalendar->getBeginning(),
                    'end'         => $availableCalendar->getEnding(),
                    'type'        => '',
                    'displayType' => 'availability',
                    'duration'    => 0,
                    'with'        => '',
                    'withLink'    => '#',
                ));
            }
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de l\'ajout de votre disponibilité, veuillez réessayer', []);
        }

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @Rest\Post("delete/custom")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning availability.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     *
     * @return Response
     */
    public function postDeleteCustomLearnerAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        /** @var Person $user */
        $user = $this->getUser();
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $days      = $paramFetcher->get('days');

        $interval = DateInterval::createFromDateString('1 day');
        $ending->add($interval);

        $period = new DatePeriod($beginning, $interval, $ending);

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));
        $messages           = [];

        // find all module session which assigned to this learner
        $moduleSessionArray = $this->findModuleSessionForLearner($profile, $em);

        foreach ($period as $dayCurrent) {
            $dayName = date('l', $dayCurrent->getTimeStamp());

            foreach ($days as $day) {
                $dayAdded = $day['dayName'];

                if ($dayAdded === $dayName) {
                    $dayBegin = clone $dayCurrent;
                    $dayEnd   = clone $dayCurrent;

                    if ((!$day['start'] || !$day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        continue;
                    } else if ((!$day['start'] || !$day['end']) && ($day['start_afternoon'] && $day['end_afternoon'])) {
                        $start              = explode(':', $day['start_afternoon']);
                        $end                = explode(':', $day['end_afternoon']);

                    } else if (($day['start'] && $day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end']);
                    } else {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end_afternoon']);
                    }

                    $beginTime = $dayBegin->setTime($start[0], $start[1]);
                    $endTime   = $dayEnd->setTime($end[0], $end[1]);

                    // Check if exist already
                    $existCalendars = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
                        ->findAvailabilityByLiveResource($profile, $beginTime, $endTime);

                    if ($existCalendars) {
                        foreach ($existCalendars as $existCalendar) {
                            if ($this->checkAvailabilityDeleteForLearner($profile, $existCalendar->getBeginning(), $existCalendar->getEnding(), $moduleSessionArray)) {
                                $em->remove($existCalendar);
                            } else {
                                $messages[]= $existCalendar->getBeginning()->format('d-m-Y');
                            }
                        }
                        $em->flush();
                    }
                }
            }
        }

        $view       = View::create();
        $returnData = ['status' => $messages ? $translator->trans('global.available_calendar_warning_delete', ['%days%' => implode(", ", $messages)]) : $translator->trans('global.available_calendar_delete_success')];
        $view->setData($returnData);
        $view->setStatusCode(200);
        return $this->handleView($view);
    }
}
