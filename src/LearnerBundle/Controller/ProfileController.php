<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Group;
use AppBundle\Entity\PersonTimeZone;
use DateTime;
use ApiBundle\Entity\BookingAgenda;
use Doctrine\Common\Persistence\ObjectRepository;
use Exception;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Traits\Controller\HasWorkingHours;

class ProfileController extends Controller
{
    use HasWorkingHours;

    private $percentLevelTable = [];
    private $percentLevelWeightTable = [];

    private function prepare(array $skillLevelTable){
        $this->percentLevelTable = [];
        $this->percentLevelWeightTable = [];
        $numberOfLevels = count($skillLevelTable) - 1;
        // prepare for levels' weight
        //(100 % (number of level - 1)) % (current level point - last level point)
        $weight = 0;
        array_push($this->percentLevelWeightTable, $weight);
        for($i = 1; $i <= $numberOfLevels; $i++){
            $weight = (100/$numberOfLevels) / ($skillLevelTable[$i]->getPoint() - $skillLevelTable[$i - 1]->getPoint());
            array_push($this->percentLevelWeightTable, $weight);
        }

        // prepare for levels' percent on meter
        $percentPerLevel = 100 / $numberOfLevels;
        $percent = 0;
        array_push($this->percentLevelTable, $percent);
        for($i = 0; $i <= $numberOfLevels; $i++){
            $percent += $percentPerLevel;
            array_push($this->percentLevelTable, $percent);
        }
    }

    public function calculatePercent(array $skillLevelTable, int $userLP): int{
        $this->prepare($skillLevelTable);
        $tableSize = sizeof($skillLevelTable);
        $currentLevel = 0;
        $percent = 0;
        for($i = 1; $i < $tableSize; $i += 1){
            if ($userLP <= $skillLevelTable[$i]->getPoint() && $userLP > $skillLevelTable[$i - 1]->getPoint()){
                $currentLevel = $i;
            }
        }
        if($userLP >= $skillLevelTable[$tableSize-1]->getPoint()){
            return 100;
        }

        //(user LP - last level point mark) * current user level weight + last level percent

        if ($currentLevel>0) {
            $percent = (($userLP - $skillLevelTable[$currentLevel - 1]->getPoint()) * $this->percentLevelWeightTable[$currentLevel]) + $this->percentLevelTable[$currentLevel - 1];
        }
        return $percent;
    }

    public function mediaFileUploadAction(Request $request)
    {
        $profileFolder = '/files/profile/';
        $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . $profileFolder;

        $fileTemp = $_FILES['avatarFile'];
        $array = explode('.', $fileTemp['name']);
        $extension = end($array);
        $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;

        if (move_uploaded_file($fileTemp['tmp_name'], $targetFile)) {
            $profile = $this->getProfileRepository()->findOneBy(array('person' => $this->getUser()));
            $em = $this->getDoctrine()->getManager();
            $profile->setAvatar($profileFolder . $fileName);

            $em->flush();
            $response = new Response(json_encode(array("uploaded" => 'avatar', 'file_name' => $profileFolder . $fileName)));
        } else {
            $response = new Response(json_encode(array("uploaded" => false, 'file_name' => 'File upload failed')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    public function deleteFileAction(Request $request)
    {
        $id = $request->request->get('profileId');
        $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
        $profile = $this->getProfileRepository()->find($id);
        $em = $this->getDoctrine()->getManager();
        if (file_exists($targetDir . $profile->getAvatar())) {
            unlink($targetDir . $profile->getAvatar());
            $profile->setAvatar(null);
        }
        $em->flush();

        return $this->redirectToRoute('learner_profile_edit');
    }
    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * @return ObjectRepository
     */
    private function getSkillResourceVarietyProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile');
    }

    /**
     * @return ObjectRepository
     */
    private function getSkillRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Skill');
    }

    /**
     * @return ObjectRepository
     */
    private function getSkillLevelRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:SkillLevel');
    }

    /**
     * @return ObjectRepository
     */
    private function getDomainRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Domain');
    }

    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        return $this->render('LearnerBundle:profile:show.html.twig', array(
            'user' => $user,
            'profile' => $profile,
        ));
    }

    public function trainingGroupsAction(Request $request)
    {
        $user = $this->getUser();

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $learnerInterventions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerIntervention')->findBy(array('learner' => $profile));

        $learnerGroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('learner' => $profile));
        $groups = [];
        foreach($learnerGroups as $learnerGroup){
            $interventionNames = [];
            $interventionIds = [];
            $group   = $learnerGroup->getGroup();
            $groupId = $group->getId();
            if (!in_array($groupId, $groups)) {
                $learners = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('group' => $group));
                foreach ($learnerInterventions as $learnerIntervention){
                    if(!in_array($learnerIntervention->getIntervention()->getId(), $interventionIds)){
                        $checkExistCourse = 1;
                        foreach ($learners as $learner){
                            $learnerCourse = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerIntervention')->findOneBy(array('intervention' => $learnerIntervention->getIntervention(), 'learner' => $learner->getLearner()));

                            if(!$learnerCourse){
                                $checkExistCourse = 0;
                            }
                        }

                        if($checkExistCourse == 1){
                            $interventionIds[]   = $learnerIntervention->getIntervention()->getId();
                            $interventionNames[] = $learnerIntervention->getIntervention()->getDesignation();
                        }
                    }

                }
                $numberLearner = count($learners);
                $groups[] = array('group' => $group, 'numberLearner' => $numberLearner, 'numberCourse' => count($interventionIds) , 'interventionNames' => implode(", ", $interventionNames)) ;
            }
        }

        return $this->render('LearnerBundle:profile:group.html.twig', array(
            'person'      => $user,
            'groups'      => $groups,
            'nextSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
        ));
    }
    /**
     * Finds and displays a group entity.
     *
     * @param Group $group
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function groupMembersAction(Group $group)
    {
        $user = $this->getUser();

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        $learnerGroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('group' => $group));

        return $this->render('LearnerBundle:profile:member.html.twig', array(
            'learnerGroups' => $learnerGroups,
            'group'         => $group,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
        ));
    }
    /**
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $timeZones = $this->getDoctrine()->getRepository(PersonTimeZone::class)->findAll();

        /*$workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);*/

        $learnerGroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('learner' => $profile));
        $groups = [];
        foreach ($learnerGroups as $learnerGroup) {
            $group   = $learnerGroup->getGroup();
            $groupId = $group->getId();
            if (!in_array($groupId, $groups)) {
                $learners = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('group' => $group));
                $numberLearner = count($learners);
                $groups[] = array('group' => $group, 'numberLearner' => $numberLearner) ;
            }
        }

        $alertCheckInfo = 0;
        $session = new Session();
        // set and get session attributes
        $checkFistLogin = $session->get('last_login');
        if ($checkFistLogin == -1) {
            $alertCheckInfo = 1;
            $session->set('last_login', null);
        }

        $skillLevels = $this->getSkillLevelRepository()->findAll();
        $maxLevel = 0;
        foreach ($skillLevels as $skillLevel) {
            $maxLevel = $skillLevel->getPoint() > $maxLevel ? $skillLevel->getPoint() : $maxLevel;
        }
        // todo: it is fake data. will be use $profile later
        $fakeProfile = $this->getProfileRepository()->findOneBy(array(
            'person' => 712,
        ));

        $domains = $this->getDomainRepository()->findAll();
        $skillResourceVarieties = $this->getSkillResourceVarietyProfileRepository()->findBy(array(
            'profile' => $fakeProfile,
        ));

        $arraySkills = $arraySkillsWorking = $arraySkillsNotWorking = [];
        foreach ($domains as $k => $domain) {
            $tempSkills = $tempSkillsWorking = $tempSkillsNotWorking = [];
            $totalLevel = $totalLevelWorking = $totalLevelNotWorking = 0;
            foreach ($skillResourceVarieties as $skillResourceVariety) {
                foreach ($skillResourceVariety->getSkill()->getDomains() as $subDomain) {
                    if ($domain->getId() == $subDomain->getId()) {
                        $percent = $this->calculatePercent($skillLevels, $skillResourceVariety->getScore());
                        $tempSkills[$percent] = $skillResourceVariety;
                        $totalLevel += $skillResourceVariety->getScore();
                        if ($skillResourceVariety->getIsWorking()) {
                            $tempSkillsWorking[$percent] = $skillResourceVariety;
                            $totalLevelWorking += $skillResourceVariety->getScore();
                        } else {
                            $tempSkillsNotWorking[$percent] = $skillResourceVariety;
                            $totalLevelNotWorking += $skillResourceVariety->getScore();
                        }
                    }
                }
            }
            if ($tempSkills) {
                $arraySkills[$k]['domain'] = $domain;
                $arraySkills[$k]['totalLevel'] = $totalLevel/count($tempSkills);
                $arraySkills[$k]['percent'] = $this->calculatePercent($skillLevels, $arraySkills[$k]['totalLevel']);
                $arraySkills[$k]['skills'] = $tempSkills;
            }
            if ($tempSkillsWorking) {
                $arraySkillsWorking[$k]['domain'] = $domain;
                $arraySkillsWorking[$k]['totalLevel'] = $totalLevelWorking/count($tempSkillsWorking);
                $arraySkillsWorking[$k]['percent'] = $this->calculatePercent($skillLevels, $arraySkillsWorking[$k]['totalLevel']);
                $arraySkillsWorking[$k]['skills'] = $tempSkillsWorking;
            }
            if ($tempSkillsNotWorking) {
                $arraySkillsNotWorking[$k]['domain'] = $domain;
                $arraySkillsNotWorking[$k]['totalLevel'] = $totalLevelNotWorking/count($tempSkillsNotWorking);
                $arraySkillsNotWorking[$k]['percent'] = $this->calculatePercent($skillLevels, $arraySkillsNotWorking[$k]['totalLevel']);
                $arraySkillsNotWorking[$k]['skills'] = $tempSkillsNotWorking;
            }
        }

        return $this->render('LearnerBundle:profile:edit.html.twig', array(
            'person' => $user,
            'timeZones' => $timeZones,
            //'workingHours' => $workingHours,
            'profile' => $profile,
            'groups' => $groups,
            'alertCheckInfo' => $alertCheckInfo,
            'skills' => $arraySkills,
            'workingSkills' => $arraySkillsWorking,
            'notWorkingSkills' => $arraySkillsNotWorking,
            'skillLevels' => $skillLevels,
            'maxLevel' => $maxLevel,
            'nextSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
        ));
    }

    public function saveAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $entityManager = $this->getDoctrine()->getManager();
        $lastName = $request->request->get('lastName');
        $firstName = $request->request->get('firstName');
        $position = $request->request->get('position');
        $fixedPhone = $request->request->get('fixedPhone');
        $mobilePhone = $request->request->get('mobilePhone');
        $email = $request->request->get('email');
        $manager_email = $request->request->get('manager_email');
        $otherEmail = $request->request->get('otherEmail');
        $employee_id = $request->request->get('employee_id');

        $data    = $request->request->all();
        $profile->setWorkingHours($this->daysProcessing($data));

        $profile->setLastName($lastName);
        $profile->setFirstName($firstName);
        $profile->setPosition($position);
        $profile->setFixedPhone($fixedPhone);
        $profile->setMobilePhone($mobilePhone);
        $profile->setManagerEmail($manager_email);
        $profile->setOtherEmail($otherEmail);
        $profile->setEmployeeId($employee_id);
        $profile->setUpdateDate(new DateTime());

        $entityManager->persist($profile);
        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return $this->render('LearnerBundle:profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('learner_profile_edit');
    }
    public function savePassAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $entityManager = $this->getDoctrine()->getManager();
        $password = $request->request->get('password');
        $rpPassword = $request->request->get('rpPassword');


        $profile->setUpdateDate(new DateTime());
        $user->setPlainPassword($rpPassword);
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->updatePassword($user);
        $entityManager->persist($user);
        $entityManager->persist($profile);
        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return $this->render('LearnerBundle:profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('learner_profile_edit');
    }

}
