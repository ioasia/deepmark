<?php

namespace LearnerBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\WorkingHours;
use AppBundle\Service\CalendarApi;
use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Traits\IsAvailable;

class DashboardApiController extends RestController
{
    use IsAvailable;
    /**
     * @Rest\Get("calendars")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=false, description="Date End")
     * @Rest\QueryParam(name="learnerId", nullable=false, description="Learner Id")
     * @Rest\QueryParam(name="typeModule", nullable=false, description="Module Type")
     */
    public function getCalendarsLearnerAction(ParamFetcher $paramFetcher)
    {
        $dateStart = new \DateTime($paramFetcher->get('dateStart'));
        $dateEnd = new \DateTime($paramFetcher->get('dateEnd'));
        $learnerId = $paramFetcher->get('learnerId') ? $paramFetcher->get('learnerId') : null;
        $moduleType = $paramFetcher->get('typeModule') ? $paramFetcher->get('typeModule') : null;

        $calendar_array = $calendar_my_booking = [];
        $calendars = $returnData = [];

        $calendarApi = $this->get(CalendarApi::class);

        $em = $this->getDoctrine()->getManager();

        $person = $this->getUser();

        if ($learnerId) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->find($learnerId);
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);
        }



        if (!$profile) {
            return $this->errorHandler();
        }

        $status = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findBy(['appId' => [
                Status::LEARNER_INTERVENTION_CREATED,
                Status::LEARNER_INTERVENTION_IN_PROGRESS,
            ]]);

        $roles   = $person->getRoles();
        // The dashboard for admin part: If user as Admin role then the calendar will show all session of all learners with Created and In Progress status
        if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array(
                'status' => $status,
            ));
        } else {
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array(
                'learner' => $profile,
                'status' => $status,
            ));
        }
        $moduleIds = [];
        foreach ($learnerInterventions as $learnerIntervention) {
            $intervention = $learnerIntervention->getIntervention();
            $modules = $em->getRepository('ApiBundle:Module')->findBy(array(
                'intervention' => $intervention,
            ));
            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                if(!in_array($module->getId(),$moduleIds)){
                    $moduleIds[] = $module->getId();
                }
            }
        }
        $modulesMonth = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByRange($moduleIds, $dateStart, $dateEnd);
        foreach ($modulesMonth as $module) {
            if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                $booking = $calendarApi->getMyBookingsByModule($profile, $module, clone $dateStart, clone $dateEnd);
                if($moduleType){
                    if ($module->getStoredModule()->getAppId() == $moduleType) {
                        if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
                            if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                                $calendar_my_booking = array_merge($calendar_my_booking, $booking);
                            } else {
                                $calendar_array = $calendar_array + $booking;
                            }
                        } else {
                            $calendar_array = array_merge($calendar_array, $booking);
                        }
                    }

                } else {
                    // The dashboard for admin part: If user as Admin role then the calendar will show duplication session. So will process it to avoid duplicate session
                    if (in_array('ROLE_ADMIN', $roles) && !$learnerId) {
                        if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                            $calendar_my_booking = array_merge($calendar_my_booking, $booking);
                        } else {
                            $calendar_array = $calendar_array + $booking;
                        }
                    } else {
                        $calendar_array = array_merge($calendar_array, $booking);
                    }
                }

            }/* else { // remove un-booking module
                $color = '#ffad17';

                switch ($module->getStoredModule()->getAppId()) {
                    case StoredModule::ONLINE:
                        $color = '#003B5C';
                        break;
                    case StoredModule::VIRTUAL_CLASS:
                        $color = '#045f7f';
                        break;
                    case StoredModule::ONLINE_WORKSHOP:
                        $color = '#659eb1';
                        break;
                    case StoredModule::PRESENTATION_ANIMATION:
                        $color = '#68d2f1';
                        break;
                    case StoredModule::QUIZ:
                        $color = '#ffa322';
                        break;
                    case StoredModule::EVALUATION:
                        $color = '#957013';
                        break;
                    default:
                        break;
                }
                $days = $dateEnd->diff($dateStart)->days;
                $dateInCalendar = clone $dateStart;
                for ($i = 0; $i < $days; $i++) {
                    $available = $this->isAvailableModule($module);
                    $dateInCalendar = $dateInCalendar->modify('+'.($i==0? '0' : '1' ).' day');
                    if (in_array('ROLE_ADMIN', $roles)) {
                        $calendars[$i][$module->getId()] = array(
                            'id' => $module->getId(),
                            'title' => $module->getStoredModule()->getDesignation() . ': '. $module->getDesignation(),
                            'start' => clone $dateInCalendar,
                            'end' => clone $dateInCalendar,
                            'type' => $module->getStoredModule()->getAppId(),
                            'displayType' => $module->getStoredModule()->getDesignation(),
                            'duration' => $module->getDuration()->format('G:i'),
                            'trainerName' => 'N/A',
                            'bookingStatus' => 'N/A',
                            'startEndDate' => $module->getBeginning()->format('d/m/Y') . ' to ' . $module->getEnding()->format('d/m/Y'),
                            'intervention' => $module->getIntervention()->getDesignation(),
                            'entity' => $module->getIntervention()->getEntity() ? $module->getIntervention()->getEntity()->getOrganisation()->getDesignation(): null,
                            'available' => $available,
                            'backgroundColor' => $color,
                        );
                    } else {
                        array_push($calendar_array, array(
                            'id' => $module->getId(),
                            'title' => $module->getStoredModule()->getDesignation() . ': '. $module->getDesignation(),
                            'start' => clone $dateInCalendar,
                            'end' => clone $dateInCalendar,
                            'type' => $module->getStoredModule()->getAppId(),
                            'displayType' => $module->getStoredModule()->getDesignation(),
                            'duration' => $module->getDuration()->format('G:i'),
                            'trainerName' => 'N/A',
                            'bookingStatus' => 'N/A',
                            'startEndDate' => $module->getBeginning()->format('d/m/Y') . ' to ' . $module->getEnding()->format('d/m/Y'),
                            'intervention' => $module->getIntervention()->getDesignation(),
                            'entity' => $module->getIntervention()->getEntity() ? $module->getIntervention()->getEntity()->getOrganisation()->getDesignation(): null,
                            'available' => $available,
                            'backgroundColor' => $color,
                        ));
                    }
                }
            }*/
        }

        $view = View::create();
        if (in_array('ROLE_ADMIN', $roles)) {
            // $returnData = call_user_func_array('array_merge', $calendars); // remove un-booking module
            $calendar_array = array_merge($calendar_my_booking, $calendar_array);
            $returnData = array_merge($returnData, $calendar_array);
            $view->setData($returnData);
        } else {
            $view->setData($calendar_array);
        }


        return $this->handleView($view);
    }

    /**
     * @Rest\Get("dayActivities")
     * @Rest\QueryParam(name="day", description="Day to search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDayActivitiesLearnerAction(ParamFetcher $paramFetcher)
    {
        $person = $this->getUser();
        $date = $paramFetcher->get('day');
        $roles   = $person->getRoles();
        if (in_array('ROLE_ADMIN', $roles)) {
            $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                ->findBy(array(
                    'status' => $this->getDoctrine()->getRepository('ApiBundle:Status')
                        ->findBy(array(
                            'appId' => [
                                Status::LEARNER_INTERVENTION_CREATED,
                                Status::LEARNER_INTERVENTION_IN_PROGRESS,
                            ],
                        )),
                ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);
            $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                ->findBy(array(
                    'learner' => $profile,
                    'status' => $this->getDoctrine()->getRepository('ApiBundle:Status')
                        ->findBy(array(
                            'appId' => [
                                Status::LEARNER_INTERVENTION_CREATED,
                                Status::LEARNER_INTERVENTION_IN_PROGRESS,
                            ],
                        )),
                ));
        }

        foreach ($coursesToStart as $intervention) {
            $interventions[] = $intervention->getIntervention();
        }

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findBy(array(
                'intervention' => $interventions
            ));

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleIds[] = $module->getId();
        }
        $date = new \DateTime($date);
        $listActivities = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByToDate($moduleIds, $date);

        $view = $this->renderView('LearnerBundle:Dashboard:learner_activities.html.twig', [
            'modules' => $listActivities
        ]);

        return new JsonResponse($view);

    }

    /**
     * @Rest\Post("activitiesByRange")
     * @Rest\RequestParam(name="startDate", description="Day to search")
     * @Rest\RequestParam(name="endDate", description="Day to search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getActivitiesByRangeAction(ParamFetcher $paramFetcher)
    {
        $moduleIds = [];
        $interventions = [];
        $person = $this->getUser();
        $roles   = $person->getRoles();
        if (in_array('ROLE_ADMIN', $roles)) {
            $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                ->findBy(array(
                    'status' => $this->getDoctrine()->getRepository('ApiBundle:Status')
                        ->findBy(array(
                            'appId' => [
                                Status::LEARNER_INTERVENTION_CREATED,
                                Status::LEARNER_INTERVENTION_IN_PROGRESS,
                            ],
                        )),
                ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);
            $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                ->findBy(array(
                    'learner' => $profile,
                    'status' => $this->getDoctrine()->getRepository('ApiBundle:Status')
                        ->findBy(array(
                            'appId' => [
                                Status::LEARNER_INTERVENTION_CREATED,
                                Status::LEARNER_INTERVENTION_IN_PROGRESS,
                            ],
                        )),
                ));
        }

        foreach ($coursesToStart as $intervention) {
            $interventions[] = $intervention->getIntervention();
        }

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findBy(array(
                'intervention' => $interventions
            ));

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleIds[] = $module->getId();
        }


        $startDate = $paramFetcher->get('startDate');
        $endDate = $paramFetcher->get('endDate');
        $listActivities = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByRange($moduleIds, $startDate, $endDate);

        $view = $this->renderView('LearnerBundle:Dashboard:learner_activities.html.twig', [
            'modules' => $listActivities
        ]);

        return new JsonResponse($view);
    }

    /**
     * POST workingHours.
     *
     * @Rest\Post("working/hours")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     * @Rest\RequestParam(name="lunchStartTime", nullable=true, strict=true, description="lunchStartTime.")
     * @Rest\RequestParam(name="lunchEndTime", nullable=true, strict=true, description="lunchEndTime.")
     *
     * @return Response
     */
    public function postWorkingHoursLearnerAction(ParamFetcher $paramFetcher)
    {
        $now = new DateTime('now');
        $em = $this->getDoctrine()->getManager();
        $days = $paramFetcher->get('days');

        $user = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));

        if ($paramFetcher->get('lunchStartTime') && $paramFetcher->get('lunchEndTime')) {

            $nowTime = new DateTime('now');
            $lunchStartTime = explode(":", $paramFetcher->get('lunchStartTime'));
            $lunchEndTime = explode(":", $paramFetcher->get('lunchEndTime'));
            $profile->setLunchStartTime(clone $nowTime->setTime($lunchStartTime[0], $lunchStartTime[1]));
            $profile->setLunchEndTime(clone $nowTime->setTime($lunchEndTime[0], $lunchEndTime[1]));
        }

        $em->persist($profile);
        $em->flush();



        if ($this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findOneBy(['profile' => $profile])) {
            $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
                ->deleteWorkingHoursByProfile($profile);
        }

        if ($days) {
            foreach ($days as $day) {
                $working = new WorkingHours();
                $working->setName($day['dayName']);

                if ($day['start'] && $day['end']) {
                    $startTime = explode(":", $day['start']);
                    $endTime = explode(":", $day['end']);
                    $working->setStart(clone $now->setTime($startTime[0], $startTime[1]));
                    $working->setEnd(clone $now->setTime($endTime[0], $endTime[1]));
                }

                $working->setProfile($profile);
                $em->persist($working);
                $em->flush();
            }
        }

        $view = View::create();
        $view->setData($profile);
        $view->setStatusCode(200);

        return $this->handleView($view);
    }
}