<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\BookingAgenda;
use Utils\Statistics\BaseStatistics;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\Intervention;
use ApiBundle\Repository\ModuleRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Traits\IsAvailable;

class DashboardController extends Controller
{
    use IsAvailable;

    private function getCoursesToBook(Profile $profile)
    {
        $coursesToBook  = [];
        $today          = date("Y-m-d");
        $coursesStarted = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->getLearnerInterventionToBook($profile->getId());

        foreach ($coursesStarted as $intervention) {
            $course = $intervention->getIntervention();
            if (strtotime($course->getBeginning()->format('Y-m-d')) <= strtotime($today) && strtotime($course->getEnding()->format('Y-m-d')) >= strtotime($today)) {
                $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')
                    ->findBy(['intervention' => $intervention->getIntervention()]);
                foreach ($modules as $module) {
                    if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                        if (strtotime($module->getBeginning()->format('Y-m-d')) <= strtotime($today) && strtotime($module->getEnding()->format('Y-m-d')) >= strtotime($today)) {
                            $book = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                                ->findOneBy(['module' => $module, 'learner' => $profile]);

                            if (is_null($book)) {
                                array_push($coursesToBook, $module);
                            }
                        }
                    }
                }
            }
        }

        return $coursesToBook;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        $alertsNumber   = 0;
        $exerciseNumber = 0;
        $interventions  = $moduleIds = [];
        $today          = date("Y-m-d");
        $now =          new DateTime('now');

        $person = $this->getUser();
        /** @var Profile $profile */
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $coursesNumber = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->countStatusToStart($profile->getId());

        $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->findBy(array(
                'learner' => $profile,
                'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')
                    ->findBy(array(
                        'appId' => [
                            Status::LEARNER_INTERVENTION_CREATED,
                            Status::LEARNER_INTERVENTION_IN_PROGRESS,
                        ],
                    )),
            ),
                array('intervention' => 'DESC')
            );
        $courses        = [];
        $newCourses      = [];
        foreach ($coursesToStart as $intervention) {
            $interventions[] = $intervention->getIntervention();
            $course          = $intervention->getIntervention();
            $begin           = $course->getBeginning()->format("Y-m-d");
            $end             = $course->getEnding()->format("Y-m-d ");
            if (strtotime($begin) <= strtotime($today) && strtotime($end) >= strtotime($today)) {
                $learnersModulesNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)->getResultsByLearnerIntervention(
                    $intervention->getIntervention()->getId(),
                    $intervention->getLearner()->getId()
                );

                $total     = 0;
                $notations = 0;
                foreach ($learnersModulesNotation as $learnerModuleNotation) {
                    $notations += $learnerModuleNotation->getNotation();
                    ++$total;
                }

                $stars = $total ? $notations / $total : 0;

                $intervention->setInterventionNotation($stars);

                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getIntervention()->getId()));
                $times         = [];

                foreach ($courseModules as $module) {
                    if (!$duration = $module->getDuration()) {
                        continue;
                    }

                    $times[] = $duration->format('H:i');
                }

                $totalDuration = $this->AddPlayTime($times);
                $courses[]     = array('totalDuration' => $totalDuration, 'learnerIntervention' => $intervention);
                if($intervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED){
                    $newCourses[]   = array('totalDuration' => $totalDuration, 'learnerIntervention' => $intervention);
                }
            }
        }
        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findBy(array(
                'intervention' => $interventions,
            ));

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleIds[] = $module->getId();
        }

        $activitiesNumber = count($this->getCoursesToBook($profile));

        $count = array(
            'courses'    => $coursesNumber,
            'alerts'     => $alertsNumber,
            'exercices'  => $exerciseNumber,
            'activities' => $activitiesNumber,
        );

        $date = new \DateTime(date("Y-m-d"));

        $modulesToday = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByToDate($moduleIds, $date);

        $dt_min = new DateTime('last saturday');
        $dt_min->modify('+1 day');
        $dt_max = clone $dt_min;
        $dt_max->modify('+6 days');
        $modulesWeek = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByRange($moduleIds, $dt_min, $dt_max);

        $startDateMonth = new DateTime('first day of this month 00:00:00');
        $endDateMonth   = new DateTime('last day of this month 00:00:00');
        $modulesMonth   = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->findModulesByRange($moduleIds, $startDateMonth, $endDateMonth);
        $coursesToBook  = $this->getCoursesToBook($profile);

        $lastActivity = $this->getDoctrine()->getRepository('ApiBundle:Activities')
            ->findOneBy(array(
                'profile'       => $profile,
                'activityName'  => Activities::LATEST_LEARNED_MODULE,
            ));

        $showPopup = ($this->getUser()->getLastlogin() != null && $request->get('login') == 1 ) ? 1 : 0;
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT `id` FROM `learner_intervention` WHERE `learner_id` = :learner_id AND `status_id` != :status_id");
        $statement->bindValue('learner_id', $profile->getId());
        $statement->bindValue('status_id', Status::INTERVENTION_FINISHED);
        $statement->execute();
        $results = $statement->fetchAll();
        $results = !empty($results);

        // PARCOURS REQUIS
        $tab = $request->query->get('tab');
        $sort = ($tab == 'required') ? $request->query->get('sort') : null;
        $doneStatus = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->find(Status::DONE);
        $myAllCourses = $myInProgressCourses = $myCreatedCourses = $myRecommendation = [];
        $learnerInterventions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerIntervention')->findLearnerInterventionByProfile($profile, $sort);

        foreach ($learnerInterventions as $learnerIntervention){
            if ($learnerIntervention->getIntervention()->getEnding() > $now) {
                $intervention = $learnerIntervention->getIntervention();
                $courseIds[] = $intervention->getId();
                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
                $times = [];
                $totalModules = $totalMinutes = $doneMinutes = 0;
                foreach ($courseModules as $module) {
                    if (!$duration = $module->getDuration()) {
                        continue;
                    }
                    $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')->findOneBy(array("module" => $module, 'learner' => $profile, 'status' => $doneStatus));
                    if ($moduleIteration) {
                        $doneMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
                    }
                    $totalMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
                    $times[] = $duration->format('H:i');
                    $totalModules++;
                }

                $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($times);
                if ($learnerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $myCreatedCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
                } else if ($learnerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $myInProgressCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
                }
                $myAllCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
            }
        }

        // PARCOURS À LA CARTE
        $sort = ($tab == 'menu') ? $request->query->get('sort') : null;
        $menuAllCourses = $menuInProgressCourses = $menuCreatedCourses = [];
        $menuInterventions = $this->getDoctrine()->getRepository(Intervention::class)->getCurrentIntervention(null, $now, $sort);
        foreach ($menuInterventions as $intervention){
            if ($intervention->getEnding() > $now) {
                $courseIds[] = $intervention->getId();
                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
                $times = [];
                $totalModules = $totalMinutes = $doneMinutes = 0;
                foreach ($courseModules as $module) {
                    if (!$duration = $module->getDuration()) {
                        continue;
                    }
                    $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')->findOneBy(array("module" => $module, 'learner' => $profile, 'status' => $doneStatus));
                    if ($moduleIteration) {
                        $doneMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
                    }
                    $totalMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
                    $times[] = $duration->format('H:i');
                    $totalModules++;
                }

                $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($times);

                $learnerIntervention = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerIntervention')->findOneBy(array('learner' => $profile, 'intervention' => $intervention));

                if ($learnerIntervention && $learnerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $menuCreatedCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
                } else if ($learnerIntervention && $learnerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $menuInProgressCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
                }
                $menuAllCourses[] = array('remainMinutes' => ($totalMinutes - $doneMinutes), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'learnerIntervention' => $learnerIntervention, 'totalModules' => $totalModules);
            }
        }

        // EVENTS
        $nextSessions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:BookingAgenda')->findBookingNextDays($profile, 15);
        $checkAvaiable = [];
        foreach ($nextSessions as $session) {
            $available = $this->isAvailableModule($session->getModule());
            // the module will be expired when the today - the booking date + the duration time > 24 hrs
            $isAvailableBooking = $this->isAvailableBooking($session->getModule(), $session);
            $checkAvaiable[$session->getId()] = $available && $isAvailableBooking ;
        }

        return $this->render('LearnerBundle:Dashboard:index.html.twig', array(
            'count'                 => $count,
            'modules'               => $modules,
            'learnerIntervention'   => $courses,
            'modulesToday'          => $modulesToday,
            'modulesWeek'           => $modulesWeek,
            'modulesMonth'          => $modulesMonth,
            'coursesToBook'         => $coursesToBook,
            'newCourses'            => $newCourses,
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),
            'lastActivity'          => $lastActivity ? $lastActivity : null,
            'activityPopup'         => $showPopup && $results,
            'myRecommendation'      => $myRecommendation,
            'myAllCourses'          => $myAllCourses,
            'myInProgressCourses'   => $myInProgressCourses,
            'myCreatedCourses'      => $myCreatedCourses,
            'menuAllCourses'        => $menuAllCourses,
            'menuInProgressCourses' => $menuInProgressCourses,
            'menuCreatedCourses'    => $menuCreatedCourses,
            'nextSessions'          => $nextSessions,
            'checkAvaiable'         => $checkAvaiable
        ));
    }

    public function alertsAction()
    {
        $alertsNumber     = 0;
        $exercicesNumber  = 0;
        $activitiesNumber = 0;

        $person  = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $coursesNumber = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->countStatusToStart($profile->getId());

        $count = array(
            'courses'    => $coursesNumber,
            'alerts'     => $alertsNumber,
            'exercices'  => $exercicesNumber,
            'activities' => $activitiesNumber,
        );

        $coursesToStart = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->findBy(array(
                'learner' => $profile,
                'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')
                    ->findBy(array(
                        'appId' => Status::LEARNER_INTERVENTION_CREATED,
                    )),
            ));
        $coursesStarted = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->findBy(array(
                'learner' => $profile,
                'status'  => $this->getDoctrine()->getRepository('ApiBundle:Status')
                    ->findBy(array(
                        'appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS,
                    )),
            ));

        $exercices = [];
        $quizz     = [];
        $toBook    = [];

        foreach ($coursesStarted as $learnerIntervention) {
            $interventionId = $learnerIntervention->getIntervention()->getId();
            /** @var ModuleRepository $moduleRepository */
            $moduleRepository = $this->getDoctrine()->getRepository('ApiBundle:Module');
            $quiz             = $moduleRepository->findByStoredModuleAppId($interventionId, StoredModule::QUIZ);
            $booking          = $moduleRepository->findByStoredModuleAppId($interventionId,
                StoredModule::MODULE_REQUIRE_BOOKING);

            $toBook = array_merge($toBook, $booking);
            $quizz  = array_merge($quizz, $quiz);
        }

        return $this->render('LearnerBundle:Dashboard:alerts.html.twig', array(
            'count'          => $count,
            'coursesToStart' => $coursesToStart,
            'coursesStarted' => $coursesStarted,
            'exercices'      => $exercices,
            'quizz'          => $quizz,
            'toBook'         => $toBook,
        ));
    }

    public function bookingsAction()
    {
        $alertsNumber   = 0;
        $exerciseNumber = 0;

        $person = $this->getUser();
        /** @var Profile $profile */
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $coursesNumber = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->countStatusToStart($profile->getId());

        $coursesToBook = $this->getCoursesToBook($profile);

        $count = array(
            'courses'    => $coursesNumber,
            'alerts'     => $alertsNumber,
            'exercices'  => $exerciseNumber,
            'activities' => count($coursesToBook),
        );

        return $this->render('LearnerBundle:Dashboard:bookings.html.twig', array(
            'count'         => $count,
            'courses'       => count($coursesToBook),
            'interventions' => $coursesToBook,
        ));
    }

    public function exercicesAction()
    {
        $person = $this->getUser();
        /** @var Profile $profile */
        $profile      = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        /*$workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);*/

        return $this->render('LearnerBundle:Dashboard:exercices.html.twig', array(
            //'workingHours' => $workingHours,
            'nextSession'  => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),

        ));
    }


    function AddPlayTime($times)
    {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours   = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }
}
