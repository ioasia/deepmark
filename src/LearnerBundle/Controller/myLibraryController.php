<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class myLibraryController
 * @package LearnerBundle\Controller
 */
class myLibraryController extends BaseController
{
    public function indexAction(Request $request)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
            ->findBy(array(
                'learner' => $this->getCurrentProfile()
            ));

        return $this->render('LearnerBundle:myLibrary:index.html.twig', array(
            'moduleIterations' => $moduleIterations,
            'nextSession'         => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile)

        ));
    }

    public function newAction(Request $request)
    {
        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file           = $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('learner_library_index'));
        }

        return $this->render('LearnerBundle:myLibrary:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param EducationalDocument $educationalDocument
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, EducationalDocument $educationalDocument)
    {
        $form = $this->createDeleteForm($educationalDocument);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($educationalDocument);
            $em->flush();
        }

        return $this->redirectToRoute('learner_library_index');
    }

    /**
     * Create the form to delete and intervention.
     *
     * @param EducationalDocument $educationalDocument
     *
     * @return FormInterface
     */
    private function createDeleteForm(EducationalDocument $educationalDocument)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('learner_library_delete', array('id' => $educationalDocument->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
