<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Exercises;
use ApiBundle\Service\QuizServiceAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Traits\Controller\HasBestPractice;

class PracticeController extends Controller
{
    use HasBestPractice;
    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id)
    {
        $result = $this->getBestPractice($id);
        return $this->render('molecules/best-practice/view.html.twig', array(
            'bestPractice'          => $result['bestPractice'],
            'others'                => $result['others'],
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser())
        ));
    }
}
