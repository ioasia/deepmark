<?php

namespace LearnerBundle\Controller;

use ApiBundle\Controller\RestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use LearnerBundle\Service\QuizResultCounter;
use Symfony\Component\HttpFoundation\JsonResponse;
use ApiBundle\Service\QuizServiceAPI;
use ApiBundle\Service\UploadFile;

use ApiBundle\Entity\Quizes;
// use ApiBundle\Entity\Module;
use ApiBundle\Entity\QuizUsersAnswers;
use ApiBundle\Entity\QuizQuestions;
use ApiBundle\Entity\QuizQuestionsItems;
// use ApiBundle\Entity\Profile;


class QuizApiController extends RestController {

    /**
     * @Rest\Post("savePlay")
     * @Rest\RequestParam(name="module_id", description="module_id")
     * @Rest\RequestParam(name="quiz_id", description="quiz_id")
     */
    public function savePlayAction(ParamFetcher $paramFetcher, QuizServiceAPI $quizServiceAPI) {
        $res = $quizServiceAPI->savePlay($paramFetcher->get('module_id'), $paramFetcher->get('quiz_id'), $this->getUser()->getId());

        // Result
        if($res != false) { 
            return new JsonResponse(array('status' => 'success', 'play_id' => $res['play_id']), 200);
        } else {
            return new JsonResponse(array('status' => 'fail', 'message' => 'This quiz has been already played the maximum iteration defined'), 200);
        }
    }

    /**
     * @Rest\Post("validateQuestion")
     * @Rest\RequestParam(name="play_id", description="play_id")
     * @Rest\RequestParam(name="module_id", description="module_id")
     * @Rest\RequestParam(name="quiz_id", description="quiz_id")
     * @Rest\RequestParam(name="question_id", description="question_id")
     * @Rest\RequestParam(name="answer", description="Object answer can be customizable")
     * @Rest\RequestParam(name="file_descriptor", nullable=true, description="Object file")
     * @Rest\RequestParam(name="timer_total", description="Timer total in seconds")
     * @Rest\RequestParam(name="timer", description="Timer for current question in seconds")
     *
     * @param ParamFetcher      $paramFetcher
     * @param QuizServiceAPI $quizServiceAPI
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validateQuestionAction(ParamFetcher $paramFetcher, QuizServiceAPI $quizServiceAPI) {
        // Send to validation
        $validating = $quizServiceAPI->validateQuestion(array(
            'play_id' => $paramFetcher->get('play_id'),
            'module_id' => $paramFetcher->get('module_id'),
            'quiz_id' => $paramFetcher->get('quiz_id'),
            'question_id' => $paramFetcher->get('question_id'),
            'answer' => $paramFetcher->get('answer'),
            'file_descriptor' => $paramFetcher->get('file_descriptor'),
            'timer_total' => $paramFetcher->get('timer_total'),
            'timer' => $paramFetcher->get('timer'),
        ));

        // Result
        return new JsonResponse($validating, 200);
    }

    /**
     * @Rest\Post("uploadFile")
     * 
     * Upload files (with Dropzone)
     */
    public function uploadFileAction(Request $request, UploadFile $fileApi, QuizServiceAPI $quizServiceAPI) {
        // Get params
        $params = $request->request->all();
        // Check type to get specs for upload
        $fileConf = $this->checkUploadType($params);
        if ($fileConf === false) {
            // Error upload not configured or bad params
            $datas = array(
                'status' => 'fail',
                'error' => 'Not configured or bad params');
        } else {
            // Upload can be executed
            $file = $request->files->get('file');
            $validation = true;
            $errors = [];
            // Validate type
            if (!in_array($file->getClientMimeType(), $this->upload_config['validation']['mimeTypes'])) {
                $validation = false;
                $errors[] = "<b>Bad file format. <i>Accepted :</i></b> " . implode(', ', $this->upload_config['validation']['mimeTypes']) . " / <b><i>Yours :</i></b> " . $file->getClientMimeType();
            }
            // Validate size
            if ($file->getClientSize() >= $this->upload_config['validation']['maxSize']) {
                $validation = false;
                $errors[] = "<b>Bad file size. <i>Accepted :</i></b> " . ($this->upload_config['validation']['maxSize'] / 1000000) . "Mo / <b><i>Yours :</i></b> " . number_format($file->getClientSize() / 1000000, 2) . 'Mo';
            }

            if ($validation === true) {
                // File API
                $fileApi->setTargetDirectory($this->upload_config['path']);
                if($file->getClientMimeType() == 'video/webm') {
                    $ext = 'webm';
                } else {
                    $ext = $file->getClientOriginalExtension();
                }
                $fileDescriptor = $fileApi->upload($file, $ext);

                // If recording, update answer in db
                if($params['type'] == 'quiz_recording') {
                    $quizServiceAPI->updateRecordAnswer($params['play_id'], $params['question_id'], $fileDescriptor->getPath());
                }
                
                // Response
                $datas = array(
                    'status' => 'success',
                    'file' => $fileDescriptor);
            } else {
                // Response
                $datas = array(
                    'status' => 'error',
                    'errors' => implode('<br />', $errors)
                );
            }
        }

        // View
        $view = View::create();
        $view->setData($datas);
        return $this->handleView($view);
    }

    /**
     * Check upload Type
     * 
     * @param array $params
     * @return array
     */
    private function checkUploadType($params) {
        // var_dump($params);
        if (isset($params['type'])) {
            // Base Path Quiz
            $path = '/web/files/quiz_learner/';
            switch ($params['type']) {
                // Quiz CSV upload for each question
                case 'quiz_xls' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        // Add path
                        $path .= $this->quiz_id . '/xls/';
                        // Validation params
                        $accepted_type = array('application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz image upload for each question
                case 'quiz_img' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/img/';
                        // Validation params
                        $accepted_type = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_doc' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/doc/';
                        // Validation params
                        $accepted_type = array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-word');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                 // Quiz doc upload for each question
                case 'quiz_ppt' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/ppt/';
                        // Validation params
                        $accepted_type = array(
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.ms-powerpoint', 'application/octet-stream');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz audio upload for each question
                case 'quiz_audio' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/audio/';
                        // Validation params
                        $accepted_type = array('audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/x-aac', 'audio/x-wav', 'audio/midi', 'audio/webm', 'audio/3gpp', 'audio/3gpp2', 'audio/aac', 'audio/wav');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz video upload for each question
                case 'quiz_video' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/video/';
                        // Validation params
                        $accepted_type = array('video/quicktime', 'video/3gpp2', 'video/3gpp', 'video/x-flv', 'video/mp4', 'video/x-msvideo', 'video/mpeg', 'video/ogg', 'video/webm');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_template' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/doc/';
                        // Validation params
                        $accepted_type = array(
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/pdf',
                            'text/plain',
                            'text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz recording upload for each question
                case 'quiz_recording' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/recording/';
                        // Validation params
                        $accepted_type = array('video/webm', 'audio/webm');
                        $accepted_size = 150000000; // 150Mo
                    } else {
                        return false;
                    }
                    break;
                default :
                    return false;
                    break;
            }

            $this->upload_config = array(
                'path' => $path,
                'validation' => [
                    'maxSize' => $accepted_size,
                    'mimeTypes' => $accepted_type
                ],
            );
        } else {
            return false;
        }
    }
    /**
    * @Rest\Post("detailQuiz")
    * @Rest\RequestParam(name="quiz_id", description="quiz_id")
    * @Rest\RequestParam(name="module_id", description="module_id")
    * @param ParamFetcher      $paramFetcher
   
    */
    public function detailQuizAction(Request $request){
        $quiz_id    = $request->request->get('quiz_id');
        $title      = $request->request->get('title');
        $play_id    = json_decode($request->request->get('play_id'));

        $userAnswers = $this->getDoctrine()
        ->getRepository(QuizUsersAnswers::class)
        ->findBy(['id' => $play_id]);
        $newArray = [];
        $type = ['qcu','qcm'] ;

        foreach ($userAnswers as $keyUA => $UA) {
           
            // ID of QuizUsersAnswers
            $newArray[$keyUA]['QuizUsersAnswers']['id'] =  $UA->getId();
            // $newArray[$keyUA]['QuizUsersAnswers']['params'] =  $UA->getParams();
            $question = $this->getDoctrine()
            ->getRepository(QuizQuestions::class)
            ->findOneBy(['id' => $UA->getQuestionId()]);
            
           
            $newArray[$keyUA]['QuizQuestions']['id'] =  $question->getId();
            $newArray[$keyUA]['QuizQuestions']['title'] =  $question->getQuestion();
            $newArray[$keyUA]['QuizQuestions']['type'] =  $question->getType();

            // $newArray[$keyUA]['QuizQuestions']['params'] =  $question->getParams();

            $questionItem = $this->getDoctrine()
            ->getRepository(QuizQuestionsItems::class)
            ->findBy(['question_id' => $UA->getQuestionId(),'status' => 1]);
            if(in_array($question->getType(),$type)) {
                $arrayRightAnwsers = json_decode($UA->getParams())->answer->value;
            }
            
            foreach ($questionItem as $keyQI => $QI) {
                $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['id'] = $QI->getId();
                $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['name'] = $QI->getName();
                $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['value'] = $QI->getValue();
                // break;
                $arrayRightAnwsers = json_decode($UA->getParams())->answer->value;
                if($question->getType() == "qcm") {
                    if(in_array($QI->getId(),$arrayRightAnwsers)) {
                        $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['userAnswers'] = 1;
                    }
                    else {
                        $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['userAnswers'] = 0;
                    }
                }
                else if ($question->getType() == "qcu"){
                    if($QI->getId() == $arrayRightAnwsers) {
                        $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['userAnswers'] = 1;
                    }
                    else {
                        $newArray[$keyUA]['QuizQuestionsItems'][$keyQI]['userAnswers'] = 0;
                    }
                }
                else {


                }

            }
        }
        return $this->render(
            'molecules/modal/detail-quiz.html.twig',
            [
                'newArray' => $newArray,
                'titleStep' => $title, 
            ]
        );
    }


}
