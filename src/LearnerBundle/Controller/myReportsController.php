<?php

namespace LearnerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class myReportsController extends Controller
{
    public function indexAction()
    {
        return $this->render('LearnerBundle:myReports:index.html.twig', array(
            'interventions' => [],
        ));
    }
}
