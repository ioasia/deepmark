<?php

namespace LearnerBundle\Controller;

use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;

trait HelperTrait
{
    public function setStatusModuleIteration(Profile $profile, Module $module, $statusId, $startTime = false)
    {
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));

        if (!isset($moduleIteration)) {
            $moduleIteration = new ModuleIteration();
            $moduleIteration->setLearner($profile);
            $moduleIteration->setModule($module);
            $moduleIteration->setExecutionDate(new \DateTime());
        }

        if ($startTime) {
            $moduleIteration->setStartTime(new \DateTime());
        }

        $status = $em->getRepository('ApiBundle:Status')
            ->findOneBy(['appId' => $statusId]);
        $moduleIteration->setStatus($status);

        $em->persist($moduleIteration);
        $em->flush();
    }

    public function updateProgressCourse(Module $module, Profile $profileLearner)
    {
        $em                  = $this->getDoctrine()->getManager();
        $intervention        = $module->getIntervention();
        $progress            = $this->countLearnerModuleByInterventionId($intervention, $profileLearner);
        $interventionLearner = $em->getRepository('ApiBundle:LearnerIntervention')
            ->findOneBy(['learner' => $profileLearner, 'intervention' => $intervention]);
        if ($interventionLearner) {
            $interventionLearner->setProgression($progress);
            if (100 === $progress) {
                $interventionLearner->setStatus(
                    $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::LEARNER_INTERVENTION_FINISHED,
                    ))
                );
            }
            $em->persist($interventionLearner);
            $em->flush();
        }
    }

    public function countLearnerModuleByInterventionId($course, $learner)
    {
        $em             = $this->getDoctrine()->getManager();
        $countIteration = 0;
        $nbModule       = $em->getRepository('ApiBundle:Module')
            ->findBy(array(
                'intervention' => $course
            ));
        foreach ($nbModule as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));
            $countIteration  = isset($moduleIteration) && $moduleIteration->getStatus()->getAppId() == Status::WITH_BOOKING_DONE ? $countIteration + 1 : $countIteration;
        }


        return ($countIteration / count($nbModule)) * 100;
    }

}
