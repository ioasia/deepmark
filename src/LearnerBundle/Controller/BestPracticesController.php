<?php

namespace LearnerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\BestPractice;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Traits\Controller\HasBestPractice;

/**
 * BestPracticesController controller.
 */
class BestPracticesController extends Controller
{
    use HasBestPractice;
    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $profile->getEntity()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        $result = $this->getBestPractice($id, $this->getCurrentUser());
        if (!$result) return $this->redirectToRoute('learner_best_practices_index');
        return $this->render('molecules/best-practices/view.html.twig', array(
            'bestPractice'          => $result['bestPractice'],
            'resourcePath'          => $result['resourcePath'],
            'others'                => $result['others'],
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser()),
            'isReview'              => $result['isReview'],
            'searchParam'           => $searchParam
        ));
    }

    public function indexAction(Request $request)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $profile->getEntity()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }
        // get tags
        $searchParam['tags'] = [];
        $tags         = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        foreach ($tags as $tag){
            $searchParam['tags'][$tag->getId()] = $tag->getTagName();
        }

        return $this->render('LearnerBundle:bestPractices:index.html.twig', array(
            'searchParam' => $searchParam
        ));
    }

    public function createAction(Request $request)
    {
        $answerId = $request->query->get('answerId');
        $resourceType = $request->query->get('resourceType');
        $params = [];
        if ($resourceType && $answerId) {
            $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($answerId);
            if ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION) {
                $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
                $params = [
                    'resourceType' => BestPractice::RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION,
                    'resourceId' => $defaultCorrection[0]->getId(),
                    'resourceParams' => $defaultCorrection[0]->getSpecifics(),
                    'moduleId' => $answer->getPlay()->getModule()->getId(),
                    'entityId' => $answer->getPlay()->getModule()->getIntervention()->getEntity()->getId(),
                    'file_descriptor' => null
                ];
            } else if ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_LEARNER_ANSWER) {
                $params = [
                    'resourceType' => BestPractice::RESOURCE_TYPE_QUIZ_LEARNER_ANSWER,
                    'resourceId' => $answer->getId(),
                    'resourceParams' => json_decode($answer->getParams())->answer,
                    'moduleId' => $answer->getPlay()->getModule()->getId(),
                    'entityId' => $answer->getPlay()->getModule()->getIntervention()->getEntity()->getId(),
                    'file_descriptor' => json_decode($answer->getParams())->file_descriptor
                ];
            }
        }
        $tags = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        $tagsName = [];
        foreach ($tags as $tag) {
            $tagsName[] = $tag->getTagName();
        }
        return $this->render('AdminBundle:bestPractices:create.html.twig', array(
            'tags' => $tagsName,
            'params' => $params,
        ));
    }
}
