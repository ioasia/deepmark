<?php

namespace LearnerBundle\Service;

use ApiBundle\Entity\NativeQuizAnswer;
use ApiBundle\Entity\NativeQuizQuestion;
use ApiBundle\Entity\Quizes;
use Doctrine\Common\Collections\Collection;

class QuizResultCounter
{
    public function countResult(Quizes $quiz, $answers)
    {
        $score = 0;
        $questions = $quiz->getQuestions();
        /** @var NativeQuizQuestion $question */
        foreach ($questions as $question) {
            if (false == empty($answers[$question->getId()]) && is_array($answers[$question->getId()])) {
                $score += $this->handleQuestionScore($answers[$question->getId()], $question->getAnswers());
            }
        }

        return $score;
    }

    protected function handleQuestionScore($answers, Collection $possibleAnswers)
    {
        $score = 0;
        $answers = array_combine($answers, $answers);
        /** @var NativeQuizAnswer $possibleAnswer */
        foreach ($possibleAnswers as $possibleAnswer) {
            if (isset($answers[$possibleAnswer->getId()])) {
                $score += $possibleAnswer->getPoints();
            }
        }

        return $score;
    }
}
