<?php

namespace Traits;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Trait HasSpreadsheet
 * @package Traits
 */
trait HasSpreadsheet
{
    /**
     * @var Spreadsheet $spreadsheet
     */
    protected $spreadsheet;

    /**
     * @required
     */
    public function setSpreadSheet()
    {
        $this->spreadsheet = new Spreadsheet();
    }

    /**
     * @param string $filePath
     */
    protected function writeToFile(string $filePath)
    {
        try {
            $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
            $writer->save($filePath . '_' . date("Y_m_d") . '.xlsx');
        } catch (\Exception $exception) {
        }
    }
}
