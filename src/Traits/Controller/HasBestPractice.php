<?php

namespace Traits\Controller;

use ApiBundle\Entity\BestPractice;
use ApiBundle\Entity\QuizUsersAnswers;
use Utils\Statistics\KeyNameConst;

trait HasBestPractice
{
    /**
     * @param $id
     * @param $profile
     * @return array
     * @throws \Exception
     */
    public function getBestPractice($id, $profile = null)
    {
        $option = [];
        $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->find($id);
        if (!$bestPractice) return false;

        $resourceType = $bestPractice->getResourceType();
        if ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_LEARNER_ANSWER) {
            $option = [KeyNameConst::KEY_MODULE => $bestPractice->getModule()];
            $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($bestPractice->getResourceId());
            $paramsAnswer = json_decode($answer->getParams());
            $resourceParam = $bestPractice->getResourceParam();
            switch ($resourceParam) {
                // resource_type is record
                case "record":
                    $resourcePath = '/files/quiz_learner/'.$answer->getPlay()->getQuiz()->getId().'/recording/'.$paramsAnswer->answer->record;
                    break;
                // resource_type is template
                case "template":
                    $resourcePath = '/files/quiz_learner/'.$answer->getPlay()->getQuiz()->getId().'/doc/'.$paramsAnswer->answer->template;
                    break;
                // resource_type is video, xls, doc, ppt, pdf, img
                case "video":
                case "xls":
                case "doc":
                case "ppt":
                case "pdf":
                case "img":
                    foreach ($paramsAnswer->answer->documents as $item) {
                        if ($item[0] == $resourceParam) {
                            if ($resourceType == 'pdf') {
                                $resourceType = 'doc';
                            }
                            $resourcePath = '/files/quiz_learner/'.$answer->getPlay()->getQuiz()->getId().'/'.$resourceParam.'/'.$item[1];
                        }
                    }
                    break;
                default:
                    $resourcePath = '';
            }
        } elseif ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION) {
            $option = [KeyNameConst::KEY_MODULE => $bestPractice->getModule()];
            $item = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->find($bestPractice->getResourceId());
            // get question
            $question = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestions')->find($item->getQuestion_id());
            $paramsAnswer = json_decode($item->getSpecifics());
            $resourceParam = $bestPractice->getResourceParam();
            switch ($resourceParam) {
                // resource_param is doc
                case "doc":
                    $resourcePath = '/files/quiz/'.$question->getId().'/doc/'.$paramsAnswer->doc;
                    break;
                // resource_param is song
                case "song":
                    $resourcePath = '/files/quiz/'.$question->getId().'/song/'.$paramsAnswer->song;
                    break;
                // resource_param is video
                case "video":
                    $resourcePath = '/files/quiz/'.$question->getId().'/video/'.$paramsAnswer->video;
                    break;
                default:
                    $resourcePath = '';
            }
        } elseif ($resourceType == BestPractice::RESOURCE_TYPE_EXTERNAL) {
            $option = [KeyNameConst::KEY_ENTITY => $bestPractice->getEntity()];
        }

        $others = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->getOtherBestPractices($bestPractice->getId(), $option);
        $response = $this->getDoctrine()->getRepository('ApiBundle:BestPracticeResponse')->findOneBy(['profile' => $profile, 'bestPractice' => $id]);

        return [
            'bestPractice'          => $bestPractice,
            'resourcePath'          => isset($resourcePath) ? $resourcePath : null,
            'others'                => isset($others) ? $others : null,
            'isReview'              => $response ? true : false
        ];
    }
}
