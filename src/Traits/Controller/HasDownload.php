<?php

namespace Traits\Controller;


use ApiBundle\Entity\FileDescriptor;
use AppBundle\Service\FileApi;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Utils\Filesystem;

/**
 * Trait HasDownload
 * @package Traits\Controller
 */
trait HasDownload
{
    /**
     * @param integer $id
     * @return BinaryFileResponse|JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function downloadAction($id)
    {
        return $this->download($id);
    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function download($id)
    {
        $fileDescriptor = $this->getDoctrine()->getRepository(FileDescriptor::class)->find($id);

        if (!$fileDescriptor) {
            return new JsonResponse(array(
                'Error' => 'Fichier non trouvé',
            ), 400);
        }

        $file = $this->get(FileApi::class)->downloadPrivateFile($fileDescriptor);

        return Filesystem::respondDownload($file, $fileDescriptor);
    }
}