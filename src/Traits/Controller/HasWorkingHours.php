<?php


namespace Traits\Controller;

use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use AppBundle\Entity\Person;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utils\Email;

/**
 * Trait HasWorkingHours
 * @package Traits\Controller
 */
trait HasWorkingHours
{
    public function daysProcessing($data)
    {
        $workingHours = [];
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        foreach ($days as $day) {
            if (isset($data[$day]) && $data[$day] = 'on') {
                $workingHours[$day]['startTime'] = $data['startTime-'.$day];
                $workingHours[$day]['endTime'] = $data['endTime-'.$day];
                $workingHours[$day]['startTimeAfternoon'] = $data['startTimeAfternoon-'.$day];
                $workingHours[$day]['endTimeAfternoon'] = $data['endTimeAfternoon-'.$day];
            }
        }
        return $workingHours;
    }
}
