<?php


namespace Traits\Controller;

use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use AppBundle\Entity\Person;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utils\Email;

/**
 * Trait HasUsers
 * @package Traits\Controller
 */
trait HasUsers
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Container
     */
    protected $container;

    /**
     * SendEmail constructor.
     * @param EntityManagerInterface $entityManager
     * @param Container $container
     */
    public function __construct(EntityManagerInterface $entityManager, Container $container)
    {
        $this->em        = $entityManager;
        $this->container = $container;
    }

    /**
     * @return Entity
     */
    public function getEntityFromProfile()
    {
        $profile = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $this->getUser(),
        ));

        return $profile ? $profile->getEntity() : new Entity;
    }

    /**
     * @return Profile
     */
    public function getCurrentProfile()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);
    }

    /**
     * @return Person
     */
    protected function getUser()
    {
        $user = parent::getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $user;
    }

    /**
     * @param Profile $profileEntity
     * @param array $option
     * @return boolean
     */
    public function sendEmail(Profile $profileEntity, $option = null)
    {
        $emailService = $this->container->get(Email::class);
        $emailOption  = [];

        // default as the registration email
        $emailOption['subject']  = $this->container->get('translator')->trans('admin.alerts.your account_activated');
        $emailOption['profile']  = $profileEntity;
        $emailOption['joinUrl']  = $this->container->get('router')->generate('fos_user_resetting_request', [], 0);
        $emailOption['template'] = 'emailRegistration';

        if (isset($option['subject'])) {
            $emailOption['subject'] = $option['subject'];
        }

        if (isset($option['template'])) {
            $emailOption['template'] = $option['template'];
        }

        // if it is booking confirm email
        if (isset($option['booked'])) {
            $emailOption['booked']  = $option['booked'];
            $emailOption['joinUrl'] = $this->container->get('router')->generate('learner_courses_module',
                ['id' => $option['booked']->getModule()->getId()], 0);
        }

        // if it is new course registration email
        if (isset($option['intervention'])) {
            $emailOption['intervention'] = $option['intervention'];
            $emailOption['duration']     = $option['duration'];
            $emailOption['joinUrl']      = $this->container->get('router')->generate('learner_courses_show', ['id' => $option['intervention']->getId()], 0);
        }

        if (isset($option['assignment'])) {
            $emailOption['assignment'] = $option['assignment'];
        }

        if (isset($option['joinUrl'])) {
            $emailOption['joinUrl'] = $option['joinUrl'];
        }

        return $emailService->send(
            $profileEntity->getPerson()->getEmail(),
            $emailOption
        );
    }

    /**
     * @param Intervention $intervention
     * @param $duration
     * @return void
     */
    public function sendCourseModification(Intervention $intervention, $duration)
    {
        $translator = $this->container->get('translator');
        $emailService = $this->container->get(Email::class);
        $option       = [
            'subject'      => $translator->trans('admin.alerts.course_modification'),
            'intervention' => $intervention,
            'joinUrl'      => $this->container->get('router')->generate('learner_courses_show',
                ['id' => $intervention->getId()], 0),
            'duration'     => $duration,
            'template'     => 'emailCourseModification',
        ];

        $moduleIterations = $intervention->getLearners();
        foreach ($moduleIterations as $moduleIteration) {
            $option['subject'] = $moduleIteration->getLearner()->getEntity()->getOrganisation()->getDesignation() . ', ' . $translator->trans('email.informs_you') . ': ' . $translator->trans('email.course_modification');
            $option['profile'] = $moduleIteration->getLearner();
            $emailService->send(
                $moduleIteration->getLearner()->getPerson()->getEmail(),
                $option
            );
        }
    }
}
