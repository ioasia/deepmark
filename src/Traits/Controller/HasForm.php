<?php

namespace Traits\Controller;

use Symfony\Component\Yaml\Yaml;

/**
 * Trait HasForm
 * @package Traits\Controller
 */
trait HasForm
{
    private $formConfigs;

    /**
     * @param string $configFile
     * @return $this
     * @throws \ReflectionException
     */
    protected function loadForm(string $configFile)
    {
        $className = str_replace((new \ReflectionClass($this))->getShortName(), '', get_class($this));
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $dirPath   = $this->get('kernel')->getRootDir() . '/../src/' . $className;

        $formFile = $dirPath . DIRECTORY_SEPARATOR . 'Forms' . DIRECTORY_SEPARATOR . $configFile . '.yml';

        if (!file_exists($formFile)) {
            throw new \Exception('Form file not found: ' . $formFile);
        }

        $this->formConfigs = Yaml::parse(file_get_contents($formFile));

        return $this;
    }
}
