<?php

namespace Traits\Controller;

use stdClass;

/**
 * Trait HasNavigator
 * @package Traits\Controller
 */
trait HasNavigator
{
    protected $items = [];

    protected function addNavigatorItem($label, $href, $class, $icon)
    {
        $item        = new stdClass;
        $item->label = $label;
        $item->href  = $href;
        $item->class = $class;
        $item->icon  = $icon;

        $this->items[] = $item;

        return $this;
    }

    protected function getNavigatorItems()
    {
        return $this->items;
    }
}
