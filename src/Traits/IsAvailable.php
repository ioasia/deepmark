<?php


namespace Traits;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use DateInterval;
use DateTime;
use ApiBundle\Entity\StoredModule;

trait IsAvailable
{
    /**
     * @param Module $module
     * @param BookingAgenda|null $booking
     * @return bool
     * @throws \Exception
     */
    public function isAvailableModule(Module $module)
    {
        $now = new \DateTime('now');
        $availableModule = ($module->getEnding() >= $now) ? true : false; //$module->getBeginning() <= $now &&
        $availableIntervention = $this->isAvailableIntervention($module->getIntervention());
        return ($availableModule && $availableIntervention);
    }

    /**
     * @param Intervention $intervention
     * @return bool
     * @throws \Exception
     */
    public function isAvailableIntervention(Intervention $intervention)
    {
        $now = new \DateTime('now');
        return ($intervention->getEnding() >= $now) ? true : false; //$intervention->getBeginning() <= $now &&
    }

    /**
     * @param Module $module
     * @param BookingAgenda $booking
     * @return bool
     * @throws \Exception
     */
    public function isAvailableBooking(Module $module, BookingAgenda $booking)
    {
        $now = new DateTime();
        $hours = $minutes = 0;
        $bookingDate = clone $booking->getBookingDate();
        $duration   = clone $module->getDuration();
        $hours      += intval($duration->format('H'));
        $minutes    += intval($duration->format('i'));
        $bookingDate->add(new DateInterval('PT' . $hours . 'H' . $minutes . 'M'));
        $available = true;
        // if the booking is expired
        if ($now > $bookingDate) {
            $available = false;
        }
        return $available;
    }

    public function isAvailableSessionBooking(ModuleSession $moduleSession)
    {
        $now = new DateTime();
        $sessionStartDate = $moduleSession->getSession()->getSessionDate();
        return ($now < $sessionStartDate) ? true : false;
    }

    /**
     * @param Profile $trainer
     * @param Module $module
     * @param DateTime $date
     * @param array $duration
     * @return bool
     * @throws \Exception
     */
    public function isAvailableBookingByProfile(Profile $profile, Module $module, DateTime $date, $duration)
    {
        $canBook = true;
        $bookings = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->findAvailableBookingByProfile($module, $profile, $date);
        if ($bookings) {
            foreach ($bookings as $booking) {
                $start = $booking->getBookingDate();
                $end = clone $start;
                $hour = (int)$booking->getModule()->getDuration()->format('G') + (int)$booking->getBookingDate()->format('G');
                $minutes = (int)$booking->getModule()->getDuration()->format('i') + (int)$booking->getBookingDate()->format('i');
                $end->setTime($hour, $minutes, 0);
                if (($duration['start'] > $start && $duration['start'] < $end && $duration['end'] > $end)
                    || ($duration['start'] <= $start && $duration['end'] >= $end)
                    || ($duration['end'] > $start && $duration['end'] < $end && $duration['start'] < $start)) {
                    $canBook = false;
                }
            }
        }
        return $canBook;
    }

    /**
     * @param Profile $trainer
     * @param Module $module
     * @param DateTime $date
     * @param array $duration
     * @return bool
     * @throws \Exception
     */
    public function isAvailableBookingClassroomTraining(Profile $trainer, Module $module, ModuleSession $moduleSession, DateTime $date)
    {
        $canBook = true;
        // find booking the same time for Classroom Training
        $bookings = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->findAvailableBookingBySession($module, $trainer, $date);

        // checking booking the same time for Classroom Training
        if ($bookings) {
            foreach ($bookings as $booking) {
                // the same trainer in a day but there is difference city and the same country
                if ($booking->getModuleSession()->getSessionPlace()->getCountry() != $moduleSession->getSessionPlace()->getCountry()) {
                    $canBook = false;
                } else if ($booking->getModuleSession()->getSessionPlace()->getCity() != $moduleSession->getSessionPlace()->getCity()) {
                    $canBook = false;
                }
            }
        }
        return $canBook;
    }

    /**
     * @param Profile $profile
     * @param DateTime $startTime
     * @param DateTime $endTime
     * @return bool
     * @throws \Exception
     */
    public function checkAvailabilityDelete(Profile $profile, DateTime $startTime, DateTime $endTime)
    {
        $canDelete = false;
        // find existing assignment resource
        $numOfAssignment = $this->getDoctrine()
            ->getRepository('ApiBundle:AssigmentResourceSystem')
            ->countAssigmentByProfile($profile, clone $startTime, clone $endTime);

        // find existing booking
        $numOfBooking = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->countBookingByProfile($profile, clone $startTime, clone $endTime);

        // there is no booking and assignment
        if (!$numOfAssignment && !$numOfBooking) {
            $canDelete = true;
        }
        return $canDelete;
    }

    /**
     * @param Profile $profile
     * @param DateTime $startTime
     * @param DateTime $endTime
     * @param Array $moduleSessionList
     * @return bool
     * @throws \Exception
     */
    public function checkAvailabilityDeleteForLearner(Profile $profile, DateTime $startTime, DateTime $endTime, $moduleSessionList)
    {
        $canDelete = false;

        // find existing assignment resource
        $numOfAssignment = 0;
        if ($moduleSessionList) {
            $numOfAssignment = $this->getDoctrine()
                ->getRepository('ApiBundle:AssigmentResourceSystem')
                ->countAssigmentByProfile(null, clone $startTime, clone $endTime, $moduleSessionList);
        }

        // find existing booking
        $numOfBooking = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->countBookingByProfile($profile, clone $startTime, clone $endTime);

        // there is no booking
        if (!$numOfBooking && !$numOfAssignment) {
            $canDelete = true;
        }
        return $canDelete;
    }

    // find all module session which assigned to this learner
    public function findModuleSessionForLearner(Profile $profile, $em)
    {
        $connection = $em->getConnection();
        $query = 'SELECT GROUP_CONCAT(module_sessions.module_session_id SEPARATOR ",") AS moduleSessionList
                    FROM
                        intervention AS intervention 
                    LEFT JOIN learner_intervention AS learners ON learners.intervention_id = intervention.id
                    LEFT JOIN `module` AS `module` ON module.intervention_id = intervention.id
                    LEFT JOIN `module_sessions` AS `module_sessions` ON module_sessions.module_id = module.id
                    WHERE module.storedModule_id IN ('.StoredModule::VIRTUAL_CLASS.', '.StoredModule::PRESENTATION_ANIMATION.') 
                        AND learners.learner_id = '.$profile->getId().'
                    GROUP BY intervention.id';
        $statement  = $connection->prepare($query);

        $statement->execute();
        $results = $statement->fetchAll();
        $moduleSessionList = '';
        foreach ($results as $k => $item) {
            $moduleSessionList .= $k == 0 ? $item['moduleSessionList'] : ',' . $item['moduleSessionList'];
        }

        return $moduleSessionList ? explode(',',$moduleSessionList) : null;
    }

    // check if the URL is valid URL
    function isValidURL($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

    // check if the URL is iframe embeddable
    function allowEmbed($url) {
        $header = @get_headers($url, 1);

        // URL okay?
        if (!$header || (stripos($header[0], '200 ok') === false && stripos($header[0], '301') === false)) return 0;

        // Check X-Frame-Option
        elseif (isset($header['X-Frame-Options']) && (stripos($header['X-Frame-Options'], 'SAMEORIGIN') !== false || stripos($header['X-Frame-Options'], 'deny') !== false)) {
            return 0;
        }
        // Everything passed? Return true!
        return 1;
    }
}
