<?php


namespace Traits;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use DateInterval;
use DateTime;

trait IsScorm
{
    /**
     * @return Profile
     */
    public function getProfileId()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()])->getId();
    }

    function readElement($SCOInstanceID, $VarName)
    {
        $db = $this->getDoctrine()->getManager();
        $safeVarName = $VarName;
        $profileId = $this->getProfileId();
        // Write your raw SQL
        $query = "select VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName') and (profileID = $profileId))";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();

        return $result ? $result[0]['VarValue'] : '';

    }

    function writeElement($SCOInstanceID, $VarName, $VarValue)
    {

        $db = $this->getDoctrine()->getManager();

        $safeVarName = $VarName;
        $safeVarValue = ($VarValue == 'undefined') ? null : $VarValue;

        // look for pre-existing values
        // Write your raw SQL
        $profileId = $this->getProfileId();
        $query = "select VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName') and (profileID = $profileId))";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();
        // if nothing found ...
        if (!$result) {
            $safeVarName = str_replace("'", "\'", $safeVarName);
            $safeVarValue = str_replace("'", "\'", $safeVarValue);
            $query = "insert into scormvars (SCOInstanceID,VarName,VarValue, profileID) values ($SCOInstanceID,'$safeVarName','$safeVarValue','$profileId')";
            $statementDB = $db->getConnection()->prepare($query);
            $statementDB->execute();
        } else {
            $safeVarValue = str_replace("'", "\'", $safeVarValue);
            // Write your raw SQL
            $query = "update scormvars set VarValue='$safeVarValue' where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName') and (profileID = $profileId))";
            // Prepare the query from $db
            $statementDB = $db->getConnection()->prepare($query);
            // Execute both queries
            $statementDB->execute();
        }

        return;
    }

    function getResultScorm($SCOInstanceID)
    {
        $data = [
            'rawsSore'      => '',
            'lessonStatus'  => '',
            'totalTime'     => '',
            'completionStatus' => '',
            'sessionTime'   => '',
            'scoreToPass'   => '',
        ];
        $profileId = $this->getProfileId();

        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "select VarName, VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (profileID = $profileId))";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();
        foreach ($result as $value) {
            if ($value['VarName'] == 'cmi.success_status' || $value['VarName'] == 'cmi.core.lesson_status') {
                $data['lessonStatus'] = $value['VarValue'];
            } else if ($value['VarName'] == 'cmi.session_time' || $value['VarName'] == 'cmi.core.total_time') {
                /*if ($value['VarName'] == 'cmi.session_time') {
                    $pieces = explode(".", $value['VarValue']);
                    $interval = new DateInterval($pieces[0].'S');
                    $data['totalTime'] = $interval->format('%H:%I:%S');
                } else {
                    $data['totalTime'] = $value['VarValue'];
                }*/
                // fixed error $interval = new DateInterval($pieces[0].'S');
                $data['totalTime'] = null;
                if ($value['VarName'] != 'cmi.session_time') {
                    $data['totalTime'] = $value['VarValue'];
                }
            } else if ($value['VarName'] == 'cmi.score.raw' || $value['VarName'] == 'cmi.core.score.raw') {
                $data['rawsSore'] = $value['VarValue'];
            } else if ($value['VarName'] == 'cmi.completion_status') {
                $data['completionStatus'] = $value['VarValue'];
            } else if ($value['VarName'] == 'cmi.core.session_time') {
                $data['sessionTime'] = $value['VarValue'];
            } else if ($value['VarName'] == 'adlcp:masteryscore') {
                $data['scoreToPass'] = $value['VarValue'];
            }
        }

        if ($data['lessonStatus'] == 'unknown') {
            $data['lessonStatus'] = $data['completionStatus'];
        }
        if ($data['totalTime'] == '0000:00:00') {
            $data['totalTime'] = $data['sessionTime'];
        }

        return $data;
    }

    function getScormSynthesis($SCOInstanceID, $moduleIteration, $minScore, $profileId = null)
    {
        $dataPassed = [
            'lessonStatus'      => null,
            'score'             => null,
            'moduleIteration'   => null,
        ];

        $maxScorePassed = 0;
        $isPassed = 0;
        if (!$profileId) {
            $profileId = $this->getProfileId();
        }
        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "select * from scorm_synthesis where ((scoinstance_id=$SCOInstanceID) and (profileID = $profileId))";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();
        foreach ($result as $k => $value) {
            if ($isPassed == 0) {
                if ($value['lesson_status'] == 'passed') {
                    $isPassed = 1;
                }
            }
            if ($maxScorePassed == 0) {
                $maxScorePassed = $value['score'];
                $dataPassed['score'] = $value['score'];
                $dataPassed['moduleIteration'] = $moduleIteration;
            } else {
                if ($value['score'] > $maxScorePassed) {
                    $maxScorePassed = $value['score'];
                    $dataPassed['score'] = $value['score'];
                    $dataPassed['moduleIteration'] = $moduleIteration;
                }
            }
        }
        if ($minScore != null) {
            if ($dataPassed['score'] >= $minScore) {
                $dataPassed['lessonStatus'] = 'passed';
            } else {
                $dataPassed['lessonStatus'] = 'failed';
            }
        } else {
            $dataPassed['lessonStatus'] = $isPassed ? 'passed' : 'N/A';
        }

        return $dataPassed;
    }

    function initializeElement($SCOInstanceID, $VarName, $VarValue)
    {
        $db = $this->getDoctrine()->getManager();
        // make safe for the database
        $safeVarName = $VarName;
        $safeVarValue = $VarValue;
        $profileId = $this->getProfileId();

        // look for pre-existing values
        // Write your raw SQL
        $query = "select VarValue from scormvars where ((SCOInstanceID=$SCOInstanceID) and (VarName='$safeVarName') and (profileID = $profileId))";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();
        // if nothing found ...
        if (!$result) {
            $safeVarName = str_replace("'", "\'", $safeVarName);
            $safeVarValue = str_replace("'", "\'", $safeVarValue);
            $query = "insert into scormvars (SCOInstanceID,VarName,VarValue, profileID) values ($SCOInstanceID,'$safeVarName','$safeVarValue','$profileId')";
            $statementDB = $db->getConnection()->prepare($query);
            $statementDB->execute();
        }

    }

    /**
     * @param $SCOInstanceID
     * @return string
     */
    public function initializeSCO($SCOInstanceID)
    {
        $profileId = $this->getProfileId();
        // has the SCO previously been initialized?
        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "select count(VarName) AS total from scormvars where (SCOInstanceID=$SCOInstanceID) and (profileID = $profileId)";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();

        // not yet initialized - initialize all elements
        //if (!$result) {

            // elements that tell the SCO which other elements are supported by this API
            $this->initializeElement($SCOInstanceID, 'cmi.core._children', 'student_id,student_name,lesson_location,credit,lesson_status,entry,score,total_time,exit,session_time');
            $this->initializeElement($SCOInstanceID, 'cmi.core.score._children', 'raw');

            // student information
            $this->initializeElement($SCOInstanceID, 'cmi.core.student_name', $this->getFromLMS('cmi.core.student_name'));
            $this->initializeElement($SCOInstanceID, 'cmi.core.student_id', $this->getFromLMS('cmi.core.student_id'));

            // test score
            $this->initializeElement($SCOInstanceID, 'cmi.core.score.raw', '');
            $this->initializeElement($SCOInstanceID, 'adlcp:masteryscore', $this->getFromLMS('adlcp:masteryscore'));

            // SCO launch and suspend data
            $this->initializeElement($SCOInstanceID, 'cmi.launch_data', $this->getFromLMS('cmi.launch_data'));
            $this->initializeElement($SCOInstanceID, 'cmi.suspend_data', '');

            // progress and completion tracking
            $this->initializeElement($SCOInstanceID, 'cmi.core.lesson_location', '');
            $this->initializeElement($SCOInstanceID, 'cmi.core.credit', 'credit');
            $this->initializeElement($SCOInstanceID, 'cmi.core.lesson_status', 'not attempted');
            $this->initializeElement($SCOInstanceID, 'cmi.core.entry', 'ab-initio');
            $this->initializeElement($SCOInstanceID, 'cmi.core.exit', '');

            // seat time
            $this->initializeElement($SCOInstanceID, 'cmi.core.total_time', '0000:00:00');
            $this->initializeElement($SCOInstanceID, 'cmi.core.session_time', '');

        //}

        // new session so clear pre-existing session time
        $this->writeElement($SCOInstanceID, 'cmi.core.session_time', '');

        // create the javascript code that will be used to set up the javascript cache, 
        $initializeCache = "var cache = new Object();\n";

        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "select VarName,VarValue from scormvars where (SCOInstanceID=$SCOInstanceID) and (profileID = $profileId)";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $result = $statementDB->fetchAll();

        foreach ($result as $row) {
            // make the value safe by escaping quotes and special characters
            $varValue = addslashes($row['VarValue']);
            $varName = $row['VarName'];
            // javascript to set the initial cache value

            // Create an array with the values you want to replace
            $searches = array("\r", "\n", "\r\n");
            // Replace the line breaks with a space
            $varValue = str_replace($searches, " ", $varValue);

            if ($varName) $initializeCache .= "cache['$varName'] = '$varValue';\n";
        }
        // return javascript for cache initialization to the calling program
        return $initializeCache;

    }

    // ------------------------------------------------------------------------------------
    // LMS-specific code
    // ------------------------------------------------------------------------------------
    function setInLMS($varname, $varvalue)
    {
        return "OK";
    }

    function getFromLMS($varName)
    {
        $profile = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $this->getUser(),
        ));

        switch ($varName) {

            case 'cmi.core.student_name':
                $varValue = $profile->getDisplayName();
                break;

            case 'cmi.core.student_id':
                $varValue = $profile->getId();
                break;

            case 'adlcp:masteryscore':
                $varValue = 0;
                break;

            case 'cmi.launch_data':
                $varValue = "";
                break;

            default:
                $varValue = '';

        }

        return $varValue;

    }

    function readIMSManifestFile($manifestfile)
    {

        // PREPARATIONS

        // central array for resource data
        global $resourceData;
        $SCORMVersion = '';
        // load the imsmanifest.xml file
        $xmlfile = new \DomDocument;
        $xmlfile->preserveWhiteSpace = false;
        $xmlfile->load($manifestfile);

        // adlcp namespace
        $manifest = $xmlfile->getElementsByTagName('manifest');
        $adlcp = $manifest->item(0)->getAttribute('xmlns:adlcp');

        // READ THE RESOURCES LIST

        // get SCORM version information
        $metaData = $xmlfile->getElementsByTagName('metadata');
        foreach ($metaData as $k => $meta) {
            if ($k == 0) {
                $SCORMVersion = $meta->textContent;
            }
        }

        // array to store the results
        $resourceData = array();

        // get the list of resource element
        $resourceList = $xmlfile->getElementsByTagName('resource');

        $r = 0;
        foreach ($resourceList as $rtemp) {

            // decode the resource attributes
            $identifier = $resourceList->item($r)->getAttribute('identifier');
            $resourceData[$identifier]['type'] = $resourceList->item($r)->getAttribute('type');
            $resourceData[$identifier]['scormtype'] = $resourceList->item($r)->getAttribute('adlcp:scormtype');
            $resourceData[$identifier]['href'] = $resourceList->item($r)->getAttribute('href');

            // list of files
            $fileList = $resourceList->item($r)->getElementsByTagName('file');

            $f = 0;
            foreach ($fileList as $ftemp) {
                $resourceData[$identifier]['files'][$f] = $fileList->item($f)->getAttribute('href');
                $f++;
            }

            // list of dependencies
            $dependencyList = $resourceList->item($r)->getElementsByTagName('dependency');

            $d = 0;
            foreach ($dependencyList as $dtemp) {
                $resourceData[$identifier]['dependencies'][$d] = $dependencyList->item($d)->getAttribute('identifierref');
                $d++;
            }

            $r++;

        }

        // resolve resource dependencies to create the file lists for each resource
        foreach ($resourceData as $identifier => $resource) {
            $resourceData[$identifier]['files'] = $this->resolveIMSManifestDependencies($identifier);
        }

        // READ THE ITEMS LIST

        // arrays to store the results
        $itemData = array();

        // get the list of resource element
        $itemList = $xmlfile->getElementsByTagName('item');

        $i = 0;
        foreach ($itemList as $itemp) {

            // decode the resource attributes
            $identifier = $itemList->item($i)->getAttribute('identifier');
            $itemData[$identifier]['identifierref'] = $itemList->item($i)->getAttribute('identifierref');
            $itemData[$identifier]['title'] = @$itemList->item($i)->getElementsByTagName('title')->item(0)->nodeValue;
            $itemData[$identifier]['masteryscore'] = @$itemList->item($i)->getElementsByTagNameNS($adlcp, 'masteryscore')->item(0)->nodeValue;
            $itemData[$identifier]['datafromlms'] = @$itemList->item($i)->getElementsByTagNameNS($adlcp, 'datafromlms')->item(0)->nodeValue;

            $i++;

        }

        // PROCESS THE ITEMS LIST TO FIND SCOS

        // array for the results
        $SCOdata = array();

        // loop through the list of items
        foreach ($itemData as $identifier => $item) {

            // find the linked resource
            $identifierref = $item['identifierref'];

            // is the linked resource a SCO? if not, skip this item
            if (strtolower($resourceData[$identifierref]['scormtype']) != 'sco') {
                //continue;
            }

            // save data that we want to the output array
            $SCOdata[$identifier]['title'] = $item['title'];
            $SCOdata[$identifier]['masteryscore'] = $item['masteryscore'];
            $SCOdata[$identifier]['datafromlms'] = $item['datafromlms'];
            $SCOdata[$identifier]['href'] = $resourceData[$identifierref]['href'];
            $SCOdata[$identifier]['files'] = $resourceData[$identifierref]['files'];

        }

        $SCOdata[$identifier]['version'] = $SCORMVersion;
        return $SCOdata;
    }

// ------------------------------------------------------------------------------------

// recursive function used to resolve the dependencies (see above)
    function resolveIMSManifestDependencies($identifier)
    {

        global $resourceData;

        $files = $resourceData[$identifier]['files'];

        $dependencies = isset($resourceData[$identifier]['dependencies']) ? $resourceData[$identifier]['dependencies'] : null;
        if (is_array($dependencies)) {
            foreach ($dependencies as $d => $dependencyidentifier) {
                if (is_array($files)) {
                    $files = array_merge($files, $this->resolveIMSManifestDependencies($dependencyidentifier));
                } else {
                    $files = $this->resolveIMSManifestDependencies($dependencyidentifier);
                }
                unset($resourceData[$identifier]['dependencies'][$d]);
            }
            $files = array_unique($files);
        }

        return $files;

    }
}
