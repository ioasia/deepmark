<?php

namespace Traits;

use DateTime;
use DateInterval;
use DatePeriod;
use ApiBundle\Entity\AvailableCalendar;

/**
 * Trait HasAvailableCalendar
 * @package Traits
 */
trait HasAvailableCalendar
{
    /**
     * @param $request
     * @param $profileEntity
     * @return bool
     * @throws \Exception
     */
    public function saveAvailableCalendar($request, $profileEntity)
    {
        $em = $this->getDoctrine()->getManager();
        $beginning = new DateTime();
        $ending    = clone $beginning;

        $ending->add(new \DateInterval('P30D'));

        $interval = DateInterval::createFromDateString('1 day');
        $ending->add($interval);

        $period = new DatePeriod($beginning, $interval, $ending);

        $all = $request->request->all();
        foreach ($period as $dayCurrent) {
            $dayName = date('l', $dayCurrent->getTimeStamp());
            foreach ($all as $key => $day) {
                if ($key === $dayName) {
                    $freezeTime = null;
                    $dayBegin = clone $dayCurrent;
                    $dayEnd   = clone $dayCurrent;

                    if ((!$all['startTime-'.$key] || !$all['endTime-'.$key]) && (!$all['startTimeAfternoon-'.$key] || !$all['endTimeAfternoon-'.$key])) {
                        continue;
                    } else if ((!$all['startTime-'.$key] || !$all['endTime-'.$key]) && ($all['startTimeAfternoon-'.$key] && $all['endTimeAfternoon-'.$key])) {
                        $start              = explode(':', $all['startTimeAfternoon-'.$key]);
                        $end                = explode(':', $all['endTimeAfternoon-'.$key]);
                    } else if (($all['startTime-'.$key] && $all['endTime-'.$key]) && (!$all['startTimeAfternoon-'.$key] || !$all['endTimeAfternoon-'.$key])) {
                        $start              = explode(':', $all['startTime-'.$key]);
                        $end                = explode(':', $all['endTime-'.$key]);
                    } else {
                        $start              = explode(':', $all['startTime-'.$key]);
                        $end                = explode(':', $all['endTimeAfternoon-'.$key]);
                        $freezeTime         = $all['endTime-'.$key] . '|' . $all['startTimeAfternoon-'.$key];
                    }

                    $beginTime = $dayBegin->setTime($start[0], $start[1]);
                    $endTime   = $dayEnd->setTime($end[0], $end[1]);

                    $availableCalendar = new AvailableCalendar();
                    $availableCalendar->setBeginning($beginTime);
                    $availableCalendar->setEnding($endTime);
                    $availableCalendar->setFreezeTime($freezeTime);
                    $availableCalendar->setProfile($profileEntity);
                    $em->persist($availableCalendar);
                    $em->flush();
                }
            }
        }
    }

    /**
     * Person $user
     * @return bool
     * @throws \Exception
     */
    public function saveDefaultAvailableCalendar($user)
    {
        $em = $this->container->get('doctrine')->getManager();
        $profileEntity = $this->container->get('doctrine')->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $user]);
        $existAvailability = $this->container->get('doctrine')->getRepository('ApiBundle:AvailableCalendar')
            ->countAvailabilityByProfile($profileEntity);

        if (!$existAvailability) {
            $beginning = new DateTime();
            $ending    = clone $beginning;

            $ending->add(new \DateInterval('P30D'));

            $interval = DateInterval::createFromDateString('1 day');
            $ending->add($interval);

            $period = new DatePeriod($beginning, $interval, $ending);

            $all = [
                "Monday" => "on",
                "startTime-Monday" => "9:00",
                "endTime-Monday" => "12:00",
                "startTimeAfternoon-Monday" => "13:00",
                "endTimeAfternoon-Monday" => "18:00",
                "Tuesday" => "on",
                "startTime-Tuesday" => "9:00",
                "endTime-Tuesday" => "12:00",
                "startTimeAfternoon-Tuesday" => "13:00",
                "endTimeAfternoon-Tuesday" => "18:00",
                "Wednesday" => "on",
                "startTime-Wednesday" => "9:00",
                "endTime-Wednesday" => "12:00",
                "startTimeAfternoon-Wednesday" => "13:00",
                "endTimeAfternoon-Wednesday" => "18:00",
                "Thursday" => "on",
                "startTime-Thursday" => "9:00",
                "endTime-Thursday" => "12:00",
                "startTimeAfternoon-Thursday" => "13:00",
                "endTimeAfternoon-Thursday" => "18:00",
                "Friday" => "on",
                "startTime-Friday" => "9:00",
                "endTime-Friday" => "12:00",
                "startTimeAfternoon-Friday" => "13:00",
                "endTimeAfternoon-Friday" => "18:00",
                "Saturday" => "on",
                "startTime-Saturday" => "9:00",
                "endTime-Saturday" => "12:00",
                "startTimeAfternoon-Saturday" => "13:00",
                "endTimeAfternoon-Saturday" => "18:00",
                "Sunday" => "on",
                "startTime-Sunday" => "9:00",
                "endTime-Sunday" => "12:00",
                "startTimeAfternoon-Sunday" => "13:00",
                "endTimeAfternoon-Sunday" => "18:00",
            ];
            foreach ($period as $dayCurrent) {
                $dayName = date('l', $dayCurrent->getTimeStamp());
                foreach ($all as $key => $day) {
                    if ($key === $dayName) {
                        $freezeTime = null;
                        $dayBegin = clone $dayCurrent;
                        $dayEnd   = clone $dayCurrent;

                        if ((!$all['startTime-'.$key] || !$all['endTime-'.$key]) && (!$all['startTimeAfternoon-'.$key] || !$all['endTimeAfternoon-'.$key])) {
                            continue;
                        } else if ((!$all['startTime-'.$key] || !$all['endTime-'.$key]) && ($all['startTimeAfternoon-'.$key] && $all['endTimeAfternoon-'.$key])) {
                            $start              = explode(':', $all['startTimeAfternoon-'.$key]);
                            $end                = explode(':', $all['endTimeAfternoon-'.$key]);
                        } else if (($all['startTime-'.$key] && $all['endTime-'.$key]) && (!$all['startTimeAfternoon-'.$key] || !$all['endTimeAfternoon-'.$key])) {
                            $start              = explode(':', $all['startTime-'.$key]);
                            $end                = explode(':', $all['endTime-'.$key]);
                        } else {
                            $start              = explode(':', $all['startTime-'.$key]);
                            $end                = explode(':', $all['endTimeAfternoon-'.$key]);
                            $freezeTime         = $all['endTime-'.$key] . '|' . $all['startTimeAfternoon-'.$key];
                        }

                        $beginTime = $dayBegin->setTime($start[0], $start[1]);
                        $endTime   = $dayEnd->setTime($end[0], $end[1]);

                        $availableCalendar = new AvailableCalendar();
                        $availableCalendar->setBeginning($beginTime);
                        $availableCalendar->setEnding($endTime);
                        $availableCalendar->setFreezeTime($freezeTime);
                        $availableCalendar->setProfile($profileEntity);
                        $em->persist($availableCalendar);
                        $em->flush();
                    }
                }
            }
        }
    }
}
