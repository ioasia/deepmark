<?php


namespace Traits;

use ApiBundle\Entity\BookingSignature;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\SessionSignature;
use ApiBundle\Entity\StoredModule;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

trait HasAttendance
{
    /**
     * @param $attendances
     * @return bool
     * @throws \Exception
     */
    public function saveAttendance($attendances, $type)
    {
        if ($attendances) {
            foreach ($attendances as $attendance) {
                if ($attendance['isAttendance']) {
                    $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                        'id' => $attendance['bookingId'],
                    ));
                    $bookingAgenda->setAttendanceDate($type == 'presence' ? new DateTime() : null);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($bookingAgenda);
                    $em->flush($bookingAgenda);
                }
            }
        }
    }

    /**
     * @param $signatures
     * @param $typeSignature
     * @return void
     * @throws \Exception
     */
    public function allowSignatures($signatures, $typeSignature)
    {
        if ($signatures) {
            foreach ($signatures as $signature) {
                $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                    'id' => $signature['bookingId'],
                ));
                $signatureNumber = $bookingAgenda->getModule()->getSignatureNumber();
                // for loop
                $params = [];
                for ($i = 1; $i <= $signatureNumber; $i++) {
                    $params[$i] = ($i == $typeSignature) ? (int) $signature['allowSignature'] : 0;
                }
                $bookingAgenda->setSignatureParams($params);
                $em = $this->getDoctrine()->getManager();
                $em->persist($bookingAgenda);
                $em->flush();
            }
        }
    }

    /**
     * @param $bookingId
     * @param $typeSignature
     * @param $isOpen
     * @return void
     * @throws \Exception
     */
    public function allowSignaturesForTrainer($bookingId, $typeSignature, $isOpen)
    {
        if ($bookingId) {
            $em = $this->getDoctrine()->getManager();
            $bookingAgenda = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->find($bookingId);

            if (StoredModule::ONLINE === $bookingAgenda->getModule()->getStoredModule()->getAppId()) {
                // todo: implement for trainer signature
            } else {
                // If open the signature then close all remain signatures
                if ($isOpen) {
                    $existingSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                        'profile' => $bookingAgenda->getTrainer(),
                        'session' => $bookingAgenda->getModuleSession()->getSession()
                    ));
                    if ($existingSignatures) {
                        foreach ($existingSignatures as $existingSignature) {
                            $existingSignature->setIsOpen(0);
                            $em->persist($existingSignature);
                        }
                    }
                }

                // close or open the signature
                $signature = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findOneBy(array(
                    'profile' => $bookingAgenda->getTrainer(),
                    'session' => $bookingAgenda->getModuleSession()->getSession(),
                    'signatureType' => $typeSignature
                ));

                if($signature){
                    $signature->setIsOpen($isOpen);
                }else {
                    $signature = new SessionSignature();
                    $signature->setProfile($bookingAgenda->getTrainer());
                    $signature->setSession($bookingAgenda->getModuleSession()->getSession());
                    $signature->setIsOpen($isOpen);
                    $signature->setSignatureType($typeSignature);
                }

                $em->persist($signature);
                $em->flush();
            }
        }
    }

    /**
     * @param $request
     * @return array
     * @throws \Exception
     */
    public function saveSessionSignature($request)
    {
        $filePath       = $fileId = '';
        $bookingId      = $request->request->get('bookingId');
        $signatureType  = $request->request->get('signatureType');
        $data_uri       = $request->request->get('signature');

        if ($bookingId && $data_uri) {
            $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/files/signature/';

            $fileName   = 'signature-' . sha1(uniqid(mt_rand(), true)) . '.png';
            $targetFile = $targetDir . $fileName;

            $encoded_image = explode(",", $data_uri)[1];
            $decoded_image = base64_decode($encoded_image);

            if (file_put_contents($targetFile, $decoded_image)) {
                // save the FileDescriptor object
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($fileName);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize('');
                $fileDescriptor->setMimeType('png');
                $fileDescriptor->setDirectory($targetDir);

                $em = $this->getDoctrine()->getManager();
                $em->persist($fileDescriptor);
                $em->flush();

                $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                    'id' => $bookingId,
                ));

                $signature = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findOneBy(array(
                    'profile' => $bookingAgenda->getTrainer(),
                    'session' => $bookingAgenda->getModuleSession()->getSession(),
                    'signatureType' => $signatureType
                ));
                if($signature){
                    $signature->setSignature($fileDescriptor);
                }else {
                    $signature = new SessionSignature();
                    $signature->setProfile($bookingAgenda->getTrainer());
                    $signature->setSession($bookingAgenda->getModuleSession()->getSession());
                    $signature->setSignature($fileDescriptor);
                    $signature->setSignatureType($signatureType);
                }

                $em->persist($signature);
                $em->flush();

                $filePath = '/trainer/documents/download/' . $fileDescriptor->getId();
                $fileId   = $fileDescriptor->getId();
            }
        }

        $response = new Response(json_encode(array('filePath' => $filePath, 'fileId' => $fileId,
            'signatureDate' => isset($signature) ? $signature->getCreated()->format('d/m/Y H:i:s') : '')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param $request
     * @return array
     * @throws \Exception
     */
    public function saveSignature($request)
    {
        $filePath       = $fileId = '';
        $signatureTotal = 0;
        $disabledButton = false;
        $bookingId      = $request->request->get('bookingId');
        $signatureType      = $request->request->get('signatureType');
        $data_uri       = $request->request->get('signature');

        if ($bookingId && $data_uri) {
            $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/files/signature/';

            $fileName   = 'signature-' . sha1(uniqid(mt_rand(), true)) . '.png';
            $targetFile = $targetDir . $fileName;

            $encoded_image = explode(",", $data_uri)[1];
            $decoded_image = base64_decode($encoded_image);

            if (file_put_contents($targetFile, $decoded_image)) {
                // save the FileDescriptor object
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($fileName);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize('');
                $fileDescriptor->setMimeType('png');
                $fileDescriptor->setDirectory($targetDir);

                $em = $this->getDoctrine()->getManager();
                $em->persist($fileDescriptor);
                $em->flush();

                // save the booking agenda object
                $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                    'id' => $bookingId,
                ));

                $signature = $this->getDoctrine()->getRepository('ApiBundle:BookingSignature')->findOneBy(array(
                    'session' => $bookingAgenda,
                    'signatureType' => $signatureType,
                    'learner' => $bookingAgenda->getLearner()
                ));
                if ($signature) {
                    $signature->setSignature($fileDescriptor);

                    $em->persist($signature);
                    $em->flush();
                } else {
                    $signature = new BookingSignature();
                    $signature->setLearner($bookingAgenda->getLearner());
                    $signature->setSession($bookingAgenda);
                    $signature->setSignature($fileDescriptor);
                    $signature->setSignatureType($signatureType);

                    $em->persist($signature);
                    $em->flush();
                }

                foreach ($bookingAgenda->getBookingSignatures() as $signature) {
                    if ($signature->getSignatureType() == $signatureType) {
                        $signatureTotal += 1;
                    }
                }

                $moduleSignatures = $signatureType == 1 ? $bookingAgenda->getModule()->getSignatureNumber() : $bookingAgenda->getModule()->getSignatureAfternoon();

                if ($signatureTotal >= $moduleSignatures) {
                    $disabledButton = true;
                }

                $filePath = '/trainer/documents/download/' . $fileDescriptor->getId();
                $fileId   = $fileDescriptor->getId();
            }
        }

        $response = new Response(json_encode(array('filePath' => $filePath, 'fileId' => $fileId, 'disabledButton' => true,
            'signatureDate' => isset($signature) ? $signature->getCreated()->format('d/m/Y H:i:s') : '')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
