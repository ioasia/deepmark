<?php


namespace Traits\Users;

use ApiBundle\Entity\Entity;
use ApiBundle\Entity\GroupOrganization;
use ApiBundle\Entity\Organisation;
use ApiBundle\Entity\Regional;
use ApiBundle\Entity\Workplace;
use Doctrine\ORM\EntityManager;

/**
 * Trait HasOrganizations
 * @package Traits\Users
 */
trait HasOrganizations
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param array $data
     * @param bool $isNew
     * @return Organisation|bool|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createOrganisation(array $data, &$isNew = false)
    {
        if (empty($data['designation'])) {
            return false;
        }

        if (!$entity = $this->checkEntityExistsByDesignation(Organisation::class, ['designation' => $data['designation']])) {
            $entity = new Organisation();
            $entity->setDesignation($data['designation']);
            $entity->setStreet('N/A');
            $entity->setCreated(new \DateTime());
            $entity->setCity('N/A');
            $entity->setZipCode('N/A');
            $entity->setCountry('N/A');
            $isNew = true;

            $this->em->persist($entity);
        }

        return $entity;
    }

    /**
     * @param array $data
     * @param bool $isNew
     * @return Entity|bool|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createEntity(array $data, &$isNew = false)
    {
        if (empty($data['designation'])) {
            return false;
        }

        if (!$entity = $this->checkEntityExistsByDesignation(Entity::class, ['designation' => $data['designation']])) {
            $entity = new Entity();
            $entity->setDesignation($data['designation']);
            $entity->setStreet('N/A');
            $entity->setCreated(new \DateTime());
            $isNew = true;

            $this->em->persist($entity);
        }

        return $entity;
    }

    /**
     * @param array $data
     * @param bool $isNew
     * @return Regional|bool|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createRegional(array $data, &$isNew = false)
    {
        if (empty($data['entity']) || is_null($data['entity'])) {
            return false;
        } else {
            $data['designation'] = !empty($data['designation']) ? $data['designation'] : '--';
            if (!$entity = $this->checkEntityExistsByDesignation(Regional::class, ['designation' => $data['designation'], 'entity' => $data['entity']])) {
                $entity = new Regional();
                $entity->setDesignation($data['designation']);
                $entity->setEntity($data['entity']);
                $entity->setCreated(new \DateTime());
                $isNew = true;

                $this->em->persist($entity);
            }
            return $entity;
        }
    }

    /**
     * @param array $data
     * @param bool $isNew
     * @return GroupOrganization|bool|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createGroupOrganization(array $data, &$isNew = false)
    {
        if (empty($data['regional']) || is_null($data['regional'])) {
            return false;
        } else {
            $data['designation'] = !empty($data['designation']) ? $data['designation'] : '--';
            if (!$entity = $this->checkEntityExistsByDesignation(GroupOrganization::class, ['designation' => $data['designation'], 'regional' => $data['regional']])) {
                $entity = new GroupOrganization();
                $entity->setDesignation($data['designation']);
                $entity->setRegional($data['regional']);
                $entity->setCreated(new \DateTime());
                $isNew = true;

                $this->em->persist($entity);
            }

            return $entity;
        }
    }

    /**
     * @param array $data
     * @param bool $isNew
     * @return GroupOrganization|bool|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createWorkplace(array $data, &$isNew = false)
    {
        if (empty($data['group']) || is_null($data['group'])) {
            return false;
        } else {
            $data['designation'] = !empty($data['designation']) ? $data['designation'] : '--';
            if (!$entity = $this->checkEntityExistsByDesignation(Workplace::class, ['designation' => $data['designation'], 'groupOrganization' => $data['group']])) {
                $entity = new Workplace();
                $entity->setDesignation($data['designation']);
                $entity->setGroupOrganization($data['group']);
                $entity->setCreated(new \DateTime());
                $isNew = true;

                $this->em->persist($entity);
            }

            return $entity;
        }
    }

    /**
     * @param string $entityName
     * @param array $condition
     * @return object|null
     */
    private function checkEntityExistsByDesignation(string $entityName, array $condition)
    {
        return $this->em->getRepository($entityName)->findOneBy($condition);
    }
}
