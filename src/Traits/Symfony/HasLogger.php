<?php

namespace Traits\Symfony;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasLogger
 * @package Traits\Symfony
 */
trait HasLogger
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @return object|\Symfony\Bridge\Monolog\Logger|null
     */
    protected function getLogger()
    {
        return $this->container->get('logger');
    }
}
