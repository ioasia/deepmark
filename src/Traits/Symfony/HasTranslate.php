<?php

namespace Traits\Symfony;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

trait HasTranslate
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return string
     */
    protected function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
        return $this->container->get('translator')->trans($id, $parameters, $domain, $locale);
    }
}
