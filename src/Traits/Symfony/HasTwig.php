<?php

namespace Traits\Symfony;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

/**
 * Trait HasTwig
 * @package Traits\Symfony
 */
trait HasTwig
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @return mixed|Environment
     */
    protected function getTwig()
    {
        return $this->container->get('twig');
    }
}
