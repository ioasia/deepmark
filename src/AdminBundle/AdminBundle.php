<?php

namespace AdminBundle;

use DependencyInjection\Compiler\ParametersCompilerPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class AdminBundle
 * @package AdminBundle
 */
class AdminBundle extends \BaseBundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ParametersCompilerPass, PassConfig::TYPE_AFTER_REMOVING);
    }
}
