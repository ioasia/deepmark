<?php

namespace AdminBundle\Services;

use ApiBundle\Entity\InterventionStep;

class InterventionStepConsistentService
{
    public function makeStepConsistent(InterventionStep $interventionStep): InterventionStep
    {
        $this->moveDataFromStep2IntoStep1($interventionStep);

        return $interventionStep;
    }

    /**
     * @todo in future when more data consistent things will appear. We can move each function to different class.
     */
    private function moveDataFromStep2IntoStep1(InterventionStep $interventionStep)
    {
        $stepTwo = $interventionStep->getStep2();
        $stepOne = $interventionStep->getStep1();

        if ((!isset($stepOne['module']) || !isset($stepTwo['module']))) {
            return;
        }

        if (empty($stepOne['module']) || empty($stepTwo['module'])) {
            return;
        }

        //change from step two to step one dates

        foreach ($stepTwo['module'] as $moduleId => $moduleData) {
            if (!$this->moduleExists($moduleId, $stepTwo)) {
                continue;
            }

            $durationHours = isset($stepTwo['module'][$moduleId]['durationHours']) ? $stepTwo['module'][$moduleId]['durationHours'] : 0;
            $durationsMinutes = isset($stepTwo['module'][$moduleId]['durationMinutes']) ? $stepTwo['module'][$moduleId]['durationMinutes'] : 0;

            // issue 248: remove it because the old logic with day was wrong
            /*if (7 === $durationHours) {
                $stepOne['module'][$moduleId]['duration'] = 1;
                $stepOne['module'][$moduleId]['time_param'] = 2;
            } elseif ($durationHours > 0 && $durationHours < 7) {
                $stepOne['module'][$moduleId]['duration'] = $durationHours;
                $stepOne['module'][$moduleId]['time_param'] = 1;
            } else {
                $stepOne['module'][$moduleId]['duration'] = $durationsMinutes;
                $stepOne['module'][$moduleId]['time_param'] = 0;
            }*/

            if($durationHours > 0){
                if($durationsMinutes > 0){
                    $stepOne['module'][$moduleId]['duration'] = $durationHours*60 + $durationsMinutes;
                    $stepOne['module'][$moduleId]['time_param'] = 0;
                }else{
                    $stepOne['module'][$moduleId]['duration'] = $durationHours;
                    $stepOne['module'][$moduleId]['time_param'] = 1;
                }
            }else{
                $stepOne['module'][$moduleId]['duration'] = $durationsMinutes;
                $stepOne['module'][$moduleId]['time_param'] = 0;
            }
            // issue 248: remove it because the old logic with day was wrong
        }

        $interventionStep->setStep1($stepOne);
    }

    private function moduleExists($moduleId, $stepOne)
    {
        return isset($stepOne['module'][$moduleId]);
    }
}
