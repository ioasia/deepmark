<?php

namespace AdminBundle\Services;

use Psr\Container\ContainerInterface;

class EntityManagerService
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
