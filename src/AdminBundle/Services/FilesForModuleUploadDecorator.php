<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 06.02.19
 * Time: 11:39.
 */

namespace AdminBundle\Services;

class FilesForModuleUploadDecorator
{
    private $templating;

    private $decorated;

    public function __construct(TemplatingService $templating)
    {
        $this->templating = $templating;
    }

    public function decorate(array $files): void
    {
        $templating = $this->templating->getTemplating();
        $this->decorated = $templating->render('AdminBundle:Intervention/ModuleCard/CommonViews:all-uploaded-module-files-common-view.html.twig', ['files' => $files, 'session' => $_SESSION]);
    }

    public function getDecorated(): string
    {
        return $this->decorated;
    }
}
