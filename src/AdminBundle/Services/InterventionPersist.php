<?php

namespace AdminBundle\Services;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Utils\FileUplaod\FileRetrieval;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\Chapter;
use ApiBundle\Entity\ChapterModule;
use ApiBundle\Entity\DaySlots;
use ApiBundle\Entity\EchoModule;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\ExcludeWeeks;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleAssessments;
use ApiBundle\Entity\ModuleSend;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\ModuleTemplate;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\Session;
use ApiBundle\Entity\SessionPlace;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\TrainerReservation;
use ApiBundle\Service\UploadFile;
use DateTime;
use Exception;
use MediaEmbed\MediaEmbed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\SplFileInfo;
use Utils\Validations\Course\Modules;

class InterventionPersist
{
    private $entityManager;

    private $interventionStep;

    private $intervention;

    private $client;

    private $admin;

    private $container;

    private $foldersToSearch = [
        'trainer',
        'learner',
        'eLearning',
        'trainerReportTemplate',
        'attendanceSheetTemplate',
        'certificateTemplate',
    ];

    private $fileApi;

    public function __construct(EntityManagerService $entityManager, UploadFile $fileApi, ContainerInterface $container)
    {
        $this->entityManager = $entityManager->getDoctrine();
        $this->fileApi       = $fileApi;
        $this->container     = $container;
    }

    public function getIntervention()
    {
        return $this->intervention;
    }

    public function setCombinedSteps(InterventionStep $interventionStep)
    {
        $this->interventionStep = $interventionStep->getCombinedSteps();
    }

    /**
     * @return Profile
     *
     * @throws IncorrectArgumentException
     */
    public function getClient(): Profile
    {
        if (empty($this->client)) {
            $client = $this->entityManager->getRepository(Profile::class)
                ->find($this->interventionStep['clientIntervention']);

            if (empty($client)) {
                throw new IncorrectArgumentException('Customer has not been found');
            }

            $this->client = $client;
        }

        return $this->client;
    }

    public function setAdmin(Profile $admin)
    {
        $this->admin = $admin;
    }

    public function setFiles()
    {
        $fileUpload = new FileRetrieval();
        foreach ($this->interventionStep['module'] as $uniqueId => $steps) {
            foreach ($this->foldersToSearch as $folder) {
                $this->attachFiles($fileUpload, $uniqueId, $folder);
            }
        }
    }

    private function attachFiles(FileRetrieval $fileUpload, $uniqueId, $folderName)
    {
        if (!empty($fileUpload->getFiles($uniqueId, $folderName))) {
            $tmpContent                                                                = $fileUpload->getFiles($uniqueId,
                $folderName);
            $this->interventionStep['module'][$uniqueId]['filesAttached'][$folderName] = $tmpContent;
        }
    }

    private function getStoredModuleByAppId($appId): ?StoredModule
    {
        return $this->entityManager->getRepository(StoredModule::class)
            ->findOneBy(array(
                'appId' => $appId,
            ));
    }

    private function getStatusByAppId($appId): ?Status
    {
        return $this->entityManager->getRepository(Status::class)
            ->findOneBy(array(
                'appId' => $appId,
            ));
    }

    private function getEducationDocVarietyByAppId($appId): ?EducationalDocVariety
    {
        return $this->entityManager->getRepository(EducationalDocVariety::class)
            ->findOneBy(array(
                'appId' => $appId,
            ));
    }

    private function createEducationalDocument(SplFileInfo $fileInfo, $s3Upload = 0)
    {
        $educationalDocument = new EducationalDocument();
        $pathName            = $fileInfo->getPathname();

        $path           = explode("Temp", $pathName);
        $isDownload     = isset($_SESSION[$path[1]]) ? $_SESSION[$path[1]] : 0;
        $fileDescriptor = $this->fileApi->saveTempFile($fileInfo);
        if ($s3Upload) {
            try {
                $storage = $this->container->get('storage');
                $storage->uploadFile($this->fileApi->getTargetDirectory() . $fileDescriptor->getPath());
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $educationalDocument->setFileDescriptor($fileDescriptor);
        $extension = $fileInfo->getExtension();
        $docType   = EducationalDocVariety::DOCUMENT;

        switch ($extension) {
            case 'mp4':
                $docType = EducationalDocVariety::VIDEO;
                break;
            case 'aac':
                $docType = EducationalDocVariety::AUDIO;
                break;
        }

        $educationalDocument->setEducationalDocVariety(
            $this->getEducationDocVarietyByAppId($docType)
        );
        $educationalDocument->setIsDownload($isDownload);

        return $educationalDocument;
    }

    /**
     * @throws IncorrectArgumentException
     * @throws Exception
     */
    public function saveIntervention()
    {
        if (empty($this->interventionStep)) {
            throw new IncorrectArgumentException('Step value array cannot be empty');
        }

        $data  = $this->interventionStep;
        $admin = $this->admin;

        $this->intervention = new Intervention();
        $format             = 'd/m/Y';

        $now = new DateTime('now');

        $this->intervention
            ->setDesignation($data['title'])
            ->setCustomer($admin)->setEntity($admin->getEntity())
            ->setAdmin($admin)
            ->setBeginning(DateTime::createFromFormat($format, $data['intervention_start_date']))
            ->setEnding(DateTime::createFromFormat($format, $data['intervention_end_date']))
            ->setSequential(isset($data['constraints']['sequential']) && $data['constraints']['sequential'] == 'on' ? true : false)
            ->setComment($data['description'])
            ->setStatus($this->getStatusByAppId(Status::INTERVENTION_CREATED));

        if ($data['constraints']) {
            $this->intervention->setOpenDays($data['constraints']['days_open_for_session']);
        }

        /**
         * @var Modules $moduleValidations
         */
        $moduleValidations = $this->container->get(Modules::class);
        $moduleValidations->validate($data['module']);

        if ($data['storedLogo']) {
            $logo = $this->fileApi->saveTempFile(new \SplFileInfo($data['storedLogo']));
            $this->intervention->setLogo($logo);
        }

        $modules         = $data['module'];
        $modulesToAssign = [];
        $modulesToAssessment = [];
        $assessmentModule = null;
        $assessmentData = null;
        $modulesAssignToChapter = [];
        $addChapter = null;
        foreach ($modules as $uniqueId => $module) {
            if ($module['appId'] == 12) {
                $addChapter = new Chapter();
                $addChapter->setChapterName($module['title'])->setPosition($module['position']);
                $this->intervention->addChapter($addChapter);
                continue;
            }
            $addModule = new Module();
            $addModule
                ->setDesignation($module['title'])
                ->setStoredModule($this->getStoredModuleByAppId((int)$module['appId']))
                ->setQuantity((int)$module['quantity'])
                ->setPrice((float)$module['price']);

            $appId = $addModule->getStoredModule()->getAppId();

            if ($appId > 3) {
                $addModule
                    ->setDescription($module['instruction'])
                    ->setBeginning(DateTime::createFromFormat($format, $module['startDate']))
                    ->setEnding(DateTime::createFromFormat($format, $module['endDate']))
                    ->setDuration(clone $now->setTime($module['durationHours'], $module['durationMinutes']));
            }

            $addModule = $this->hydrateModuleWithFiles($module, $addModule);

            if (StoredModule::ELEARNING === $appId) {
                if ($module['uploadType'] == EducationalDocVariety::SCORM && $module['isNeedScoreToPass'] == 1 ) {
                    $addModule->setMinScore($module['elearningScore']);
                    $addModule->setIsNeedScoreToPass($module['isNeedScoreToPass']);
//                    $addModule->setIteration($module['maxIteration']);
                }
            }

            if (StoredModule::QUIZ === $appId) {
                /** @var Quiz $quiz */
                if (isset($module['quiz']) && $module['quiz']) {
                    $quiz = $this->entityManager->getRepository(Quizes::class)
                        ->find((int)$module['quiz']);
                    $addModule->setQuiz($quiz);
                }
                $addModule->setIteration($module['maxIteration']);
                if ($quiz->getIsSurvey() != 1) {
                    $addModule->setTypeQuiz($module['typeQuiz']);
                    $addModule->setScoreQuiz($module['min_score']);
                }

                if (!empty($module['best_practice_score'])){
                    $addModule->setBestPracticeScore($module['best_practice_score']);
                }

                $addModule->setDurationActive($module['durationActive']);
                $addModule->setCorrectionDuration(clone $now->setTime($module['correctionDurationHours'], $module['correctionDurationMinutes']));
                $addModule->setCorrectionDurationDays($module['correctionDurationDays']);
                $addModule->setCorrectionDurationMax(DateTime::createFromFormat('d/m/Y', $module['correctionDurationMax']));
                if(isset($module['poolTrainers'])) {
                    $addModule->setPoolTrainers($module['poolTrainers']);
                } else {
                    $addModule->setPoolTrainers(0);
                } 
                if(isset($module['showCorrection'])) {
                    $addModule->setshowCorrection($module['showCorrection']);
                } else {
                    $addModule->setshowCorrection(0);
                } 
            }

            if (StoredModule::EVALUATION === $appId && isset($module['quiz'])) {
                $tempQuizId = explode("||", $module['quiz']);
                $evaluation = $this->entityManager->getRepository(EchoModule::class)
                    ->findOneBy(['echo_id' => (int)$tempQuizId[0]]);
                if (!$evaluation) {
                    $evaluation = new EchoModule();
                    $evaluation->setTitle($tempQuizId[1]);
                    $evaluation->setEchoId($tempQuizId[0]);
                    $evaluation->setCreateDate(new \DateTime());
                    $this->entityManager->getManager()->persist($evaluation);
                    $this->entityManager->getManager()->flush();
                }

                $addModule->setEchoModule($evaluation);
            }

            $sessionsModule = [
                StoredModule::VIRTUAL_CLASS,
                StoredModule::ONLINE_WORKSHOP,
                StoredModule::PRESENTATION_ANIMATION,
            ];

            $addSessions = [];
            if (in_array($appId, $sessionsModule)) {
                $addModule->setSignatureParameters($module['signatures']);
                $sessions = $module['session'];

                foreach ($sessions as $session) {
                    $addModuleSession = new ModuleSession();
                    if (StoredModule::PRESENTATION_ANIMATION == $appId) {
                        $sessionPlace = new SessionPlace();
                        $sessionPlace->setPlace($session['place']);
                        $sessionPlace->setAddress($session['address']);
                        $sessionPlace->setCity($session['town']);
                        $sessionPlace->setCountry($session['country']);
                        $addModuleSession->setSessionPlace($sessionPlace);
                    }
                    $addSession  = new Session();
                    $dateSession = DateTime::createFromFormat('d/m/Y', $session['startDate']);
                    $startTime   = new DateTime($session['startTime']);
                    $dateSession->setTime($startTime->format('H'), $startTime->format('i'));
                    $addSession->setSessionDate($dateSession);
                    $addSession->setParticipant((int)$session['participants']);
                    $addSession->setParticipantMax((int)$session['participants_max']);
                    $addModuleSession->setSession($addSession);
                    $addModule->addSession($addModuleSession);
                    $addSessions[$session['unique-session-id']] = $addModuleSession;
                }
            }

            $addModule->setIntervention($this->intervention);
            if ($addChapter) {
                array_push($modulesAssignToChapter, array(
                    'chapter'   => $addChapter,
                    'module'    => $addModule,
                    'position'  => $module['position'],
                ));
            }
            $this->intervention->addModule($addModule);

            if (in_array($appId, StoredModule::MODULE_REQUIRE_BOOKING)) {
                array_push($modulesToAssign, array(
                    'moduleUniqueId' => $uniqueId,
                    'module'         => $addModule,
                    'sessions'       => $addSessions,
                ));
                if (isset($module['signatureNumber'])) {
                    $addModule->setSignatureNumber($module['signatureNumber']);
                }
                /*if (isset($module['signatureAfternoon'])) {
                    $addModule->setSignatureAfternoon($module['signatureAfternoon']);
                }*/
                if (isset($module['minScore'])) {
                    $addModule->setMinScore($module['minScore']);
                }
                if (StoredModule::ONLINE == $appId || StoredModule::PRESENTATION_ANIMATION == $appId) {
                    $addModule->setIsNeedReport($module['isNeedReport']);
                    if (!empty($module['profileSendTo'])) {
                        foreach ($module['profileSendTo'] as $profileId) {
                            $profile    = $this->entityManager->getRepository(Profile::class)->find($profileId);
                            $moduleSend = new ModuleSend();
                            $moduleSend->setModule($addModule);
                            $moduleSend->setProfile($profile);
                            $moduleSend->setStatus($this->getStatusByAppId(Status::REPORT_UNDONE));
                            $this->entityManager->getManager()->persist($moduleSend);
                        }
                    }
                }
                if (isset($module['isPersonalBooking'])) {
                    $addModule->setIsPersonalBooking($module['isPersonalBooking']);
                }
                if (isset($module['signature'])) {
                    $addModule->setSignature($module['signature']);
                }
            }

            if ($appId > 3 && $appId != 11) {
                $modulesToAssessment[$uniqueId] = $addModule;
            } elseif ($appId == 11) {
                $assessmentModule = $addModule;
                $assessmentData = $module;
            }
        }
        $this->entityManager->getManager()->persist($this->intervention);
        $this->assignModules($modulesToAssign);
        // assign modules to the chapter
        if ($modulesAssignToChapter) {
            $this->assignModulesToChapter($modulesAssignToChapter);
        }
        if ($assessmentModule && $modulesToAssessment) {
            $this->assignAssessments($modulesToAssessment, $assessmentModule, $assessmentData);
        }
        $this->entityManager->getManager()->flush();
        $this->addSkillsToModule($modulesToAssign, $modules);
        // add more constraints with day slots and weeks exclude
        $this->addConstraints($data, $now);
    }

    private function hydrateModuleWithFiles(array $moduleData, Module $module)
    {

        if (isset($moduleData['uploadType']) && $moduleData['uploadType'] == EducationalDocVariety::URL) {
            $MediaEmbed = new MediaEmbed();
            $mediaName = 'Site';
            $iframe     = $MediaEmbed->parseUrl($moduleData['url']);
            if ($iframe) {
                $iframe->setParam([
                    'autoplay' => 1,
                    'loop'     => 1,
                ]);
                $iframe->setAttribute([
                    'type'                 => null,
                    'class'                => 'courses-video-url',
                    'data-html5-parameter' => true,
                ]);
                $mediaName = 'MediaEmbed';
            }
            $educationalDocumentURL = new EducationalDocument();
            $educationalDocumentURL->setUrl($iframe?$iframe:$moduleData['url']);
            $educationalDocumentURL->setName($mediaName);
            $educationalDocumentURL->setEducationalDocVariety(
                $this->getEducationDocVarietyByAppId(EducationalDocVariety::URL)
            );
            $educationalDocumentURL->setModule($module);
            $module->setMediaDocument($educationalDocumentURL);
        }

        if (array_key_exists('filesAttached', $moduleData)) {
            if (array_key_exists('learner', $moduleData['filesAttached'])) {
                $filesLearner = $moduleData['filesAttached']['learner'];
                foreach ($filesLearner as $fileLearner) {
                    $educationalDocument = $this->createEducationalDocument($fileLearner['file'], 1);
                    $educationalDocument->setModule($module);
                    $module->addEducationalDocument($educationalDocument);
                }
            }
            if (array_key_exists('trainer', $moduleData['filesAttached'])) {
                $filesTrainer = $moduleData['filesAttached']['trainer'];

                foreach ($filesTrainer as $fileTrainer) {
                    $educationalDocument = $this->createEducationalDocument($fileTrainer['file'], 1);
                    $educationalDocument->setModule($module);
                    $module->addTrainerDocument($educationalDocument);
                }
            }
            if (array_key_exists('eLearning',
                    $moduleData['filesAttached']) && $moduleData['uploadType'] != EducationalDocVariety::URL) {
                $filesMedia = $moduleData['filesAttached']['eLearning'];
                foreach ($filesMedia as $media) {
                    $educationalDocument = $this->createEducationalDocument($media['file']);
                    $educationalDocument->setModule($module);
                    $module->setMediaDocument($educationalDocument);
                    if ($moduleData['uploadType'] == EducationalDocVariety::SCORM) {
                        $educationalDocument->setEducationalDocVariety(
                            $this->getEducationDocVarietyByAppId(EducationalDocVariety::SCORM)
                        );

                    }
                }
            }
            if (array_key_exists('trainerReportTemplate', $moduleData['filesAttached'])) {
                $filesMedia = $moduleData['filesAttached']['trainerReportTemplate'];
                foreach ($filesMedia as $media) {
                    $moduleTemplate = new ModuleTemplate();
                    $fileDescriptor = $this->fileApi->saveTempFile($media['file']);
                    $moduleTemplate->setFileDescriptor($fileDescriptor);
                    $moduleTemplate->setAppId(ModuleTemplate::INDIVIDUAL_REPORT_TEMPLATE);
                    $moduleTemplate->setModule($module);
                    $this->entityManager->getManager()->persist($moduleTemplate);

                }
            }
            if (array_key_exists('attendanceSheetTemplate', $moduleData['filesAttached'])) {
                $filesMedia = $moduleData['filesAttached']['attendanceSheetTemplate'];

                foreach ($filesMedia as $media) {
                    $moduleTemplate = new ModuleTemplate();
                    $fileDescriptor = $this->fileApi->saveTempFile($media['file']);
                    $moduleTemplate->setFileDescriptor($fileDescriptor);
                    $moduleTemplate->setAppId(ModuleTemplate::ATTENDANCE_REPORT_TEMPLATE);
                    $moduleTemplate->setModule($module);
                    $this->entityManager->getManager()->persist($moduleTemplate);
                }
            }
            if (array_key_exists('certificateTemplate', $moduleData['filesAttached'])) {
                $filesMedia = $moduleData['filesAttached']['certificateTemplate'];
                foreach ($filesMedia as $media) {
                    $moduleTemplate = new ModuleTemplate();
                    $fileDescriptor = $this->fileApi->saveTempFile($media['file']);
                    $moduleTemplate->setFileDescriptor($fileDescriptor);
                    $moduleTemplate->setAppId(ModuleTemplate::CERTIFICATE_TEMPLATE);
                    $moduleTemplate->setModule($module);
                    $this->entityManager->getManager()->persist($moduleTemplate);
                }
            }
        }

        return $module;
    }

    private function assignModules(array $modulesToAssign)
    {
        foreach ($modulesToAssign as $moduleToAssign) {
            $reservations = $this->entityManager->getRepository(TrainerReservation::class)
                ->findBy(['module_unique' => $moduleToAssign['moduleUniqueId']]);
            /** @var TrainerReservation $reservation */
            if ($reservations) {
                $sessionUniques = [];
                foreach ($reservations as $reservation) {
                    $assignment = new AssigmentResourceSystem();
                    $assignment->setModule($moduleToAssign['module']);
                    $assignment->setLiveResource($reservation->getProfile());
                    $assignment->setMinBookingDate($reservation->getFromDatetime());
                    $assignment->setMaxBookingDate($reservation->getToDatetime());
                    $assignment->setStatus($this->getStatusByAppId(Status::WITH_BOOKING_SCHEDULED));
                    if ($reservation->getSessionUnique()) {
                        $moduleSession = @$moduleToAssign['sessions'][$reservation->getSessionUnique()];
                        $assignment->setModuleSession($moduleSession);
                        $sessionUniques[] = $reservation->getSessionUnique();
                    }
                    $this->entityManager->getManager()->persist($assignment);
                    $this->entityManager->getManager()->remove($reservation);
                }
                // Add for session which can't exist in the trainer_reservation table
                foreach ($moduleToAssign['sessions'] as $key => $session) {
                    if (!in_array($key, $sessionUniques)) {
                        $start = $session->getSession()->getSessionDate();
                        $end = clone $start;
                        $end->setTime($start->format('H') + $moduleToAssign['module']->getDuration()->format('H'), $start->format('i') + $moduleToAssign['module']->getDuration()->format('i'));
                        $assignment = new AssigmentResourceSystem();
                        $assignment->setModule($moduleToAssign['module']);
                        $assignment->setLiveResource(null);
                        $assignment->setMinBookingDate($start);
                        $assignment->setMaxBookingDate($end);
                        $assignment->setStatus($this->getStatusByAppId(Status::WITH_BOOKING_SCHEDULED));
                        $moduleSession = @$moduleToAssign['sessions'][$key];
                        $assignment->setModuleSession($moduleSession);
                        $this->entityManager->getManager()->persist($assignment);
                    }
                }
            } else {
                if (!$moduleToAssign['sessions']) { // Online course
                    $assignment = new AssigmentResourceSystem();
                    $assignment->setModule($moduleToAssign['module']);
                    $assignment->setLiveResource(null);
                    $assignment->setMinBookingDate($moduleToAssign['module']->getBeginning());
                    $assignment->setMaxBookingDate($moduleToAssign['module']->getEnding());
                    $assignment->setStatus($this->getStatusByAppId(Status::WITH_BOOKING_SCHEDULED));
                    $this->entityManager->getManager()->persist($assignment);
                } else {
                    foreach ($moduleToAssign['sessions'] as $key => $session) {
                        $start = $session->getSession()->getSessionDate();
                        $end = clone $start;
                        $end->setTime($start->format('H') + $moduleToAssign['module']->getDuration()->format('H'), $start->format('i') + $moduleToAssign['module']->getDuration()->format('i'));
                        $assignment = new AssigmentResourceSystem();
                        $assignment->setModule($moduleToAssign['module']);
                        $assignment->setLiveResource(null);
                        $assignment->setMinBookingDate($start);
                        $assignment->setMaxBookingDate($end);
                        $assignment->setStatus($this->getStatusByAppId(Status::WITH_BOOKING_SCHEDULED));
                        $moduleSession = @$moduleToAssign['sessions'][$key];
                        $assignment->setModuleSession($moduleSession);
                        $this->entityManager->getManager()->persist($assignment);
                    }
                }
            }
        }
    }

    private function assignModulesToChapter(array $modulesToAssign)
    {
        foreach ($modulesToAssign as $moduleToAssign) {
            $assignment = new ChapterModule();
            $assignment->setModule($moduleToAssign['module']);
            $assignment->setChapter($moduleToAssign['chapter']);
            $assignment->setPosition($moduleToAssign['position']);
            $this->entityManager->getManager()->persist($assignment);
        }
    }

    private function assignAssessments(array $modulesToAssessment, $assessmentModule, $assessmentData)
    {
        if (isset($assessmentData['session']) && !empty($assessmentData['session'])) {
            foreach ($assessmentData['session'] as $k => $assessment) {
                if (array_key_exists($assessment['assessmentModules'], $modulesToAssessment)) {
                    $moduleAssessment = new ModuleAssessments();
                    $moduleAssessment->setModule($assessmentModule);
                    if(isset($assessment['isCompleted'])) {
                        $moduleAssessment->setCompleted($assessment['isCompleted']);
                    }
                    $moduleAssessment->setAssessment($modulesToAssessment[$assessment['assessmentModules']]);
                    if(isset($assessment['operationOne'])) {
                        $moduleAssessment->setOperationOne($assessment['operationOne']);
                    }
                    if(isset($assessment['scoreToPass'])) {
                        $moduleAssessment->setScoreToPass($assessment['scoreToPass']);
                    }
                    if(isset($assessment['operationTwo'])) {
                        $moduleAssessment->setOperationTwo($assessment['operationTwo']);
                    }
                    if(isset($assessment['timeSpent'])) {
                        $moduleAssessment->setTimeSpent($assessment['timeSpent']);
                    }
                    if(isset($assessment['timeSpentCheck'])) {
                        $moduleAssessment->setTimeSpentCheck($assessment['timeSpentCheck']);
                    }
                    if(isset($assessment['weightNumber'])) {
                        $moduleAssessment->setWeight($assessment['weightNumber']);
                    }
                    if(!empty($assessmentData['certificateTemplate'])){
                        $moduleAssessment->setCertificationTemplate($assessmentData['certificateTemplate']);
                    }
                    $this->entityManager->getManager()->persist($moduleAssessment);
                }
            }
        }
    }

    private function addSkillsToModule($modulesToAssign, $modules)
    {
        if ($modulesToAssign) {
            $db = $this->entityManager->getManager();
            foreach ($modulesToAssign as $assignModule) {
                if ($modules[$assignModule['moduleUniqueId']]) {
                    foreach ($modules[$assignModule['moduleUniqueId']]['skills'] as $skill) {
                        $moduleId = $assignModule['module']->getId();
                        $operator = $modules[$assignModule['moduleUniqueId']]['allSkills'];
                        // Write your raw SQL
                        $query = "INSERT INTO module_skills (module_id, skill_id, operator) values ($moduleId, $skill, '$operator')";
                        // Prepare the query from $db
                        $statementDB = $db->getConnection()->prepare($query);
                        // Execute both queries
                        $statementDB->execute();
                    }
                }
            }
        }
    }

    private function addConstraints($data, $now)
    {
        if ($data['constraints'] && $data['constraints']['days_open_for_session']) {
            $daysOpen = explode(',', $data['constraints']['days_open_for_session']);
            foreach ($daysOpen as $dayOpen) {
                $startMorningTime = $data['constraints']['startMorningTime' . $dayOpen];
                $startMorningTime = explode(":", $startMorningTime);

                $endMorningTime = $data['constraints']['endMorningTime' . $dayOpen];
                $endMorningTime = explode(":", $endMorningTime);

                $startAfternoonTime = $data['constraints']['startAfternoonTime' . $dayOpen];
                $startAfternoonTime = explode(":", $startAfternoonTime);

                $endAfternoonTime = $data['constraints']['endAfternoonTime' . $dayOpen];
                $endAfternoonTime = explode(":", $endAfternoonTime);
                if ($startMorningTime && $endMorningTime && $startAfternoonTime && $endAfternoonTime) {

                    $addDay = new DaySlots();
                    $addDay
                        ->setIntervention($this->intervention)
                        ->setStartMorningTime(clone $now->setTime($startMorningTime[0], $startMorningTime[1]))
                        ->setEndMorningTime(clone $now->setTime($endMorningTime[0], $endMorningTime[1]))
                        ->setStartAfternoonTime(clone $now->setTime($startAfternoonTime[0], $startAfternoonTime[1]))
                        ->setEndAfternoonTime(clone $now->setTime($endAfternoonTime[0], $endAfternoonTime[1]))
                        ->setDayId($dayOpen);
                    $this->entityManager->getManager()->persist($addDay);
                    $this->entityManager->getManager()->flush();
                }
            }
        }

        if ($data['constraints'] && $data['constraints']['weeks_exclude']) {
            $weeksExclude = explode(',', $data['constraints']['weeks_exclude']);
            foreach ($weeksExclude as $exclude) {
                $excludeArr = explode('|', $exclude);
                if ($excludeArr) {
                    $addExcludeWeek = new ExcludeWeeks();
                    $addExcludeWeek
                        ->setIntervention($this->intervention)
                        ->setStart(new DateTime($excludeArr[0]))
                        ->setEnd(new DateTime($excludeArr[1]));
                    $this->entityManager->getManager()->persist($addExcludeWeek);
                    $this->entityManager->getManager()->flush();
                }
            }
        }
    }
}
