<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24/08/18
 * Time: 09:39.
 */

namespace AdminBundle\Services;

class AccessService
{
    private $token = 'trUGgtErGDev4I9KH3sRmjEocLpZ';
    private $authentification = '5fbe8debfd4ea533f82558b63db11eb3';

    public function getAccessToken()
    {
        $curl = curl_init();
        $url = 'https://api.getgo.com/oauth/v2/token';

        // OPTIONS:
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $data = [
            'grant_type' => 'authorization_code',
            'code' => $this->authentification,
        ];

        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
            throw new \Exception('Connection failure');
        }
        curl_close($curl);

        return $result;
    }

    public function getAccountInfo()
    {
        $url = 'https://api.getgo.com/admin/rest/v1/me';
        $data = CurlApi::callAPI('GET', 'https://api.getgo.com/admin/rest/v1/me', '', $this->token);

        return $data;
    }
}
