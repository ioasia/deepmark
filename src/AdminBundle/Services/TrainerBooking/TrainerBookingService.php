<?php

namespace AdminBundle\Services\TrainerBooking;

use AdminBundle\Services\TrainerBookingInterface;
use AdminBundle\Utils\TrainerApiFormat\TrainerProfileToArray;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\TrainerReservation;
use ApiBundle\Repository\AssigmentResourceSystemRepository;
use ApiBundle\Repository\AvailableCalendarRepository;
use ApiBundle\Repository\BookingAgendaRepository;
use ApiBundle\Repository\ModuleRepository;
use ApiBundle\Repository\ModuleTrainerResponseRepository;
use ApiBundle\Repository\ProfileRepository;
use ApiBundle\Repository\SkillRepository;
use ApiBundle\Repository\StatusRepository;
use ApiBundle\Repository\TrainerReservationRepository;
use AppBundle\Utils\UserTimeZone;
use Doctrine\ORM\EntityManager;

class TrainerBookingService implements TrainerBookingInterface
{
    const SESSION_TYPE = 'online_session';
    const WORKSHOP_TYPE = 'workshop';
    const OTHER_TYPES = 'other';

    /** @var TimeExtract */
    private $timeExtract;

    /** @var SkillRepository */
    private $skillRepository;

    /**
     * @var ModuleTrainerResponseRepository
     */
    private $moduleTrainerResponseRepository;

    /** @var ProfileRepository */
    private $profileRepository;

    /** @var TrainerReservationRepository */
    private $reservationRepository;

    /** @var AssigmentResourceSystemRepository */
    private $assignmentRepository;

    /** @var StatusRepository */
    private $statusRepository;

    /** @var AvailableCalendarRepository */
    private $availableCalendarRepository;

    /** @var BookingAgendaRepository */
    private $bookingAgendaRepository;

    /** @var ModuleRepository */
    private $moduleRepository;

    private $entityManager;

    public function __construct(
        TimeExtract $timeExtract,
        SkillRepository $skillRepository,
        ModuleTrainerResponseRepository $moduleTrainerResponseRepository,
        ProfileRepository $profileRepository,
        TrainerReservationRepository $reservationRepository,
        AssigmentResourceSystemRepository $assignmentRepository,
        StatusRepository $statusRepository,
        AvailableCalendarRepository $availableCalendarRepository,
        BookingAgendaRepository $bookingAgendaRepository,
        ModuleRepository $moduleRepository,
        EntityManager $entityManager
    ) {
        $this->timeExtract = $timeExtract;
        $this->skillRepository = $skillRepository;
        $this->moduleTrainerResponseRepository = $moduleTrainerResponseRepository;
        $this->profileRepository = $profileRepository;
        $this->reservationRepository = $reservationRepository;
        $this->assignmentRepository = $assignmentRepository;
        $this->statusRepository = $statusRepository;
        $this->availableCalendarRepository = $availableCalendarRepository;
        $this->bookingAgendaRepository = $bookingAgendaRepository;
        $this->moduleRepository = $moduleRepository;
        $this->entityManager = $entityManager;
    }

    protected function isWorkShopTypeModule($appId): bool
    {
        return in_array(
            $appId,
            [StoredModule::VIRTUAL_CLASS, StoredModule::ONLINE_WORKSHOP, StoredModule::PRESENTATION_ANIMATION]
        );
    }

    protected function isOnlineSessionModule($appId): bool
    {
        return StoredModule::ONLINE == $appId;
    }

    public function automaticAssignTrainers(InterventionStep $steps): array
    {
        $return = [];

        $step2Data = $steps->getStep2();
        $modulesData = $step2Data['module'] ?? [];
        foreach ($modulesData as $unique => $moduleData) {
            $appId = $moduleData['appId'];
            if (!$this->isOnlineSessionModule($appId) && !$this->isWorkShopTypeModule($appId)) {
                unset($modulesData[$unique]);
            }
        }

        while (false == empty($modulesData)) {
            $unique = key($modulesData);
            $return[$unique] = $this->assignPerModule($unique, $modulesData);
            unset($modulesData[$unique]);
        }

        return $return;
    }

    public function publish(array $modules): void
    {
        $this->reservationRepository->clearOld();
        $modules = $stepData['module'] ?? [];
        foreach ($modules as $moduleCode => $module) {
            $this->moveReservationPerModule($moduleCode, $module);
        }
    }

    public function assignTrainersForOnline($moduleId, $trainersIds, \DateTime $startDate, \DateTime $endDate)
    {
        $return = [];
        foreach ($trainersIds as $trainerId) {
            $trainer = $this->profileRepository->find($trainerId);
            // dont allow make reservation the module with the same trainer
            $existedReservation = $this->reservationRepository->findOneBy(['profile' => $trainer, 'module_unique' => $moduleId]);
            if (!$existedReservation) {
                $this->reservationRepository->makeReservationOnline($trainer, $startDate, $endDate, $moduleId);
                array_push($return, $trainer);
            }
        }

        return $return;
    }

    public function getTrainersAvailableDatesWithModule($trainerIds, \DateTime $startDate, \DateTime $endDate)
    {
        $datesMatched = $this->availableCalendarRepository->findAvailabilitiesOfTrainers($trainerIds, $startDate, $endDate);
        // set time to get full days of DatePeridod
        $startDate->setTime(0, 0, 0);
        $endDate->setTime(0, 0, 1);
        $period = new \DatePeriod(
            $startDate,
            new \DateInterval('P1D'),
            $endDate
        );
        $return = [];
        if($datesMatched) {
            foreach ($period as $key => $value) {
                $isFilled = false;
                foreach ($datesMatched as $matched){
                    if($value->format('Y-m-d') == $matched->getBeginning()->format('Y-m-d')){
                        $isFilled = true;
                        break;
                    }
                }
                if(!$isFilled){
                    $return[] = $value->format('d/m/Y');
                }
            }
        }

        return $return;
    }

    public function assignTrainerForSession($moduleId, $sessionId, $trainerId, \DateTime $startDate, \DateTime $endDate)
    {
        $trainer = $this->profileRepository->find($trainerId);
        $trainerReservation = $this->reservationRepository->findOneBy(array('module_unique' => $moduleId, 'session_unique' => $sessionId));
        if(!empty($trainerReservation)){
            $this->reservationRepository->updateTrainer($trainerReservation, $trainer);
        } else {
            $this->reservationRepository->makeReservationSession($trainer, $startDate, $endDate, $moduleId, $sessionId);
        }

        return array(
            'id' => $trainer->getId(),
            'first_name' => $trainer->getFirstName(),
            'last_name' => $trainer->getLastName(),
        );
    }

    public function getAvailableTrainers(InterventionStep $steps): array
    {
        $step2Data = $steps->getStep2();
        $modulesData = $step2Data['module'] ?? [];

        return $this->getAvailableTrainersForModules($modulesData);
    }

    public function getTrainerHours(Profile $trainer, \DateTime $startDate, \DateTime $endDate, $booked = false)
    {
        return $this->getTrainerWithHour($trainer, $startDate, $endDate, $booked);
    }

    private function getTrainerWithHour(Profile $trainer, \DateTime $startDate, \DateTime $endDate, $booked = false)
    {
        // find the availabilities and make sure the beginning >= start date AND ending <= end date in the available_calendar table
        $availabilities = $this->availableCalendarRepository->findAvailabilityByLiveResource($trainer, $startDate, $endDate);
        // calculation the lunch time of the trainer
        /*$diffLunch = $trainer->getLunchStartTime()->diff($trainer->getLunchEndTime());
        $hoursLunch = $diffLunch->h;
        $minutesLunch = $diffLunch->i;
        if ($minutesLunch > 0) {
            $hoursLunch += $hoursLunch/60;
        }*/

        $hoursCanDo = 0;
        $minutesCanDo = 0;
        $hoursCanMinus = 0;
        $minutesCanMinus = 0;
        $dateMin = null;
        $dateMax = null;

        foreach ($availabilities as $availability) {
            if (!$dateMin) {
                $dateMin = $availability->getBeginning();
            }
            if (!$dateMax) {
                $dateMax = $availability->getEnding();
            }

            if ($dateMin > $availability->getBeginning()) {
                $dateMin = $availability->getBeginning();
            }

            if ($dateMax < $availability->getEnding()) {
                $dateMax = $availability->getEnding();
            }
            // find the difference between ending and beginning of each availability to calculate the hour can do
            $diff = $availability->getEnding()->diff($availability->getBeginning());
            $hoursCanDo += $diff->h;
            $minutesCanDo += $diff->i;
        }
        $rankingGrades =  $this->moduleTrainerResponseRepository->findBy(array('trainer' => $trainer));
        $ranking = 0;
        if ($rankingGrades) {
            $grade = 0;
            foreach ($rankingGrades as $key => $item) {
                $grade += $item->getNotation();
            }
            $ranking = round($grade / ($key + 1), 2);
        }

        if ($minutesCanDo > 0) {
            $hoursCanDo += $minutesCanDo/60;
        }
        // minus the lunch time of the trainer
        /*if ($availabilities) {
            $totalHoursLunch = $hoursLunch * count($availabilities);
            $hoursCanDo = $hoursCanDo - $totalHoursLunch;
        }*/

        // find the trainers who have booking to minus it in the hours can do
        $bookings = $this->bookingAgendaRepository->isTrainerBooked($trainer, $startDate, $endDate);
        if ($bookings) {
            foreach ($bookings as $booking) {
                $hoursCanMinus += $booking->getModule()->getDuration()->format('G');
                $minutesCanMinus += $booking->getModule()->getDuration()->format('i');
            }

            if ($hoursCanMinus || $minutesCanMinus) {
                $hoursCanMinus += ($minutesCanMinus != 0) ? $minutesCanMinus/60 : 0;
                $hoursCanDo = $hoursCanDo - $hoursCanMinus;
            }
        }

        return array(
            'trainer' => array(
                'id' => $trainer->getId(),
                'first_name' => $trainer->getFirstName(),
                'last_name' => $trainer->getLastName(),
                'city' => $trainer->getCity(),
            ),
            'hours' => $hoursCanDo,
            'ranking' => $ranking,
            'dateStart' => $dateMin,
            'dateEnd' => $dateMax,
            'booked' => $booked,
        );
    }

    /**
     * @param $moduleId
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getOperatorModuleSkill($moduleId)
    {
        // get skills operator
        $db = $this->entityManager;
        // Write your raw SQL
        $query = "SELECT `operator` FROM module_skills ms where ms.module_id = :module_id LIMIT 1;";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        $statementDB->bindValue('module_id', $moduleId);
        // Execute both queries
        $statementDB->execute();
        // Get results
        $resultDB             = $statementDB->fetchAll();
        $skillsSearchOperator = reset($resultDB);

        return $skillsSearchOperator['operator'];
    }

    /**
     * @param $module
     * @param $sessionIds
     * @param $trainerId
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function validationTrainerWithSessions($module, $sessionIds, $trainerId)
    {
        $sessionPeriods = $this->timeExtract->extractDatesModule($module);
        $skills = [];
        $operatorSkill = null;
        if ($module->getSkills()) {
            foreach ($module->getSkills() as $key => $skill) {
                $skills[$key] = $skill->getDesignation();
            }
            $operatorSkill = $this->getOperatorModuleSkill($module->getId());
        }
        $requiredSkills = $this->skillRepository->findBy(['designation' => $skills]);
        $skillsSearchOperator = count($skills) > 1 ? $operatorSkill : null;

        $return = [];
        foreach ($sessionPeriods as $sessionUnique => $period) {
            $sessionStartDate = clone $period['start'];
            if(in_array($sessionUnique, $sessionIds)){
                $trainers = $this->getTrainersForTimePeriodAndSkillsSession($requiredSkills, $period, $skillsSearchOperator);
                $found = false;
                foreach ($trainers as $trainer){
                    if($trainer->getId() == $trainerId){
                        $found = true;
                    }
                }
                if(!$found){
                    $return[] = $sessionStartDate->format('Y-m-d G:i');
                }
            }
        }

        return $return;
    }

    /**
     * @param $module
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAvailableTrainersForWorkShopsModule($module)
    {
        $appId = $module->getStoredModule()->getAppId();
        $result = [];
        if ($this->isWorkShopTypeModule($appId)) {
            $sessionPeriods = $this->timeExtract->extractDatesModule($module);
            $skills = [];
            $operatorSkill = null;
            if ($module->getSkills()) {
                foreach ($module->getSkills() as $key => $skill) {
                    $skills[$key] = $skill->getDesignation();
                }
                $operatorSkill = $this->getOperatorModuleSkill($module->getId());
            }
            $requiredSkills = $this->skillRepository->findBy(['designation' => $skills]);
            $skillsSearchOperator = count($skills) > 1 ? $operatorSkill : null;

            foreach ($sessionPeriods as $sessionUnique => $period) {
                $trainers = $this->getTrainersForTimePeriodAndSkillsSession($requiredSkills, $period, $skillsSearchOperator);
                foreach ($trainers as $trainer){
                    $dataTrainer = [];
                    $trainerId = $trainer->getId();
                    if(isset($result[$trainerId])){
                        $dataTrainer = $result[$trainerId];
                    }
                    $ranking =  $this->moduleTrainerResponseRepository->getRankingByTrainer($trainer);
                    $dataTrainer['ranking'] = $ranking;
                    $dataTrainer['trainer'] = $trainer;
                    $dataTrainer['sessions'][] = $sessionUnique;
                    $result[$trainerId] = $dataTrainer;
                }
            }
            usort($result, function($a, $b) {
                return $b['ranking'] <=> $a['ranking'];
            });
        }

        return $result;
    }

    public function getAvailableTrainerForModule($moduleId, $moduleData): array
    {
        $appId = $moduleData['appId'];
        if ($this->isWorkShopTypeModule($appId)) {
            $trainers = $this->getAvailableTrainersForWorkShops($moduleData);

            return array(
                'appId' => $appId,
                'results' => $trainers,
            );
        } elseif ($this->isOnlineSessionModule($appId)) {
            $participant = isset($moduleData['participant']) ? $moduleData['participant'] : 1;
            $moduleQuantity = $moduleData['quantity'];
            $moduleDuration = $moduleData['durationHours'] + ($moduleData['durationMinutes'] / 60);
            $startDate = \DateTime::createFromFormat('d/m/Y', $moduleData['startDate'], UserTimeZone::getTimeZoneObject());
            $endDate = \DateTime::createFromFormat('d/m/Y', $moduleData['endDate'], UserTimeZone::getTimeZoneObject());
            // correct date to check correct in isTrainerAvailable function
            //$startDate->setTime(0, 1);
            //$endDate->setTime(23, 59);

            /*$interval = $startDate->diff($endDate);
            $intervalDay = $interval->days;
            $hourQuotaNeeded = floor($moduleDuration * 3) * $intervalDay;*/

            $hourQuotaNeeded = $moduleDuration * 3; // * ($participant ? $participant : 1)
            // get trainers which adapt with skills and reservation in the trainer_reservation table with start and end date
            $trainers = $this->getAvailableTrainersForOnlineSession($moduleData);

            $trainersWithHour = [];
            foreach ($trainers as $trainer) {
                $trainerHour = $this->getTrainerWithHour($trainer, $startDate, $endDate);
                //if ($trainerHour['hours'] >= $hourQuotaNeeded) {
                    $trainersWithHour[$trainer->getId()] = $trainerHour;
                //}
            }

            $reservations = $this->reservationRepository->findBy(array(
                'module_unique' => $moduleId,
            ));
            foreach ($reservations as $reservation) {
                $trainerHourBooked = $this->getTrainerWithHour($reservation->getProfile(), $startDate, $endDate, true);
                $trainersWithHour[$reservation->getProfile()->getId()] = $trainerHourBooked;
            }

            usort($trainersWithHour, function($a, $b) {
                return $a['ranking'] <=> $b['ranking'];
            });
            return array(
                'appId' => $appId,
                'totalHours' => $moduleQuantity * $moduleDuration,
                'hourQuotaNeeded' => $hourQuotaNeeded,
                'results' => $trainersWithHour,
            );
        }
    }

    public function getAvailableTrainerForModuleAndProfile($moduleId, $profileId): array
    {
        $module = $this->moduleRepository->find($moduleId);
        $appId = $module->getStoredModule()->getAppId();
        if ($this->isWorkShopTypeModule($appId)) {
            $trainers = $this->getAvailableTrainersForWorkShopsByProfileAndModule($module, $profileId);

            return array(
                'appId' => $appId,
                'results' => $trainers,
            );
        } elseif ($this->isOnlineSessionModule($appId)) {
            $moduleQuantity = $module->getQuantity();
            $moduleDuration = $module->getDuration()->format('G') + ($module->getDuration()->format('i') / 60);

            $startDate = $module->getBeginning();
            $endDate = $module->getEnding();
            $startDate->setTime(0, 1);
            $endDate->setTime(23, 59);
            $trainers = $this->getAvailableTrainersForOnlineSessionByModuleAndProfile($module);
            $trainersWithHour = [];
            foreach ($trainers as $trainer) {
                if ($trainer->getId() != $profileId) {
                    $trainerHour = $this->getTrainerWithHour($trainer, $startDate, $endDate);
                    array_push($trainersWithHour, $trainerHour);
                }
            }

            return array(
                'appId' => $appId,
                'totalHours' => $moduleQuantity * $moduleDuration,
                'results' => $trainersWithHour,
            );
        }
    }

    protected function getAvailableTrainersForModules(array $modulesData)
    {
        $return = null;
        foreach ($modulesData as $unique => $moduleData) {
            $appId = $moduleData['appId'];
            if ($this->isWorkShopTypeModule($appId)) {
                $return[$unique] = array(
                    'appId' => $appId,
                    'results' => $this->getAvailableTrainersForWorkShops($moduleData),
                );
            } elseif ($this->isOnlineSessionModule($appId)) {
                $moduleQuantity = $moduleData['quantity'];
                $moduleDuration = $moduleData['durationHours'];

                $trainers = $this->getAvailableTrainersForOnlineSession($moduleData);
                $trainersWithHour = [];
                foreach ($trainers as $trainer) {
                    $availabilities = $trainer->getAvailableCalendar();
                    $hoursCanDo = 0;
                    $minutesCanDo = 0;
                    $dateMin = null;
                    $dateMax = null;

                    foreach ($availabilities as $availability) {
                        if (!$dateMin) {
                            $dateMin = $availability->getBeginning();
                        }
                        if (!$dateMax) {
                            $dateMax = $availability->getEnding();
                        }

                        if ($dateMin > $availability->getBeginning()) {
                            $dateMin = $availability->getBeginning();
                        }

                        if ($dateMax < $availability->getEnding()) {
                            $dateMax = $availability->getEnding();
                        }

                        $diff = $availability->getEnding()->diff($availability->getBeginning());
                        $hoursCanDo += $diff->h;
                        $minutesCanDo += $diff->i;
                    }
                    array_push($trainersWithHour, array(
                        'trainer' => $trainer,
                        'hours' => $hoursCanDo,
                        'dateStart' => $dateMin,
                        'dateEnd' => $dateMax,
                    ));
                }

                $return[$unique] = array(
                    'appId' => $appId,
                    'totalHours' => $moduleQuantity * $moduleDuration,
                    'results' => $trainersWithHour,
                );
            }
        }

        return $return;
    }

    protected function getTrainersForTimePeriodAndSkillsOnline(array $requiredSkills, array $period, $skills_join_operator = null)
    {
        // find the trainers which adapt with skills
        $trainers = $this->profileRepository->getAvailableTrainersBySkillsOnline(
            $requiredSkills, $period['start'], $period['end'], $skills_join_operator
        );
        $return = [];
        foreach ($trainers as $trainer) {
            // filter the trainers which adapt with trainer reservation
            if ($this->reservationRepository->isTrainerAvailable($trainer, $period['start'], $period['end'])) {
                $return[] = $trainer;
            }
        }

        return $return;
    }

    protected function getTrainersForTimePeriodAndSkillsSession(array $requiredSkills, array $period, $skills_join_operator = null)
    {
        $trainers = $this->profileRepository->getAvailableTrainersBySkillsSession(
            $requiredSkills, $period['start'], $period['end'], $skills_join_operator
        );

        $return = [];
        foreach ($trainers as $trainer) {
            // filter the trainers which adapt with trainer reservation, AssigmentResourceSystem table and the trainer don't have booking in this period
//            dump($this->reservationRepository->isTrainerAvailableSession($trainer, $period['start'], $period['end']));
//            dump($this->bookingAgendaRepository->isTrainerBooked($trainer, $period['start'], $period['end']));
//            dump($this->assignmentRepository->isTrainerAvailableSession($trainer, $period['start'], $period['end']));
//            dump($trainer->getId());
            if ($this->reservationRepository->isTrainerAvailableSession($trainer, $period['start'], $period['end']) &&
                !$this->bookingAgendaRepository->isTrainerBooked($trainer, $period['start'], $period['end']) &&
                $this->assignmentRepository->isTrainerAvailableSession($trainer, $period['start'], $period['end'])) {
                $return[] = $trainer;
            }
        }
        return $return;
    }

    public function requestReplacementTrainerSession($booking, $session)
    {
        $requestReplacement = $this->entityManager->getRepository(RequestReplacementResource::class)->findOneBy(['session' => $session]);

        if (!$requestReplacement) {
            $requestAssign = new RequestReplacementResource();
            $requestAssign->setSession($session);
            $requestAssign->setBookingAgenda($booking);
            $requestAssign->setDateAdd(new \DateTime());
            $requestAssign->setStatus(RequestReplacementResource::STATUS_UNKNOWN);
            $this->entityManager->persist($requestAssign);
            $this->entityManager->flush();
        } else {
            $requestReplacement->setStatus(RequestReplacementResource::STATUS_UNKNOWN);
            $requestReplacement->setUpdated(new \DateTime());
            $this->entityManager->persist($requestReplacement);
            $this->entityManager->flush();
        }
    }

    protected function getTrainersForTimePeriodAndSkillsSessionWithProfile(array $requiredSkills, array $period, $skills_join_operator = null, $profileId)
    {
        $trainers = $this->profileRepository->getAvailableTrainersBySkillsSession(
            $requiredSkills, $period['start'], $period['end'], $skills_join_operator
        );

        $return = [];
        foreach ($trainers as $trainer) {
            if (!$this->bookingAgendaRepository->isTrainerBooked($trainer, $period['start'], $period['end']) && $profileId != $trainer->getId()) {
                $return[] = $trainer;
            }
        }

        return $return;
    }

    protected function getAvailableTrainersForWorkShops($moduleData)
    {
        $startDate = \DateTime::createFromFormat('d/m/Y', $moduleData['startDate'], UserTimeZone::getTimeZoneObject());
        $endDate = \DateTime::createFromFormat('d/m/Y', $moduleData['endDate'], UserTimeZone::getTimeZoneObject());
        $startDate->setTime(0, 1);
        $endDate->setTime(23, 59);

        $moduleDuration = $moduleData['durationHours'] + ($moduleData['durationMinutes'] / 60);
        $hourQuotaNeeded = $moduleDuration * 3;

        $sessionsDay = [];
        $sessionPeriods = $this->timeExtract->extractDatesForWorkshopModule($moduleData);

        $requiredSkills = $this->skillRepository->findBy(['designation' => $moduleData['skills']]);
        $skillsSearchOperator = isset($moduleData['allSkills']) || count($moduleData['skills']) > 1
            ? $moduleData['allSkills'] :
            (isset($moduleData['allSkills']) ? $moduleData['allSkills'] : null);
        foreach ($sessionPeriods as $sessionUnique => $period) {
            $day = $period['start']->format('Y-m-d');
            $chosenTrainer = null;
            $reservation = $this->reservationRepository->findOneBy(array(
                'session_unique' => $sessionUnique,
            ));
            $trainersFormatter = new TrainerProfileToArray();
            if ($reservation) {
                /** @var Profile $profile */
                $profile = $reservation->getProfile();
                $chosenTrainer = $profile->__toArray();
                $chosenTrainer['ranking'] = $this->moduleTrainerResponseRepository->getRankingByTrainer($profile);
            }

            if (!array_key_exists($day, $sessionsDay)) {
                $sessionsDay[$day] = array(
                    'date' => $day,
                    'sessions' => [],
                );

                $trainers = $this->getTrainersForTimePeriodAndSkillsSession($requiredSkills, $period, $skillsSearchOperator);
                $trainers = $trainersFormatter($trainers);
                $returnTrainer = [];
                foreach ($trainers as $trainer){
                    $ranking =  $this->moduleTrainerResponseRepository->getRankingByTrainer($trainer);
                    $trainer['ranking'] = $ranking;
                    $trainer['avatar'] = $trainer['avatar'] == null ? '' : $trainer['avatar'];
                    array_push($returnTrainer, $trainer);
                }
                usort($returnTrainer, function($a, $b) {
                    return $b['ranking'] <=> $a['ranking'];
                });

                // find the address of the session
                $city = null;
                foreach ($moduleData['session'] as $address) {
                    if ($address['unique-session-id'] == $sessionUnique && isset($address['town']) && !empty($address['country'])) {
                        $city = $address['town'] . ", " . $address['country'];
                    }
                }

                // find the address of the session
                array_push($sessionsDay[$day]['sessions'], array(
                    'userTimeZone' => UserTimeZone::getTimeZone(),
                    'trainers' => $returnTrainer,
                    'title' => '',
                    'start' => $period['start'],
                    'end' => $period['end'],
                    'sessionId' => $sessionUnique,
                    'address' => $city,
                    'chosen_trainer' => isset($chosenTrainer) ? $chosenTrainer : null,
                ));
            } else {
                $trainers = $this->getTrainersForTimePeriodAndSkillsSession($requiredSkills, $period, $skillsSearchOperator);
                $trainers = $trainersFormatter($trainers);
                $returnTrainer = [];
                foreach ($trainers as $trainer){
                    $ranking =  $this->moduleTrainerResponseRepository->getRankingByTrainer($trainer);
                    $trainer['ranking'] = $ranking;
                    $trainer['avatar'] = $trainer['avatar'] == null ? '' : $trainer['avatar'];
                    array_push($returnTrainer, $trainer);
                }
                usort($returnTrainer, function($a, $b) {
                    return $b['ranking'] <=> $a['ranking'];
                });

                // find the address of the session
                $city = null;
                foreach ($moduleData['session'] as $address) {
                    if ($address['unique-session-id'] == $sessionUnique && isset($address['town']) && !empty($address['country'])) {
                        $city = $address['town'] . ", " . $address['country'];
                    }
                }
                // find the address of the session
                array_push($sessionsDay[$day]['sessions'], array(
                    'userTimeZone' => UserTimeZone::getTimeZone(),
                    'trainers' => $returnTrainer,
                    'title' => '',
                    'start' => $period['start'],
                    'end' => $period['end'],
                    'sessionId' => $sessionUnique,
                    'address' => $city,
                    'chosen_trainer' => isset($chosenTrainer) ? $chosenTrainer : null,
                ));
            }
        }

        // filter the trainer with total needed hours
        $data = [];
        foreach ($sessionsDay as $day) {
            foreach ($day['sessions'] as $k => $session) {
                foreach ($session['trainers'] as $t => $trainer) {
                    $trainer = $this->profileRepository->find($trainer['id']);
                    $trainerHour = $this->getTrainerWithHour($trainer, $startDate, $endDate);
                    if ($trainerHour['hours'] < $hourQuotaNeeded) {
                        unset($day['sessions'][$k]['trainers'][$t]);
                    }
                }
            }
            array_push($data, $day);
        }

        return $data;
    }

    protected function getSkillsSearchOperator($moduleId, $skillId)
    {
        // get skills operator
        $db = $this->entityManager;
        // Write your raw SQL
        $query = "SELECT `operator` FROM module_skills ms where ms.module_id = :module_id AND ms.skill_id = :skill_id LIMIT 1;";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        $statementDB->bindValue('module_id', $moduleId);
        $statementDB->bindValue('skill_id', $skillId);
        // Execute both queries
        $statementDB->execute();
        // Get results
        $resultDB = $statementDB->fetchAll();
        $skillsSearchOperator = reset($resultDB);
        return $skillsSearchOperator['operator'];
    }

    protected function getAvailableTrainersForWorkShopsByProfileAndModule($module, $profileId)
    {
        $skillId = $skillsSearchOperator = null;
        $requiredSkills = $sessionsDay = [];
        $sessionPeriods = $this->timeExtract->extractDatesWithWorkshopModule($module);

        if ($module->getSkills()) {
            foreach ($module->getSkills() as $skill) {
                $requiredSkills[] = $skill->getId();
                $skillId = $skill->getId();
            }

            // get skills operator
            $skillsSearchOperator = $this->getSkillsSearchOperator($module->getId(), $skillId);
        }

        foreach ($sessionPeriods as $period) {
            $day = $period['start']->format('Y-m-d');
            $chosenTrainer = null;
            $trainersFormatter = new TrainerProfileToArray();

            if (!array_key_exists($day, $sessionsDay)) {
                $sessionsDay[$day] = array(
                    'date' => $day,
                    'sessions' => [],
                );

                $trainers = $this->getTrainersForTimePeriodAndSkillsSessionWithProfile($requiredSkills, $period, $skillsSearchOperator, $profileId);
                $trainers = $trainersFormatter($trainers);

                array_push($sessionsDay[$day]['sessions'], array(
                    'trainers' => $trainers,
                    'title' => '',
                    'start' => $period['start'],
                    'end' => $period['end'],
                    'chosen_trainer' => $chosenTrainer,
                ));
            } else {
                $trainers = $this->getTrainersForTimePeriodAndSkillsSessionWithProfile($requiredSkills, $period, $skillsSearchOperator, $profileId);
                $trainers = $trainersFormatter($trainers);

                array_push($sessionsDay[$day]['sessions'], array(
                    'trainers' => $trainers,
                    'title' => '',
                    'start' => $period['start'],
                    'end' => $period['end'],
                    'chosen_trainer' => $chosenTrainer,
                ));
            }
        }

        $data = [];
        foreach ($sessionsDay as $day) {
            array_push($data, $day);
        }

        return $data;
    }

    protected function getAvailableTrainersForOnlineSession($moduleData)
    {
        $requirements = $this->timeExtract->extractDatesForOnlineSession($moduleData);
        $requiredSkills = $this->skillRepository->findBy(['id' => $moduleData['skills']]);
        $skillsSearchOperator = $moduleData['allSkills'];

        return $this->getTrainersForTimePeriodAndSkillsOnline($requiredSkills, $requirements, $skillsSearchOperator);
    }

    protected function getAvailableTrainersForOnlineSessionByModuleAndProfile($module)
    {
        $skills = [];
        $skillsSearchOperator = $skillId = null;
        $startDate = $module->getBeginning();
        $endDate = $module->getEnding();
        $startDate->setTime(0, 0);
        $endDate->setTime(23, 59);

        $requiredSkills = $module->getSkills();
        if ($requiredSkills) {
            foreach ($requiredSkills as $skill) {
                $skills[] = $skill->getId();
                $skillId = $skill->getId();
            }

            // get skills operator
            $skillsSearchOperator = $this->getSkillsSearchOperator($module->getId(), $skillId);
        }
        return $this->profileRepository->getAvailableTrainersBySkillsOnline(
            $skills, $startDate, $endDate, $skillsSearchOperator
        );
    }

    protected function moveReservationPerModule($moduleCode, Module $module)
    {
        $reservations = $this->reservationRepository->findBy(['module_unique' => $moduleCode]);
        /** @var TrainerReservation $reservation */
        foreach ($reservations as $reservation) {
            $assignment = new AssigmentResourceSystem();
            $assignment->setModule($module);
            $assignment->setLiveResource($reservation->getProfile());
            $assignment->setMinBookingDate($reservation->getFromDatetime());
            $assignment->setMaxBookingDate($reservation->getToDatetime());
            $assignment->setStatus($this->statusRepository->findOneBy(
                ['appId' => Status::WITH_BOOKING_SCHEDULED]
            ));

            $this->assignmentRepository->save($assignment);
            $this->reservationRepository->delete($reservation);
        }
    }

    protected function assignPerModule($unique, $modulesData)
    {
        $appId = $modulesData[$unique]['appId'];
        if ($this->isWorkShopTypeModule($appId)) {
            return $this->assignAvailableTrainersForWorkShops($unique, $modulesData);
        } elseif ($this->isOnlineSessionModule($appId)) {
            return $this->assignAvailableTrainersForOnlineSession($unique, $modulesData);
        }
    }

    protected function assignAvailableTrainersForWorkShops($unique, $modulesData)
    {
        $sessionPeriods = $this->timeExtract->extractDatesForWorkshopModule($modulesData[$unique]);
        $return = [];
        $availableTrainers = $this->getAvailableTrainersForModules($modulesData);

        foreach ($sessionPeriods as $sessionUnique => $period) {
            $list = $availableTrainers[self::WORKSHOP_TYPE][$unique][$sessionUnique] ?? [];
            if (empty(current($list))) {
                $return[$sessionUnique] = null;
                continue;
            }

            $return[$sessionUnique] = current($list);
            unset($modulesData[$unique]['sessions'][$sessionUnique]);
            $availableTrainers = $this->getAvailableTrainersForModules($modulesData);
        }

        return $return;
    }

    protected function assignAvailableTrainersForOnlineSession($unique, $modulesData)
    {
        $availableTrainers = $this->getAvailableTrainersForModules($modulesData);
        $list = $availableTrainers[self::SESSION_TYPE][$unique] ?? [];

        if (empty(current($list))) {
            return null;
        }

        return current($list);
    }

    /**
     * check send mail booking to trainer when assigned learner to session
     * send only one time
     * @param $trainer
     * @param $module
     * @param $moduleSession
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function hasSentMailBookingConfirm($module, $moduleSession)
    {
        $bookingsAgenda     = $this->entityManager->getRepository(BookingAgenda::class)->findBy([
            'moduleSession' => $moduleSession,
            'module'        => $module,
        ]);
        $hasSent = 0;
        foreach ($bookingsAgenda as $bookingAgenda){
            if($bookingAgenda->getReminderSent() === 1){
                $hasSent = 1;
                continue;
            }
            $bookingAgenda->setReminderSent(1);
            $this->entityManager->persist($bookingAgenda);
        }
        $this->entityManager->flush();

        return $hasSent;
    }
}
