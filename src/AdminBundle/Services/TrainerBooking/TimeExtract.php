<?php

namespace AdminBundle\Services\TrainerBooking;

use AppBundle\Utils\UserTimeZone;

class TimeExtract
{
    public function extractDatesForWorkshopModule(array $moduleData)
    {
        $periods = [];
        $sessions = $moduleData['session'];

        foreach ($sessions as $session) {
            $start = \DateTime::createFromFormat('d/m/Y', $session['startDate'], UserTimeZone::getTimeZoneObject());
            $startTime = new \DateTime($session['startTime']);
            $start->setTime($startTime->format('H'), $startTime->format('i'));

            $end = clone $start;

            $end->setTime($startTime->format('H') + (int) $moduleData['durationHours'],
                $startTime->format('i') + (int) $moduleData['durationMinutes']);

            $unique = $session['unique-session-id'];

            $periods[$unique] = ['start' => $start, 'end' => $end];
        }

        return $periods;
    }

    public function extractDatesForOnlineSession(array $moduleData)
    {
        $startDate = \DateTime::createFromFormat('d/m/Y', $moduleData['startDate'], UserTimeZone::getTimeZoneObject());
        $endDate = \DateTime::createFromFormat('d/m/Y', $moduleData['endDate'], UserTimeZone::getTimeZoneObject());
        // correct date to check correct in isTrainerAvailable function
        //$startDate->setTime(0, 0);
        //$endDate->setTime(23, 59);

        return [
            'start' => $startDate,
            'end' => $endDate,
        ];
    }

    public function extractDatesWithWorkshopModule($module)
    {
        $periods = [];
        $sessions = $module->getSessions();
        foreach ($sessions as $session) {
            $start = $session->getSession()->getSessionDate();
            $end = clone $start;

            $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));

            $periods[] = ['start' => $start, 'end' => $end];
        }

        return $periods;
    }

    /**
     * @param $module
     * @return array
     */
    public function extractDatesModule($module)
    {
        $periods = [];
        $sessions = $module->getSessions();
        foreach ($sessions as $session) {
            $start = $session->getSession()->getSessionDate();

            $end = clone $start;

            $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));

            $periods[$session->getSession()->getId()] = ['start' => $start, 'end' => $end];
        }

        return $periods;
    }
}
