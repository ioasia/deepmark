<?php

namespace AdminBundle\Services;

use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\QuizSteps;
use ApiBundle\Entity\QuizQuestions;
use ApiBundle\Entity\QuizQuestionsItems;
use ApiBundle\Entity\QuizQuestionsXref;
use ApiBundle\Entity\QuizUsersPlayed;
use ApiBundle\Entity\QuizUsersAnswers;
use ApiBundle\Service\QuizServiceAPI;
use ApiBundle\Entity\Module;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Utils\Statistics\KeyNameConst;

class QuizStatsService {

    private $params;
    private $em;
    private $quiz_id;
    private $step_id;
    private $question_id;
    private $quizServiceAPI;

    /**
     * Construct
     */
    public function __construct(ContainerInterface $container, QuizServiceAPI $quizServiceAPI) {
        // Init
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->quizServiceAPI = $quizServiceAPI;
    }

    /**
     * Get stats for a quiz
     * 
     * @param Module $module
     */
    public function getByModule(Module $module) {
        // Init
        $quiz = $module->getQuiz();
        $intervention = $module->getIntervention();
        $learners = $intervention->getLearners();
        $usersPlayed = [];
        $usersStarted = [];
        $usersFinished = [];
        $totalEnded = 0;
        $totalPass = 0;
        $totalPoints = 0;
        $stepsScoring = [];
        $questionsScoring = [];
        $scoresByUser = [];
        $usersPlayed = [];
        $timers = [];

        // Check results
        $results = $this->quizServiceAPI->getResults($module, $quiz, false, true);
        foreach ($results as $result) {
            // Init
            $user_id = $result['quizPlayed']->getUser_id();

            // Stock users scores
            if (!isset($scoresByUser[$user_id])) {
                $scoresByUser[$user_id] = array(
                    array(
                        'score' => $result['quizPlayed']->getScores(),
                        'date' => $result['quizPlayed']->getCreated()->format('d/m/Y H:i:s'),
                    )
                );
            } else {
                $scoresByUser[$user_id][] = array(
                    'score' => $result['quizPlayed']->getScores(),
                    'date' => $result['quizPlayed']->getCreated()->format('d/m/Y H:i:s'),
                );
            }

            // User played
            if (!isset($usersPlayed[$user_id])) {
                $usersPlayed[$user_id] = array(
                    $result['quizPlayed']->getId() => array(
                        'answers' => $result['quizAnswers']
                    )
                );
            } else {
                $usersPlayed[$user_id][$result['quizPlayed']->getId()] = array(
                    'answers' => $result['quizAnswers']
                );
            }

            // Users played
            if (!isset($usersPlayed[$user_id])) {
                $usersPlayed[$user_id] = 0;
            }
            $usersPlayed[$user_id] ++;

            // Users finished
            if (!isset($usersFinished[$user_id]) && count($result['quizAnswers']['total_answers']) == count($quiz->getQuestions())) {
                $usersFinished[$user_id] = true;
                if (isset($usersStarted[$user_id])) {
                    unset($usersStarted[$user_id]);
                }
            }


            // Users starts but not finished
            if (!isset($usersFinished[$user_id]) && (!isset($usersStarted[$user_id]) || $usersStarted[$user_id] < count($result['quizAnswers']['total_answers']))) {
                $usersStarted[$user_id] = count($result['quizAnswers']['total_answers']);
            }


            // Score for each step (Not finished quiz)
            foreach ($result['quizAnswers']['answers'] as $answer) {
                $question_id = $answer->getQuestion()->getId();
                $params = (array) $answer->getParams();

                // Question scoring
                if (!isset($questionsScoring[$question_id])) {
                    $questionsScoring[$question_id] = array(
                        'played' => 1,
                        'score' => $answer->getScores(),
                        'itemsCount' => 0,
                        'items' => []
                    );
                } else {
                    $questionsScoring[$question_id]['played'] ++;
                    $questionsScoring[$question_id]['score'] += $answer->getScores();
                }

                // Add items_id choosed to get a % of replies
                if (isset($params['answer']->value)) {
                    switch ($answer->getQuestion()->getType()) {
                        case 'category' :
                            $items = $answer->getQuestion()->getItems();
                            foreach ($items as $item) {
                                foreach ($params['items_ids'] as $keyItem => $item_choosed) {
                                    if ($item->getId() == $item_choosed) {
                                        if (!isset($questionsScoring[$question_id]['items'][(string) $item_choosed . '-' . $params['answer']->value[$keyItem]])) {
                                            $questionsScoring[$question_id]['items'][(string) $item_choosed . '-' . $params['answer']->value[$keyItem]] = 1;
                                        } else {
                                            $questionsScoring[$question_id]['items'][(string) $item_choosed . '-' . $params['answer']->value[$keyItem]] ++;
                                        }
                                        $questionsScoring[$question_id]['itemsCount'] ++;
                                    }
                                }
                            }
                            break;
                        case 'scoring' :
                            if (!isset($questionsScoring[$question_id]['items'][(string) $params['answer']->value])) {
                                $questionsScoring[$question_id]['items'][(string) $params['answer']->value] = 1;
                            } else {
                                $questionsScoring[$question_id]['items'][(string) $params['answer']->value] ++;
                            }
                            $questionsScoring[$question_id]['itemsCount'] ++;
                            break;
                        case 'drag_and_drop' :
                            if (is_array($params['answer']->value)) {
                                foreach ($params['answer']->value as $value) {
                                    $matching = $value[0] . '-' . $value[1];
                                    if (!isset($questionsScoring[$question_id]['items'][(string) $matching])) {
                                        $questionsScoring[$question_id]['items'][(string) $matching] = 1;
                                    } else {
                                        $questionsScoring[$question_id]['items'][(string) $matching] ++;
                                    }
                                    $questionsScoring[$question_id]['itemsCount'] ++;
                                }
                            }
                            break;
                        default :
                            if (is_array($params['answer']->value)) {
                                foreach ($params['answer']->value as $value) {
                                    if (is_string($value)) {
                                        if (!isset($questionsScoring[$question_id]['items'][(string) $value])) {
                                            $questionsScoring[$question_id]['items'][(string) $value] = 1;
                                        } else {
                                            $questionsScoring[$question_id]['items'][(string) $value] ++;
                                        }
                                        $questionsScoring[$question_id]['itemsCount'] ++;
                                    }
                                }
                            } else {
                                if (!isset($questionsScoring[$question_id]['items'][(string) $params['answer']->value])) {
                                    $questionsScoring[$question_id]['items'][(string) $params['answer']->value] = 1;
                                } else {
                                    $questionsScoring[$question_id]['items'][(string) $params['answer']->value] ++;
                                }
                                $questionsScoring[$question_id]['itemsCount'] ++;
                            }
                            break;
                    }
                }
            }

            // Scores (only when finished)
            if ($result['quizPlayed']->getScores() !== NULL) {
                // Init
                $totalEnded++;
                $totalPoints += $result['quizPlayed']->getScores();

                // Total pass = number of quiz with scoreToPass OK / number of quiz ended
                if ($result['quizPlayed']->getScores() >= $module->getScoreQuiz()) {
                    $totalPass++;
                }


                // Score for each step
                foreach ($result['quizAnswers']['answers'] as $answer) {
                    $step_id = $answer->getQuestion()->getQuestionXref()[0]->getStep()->getId();
                    $question_id = $answer->getQuestion()->getId();
                    $user_id = $answer->getPlay()->getUser_id();
                    $params = (array) $answer->getParams();

                    // Get timer
                    if (!isset($timers[$answer->getPlay()->getId()])) {
                        if (isset($params['timer_total'])) {
                            $timers[$answer->getPlay()->getId()] = (int) $params['timer_total'];
                        }
                    } else {
                        if (isset($params['timer_total']) && (int) $params['timer_total'] > $timers[$answer->getPlay()->getId()]) {
                            $timers[$answer->getPlay()->getId()] = (int) $params['timer_total'];
                        }
                    }

                    // First time, we create an array with 
                    // played=number of users played, 
                    // usersDetails = detail by user (better play saved)
                    // totalScore = increment score of each try
                    // questionsScores = score_to_pass for each question in this step
                    if (!isset($stepsScoring[$step_id])) {
                        $stepsScoring[$step_id] = array(
                            'played' => 1,
                            'usersDetails' => array(
                                $answer->getPlay()->getUser_id() => array(
                                    'play_score' => $answer->getPlay()->getScores(),
                                    'play_id' => $answer->getPlay()->getId(),
                                    'questions' => array(
                                        $question_id => $answer->getScores()
                                    )
                                )
                            ),
                            'totalScore' => $answer->getScores(),
                            'totalTime' => 0,
                            'questionsScores' => array($question_id => array('expected' => $answer->getQuestion()->getScore_to_pass(), 'totalScore' => $answer->getScores()))
                        );
                    } else {
                        $stepsScoring[$step_id]['played'] ++;

                        // Fill user scores
                        if (!isset($stepsScoring[$step_id]['usersDetails'][$user_id])) {
                            // If user not previously created
                            $stepsScoring[$step_id]['usersDetails'][$user_id] = array(
                                'play_score' => $answer->getPlay()->getScores(),
                                'play_id' => $answer->getPlay()->getId(),
                                'questions' => array(
                                    $question_id => $answer->getScores()
                                )
                            );
                        } else {
                            // User previously created
                            if (!isset($stepsScoring[$step_id]['usersDetails'][$user_id]['questions'][$question_id])) {
                                // if question not previously created for this user
                                $stepsScoring[$step_id]['usersDetails'][$user_id]['questions'][$question_id] = $answer->getScores();
                            } else {
                                // if question already created for this user, update the score only if test score is better
                                if ($stepsScoring[$step_id]['usersDetails'][$user_id]['play_score'] < $answer->getPlay()->getScores()) {
                                    // Start new stock for this user, next lines will increment the score
                                    $stepsScoring[$step_id]['usersDetails'][$user_id] = array(
                                        'play_score' => $answer->getPlay()->getScores(),
                                        'play_id' => $answer->getPlay()->getId(),
                                        'questions' => array(
                                            $question_id => $answer->getScores()
                                        )
                                    );
                                }
                            }
                        }

                        // Check Score already created
                        if (!isset($stepsScoring[$step_id]['totalScore'])) {
                            $stepsScoring[$step_id]['totalScore'] = 0;
                        }
                        $stepsScoring[$step_id]['totalScore'] += $answer->getScores();

                        // Total time by step
                        if (!isset($stepsScoring[$step_id]['totalTime'])) {
                            $stepsScoring[$step_id]['totalTime'] = 0;
                        }
                        $stepsScoring[$step_id]['totalTime'] += (isset($params['timer']) ? $params['timer'] : 0);

                        // Check question already created
                        if (!isset($stepsScoring[$step_id]['questionsScores'][$question_id])) {
                            $stepsScoring[$step_id]['questionsScores'][$question_id] = array(
                                'expected' => $answer->getQuestion()->getScore_to_pass(),
                                'totalScore' => $answer->getScores()
                            );
                        } else {
                            $stepsScoring[$step_id]['questionsScores'][$question_id] = array(
                                'expected' => $answer->getQuestion()->getScore_to_pass(),
                                'totalScore' => $stepsScoring[$step_id]['questionsScores'][$question_id]['totalScore'] + $answer->getScores()
                            );
                        }
                    }
                }
            }
        }

        // Export datas
        if ($intervention && $quiz) {
            if (!empty($timers)) {
                $cumulTimer = 0;
                foreach ($timers as $timer) {
                    $cumulTimer += $timer;
                }
                $datas['quiz_by_module_duration_moy'] = gmdate('H:i:s', $cumulTimer / count($usersPlayed));
            } else {
                $datas['quiz_by_module_duration_moy'] = KeyNameConst::VALUE_N_A;
            }
            $datas['quiz_by_module_progress_pourcent'] = number_format((count($usersFinished) / count($learners) * 100), 2) . '%';
            $datas['quiz_by_module_progress_started'] = count($usersStarted);
            $datas['quiz_by_module_progress_finished'] = count($usersFinished);
            $datas['quiz_by_module_progress_not_started'] = count($learners) - count($usersStarted) - count($usersFinished);
            $datas['quiz_by_module_score_moy'] = ($totalEnded > 0 ? number_format($totalPoints / $totalEnded, 2) . '%' : 'N/A');
            $datas['quiz_by_module_score_pass'] = (($totalPass > 0 & $totalEnded > 0) ? number_format($totalPass / $totalEnded * 100, 2) . '%' : '0%');
            if ($stepsScoring) {
                $datas['quiz_by_module_step_details'] = $stepsScoring;
            } else {
                $datas['quiz_by_module_step_details'] = false;
            }
            if ($questionsScoring) {
                $datas['quiz_by_module_by_question'] = $questionsScoring;
            } else {
                $datas['quiz_by_module_by_question'] = false;
            }
            if ($scoresByUser) {
                $datas['quiz_by_module_learner_score'] = $scoresByUser;
            } else {
                $datas['quiz_by_module_learner_score'] = false;
            }
            if ($usersPlayed) {
                $datas['quiz_by_module_learner_by_question_answer'] = $usersPlayed;
                $playedCount = 0;
                foreach ($usersPlayed as $played) {
                    $playedCount += count($played);
                }
                $datas['quiz_by_module_nb_played_moy'] = number_format($playedCount / count($usersPlayed), 1);
            } else {
                $datas['quiz_by_module_learner_by_question_answer'] = false;
                $datas['quiz_by_module_nb_played_moy'] = false;
            }
        }

        return $datas;
    }

}
