<?php

namespace AdminBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class TemplatingService
{
    private $templating;

    public function __construct(ContainerInterface $container)
    {
        $this->templating = $container->get('templating');
    }

    public function getTemplating()
    {
        return $this->templating;
    }
}
