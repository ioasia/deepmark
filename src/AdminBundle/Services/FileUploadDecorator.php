<?php

namespace AdminBundle\Services;

class FileUploadDecorator
{
    private $templating;

    private $decorated;

    public function __construct(TemplatingService $templating)
    {
        $this->templating = $templating;
    }

    public function decorate(array $files, $role): void
    {
        $templating = $this->templating->getTemplating();
        $this->decorated = $templating->render('AdminBundle:Intervention/ModuleCard/CommonViews:FileModuleLearner.html.twig', ['files' => $files, 'role' => $role, 'session' => $_SESSION]);
    }

    public function getDecorated(): string
    {
        return $this->decorated;
    }
}
