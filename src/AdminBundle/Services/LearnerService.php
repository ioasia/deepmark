<?php

namespace AdminBundle\Services;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Repository\AvailableCalendarRepository;
use ApiBundle\Repository\BookingAgendaRepository;
use ApiBundle\Repository\ProfileRepository;

class LearnerService
{
    /** @var AvailableCalendarRepository */
    private $availableCalendarRepository;
    /** @var ProfileRepository*/
    private $profile;
    /** @var BookingAgendaRepository  */
    private $bookingAgenda;

    public function __construct(AvailableCalendarRepository $availableCalendarRepository
        , ProfileRepository $profile
        , BookingAgendaRepository $bookingAgenda
    )
    {
        $this->availableCalendarRepository = $availableCalendarRepository;
        $this->profile = $profile;
        $this->bookingAgenda = $bookingAgenda;
    }

    /**
     * @param ModuleSession $moduleSession
     * @param array $learners
     * @param $moduleDuration
     * @return array
     */
    public function validateLearnersToSession(ModuleSession $moduleSession, array $learners, $moduleDuration)
    {
        $return = [];
        $start = $moduleSession->getSession()->getSessionDate();
        $end = clone $start;
        $end->setTime($start->format('H') + $moduleDuration->format('H'), $start->format('i') + $moduleDuration->format('i'));
        foreach ($learners as $learner){
            $availableCalendar = false;
            if($learner->getWorkingHours()){
                $availableCalendar = $this->availableWorkingHoursProfileWithSessionTime($learner,clone $start, clone $end);
            }
            $isLearnerBooked = $this->bookingAgenda->isLearnerBooked($learner->getId(), clone $start, clone $end);
            if ($availableCalendar == false || count($isLearnerBooked) > 0){
                $return[] = $learner->getDisplayName();
            }
        }

        return $return;
    }

    /**
     * @param ModuleSession $moduleSession
     * @param array $learners
     * @param Module $module
     * @return array
     */
    public function validateLearnersToSessionByTrainer(ModuleSession $moduleSession, array $learners, $module)
    {
        $return = [];
        $start = $moduleSession->getSession()->getSessionDate();
        $end = clone $start;
        $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));
        foreach ($learners as $learner){
            $availableCalendar = false;
            if($learner->getWorkingHours()){
                $availableCalendar = $this->availableWorkingHoursProfileWithSessionTime($learner,clone $start, clone $end);
            }
            $isLearnerBooked = $this->bookingAgenda->isLearnerBookedWithoutModule($module, $learner->getId(), clone $start, clone $end);
            if ($availableCalendar == false || count($isLearnerBooked) > 0){
                $return[] = $learner->getDisplayName();
            }
        }

        return $return;
    }

    public function validateLearnersToCourse(Intervention $intervention, array $learnerIds)
    {
        $modules = $intervention->getModules();
        $return = [];
        $learners = $this->profile->findBy(array(
                'id' => $learnerIds,
            ));
        foreach ($modules as $module){
            $storedModuleAppId = $module->getStoredModule()->getAppId();

            if (StoredModule::ONLINE == $storedModuleAppId) {
                //individual remote session
                foreach ($learners as $learner) {
                    if (!$learner->getWorkingHours()) {
                        $return[] = $learner->getDisplayName();
                    }
                }
            } else if (StoredModule::VIRTUAL_CLASS == $storedModuleAppId ||
                    StoredModule::ONLINE_WORKSHOP == $storedModuleAppId ||
                    StoredModule::PRESENTATION_ANIMATION == $storedModuleAppId
            ) {
                foreach ($module->getSessions() as $moduleSession) {
                    $return = array_merge($return, $this->validateLearnersToSession($moduleSession, $learners, $module->getDuration()));
                }
            }
        }

        return array_unique($return);
    }

    /**
     * @param Profile $profile
     * @param \DateTime $startTime
     * @param \DateTime $endTime
     * @return bool
     */
    public function availableWorkingHoursProfileWithSessionTime(Profile $profile, \DateTime $startTime, \DateTime $endTime)
    {
        $dayOfWeek = $startTime->format('l');
        $availableStartTime = $startTime->format('H:i');
        $availableEndTime = $endTime->format('H:i');
        $currentAvailabilityLearner = [];
        $availableCalendar = false;

        foreach ($profile->getWorkingHours() as $key => $availabilityLearner) {
            if ($dayOfWeek == $key){
                // find the beginning and ending of day of the learner's working hours
                if ((!$availabilityLearner['startTime'] || !$availabilityLearner['endTime']) && ($availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon'])) {
                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTimeAfternoon'];
                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                } else if (($availabilityLearner['startTime'] && $availabilityLearner['endTime']) && (!$availabilityLearner['startTimeAfternoon'] || !$availabilityLearner['endTimeAfternoon'])) {
                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTime'];
                } else if ($availabilityLearner['startTime'] && $availabilityLearner['endTime'] && $availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon']) {
                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                }

                if (strtotime($availableStartTime) >= strtotime($currentAvailabilityLearner['beginning'])
                    && strtotime($availableEndTime) <= strtotime($currentAvailabilityLearner['ending'])) {
                    $availableCalendar = true;
                }
            }
        }

        return $availableCalendar;
    }
}
