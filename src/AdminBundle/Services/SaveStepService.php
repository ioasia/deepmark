<?php

namespace AdminBundle\Services;

use AdminBundle\Exception\IncorrectArgumentException;
use ApiBundle\Entity\InterventionStep;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SaveStepService
 * @package AdminBundle\Services
 */
class SaveStepService
{
    private $container;

    private $interventionStore;

    private $currentUser;

    private $request;

    private $id;

    private $sessionUniqueId;

    private $unpublisedSession;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setRequest(array $request): void
    {
        $this->request = $request;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function save(): void
    {
        if (!isset($this->request['step'])) {
            throw new IncorrectArgumentException('Step filed cannot be empty');
        }

        $this->setInterventionDraftStorage();

        $stepToSave        = 'setStep' . $this->request['step'];
        $interventionStore = $this->interventionStore;

        if ($this->currentUser != null) {
            $interventionStore->setEntity(
                $this->container->get('doctrine')->getManager()->getRepository('ApiBundle:Profile')
                    ->findOneBy(array('person' => $this->currentUser))->getEntity()
            );
        }
        $interventionStore->$stepToSave($this->request);
        $interventionStore->setSessionId();

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($interventionStore);
        $em->flush();

        $this->id              = $interventionStore->getId();
        $this->sessionUniqueId = $interventionStore->getSessionId();

        $this->unpublisedSession = $interventionStore;
    }

    public function getUnpublishedSession(): ?InterventionStep
    {
        return $this->unpublisedSession;
    }

    public function getSessionUniqueId()
    {
        return $this->sessionUniqueId;
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    /**
     * @param mixed $currentUser
     *
     * @return SaveStepService
     */
    public function setCurrentUser($currentUser)
    {
        $this->currentUser = $currentUser;
        return $this;
    }


    public function saveStepNr(int $stepNr, $content)
    {
        $stepToSave = 'setStep' . $stepNr;
        $this->unpublisedSession->$stepToSave($content);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($this->unpublisedSession);
        $em->flush();
    }

    private function setInterventionDraftStorage(): void
    {
        $foundRecord = '';
        //check if the intervention exists
        if (isset($this->request['sessionId'])) {
            $foundRecord = $this->container->get('doctrine')->getManager()->getRepository(InterventionStep::class)
                ->findOneBy(['sessionId' => $this->request['sessionId']]);
        }

        $this->interventionStore = '' != $foundRecord ? $foundRecord : new InterventionStep();
    }
}
