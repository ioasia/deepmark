<?php

namespace AdminBundle\Services;

use ApiBundle\Entity\InterventionStep;

interface TrainerBookingInterface
{
    public function automaticAssignTrainers(InterventionStep $steps): array;

    public function publish(array $modules): void;

    public function getAvailableTrainers(InterventionStep $steps): array;

    public function getAvailableTrainerForModule($moduleId, $moduleData): array;

    public function assignTrainersForOnline($moduleId, $trainersIds, \DateTime $startDate, \DateTime $endDate);

    public function assignTrainerForSession($moduleId, $sessionId, $trainerId, \DateTime $startDate, \DateTime $endDate);
}
