<?php

namespace AdminBundle\Controller;

use ApiBundle\Controller\RestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Services\QuizService;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\QuizQuestionsXref;
use ApiBundle\Service\QuizServiceAPI;
// Uploads
use ApiBundle\Service\UploadFile;

// END Uploads

class QuizApiController extends RestController {

    public $types = [];
    private $upload_config = [];
    private $quiz_id;
    private $step_id;
    private $question_id;

    /**
     * @Rest\Post("save question")
     * 
     * Save question
     */
    public function saveQuestionAction(Request $request, QuizService $quizService) {
        $params = $request->request->all();
        // Save question
        list($this->quiz_id, $this->step_id, $this->question_id) = $quizService->saveQuestion($params);
        // View
        return new JsonResponse(array(
            'quiz_id' => $this->quiz_id,
            'step_id' => $this->step_id,
            'question_id' => $this->question_id
                ), 200);
    }

    /**
     * Check Quiz tmp or real
     * @param int $id
     * @return boolean
     */
    private function isTmpQuiz($id) {
        return strpos($id, 'tmp_') !== false;
    }

    /**
     * Update all positions on save question
     * 
     * @param Request $request
     */
    public function updatePositionsAction(Request $request, QuizService $quizService) {
        // Get params
        $params = $request->request->all();
        $res = true;
        // Update positions
        if (isset($params['steps']) && !empty($params['steps'])) {
            $res = $quizService->updatePosition($params['quiz_id'], $params['steps'], $params['questions']);
        }
        // View
        return new JsonResponse(array('status' => ($res === true ? 'updated' : 'failed')), 200);
    }

    /**
     * Update status for question / step / quiz
     * 
     * @param string $type question / step / quiz
     * @param int $status
     * @param int $id
     * @param QuizService $quizService
     */
    public function updateStatusAction($type = null, $status = null, $id = null, $json = true, QuizService $quizService) {
        if ($type !== null && $status !== null && $id !== null) {
            $res = $quizService->updateStatus($type, $status, $id);
        } else {
            $res = false;
        }

        // Redirect
        if ($json === 'true') {
            return new JsonResponse(array('status' => ($res === false ? 'failed' : 'updated')), 200);
        } else {
            return $this->redirectToRoute('admin_quiz_index');
        }
    }

    /**
     * Duplicate a quiz
     * 
     * @param Request $request
     */
    public function duplicateAction(Quizes $quiz = null, QuizService $quizService) {
        // Get params
        if ($quiz) {
            $res = $quizService->duplicateQuiz($quiz);
        } else {
            $res = false;
        }

        // Redirect
        return $this->redirectToRoute('admin_quiz_edit', array('id' => $res));
    }

    /**
     * @Rest\Post("upload")
     * 
     * Upload files (with Dropzone)
     */
    public function uploadAction(Request $request, UploadFile $fileApi) {
        // Get params
        $params = $request->request->all();
        // Check type to get specs for upload
        $fileConf = $this->checkUploadType($params);
        if ($fileConf === false) {
            // Error upload not configured or bad params
            $datas = array(
                'status' => 'fail',
                'error' => 'Not configured or bad params');
        } else {
            // Upload can be executed
            $file = $request->files->get('file');
            $validation = true;
            $errors = [];
            // Validate type
            if(!in_array($file->getClientMimeType(), $this->upload_config['validation']['mimeTypes'])) {
                $validation = false;
                $errors[] = "<b>Bad file format. <i>Accepted :</i></b> ".implode(', ', $this->upload_config['validation']['mimeTypes'])." / <b><i>Yours :</i></b> ".$file->getClientMimeType();
            } 
            // Validate size
            if($file->getClientSize() >= $this->upload_config['validation']['maxSize']) {
                $validation = false;
                $errors[] = "<b>Bad file size. <i>Accepted :</i></b> ".($this->upload_config['validation']['maxSize']/1000000)."Mo / <b><i>Yours :</i></b> ". number_format($file->getClientSize()/1000000, 2).'Mo';
            }
            
            if ($validation === true) {
                // File API
                $fileApi->setTargetDirectory($this->upload_config['path']);
                $fileDescriptor = $fileApi->upload($file, $file->getClientOriginalExtension());
                $this->getDoctrine()->getManager()->persist($fileDescriptor);
                $this->getDoctrine()->getManager()->flush();

                // Quiz doc upload for each question into S3
                if (isset($params['type']) && $params['type'] == 'quiz_doc') {
                    try {
                        $base_path = $this->container->getParameter('kernel.root_dir') . '/..';
                        $storage = $this->container->get('storage');
                        $storage->uploadFile($base_path. $this->upload_config['path'] . $fileDescriptor->getPath());
                    } catch (Exception $e) {
                        //echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
                }

                // Import CSV
                if ($params['type'] == 'quiz_csv') {
                    $row = 0;
                    if (($handle = fopen($fileDescriptor->getDirectory() . $fileDescriptor->getPath(), "r")) !== FALSE) {
                        $questionsJS = [];
                        while (($data = fgetcsv($handle, 8000, ";")) !== FALSE) {
                            $type = $data[0];
                            if ($row > 0) {
                                $columns = count($data);
                                $question_id = 'tmp_' . uniqid();
                                $currQuestion = [
                                    'id' => $question_id,
                                    'type' => $type,
                                    'question' => $data[1],
                                    'score_to_pass' => $data[2],
                                    'params' => [
                                        'msg_ok' => $data[3],
                                        'msg_nok' => $data[4],
                                        'answer_type' => $data[5],
                                    ],
                                    'items' => [],
                                    'step' => [
                                        'id' => 'x',
                                        'title' => 'x',
                                        'ordering' => 'x',
                                        'params' => []
                                    ]
                                ];

                                switch ($type) {
                                    case 'qcm' :
                                    case 'qcu' :
                                    case 'ordering' :
                                    case 'category' :
                                        for ($c = 0; $c < $columns; $c++) {
                                            if ($c > 5) {
                                                if (!empty($data[$c]) && (($data[$c + 1] == '0' || $data[$c + 1] == '1') && ($type == 'qcu' || $type == 'qcm') || (!empty($data[$c + 1]) && $type == 'category') || ($type == 'ordering'))) {
                                                    $currQuestion['items'][] = [
                                                        'question_id' => $question_id,
                                                        'type' => $type,
                                                        'name' => $data[$c],
                                                        'value' => $data[$c + 1],
                                                        'specifics' => []
                                                    ];
                                                }
                                                $c++;
                                            }
                                        }
                                        break;

                                    case 'text_to_fill' :
                                        if (!empty($data[6])) {
                                            preg_match_all("/{(.*?)}/si", $data[6], $matches);
                                            $options = [];
                                            if ($matches[1]) {
                                                foreach ($matches[1] as $key => $match) {
                                                    $data[6] = preg_replace('#{' . $match . '}#si', '<span class="marker" data-pos="' . $key . '">' . $match . '</span>', $data[6]);
                                                }
                                            }

                                            $currQuestion['items'][] = [
                                                'question_id' => $question_id,
                                                'type' => $type,
                                                'name' => 'text_to_fill',
                                                'value' => $data[6],
                                                'specifics' => [
                                                    'options' => $matches[1]
                                                ]
                                            ];
                                        }
                                        break;
                                    default :
                                        $currQuestion = false;
                                        break;
                                }
                                if ($currQuestion != false) {
                                    $questionsJS[] = $currQuestion;
                                }
                            }
                            $row++;
                        }
                        fclose($handle);
                    }
                    // Response import csv
                    $datas = array(
                        'status' => 'success',
                        'file' => $fileDescriptor,
                        'questions' => $questionsJS);
                } else {
                    // Other uploads = send file 
                    // Response
                    $datas = array(
                        'status' => 'success',
                        'file' => $fileDescriptor);
                }
            } else {
                // Response
                $datas = array(
                    'status' => 'error',
                    'errors' => implode('<br />', $errors)
                );
            }
        }

        // View
        $view = View::create();
        $view->setData($datas);
        return $this->handleView($view);
    }

    /**
     * Check upload Type
     * 
     * @param array $params
     * @return array
     */
    private function checkUploadType($params) {
        // var_dump($params);
        if (isset($params['type'])) {
            // Base Path Quiz
            $path = '/web/files/InterventionFiles/Quiz/';
            switch ($params['type']) {
                // Quiz CSV upload for each question
                case 'quiz_csv' :
                    if (isset($params['quiz_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        // Add path
                        $path .= $this->quiz_id . '/csv/';
                        // Validation params
                        $accepted_type = array('text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz image upload for each question
                case 'quiz_image' :
                    if (isset($params['quiz_id']) && isset($params['step_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->step_id = $params['step_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/picture/';
                        // Validation params
                        $accepted_type = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_doc' :
                    if (isset($params['quiz_id']) && isset($params['step_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->step_id = $params['step_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/doc/';
                        // Validation params
                        $accepted_type = array(
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/pdf',
                            'text/plain',
                            'text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz audio upload for each question
                case 'quiz_song' :
                    if (isset($params['quiz_id']) && isset($params['step_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->step_id = $params['step_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/song/';
                        // Validation params
                        $accepted_type = array('audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/x-aac', 'audio/x-wav', 'audio/midi', 'audio/webm', 'audio/3gpp', 'audio/3gpp2', 'audio/aac', 'audio/wav');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz video upload for each question
                case 'quiz_video' :
                    if (isset($params['quiz_id']) && isset($params['step_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->step_id = $params['step_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/video/';
                        // Validation params
                        $accepted_type = array('video/quicktime', 'video/3gpp2', 'video/3gpp', 'video/x-flv', 'video/mp4', 'video/x-msvideo', 'video/mpeg', 'video/ogg', 'video/webm');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz question type "Drag And Drop" > Upload image
                case 'quiz_q_drad_and_drop' :
                    if (isset($params['quiz_id']) && isset($params['step_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->step_id = $params['step_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/drag-and-drop/';
                        // Validation params
                        $accepted_type = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                default :
                    return false;
                    break;
            }

            $this->upload_config = array(
                'path' => $path,
                'validation' => [
                    'maxSize' => $accepted_size,
                    'mimeTypes' => $accepted_type
                ],
            );
        } else {
            return false;
        }
    }

    /**
     * Get history
     * 
     * @param QuizServiceAPI $quizServiceAPI
     */
    public function getHistoryAction($type = 'all', QuizServiceAPI $quizServiceAPI) {
        $repository = $this->getDoctrine()->getRepository(Quizes::class);
        $quizes = $repository->findBy(['status' => [0, 1]], ['created' => 'DESC']);

        $quizDatas = [];
        if ($quizes) {
            foreach ($quizes as $key => $quiz) {
                $quizSteps = [];
                $quizSteps = $quizServiceAPI->sanitizeQuiz($quiz);
                $questions_count = 0;
                $quizes[$key]->steps = $quizSteps;
                $steps = [];
                $step_hidden = '';
                $question_hidden = '';
                $type_hidden = '';
                $question_types = [];
                if ($quizSteps) {
                    foreach ($quizSteps as $step) {
                        $questions_count += count($step['questions']);
                        $questions = [];
                        foreach ($step['questions'] as $question) {
                            $questions[] = [
                                'id' => $question->getId(),
                                'name' => mb_substr(strip_tags($question->getQuestion()), 0, 85) . (strlen(strip_tags($question->getQuestion())) > 85 ? '...' : ''),
                                'type' => $question->getType()
                            ];

                            $question_hidden .= $question->getQuestion() . ' <br /> ';
                            $type_hidden .= $question->getType() . ' <br /> ';
                            $question_types[] = $question->getType();
                        }
                        $steps[] = [
                            'id' => $step['step_id'],
                            'name' => $step['step']->getTitle(),
                            'questions' => $questions
                        ];
                        $step_hidden .= $step['step']->getTitle() . ' <br /> ';
                    }
                }

                if ($type == 'all' || in_array($type, $question_types)) {
                    $quizDatas[] = [
                        'quiz_id' => $quiz->getId(),
                        'quiz' => $quiz->getTitle(),
                        'steps_count' => count($quizSteps),
                        'questions_count' => $questions_count,
                        'created' => $quiz->getCreated()->format('Y-m-d H:i:s'),
                        'steps' => $steps,
                        'step_hidden' => $step_hidden,
                        'question_hidden' => $question_hidden,
                        'type_hidden' => $type_hidden,
                    ];
                }
            }
        }

        return new JsonResponse(array('data' => $quizDatas), 200);
    }

    /**
     * Get questions for JS
     * 
     * @param QuizServiceAPI $quizServiceAPI
     */
    public function getQuestionsAction(Request $request, QuizService $quizService) {
        $params = $request->request->all();

        $repository = $this->getDoctrine()->getRepository(QuizQuestionsXref::class);
        $questions = $repository->findBy(['question' => $params['questions']]);

        $questionsJS = [];
        if ($questions) {
            foreach ($questions as $question) {
                $items = $quizService->getItems($question->getId());
                $itemsJS = [];
                $question_id = 'tmp_' . uniqid();
                if ($items) {
                    foreach ($items as $key => $item) {
                        $itemsJS[] = [
                            'question_id' => $question_id,
                            'type' => $question->getQuestion()->getType(),
                            'name' => $item->getName(),
                            'value' => $item->getValue(),
                            'specifics' => json_decode($item->getSpecifics())
                        ];
                    }
                }
                $questionsJS[] = [
                    'history_id' => $question->getId(),
                    'id' => $question_id,
                    'type' => $question->getQuestion()->getType(),
                    'question' => $question->getQuestion()->getQuestion(),
                    'score_to_pass' => $question->getQuestion()->getScore_to_pass(),
                    'params' => json_decode($question->getQuestion()->getParams()),
                    'items' => $itemsJS,
                    'step' => [
                        'id' => $question->getStep()->getId(),
                        'title' => $question->getStep()->getTitle(),
                        'ordering' => $question->getStep()->getOrdering(),
                        'params' => json_decode($question->getStep()->getParams()),
                    ],
                ];
            }
        }

        return new JsonResponse(array('status' => (!empty($questionsJS) ? 'success' : 'error'), 'questions' => $questionsJS), 200);
    }
    

}
