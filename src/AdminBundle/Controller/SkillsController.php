<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Skill;
use ApiBundle\Entity\Domain;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Form\SkillType as Form;
use ApiBundle\Form\DomainType as DomainForm;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SkillsController
 * @package AdminBundle\Controller
 */
class SkillsController extends BaseController implements AdminControllerInterface
{
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Skill');
        $skills     = $repository->findAll();
        $numTrainer = [];
        foreach ($skills as $skill) {
            $numTrainer[$skill->getId()]  = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')->getTotalTrainersBySkillId($skill->getId());
        }

        return $this->render(
            'AdminBundle:skills:index.html.twig',
            ['skills' => $skills, 'numTrainer' => $numTrainer, 'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()]
        );
    }

    public function validationAction(Request $request)
    {
        $liveResourcesWaitingValidation = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['status' => SkillResourceVarietyProfile::STATUS_UNKNOWN]);
        $arrRanking = [];
        foreach ($liveResourcesWaitingValidation as $item) {
            $ranking      = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($item->getProfile());
            $arrRanking[$item->getId()] = $ranking;
        }

        return $this->render(
            'AdminBundle:skills:validation.html.twig',
            ['live_resources_waiting_validation' => $liveResourcesWaitingValidation, 'arrRanking' => $arrRanking, 'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()]
        );
    }

    public function skillStatisticsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Skill');
        $skills     = $repository->findAll();

        return $this->render(
            'statistics/skills.html.twig',
            ['skills' => $skills, 'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()]
        );
    }

    public function deleteAction(Request $request)
    {
        try {
            $id    = $request->get('id');
            $skill = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findOneBy(['id' => $id]);

            if ($skill && $skill->getId()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($skill);
                $entityManager->flush();
            }
        } catch (\Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e, ['id' => $id]);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('notice', 'Could not delete skill');
        }

        return $this->redirectToRoute('admin_skills_index');
    }

    public function deleteSkillAction(Request $request)
    {
        try {
            $id    = $request->get('id');
            $skill = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findOneBy(['id' => $id]);

            if ($skill && $skill->getId()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($skill);
                $entityManager->flush();
            }
        } catch (\Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e, ['id' => $id]);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('notice', 'Could not delete skill');
        }

        return $this->redirectToRoute('admin_skills_index');
    }

    public function editAction(Request $request)
    {
        $id    = $request->get('id', null);
        $skill = null;
        if ($id) {
            $skill = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findOneBy(['id' => $id]);
        }

        $form = $this->createForm(Form::class, $skill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $skill = $form->getData();
            $entityManager->persist($skill);
            $entityManager->flush();

            return $this->redirectToRoute('admin_skills_index');
        }

        return $this->render(
            'AdminBundle:skills:edit.html.twig',
            [ 'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession(), 'form' => $form->createView(), 'isEdit' => $skill instanceof Skill && $skill->getId()]
        );
    }

    /**
     * @param $pageNumber
     *
     * @return PaginationInterface|null
     */
    private function getSkillsForPage($pageNumber): ?PaginationInterface
    {
        try {
            $repository = $this->getDoctrine()->getRepository('ApiBundle:Skill');
            $skills     = $repository->findAll();
            /** @var \Knp\Component\Pager\Paginator $paginator */
            $paginator = $this->get('knp_paginator');
            $skills    = $paginator->paginate($skills, $pageNumber);
        } catch (\Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('notice', 'Could not load the skills');
        }

        return $skills;
    }

    public function domainsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Domain');
        $domains     = $repository->findAll();
        return $this->render(
            'AdminBundle:skills:domains.html.twig',
            ['domains' => $domains]
        );
    }

    public function domainDeleteAction(Request $request)
    {
        try {
            $id    = $request->get('id');
            $domain = $this->getDoctrine()->getRepository('ApiBundle:Domain')->findOneBy(['id' => $id]);

            if ($domain && $domain->getId()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($domain);
                $entityManager->flush();
            }
        } catch (\Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e, ['id' => $id]);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('notice', 'Could not delete domain');
        }

        return $this->redirectToRoute('admin_skills_skill_domains');
    }

    public function domainEditAction(Request $request)
    {
        $id    = $request->get('id', null);
        $domain = null;
        if ($id) {
            $domain = $this->getDoctrine()->getRepository('ApiBundle:Domain')->findOneBy(['id' => $id]);
        }

        $form = $this->createForm(DomainForm::class, $domain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $domain = $form->getData();
            $entityManager->persist($domain);
            $entityManager->flush();

            return $this->redirectToRoute('admin_skills_skill_domains');
        }

        return $this->render(
            'AdminBundle:skills:domainEdit.html.twig',
            [ 'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession(), 'form' => $form->createView(), 'isEdit' => $domain instanceof Domain && $domain->getId()]
        );
    }
}
