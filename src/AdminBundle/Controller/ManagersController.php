<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use Controller\BaseController;
use DateTime;
use Exception;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ManagersController
 * @package AdminBundle\Controller
 */
class ManagersController extends BaseController
{
    /**
     * Lists all profile entities.
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::MANAGER,
            )
        );
        $managers     = $em->getRepository('ApiBundle:Profile')->findBy(array(
            'profileType' => $profileType,
        ));

        $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $managers   = $paginator->paginate($managers, $request->query->getInt('page', 1));

        return $this->render('AdminBundle:managers:index.html.twig', array(
            'managers'             => $managers,
            'interventionsManager' => $interventions,
        ));
    }

    /**
     * Creates a new profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form    = $this->createForm('ApiBundle\Form\ProfileManagerType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailProfile = $profile->getPerson()->getEmail();
            $em           = $this->getDoctrine()->getManager();
            $profileType  = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::MANAGER,
                )
            );
            $profile->setProfileType($profileType);
            $profile->getPerson()->setRoles(array('ROLE_MANAGER'));
            $profile->getPerson()->setUsername($emailProfile);
            $profile->getPerson()->setEnabled(true);
            $profile->setCreationDate(new DateTime());
            $profile->setOtherEmail($emailProfile);
            $profile->setUpdateDate(new DateTime());
            $profile->setBillingCode('');
            $profile->setStreet('');

            $em->persist($profile->getPerson());
            $em->persist($profile);
            $em->flush();
            $this->sendEmail($profile);
            return $this->redirectToRoute('admin_managers_show', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:managers:formManagers.html.twig', array(
            'profile' => $profile,
            'form'    => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     *
     * @param Profile $profile
     *
     * @return Response
     */
    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('AdminBundle:managers:show.html.twig', array(
            'profile'     => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm   = $this->createForm('ApiBundle\Form\ProfileManagerEditType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $profile->setUpdateDate(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('admin_managers_edit', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:managers:formManagers.html.twig', array(
            'profile'     => $profile,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit'      => true,
        ));
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param $id
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $isDelete   = $request->request->get('isDelete');

        $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
        $person     = $profile->getPerson();

        if ($isDelete) {
            $em->remove($profile);
            $em->remove($profile->getPerson());
            $em->flush();
        } else {
            $person->setEnabled(0);
            $em->persist($person);
            $em->flush();
        }

        return $this->redirectToRoute('admin_managers_index');
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_managers_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
