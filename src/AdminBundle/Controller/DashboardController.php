<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use Controller\BaseController;
use DateTime;
use ApiBundle\Entity\Exercises;
use Symfony\Component\HttpFoundation\Response;
use Utils\Email;
use Utils\Statistics\KeyNameConst;

/**
 * Class DashboardController
 * @package AdminBundle\Controller
 */
class DashboardController extends BaseController
{
    /**
     * @var array
     */
    private $notificationsCount = array(
        'activities' => 0,
        'alerts'     => 0,
        'reports'    => 0,
        'exercices'  => 0,
    );

    /**
     * For learner/activities
     * @return Response
     * @throws \Exception
     */
    public function indexAction()
    {
        $em      = $this->getDoctrine()->getManager();
        $profile = $this->getCurrentProfile();
        $roles   = $this->getUser()->getRoles();
        //allow supper admin show all report, no need to filter by admin id
        $adminId = !in_array('ROLE_SUPER_ADMIN', $roles) ? $profile->getId() : null;
        $unDoneStatusId    = $em->getRepository('ApiBundle:Status')->findOneBy(['appId' => Status::REPORT_UNDONE])->getId();

        $liveResourcesWaitingValidation = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['status' => SkillResourceVarietyProfile::STATUS_UNKNOWN]);
        //$numReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->countAdminReportByStatus($adminId, ModuleReports::STATUS_SUBMITTED);
        $numReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus(null, ModuleReports::STATUS_SUBMITTED);

        // show code
        $now = new DateTime('now');
        if ($this->getUser()->isRole('ROLE_SUPER_ADMIN')) {
            $interventions = $this->getDoctrine()->getRepository(Intervention::class)
                ->getCurrentIntervention(null, $now);
        } else {
            $interventions = $this->getDoctrine()->getRepository(Intervention::class)
                ->getCurrentIntervention($this->getEntityFromProfile(), $now);
        }

        $progressionLearners = array();
        $deleteForms   = array();

        foreach ($interventions as $intervention) {
            $learnersProgress = $this->getDoctrine()->getRepository(LearnerIntervention::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $modules = $this->getDoctrine()->getRepository(Module::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $totalDurations = 0;
            $avgSpentTime = 0;
            $numNotStarted = $numStarted = $numFinished = 0;
            $nbLearners    = 0;
            $progress      = 0;
            $totalProgress = 0;
            foreach ($learnersProgress as $learnerProgress) {
                ++$nbLearners;
                $progress += $learnerProgress->getProgression();
                if ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $numNotStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $numStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_FINISHED) {
                    $numFinished++;
                }
            }

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $numDurations = $module->getDuration()->format('H:i:s');
                $totalDurations += $this->convertTimeToDecimal($numDurations);

                // calculation the avg time spent
                $realTimeSpentModule = 0;
                $numOfStatusCompleted = 0;
                $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                foreach ($moduleIterations as $iteration) {
                    $status = $iteration->getStatus() ? $iteration->getStatus()->getAppId() : null;
                    switch ($status) {
                        case Status::WITH_BOOKING_DONE:
                            $realTimeSpentModule += $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 0;
                            $numOfStatusCompleted++;
                            break;
                        default:
                            break;
                    }
                }
                $avgSpentTime += $numOfStatusCompleted > 0 ? $realTimeSpentModule/$numOfStatusCompleted : 0;
            }

            if ($nbLearners > 0) {
                $totalProgress = $progress / $nbLearners;
            }

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                ->getResultsByIntervention($intervention->getId());

            $nbNotation    = 0;
            $notation      = 0;
            $totalNotation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                ++$nbNotation;
                $notation += $learnerNotation->getNotation();
            }

            if ($nbNotation) {
                $totalNotation = $notation / $nbNotation;
            }

            $intervention->setLearnersNotation($totalNotation);
            $intervention->setLearnersProgress($totalProgress);
            $intervention->setNbLearner($nbLearners);

            $progressionLearners[$intervention->getId()] = [
                'numNotStarted' => $numNotStarted,
                'numStarted' => $numStarted,
                'numFinished' => $numFinished,
                KeyNameConst::KEY_NUM_DURATIONS => $this->secondsToTime($totalDurations*60),
                KeyNameConst::KEY_TOTAL_SPENT_TIME => $this->secondsToTime($avgSpentTime)
            ];
        }
        // get answers to validation
        $exercisesToValidations = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_SUBMITTED);

        return $this->render('AdminBundle:Dashboard:index.html.twig', array(
            'numSkill' => count($liveResourcesWaitingValidation),
            'numReport' => $numReport,
            'interventions' => $interventions,
            'progressionLearners' => $progressionLearners,
            'deleteForms'   => $deleteForms,
            'numberOfExercisesToValidations'   => count($exercisesToValidations),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * For trainer/activities
     * @return Response
     * @throws \Exception
     */
    public function trainerActivitiesAction()
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);

        $dateMin = new \DateTime('last saturday');
        $dateMin->modify('+1 day');
        $dtMax = clone $dateMin;
        $dtMax->modify('+6 days');

        return $this->render('AdminBundle:Dashboard:trainer-activities.html.twig', array(
            'count'         => $this->notificationsCount,
            'modal'         => false,
            'bookings'      => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBookingByProfileAndDate(null, new \DateTime(date("Y-m-d"))),
            'bookingsWeek'  => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findModulesByRange(null, $dateMin, $dtMax),
            'bookingsMonth' => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findModulesByRange(
                    null,
                    new \DateTime('first day of this month 00:00:00'),
                    new \DateTime('last day of this month 00:00:00')
                ),
        ));
    }

    /**
     * For /
     * @return Response
     */
    public function alertsAction()
    {
        $em      = $this->getDoctrine()->getManager();
        $profile = $this->getCurrentProfile();
        $roles   = $this->getUser()->getRoles();
        //allow supper admin show all report, no need to filter by admin id
        $adminId = !in_array('ROLE_SUPER_ADMIN', $roles) ? $profile->getId() : null;

        $liveResourcesWaitingValidation = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['status' => SkillResourceVarietyProfile::STATUS_UNKNOWN]);

        $replaceResourcesWaitingValidation = $this->getDoctrine()->getRepository('ApiBundle:RequestReplacementResource')
            ->findBy(['status' => RequestReplacementResource::STATUS_UNKNOWN]);

        $count = array(
            'activities'  => 0,
            'alerts'      => 0,
            'resources'   => 0,
            'validations' => 0,
        );

        $alertCount = array(
            'interventions' => 0,
            'reports'       => 0,
            'courses'       => 0,
            'plannings'     => (is_array($liveResourcesWaitingValidation) ? count($liveResourcesWaitingValidation) : 0)
                + (is_array($replaceResourcesWaitingValidation) ? count($replaceResourcesWaitingValidation) : 0),
            'messages'      => 0,
            'billings'      => 0,
        );

        foreach ($alertCount as $alertNb) {
            $count['alerts'] += $alertNb;
        }

        return $this->render('AdminBundle:Dashboard:alerts.html.twig', array(
            'count'                                      => $count,
            'alert_count'                                => $alertCount,
            'interventions'                              => [],
            'count_reports_un_realised'                  => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')->countAdminReportByStatus($adminId, ModuleReports::STATUS_TODO),
            'reports_un_realised'                        => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')->findAdminReportByStatus($adminId, ModuleReports::STATUS_TODO),
            'count_reports_un_validated'                 => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')->countAdminReportByStatus($adminId, ModuleReports::STATUS_SUBMITTED),
            'reports_un_validated'                       => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')->findAdminReportByStatus($adminId, ModuleReports::STATUS_SUBMITTED),
            'count_reports_rejected'                     => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')->countAdminReportByStatus($adminId, ModuleReports::STATUS_REJECTED),
            'reports_rejected'                           => $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleReports')
                ->findAdminReportByStatus($adminId, ModuleReports::STATUS_REJECTED),
            'count_modules_not_done_learner'             => 0,
            'modules_not_done_learner'                   => [],
            'count_modules_not_corrected_trainer'        => 0,
            'modules_not_corrected_trainer'              => [],
            'count_modules_not_validated_admin'          => 0,
            'modules_not_validated_admin'                => [],
            'activities'                                 => [],
            'live_resources_waiting_validation'          => $liveResourcesWaitingValidation,
            'replace_resources_waiting_validation'       => $replaceResourcesWaitingValidation,
            'messages_live_trainers'                     => [],
            'messages_learners'                          => [],
            'messages_clients'                           => [],
            'messages_clients_renting'                   => [],
            'invoices_clients_sent'                      => [],
            'count_invoices_clients_sent'                => 0,
            'invoices_clients_waiting_validation'        => [],
            'count_invoices_clients_waiting_validation'  => 0,
            'invoices_provider_waiting_validation'       => [],
            'count_invoices_provider_waiting_validation' => 0,
            // TODO : Remove
            'reports'                                    => [],
        ));
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function alerts2Action()
    {
        return $this->render('AdminBundle:Dashboard:alerts.html.twig', array(
            'interventions' => $this->dayActivitiesAction(null),
            'reports'       => $this->getReports(),
            'invoices'      => $this->getInvoices(),
        ));
    }

    /**
     * @return Response
     */
    public function validationAction()
    {
        return $this->render('@Admin/Dashboard/validations.html.twig', array(
            'interventions' => [],
            'reports'       => [],
            'exercices'     => [],
        ));
    }

    /**
     * Format the data to display in the dashboard.
     *
     * @param AssigmentResourceSystem $resource
     * @param BookingAgenda $booking
     *
     * @return array
     */
    private function getInterventionDisplay(AssigmentResourceSystem $resource, BookingAgenda $booking)
    {
        $moduleIntervention = $this->getDoctrine()->getRepository('ApiBundle:ModuleIntervention')
            ->findOneBy(['module' => $resource->getModule()]);
        if (null != $moduleIntervention->getModule()->getSubject()) {
            $theme = $moduleIntervention->getModule()->getSubject()->getDesignation();
        } else {
            $theme = '';
        }

        if (null != $moduleIntervention->getModule()->getStatus()) {
            $status = $moduleIntervention->getModule()->getStatus()->getDesignation();
        } else {
            $status = '';
        }

        return [
            'org'          => '/img/Logo_BP-NORD.png',
            'intervention' => $moduleIntervention->getIntervention()->getDesignation(),
            'theme'        => $theme,
            'liveTrainer'  => $resource->getLiveResource(),
            'apprenant'    => $booking->getLearner(),
            'type'         => $moduleIntervention->getModule()->getStoredModule()->getDesignation(),
            'doc'          => 'file',
            'beginning'    => $booking->getBookingDate(),
            'duration'     => $booking->getBookingDate()->format('H:m') . ' '
                . $moduleIntervention->getModule()->getDuration()->format('m') . 'min',
            'status'       => $status,
        ];
    }

    /**
     * Get the resource for the user.
     *
     * @return object[]
     */
    private function getAssigmentRessourceSystem()
    {
        $person  = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findBy(['person' => $person]);

        return $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy(['liveResource' => $profile]);
    }

    /**
     * Get booking with date after the param.
     * @param DateTime $date
     * @return array
     */
    private function getBookingByDate(DateTime $date)
    {
        $allbookings = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findAll();
        $bookings    = [];

        foreach ($allbookings as $booking) {
            if ($booking->getBookingDate() > $date->setTime(0, 0, 0)) {
                if ($booking->getBookingDate() < $date->setTime(23, 0, 0)) {
                    array_push($bookings, $booking);
                }
            }
        }

        return $bookings;
    }

    /**
     * Get activity of a day
     * @param $date
     * @return array
     * @throws \Exception
     */
    public function dayActivitiesAction($date)
    {
        $date = $date ?? new DateTime;

        $resources = $this->getAssigmentRessourceSystem();
        //Set the time at the begin and the end of the days
        $bookings = $this->getBookingByDate($date);

        $interventions = [];

        //Get module in the day
        foreach ($resources as $resource) {
            foreach ($bookings as $booking) {
                if ($booking->getModule()->getId() === $resource->getModule()->getId()) {
                    $intervention = $this->getInterventionDisplay($resource, $booking);
                    array_push($interventions, $intervention);
                }
            }
        }

        return $interventions;
    }

    /**
     * @return \ApiBundle\Entity\Quotation[]|\ApiBundle\Entity\Report[]|SkillResourceVarietyProfile[]|object[]
     */
    private function getReports()
    {
        $reports = $this->getDoctrine()->getRepository('ApiBundle:Report')
            ->findBy(['admin' => $this->getCurrentProfile()]);

        return $reports;
    }

    /**
     * @return \ApiBundle\Entity\Quotation[]|\ApiBundle\Entity\Report[]|SkillResourceVarietyProfile[]|object[]
     */
    private function getInvoices()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Quotation')
            ->findBy(['admin' => $this->getCurrentProfile()]);
    }

    private function getCurrent()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);
    }

    /**
     * @return ProfileVariety|object
     */
    private function getLearnerProfileType()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(array(
            'designation' => 'TYPE_LEARNER',
        ));
    }

    /**
     * @return ProfileVariety|object
     */
    private function getTrainerProfileType()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(array(
            'designation' => 'TYPE_TRAINER',
        ));
    }

    private function getProfileDisabled()
    {
        $entity = $this->getCurrent()->getEntity();

        $profiles = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findBy(['entity' => $entity]);

        $learnerType = $this->getLearnerProfileType();
        $trainerType = $this->getTrainerProfileType();

        $profilesDisable = [];
        foreach ($profiles as $profile) {
            if ($profile->getProfileType() === $learnerType || $profile->getProfileType() === $trainerType) {
                if (!$profile->getPerson()->isEnabled()) {
                    array_push($profilesDisable, $profile);
                }
            }
        }

        return $profilesDisable;
    }

    private function getCoursesToBook()
    {
        $coursesToBook = [];

        $coursesStarted = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->getLearnerInterventionToBook(null);

        foreach ($coursesStarted as $intervention) {
            $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')
                ->findBy(['intervention' => $intervention->getIntervention()]);

            foreach ($modules as $module) {
                if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                    $book = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                        ->findOneBy(['module' => $module]);

                    if (is_null($book)) {
                        array_push($coursesToBook, $module);
                    }
                }
            }
        }

        return $coursesToBook;
    }

    /**
     * Convert time into decimal time (minute).
     *
     * @param string $time The time in the format hh:mm:ss to convert
     *
     * @return integer The time as a decimal value.
     */
    function convertTimeToDecimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60);

        return $decTime;
    }

    function secondsToTime($seconds)
    {
        if ($seconds <= 0) return '';

        $ret = "";

        /*** get the days ***/
        $days = intval(intval($seconds) / (3600*24));
        if($days> 0)
        {
            $ret .= $days > 10 ? "$days:" : "0$days:";
        }

        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if($hours > 0)
        {
            $ret .= $hours > 9 ? "$hours:" : "0$hours:";
        } else {
            $ret .= "00:";
        }

        /*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if($minutes > 0)
        {
            $ret .= $minutes > 9 ? "$minutes:" : "0$minutes:";
        } else {
            $ret .= "00:";
        }

        /*** get the seconds ***/
        $seconds = intval($seconds) % 60;
        if ($seconds > 0) {
            $ret .= $seconds > 9 ? "$seconds" : "0$seconds";
        } else {
            $ret .= "00";
        }

        return $ret;
    }
}
