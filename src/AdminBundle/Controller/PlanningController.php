<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Planning controller.
 */
class PlanningController extends BaseController implements AdminControllerInterface
{
    /**
     * Lists all booking.
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AdminBundle:planning:index.html.twig', array(
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()

        ));
    }

}
