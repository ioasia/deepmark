<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\ModuleReports;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;

/**
 * Entity controller.
 */
class ReportsController extends BaseController
{
    public function indexAction(Request $request)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $reportsToDo = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus(null, ModuleReports::STATUS_TODO);
        $reportsSubmitted = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus(null, ModuleReports::STATUS_SUBMITTED);
        $reportsApproved = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus(null, ModuleReports::STATUS_APPROVED);
        $reportsRejected = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus(null, ModuleReports::STATUS_REJECTED);

        return $this->render('AdminBundle:report:index.html.twig', array(
            'reportsToDo' => $reportsToDo,
            'reportsSubmitted' => $reportsSubmitted,
            'reportsApproved' => $reportsApproved,
            'reportsRejected' => $reportsRejected,
            'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function validationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entityIds = $request->request->get('selectedEntities');
        $type = $request->request->get('type');
        $comment = $request->request->get('comment');
        foreach ($entityIds as $id){
            try {
                $report = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->find($id);
                if ($report) {
                    // save the ModuleReports object
                    $status = ($type == 1) ? ModuleReports::STATUS_APPROVED : ModuleReports::STATUS_REJECTED;
                    $report->setStatus($status);
                    $report->setComment($comment);
                    $report->setValidationDate(new DateTime());
                    $em->persist($report);
                    $em->flush();
                }
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $response = 'ok';
        return new JsonResponse($response, 200);
    }
}
