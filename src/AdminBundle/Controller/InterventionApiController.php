<?php

namespace AdminBundle\Controller;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Exception\InterventionBaseException;
use AdminBundle\Services\InterventionStepConsistentService;
use AdminBundle\Services\SaveStepService;
use AdminBundle\Utils\Intervention\ValueSwap;
use ApiBundle\Controller\RestController;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Traits\IsAvailable;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InterventionApiController
 * @package AdminBundle\Controller
 */
class InterventionApiController extends RestController
{
    use IsAvailable;
    /**
     * @Rest\Post("step")
     *
     * @param ParamFetcher $paramFetcher
     * @param LoggerInterface $logger
     * @param SaveStepService $saveStepService
     *
     * @return Response
     *
     * @throws IncorrectArgumentException
     * @Rest\RequestParam(name="current", nullable=false, strict=true, description="step id")
     * @Rest\RequestParam(name="session_id", nullable=false, strict=true, description="Intervention step session id.")
     * @Rest\RequestParam(name="data", nullable=false, strict=false, description="form data")
     */
    public function saveStepAction(
        ParamFetcher $paramFetcher,
        LoggerInterface $logger,
        SaveStepService $saveStepService,
        InterventionStepConsistentService $interventionStepConsistentService
    ) {
        $request = $paramFetcher->all();
        if (is_string($request['data'])) {
            parse_str($request['data'], $requestData);
        }

        if (!isset($requestData) || empty($requestData)) {
            $view = View::create();
            $view->setData(array_merge(['status' => 'ommited']));

            return $this->handleView($view);
        }

        $currentStep = $request['current'];

        $remake = false;
        if (isset($requestData['remake'])) {
            $remake = 'yes' == $requestData['remake'];
        }

        if (!isset($currentStep)) {
            $logger->error('missing current step param in request', ['request' => $requestData]);

            return $this->failResponse();
        }

        $saveStepService->setRequest($requestData);
        $saveStepService->save();

        $unpublishedSession = $saveStepService->getUnpublishedSession();

        $requestData['step_serialize_id'] = $saveStepService->getId();
        $requestData['session_unique_id'] = $saveStepService->getSessionUniqueId();

        if (2 == $currentStep && $unpublishedSession) {
            $interventionStepConsistentService->makeStepConsistent($unpublishedSession);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($unpublishedSession);
            $entityManager->flush();
        }

        /*
         *  If the user will go back to step one and amend his/hers choices of
         *  modules. For now the need is only for step one
         */

        if ($remake) {
            try {
                $unpublishedSessionArray = $unpublishedSession->__toArray();
                $stepSwap                = new ValueSwap($unpublishedSessionArray, $requestData);
                $stepSwap->updateValues();

                $saveStepService->saveStepNr($stepSwap->getModifiedStep(), $stepSwap->getMofidiedContent());

                return $this->successResponse(
                    [
                        'stepNr'    => ValueSwap::SESSION_NR,
                        'sessionId' => $saveStepService->getSessionUniqueId(),
                    ]
                );
            } catch (InterventionBaseException $exception) {
                $logger->error($exception->getMessage(), ['exception' => $exception]);

                return $this->failResponse($exception);
            }
        }

        return $this->successResponse();
    }

    private function successResponse($additionalData = [])
    {
        $view = View::create();
        $view->setData(array_merge(['status' => 'success'], $additionalData));

        return $this->handleView($view);
    }

    private function failResponse(Exception $e = null)
    {
        $responseData = ['status' => 'fail'];
        if ($e) {
            $responseData['message'] = $e->getMessage();
        }
        $view = View::create();
        $view->setData($responseData);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("calendars-constraints")
     * GET calendar
     * Display exclude weeks in constraints for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=false, description="Date End")
     * @Rest\QueryParam(name="excludeWeeks", nullable=false, description="Exclude Weeks")
     */
    public function getCalendarsConstraintsAction(ParamFetcher $paramFetcher)
    {
        $dateStart    = new \DateTime($paramFetcher->get('dateStart'));
        $dateEnd      = new \DateTime($paramFetcher->get('dateEnd'));
        $excludeWeeks = $paramFetcher->get('excludeWeeks');

        $calendar_array = $this->getConstraints($dateStart, $dateEnd, $excludeWeeks);

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    public function getConstraints(\DateTime $dateStart, \DateTime $dateEnd, $excludeWeeks)
    {
        $booking_array = [];
        $excludeWeeks  = explode(",", $excludeWeeks);

        foreach ($excludeWeeks as $excludeWeek) {
            $days = explode("|", $excludeWeek);
            if ($days[0] && $days[1]) {
                $booking_array[] = array(
                    'title' => '',
                    'start' => $days[0],
                    'end'   => $days[1],
                );
            }
        }

        return $booking_array;
    }

    /**
     * @Rest\Get("echo-programs")
     * GET echo programs
     * Get date of programs of ECHO application
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="organizationId", nullable=false, description="Organization Id")
     * @Rest\QueryParam(name="applicationId", nullable=false, description="Application Id")
     */
    public function getEchoProgramsAction(ParamFetcher $paramFetcher)
    {
        $organizationId = $this->getParameter('echo_organization_id');
        $applicationId  = $this->getParameter('echo_application_id');
        $url            = $this->getParameter('echo_url') . '/api/integration/' . $organizationId . '/' . $applicationId . '/programs';

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'x-api-key'    => $this->getParameter('echo_api_key'),
                'Content-Type' => 'application/json',
            ],
        ]);

        $response = $client->request('GET', $url);

        $view = View::create();

        $view->setData(json_decode($response->getBody(), true));

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("embed-checking")
     * GET check allow embed
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="url", nullable=true, description="Url")
     */
    public function getEmbedCheckingAction(ParamFetcher $paramFetcher)
    {
        $url = $paramFetcher->get('url');
        $view = View::create();
        if ($this->isValidURL($url)) {
            $view->setData(['status' => $this->allowEmbed($url)]);
        } else {
            $view->setData(['status' => -1]);
        }

        return $this->handleView($view);
    }
}
