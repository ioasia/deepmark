<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\GroupOrganization;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Regional;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Regional controller.
 */
class RegionalController extends BaseController implements AdminControllerInterface
{
    /**
     * Lists all entity entities.
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $regionals = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Regional')->findAll();

        return $this->render('AdminBundle:regional:index.html.twig', array(
            'regionals' => $regionals,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Creates a new Regional entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $regional = new Regional();
        $form     = $this->createForm('ApiBundle\Form\RegionalType', $regional);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($regional);
            $em->flush();

            return $this->redirectToRoute('admin_regional_show', array(
                'id' => $regional->getId(),
            ));
        }

        return $this->render('AdminBundle:regional:formRegional.html.twig', array(
            'regional' => $regional,
            'form'     => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Finds and displays a regional entity.
     *
     * @param Regional $regional
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Regional $regional)
    {
        $deleteForm = $this->createDeleteForm($regional);

        return $this->render('AdminBundle:regional:show.html.twig', array(
            'regional'    => $regional,
            'delete_form' => $deleteForm->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Displays a form to edit an existing regional entity.
     *
     * @param Request $request
     * @param Regional $regional
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Regional $regional)
    {
        $deleteForm = $this->createDeleteForm($regional);
        $editForm   = $this->createForm('ApiBundle\Form\RegionalType', $regional);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_regional_edit', array('id' => $regional->getId()));
        }

        return $this->render('AdminBundle:regional:formRegional.html.twig', array(
            'regional'    => $regional,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit' => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Deletes a regional entity.
     *
     * @param Request $request
     * @param Regional $regional
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Regional $regional)
    {
        $form = $this->createDeleteForm($regional);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            $flashBag = $this->get('session')->getFlashBag();
            $messages = $this->deleteRegion($regional, $translator);
            if (!$messages) {
                $flashBag->add('warning', $translator->trans('global.regional_delete_success', ['%regional%' => $regional->getDesignation()]));
                $em = $this->getDoctrine()->getManager();
                $em->remove($regional);
                $em->flush();
            } else {
                $flashBag->add('success', $messages);
            }
        }

        return $this->redirectToRoute('admin_regional_index');
    }

    public function deleteRegionsAction(Request $request)
    {
        $translator = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $regionIds = $request->request->get('selectedRegions');
        $flashBag = $this->get('session')->getFlashBag();
        foreach ($regionIds as $id){
            $messages = '';
            $region = $em->getRepository('ApiBundle:Regional')->find($id);
            if ($type == 1) {
                try {
                    $messages = $this->deleteRegion($region, $translator);
                    if (!$messages) {
                        $flashBag->add('warning', $translator->trans('global.regional_delete_success', ['%region%' => $region->getDesignation()]));
                        $em->remove($region);
                        $em->flush();
                    } else {
                        $flashBag->add('success', $messages);
                    }
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        return new JsonResponse($messages, 200);
    }

    /**
     * Delete a region.
     *
     * @param Regional $region The region
     *
     * @return Boolean
     */
    private function deleteRegion(Regional $region, $translator)
    {
        $errors = '';
        $em                     = $this->getDoctrine()->getManager();
        $profiles               = $em->getRepository(Profile::class)->findBy(['regional' => $region]);
        $groupOrganizations     = $em->getRepository(GroupOrganization::class)->findBy(['regional' => $region]);

        if ($profiles || $groupOrganizations) {
            $errors .= $translator->trans('global.regional_can_not_delete_success', ['%regional%' => $region->getDesignation()]);
        }

        if ($profiles) {
            $errors .= $translator->trans('global.regional_has_total_profile', ['%profile%' => count($profiles)]);
        }

        if ($groupOrganizations) {
            $errors .= $translator->trans('global.regional_has_total_sector', ['%sector%' => count($groupOrganizations)]);
        }

        return $errors;
    }

    /**
     * Creates a form to delete a regional entity.
     *
     * @param Regional $regional The regional entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Regional $regional)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_regional_delete', array('id' => $regional->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
