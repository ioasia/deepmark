<?php

namespace AdminBundle\Controller;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Exception\InterventionBaseException;
use AdminBundle\Services\FilesForModuleUploadDecorator;
use AdminBundle\Services\FileUploadDecorator;
use AdminBundle\Services\InterventionPersist;
use AdminBundle\Services\InterventionStepConsistentService;
use AdminBundle\Services\SaveStepService;
use AdminBundle\Services\TrainerBooking\TrainerBookingService;
use AdminBundle\Utils\FileUplaod\FileConstantMap;
use AdminBundle\Utils\FileUplaod\FileRetrieval;
use AdminBundle\Utils\FileUplaod\FileUploadFactory;
use AdminBundle\Utils\Intervention\SessionHandler;
use AdminBundle\Utils\Intervention\StepTemplateRender;
use AdminBundle\Utils\Intervention\ValueSwap;
use AdminBundle\Utils\Modules\ModuleValueCommand;
use AdminBundle\Utils\Modules\ModuleValueFactory;
use AdminBundle\Utils\Modules\Validation\ModuleValidator;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\ModuleTemplate;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Service\UploadFile;
use AppBundle\Utils\UserTimeZone;
use Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Yaml\Yaml;
use Traits\Controller\HasForm;
use Traits\Controller\HasUsers;
use Traits\IsAvailable;
use Traits\Symfony\HasTranslate;
use Utils\Statistics\KeyNameConst;

/**
 * Class InterventionController
 * @package AdminBundle\Controller
 */
class InterventionController extends BaseController
{
    use HasUsers;
    use HasForm;
    use HasTranslate;
    use IsAvailable;

    private $fileApi;

    /** @var \AdminBundle\Utils\FileUplaod\UploadFile $uploader */
    private $uploader;

    /** @var Request $request */
    private $request;

    /**
     * @param Request $request
     * @return Response
     */
    public function indexPublishedAction(Request $request): Response
    {
        if ($this->getUser()->isRole('ROLE_SUPER_ADMIN')) {
            /** @var Intervention[] $interventions */
            $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
                ->findAll();
        } else {
            /** @var Intervention[] $interventions */
            $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
                ->findBy(['entity' => $this->getEntityFromProfile()], [
                    'id' => 'DESC',
                ]);
        }

        $progressionLearners = array();
        $deleteForms   = array();

        foreach ($interventions as $intervention) {
            $learnersProgress = $this->getDoctrine()->getRepository(LearnerIntervention::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $modules = $this->getDoctrine()->getRepository(Module::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $totalDurations = 0;
            $avgSpentTime = 0;
            $numNotStarted = $numStarted = $numFinished = 0;
            $nbLearners    = 0;
            $progress      = 0;
            $totalProgress = 0;
            foreach ($learnersProgress as $learnerProgress) {
                ++$nbLearners;
                $progress += $learnerProgress->getProgression();
                if ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $numNotStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $numStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_FINISHED) {
                    $numFinished++;
                }
            }

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $numDurations = $module->getDuration()->format('H:i:s');
                $totalDurations += $this->convertTimeToDecimal($numDurations);

                // calculation the avg time spent
                $realTimeSpentModule = 0;
                $numOfStatusCompleted = 0;
                $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                foreach ($moduleIterations as $iteration) {
                    $status = $iteration->getStatus() ? $iteration->getStatus()->getAppId() : null;
                    switch ($status) {
                        case Status::WITH_BOOKING_DONE:
                            $realTimeSpentModule += $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 0;
                            $numOfStatusCompleted++;
                            break;
                        default:
                            break;
                    }
                }
                $avgSpentTime += $numOfStatusCompleted > 0 ? $realTimeSpentModule/$numOfStatusCompleted : 0;
            }

            if ($nbLearners > 0) {
                $totalProgress = $progress / $nbLearners;
            }

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                ->getResultsByIntervention($intervention->getId());

            $nbNotation    = 0;
            $notation      = 0;
            $totalNotation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                ++$nbNotation;
                $notation += $learnerNotation->getNotation();
            }

            if ($nbNotation) {
                $totalNotation = $notation / $nbNotation;
            }

            $intervention->setLearnersNotation($totalNotation);
            $intervention->setLearnersProgress($totalProgress);
            $intervention->setNbLearner($nbLearners);
            $deleteForms[$intervention->getId()] = $this->createDeleteForm($intervention)->createView();
            $progressionLearners[$intervention->getId()] = [
                'numNotStarted' => $numNotStarted,
                'numStarted' => $numStarted,
                'numFinished' => $numFinished,
                KeyNameConst::KEY_NUM_DURATIONS => $this->secondsToTime($totalDurations*60),
                KeyNameConst::KEY_TOTAL_SPENT_TIME => $this->secondsToTime($avgSpentTime)
                ];
        }

        return $this->render('AdminBundle:Intervention:indexPublished.html.twig', [
            'interventions' => $interventions,
            'progressionLearners' => $progressionLearners,
            'deleteForms'   => $deleteForms,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()

        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexDraftAction(Request $request): Response
    {
        if ($this->getUser()->isRole('ROLE_SUPER_ADMIN')) {
            $interventions = $this->getDoctrine()->getRepository(InterventionStep::class)
                ->findAll();
        } else {
            $interventions = $this->getDoctrine()->getRepository(InterventionStep::class)
                ->findBy(['entity' => $this->getEntityFromProfile()]);
        }

        $durations  = array();
        $startDates = [];
        $endDates   = [];
        foreach ($interventions as $intervention) {
            $totalDurationMinutes = 0;
            $step1 = $intervention->getStep1();
            $step2 = $intervention->getStep2();
            if ($step2) {
                $modules = isset($step2['module']) ? $step2['module'] : null;
                if ($modules) {
                    foreach ($modules as $module) {
                        if (isset($module['durationHours'])) {
                            $totalDurationMinutes += $module['durationHours'] * 60;
                        }
                        if (isset($module['durationMinutes'])) {
                            $totalDurationMinutes += $module['durationMinutes'];
                        }
                    }
                }
            } else {
                $modules = isset($step1['module']) ? $step1['module'] : null;
                if ($modules) {
                    foreach ($modules as $module) {
                        if (isset($module['duration'])) {
                            $totalDurationMinutes += $module['time_param'] == 1 ? $module['duration'] * 60 : $module['duration'];
                        }
                    }
                }
            }
            $durations[$intervention->getId()]  = $this->secondsToTime($totalDurationMinutes*60);
            $startDates[$intervention->getId()] = $step1['intervention_start_date'] != "" ? \DateTime::createFromFormat('d/m/Y', $step1['intervention_start_date']) : "";
            $endDates[$intervention->getId()]   = $step1['intervention_end_date'] != "" ? \DateTime::createFromFormat('d/m/Y', $step1['intervention_end_date']) : "";
        }

        return $this->render('AdminBundle:Intervention:indexDraft.html.twig', [
            'interventions' => $interventions,
            'durations'     => $durations,
            'startDates'    => $startDates,
            'endDates'      => $endDates,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ]);
    }

    /**
     * @param $sessionId
     *
     * @return RedirectResponse
     *
     * @throws IncorrectArgumentException
     */
    public function duplicateDraftAction($sessionId)
    {
        // Base app path
        $base_path = $this->container->getParameter('kernel.root_dir') . '/../';
        // Real path previously saved
        $path = $base_path . 'web/files/InterventionFiles/Temp/';

        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
        /** @var InterventionStep $interventionStep */
        $interventionStep = $interventionStep->findOneBy(['sessionId' => $sessionId]);

        $step1 = $interventionStep->getStep1();
        $step2 = $interventionStep->getStep2();

        $mapping = [];
        if ($step1) {
            $step1['title'] = $step1['title'] . ' (2)';
            $modules = isset($step2['module']) ? $step1['module'] : null;
            if ($modules) {
                foreach ($modules as $k => $module) {
                    $mapping[$k] = uniqid();
                    $step1['module'][$mapping[$k]] = $module;
                    unset($step1['module'][$k]);
                }
            }
        }

        if ($step2) {
            $modules = isset($step2['module']) ? $step2['module'] : null;
            if ($modules) {
                foreach ($modules as $k => $module) {
                    $newKey = isset($mapping[$k]) ? $mapping[$k] : uniqid();
                    if(!empty($module['session'])){
                        foreach ($module['session'] as &$s){
                            if(isset($s['startDate'])){
                                $s['startDate'] = NULL;
                            }
                        }
                    }
                    $step2['module'][$newKey] = $module;
                    unset($step2['module'][$k]);
                }
            }
        }

        // copy the logo
        if (file_exists($step1['storedLogo'])) {
            $pathLogo = $base_path . 'web/files/InterventionFiles/Logo/'.date("Ymdhis");
            echo $pathLogo;
            // create new logo path
            mkdir($pathLogo);
            mkdir($pathLogo.'/logo');
            // copy the logo
            if (file_exists($pathLogo . '/logo')){
                $newFile = $pathLogo.'/logo/'.$step1['logoName'];
                copy($step1['storedLogo'], $newFile);
            }
        }

        // sync folder of the modules
        if ($mapping) {
            foreach ($mapping as $k => $map) {
                if (file_exists($path . $k)){
                    $src = $path . $k;
                    $dest = $path . $map;
                    shell_exec("cp -r $src $dest");
                }
            }
        }

        $newInterventionStep = new InterventionStep();
        $newInterventionStep->setSessionId();
        $newInterventionStep->setStep1($step1);
        $newInterventionStep->setStep2($step2);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($newInterventionStep);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_interventions_step_storage', ['stepNr' => 0, 'sessionId' => $newInterventionStep->getSessionId()] ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexEndedAction(Request $request): Response
    {
        $endedStatuses = $this->getDoctrine()->getRepository(Status::class)->findBy([
            'appId' => Status::INTERVENTION_FINISHED,
        ]);

        if ($this->getUser()->isRole('ROLE_SUPER_ADMIN')) {
            $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
                ->findBy(
                    [
                        'status' => $endedStatuses,
                    ],
                    [
                        'id' => 'DESC',
                    ]
                );
        } else {
            $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
                ->findBy(
                    [
                        'entity'  => $this->getEntityFromProfile(),
                        'status' => $endedStatuses,
                    ],
                    [
                        'id' => 'DESC',
                    ]
                );
        }

        $progressionLearners = array();
        $deleteForms   = array();

        foreach ($interventions as $intervention) {
            $learnersProgress = $this->getDoctrine()->getRepository(LearnerIntervention::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $modules = $this->getDoctrine()->getRepository(Module::class)
                ->findBy([
                    'intervention' => $intervention->getId(),
                ]);

            $totalDurations = 0;
            $avgSpentTime = 0;
            $numNotStarted = $numStarted = $numFinished = 0;
            $nbLearners    = 0;
            $progress      = 0;
            $totalProgress = 0;
            foreach ($learnersProgress as $learnerProgress) {
                ++$nbLearners;
                $progress += $learnerProgress->getProgression();
                if ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $numNotStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $numStarted++;
                } elseif ($learnerProgress->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_FINISHED) {
                    $numFinished++;
                }
            }

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $numDurations = $module->getDuration()->format('H:i:s');
                $totalDurations += $this->convertTimeToDecimal($numDurations);

                // calculation the avg time spent
                $realTimeSpentModule = 0;
                $numOfStatusCompleted = 0;
                $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                foreach ($moduleIterations as $iteration) {
                    $status = $iteration->getStatus() ? $iteration->getStatus()->getAppId() : null;
                    switch ($status) {
                        case Status::WITH_BOOKING_DONE:
                            $realTimeSpentModule += $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 0;
                            $numOfStatusCompleted++;
                            break;
                        default:
                            break;
                    }
                }
                $avgSpentTime += $numOfStatusCompleted > 0 ? $realTimeSpentModule/$numOfStatusCompleted : 0;
            }

            if ($nbLearners > 0) {
                $totalProgress = $progress / $nbLearners;
            }

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                ->getResultsByIntervention($intervention->getId());

            $nbNotation    = 0;
            $notation      = 0;
            $totalNotation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                ++$nbNotation;
                $notation += $learnerNotation->getNotation();
            }

            if ($nbNotation) {
                $totalNotation = $notation / $nbNotation;
            }

            $intervention->setLearnersNotation($totalNotation);
            $intervention->setLearnersProgress($totalProgress);
            $intervention->setNbLearner($nbLearners);
            $deleteForms[$intervention->getId()] = $this->createDeleteForm($intervention)->createView();
            $progressionLearners[$intervention->getId()] = [
                'numNotStarted' => $numNotStarted,
                'numStarted' => $numStarted,
                'numFinished' => $numFinished,
                KeyNameConst::KEY_NUM_DURATIONS => $this->secondsToTime($totalDurations*60),
                KeyNameConst::KEY_TOTAL_SPENT_TIME => $this->secondsToTime($avgSpentTime)
            ];
        }

        return $this->render('AdminBundle:Intervention:indexEnded.html.twig', [
            'interventions' => $interventions,
            'progressionLearners' => $progressionLearners,
            'deleteForms'   => $deleteForms,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ]);
    }

    /**
     * Creates a form to delete a intervention entity.
     *
     * @param Intervention $intervention The profile entity
     *
     * @return FormInterface
     */
    private function createDeleteForm(Intervention $intervention)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_interventions_delete', array('id' => $intervention->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a intervention entity.
     *
     * @param Request $request
     * @param Intervention $intervention
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Intervention $intervention)
    {
        $form = $this->createDeleteForm($intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($intervention);
            $em->flush();
        }

        return $this->redirectToRoute('admin_interventions_index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function assignQuizAction(Request $request)
    {
        $moduleId       = $request->request->get('moduleId');
        $quizId         = $request->request->get('quizId');
        $interventionId = $request->request->get('interventionId');
        $entityManager  = $this->getDoctrine()->getManager();
        $quiz           = $entityManager->getRepository('ApiBundle:Quizes')->find($quizId);
        $module         = $entityManager->getRepository('ApiBundle:Module')->find($moduleId);
        $module->setQuiz($quiz);
        $entityManager->persist($module);
        $entityManager->flush();

        return $this->redirectToRoute('admin_interventions_show', array('id' => $interventionId));
    }

    /**
     * @param Request $request
     * @param UploadFile $fileApi
     * @return RedirectResponse
     */
    public function addTrainerReportAction(Request $request, UploadFile $fileApi)
    {
        $moduleId                      = $request->request->get('moduleId');
        $interventionId                = $request->request->get('interventionId');
        $entityManager                 = $this->getDoctrine()->getManager();
        $module                        = $entityManager->getRepository('ApiBundle:Module')->find($moduleId);
        $fileUpload                    = new FileRetrieval();
        $data['trainerReportTemplate'] = [];
        if (!empty($fileUpload->getFiles(0, 'trainerReportTemplate'))) {
            $tmpContent                    = $fileUpload->getFiles(0, 'trainerReportTemplate');
            $data['trainerReportTemplate'] = $tmpContent;
        }
        $filesMedia    = $data['trainerReportTemplate'];
        $this->fileApi = $fileApi;
        foreach ($filesMedia as $media) {
            $moduleTemplate = new ModuleTemplate();
            $fileDescriptor = $this->fileApi->saveTempFile($media['file']);
            $moduleTemplate->setFileDescriptor($fileDescriptor);
            $moduleTemplate->setAppId(ModuleTemplate::INDIVIDUAL_REPORT_TEMPLATE);
            $moduleTemplate->setModule($module);
            $entityManager->persist($moduleTemplate);

        }
        $entityManager->persist($module);
        $entityManager->flush();

        return $this->redirectToRoute('admin_interventions_show', array('id' => $interventionId));
    }

    private function getModuleRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Module');
    }

    /**
     * @param int $id
     */
    private function validateCourse(int $id)
    {
        if (!$intervention = $this->getDoctrine()->getRepository(Intervention::class)->findOneBy(['id' => $id])) {
            $this->addFlash('warning', 'Course not found');

            return $this->redirectToRoute('admin_interventions_index');
        }

        return $intervention;
    }

    private function getSessionsModule($moduleId)
    {
        $module = $this->getModuleRepository()->find($moduleId);
        $moduleSessions = $module->getSessions();
        $sessions = [];
        if($moduleSessions){
            foreach ($moduleSessions as $key => $moduleSession){
                $start = $moduleSession->getSession()->getSessionDate();
                $sessions[$key]["sessionId"] = $moduleSession->getId();
                $sessions[$key]["startDate"]         = $start->format('d/m/Y');
                $sessions[$key]["startDateTime"]         = $start;
                $sessions[$key]["startTime"]         = $start->format('G:i');
                $sessions[$key]['session']         = $moduleSession->getSession();
                $end = clone $start;

                $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));

                $sessions[$key]["endTime"] = $end->format('G:i');
                $sessions[$key]["address"] = "";
                $sessions[$key]["totalLearners"] = 0;
                $sessions[$key]["trainers"] = [];

                if ($moduleSession->getSessionPlace()) {
                    $sessions[$key]["address"] = $moduleSession->getSessionPlace()->getAddress() . ' - ' . $moduleSession->getSessionPlace()->getPlace();
                }

                if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                    $bookingAgendaByModuleSession = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                        'module'  => $module,
                        'moduleSession' => $moduleSession,
                    ));
                    $assigment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findOneBy(['module' => $module, 'moduleSession' => $moduleSession]);
                    if($assigment){
                        $trainer = $assigment->getLiveResource();
                        if($trainer) {
                            $sessions[$key]["trainers"] = [$trainer->getFirstName() . ' ' . $trainer->getLastName()];
                        }
                    }
                    if ($bookingAgendaByModuleSession) {
                        $sessions[$key]["totalLearners"] = count($bookingAgendaByModuleSession);
                    }

                }
            }
        }

        return $sessions;
    }

    /**
     * Finds and displays sessions of course.
     * @param int $id
     * @param TrainerBookingService $trainerBookingService
     * @return RedirectResponse|Response
     */
    public function showCourseSessionsAction(int $id, TrainerBookingService $trainerBookingService)
    {
        $intervention = $this->validateCourse($id);
        $modules = $intervention->getModules();
        $modulesData = [];
        $trainersWithHour = [];

        foreach ($modules as $module){
            if(!in_array($module->getStoredModule()->getAppId(), [
                StoredModule::ONLINE_WORKSHOP, StoredModule::VIRTUAL_CLASS, StoredModule::PRESENTATION_ANIMATION, StoredModule::ONLINE
            ])){
                continue;
            }
            $moduleData = $this->getModuleData($module);
            if ($module->getStoredModule()->getAppId() === StoredModule::ONLINE){
                $startDate = \DateTime::createFromFormat('d/m/Y', $module->getBeginning()->format('d/m/Y'), UserTimeZone::getTimeZoneObject());
                $endDate = \DateTime::createFromFormat('d/m/Y', $module->getEnding()->format('d/m/Y'), UserTimeZone::getTimeZoneObject());
                $moduleData['trainers'] = [];
                if ($module->getAssigments()){
                    foreach ($module->getAssigments() as $assigment){
                        if($assigment->getLiveResource()) {
                            $trainer = $assigment->getLiveResource();
                            $trainerHour = $trainerBookingService->getTrainerhours($trainer, $startDate, $endDate);
                            $trainersWithHour[$trainer->getId()] = $trainerHour;
                            //$ranking = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($trainer);
                            $moduleData['trainers'][] = ['trainer' => $trainer, 'ranking' => $trainerHour['ranking']];
                        }
                    }
                }
            }else{
                $moduleData['sessions'] = $this->getSessionsModule($module->getId());
            }
            $modulesData[] = $moduleData;
        }

        return $this->render('AdminBundle:Intervention:sessions.html.twig', array(
            'intervention' => $intervention,
            'modules' => $modulesData,
            'trainerHours' => $trainersWithHour
        ));

    }

    /**
     * Finds and displays sessions of module.
     * @param int $id
     * @param int $moduleId
     * @return RedirectResponse|Response
     */
    public function showSessionsModuleAction(int $id, int $moduleId)
    {
        $intervention = $this->validateCourse($id);

        $module = $this->getModuleRepository()->find($moduleId);
        $moduleData = $this->getModuleData($module);
        $moduleData['sessions'] = $this->getSessionsModule($moduleId);

        return $this->render('AdminBundle:Intervention:sessions.html.twig', array(
            'intervention' => $intervention,
            'modules' => [$moduleData]
        ));
    }

    private function getModuleData($module)
    {
        $moduleData = [];
        if($module){
            $moduleData['id'] = $module->getId();
            $moduleData['name'] = $module->getDesignation();
            $moduleData['description'] = $module->getDescription();
            $moduleData['storedModuleAppId'] = $module->getStoredModule()->getAppId();
            $moduleData['quantity'] = $module->getQuantity();
            $moduleData['alert'] = $module->getStatus() ? $module->getStatus()->getId() : null;
            $numDurations = $module->getDuration()->format('H:i:s');
            $moduleData['duration'] = $this->convertTimeToDecimal($numDurations);
            if ($module->getSkills()) {
                $skills = [];
                foreach ($module->getSkills() as $key => $skill) {
                    $skills[$key] = $skill->getDesignation();
                }
                $moduleData['skills'] = $skills;
            }

        }

        return $moduleData;
    }

    /**
     * Finds and displays trainers in sessions.
     * @param int $id
     * @param int $moduleId
     * @return RedirectResponse|Response
     */
    public function showSessionsTrainersAction(int $id, int $moduleId, TrainerBookingService $trainerBookingService)
    {
        $intervention = $this->validateCourse($id);
        $module = $this->getModuleRepository()->find($moduleId);
        $moduleData = [];
        if($module){
            $moduleData['id'] = $module->getId();
            $moduleData['name'] = $module->getDesignation();
            $moduleData['quantity'] = $module->getQuantity();
            $moduleData['storedModuleAppId'] = $module->getStoredModule()->getAppId();
        }

        $profileType = $this->em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );

        $conditionArr = array(
            'profileType' => $profileType,
        );

        $roles = $this->getUser()->getRoles();
        if (!in_array('ROLE_SUPER_ADMIN', $roles)) {
            $conditionArr['entity'] = $this->getEntityFromProfile()->getId();
        }

        $trainers = $this->em->getRepository('ApiBundle:Profile')->findBy($conditionArr);

        $arrTrainer = [];
        foreach ($trainers as $trainer) {
            $activities = count($this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                ->findBy(['liveResource' => $trainer]));
            $trainer->setNbActivitiesExecuted($activities);
            $ranking      = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($trainer);
            $arrTrainer[] = array('ranking' => $ranking, 'trainer' => $trainer);
        }

        $sessions = $this->getSessionsModule($moduleId);

        return $this->render('AdminBundle:Intervention:sessions_trainers.html.twig', array(
            'trainers' => $trainerBookingService->getAvailableTrainersForWorkShopsModule($module),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession(),
            'intervention' => $intervention,
            'module'    => $moduleData,
            'sessions'  => $sessions
        ));
    }

    /**
     * Finds and displays learners in sessions.
     * @param int $id
     * @param int $moduleId
     * @return RedirectResponse|Response
     */
    public function showSessionsLearnersAction(int $id, int $moduleId)
    {
        $intervention = $this->validateCourse($id);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->em->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->em->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $this->getEntityFromProfile()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        $module = $this->getModuleRepository()->find($moduleId);
        $moduleSessions = $module->getSessions();
        $sessions = [];
        $moduleData = [];
        if($module){
            $moduleData['id'] = $module->getId();
            $moduleData['name'] = $module->getDesignation();
            $moduleData['description'] = $module->getDescription();
            $moduleData['quantity'] = $module->getQuantity();
            $moduleData['storedModuleAppId'] = $module->getStoredModule()->getAppId();
            $moduleData['alert'] = $module->getStatus();
            $numDurations = $module->getDuration()->format('H:i:s');
            $moduleData['duration'] = $this->convertTimeToDecimal($numDurations);

        }
        $canBooking = $this->isAvailableModule($module);
        $learnersGroup = [];
        if($moduleSessions){
            foreach ($moduleSessions as $key => $moduleSession){
                $start = $moduleSession->getSession()->getSessionDate();
                $sessions[$key]["sessionId"] = $moduleSession->getId();
                $sessions[$key]["startDate"]         = $start->format('d/m/Y');
                $sessions[$key]["startTime"]         = $start->format('G:i');
                $sessions[$key]['session']           = $moduleSession->getSession();
                $sessions[$key]['canBooking']        = $canBooking && $this->isAvailableSessionBooking($moduleSession);
                $end = clone $start;

                $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));

                $sessions[$key]["endTime"] = $end->format('G:i');
                $sessions[$key]["address"] = "";
                $sessions[$key]["totalLearners"] = 0;
                $sessions[$key]["learners"] = [];

                if ($moduleSession->getSessionPlace()) {
                    $sessions[$key]["address"] = $moduleSession->getSessionPlace()->getAddress() . ' - ' . $moduleSession->getSessionPlace()->getPlace();
                }

                if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                    $bookingAgendaByModuleSession = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                        'module'  => $module,
                        'moduleSession' => $moduleSession,
                    ));
                    if ($bookingAgendaByModuleSession) {
                        $learners = 0;
                        $learnerIds = [];
                        foreach ($bookingAgendaByModuleSession as $booking){
                            $learner = $booking->getLearner()->getPerson();
                            $lgroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getId()));
                            $groupNames = [];
                            if ($lgroups) {
                                foreach ($lgroups as $learnerGroup) {
                                    $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                                }
                            }
                            $groupName  = implode(", ", $groupNames);
                            $learnerIds[] = $learner->getId();
                            $learnersGroup[$learner->getId()] = $groupName;
                            $learners++;
                        }
                        $sessions[$key]["totalLearners"] = $learners;
                        $profiles = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array(
                            'person' => $learnerIds,
                        ));
                        $sessions[$key]["learners"] = $profiles;
                    }

                }
            }
        }

        return $this->render('AdminBundle:Intervention:sessions_learners.html.twig', array(
            'intervention' => $intervention,
            'module' => $moduleData,
            'sessions'=> $sessions,
            'searchParam' => $searchParam,
            'learnersGroup' => $learnersGroup
        ));
    }

    /**
     * Finds and displays a intervention entity.
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function showAction(int $id)
    {
        $intervention = $this->validateCourse($id);
        // call the AdminStats service
        $adminStatsController = $this->get('AdminStats');

        $deleteForm      = $this->createDeleteForm($intervention);
        $isAssignLearner = 1;
        $totalDurations  = 0;
        //$modules         = $intervention->getModules();
        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findModulesByIntervention($intervention->getId());
        $data            = [];
        $numLearner      = count($intervention->getLearners());
        $checkAssessment = 0;
        $moduleAssessment  = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'intervention' => $intervention,
            'storedModule' => StoredModule::ASSESSMENT,
        ));
        $existSessions = 0;
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }

            if (in_array($module->getStoredModule()->getAppId(), [StoredModule::ONLINE_WORKSHOP, StoredModule::VIRTUAL_CLASS, StoredModule::PRESENTATION_ANIMATION])){
                $existSessions = 1;
            }

            $progress = $alert = $rating = 0;
            $targetEvaluation = 'N/A';
            $realTime         = 0;
            $evaluation       = 0;
            if ($module->getStoredModule()->getAppId() == StoredModule::QUIZ) {
                $targetEvaluation = $module->getScoreQuiz() ? $module->getScoreQuiz() : $targetEvaluation;
                if (!$module->getQuiz()) {
                    $isAssignLearner = 0;
                    $alert = 1;
                }
            }
            if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                $bookingAgendaByModule = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                    'module' => $module,
                    'status' => Status::DONE,
                ));
                if ($bookingAgendaByModule) {
                    foreach ($bookingAgendaByModule as $booking) {
                        $grade = $booking->getLearnerGrade();
                        if ($grade) {
                            $evaluation += $booking->getLearnerGrade();
                        }
                    }
                    $evaluation = $evaluation / count($bookingAgendaByModule);
                }

                $assigments = $module->getAssigments();

                if ($assigments) {
                    $isAssignLearner = 0;
                    $alert = 1;
                    foreach ($assigments as $assigment) {
                        if ($assigment->getLiveResource()) {
                            $isAssignLearner = 1;
                            $alert = 0;
                            break;
                        }
                    }
                }

            }

            $moduleDuration = $module->getDuration();
            $numDurations = $module->getDuration()->format('H:i:s');
            $totalDurations += $this->convertTimeToDecimal($numDurations);
            $evaluation     = $evaluation == 0 ? 'N/A' : $evaluation;

            $moduleNotations = $this->getDoctrine()->getRepository(ModuleResponse::class)->findBy(array('module' => $module));

            foreach ($moduleNotations as $notation) {
                if ($notation->getNotation()) {
                    $rating += $notation->getNotation();
                }
            }

            $rating            = $rating != 0 ? ($rating / count($moduleNotations)) : 0;
            $iterationBooked   = 0;
            $iterationStarted  = 0;
            $iterationComleted = 0;
            $iterations        = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findBy(array('module' => $module));
            $num               = 0;

            foreach ($iterations as $iteration) {
                $status = $iteration->getStatus() ? $iteration->getStatus()->getId() : null;
                if ($status == Status::BOOKED) {
                    $iterationBooked++;
                } elseif ($status == Status::IN_PROGRESS) {
                    $iterationStarted++;
                } elseif ($status == Status::DONE) {
                    $iterationComleted++;
                }
                $time = $iteration->getLearnedTime();
                if ($time) {
                    $realTime += $time;
                    $num++;
                }
            }
            $realTime = $realTime == 0 ? 'N/A' : ($realTime / $num);
            if ($numLearner != 0) {
                $progress = ($iterationComleted * 100) / $numLearner;
            }

            $learnerStatus    = $adminStatsController->getLearnerStatusByModule($module, $numLearner);
            $scoreArr         = $adminStatsController->getMinScoreByModule($module);


            $hasAssessment = $this->getDoctrine()->getRepository('ApiBundle:ModuleAssessments')
                ->findOneBy(array(
                    'assessment' => $module,
                    'module'  => $moduleAssessment,
                ));

            if ($checkAssessment == 0 && $hasAssessment) {
                $checkAssessment = 1;
            }
            $dataRow = array(
                'hasAssessment'    => $hasAssessment ? 1 : 0,
                'templates'        => $module->getModuleTemplates(),
                'id'               => $module->getId(),
                'name'             => $module->getDesignation(),
                'type'             => $module->getStoredModule()->getDesignation(),
                'beginning'        => $module->getBeginning(),
                'ending'           => $module->getEnding(),
                'duration'         => $moduleDuration,
                'numBooked'        => $iterationBooked,
                'numStarted'       => $iterationStarted,
                'numCompleted'     => $iterationComleted,
                'targetEvaluation' => $targetEvaluation,
                'evaluation'       => $evaluation,
                'progress'         => $progress,
                'rating'           => $rating,
                'typeId'           => $module->getStoredModule()->getAppId(),
                'quiz'             => $module->getQuiz(),
                'isNeedReport'     => $module->getIsNeedReport(),
            );

            if ($module->getIsNeedReport() == 1 && $module->getModuleTemplates()->isEmpty()) {
                $alert = 1;
            }
            if ($alert == 0) {
                $alert = $learnerStatus['progressing'] == 100 ? 2 : 3;
            }
            $dataRow['alert'] = $alert;
            $data[] = array_merge($dataRow, $learnerStatus, $scoreArr);
        }

        $quizzes              = $this->container->get('doctrine')->getRepository(Quizes::class)->findAll();
        $dataLearner          = [];
        $numNotStarted = $numStarted = $numFinished = 0;
        $learnersIntervention = $intervention->getLearners();
        foreach ($learnersIntervention as $learner) {
            $dataLearner[] = array("learner" => $learner->getLearner());
            if ($learner->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                $numNotStarted++;
            } elseif ($learner->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                $numStarted++;
            } elseif ($learner->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_FINISHED) {
                $numFinished++;
            }
        }

        $learnersProgress = $this->getDoctrine()->getRepository(LearnerIntervention::class)
            ->findBy([
                'intervention' => $intervention->getId(),
            ]);

        $nbLearners    = 0;
        $progress      = 0;
        $totalProgress = 0;
        foreach ($learnersProgress as $learnerProgress) {
            ++$nbLearners;
            $progress += $learnerProgress->getProgression();
        }

        if ($nbLearners > 0) {
            $totalProgress = $progress / $nbLearners;
        }

        $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
            ->getResultsByIntervention($intervention->getId());

        $nbNotation    = 0;
        $notation      = 0;
        $totalNotation = 0;
        foreach ($learnersNotation as $learnerNotation) {
            ++$nbNotation;
            $notation += $learnerNotation->getNotation();
        }

        if ($nbNotation) {
            $totalNotation = $notation / $nbNotation;
        }

        $intervention->setLearnersNotation($totalNotation);
        $intervention->setLearnersProgress($totalProgress);


        return $this->render('AdminBundle:Intervention:show.html.twig', array(
            'moduleAssessment' => $moduleAssessment,
            'checkAssessment'=> $checkAssessment,
            'dataLearner'     => $dataLearner,
            'intervention'    => $intervention,
            'isAssignLearner' => 1,
            'delete_form'     => $deleteForm->createView(),
            'quizzes'         => $quizzes,
            'data'            => $data,
            'modules'         => $modules,
            'totalHours'      => $totalDurations > 0 ? number_format($totalDurations/60, 2, '.', ',') : '',
            KeyNameConst::KEY_NUM_OF_LEARNERS => $numLearner,
            KeyNameConst::KEY_NUM_OF_NOT_STARTED => $numNotStarted,
            KeyNameConst::KEY_NUM_OF_STARTED => $numStarted,
            KeyNameConst::KEY_NUM_OF_FINISHED => $numFinished,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession(),
            'existSessions' => $existSessions
        ));
    }

    public function showCourseLearnersAction(int $id)
    {

        $intervention = $this->validateCourse($id);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->em->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->em->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $this->getEntityFromProfile()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        $learners = [];
        $learnersIntervention = $intervention->getLearners();
        $learnersGroup = [];
        foreach ($learnersIntervention as $learner) {
            $learners[] = $learner->getLearner();
            $lgroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getLearner()->getId()));
            $groupNames = [];
            if ($lgroups) {
                foreach ($lgroups as $learnerGroup) {
                    $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                }
            }
            $groupName  = implode(", ", $groupNames);
            $learnersGroup[$learner->getLearner()->getId()] = $groupName;
        }
        return $this->render('AdminBundle:Intervention:learners.html.twig', [
            'intervention' => $intervention,
            'learners' => $learners,
            'learnersGroup' => $learnersGroup,
            'searchParam' => $searchParam
        ]);
    }

    /**
     * @return Response
     */
    public function newAction()
    {
        $form = $this->createFormBuilder()
            ->add('Submit', SubmitType::class, array(
                'attr' => [
                    'class' => 'btn-validation-intervention',
                    'style' => 'display:none',
                ],
            ))
            ->getForm();

        return $this->render('AdminBundle:Intervention:main.html.twig', array(
            'form' => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * @param Request $request
     * @param LoggerInterface $logger
     * @param SaveStepService $saveStepService
     * @param InterventionStepConsistentService $interventionStepConsistentService
     *
     * @return RedirectResponse
     *
     * @throws IncorrectArgumentException
     */
    public function saveStepAction(
        Request $request,
        LoggerInterface $logger,
        SaveStepService $saveStepService,
        InterventionStepConsistentService $interventionStepConsistentService
    ) {
        $request     = $request->request->all();
        $currentStep = $request['step'];

        $remake = false;
        if (isset($request['remake'])) {
            $remake = 'yes' == $request['remake'];
        }

        if (!isset($currentStep)) {
            $logger->error('missing current step param in request', ['request' => $request]);

            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('danger', 'Intervention system error occoured. Please try again or contact IT.');

            return $this->redirectToRoute('admin_interventions_new');
        }

        if ($currentStep == 1) {
            $session = $this->get('session');
            $session->set('intervention', array(
                'participantMax' => $request['participant'],
            ));
        }
        $saveStepService->setRequest($request);
        $saveStepService->setCurrentUser($this->getUser());
        $saveStepService->save();

        $unpublishedSession = $saveStepService->getUnpublishedSession();

        $request['step_serialize_id'] = $saveStepService->getId();
        $request['session_unique_id'] = $saveStepService->getSessionUniqueId();

        if (2 == $request['step'] && $unpublishedSession) {
            $interventionStepConsistentService->makeStepConsistent($unpublishedSession);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($unpublishedSession);
            $entityManager->flush();
        }

        /*
         *  If the user will go back to step one and amend his/hers choices of
         *  modules. For now the need is only for step one
         */

        if ($remake) {
            try {
                $unpublishedSessionArray = $unpublishedSession->__toArray();
                $stepSwap                = new ValueSwap($unpublishedSessionArray, $request);
                $stepSwap->updateValues();

                $saveStepService->saveStepNr($stepSwap->getModifiedStep(), $stepSwap->getMofidiedContent());

                return $this->redirectToRoute('admin_interventions_step_storage',
                    [
                        'stepNr'    => ValueSwap::SESSION_NR,
                        'sessionId' => $saveStepService->getSessionUniqueId(),
                    ]);
            } catch (InterventionBaseException $exception) {
                $logger->error($exception->getMessage(), ['exception' => $exception]);

                $flashbag = $this->get('session')->getFlashBag();
                $flashbag->add('error', 'We are unable process this intervention. 
                          Try again later. If the error persists, please contact IT. ');

                return $this->redirect($this->generateUrl('admin_interventions_index'));
            }
        }

        // if at the step 3 there  the Online module then will redirect the step 3
        if ($currentStep == 2) {
            $redirectToStep3 = true;
            $stepTwo         = $unpublishedSession->getStep2();
            if ($stepTwo['module']) {
                foreach ($stepTwo['module'] as $module) {
                    if ($module['appId'] == '6') {
                        $redirectToStep3 = false;
                        break;
                    }
                }
            }
            if ($redirectToStep3) {
                return $this->redirectToRoute('admin_interventions_step',
                    ['stepNr' => $currentStep + 1, 'sessionNr' => $saveStepService->getSessionUniqueId()]);
            }
        }

        return $this->redirectToRoute('admin_interventions_step',
            ['stepNr' => $currentStep, 'sessionNr' => $saveStepService->getSessionUniqueId()]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function editAction(int $id, Request $request)
    {
        $now = new \DateTime('now');
        $course = $this->em->getRepository(Intervention::class)->find($id);
        if ($request->getMethod() == 'POST') {
            foreach ($request->get('course') as $fieldName => $value) {
                $dateTime = \DateTime::createFromFormat('m/d/Y', $value);

                if ($dateTime !== false) {
                    $value = $dateTime;
                }

                $method = 'set' . ucfirst($fieldName);
                if (!method_exists($course, $method)) {
                    continue;
                }

                call_user_func_array([$course, $method], [$value]);

                $this->getDoctrine()->getManager()->persist($course);
                $this->getDoctrine()->getManager()->flush();
            }

            foreach ($request->get('module') as $moduleId => $data) {
                $moduleEntity = $this->em->getRepository(Module::class)->find($moduleId);
                $hours = $minutes = 0;
                foreach ($data as $fieldName => $value) {
                    $dateTime = \DateTime::createFromFormat('m/d/Y', $value);
                    if ($dateTime !== false) {
                        $value = $dateTime;
                    }
                    $method = 'set' . ucfirst($fieldName);

                    if (!method_exists($moduleEntity, $method) && $fieldName!='durationHours' && $fieldName!='durationMinutes') {
                        continue;
                    }
                    if ($fieldName == 'durationHours') {
                        $hours      = $value;
                    } else if ($fieldName == 'durationMinutes') {
                        $minutes    = $value;
                    } else {
                        call_user_func_array([$moduleEntity, $method], [$value]);
                    }
                }
                $moduleEntity->setDuration(clone $now->setTime($hours, $minutes));
                $this->getDoctrine()->getManager()->persist($moduleEntity);
            }

            foreach ($request->files->get('module') as $moduleId => $file) {
                /**
                 * @var UploadedFile $file
                 */
                $dir = realpath($this->get('kernel')->getRootDir() . '/../web/files');

                $file = $file['file'];
                if ($file) {
                    $fileDescriptor = new FileDescriptor();
                    $fileDescriptor->setCreated(new \DateTime());
                    $fileDescriptor->setDirectory($dir);
                    $fileDescriptor->setMimeType($file->getMimeType());
                    $fileDescriptor->setSize(filesize($file->getPath()));
                    $fileDescriptor->setName(strip_tags($request->get('module')[$moduleId]['description']));
                    $fileDescriptor->setPath($file->getFilename());
                    $file->move($dir, $file->getFilename());
                    $this->getDoctrine()->getManager()->persist($fileDescriptor);
                    $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

                    switch ($mimeType) {
                        case 'audio':
                            $appId = EducationalDocVariety::AUDIO;
                            break;
                        case 'video':
                            $appId = EducationalDocVariety::VIDEO;
                            break;
                        default:
                            $appId = EducationalDocVariety::DOCUMENT;
                            break;
                    }

                    $eduDocument = new EducationalDocument();
                    $eduDocument->setFileDescriptor($fileDescriptor);
                    $eduDocument->setCreated(new \DateTime());
                    $eduDocument->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')
                        ->findOneBy(array('appId' => $appId)));
                    $this->getDoctrine()->getManager()->persist($eduDocument);

                    $moduleEntity = $this->getDoctrine()->getManager()->getRepository(Module::class)->find($moduleId);
                    $moduleEntity->setMediaDocument($eduDocument);
                    $this->getDoctrine()->getManager()->persist($moduleEntity);
                }

            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Update course succeed');
        }

        $courseForm = Yaml::parse(file_get_contents(__DIR__ . '/Forms/intervention.yml'));
        foreach ($courseForm['form']['fields'] as $fieldName => $field) {
            $method = 'get' . ucfirst($fieldName);
            if (!method_exists($course, $method)) {
                continue;
            }
            $courseForm['form']['fields'][$fieldName]['value'] = call_user_func([$course, $method]);
        }

        $moduleForm = Yaml::parse(file_get_contents(__DIR__ . '/Forms/module.yml'));
        if (!$modules = $this->em->getRepository(Module::class)->findBy(['intervention' => $course])) {
            $this->addFlash('warning', 'Course not found');

            return $this->redirectToRoute('admin_interventions_index');
        }

        $moduleForms = [];
        foreach ($modules as $module) {
            foreach ($moduleForm['form']['fields'] as $fieldName => $field) {
                $method = 'get' . ucfirst($fieldName);
                if (!method_exists($module, $method) && $fieldName!='durationHours' && $fieldName!='durationMinutes') {
                    continue;
                }
                if ($fieldName == 'durationHours') {
                    $method = 'get' . ucfirst('duration');
                    $duration = clone call_user_func([$module, $method]);
                    $hours      = intval($duration->format('H'));
                    $moduleForm['form']['fields'][$fieldName]['value'] = $hours;
                } else if ($fieldName == 'durationMinutes') {
                    $method = 'get' . ucfirst('duration');
                    $duration = clone call_user_func([$module, $method]);
                    $minutes    = intval($duration->format('i'));
                    $moduleForm['form']['fields'][$fieldName]['value'] = $minutes;
                } else {
                    $moduleForm['form']['fields'][$fieldName]['value'] = call_user_func([$module, $method]);
                }
            }
            $moduleForm['raw']             = $module;
            $moduleForms[$module->getId()] = $moduleForm;
        }

        return $this->render(
            'AdminBundle:Intervention:edit.html.twig',
            [
                'id'      => $course->getId(),
                'course'  => $courseForm['form']['fields'],
                'modules' => $moduleForms,
            ]
        );
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function editInformationAction(Request $request, UploadFile $fileApi)
    {
        $response = [];
        $format             = 'd/m/Y';
        $data = $request->request->all();
        $intervention = $this->em->getRepository(Intervention::class)->find($data['id']);
        if ($intervention) {
            $intervention->setDesignation($data['title'])
                ->setComment($data['description'])
                ->setBeginning(\DateTime::createFromFormat($format, $data['intervention_end_date']))
                ->setEnding(\DateTime::createFromFormat($format, $data['intervention_end_date']));
            $entityManager = $this->getDoctrine()->getManager();
            if ($data['storedLogo']) {
                $this->fileApi = $fileApi;
                $logo = $this->fileApi->saveTempFile(new \SplFileInfo($data['storedLogo']));
                $intervention->setLogo($logo);
            }
            $entityManager->persist($intervention);
            $entityManager->flush($intervention);
            $response = ['status' => 1, 'message' => 'ok'];
        }

        return new JsonResponse($response);
    }

    /**
     * @param int $stepNr
     * @param null $sessionNr
     * @return Response
     */
    public function renderStepAction(int $stepNr, $sessionNr = null)
    {
        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
        /**
         * @var InterventionStep $sessionEntity
         */
        $sessionEntity = $interventionStep->findOneBy(['sessionId' => $sessionNr]);
        $session       = $sessionEntity->__toArray();
        $step          = $session['step'][$stepNr];
        $modules       = isset($step['module']) ? $step['module'] : [];
        $sessionId     = $session['sessionId'];

        $validator = new ModuleValidator($session);
        $validator->validate();

        if (false == $validator->isEmpty()) {
            $validatorBag = $validator->getSerialziedValidationBag();
            $sessionEntity->setStepErrors($validatorBag);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sessionEntity);
            $entityManager->flush();

            $stepNr = (3 == $stepNr) ? 2 : $stepNr;
        }

        $values                             = $this->stepRenderer($stepNr, $modules, $sessionId, $session);
        $values['templateValues']['errors'] = $validator->getValidationBag();

        return $this->render($values['template'], $values['templateValues']);
    }

    /**
     * @param $stepNr
     * @param $modules
     * @param $sessionId
     * @param null $session
     * @return mixed
     */
    private function stepRenderer($stepNr, $modules, $sessionId, $session = null)
    {
        // Add more `steps` into $session for get values of all steps. dump($session) on twig template
        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
        /**
         * @var InterventionStep $sessionEntity
         */
        $sessionEntity    = $interventionStep->findOneBy(['sessionId' => $sessionId]);
        $sessionFromStep1 = $sessionEntity->__toArray();
        $session['steps'] = isset($sessionFromStep1['step']) ? $sessionFromStep1['step'] : null;
        // Add more `steps` into $session for get values of all steps. dump($session) on twig template

        $moduleValueCommand = new ModuleValueCommand();
        $moduleValueCommand->setStep($stepNr);
        $moduleValueCommand->setSessionId($sessionId);
        $moduleValueCommand->setModuleFactory(new ModuleValueFactory($this->container, $sessionId));
        $moduleValueCommand->setTemplateRenderer(new StepTemplateRender($this->container));
        $moduleValueCommand->setModules($modules);
        $moduleValueCommand->setSession($session);
        $moduleValueCommand->command();
        $views = $moduleValueCommand->getViewsWithValues();

        // Validation check

        return $views;
    }

    /**
     * @param $stepNr
     * @param $sessionId
     * @return Response
     */
    public function renderStepFromStorageAction($stepNr, $sessionId)
    {
        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
        $previousSession  = $interventionStep->findOneBy(['sessionId' => $sessionId]);
        $step             = [];

        if (!empty($previousSession)) {
            $session        = $previousSession->__toArray();
            $stepNrToRender = $stepNr + 1;

            $validator = new ModuleValidator($session);
            $validator->validate();

            if (false == $validator->isEmpty()) {
                $validatorBag = $validator->getSerialziedValidationBag();
                $previousSession->setStepErrors($validatorBag);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($previousSession);
                $entityManager->flush();

                if (4 == $stepNrToRender) {
                    $stepNrToRender = 2;
                    $stepNr         = 2;
                }
            }

            $step = $session['step'][$stepNrToRender];
        }

        $modules                            = isset($step['module']) ? $step['module'] : [];
        $values                             = $this->stepRenderer($stepNr, $modules, $sessionId, $step);
        $values['templateValues']['errors'] = $validator->getValidationBag();
        if (empty($values['templateValues']['modules'])) {
            $stepNr                             = 2;
            $modules                            = isset($step['module']) ? $step['module'] : [];
            $values                             = $this->stepRenderer($stepNr, $modules, $sessionId, $step);
            $values['templateValues']['errors'] = $validator->getValidationBag();
        }

        return $this->render($values['template'], $values['templateValues']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getContentStoreAction(Request $request)
    {
        if (empty($request->request->get('module_detail'))) {
            return new JsonResponse(Response::HTTP_BAD_REQUEST);
        }

        $request = $request->request->get('module_detail');

        $view = $this->renderView('AdminBundle:Intervention:step1-row.html.twig', [
            'id'           => $request['app_id'],
            'rowClick'     => $request['rowClick'],
            'color'        => $request['color'],
            'icon'         => $request['icon'],
            'title'        => $request['title'],
            'displayTitle' => $request['title'],
            'quantity'     => $request['quantity'],
            'width'        => $request['width'],
            'height'       => $request['height'],
            'index'        => $request['index'],
            'delete'       => false, // to be completed
        ]);

        return new JsonResponse($view);
    }

    /**
     * @param Request $request
     * @return JsonResponsesession.startDate
     */
    public function fileUploadAction(Request $request)
    {
        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('temporary');
            $uploader->saveFile();

            $filePath   = $uploader->getFilePath();
            $fileName   = $uploader->getFileName();
            $personRole = $uploader->getPersonRole();

            $response = ['filePath' => $filePath, 'fileName' => $fileName, 'personRole' => $personRole];
        } catch (IncorrectArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function mediaFileUploadAction(Request $request)
    {
        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('media');
            $uploader->saveFile();

            $response = 'ok';
        } catch (IncorrectArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        } catch (Exception $exception){
            return new JsonResponse($exception->getMessage(), 500);
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @param FileUploadDecorator $fileUploadDecorator
     * @return JsonResponse
     */
    public function getFileTemplateAction(Request $request, FileUploadDecorator $fileUploadDecorator)
    {
        $expectedKeys = ['module_id', 'person_role'];
        $requestData  = $request->request->get('data');

        $wrongPayload = false;
        foreach ($expectedKeys as $keyToCheck) {
            if (!isset($requestData[$keyToCheck])) {
                $wrongPayload = true;
                break;
            }
        }

        if (empty($requestData) || $wrongPayload) {
            return new JsonResponse('Incorrect request payload', 400);
        }

        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('temporary');
            $files             = $uploader->getFiles($requestData['module_id'], $requestData['person_role']);
        } catch (InterventionBaseException $exception) {
            return new JsonResponse($exception->getMessage(), 400);
        }

        $fileUploadDecorator->decorate($files, $requestData['person_role']);
        $viewToReturn = $fileUploadDecorator->getDecorated();

        return new JsonResponse($viewToReturn);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteFileAction(Request $request)
    {
        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('temporary');
            $uploader->deleteFile();
        } catch (InterventionBaseException $exception) {
            return new JsonResponse($exception->getMessage(), 400);
        }

        return new JsonResponse('ok');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteLogoAction(Request $request)
    {
        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('logo');
            $uploader->deleteFile();

            $sessionHandler = new SessionHandler();
            $sessionHandler->isSession();
            $session = $sessionHandler->getSession();

            if (!is_array($session)) {
                return new JsonResponse('ok');
            }

            if (isset($session['step'][1]['storedLogo'])) {
                unset($session['step'][1]['storedLogo']);

                $sessionHttp = new Session();
                $sessionHttp->remove('intervention');
                $sessionHttp->set('intervention', $session);
            }
        } catch (InterventionBaseException $exception) {
            return new JsonResponse($exception->getMessage(), 400);
        }

        return new JsonResponse('ok');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logoUploadAction(Request $request)
    {
        try {
            $fileUploadFactory = new FileUploadFactory($request);
            $uploader          = $fileUploadFactory->get('logo');
            $uploader->saveFile();

            $filePath   = $uploader->getFilePath();
            $fileName   = $uploader->getFileName();
            $personRole = $uploader->getPersonRole();

            $response = ['filePath' => $filePath, 'fileName' => $fileName, 'personRole' => $personRole];
        } catch (IncorrectArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        }

        return new JsonResponse($response);
    }

    /**
     * @param $sessionId
     * @param TrainerBookingService $trainerBooking
     */
    public function getTrainersForModuleAction($sessionId, TrainerBookingService $trainerBooking)
    {
        /** @var InterventionStep $interventionStep */
        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class)->findOneBy(['sessionId' => $sessionId]);
        $trainerBooking->getAvailableTrainers($interventionStep);
    }

    /**
     * @param $sessionId
     *
     * @return RedirectResponse
     *
     * @throws IncorrectArgumentException
     */
    public function saveInterventionAction($sessionId, InterventionPersist $interventionPersist)
    {
        $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
        /** @var InterventionStep $previousSession */
        $previousSession = $interventionStep->findOneBy(['sessionId' => $sessionId]);

        /** @var Profile $admin */
        $admin = $this->getDoctrine()->getRepository(Profile::class)
            ->findOneBy(
                [
                    'person' => $this->getUser()->getId(),
                ]
            );

        $interventionPersist->setCombinedSteps($previousSession);
        $interventionPersist->setAdmin($admin);
        $interventionPersist->setFiles();
        $interventionPersist->saveIntervention();

        $sessionHandler = new SessionHandler();
        if ($sessionHandler->isSession()) {
            $sessionHandler->clearSession();
        }

        $this->sendNewInterventionAssignment($interventionPersist->getIntervention());

        return $this->redirect($this->generateUrl('admin_interventions_index'));
    }

    /**
     * @param Request $request
     * @param SaveStepService $saveStepService
     * @param InterventionStepConsistentService $interventionStepConsistentService
     * @return RedirectResponse
     */
    public function saveForLaterAction(
        Request $request,
        SaveStepService $saveStepService,
        InterventionStepConsistentService $interventionStepConsistentService
    ) {
        try {
            $interventionRequest = $request->request->all();
            $saveStepService->setRequest($interventionRequest);
            $saveStepService->save();

            $unpublishedSession = $saveStepService->getUnpublishedSession();

            if (2 == $interventionRequest['step'] && $unpublishedSession) {
                $interventionStepConsistentService->makeStepConsistent($unpublishedSession);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($unpublishedSession);
                $entityManager->flush();
            }

            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('success', 'Intervention draft was successfully saved');
        } catch (Exception $masterException) {
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('error', 'We are unable to save the intervention. 
            Try again later. If the error persists, please contact IT. ');
        }

        return $this->redirect($this->generateUrl('admin_interventions_draft'));
    }

    /**
     * @param $sessionId
     * @param LoggerInterface $logger
     * @return RedirectResponse
     */
    public function deleteUnpublishedInterventionAction($sessionId, LoggerInterface $logger)
    {
        try {
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('success', 'Record has been deleted. ');

            $interventionStep = $this->getDoctrine()->getRepository(InterventionStep::class);
            $previousSession  = $interventionStep->findOneBy(['sessionId' => $sessionId]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($previousSession);
            $entityManager->flush();
        } catch (Exception $exception) {
            $flashbag->add('error', 'We are unable to save the intervention. 
            Try again later. If the error persists, please contact IT. ');

            $logger->log('error', $exception->getMessage());
        }

        return $this->redirect($this->generateUrl('admin_interventions_draft'));
    }

    /**
     * @param $moduleId
     * @param Request $request
     * @param FilesForModuleUploadDecorator $filesForModuleUploadDecorator
     *
     * @return Response
     *
     * @throws IncorrectArgumentException
     */
    public function getAllFilesForModuleAction(
        $moduleId,
        Request $request,
        FilesForModuleUploadDecorator $filesForModuleUploadDecorator
    ) {
        $fileUploadFactory = new FileUploadFactory($request);

        /** @var \AdminBundle\Utils\FileUplaod\UploadFile $uploader */
        $uploader       = $fileUploadFactory->get('temporary');
        $this->uploader = $uploader;
        $this->request  = $request;

        $files = [];

        $fileConstantsMap = [
            FileConstantMap::LEARNER,
            FileConstantMap::TRAINER,
            FileConstantMap::E_LEARNING,
            FileConstantMap::TRAINER_REPORT_TEMPLATE,
            FileConstantMap::ATTENDANCE_SHEET_TEMPLATE,
            FileConstantMap::CERTIFICATE_TEMPLATE,
        ];

        foreach ($fileConstantsMap as $mappedValue) {
            $this->getFilesPerUploadType($mappedValue, $files, $moduleId);
        }

        $filesForModuleUploadDecorator->decorate($files);

        return new Response($filesForModuleUploadDecorator->getDecorated());
    }

    /**
     * @param $fileConstant
     * @param $files
     * @param $moduleId
     */
    private function getFilesPerUploadType($fileConstant, &$files, $moduleId)
    {
        try {
            $files[$fileConstant] = $this->uploader->getFiles($moduleId, $fileConstant);
        } catch (InterventionBaseException $exception) {
            $files[$fileConstant] = [];
        }
    }

    /**
     * @param Intervention $intervention
     * @return Response
     */
    public function showStatsAction(Intervention $intervention)
    {
        $learnersProgress = $this->getDoctrine()->getRepository(LearnerIntervention::class)
            ->findBy([
                'intervention' => $intervention->getId(),
            ]);

        $nbLearners    = 0;
        $progress      = 0;
        $totalProgress = 0;
        foreach ($learnersProgress as $learnerProgress) {
            ++$nbLearners;
            $progress += $learnerProgress->getProgression();
        }

        if ($nbLearners > 0) {
            $totalProgress = $progress / $nbLearners;
        }

        $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
            ->getResultsByIntervention($intervention->getId());

        $nbNotation    = 0;
        $notation      = 0;
        $totalNotation = 0;
        foreach ($learnersNotation as $learnerNotation) {
            ++$nbNotation;
            $notation += $learnerNotation->getNotation();
        }

        if ($nbNotation) {
            $totalNotation = $notation / $nbNotation;
        }

        $intervention->setNbLearner($nbLearners);
        $intervention->setLearnersNotation($totalNotation);
        $intervention->setLearnersProgress($totalProgress);

        $quizzes = [];

        foreach ($intervention->getModules() as $module) {
            if ($module->getQuiz() && $module->getQuiz()->getQuizable()) {
                $quizzes[$module->getId()] = [
                    'module'  => $module,
                    'results' => [],
                ];
                /*if ('NATIVE' === $module->getQuiz()->getType()) {
                    $quizzesResults = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                        ->findBy([
                            'quiz'   => $module->getQuiz(),
                            'module' => $module->getId(),
                        ]);

                    foreach ($quizzesResults as $quizResult) {
                        if (!array_key_exists($quizResult->getStudent()->getId(),
                            $quizzes[$module->getId()]['results'])) {
                            $quizzes[$module->getId()]['results'][$quizResult->getStudent()->getId()] = [
                                'learner' => $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy([
                                    'person' => $quizResult->getStudent(),
                                ]),
                                'results' => [],
                            ];
                        }
                        array_push($quizzes[$module->getId()]['results'][$quizResult->getStudent()->getId()]['results'],
                            $quizResult);
                    }
                }*/
            }
        }

        return $this->render('AdminBundle:Intervention:show-stats.html.twig', array(
            'intervention' => $intervention,
            'quizzes'      => $quizzes,
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function removeAssignmentAction(Request $request)
    {
        $assignments    = $request->request->get('assignments');
        $interventionId = $request->request->get('interventionId');
        $data = [];
        if ($assignments && $interventionId) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($assignments as $assignment) {
                $hasModuleAssigment = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                        ->countLearnerModuleIterationByInterventionId($interventionId, $assignment);
                if (!$hasModuleAssigment) {
                    $learnerIntervention = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                        ->findOneBy(['intervention' => $interventionId, 'learner' => $assignment]);
                    if ($learnerIntervention) {
                        $entityManager->remove($learnerIntervention);
                    }
                } else {
                    $data[] = [
                        'displayName' => $this->getDoctrine()->getRepository('ApiBundle:Profile')
                        ->find($assignment)->getDisplayName()
                    ];
                }
            }
            $entityManager->flush();
        }

        $response = new Response(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param Request $request
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeTrainerModuleAction(Request $request)
    {
        $moduleId = $request->request->get('moduleId');
        if(!$moduleId){
            $moduleIds = $moduleId = $request->request->get('moduleIds');
        }
        $trainerIds = $request->request->get('trainerIds');
        foreach ($trainerIds as $key => $trainerId){
            if (isset($moduleIds[$key])){
                $moduleId = $moduleIds[$key];
            }
            $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                ->findOneBy(['module' => $moduleId, 'liveResource' => $trainerId]);
            if($assignment){
                $assignment->setLiveResource(NULL);
                $this->em->persist($assignment);
            }
        }
        $this->em->flush();

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function removeTrainerAssignmentAction(Request $request)
    {
        $assignments    = $request->request->get('assignments');
        $interventionId = $request->request->get('interventionId');
        if ($assignments && $interventionId) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($assignments as $assignment) {
                $hasAssigments = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                    ->findBy(['module' => $assignment]);
                foreach ($hasAssigments as $hasAssigment) {
                    if ($hasAssigment) {
                        $hasAssigment->setLiveResource(null);
                        $entityManager->persist($hasAssigment);
                        $entityManager->flush();
                    }
                }
            }
        }
        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionIsdownloadAction(Request $request)
    {
        try {
            $path            = $request->request->get('path');
            $isChecked       = $request->request->get('isChecked');
            $_SESSION[$path] = $isChecked;

            $response = 'ok';
        } catch (IncorrectArgumentException $exception) {
            return new JsonResponse($exception->getMessage(), 404);
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function imageShowAction(Request $request)
    {
        $path     = $request->query->get('path');
        if (file_exists($path)) {
            $response = new BinaryFileResponse($path);
            $response->trustXSendfileTypeHeader();
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_INLINE,
                'show_image',
                iconv('UTF-8', 'ASCII//TRANSLIT', 'show_image')
            );
            return $response;
        } else {
            return new JsonResponse("The file $path does not exist");
        }
    }

    /**
     * @param Intervention $intervention
     */
    public function sendNewInterventionAssignment(Intervention $intervention)
    {
        $modules  = $intervention->getModules();
        $hours    = 0;
        $minutes  = 0;
        $assignmentResources = [];

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $duration   = clone $module->getDuration();
            $hours      += intval($duration->format('H'));
            $minutes    += intval($duration->format('i'));
            $assigments = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(['module' => $module]);
            if ($assigments) {
                foreach ($assigments as $assigment) {
                    if ($assigment->getLiveResource()) {
                        $assignmentResources[] = $assigment;
                    }
                }
            }
        }
        $hours   += floor($minutes / 60);
        $minutes = $minutes % 60;

        $option = [
            'intervention' => $intervention,
            'duration'     => $hours . ':' . $minutes,
            'joinUrl'      => $this->container->get('router')->generate('trainer_planning_index', [], 0),
            'template'     => 'emailCourseAssignment',
        ];

        foreach ($assignmentResources as $assignmentResource) {
            $option['assignment'] = $assignmentResource;
            $option['subject'] = $assignmentResource->getLiveResource()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->get('translator')->trans('email.informs_you') . ': ' . $this->get('translator')->trans('admin.alerts.new_course_assignment');
            $this->sendEmail($assignmentResource->getLiveResource(), $option);
        }
    }

    /**
     * Convert time into decimal time (minute).
     *
     * @param string $time The time in the format hh:mm:ss to convert
     *
     * @return integer The time as a decimal value.
     */
    function convertTimeToDecimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60);

        return $decTime;
    }

    function secondsToTime($seconds)
    {
        if ($seconds <= 0) return '';

        $ret = "";

        /*** get the days ***/
        $days = intval(intval($seconds) / (3600*24));
        if($days> 0)
        {
            $ret .= $days > 10 ? "$days:" : "0$days:";
        }

        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if($hours > 0)
        {
            $ret .= $hours > 9 ? "$hours:" : "0$hours:";
        } else {
            $ret .= "00:";
        }

        /*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if($minutes > 0)
        {
            $ret .= $minutes > 9 ? "$minutes:" : "0$minutes:";
        } else {
            $ret .= "00:";
        }

        /*** get the seconds ***/
        $seconds = intval($seconds) % 60;
        if ($seconds > 0) {
            $ret .= $seconds > 9 ? "$seconds" : "0$seconds";
        } else {
            $ret .= "00";
        }

        return $ret;
    }
}
