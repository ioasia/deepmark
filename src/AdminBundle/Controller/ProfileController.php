<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use AppBundle\Entity\PersonTimeZone;
use Controller\BaseController;
use DateTime;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Traits\Controller\HasWorkingHours;

/**
 * Class ProfileController
 * @package AdminBundle\Controller
 */
class ProfileController extends BaseController
{
    use HasWorkingHours;

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    public function showAction()
    {
        $user = $this->getUser();

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        return $this->render('AdminBundle:profile:show.html.twig', array(
            'user'    => $user,
            'profile' => $profile,
        ));
    }

    public function editAction()
    {
        $user    = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $em        = $this->getDoctrine()->getManager();
        $entities  = $em->getRepository('ApiBundle:Entity')->findBy(['organisation' => $profile->getEntity()->getOrganisation()->getId()]);
        $timeZones = $em->getRepository(PersonTimeZone::class)->findAll();
        $person    = $profile->getPerson();
        /*$workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);*/
        return $this->render('AdminBundle:profile:edit.html.twig', array(
            'profile'       => $profile,
            'person'        => $person,
            'timeZones'     => $timeZones,
            'entities'      => $entities,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function saveAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $person        = $profile->getPerson();
        $entityManager = $this->getDoctrine()->getManager();
        $lastName      = $request->request->get('lastName');
        $firstName     = $request->request->get('firstName');
        $position      = $request->request->get('position');
        $fixedPhone    = $request->request->get('fixedPhone');
        $mobilePhone   = $request->request->get('mobilePhone');
        $timeZoneId    = $request->request->get('time_zone_id');
        $timeZone      = $this->getDoctrine()->getRepository('AppBundle:PersonTimeZone')->find($timeZoneId);
        $person->setTimeZone($timeZone);
        $entityManager->persist($person);

        $data    = $request->request->all();
        $profile->setWorkingHours($this->daysProcessing($data));
        $profile->setLastName($lastName);
        $profile->setFirstName($firstName);
        $profile->setPosition($position);
        $profile->setFixedPhone($fixedPhone);
        $profile->setMobilePhone($mobilePhone);
        $profile->setUpdateDate(new DateTime());

        $entityManager->persist($profile);
        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return $this->render('AdminBundle:profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('admin_profile_edit');
    }

    public function savePassAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $entityManager = $this->getDoctrine()->getManager();
        $password = $request->request->get('password');
        $rpPassword = $request->request->get('rpPassword');


        $profile->setUpdateDate(new DateTime());
        $user->setPlainPassword($rpPassword);
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->updatePassword($user);
        $entityManager->persist($user);
        $entityManager->persist($profile);
        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return $this->render('AdminBundle:profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('admin_profile_edit');
    }

    public function configurationAction()
    {
        return $this->render('@Admin/Dashboard/configuration.html.twig');
    }
}
