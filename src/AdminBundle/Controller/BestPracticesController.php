<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BestPractice;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Profile;
use Traits\Controller\HasBestPractice;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

/**
 * BestPracticesController controller.
 */
class BestPracticesController extends BaseController
{
    use HasBestPractice;

    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    public function indexAction(Request $request, $tab)
    {
        if ($tab == 3) {
            $url = $this->generateUrl(
                'admin_best_practices_index'
            );
            return $this->redirect($url, 308);
        }

        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $profile->getEntity()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }
        // get tags
        $searchParam['tags'] = [];
        $tags         = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        foreach ($tags as $tag){
            $searchParam['tags'][$tag->getId()] = $tag->getTagName();
        }

        return $this->render('AdminBundle:bestPractices:index.html.twig', array(
            'searchParam' => $searchParam
        ));
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $profile->getEntity()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        $result = $this->getBestPractice($id, $this->getCurrentUser());

        return $this->render('molecules/best-practices/view.html.twig', array(
            'bestPractice'          => $result['bestPractice'],
            'resourcePath'          => $result['resourcePath'],
            'others'                => $result['others'],
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser()),
            'isReview'              => $result['isReview'],
            'searchParam'           => $searchParam
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function managementAction(Request $request)
    {
        $submittedBestPractices = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->findBy(['status' => BestPractice::STATUS_SUBMITTED]);
        $validatedBestPractices = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->findBy(['status' => BestPractice::STATUS_APPROVED]);
        $rejectedBestPractices = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->findBy(['status' => BestPractice::STATUS_REJECTED]);

        return $this->render('AdminBundle:bestPractices:management.html.twig', array(
            'submittedBestPractices'        => $submittedBestPractices,
            'validatedBestPractices'        => $validatedBestPractices,
            'rejectedBestPractices'         => $rejectedBestPractices,
            'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function validationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entityIds = $request->request->get('selectedEntities');
        $type = $request->request->get('type');
        $comment = $request->request->get('comment');
        foreach ($entityIds as $id){
            try {
                $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->find($id);
                if ($bestPractice) {
                    // save the BestPractice object
                    $status = ($type == 1) ? BestPractice::STATUS_APPROVED : BestPractice::STATUS_REJECTED;
                    $bestPractice->setStatus($status);
                    $bestPractice->setComment($comment);
                    $bestPractice->setValidateDate(new DateTime());
                    $em->persist($bestPractice);
                    $em->flush($bestPractice);
                }
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $response = 'ok';
        return new JsonResponse($response, 200);
    }

    public function createAction(Request $request)
    {
        $answerId = $request->query->get('answerId');
        $resourceType = $request->query->get('resourceType');
        $params = [];
        if ($resourceType && $answerId) {
            $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($answerId);
            if ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION) {
                $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
                $params = [
                    'resourceType' => BestPractice::RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION,
                    'resourceId' => $defaultCorrection[0]->getId(),
                    'resourceParams' => $defaultCorrection[0]->getSpecifics(),
                    'moduleId' => $answer->getPlay()->getModule()->getId(),
                    'entityId' => $answer->getPlay()->getModule()->getIntervention()->getEntity()->getId(),
                    'file_descriptor' => null
                    ];
            } else if ($resourceType == BestPractice::RESOURCE_TYPE_QUIZ_LEARNER_ANSWER) {
                $params = [
                    'resourceType' => BestPractice::RESOURCE_TYPE_QUIZ_LEARNER_ANSWER,
                    'resourceId' => $answer->getId(),
                    'resourceParams' => json_decode($answer->getParams())->answer,
                    'moduleId' => $answer->getPlay()->getModule()->getId(),
                    'entityId' => $answer->getPlay()->getModule()->getIntervention()->getEntity()->getId(),
                    'file_descriptor' => json_decode($answer->getParams())->file_descriptor
                ];
            }
        }
        $tags         = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        $tagsName = [];
        foreach ($tags as $tag){
            $tagsName[] = $tag->getTagName();
        }
        return $this->render('AdminBundle:bestPractices:create.html.twig', array(
            'tags' => $tagsName,
            'params' => $params,
        ));
    }
}
