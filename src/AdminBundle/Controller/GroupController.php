<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Group;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupController
 * @package AdminBundle\Controller
 */
class GroupController extends BaseController implements AdminControllerInterface
{
    /**
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $groups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Group')->findAll();
        $data   = [];
        foreach ($groups as $group) {
            $learnerGroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('group' => $group));
            $numLearner    = count($learnerGroups);
            $data[]        = array('group' => $group, 'numLearner' => $numLearner);
        }

        return $this->render('AdminBundle:group:index.html.twig', array(
            'data' => $data,
        ));
    }

    /**
     * Creates a new Group entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $group = new Group();
        $form  = $this->createForm('ApiBundle\Form\GroupType', $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('admin_group_show', array('id' => $group->getId()));
        }

        return $this->render('AdminBundle:group:formGroup.html.twig', array(
            'group' => $group,
            'form'  => $form->createView(),
        ));
    }

    /**
     * Finds and displays a group entity.
     *
     * @param Group $group
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Group $group)
    {
        $learnerGroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerGroup')->findBy(array('group' => $group));

        return $this->render('AdminBundle:group:show.html.twig', array(
            'learnerGroups' => $learnerGroups,
            'group'         => $group,
            'delete_form'   => $this->createDeleteForm($group)->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing group entity.
     *
     * @param Request $request
     * @param Group $group
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Group $group)
    {
        $deleteForm = $this->createDeleteForm($group);
        $editForm   = $this->createForm('ApiBundle\Form\GroupType', $group);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_group_edit', array('id' => $group->getId()));
        }

        return $this->render('AdminBundle:group:formGroup.html.twig', array(
            'group'       => $group,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit'      => true,
        ));
    }

    /**
     * Deletes a group entity.
     *
     * @param Request $request
     * @param Group $group
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Group $group)
    {
        $form = $this->createDeleteForm($group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')
                ->deleteLearnerGroupByGroup($group);
            $em = $this->getDoctrine()->getManager();
            $em->remove($group);
            $em->flush();
        }

        return $this->redirectToRoute('admin_group_index');
    }

    /**
     * Creates a form to delete a group entity.
     * @param Group $group
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Group $group)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_group_delete', array('id' => $group->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
