<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Organisation;
use Controller\AdminControllerInterface;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Traits\Controller\HasWorkingHours;

/**
 * Organisation controller.
 */
class OrganisationController extends Controller implements AdminControllerInterface
{
    use HasWorkingHours;
    /**
     * Lists all organisation entities.
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $organisations = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Organisation')->findAll();

        return $this->render('AdminBundle:organisation:index.html.twig', array(
            'organisations' => $organisations,
        ));
    }

    /**
     * Creates a new organisation entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $organisation = new Organisation();
        $form         = $this->createForm('ApiBundle\Form\OrganisationType', $organisation, [
            'attr' => ['enctype' => 'multipart/form-data']
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data    = $request->request->all();
            $organisation->setWorkingHours($this->daysProcessing($data));
            if (isset($_FILES['logoFile'])) {
                $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/organisation/';
                $array = explode('.', $_FILES['logoFile']['name']);
                $extension = end($array);
                $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $targetFile = $targetDir . $fileName;
                $path = '/files/organisation/' . $fileName;

                if (move_uploaded_file($_FILES['logoFile']['tmp_name'], $targetFile)) {
                    $organisation->setLogo($path);
                }
            }
            $em->persist($organisation);
            $em->flush();

            return $this->redirectToRoute('admin_organisation_show', array('id' => $organisation->getId()));
        }

        return $this->render('AdminBundle:organisation:new.html.twig', array(
            'organisation' => $organisation,
            'form'         => $form->createView(),
        ));
    }

    /**
     * Finds and displays a organisation entity.
     *
     * @param Organisation $organisation
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Organisation $organisation)
    {
        $deleteForm = $this->createDeleteForm($organisation);

        return $this->render('AdminBundle:organisation:show.html.twig', array(
            'organisation' => $organisation,
            'delete_form'  => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing organisation entity.
     *
     * @param Request $request
     * @param Organisation $organisation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Organisation $organisation)
    {
        $deleteForm = $this->createDeleteForm($organisation);
        $editForm   = $this->createForm('ApiBundle\Form\OrganisationType', $organisation, [
            'attr' => ['enctype' => 'multipart/form-data']
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data = $request->request->all();
            $organisation->setWorkingHours($this->daysProcessing($data));
            if (isset($_FILES['logoFile'])) {
                $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/organisation/';
                $array = explode('.', $_FILES['logoFile']['name']);
                $extension = end($array);
                $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $targetFile = $targetDir . $fileName;
                $path = '/files/organisation/' . $fileName;

                if (move_uploaded_file($_FILES['logoFile']['tmp_name'], $targetFile)) {
                    $organisation->setLogo($path);
                }
            }
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_organisation_edit', array('id' => $organisation->getId()));
        }

        return $this->render('AdminBundle:organisation:edit.html.twig', array(
            'organisation' => $organisation,
            'form'    => $editForm->createView(),
            'delete_form'  => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a organisation entity.
     */
    public function deleteAction(Request $request, Organisation $organisation)
    {
        $form = $this->createDeleteForm($organisation);
        $form->handleRequest($request);

        // delete the organisation
        $translator = $this->get('translator');
        $flashBag = $this->get('session')->getFlashBag();
        $messages = $this->deleteOrganisation($organisation, $translator);
        if (!$messages) {
            $flashBag->add('warning', $translator->trans('global.organisation_delete_success', ['%organisation%' => $organisation->getDesignation()]));
            $em = $this->getDoctrine()->getManager();
            $em->remove($organisation);
            $em->flush();
        } else {
            $flashBag->add('success', $messages);
        }

        return $this->redirectToRoute('admin_organisation_index');
    }

    public function deleteOrganisationsAction(Request $request)
    {
        $translator = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $organisationIds = $request->request->get('selectedOrganisations');
        $flashBag = $this->get('session')->getFlashBag();
        foreach ($organisationIds as $id){
            $messages = '';
            $organisation    = $em->getRepository('ApiBundle:Organisation')->find($id);
            if ($type == 1) {
                try {
                    $messages = $this->deleteOrganisation($organisation, $translator);
                    if (!$messages) {
                        $flashBag->add('warning', $translator->trans('global.organisation_delete_success', ['%organisation%' => $organisation->getDesignation()]));
                        $em->remove($organisation);
                        $em->flush();
                    } else {
                        $flashBag->add('success', $messages);
                    }
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        return new JsonResponse($messages, 200);
    }

    /**
     * Delete a organisation.
     *
     * @param Organisation $organisation The organisation
     *
     * @return Boolean
     */
    private function deleteOrganisation(Organisation $organisation, $translator)
    {
        $errors = '';
        $em                = $this->getDoctrine()->getManager();
        $entities          = $em->getRepository(Entity::class)->findBy(['organisation' => $organisation]);

        if ($entities) {
            $errors .= $translator->trans('global.organisation_can_not_delete_success', ['%organisation%' => $organisation->getDesignation()]);
            $errors .= $translator->trans('global.organisation_has_total_entity', ['%entity%' => count($entities)]);
        }
        return $errors;
    }

    /**
     * Creates a form to delete a organisation entity.
     *
     * @param Organisation $organisation The organisation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Organisation $organisation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_organisation_delete', array('id' => $organisation->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
