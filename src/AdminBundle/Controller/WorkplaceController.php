<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Workplace;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WorkplaceController
 * @package AdminBundle\Controller
 */
class WorkplaceController extends BaseController implements AdminControllerInterface
{
    /**
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AdminBundle:workplace:index.html.twig', array(
            'workplaces' => $this->getDoctrine()->getManager()->getRepository('ApiBundle:Workplace')->findAll(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Creates a new Workplace entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $workplace = new Workplace();
        $form  = $this->createForm('ApiBundle\Form\WorkplaceType', $workplace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workplace);
            $em->flush();

            return $this->redirectToRoute('admin_workplace_show', array('id' => $workplace->getId()));
        }

        return $this->render('AdminBundle:workplace:formWorkplace.html.twig', array(
            'group' => $workplace,
            'form'  => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Finds and displays a Workplace entity.
     *
     * @param Workplace $workplace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Workplace $workplace)
    {
        return $this->render('AdminBundle:workplace:show.html.twig', array(
            'workplace'       => $workplace,
            'delete_form' => $this->createDeleteForm($workplace)->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Displays a form to edit an existing Workplace entity.
     *
     * @param Request $request
     * @param Workplace $workplace
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Workplace $workplace)
    {
        $deleteForm = $this->createDeleteForm($workplace);
        $editForm   = $this->createForm('ApiBundle\Form\WorkplaceType', $workplace);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_workplace_edit', array('id' => $workplace->getId()));
        }

        return $this->render('AdminBundle:workplace:formWorkplace.html.twig', array(
            'workplace'       => $workplace,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit' => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Deletes a Workplace entity.
     *
     * @param Request $request
     * @param Workplace $workplace
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Workplace $workplace)
    {
        $form = $this->createDeleteForm($workplace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // delete the organisation
            $translator = $this->get('translator');
            $flashBag = $this->get('session')->getFlashBag();
            $messages = $this->deleteWorkplace($workplace, $translator);
            if (!$messages) {
                $flashBag->add('warning', $translator->trans('global.workplace_delete_success', ['%workplace%' => $workplace->getDesignation()]));
                $em = $this->getDoctrine()->getManager();
                $em->remove($workplace);
                $em->flush();
            } else {
                $flashBag->add('success', $messages);
            }
        }

        return $this->redirectToRoute('admin_workplace_index');
    }

    public function deleteWorkplacesAction(Request $request)
    {
        $translator = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $workplaceIds = $request->request->get('selectedWorkplaces');
        $flashBag = $this->get('session')->getFlashBag();
        foreach ($workplaceIds as $id){
            $messages = '';
            $workplace = $em->getRepository('ApiBundle:Workplace')->find($id);
            if ($type == 1) {
                try {
                    $messages = $this->deleteWorkplace($workplace, $translator);
                    if (!$messages) {
                        $flashBag->add('warning', $translator->trans('global.workplace_delete_success', ['%workplace%' => $workplace->getDesignation()]));
                        $em->remove($workplace);
                        $em->flush();
                    } else {
                        $flashBag->add('success', $messages);
                    }
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        return new JsonResponse($messages, 200);
    }

    /**
     * Delete a workplace.
     *
     * @param Workplace $workplace The workplace
     *
     * @return Boolean
     */
    private function deleteWorkplace(Workplace $workplace, $translator)
    {
        $errors = '';
        $em                = $this->getDoctrine()->getManager();
        $profiles          = $em->getRepository(Profile::class)->findBy(['workplace' => $workplace]);

        if ($profiles) {
            $errors .= $translator->trans('global.workplace_can_not_delete_success', ['%workplace%' => $workplace->getDesignation()]);
            $errors .= $translator->trans('global.workplace_has_total_profile', ['%profile%' => count($profiles)]);
        }
        return $errors;
    }

    /**
     * Creates a form to delete a Workplace entity.
     * @param Workplace $workplace
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Workplace $workplace)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_workplace_delete', array('id' => $workplace->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
