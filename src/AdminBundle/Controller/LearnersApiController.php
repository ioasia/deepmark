<?php

namespace AdminBundle\Controller;

use AdminBundle\Helper\Traits\HelperTrait;
use AdminBundle\Services\LearnerService;
use AdminBundle\Services\TrainerBooking\TrainerBookingService;
use ApiBundle\Controller\RestController;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Traits\Controller\HasUsers;
use Utils\Email;

/**
 * LearnersApi controller.
 */
class LearnersApiController extends RestController
{
    use HasUsers;
    use \LearnerBundle\Controller\HelperTrait;

    /**
     * @Rest\Post("reSentSessionEmail")
     *
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="module")
     * @Rest\RequestParam(name="sessionId", nullable=false, strict=true, description="session")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function reSentSessionEmailAction(ParamFetcher $paramFetcher, TrainerBookingService $trainerBookingService)
    {
        $translator = $this->get('translator');
        $moduleId  = $paramFetcher->get('moduleId');
        $sessionId = $paramFetcher->get('sessionId');
        $learnerIds = $paramFetcher->get('selectedProfiles');
        $learners = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->findBy(array(
                'id' => $learnerIds,
            ));
        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModuleAppId = $module->getStoredModule()->getAppId();

        if ((StoredModule::ONLINE !== $storedModuleAppId) &&
            (StoredModule::VIRTUAL_CLASS !== $storedModuleAppId) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModuleAppId) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModuleAppId)
        ) {
            return $this->errorHandler('Not a booking module');
        }
        $errors = $this->get('validator')->validate($module);
        $now = new \DateTime();
        if (0 == count($errors)) {
            $moduleSession = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession')->findOneBy(array(
                'id' => $sessionId,
            ));

            $moduleAssignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findOneBy([
                'module' => $module,
                'moduleSession' => $moduleSession->getSession()
            ]);
            if (!$moduleAssignment) {
                return $this->error($translator->trans('booking.session_messages.no_assignment_session'));
            }
            $trainer          = $moduleAssignment->getLiveResource();
            if (!$trainer){
                return $this->error($translator->trans('booking.session_messages.no_assignment_session'));
            }

            foreach ($learners as $learner){
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));

                if($bookingAgendas) {
                    foreach ($bookingAgendas as $booked) {
                        if ($booked->getModuleSession()->getId() === $moduleSession->getId()) {
                            $bookingAgenda = $booked;
                            $this->get(Email::class)->sendMailBookingConfirm($learner, $module, $bookingAgenda);
                        }
                    }
                }
            }
            // send mail to trainer one time when learner booking
            if(!$trainerBookingService->hasSentMailBookingConfirm($module, $moduleSession)) {
                $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingAgenda);
            }
        }else {
            return $this->missing('Error :', $errors);
        }

        return $this->json(['success']);
    }

    /**
     * @Rest\Post("unAttachmentSession")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="module")
     * @Rest\RequestParam(name="sessionId", nullable=false, strict=true, description="session")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function unAttachmentSessionAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $moduleId  = $paramFetcher->get('moduleId');
        $sessionId = $paramFetcher->get('sessionId');
        $learnerIds = $paramFetcher->get('selectedProfiles');
        $learners = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->findBy(array(
                'id' => $learnerIds,
            ));
        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModuleAppId = $module->getStoredModule()->getAppId();

        if ((StoredModule::ONLINE !== $storedModuleAppId) &&
            (StoredModule::VIRTUAL_CLASS !== $storedModuleAppId) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModuleAppId) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModuleAppId)
        ) {
            return $this->errorHandler('Not a booking module');
        }
        $errors = $this->get('validator')->validate($module);
        $now = new \DateTime();
        if (0 == count($errors)) {
            $moduleSession = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession')->findOneBy(array(
                'id' => $sessionId,
            ));
            foreach ($learners as $learner){
                // set status cancel to send email to trainer
                $em = $this->getDoctrine()->getManager();
                $statusCanceled = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_CANCELED,
                ));

                $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));

                if(!$isBookingAgenda){
                    continue;
                }
                if($now >= $isBookingAgenda->getBookingDate()){
                    return $this->errorHandler($translator->trans('booking.session_messages.session_has_started'));
                }

                $bookingsAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                    'module' => $module,
                    'moduleSession' => $moduleSession->getSession()
                ]);

                $isBookingAgenda->setStatus($statusCanceled);
                $em->persist($isBookingAgenda);
                $em->flush();

                // send mail cancel when session dont have any learner
                if($isBookingAgenda->getTrainer() && count($bookingsAgenda) === 1) {
                    $this->get(Email::class)->sendMailBookingCancel($isBookingAgenda, false);
                }
                // remove after send mail
                $em->remove($isBookingAgenda);
                $em->flush();

                $this->setStatusModuleIteration($learner, $module, Status::LEARNER_INTERVENTION_CREATED);

                // begin set Status::WITH_BOOKING_BOOKED status for all booked with Status::WITH_BOOKING_CONFIRMED status
                if (StoredModule::PRESENTATION_ANIMATION == $storedModuleAppId) {
                    $participantMinimum = $isBookingAgenda->getModuleSession()->getSession()->getParticipant();
                    $confirmedBooking   = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                        'moduleSession' => $isBookingAgenda->getModuleSession(),
                        'module'        => $module,
                        'status'        => $this->getDoctrine()->getRepository('ApiBundle:Status')->findBy(array(
                            'appId' => [Status::WITH_BOOKING_CONFIRMED, Status::WITH_BOOKING_BOOKED],
                        )),
                    ]);

                    if (count($confirmedBooking) < $participantMinimum) {
                        foreach ($confirmedBooking as $confirmed) {
                            $this->setStatusModuleIteration($confirmed->getLearner(), $module, Status::WITH_BOOKING_BOOKED);
                            $confirmed->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                                'appId' => Status::WITH_BOOKING_BOOKED,
                            )));
                        }
                    }
                }
                // end set Status::WITH_BOOKING_BOOKED status for all booked with Status::WITH_BOOKING_CONFIRMED status
                $em->flush();
            }
        }else {
            return $this->missing('Error :', $errors);
        }

        return $this->json(['success']);
    }

    /**
     * @Rest\Post("attachmentSession")
     *
     * @param ParamFetcher $paramFetcher
     * @param TranslatorInterface $translator
     * @param TrainerBookingService $trainerBookingService
     * @param LearnerService $learnerService
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="module")
     * @Rest\RequestParam(name="sessionId", nullable=false, strict=true, description="session")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function attachmentSessionAction(ParamFetcher $paramFetcher, TranslatorInterface $translator
        , TrainerBookingService $trainerBookingService, LearnerService $learnerService)
    {
        $moduleId  = $paramFetcher->get('moduleId');
        $sessionId = $paramFetcher->get('sessionId');
        $learnerIds = $paramFetcher->get('selectedProfiles');

        $learners = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->findBy(array(
                'id' => $learnerIds,
            ));

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModule = $module->getStoredModule()->getAppId();

        if ((StoredModule::VIRTUAL_CLASS !== $storedModule) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModule) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModule)
        ) {
            return $this->errorHandler('Not a session module');
        }

        /** @var ModuleSession $moduleSession */
        $moduleSessionRepository = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession');
        $moduleSession = $moduleSessionRepository->findOneBy(array(
            'id' => $sessionId,
        ));

        if (!$moduleSession) {
            return $this->errorHandler($translator->trans('booking.session_messages.no_session_module'));
        }

        $unAvailableTimesLearners = $learnerService->validateLearnersToSession($moduleSession, $learners, $module->getDuration());
        if($unAvailableTimesLearners){
            return $this->errorHandler($translator->trans('booking.session_messages.learner_not_available_session', ['%learnerNames%' => implode(", ", $unAvailableTimesLearners)]));
        }

        if(!$moduleSessionRepository->validateLearnerSessionSlot($moduleSession, $learnerIds)){
            return $this->errorHandler($translator->trans('booking.session_messages.not_enough_slots'));
        }

        $start   = $moduleSession->getSession()->getSessionDate();
        $end     = clone $start;
        $hour    = (int)$module->getDuration()->format('G') + (int)$moduleSession->getSession()->getSessionDate()->format('G');
        $minutes = (int)$module->getDuration()->format('i') + (int)$moduleSession->getSession()->getSessionDate()->format('i');
        $end->setTime($hour, $minutes, 0);

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $moduleAssignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findOneBy([
                'module' => $module,
                'moduleSession' => $moduleSession->getSession()
            ]);
            if (!$moduleAssignment) {
                return $this->errorHandler($translator->trans('booking.session_messages.no_assignment_session'));
            }
            $trainer          = $moduleAssignment->getLiveResource();
            if (!$trainer){
                return $this->errorHandler($translator->trans('booking.session_messages.no_assignment_session'));
            }

            $em              = $this->getDoctrine()->getManager();
            foreach ($learners as $learner){
                $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));

                if ($isBookingAgenda) {
                    return $this->errorHandler($translator->trans('booking.session_messages.already_booked_session'));
                }

                $bookingAgenda = new BookingAgenda();
                $bookingAgenda->setBookingDate($moduleSession->getSession()->getSessionDate());
                $bookingAgenda->setLearner($learner);
                $bookingAgenda->setTrainer($trainer);
                $bookingAgenda->setModule($module);

                $bookingAgenda->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_BOOKED,
                )));

                $this->setStatusModuleIteration($learner, $module, Status::WITH_BOOKING_BOOKED);
                $bookingAgenda->setModuleSession($moduleSession);
                $em->persist($bookingAgenda);

                // begin set Status::WITH_BOOKING_CONFIRMED status for all booked with Status::WITH_BOOKING_BOOKED status
                if (StoredModule::PRESENTATION_ANIMATION == $storedModule) {
                    $participantMinimum = $moduleSession->getSession()->getParticipant();
                    $bookingAgendas     = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                        'moduleSession' => $moduleSession,
                        'module'        => $module,
                        'status'        => $this->getDoctrine()->getRepository('ApiBundle:Status')->findBy(array(
                            'appId' => [Status::WITH_BOOKING_BOOKED, Status::WITH_BOOKING_CONFIRMED],
                        )),
                    ]);

                    if (count($bookingAgendas) >= $participantMinimum) {
                        foreach ($bookingAgendas as $booked) {
                            $this->setStatusModuleIteration($booked->getLearner(), $module, Status::WITH_BOOKING_CONFIRMED);
                            $booked->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                                'appId' => Status::WITH_BOOKING_CONFIRMED,
                            )));
                        }
                    }
                }
                $em->flush();
                // end set Status::WITH_BOOKING_CONFIRMED status for all booked with Status::WITH_BOOKING_BOOKED status
                $this->get(Email::class)->sendMailBookingConfirm($learner, $module, $bookingAgenda);
            }
            // send mail to trainer one time when learner booking
            if(!$trainerBookingService->hasSentMailBookingConfirm($module, $moduleSession)) {
                $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingAgenda);
            }
        } else{
            return $this->missing('Error :', $errors);
        }
        $view->setData('ok');
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("attachment")
     *
     * @param ParamFetcher $paramFetcher
     * @param LearnerService $learnerService
     * @param TranslatorInterface $translator
     * @Rest\RequestParam(name="intervention", nullable=false, strict=true, description="Intervention.")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function attachmentInterventionAction(ParamFetcher $paramFetcher, LearnerService $learnerService, TranslatorInterface $translator)
    {
        $em = $this->getDoctrine()->getManager();

        $idIntervention = $paramFetcher->get('intervention');
        $learners       = $paramFetcher->get('selectedProfiles');
        $intervention   = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
            ->findOneBy(['id' => $idIntervention]);

        // calculation the duration of the intervention
        $modules = $intervention->getModules();
        $hours   = 0;
        $minutes = 0;
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $duration = clone $module->getDuration();
            $hours    += intval($duration->format('H'));
            $minutes  += intval($duration->format('i'));
        }

        $hours   += floor($minutes / 60);
        $minutes = $minutes % 60;

        $status = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_CREATED]);

        /* comment to allow assign learner to course without validation
        $unAvailableTimesLearners = $learnerService->validateLearnersToCourse($intervention, $learners);
        if($unAvailableTimesLearners){
            return $this->errorHandler($translator->trans('booking.session_messages.learner_not_available_session', ['%learnerNames%' => implode(", ", $unAvailableTimesLearners)]));
        }
        */

        foreach ($learners as $learner) {
            $learnerProfile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($learner);
            // update learner intervention  and send email to learner
            if ($learnerProfile) {

                $isExistLearnerIntervention = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array('learner' => $learner, 'intervention' => $intervention));
                if(count($isExistLearnerIntervention) == 0 ){
                    $attachment = new LearnerIntervention();
                    $attachment->setIntervention($intervention);
                    $attachment->setLearner($learnerProfile);
                    $attachment->setProgression(0);
                    $attachment->setStatus($status);
                    $em->persist($attachment);

                    $this->get(Email::class)->sendMailCourseRegistration(
                        $learnerProfile,
                        $intervention,
                        [$hours, $minutes]
                    );
                }
            }
        }
        $em->flush();
        $view = View::create();

        $view->setData(array(
            'message' => 'The learners have been registered for the intervention',
        ));

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("reSentAssignCourseEmail")
     *
     * @param ParamFetcher $paramFetcher
     * @param LearnerService $learnerService
     * @param TranslatorInterface $translator
     * @Rest\RequestParam(name="interventionId", nullable=false, strict=true, description="Intervention Id")
     * @Rest\RequestParam(name="assignments", nullable=false, strict=true, description="Assignments")
     *
     * @return Response
     */
    public function reSentAssignCourseEmailAction(ParamFetcher $paramFetcher, LearnerService $learnerService, TranslatorInterface $translator)
    {
        $idIntervention = $paramFetcher->get('interventionId');
        $learners       = $paramFetcher->get('assignments');
        $intervention   = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
            ->findOneBy(['id' => $idIntervention]);

        // calculation the duration of the intervention
        $modules = $intervention->getModules();
        $hours   = 0;
        $minutes = 0;
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $duration = clone $module->getDuration();
            $hours    += intval($duration->format('H'));
            $minutes  += intval($duration->format('i'));
        }

        $hours   += floor($minutes / 60);
        $minutes = $minutes % 60;

        foreach ($learners as $learner) {
            $learnerProfile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($learner);
            // send email to learner
            if ($learnerProfile) {

                $isExistLearnerIntervention = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array('learner' => $learner, 'intervention' => $intervention));
                if($isExistLearnerIntervention){
                    $this->get(Email::class)->sendMailCourseRegistration(
                        $learnerProfile,
                        $intervention,
                        [$hours, $minutes]
                    );
                }
            }
        }

        $view = View::create();
        $view->setData(array(
            'message' => 'The learners have been registered for the intervention',
        ));

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("sendAgain")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="template", nullable=false, strict=true, description="Template email.")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function sendAgainAction(ParamFetcher $paramFetcher)
    {
        $em = $this->container->get('doctrine')->getManager();
        $template = $paramFetcher->get('template');
        $learners = $paramFetcher->get('selectedProfiles');
        foreach ($learners as $learner) {
            $learnerProfile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($learner);
            if ($learnerProfile) {
                // Setup default password
                $personEntity = $learnerProfile->getPerson();
                $personEntity->setPlainPassword(Profile::DEFAULT_PASSWORD);
                $userManager = $this->container->get('fos_user.user_manager');
                $userManager->updatePassword($personEntity);
                $em->persist($personEntity);
                $em->flush();

                $email = $this->container->get(Email::class);
                $email->send(
                    $learnerProfile->getPerson()->getEmail(),
                    [
                        'subject'  => $learnerProfile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->container->get('translator')->trans('email.informs_you') . ': ' . $this->container->get('translator')->trans('admin.alerts.your account_activated'),
                        'profile'  => $learnerProfile,
                        'email'    => $learnerProfile->getPerson()->getEmail(),
                        'yourPassword' => Profile::DEFAULT_PASSWORD,
                        'joinUrl'  => $this->container->get('router')
                            ->generate(
                                'fos_user_security_login', array(), UrlGeneratorInterface::ABSOLUTE_URL
                            ),
                        'template' => $template ? $template : 'emailRegistration',
                    ]
                );
            }
        }
        $view = View::create();
        $view->setData(array(
            'message' => 'The emails have been sent to the learners',
        ));

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("assign/group")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="group", nullable=false, strict=true, description="Group.")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function assignGroupAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $idGroup  = $paramFetcher->get('group');
        $learners = $paramFetcher->get('selectedProfiles');

        $group = $this->getDoctrine()->getRepository('ApiBundle:Group')
            ->find($idGroup);

        foreach ($learners as $learner) {
            $learner = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($learner);

            $assign = new LearnerGroup();
            $assign->setGroup($group);
            $assign->setLearner($learner);
            $em->persist($assign);
        }

        $em->flush();
        $view = View::create();

        $view->setData(array(
            'message' => 'The learners have been registered for the group',
        ));

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("regional")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="entity_id", nullable=false, strict=true, description="Entity id.")
     *
     * @return Response
     */
    public function regionalAction(ParamFetcher $paramFetcher)
    {
        $em           = $this->getDoctrine()->getManager();
        $entityId     = $paramFetcher->get('entity_id');
        $regionalList = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entityId]);
        $result       = [];

        foreach ($regionalList as $regional) {
            $result[] = array('id' => $regional->getId(), 'designation' => $regional->getDesignation());
        }

        $view = View::create();

        $view->setData($result);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("group")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="regional_id", nullable=false, strict=true, description="Regional id.")
     *
     * @return Response
     */
    public function groupAction(ParamFetcher $paramFetcher)
    {
        $em         = $this->getDoctrine()->getManager();
        $regionalId = $paramFetcher->get('regional_id');
        $regional   = $em->getRepository('ApiBundle:Regional')->find($regionalId);
        $groups     = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));
        $result     = [];

        foreach ($groups as $group) {
            $result[] = array('id' => $group->getId(), 'designation' => $group->getDesignation());
        }

        $view = View::create();

        $view->setData($result);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("workplace")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="group_id", nullable=false, strict=true, description="Group id.")
     *
     * @return Response
     */
    public function workplaceAction(ParamFetcher $paramFetcher)
    {
        $em         = $this->getDoctrine()->getManager();
        $groupId    = $paramFetcher->get('group_id');
        $workplaces = $em->getRepository('ApiBundle:Workplace')->findBy(array('groupOrganization' => $groupId));
        $result     = [];

        foreach ($workplaces as $workplace) {
            $result[] = array('id' => $workplace->getId(), 'designation' => $workplace->getDesignation());
        }

        $view = View::create();

        $view->setData($result);

        return $this->handleView($view);
    }
}
