<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\QuizUsersAnswers;
use ApiBundle\Service\QuizServiceAPI;
use ApiBundle\Entity\ProfileVariety;
use Controller\BaseController;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;
use PDO;
use Utils\Email;

/**
 * Entity controller.
 */
class ExercicesController extends BaseController
{
    public function indexAction(Request $request)
    {
        // get the trainers list
        $em = $this->getDoctrine()->getManager();
        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );
        $conditionArr = array(
            'profileType' => $profileType,
        );
        $roles = $this->getUser()->getRoles();
        if (!in_array('ROLE_SUPER_ADMIN', $roles)) {
            $conditionArr['entity'] = $this->getEntityFromProfile()->getId();
        }
        $trainers = $em->getRepository('ApiBundle:Profile')->findBy($conditionArr);

        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // get answers to assign
        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "SELECT answer_id FROM `exercises` GROUP BY answer_id";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $answerIds = $statementDB->fetchAll(PDO::FETCH_COLUMN, 0);

        $exercisesToAssigns = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersToAssign('open', $answerIds);
        $exercisesToAssignUsers = [];
        foreach ($exercisesToAssigns as $exercisesToAssign) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToAssign->getPlay()->getUser_id()));
            $exercisesToAssignUsers[$exercisesToAssign->getId()] = $player;
        }

        // get answers to do
        $exercisesToDos = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_TODO);
        $exercisesToDoUsers = [];
        foreach ($exercisesToDos as $exercisesToDo) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToDo->getPlay()->getUser_id()));
            $exercisesToDoUsers[$exercisesToDo->getId()] = $player;
        }

        // get answers to validation
        $exercisesToValidations = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_SUBMITTED);
        $exercisesToValidationUsers = [];
        foreach ($exercisesToValidations as $exercisesToValidation) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToValidation->getPlay()->getUser_id()));
            $exercisesToValidationUsers[$exercisesToValidation->getId()] = $player;
        }

        // get validated answers
        $exercisesValidateds = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_APPROVED);
        $exercisesToValidatedUsers = [];
        foreach ($exercisesValidateds as $exercisesValidated) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesValidated->getPlay()->getUser_id()));
            $exercisesToValidatedUsers[$exercisesValidated->getId()] = $player;
        }

        // get reject answers
        $exercisesRejects = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_REJECTED);
        $exercisesRejectedUsers = [];
        foreach ($exercisesRejects as $exercisesReject) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesReject->getPlay()->getUser_id()));
            $exercisesRejectedUsers[$exercisesReject->getId()] = $player;
        }

        return $this->render('AdminBundle:exercices:index.html.twig', array(
            'trainers'                      => $trainers,
            'exercisesToAssigns'            => $exercisesToAssigns,
            'exercisesToAssignUsers'        => $exercisesToAssignUsers,
            'exercisesToDos'                => $exercisesToDos,
            'exercisesToDoUsers'            => $exercisesToDoUsers,
            'exercisesToValidations'        => $exercisesToValidations,
            'exercisesToValidationUsers'    => $exercisesToValidationUsers,
            'exercisesValidateds'           => $exercisesValidateds,
            'exercisesToValidatedUsers'     => $exercisesToValidatedUsers,
            'exercisesRejects'              => $exercisesRejects,
            'exercisesRejectedUsers'        => $exercisesRejectedUsers,
            'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id, Request $request, QuizServiceAPI $quizServiceAPI)
    {
        $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($id);
        $questionItems = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'personalized_correction');
        $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
        $items = [];
        foreach ($questionItems as $k => $questionItem) {
            $items[$k]['item'] = $questionItem;
            $mark = $this->getDoctrine()->getRepository('ApiBundle:Marks')
                ->findBy(array(
                    'exercises' => $answer->getExercises(),
                    'questionsItem' => $questionItem,
                ));
            $items[$k]['mark'] = $mark;
        }

        // get step and total step
        $quizSteps = $quizServiceAPI->sanitizeQuiz($answer->getPlay()->getQuiz());
        $step = 1;
        foreach ($quizSteps as $k => $quizStep) {
            foreach ($quizStep['questions'] as $question) {
                if ($question->getId() == $answer->getQuestion()->getId()) {
                    $step = $k + 1;
                    break;
                }
            }
        }
        $tags = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findAll();
        
        return $this->render('TrainerBundle:myExercices:mark.html.twig', array(
            'totalStep'             => count($quizSteps),
            'step'                  => $step,
            'answer'                => $answer,
            'isSubmitted'           => 1,
            'questionMarkItems'     => $items,
            'defaultCorrection'     => $defaultCorrection,
            'templateFile'          => null,
            'reportFile'            => null,
            'tags'                  => $tags,
            'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function assignAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $flashBag = $this->get('session')->getFlashBag();
        $translator = $this->get('translator');
        $entityIds = $request->request->get('selectedEntities');
        $trainerId = $request->request->get('trainerId');
        $trainer = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->find($trainerId);
        foreach ($entityIds as $id){
            try {
                $answer = $this->getDoctrine()
                    ->getRepository('ApiBundle:QuizUsersAnswers')
                    ->find($id);
                $messages = $this->checkAssignToTrainer($trainer, $answer, $translator);
                if (!$messages) {
                    $exercises = $this->getDoctrine()->getRepository('ApiBundle:Exercises')->findOneBy(array(
                        'answer' => $answer, 'trainer' => $trainer
                    ));

                    if (!$exercises) {
                        $exercises = new Exercises();
                    }
                    // save the Exercises object
                    $exercises->setTrainer($trainer);
                    $exercises->setAnswer($answer);
                    $exercises->setStatus(ModuleReports::STATUS_TODO);
                    $exercises->setDateAdd(new DateTime());
                    $em->persist($exercises);
                    $em->flush();
                    // send assigned to exercises email to the trainer
                    $this->get(Email::class)->sendMailAssignedToExercise($trainer, $exercises);
                } else {
                    $flashBag->add('success', $messages);
                }

            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $response = 'ok';
        return new JsonResponse($response, 200);
    }

    public function validationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entityIds = $request->request->get('selectedEntities');
        $type = $request->request->get('type');
        $comment = $request->request->get('comment');
        foreach ($entityIds as $id){
            try {
                $exercises = $this->getDoctrine()->getRepository('ApiBundle:Exercises')->find($id);
                if ($exercises) {
                    // save the Exercises object
                    $status = ($type == 1) ? Exercises::STATUS_APPROVED : Exercises::STATUS_REJECTED;
                    $exercises->setStatus($status);
                    $exercises->setComment($comment);
                    $exercises->setValidationDate(new DateTime());
                    $em->persist($exercises);
                    $em->flush();
                }
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        $response = 'ok';
        return new JsonResponse($response, 200);
    }

    /**
     * check assign a exercises to a trainer.
     *
     * @param Profile $trainer The trainer
     * @param QuizUsersAnswers $answer The entity
     *
     * @return Boolean
     */
    private function checkAssignToTrainer(Profile $trainer, QuizUsersAnswers $answer, $translator)
    {
        $errors = '';
        $skillOrAnd = $answer->getPlay()->getQuiz()->getSkills_or_and();
        $db = $this->getDoctrine()->getManager();

        // Write your raw SQL
        $query = "SELECT skill_id AS skills 
                  FROM `skill_resource_variety_profile` 
                  WHERE status = 'APPROVED' AND profile_id = " . $trainer->getId() ."
                  ORDER BY skill_id";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $trainerSkills = $statementDB->fetchAll(PDO::FETCH_COLUMN, 0);

        // calculation the quiz
        $query = "SELECT skill_id AS skills 
                  FROM `quiz_skills` 
                  WHERE quiz_id = " . $answer->getPlay()->getQuiz()->getId() ."
                  ORDER BY skill_id";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $quizSkills = $statementDB->fetchAll(PDO::FETCH_COLUMN, 0);

        if ($skillOrAnd == 1) {
            if (array_diff($quizSkills, $trainerSkills)) {
                $errors .= $translator->trans('global.trainer_can_not_assign_success', ['%trainer%' => $trainer->getDisplayName()]);
            }
        } else {
            if (!array_intersect($quizSkills, $trainerSkills)) {
                $errors .= $translator->trans('global.trainer_can_not_assign_success', ['%trainer%' => $trainer->getDisplayName()]);
            }
        }
        return $errors;
    }
}
