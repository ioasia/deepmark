<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use DateTime;
use Exception;
use ImportBundle\Entity\ImportUser;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Email;

/**
 * Class AdminsController
 * @package AdminBundle\Controller
 */
class AdminsController extends BaseController
{
    /**
     * Lists all profile entities.
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::ADMIN,
            )
        );
        $admins      = $this->getProfileFromVariety($profileType);

        $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');

        return $this->render('AdminBundle:admins:index.html.twig', array(
            'admins'             => $paginator->paginate($admins, $request->query->getInt('page', 1)),
            'interventionsAdmin' => $interventions,
        ));
    }

    /**
     * Creates a new profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form    = $this->createForm('ApiBundle\Form\ProfileClientType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailProfile = $profile->getPerson()->getEmail();
            $em           = $this->getDoctrine()->getManager();
            $profileType  = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::ADMIN,
                )
            );
            $profile->setProfileType($profileType);
            $profile->getPerson()->setRoles(array('ROLE_ADMIN'));
            $profile->getPerson()->setUsername($emailProfile);
            $profile->getPerson()->setEnabled(true);
            $profile->setCreationDate(new DateTime());
            $profile->setOtherEmail($emailProfile);
            $profile->setUpdateDate(new DateTime());
            $profile->setBillingCode('');
            $profile->setStreet('');

            $em->persist($profile->getPerson());
            $em->persist($profile);

            // Setup reset token
            $token        = $this->container->get('fos_user.util.token_generator');
            $personEntity = $profile->getPerson();
            $personEntity->setPasswordRequestedAt(new \DateTime());
            $personEntity->setConfirmationToken($token->generateToken());
            $em->persist($personEntity);

            $em->flush();
            $this->get(Email::class)->sendMailNewUserRegistration($profile);

            return $this->redirectToRoute('admin_admins_show', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:admins:new.html.twig', array(
            'profile' => $profile,
            'form'    => $form->createView(),
        ));
    }

    /**
     * Upload profile entity.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function uploadAction(Request $request)
    {
        $profile = $this->getCurrentProfile();

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file           = $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            echo $fileDescriptor->getName();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);
            $document->setProfile($profile);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_admins_index'));
        }

        return $this->render('AdminBundle:admins:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     *
     * @param Profile $profile
     *
     * @return Response
     */
    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('AdminBundle:admins:show.html.twig', array(
            'profile'     => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     * @param Request $request
     * @param Profile $profile
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm   = $this->createForm('ApiBundle\Form\ProfileClientEditType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $profile->setUpdateDate(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('admin_admins_edit', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:admins:edit.html.twig', array(
            'profile'     => $profile,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param $id
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $isDelete   = $request->request->get('isDelete');

        $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
        $person     = $profile->getPerson();

        if ($isDelete) {
            $em->remove($profile);
            $em->remove($profile->getPerson());
            $em->flush();
        } else {
            $person->setEnabled(0);
            $em->persist($person);
            $em->flush();
        }

        return $this->redirectToRoute('admin_admins_index');
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_admins_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function showImportAction($role)
    {
        $importUsers = $this->getDoctrine()->getManager()->getRepository(ImportUser::class)->findBy([
            'importType' => $role,
        ]);

        return $this->render('AdminBundle:import:import.html.twig', ['importedUsers' => $importUsers]);
    }
}
