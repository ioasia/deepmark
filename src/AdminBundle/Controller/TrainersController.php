<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileDocument;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\TrainerReservation;
use ApiBundle\Entity\ModuleTrainerResponse;
use ApiBundle\Entity\ModuleNotes;
use ApiBundle\Entity\ModuleAttendanceReport;
use ApiBundle\Entity\ModuleCancel;
use ApiBundle\Entity\WorkingHours;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonTimeZone;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use DateTime;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Email;
use Utils\Import\Users;
use Traits\HasAvailableCalendar;

/**
 * Class TrainersController
 * @package AdminBundle\Controller
 */
class TrainersController extends BaseController
{
    use HasAvailableCalendar;
    /**
     * Count the number of activities executed by the trainer.
     *
     * @param $trainer
     *
     * @return int
     */
    private function getCountActitiesByTrainer($trainer)
    {
        $activities = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy(['liveResource' => $trainer]);

        return count($activities);
    }

    /**
     * Lists all profile entities.
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );

        $conditionArr = array(
            'profileType' => $profileType,
        );

        $roles = $this->getUser()->getRoles();
        if (!in_array('ROLE_SUPER_ADMIN', $roles)) {
            $conditionArr['entity'] = $this->getEntityFromProfile()->getId();
        }

        $trainers = $em->getRepository('ApiBundle:Profile')->findBy($conditionArr);

        $arrTrainer = [];
        foreach ($trainers as $trainer) {
            $trainer->setNbActivitiesExecuted($this->getCountActitiesByTrainer($trainer));
            $ranking      = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($trainer);
            $arrTrainer[] = array('ranking' => $ranking, 'trainer' => $trainer);
        }

        return $this->render('AdminBundle:trainers:index.html.twig', array(
            'trainers' => $arrTrainer,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Creates a new profile entity.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction()
    {
        $em        = $this->getDoctrine()->getManager();
        $profile = $this->getCurrentProfile();
        $timeZones = $em->getRepository(PersonTimeZone::class)->findAll();
        $entities        = $em->getRepository('ApiBundle:Entity')->findBy(['organisation' => $profile->getEntity()->getOrganisation()->getId()]);
        $person    = $profile->getPerson();
        $allSkills = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findAll();
        $statusAll = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceOrigin')->findAll();
        $perimeters = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->findAll();
        $cvTemplateDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::CV_DOCUMENT,
        ));

        return $this->render('AdminBundle:trainers:new.html.twig', array(
            'timeZones' => $timeZones,
            'status'    => $statusAll,
            'skills'    => $allSkills,
            'entities'  => $entities,
            'perimeters'=> $perimeters,
            'adminProfile'  => $profile,
            'cvTemplateDoc' => $cvTemplateDoc,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function uploadProfileInfo(Profile $profile, $targetDir, $file, $type)
    {
        $array      = explode('.', $file['name']);
        $extension  = end($array);
        $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;
        $path       = '/files/profile/' . $fileName;

        if (move_uploaded_file($file['tmp_name'], $targetFile)) {
            $em = $this->getDoctrine()->getManager();
            if ($type == 'avatar') {
                $profile->setAvatar($path);

            } elseif ($type == 'cv') {
                $profile->setCv($path);

            } elseif ($type == 'video') {
                $profile->setVideo($path);
            } elseif ($type == 'iban') {
                $profile->setIban($path);
            }
            $em->persist($profile);
            $em->flush();

        }

        return $fileName;
    }

    public function uploadProfileDocument(Profile $profile, $targetDir, $myFile, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $fileCount = count($myFile["name"]);
        for ($i = 0; $i < $fileCount; $i++) {
            $array      = explode('.', $myFile['name'][$i]);
            $extension  = end($array);
            $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
            $targetFile = $targetDir . $fileName;

            if (move_uploaded_file($myFile['tmp_name'][$i], $targetFile)) {

                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($myFile['name'][$i]);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize($myFile['size'][$i]);
                $fileDescriptor->setMimeType(pathinfo( $myFile['name'][$i], PATHINFO_EXTENSION));
                $fileDescriptor->setDirectory($myFile['tmp_name'][$i]);
                $em->persist($fileDescriptor);

                $profileDocument = new ProfileDocument();
                $profileDocument->setProfile($profile);
                $profileDocument->setFileDescriptor($fileDescriptor);
                $profileDocument->setDocumentType($type);
                $profileDocument->setCreated(new DateTime());
                $profileDocument->setUpdated(new DateTime());
                $em->persist($profileDocument);
                $em->flush();

            }
        }
    }

    public function saveAction(Request $request)
    {
        $roles       = $this->getUser()->getRoles();
        $idSkills    = $request->request->get('newSkills');
        $lastName    = $request->request->get('lastName');
        $firstName   = $request->request->get('firstName');
        $fixedPhone  = $request->request->get('fixedPhone');
        $mobilePhone = $request->request->get('mobilePhone');
        $email       = $request->request->get('email');
        $otherEmail  = $request->request->get('otherEmail');
        $employee_id = $request->request->get('employeeId');
        $password    = $request->request->get('password');
        if (empty($password)) $password = Profile::DEFAULT_PASSWORD;
        $timeZoneId  = $request->request->get('time_zone_id');
        $idEntity    = $request->request->get('entity');
        $idRegional  = $request->request->get('regional');
        $idGroup     = $request->request->get('group');
        $idWorkplace = $request->request->get('workplace');
        $status      = $request->request->get('status'); // liveResourceOrigin_id
        $statusActive= $request->request->get('statusActive');
        $zoneId      = $request->request->get('action_zone');
        $bankName    = $request->request->get('bank_name');
        $currency    = $request->request->get('account_currency');
        $iban        = $request->request->get('iban');
        $bic         = $request->request->get('bic');
        $bankCode    = $request->request->get('bankCode');
        $agencyCode  = $request->request->get('agencyCode');
        $accountNum  = $request->request->get('accountNum');
        $ribKey      = $request->request->get('ribKey');
        $domi        = $request->request->get('domiciliationAgency');
        $em          = $this->getDoctrine()->getManager();
        $checkEmail  = $this->getDoctrine()->getRepository(Person::class)->findOneBy(['email' => $email]);
        $live = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceOrigin')
            ->find($status);
        $perimeters = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->find($zoneId);

        if (!$checkEmail) {

            $timeZone = $this->getDoctrine()->getRepository('AppBundle:PersonTimeZone')
                ->find($timeZoneId);
            $person   = new Person();
            $person->setEmail($email);
            $person->setTimeZone($timeZone);
            $person->setUsername($email);
            $person->setEnabled($statusActive);
            $person->setPlainPassword($password);
            $person->setRoles(array('ROLE_TRAINER'));
            $em->persist($person);

            $profileEntity = new Profile();
            $profileType   = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::TRAINER,
                )
            );
            // the super admin doesn't see the entity select box so it saves the trainer without entity
            //if (!in_array('ROLE_SUPER_ADMIN', $roles)) {
//                $profileEntity->setEntity($this->getEntityFromProfile());
            //}
            $profileEntity->setProfileType($profileType);
            $profileEntity->setPerson($person);
            $profileEntity->setBillingCode('');
            $profileEntity->setCreationDate(new DateTime());
            $profileEntity->setUpdateDate(new DateTime());
            $profileEntity->setEmployeeId($employee_id);
            $profileEntity->setLiveResourceOrigin($live); // save liveResourceOrigin_id
            $profileEntity->setInterventionPerimeter($perimeters); // save interventionPerimeter_id
            $profileEntity->setOtherEmail($otherEmail);
            $profileEntity->setLastName($lastName);
            $profileEntity->setFirstName($firstName);
            $profileEntity->setFixedPhone($fixedPhone);
            $profileEntity->setMobilePhone($mobilePhone);
            //bank info
            $profileEntity->setBankName($bankName);
            $profileEntity->setAccountCurrency($currency);
            $profileEntity->setIban($iban);
            $profileEntity->setBic($bic);
            $profileEntity->setBankCode($bankCode);
            $profileEntity->setAgencyCode($agencyCode);
            $profileEntity->setAccountNum($accountNum);
            $profileEntity->setRibKey($ribKey);
            $profileEntity->setDomiciliationAgency($domi);
            if ($idEntity) {
                $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')
                    ->find($idEntity);
                $profileEntity->setEntity($entity);
                if ($idRegional) {
                    $regional = $this->getDoctrine()->getRepository('ApiBundle:Regional')
                        ->find($idRegional);
                    $profileEntity->setRegional($regional);
                    if ($idGroup) {
                        $group = $this->getDoctrine()->getRepository('ApiBundle:GroupOrganization')
                            ->find($idGroup);
                        $profileEntity->setGroupOrganization($group);
                        if ($idWorkplace) {
                            $workplace = $this->getDoctrine()->getRepository('ApiBundle:Workplace')
                                ->find($idWorkplace);
                            $profileEntity->setWorkplace($workplace);
                        }
                    }
                }
            }
            $em->persist($profileEntity);
            if (!empty($idSkills)) {
                foreach ($idSkills as $idSkill) {
                    $skill               = $this->getDoctrine()->getRepository('ApiBundle:Skill')->find($idSkill);
                    $liveResourceVariety = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
                        'appId' => LiveResourceVariety::LIVE_TRAINER,
                    ));
                    $assign              = new SkillResourceVarietyProfile();
                    $assign->setSkill($skill);
                    $assign->setStatus(SkillResourceVarietyProfile::STATUS_APPROVED);
                    $assign->setProfile($profileEntity);
                    $assign->setScore(0);
                    $assign->setDateAdd(new DateTime());
                    $assign->setLiveResourceVariety($liveResourceVariety);
                    $em->persist($assign);
                }
            }

            $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile/';

            if (isset($_FILES['avatarFile'])) {
                $this->uploadProfileInfo($profileEntity, $targetDir, $_FILES['avatarFile'], 'avatar');
            }

            if (isset($_FILES['videoFile'])) {
                $this->uploadProfileInfo($profileEntity, $targetDir, $_FILES['videoFile'], 'video');
            }

            if (isset($_FILES['cvFile'])) {
                $this->uploadProfileInfo($profileEntity, $targetDir, $_FILES['cvFile'], 'cv');
            }

            if (isset($_FILES['billing'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['billing'],
                    ProfileDocument::BANK_ACCOUNT_STATEMENT);
            }
            if (isset($_FILES['identite'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['identite'],
                    ProfileDocument::IDENTITY_DOCUMENT);
            }
            if (isset($_FILES['social'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['social'],
                    ProfileDocument::SOCIAL_DOCUMENT);
            }
            if (isset($_FILES['assurance'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['assurance'],
                    ProfileDocument::ASSURANCE_DOCUMENT);
            }
            if (isset($_FILES['contrat_cadre'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['contrat_cadre'],
                    ProfileDocument::CONTRACT_DOCUMENT);
            }
            if (isset($_FILES['contrat_mission'])) {
                $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['contrat_mission'],
                    ProfileDocument::MISSION_DOCUMENT);
            }
            // Setup reset token
            $token        = $this->container->get('fos_user.util.token_generator');
            $personEntity = $profileEntity->getPerson();
            $personEntity->setPasswordRequestedAt(new \DateTime());
            $personEntity->setConfirmationToken($token->generateToken());
            $em->persist($personEntity);
            $em->flush();

            // add AvailableCalendar for trainer
            $this->saveAvailableCalendar($request, $profileEntity);

            $this->get(Email::class)->sendMailNewUserRegistration($profileEntity);

            return $this->redirectToRoute('admin_trainers_show', array('id' => $profileEntity->getId()));
        } else {
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('error', 'This email already exists');

            return $this->redirectToRoute('admin_trainers_new');

        }
    }

    public function deleteFileAction(Request $request)
    {
        $fileType  = $request->request->get('fileProfileType') ? $request->request->get('fileProfileType') : null;
        $fileId    = $request->request->get('fileId');
        $profileId = $request->request->get('profileId');
        $em        = $this->getDoctrine()->getManager();

        if ($fileType) {
            $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
            $profile   = $document = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($profileId);
            if ($fileType == 'avatar') {
                if (file_exists($targetDir . $profile->getAvatar())) {
                    unlink($targetDir . $profile->getAvatar());
                    $profile->setAvatar(null);
                }
            } elseif ($fileType == 'video') {
                if (file_exists($targetDir . $profile->getVideo())) {
                    unlink($targetDir . $profile->getVideo());
                    $profile->setVideo(null);
                }
            } elseif ($fileType == 'iban') {
                if (file_exists($targetDir . $profile->getIban())) {
                    unlink($targetDir . $profile->getIban());
                    $profile->setIban(null);
                }
            } elseif ($fileType == 'cv') {
                if (file_exists($targetDir . $profile->getCv())) {
                    unlink($targetDir . $profile->getCv());
                    $profile->setCv(null);
                }
            }
            $em->flush();

        } else {
            $fileDescriptor = $document = $this->getDoctrine()->getRepository('ApiBundle:FileDescriptor')->find($fileId);
            $documents      = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array('fileDescriptor' => $fileDescriptor));
            foreach ($documents as $document) {
                $em->remove($document);
            }
            $em->remove($fileDescriptor);
            $em->flush();
        }

        return $this->redirectToRoute('admin_trainers_edit', array('id' => $profileId));
    }

    public function saveEditAction(Request $request, Profile $profile)
    {
        $person      = $profile->getPerson();
        $roles       = $this->getUser()->getRoles();
        $lastName    = $request->request->get('lastName');
        $firstName   = $request->request->get('firstName');
        $fixedPhone  = $request->request->get('fixedPhone');
        $mobilePhone = $request->request->get('mobilePhone');
        $email       = $request->request->get('email');
        $otherEmail  = $request->request->get('otherEmail');
        $status      = $request->request->get('status'); // liveResourceOrigin_id
        $employee_id = $request->request->get('employeeId');
        $password    = $request->request->get('password');
        $statusActive= $request->request->get('statusActive'); 
        $idEntity    = $request->request->get('entity');
        $idRegional  = $request->request->get('regional');
        $idGroup     = $request->request->get('group');
        $idWorkplace = $request->request->get('workplace');
        $timeZoneId  = $request->request->get('time_zone_id');
        $zoneId      = $request->request->get('action_zone');
        $idSkills    = $request->request->get('skills');
        // Thông tin thanh toán
        $bankName    = $request->request->get('bank_name');
        $currency    = $request->request->get('account_currency');
        $iban        = $request->request->get('iban');
        $bic         = $request->request->get('bic');
        $bankCode    = $request->request->get('bankCode');
        $agencyCode  = $request->request->get('agencyCode');
        $accountNum  = $request->request->get('accountNum');
        $ribKey      = $request->request->get('ribKey');
        $domi        = $request->request->get('domiciliationAgency');

        $live = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceOrigin')
                ->find($status);
        $perimeters = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->find($zoneId);

        $checkEmail = $this->getDoctrine()->getRepository(Person::class)->findOneBy(['email' => $email]);
        $em         = $this->getDoctrine()->getManager();

        if (trim($email) == trim($person->getEmail()) || !$checkEmail) {
            $timeZone = $this->getDoctrine()->getRepository('AppBundle:PersonTimeZone')
                ->find($timeZoneId);
            $person->setEmail($email);
            $person->setEnabled($statusActive);
            $person->setPlainPassword($password);
            $person->setTimeZone($timeZone);
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updatePassword($person);
            $em->persist($person);
        } else {
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('error', 'This email already exists');

            return $this->redirectToRoute('admin_learners_edit', array('id' => $profile->getId()));
        }
        

        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );

        $profile->setProfileType($profileType);
        $profile->setPerson($person);
        $profile->setCreationDate(new DateTime());
        $profile->setUpdateDate(new DateTime());
        $profile->setBillingCode('');
        $profile->setStreet('');
        $profile->setLastName($lastName);
        $profile->setFirstName($firstName);
        $profile->setLiveResourceOrigin($live); // save liveResourceOrigin_id
        $profile->setInterventionPerimeter($perimeters); // save interventionPerimeter_id
        $profile->setFixedPhone($fixedPhone);
        $profile->setMobilePhone($mobilePhone);
        $profile->setEmployeeId($employee_id);

        // Thông tin thanh toán
        $profile->setBankName($bankName);
        $profile->setAccountCurrency($currency);
        $profile->setIban($iban);
        $profile->setBic($bic);
        $profile->setBankCode($bankCode);
        $profile->setAgencyCode($agencyCode);
        $profile->setAccountNum($accountNum);
        $profile->setRibKey($ribKey);
        $profile->setDomiciliationAgency($domi);

        if ($idEntity) {
            $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')
                ->find($idEntity);
            $profile->setEntity($entity);
            $profile->setRegional(null);
            $profile->setGroupOrganization(null);
            $profile->setWorkplace(null);
            if ($idRegional) {
                $regional = $this->getDoctrine()->getRepository('ApiBundle:Regional')
                    ->find($idRegional);
                $profile->setRegional($regional);
                if ($idGroup) {
                    $group = $this->getDoctrine()->getRepository('ApiBundle:GroupOrganization')
                        ->find($idGroup);
                    $profile->setGroupOrganization($group);
                    if ($idWorkplace) {
                        $workplace = $this->getDoctrine()->getRepository('ApiBundle:Workplace')
                            ->find($idWorkplace);
                        $profile->setWorkplace($workplace);    
                    }
                }
            }
        } else {
            $profile->setEntity($this->getEntityFromProfile());
        }

        if ($request->request->get('lunch_start_time') && $request->request->get('lunch_end_time')) {
            $now            = new DateTime('now');
            $lunchStartTime = explode(":", $request->request->get('lunch_start_time'));
            $lunchEndTime   = explode(":", $request->request->get('lunch_end_time'));
            $profile->setLunchStartTime(clone $now->setTime($lunchStartTime[0], $lunchStartTime[1]));
            $profile->setLunchEndTime(clone $now->setTime($lunchEndTime[0], $lunchEndTime[1]));
        }
        $em->persist($profile);

        if (!empty($idSkills)) {

            $oldSkills   = $em->getRepository('ApiBundle:SkillResourceVarietyProfile')
                ->findBy(['profile' => $profile]);
            $oldSkillIds = [];

            foreach ($oldSkills as $skill) {
                $id = $skill->getSkill()->getId();

                if (!in_array($id, $idSkills)) {
                    $em->remove($skill);
                } else {
                    $oldSkillIds[] = $id;
                }
            }

            foreach ($idSkills as $idSkill) {
                if (!in_array($idSkill, $oldSkillIds)) {
                    $skill               = $this->getDoctrine()->getRepository('ApiBundle:Skill')->find($idSkill);
                    $liveResourceVariety = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
                        'appId' => LiveResourceVariety::LIVE_TRAINER,
                    ));
                    $assign              = new SkillResourceVarietyProfile();
                    $assign->setSkill($skill);
                    $assign->setProfile($profile);
                    $assign->setScore(0);
                    $assign->setDateAdd(new DateTime());
                    $assign->setLiveResourceVariety($liveResourceVariety);
                    $em->persist($assign);
                }
            }
        }

        $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile/';

        if (isset($_FILES['avatarFile'])) {
            $this->uploadProfileInfo($profile, $targetDir, $_FILES['avatarFile'], 'avatar');
        }

        if (isset($_FILES['videoFile'])) {
            $this->uploadProfileInfo($profile, $targetDir, $_FILES['videoFile'], 'video');
        }

        if (isset($_FILES['cvFile'])) {
            $this->uploadProfileInfo($profile, $targetDir, $_FILES['cvFile'], 'cv');
        }

        if (isset($_FILES['billing'])) {
            $this->uploadProfileInfo($profile, $targetDir, $_FILES['billing'], 'iban');
        }

        $em->flush();
        $flashbag = $this->get('session')->getFlashBag();
        $flashbag->add('success', 'All changed successfully');

        return $this->redirectToRoute('admin_trainers_edit', array('id' => $profile->getId()));
    }

    /**
     * Upload profile entity
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function uploadAction(Request $request)
    {
        $profile = $this->getCurrentProfile();

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if (!$form->isSubmitted() || !$form->isValid()) {
            $fields = array_merge($this->get(Users::class)->getFields(),
                [
                    'liveResource' => false,
                    'perimeter' => false,
                    'regional'  => false,
                    'group_org' => false,
                    'workplace' => false,
                    'skill_1' => false,
                    'skill_2' => false,
                    'skill_3' => false,
                    'skill_4' => false,
                    'skill_5' => false
                ]);
            unset($fields['otherEmail']);
            unset($fields['group']);
            $fieldSorted = array_replace(
                array_flip(array('employeeId', 'lastName', 'firstName', 'position', 'email', 'fixedPhone', 'mobilePhone', 'managerEmail', 'liveResource', 'perimeter', 'skill_1', 'skill_2', 'skill_3', 'skill_4', 'skill_5', 'organization', 'entity', 'regional', 'group_org', 'workplace')),
                $fields);
            return $this->render('AdminBundle:learners:upload.html.twig', array(
                'form'         => $form->createView(),
                'headers'      => [],
                // Import fields
                'requirements' => [
                    'post_max_size'       => ini_get('post_max_size'),
                    'upload_max_filesize' => ini_get('upload_max_filesize'),
                ],
                'importType'   => 'trainers',
                'fields'       => $fieldSorted,
            ));
        }

        /**
         * @TODO Improve mapped fields allow use duplicate fields
         */
        if ($mappedFields = $request->get('mapped_fields')) {
            /**
             * @var Users $importUsers
             */
            $importUsers = $this->get(Users::class);
            $importUsers->loadFile($request->files->get('file_descriptor')['path']->getPathname(),
                $request->get('delimiter'));

            foreach ($mappedFields as $field => $header) {
                $importUsers->setMappedField($header, $field);
            }

            // Default profile entity from current user
            $importUsers->setProfileEntity($this->getEntityFromProfile());
            $importUsers->process(ProfileVariety::TRAINER);
            $errors = $importUsers->getErrors();

            if (empty($errors)) {
                return $this->redirect($this->generateUrl('admin_trainers_index'));
            }

            foreach ($errors as $error) {
                $this->addFlash('error', $error);
            }

            return $this->redirect($this->generateUrl('admin_trainers_index'));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $request->files->get('file_descriptor')['path'];
            $fileDescriptor->setName($file->getFilename());
            $fileDescriptor->setDirectory($file->getPath());
            $fileDescriptor->setMimeType($file->getMimeType());
            $fileDescriptor->setSize($file->getSize());

            $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);
            $em             = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);
            $document->setProfile($profile);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];
            $appId    = EducationalDocVariety::DOCUMENT;

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                case 'application':
                    $relativePath = $fileDescriptor->getDirectory() . '/' . $fileDescriptor->getPath();
                    $response     = $this->forward('ImportBundle:TrainerImport:importUser', [
                        'path' => $relativePath,
                    ]);

                    $response = json_decode($response->getContent());

                    if (!empty($response->errors)) {
                        $flashbag = $this->get('session')->getFlashBag();
                        $flashbag->add('danger', $response->message);

                        return $this->render('AdminBundle:trainers:upload.html.twig', array(
                            'form' => $form->createView(),
                        ));
                    }
                    $flashbag = $this->get('session')->getFlashBag();
                    $flashbag->add('notice', 'Import successfully completed');

                    return $this->redirect($this->generateUrl('admin_trainers_import', ['role' => 'trainer']));
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_trainers_index', ['role' => 'trainer']));
        }

        return $this->render('AdminBundle:trainers:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function showAction($id, $isNew = false)
    {
        $profile = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->find($id);

        $bookingsToday = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findBookingForToday($profile);

        //$bookingsWeek = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
        //    ->findBookingWeek($profile);

        //$bookingsMonth = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
        //    ->findBookingMonth($profile);

        $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
            'liveResource' => $profile,
        ));
        $skillNames        = [];
        $skills            = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['profile' => $profile]);
        foreach ($skills as $skill) {
            $skillNames[] = $skill->getSkill()->getDesignation();
        }
        $ranking = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($profile);

        return $this->render('AdminBundle:trainers:show.html.twig', array(
            'skillNames'    => $skillNames,
            'ranking'       => $ranking,
            'profile'       => $profile,
            'bookingsDay'   => $bookingsToday,
            'bookingsWeek'  => [],
            'bookingsMonth' => [],
            'assignments'   => $assignmentTrainer,
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Profile $profile)
    {   
        $em        = $this->getDoctrine()->getManager();
        // bộ phận cho select 
        $entities        = $em->getRepository('ApiBundle:Entity')->findBy(['organisation' => $profile->getEntity()->getOrganisation()->getId()]);
        $roles     = $this->getUser()->getRoles();
        $timeZones = $em->getRepository(PersonTimeZone::class)->findAll();
        $person    = $profile->getPerson();
        $skills    = $em->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['profile' => $profile]);
        $skillIds  = [];

        foreach ($skills as $skill) {
            $skillIds[] = $skill->getSkill()->getId();
        }

        $allSkills = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findAll();
        $identityDoc   = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::IDENTITY_DOCUMENT,
            'profile'      => $profile,
        ));
        $socialDoc     = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::SOCIAL_DOCUMENT,
            'profile'      => $profile,
        ));
        $assuranceDoc  = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::ASSURANCE_DOCUMENT,
            'profile'      => $profile,
        ));
        $contractDoc   = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::CONTRACT_DOCUMENT,
            'profile'      => $profile,
        ));
        $missionDocs   = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::MISSION_DOCUMENT,
            'profile'      => $profile,
        ));
        $statement   = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::BANK_ACCOUNT_STATEMENT,
            'profile'      => $profile,
        ));
        $cvTemplateDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::CV_DOCUMENT,
        ));

        $extIban = $profile->getIban() != null ? pathinfo($profile->getIban(), PATHINFO_EXTENSION) : null;
        $rankingGrades = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->findBy(array(
            'trainer' => $profile,
        ));
        if ($rankingGrades) {
            $grade = 0;
            foreach ($rankingGrades as $k => $item) {
                $grade += $item->getNotation();
            }
            $ranking['grade'] = round($grade / ($k + 1), 2);
        }
        $status = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceOrigin')->findAll();
        $fileFormatCv = $fileFormatVideo = '';
        if($profile->getCv()){
            $cvInfo = pathinfo($profile->getCv());
            $fileFormatCv = $cvInfo['extension'];
        }
        if($profile->getVideo()){
            $videoInfo = pathinfo($profile->getVideo());
            $fileFormatVideo = $videoInfo['extension'];
        }
        $perimeters = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->findAll();

        $workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);

        $live_skills = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')->findBy(array(
            'profile' => $profile,
            'liveResourceVariety' => $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
                    'appId' => LiveResourceVariety::LIVE_TRAINER,
                )
            ),
        ));
        return $this->render('AdminBundle:trainers:edit.html.twig', array(
            'profile'       => $profile,
            'person'        => $person,
            'timeZones'     => $timeZones,
            'trainerSkills' => $skillIds,
            'skills'        => $allSkills,
            'identityDoc'   => $identityDoc,
            'socialDoc'     => $socialDoc,
            'assuranceDoc'  => $assuranceDoc,
            'contractDoc'   => $contractDoc,
            'missionDocs'   => $missionDocs,
            'cvTemplateDoc' => $cvTemplateDoc,
            'statement'     => $statement,
            'extIban'       => $extIban,
            'rankingGrades' => $rankingGrades,
            'status' => $status,
            'fileFormatCv' => $fileFormatCv,
            'fileFormatVideo' => $fileFormatVideo,
            'perimeters' => $perimeters,
            'workingHours' => $workingHours,
            'live_skills' => $live_skills,
            'entities'        => $entities,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
            // 'regionals'       => $regionals,
        ));
    }
    /**
     * @return ObjectRepository
     */
    // private function getSkillResourceVarietyProfileRepository()
    // {
    //     return 
    // }
    /**
     * @param $appId
     *
     * @return LiveResourceVariety|object
     */
    private function getLiveResourceVarietyByAppId($appId)
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
            'appId' => $appId,
        ));
    }

    public function deleteTrainersAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $trainerIds = $request->request->get('selectedProfiles');

        foreach ($trainerIds as $id){
            $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
            $person     = $profile->getPerson();
            if ($type == 1) {
                try {
                    $this->deleteTrainer($person);
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            } else {
                $person->setEnabled(0);
                $em->persist($person);
                $em->flush();
            }
        }
        return $this->redirectToRoute('admin_trainers_index');
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param $id
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $isDelete   = $request->request->get('isDelete');

        $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
        $person     = $profile->getPerson();

        if ($isDelete) {
            $this->deleteTrainer($person);
        } else {
            $person->setEnabled(0);
            $em->persist($person);
            $em->flush();
        }

        return $this->redirectToRoute('admin_trainers_index');
    }

    /**
     * Delete a learner.
     *
     * @param Profile $profile The profile entity
     *
     * @return Boolean
     */
    private function deleteTrainer(Person $user)
    {
        $em                     = $this->getDoctrine()->getManager();
        $profile                = $em->getRepository(Profile::class)->findOneBy(['person' => $user]);
        $workHours              = $em->getRepository(WorkingHours::class)->findBy(['profile' => $profile]);
        $calendars              = $em->getRepository(AvailableCalendar::class)->findBy(['profile' => $profile]);
        $learnerVerieties       = $em->getRepository(SkillResourceVarietyProfile::class)->findBy(['profile' => $profile]);
        $learnerDocuments       = $em->getRepository(ProfileDocument::class)->findBy(['profile' => $profile]);
        $learnerActivities      = $em->getRepository(Activities::class)->findBy(['profile' => $profile]);
        $trainerReservations    = $em->getRepository(TrainerReservation::class)->findBy(['profile' => $profile]);
        $moduleNotes            = $em->getRepository(ModuleNotes::class)->findBy(['trainer' => $profile]);
        $bookingAgendas         = $em->getRepository(BookingAgenda::class)->findBy(['trainer' => $profile]);
        $assignments            = $em->getRepository(AssigmentResourceSystem::class)->findBy(['liveResource' => $profile]);
        $moduleTrainerResponses = $em->getRepository(ModuleTrainerResponse::class)->findBy(['trainer' => $profile]);
        $attendanceReports      = $em->getRepository(ModuleAttendanceReport::class)->findBy(['trainer' => $profile]);
        $moduleCancels          = $em->getRepository(ModuleCancel::class)->findBy(['trainer' => $profile]);

        foreach ($workHours as $workHour) {
            if ($workHour) {
                $em->remove($workHour);
            }
        }


        foreach ($calendars as $calendar) {
            if ($calendar) {
                $em->remove($calendar);
            }
        }

        foreach ($learnerVerieties as $learnerVeriety) {
            if ($learnerVeriety) {
                $em->remove($learnerVeriety);
            }
        }

        foreach ($learnerDocuments as $learnerDocument) {
            if ($learnerDocument) {
                $em->remove($learnerDocument);
            }
        }

        foreach ($learnerActivities as $learnerActivity) {
            if ($learnerActivity) {
                $em->remove($learnerActivity);
            }
        }

        foreach ($trainerReservations as $trainerReservation) {
            if ($trainerReservation) {
                $em->remove($trainerReservation);
            }
        }

        foreach ($moduleNotes as $moduleNote) {
            if ($moduleNote) {
                $em->remove($moduleNote);
            }
        }

        foreach ($moduleCancels as $moduleCancel) {
            if ($moduleCancel) {
                $em->remove($moduleCancel);
            }
        }

        foreach ($bookingAgendas as $bookingAgenda) {
            if ($bookingAgenda) {
                $em->remove($bookingAgenda);
            }
        }

        foreach ($assignments as $assignment) {
            if ($assignment) {
                $em->remove($assignment);
            }
        }

        foreach ($moduleTrainerResponses as $moduleTrainerResponse) {
            if ($moduleTrainerResponse) {
                $em->remove($moduleTrainerResponse);
            }
        }

        foreach ($attendanceReports as $attendanceReport) {
            if ($attendanceReport) {
                $em->remove($attendanceReport);
            }
        }

        $em->remove($user);
        $em->remove($profile);
        $em->flush();
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_trainers_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit CV template for the trainer.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function cvTemplateAction(Request $request)
    {
        $cvTemplate = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::CV_DOCUMENT,
        ));

        return $this->render('AdminBundle:trainers:cv-template.html.twig', array(
            'cvTemplate' => $cvTemplate ? $cvTemplate : null,
        ));
    }

    public function cvUploadAction(Request $request)
    {
        $profileFolder = '/files/profile/';
        $targetDir     = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . $profileFolder;

        $fileTemp   = isset($_FILES['cvFile']) ? $_FILES['cvFile'] : null;
        $array      = explode('.', $fileTemp['name']);
        $extension  = end($array);
        $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;

        if (move_uploaded_file($fileTemp['tmp_name'], $targetFile)) {
            $em = $this->getDoctrine()->getManager();

            $fileDescriptor = new FileDescriptor();
            $fileDescriptor->setName($fileTemp['name']);
            $fileDescriptor->setPath($fileName);
            $fileDescriptor->setSize($fileTemp['size']);
            $fileDescriptor->setMimeType($fileTemp['type']);
            $fileDescriptor->setDirectory($targetDir);
            $em->persist($fileDescriptor);

            $profileDocument = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
                'documentType' => ProfileDocument::CV_DOCUMENT,
            ));

            $profileDocument = $profileDocument ? $profileDocument : new ProfileDocument();
            $profileDocument->setFileDescriptor($fileDescriptor);
            $profileDocument->setDocumentType(ProfileDocument::CV_DOCUMENT);
            $profileDocument->setCreated(new DateTime());
            $profileDocument->setUpdated(new DateTime());
            $em->persist($profileDocument);
            $em->flush();

            $response = new Response(json_encode(array(
                "uploaded"  => 'cv template',
                'file_name' => $profileFolder . $fileName,
            )));
        } else {
            $response = new Response(json_encode(array("uploaded" => false, 'file_name' => 'File upload failed')));
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Process ajax upload import file
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function ajaxUploadAction(Request $request)
    {
        $type = $request->get('task');

        switch ($type) {
            case 'uploadFile':
                /**
                 * @var UploadedFile $file
                 */
                $file = $request->files->get('file');

                if ($file->getError() !== 0) {
                    return $this->json([
                        'error' => $file->getError(),
                        'msg'   => 'File error',
                    ]);
                }

                /**
                 * @var Users $importer
                 */
                $importer  = $this->get(Users::class)->loadFile($file->getPathname(), $request->get('delimiter'));
                $sign      = md5_file($file->getPathname()) . $this->getUser()->getId();
                $uploadDir = $this->get('kernel')->getRootDir() . '/../var/uploads';

                if (!file_exists($uploadDir) || !is_dir($uploadDir)) {
                    mkdir($uploadDir);
                }

                move_uploaded_file($file->getPathname(), $uploadDir . '/' . $sign);

                /**
                 * Return parsed header from import file
                 */
                return $this->json([
                    'error' => $file->getError(),
                    'msg'   => '',
                    'data'  => [
                        'headers' => $importer->getHeaders(),
                        'sign'    => $sign,
                    ],
                ]);

                break;
            case 'importUsers':
                $uploadedDirFile = $this->get('kernel')->getRootDir() . '/../var/uploads/' . $request->get('sign');

                if (!file_exists($uploadedDirFile)) {
                    return $this->json([
                        'error' => true,
                        'msg'   => 'File not found',
                    ]);
                }
                /**
                 * @var Users $importer
                 */
                $importer = $this->get(Users::class)->loadFile(
                    $uploadedDirFile,
                    $request->get('delimiter')
                );

                /**
                 * Temporary code for importing
                 */
                $importer->setMappedFields($request->get('mappedFields'));
                $importer->process(ProfileVariety::TRAINER);

                unlink($uploadedDirFile);

                return $this->json([
                    'redirect' => $this->generateUrl('admin_learners_index'),
                    'console'  => $importer->getErrors(),
                ]);
        }
    }

}
