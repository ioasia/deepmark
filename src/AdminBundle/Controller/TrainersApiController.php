<?php

namespace AdminBundle\Controller;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Services\TrainerBooking\TrainerBookingService;
use ApiBundle\Controller\RestController;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Repository\AssigmentResourceSystemRepository;
use AppBundle\Service\CalendarApi;
use AppBundle\Utils\UserTimeZone;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use Traits\Controller\HasUsers;
use Traits\IsAvailable;
use Utils\Email;

/**
 * TrainersApi controller.
 */
class TrainersApiController extends RestController
{
    use HasUsers;
    use IsAvailable;
    /**
     * @Rest\Get("calendars/{idProfile}")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param $idProfile
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Rest\QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=false, description="Date End")
     */
    public function getAvailabilitiesProfileAction($idProfile, ParamFetcher $paramFetcher)
    {
        $dateStart = new \DateTime($paramFetcher->get('dateStart'));
        $dateEnd   = new \DateTime($paramFetcher->get('dateEnd'));

        $calendarApi = $this->get(CalendarApi::class);

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->find($idProfile);

        if (!$profile) {
            return $this->errorHandler();
        }

        $calendar_array = $calendarApi->getBookingsByTrainer($profile, $dateStart, $dateEnd);

        $calendar_array = array_merge($calendar_array,
            $calendarApi->getAvailabilityByTrainer($profile, $dateStart, $dateEnd));

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("available/{sessionId}")
     * GET trainers
     *
     * @param $sessionId
     * @param TrainerBookingService $trainerBookingService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function getAvailableTrainersAction($sessionId, TrainerBookingService $trainerBookingService)
    {
        $step              = $this->getInterventionStep($sessionId);
        $availableTrainers = $trainerBookingService->getAvailableTrainers($step);

        return $this->sendResponse($availableTrainers);
    }

    /**
     * @Rest\Get("available/module/{sessionId}")
     * GET trainers
     * @Rest\QueryParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     *
     * @param $sessionId
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function getAvailableTrainersByModuleAction(
        $sessionId,
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $moduleId = $paramFetcher->get('moduleId');
        $step     = $this->getInterventionStep($sessionId);

        if (!$step) {
            return $this->errorHandler();
        }
        $stepData                  = $step->getCombinedSteps();
        $moduleData                = $stepData['module'][$moduleId];
        $moduleData['participant'] = $stepData['participant'];

        $availableTrainers = $trainerBookingService->getAvailableTrainerForModule($moduleId, $moduleData);

        return $this->sendResponse($availableTrainers);
    }

    /**
     * @Rest\Get("available/module/custom/{moduleId}")
     * GET trainers
     *
     * @param $moduleId
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function getAvailableTrainersByModuleCustomAction(
        $moduleId,
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        /* Example module data
         TrainersApiController.php on line 106:
            array:24 [▼
              "title" => "E"
              "color" => "lightblue"
              "icon" => "user"
              "displayTitle" => "Classroom training"
              "appId" => "9"
              "quantity" => "1"
              "duration" => "10"
              "time_param" => "0"
              "price" => "10"
              "bookable" => "1"
              "instruction" => "<p>E</p>\r\n"
              "skills" => array:1 [▶]
              "allSkills" => "and"
              "signatureNumber" => "1"
              "signatureAfternoon" => "1"
              "isNeedReport" => "1"
              "profileSendTo" => array:1 [▶]
              "file" => array:5 [▶]
              "isPersonalBooking" => "1"
              "session" => array:1 [▼
                1 => array:10 [▼
                  "unique-session-id" => "20191017085905"
                  "startDate" => "23/10/2019"
                  "startTime" => "10:00"
                  "participants" => "1"
                  "participants_max" => "10"
                  "place" => "123"
                  "autocomplete" => "14 Phan Văn Sửu, Ward 13, Thành phố Hồ Chí Minh, Vietnam"
                  "address" => "14 Phan Văn Sửu, Ward 13, Thành phố Hồ Chí Minh, Vietnam"
                  "city" => "Thành phố Hồ Chí Minh"
                  "session_time_finish" => ""
                ]
              ]
              "startDate" => "17/10/2019"
              "endDate" => "31/10/2019"
              "durationHours" => "0"
              "durationMinutes" => "10"
            ]
         */
        $moduleData = $this->getModuleData($moduleId);

        $availableTrainers = $trainerBookingService->getAvailableTrainerForModule($moduleId, $moduleData);

        return $this->sendResponse($availableTrainers);
    }

    /**
     * @Rest\Get("available/module/{moudleId}/{profileId}")
     * GET trainers
     * @param $moudleId
     * @param $profileId
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function getAvailableTrainersByModuleAndProfileAction(
        $moudleId,
        $profileId,
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $availableTrainers = $trainerBookingService->getAvailableTrainerForModuleAndProfile($moudleId, $profileId);

        return $this->sendResponse($availableTrainers);
    }

    /**
     * @Rest\Post("assignTrainerSessions")
     *
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     * @param TranslatorInterface $translator
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     * @Rest\RequestParam(name="sessionIds", nullable=false, strict=true, description="sessionIds")
     * @Rest\RequestParam(name="trainerId", nullable=true, strict=true, description="trainer id")
     * @Rest\RequestParam(name="action", nullable=false, strict=true, description="action can be assign or remove")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function assignTrainerSessionsAction(ParamFetcher $paramFetcher, TrainerBookingService $trainerBookingService, TranslatorInterface $translator)
    {
        $moduleId    = $paramFetcher->get('moduleId');
        $sessionIds = $paramFetcher->get('sessionIds');
        $trainerId = $paramFetcher->get('trainerId');
        $action = $paramFetcher->get('action');
        if(empty($sessionIds)){
            return $this->errorHandler('empty sessions');
        }
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->find($moduleId);

        if($action == "assign") {
            $canNotBookSessions = $trainerBookingService->validationTrainerWithSessions($module, $sessionIds, $trainerId);
            if(!empty($canNotBookSessions)){
                return $this->errorHandler($translator->trans('booking.session_messages.trainer_not_available_session', ['%sessions%' => implode(", ", $canNotBookSessions)]));
            }
            $trainer = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($trainerId);
        }
        $moduleSessions = $module->getSessions();
        $assignedTrainers = NULL;
        $em = $this->getDoctrine()->getManager();
        $storedModule = $module->getStoredModule()->getAppId();
        $classRoomTraining = StoredModule::PRESENTATION_ANIMATION == $storedModule ? true : false;
        foreach ($moduleSessions as $session){
            if(!in_array($session->getId(), $sessionIds)){
                continue;
            }
            $sessionId = $session->getId();
            $start = $session->getSession()->getSessionDate();
            $end = clone $start;
            $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));
            if($action === 'remove') {
                // request replace instead of remove
                $oneBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy([
                    'module'  => $module,
                    'moduleSession' => $session
                ]);
                $trainerBookingService->requestReplacementTrainerSession($oneBookingAgenda, $session->getSession());
                // backup code remove trainer session
                /*$assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                    ->findOneBy(['module' => $moduleId, 'moduleSession' => $sessionId]);
                if($assignment){
                    $assignment->setLiveResource(NULL);
                    $trainerReservation = $this->getDoctrine()->getRepository('ApiBundle:TrainerReservation')->findOneBy(
                        ['module_unique' => $moduleId, 'session_unique' => $sessionId]
                    );
                    if($trainerReservation){
                        $em->remove($trainerReservation);
                    }
                }

                $bookingsAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                    'module'  => $module,
                    'moduleSession' => $session
                ]);
                if($bookingsAgenda){
                    foreach ($bookingsAgenda as $bookingAgenda){
                        $bookingAgenda->setTrainer(NULL);
                        $em->persist($bookingAgenda);
                    }
                }*/

            }else if($action === 'assign') {
                if ($classRoomTraining) {
                    $availableBookClassroom = $this->isAvailableBookingClassroomTraining($trainer, $module,
                        $session, clone $start);
                    if(!$availableBookClassroom){
                        return $this->errorHandler($translator->trans('booking.session_messages.difference_place_trainer_session'));
                    }
                }
                $assignedTrainers = $trainerBookingService->assignTrainerForSession($moduleId, $sessionId, $trainerId, $start, $end);
                if ($assignedTrainers) {
                    $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                        ->findOneBy(['module' => $moduleId, 'moduleSession' => $sessionId]);
                    $trainer = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($trainerId);

                    if (!$assignment) {
                        $assignment = new AssigmentResourceSystem();
                        $assignment->setModule($module);
                        $assignment->setLiveResource($trainer);
                        $assignment->setMinBookingDate($start);
                        $assignment->setMaxBookingDate($end);
                        $assignment->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(['appId' => Status::WITH_BOOKING_SCHEDULED]));
                        $assignment->setModuleSession($session);
                    }else {
                        $assignment->setLiveResource($trainer);
                    }

                    $bookingsAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                        'module'  => $module,
                        'moduleSession' => $session
                    ]);
                    if($bookingsAgenda){
                        foreach ($bookingsAgenda as $bookingAgenda){
                            $bookingAgenda->setTrainer($trainer);
                            $em->persist($bookingAgenda);
                        }
                    }

                    $em->persist($assignment);

                    $intervention = $assignment->getModule()->getIntervention();
                    $modules = $intervention->getModules();
                    $hours = 0;
                    $minutes = 0;

                    foreach ($modules as $module) {
                        if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                            StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                            StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                            StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                        ) {
                            continue;
                        }
                        $duration = clone $module->getDuration();
                        $hours += intval($duration->format('H'));
                        $minutes += intval($duration->format('i'));
                    }
                    $hours += floor($minutes / 60);
                    $minutes = $minutes % 60;
                    $this->get(Email::class)->sendMailCourseAssignment($assignment, $intervention, [$hours, $minutes]);
                    $this->get(Email::class)->sendMailSessionAssignment($assignment, $intervention, [$hours, $minutes]);
                    // send bookingConfirm email when have atleast one learner
                    if($bookingsAgenda){
                        $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingsAgenda[0]);
                    }
                    // send email to all learners who has assigned to the intervention
                    //$this->sendCourseModification($intervention, $hours . ':' . $minutes);
                }
            }
            $em->flush();
        }

        return $this->sendResponse('success');
    }

    /**
     * @Rest\Post("assign/online/{sessionId}")
     *
     * @param $sessionId
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     * @Rest\RequestParam(name="trainersIds", nullable=false, strict=true, description="Trainers id. Separated by comma")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function assignTrainersForOnlineModuleAction(
        $sessionId,
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $moduleId    = $paramFetcher->get('moduleId');
        $trainersIds = $paramFetcher->get('trainersIds');
        $step        = $this->getInterventionStep($sessionId);

        if (!$step) {
            return $this->errorHandler();
        }
        $stepData   = $step->getCombinedSteps();
        $moduleData = $stepData['module'][$moduleId];

        $startDate = \DateTime::createFromFormat('d/m/Y', $moduleData['startDate'], UserTimeZone::getTimeZoneObject());
        $endDate   = \DateTime::createFromFormat('d/m/Y', $moduleData['endDate'], UserTimeZone::getTimeZoneObject());
        $view      = View::create();
        try {
            $trainerBookingService->assignTrainersForOnline($moduleId, $trainersIds, $startDate, $endDate);
            $moduleDatesNotAssign = $trainerBookingService->getTrainersAvailableDatesWithModule($trainersIds, $startDate, $endDate);
            $view->setData($moduleDatesNotAssign);
            $view->setStatusCode(200);
        } catch (\InvalidArgumentException $ex) {
            $view->setData($ex->getMessage());
            $view->setStatusCode(500);
        }

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("assign/trainer/online/custom")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     * @Rest\RequestParam(name="trainersIds", nullable=false, strict=true, description="Trainers id. Separated by comma")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function assignTrainersForOnlineModuleCustomAction(
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $moduleId    = $paramFetcher->get('moduleId');
        $trainersIds = $paramFetcher->get('trainersIds');

        if ($trainersIds) {
            // calculate the duration of the intervention
            $currentModule = $this->getDoctrine()->getRepository('ApiBundle:Module')
                ->find($moduleId);
            $intervention  = $currentModule->getIntervention();
            $modules       = $intervention->getModules();
            $hours         = 0;
            $minutes       = 0;

            $em = $this->getDoctrine()->getManager();

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $duration = clone $module->getDuration();
                $hours    += intval($duration->format('H'));
                $minutes  += intval($duration->format('i'));
            }

            $hours   += floor($minutes / 60);
            $minutes = $minutes % 60;

            // remove all records of this module
            $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->removeAssignments($moduleId);
            // add records with chosen trainer
            foreach ($trainersIds as $trainersId) {
                $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                    ->findOneBy(['module' => $moduleId, 'liveResource' => $trainersId]);

                if (!$assignment) {
                    $assignment = new AssigmentResourceSystem();
                    $assignment->setModule($currentModule);
                    $assignment->setLiveResource($this->getDoctrine()->getRepository('ApiBundle:Profile')
                        ->find($trainersId['id']));
                    $startDate = \DateTime::createFromFormat('m/d/Y', $trainersId['start'],
                        UserTimeZone::getTimeZoneObject());
                    $endDate   = \DateTime::createFromFormat('m/d/Y', $trainersId['end'],
                        UserTimeZone::getTimeZoneObject());
                    $assignment->setMinBookingDate($startDate);
                    $assignment->setMaxBookingDate($endDate);
                    $assignment->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::WITH_BOOKING_SCHEDULED]));
                } else {
                    $assignment->setLiveResource($this->getDoctrine()->getRepository('ApiBundle:Profile')
                        ->find($trainersId['id']));
                }
                $em->persist($assignment);
                $em->flush();

                $this->get(Email::class)->sendMailCourseAssignment($assignment, $intervention, [$hours, $minutes]);
            }

            // send email to all learners who has assigned to the intervention
            // $this->sendCourseModification($intervention, $hours . ':' . $minutes);
        }

        $view = View::create();
        $view->setData('OK');
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("assign/session/{sessionId}")
     *
     * @param $sessionId
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     * @Rest\RequestParam(name="trainerId", nullable=false, strict=true, description="Trainer id.")
     * @Rest\RequestParam(name="sessionHash", nullable=false, strict=true, description="Session Hash.")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function assignTrainerForSessionModuleAction(
        $sessionId,
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $moduleId    = $paramFetcher->get('moduleId');
        $trainerId   = $paramFetcher->get('trainerId');
        $sessionHash = $paramFetcher->get('sessionHash');
        $step        = $this->getInterventionStep($sessionId);

        if (!$step) {
            return $this->errorHandler();
        }
        $stepData   = $step->getCombinedSteps();
        $moduleData = $stepData['module'][$moduleId];

        $sessions        = $moduleData['session'];
        $selectedSession = null;

        foreach ($sessions as $session) {
            if ($session['unique-session-id'] === $sessionHash) {
                $selectedSession = $session;
            }
        }

        if (!$selectedSession) {
            return $this->errorHandler('Session not found');
        }

        $start     = \DateTime::createFromFormat('d/m/Y', $selectedSession['startDate'],
            UserTimeZone::getTimeZoneObject());
        $startTime = new \DateTime($selectedSession['startTime']);

        $start->setTime($startTime->format('H'), $startTime->format('i'));
        $end     = clone $start;
        $hour    = (int)$moduleData['durationHours'] + (int)$start->format('H');
        $minutes = (int)$moduleData['durationMinutes'] + (int)$start->format('i');

        $end->setTime($hour, $minutes);

        $assignedTrainers = $trainerBookingService->assignTrainerForSession($moduleId, $sessionHash, $trainerId, $start,
            $end);

        return $this->sendResponse($assignedTrainers);
    }

    /**
     * @Rest\Post("assign/trainer/session/custom")
     *
     * @param ParamFetcher $paramFetcher
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="Module id.")
     * @Rest\RequestParam(name="trainerId", nullable=false, strict=true, description="Trainer id.")
     * @Rest\RequestParam(name="sessionHash", nullable=false, strict=true, description="Session Hash.")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     */
    public function assignTrainerForSessionModuleCustomAction(
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $moduleId    = $paramFetcher->get('moduleId');
        $trainerId   = $paramFetcher->get('trainerId');
        $sessionHash = $paramFetcher->get('sessionHash');
        $moduleData  = $this->getModuleData($moduleId);

        $sessions        = $moduleData['session'];
        $selectedSession = null;

        foreach ($sessions as $session) {
            if ($session['unique-session-id'] == $sessionHash) {
                $selectedSession = $session;
            }
        }
        if (!$selectedSession) {
            return $this->errorHandler('Session not found');
        }

        $start     = \DateTime::createFromFormat('d/m/Y', $selectedSession['startDate'],
            UserTimeZone::getTimeZoneObject());
        $startTime = new \DateTime($selectedSession['startTime']);

        $start->setTime($startTime->format('H'), $startTime->format('i'));
        $end     = clone $start;
        $hour    = (int)$moduleData['durationHours'] + (int)$start->format('H');
        $minutes = (int)$moduleData['durationMinutes'] + (int)$start->format('i');

        $end->setTime($hour, $minutes);

        $assignedTrainers = $trainerBookingService->assignTrainerForSession($moduleId, $sessionHash, $trainerId, $start,
            $end);

        if ($assignedTrainers) {
            $em         = $this->getDoctrine()->getManager();
            $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                ->findOneBy(['module' => $moduleId, 'moduleSession' => $sessionHash]);

            if ($assignment) {
                $assignment->setLiveResource($this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->find($assignedTrainers['id']));
            }
            $em->persist($assignment);
            $em->flush();

            $intervention = $assignment->getModule()->getIntervention();
            $modules      = $intervention->getModules();
            $hours        = 0;
            $minutes      = 0;

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $duration = clone $module->getDuration();
                $hours    += intval($duration->format('H'));
                $minutes  += intval($duration->format('i'));
            }
            $hours   += floor($minutes / 60);
            $minutes = $minutes % 60;

            $this->get(Email::class)->sendMailCourseAssignment($assignment, $intervention, [$hours, $minutes]);

            // send email to all learners who has assigned to the intervention
            // $this->sendCourseModification($intervention, $hours . ':' . $minutes);
        }

        return $this->sendResponse($assignedTrainers);
    }

    /**
     * @Rest\Post("assign")
     *
     * @param ParamFetcher $paramFetcher
     * @param TrainerBookingService $trainerBookingService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws IncorrectArgumentException
     * @Rest\RequestParam(name="stepId", nullable=false, strict=true, description="step id.")
     */
    public function assignAvailableTrainersAction(
        ParamFetcher $paramFetcher,
        TrainerBookingService $trainerBookingService
    ) {
        $stepId           = $paramFetcher->get('stepId');
        $step             = $this->getInterventionStep($stepId);
        $assignedTrainers = $trainerBookingService->automaticAssignTrainers($step);

        return $this->sendResponse($assignedTrainers);
    }

    /**
     * POST AssignTrainer.
     *
     * @Rest\Post("assign/trainer")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @param TranslatorInterface $translator
     * @param TrainerBookingService $trainerBookingService
     *
     * @Rest\RequestParam(name="bookingId", nullable=true, strict=true, description="Booking ID.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     * @Rest\RequestParam(name="replacement", nullable=true, strict=true, description="Replacement.")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function postAssignTrainerAction(ParamFetcher $paramFetcher, TranslatorInterface $translator, TrainerBookingService $trainerBookingService)
    {
        $em           = $this->getDoctrine()->getManager();
        $profileId    = $paramFetcher->get('profileId') ?: null;
        $bookingId    = $paramFetcher->get('bookingId') ?: null;
        $replacement  = $paramFetcher->get('replacement') ?: null;

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'id' => $profileId,
        ));

        $booking    = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findOneBy(['id' => $bookingId]);
        $oldTrainer = $booking->getTrainer();

        if ($booking->getModule()->getStoredModule()->getAppId() == StoredModule::ONLINE) {
            $booking->setTrainer($profile);
            $em->persist($booking);
            $em->flush();

            $intervention = $booking->getModule()->getIntervention();
            $modules      = $intervention->getModules();
            $hours        = 0;
            $minutes      = 0;

            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                    StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $duration = clone $module->getDuration();
                $hours    += intval($duration->format('H'));
                $minutes  += intval($duration->format('i'));
            }
            $hours   += floor($minutes / 60);
            $minutes = $minutes % 60;

            $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                ->findOneBy(['module' => $booking->getModule(), 'liveResource' => $profile]);
            $this->get(Email::class)->sendMailCourseAssignment($assignment, $intervention, [$hours, $minutes]);
        } else {
            $session = $booking->getModuleSession()->getSession();
            $start = $session->getSessionDate();
            $module = $booking->getModule();
            $classRoomTraining = StoredModule::PRESENTATION_ANIMATION == $module->getStoredModule()->getAppId() ? true : false;
            $trainer = $profile;
            $end = clone $start;
            $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));
            if ($classRoomTraining === true) {
                $availableBookClassroom = $this->isAvailableBookingClassroomTraining($trainer, $module, $session, clone $start);
                if(!$availableBookClassroom){
                    return $this->errorHandler($translator->trans('booking.session_messages.difference_place_trainer_session'));
                }
            }

            $assignedTrainers = $trainerBookingService->assignTrainerForSession($module->getId(), $session->getId(), $trainer->getId(), $start, $end);
            if ($assignedTrainers) {
                $assignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                    ->findOneBy(['module' => $module->getId(), 'moduleSession' => $session->getId()]);

                if (!$assignment) {
                    $assignment = new AssigmentResourceSystem();
                    $assignment->setModule($module);
                    $assignment->setLiveResource($trainer);
                    $assignment->setMinBookingDate($start);
                    $assignment->setMaxBookingDate($end);
                    $assignment->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(['appId' => Status::WITH_BOOKING_SCHEDULED]));
                    $assignment->setModuleSession($session);
                }else {
                    $assignment->setLiveResource($trainer);
                }

                $bookingsAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                    'module'  => $module,
                    'moduleSession' => $session
                ]);
                if($bookingsAgenda){
                    foreach ($bookingsAgenda as $bookingAgenda){
                        $bookingAgenda->setTrainer($trainer);
                        $em->persist($bookingAgenda);
                    }
                }

                $em->persist($assignment);

                $intervention = $assignment->getModule()->getIntervention();
                $modules = $intervention->getModules();
                $hours = 0;
                $minutes = 0;

                foreach ($modules as $module) {
                    if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                        StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                        StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                        StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                    ) {
                        continue;
                    }
                    $duration = clone $module->getDuration();
                    $hours += intval($duration->format('H'));
                    $minutes += intval($duration->format('i'));
                }
                $hours += floor($minutes / 60);
                $minutes = $minutes % 60;
                $this->get(Email::class)->sendMailCourseAssignment($assignment, $intervention, [$hours, $minutes]);
                $this->get(Email::class)->sendMailSessionAssignment($assignment, $intervention, [$hours, $minutes]);
                // send bookingConfirm email when have at least one learner
                if($bookingsAgenda){
                    $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingsAgenda[0]);
                }
                $em->flush();
            }
        }

        // approved the replacement trainer
        if ($replacement) {
            if($booking->getModule()->getStoredModule()->getAppId() == StoredModule::ONLINE){
                $replace = $this->getDoctrine()
                    ->getRepository('ApiBundle:RequestReplacementResource')
                    ->findOneBy(['bookingAgenda' => $booking]);
            }else{
                $replace = $this->getDoctrine()
                    ->getRepository('ApiBundle:RequestReplacementResource')
                    ->findOneBy(['session' => $booking->getModuleSession()->getSession()]);
            }
            if($replace) {
                $replace->setStatus(RequestReplacementResource::STATUS_APPROVED);
                $replace->setValidationDate(new \DateTime());
                $em->persist($replace);
                $em->flush();

                $this->get(Email::class)->sendEmailReplacementConfirmed($oldTrainer, $profile, $replace);
            }
        }

        $view = View::create();
        $view->setData($booking->getId());
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * POST CancelBooking.
     *
     * @Rest\Post("cancel/booking")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @Rest\RequestParam(name="bookingId", nullable=true, strict=true, description="Booking ID.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function postCancelBookingAction(ParamFetcher $paramFetcher)
    {
        $em        = $this->getDoctrine()->getManager();
        $bookingId = $paramFetcher->get('bookingId') ?: null;
        $booking   = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findOneBy(['id' => $bookingId]);

        if ($booking->getModule()->getStoredModule()->getAppId() == StoredModule::ONLINE) {
            $booking->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_CANCELED,
            )));
            $em->persist($booking);
            $em->flush();

            $this->get(Email::class)->sendMailBookingCancel($booking);
        } else {
            $bookings = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBy(['module' => $booking->getModule()]);
            foreach ($bookings as $k => $booked) {
                $booked->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_CANCELED,
                )));
                $em->persist($booked);
                $em->flush();
                // just send email once time to trainer
                if ($k == 0) {
                    $this->get(Email::class)->sendMailBookingCancel($booked);
                }
            }
        }

        $view = View::create();
        $view->setData('OK');
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    protected function getInterventionStep($sessionId): InterventionStep
    {
        $stepRepository = $this->getDoctrine()->getRepository(InterventionStep::class);
        $step           = $stepRepository->findOneBy(array(
            'sessionId' => $sessionId,
        ));
        if (!$step instanceof InterventionStep) {
            throw new IncorrectArgumentException('wrong session id given');
        }

        return $step;
    }

    protected function sendResponse($data)
    {
        $view = View::create();

        $view->setData($data);

        return $this->handleView($view);
    }

    protected function getModuleData($moduleId)
    {
        $moduleData = [];
        $module     = $this->getDoctrine()->getRepository('ApiBundle:Module')
            ->find($moduleId);

        if (!$module) {
            return $this->errorHandler();
        }

        $moduleData['title']        = $module->getDesignation();
        $moduleData['displayTitle'] = $module->getStoredModule()->getDesignation();
        $moduleData['appId']        = $module->getStoredModule()->getAppId();
        $moduleData['quantity']     = $module->getQuantity();
        $moduleData['duration']     = $module->getDuration()->format('i');
        $moduleData['price']        = $module->getPrice();
        $moduleData['instruction']  = $module->getDesignation();

        if ($module->getSkills()) {
            $skills = [];
            foreach ($module->getSkills() as $key => $skill) {
                $skills[$key] = $skill->getId();
            }
            $moduleData['skills'] = $skills;
        }

        $moduleData['allSkills'] = $this->getSkillsOperator($moduleId);

        if ($module->getSessions()) {
            $sessions = [];
            foreach ($module->getSessions() as $key => $moduleSession) {
                $sessions[$key]["unique-session-id"] = $moduleSession->getId();
                $sessions[$key]["startDate"]         = $moduleSession->getSession()->getSessionDate()->format('d/m/Y');
                $sessions[$key]["startTime"]         = $moduleSession->getSession()->getSessionDate()->format('G:i');
            }
            $moduleData['session'] = $sessions;
        }
        $moduleData['startDate']       = $module->getBeginning()->format('d/m/Y');
        $moduleData['endDate']         = $module->getEnding()->format('d/m/Y');
        $moduleData['durationHours']   = $module->getDuration()->format('G');
        $moduleData['durationMinutes'] = $module->getDuration()->format('i');

        return $moduleData;
    }

    protected function getSkillsOperator($moduleId)
    {
        // get skills operator
        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "SELECT `operator` FROM module_skills ms where ms.module_id = :module_id LIMIT 1;";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        $statementDB->bindValue('module_id', $moduleId);
        // Execute both queries
        $statementDB->execute();
        // Get results
        $resultDB             = $statementDB->fetchAll();
        $skillsSearchOperator = reset($resultDB);

        return $skillsSearchOperator['operator'];
    }
}
