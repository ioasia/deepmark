<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class myLibraryController
 * @package AdminBundle\Controller
 */
class myLibraryController extends BaseController implements AdminControllerInterface
{
    /**
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $documents = $this->getDoctrine()->getRepository('ApiBundle:EducationalDocument')->findAll();
        return $this->render('AdminBundle:myLibrary:index.html.twig', array(
            'documents' => $documents,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function newAction(Request $request)
    {
        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['path']->getData();

            $fileDescriptor = $fileApi->uploadPublicFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_library_index'));
        }

        return $this->render('AdminBundle:myLibrary:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
