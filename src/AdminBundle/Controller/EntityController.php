<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Group;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Regional;
use Controller\BaseController;
use FOS\RestBundle\View\View;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Traits\Controller\HasWorkingHours;
use Symfony\Component\HttpFoundation\Response;

/**
 * Entity controller.
 */
class EntityController extends BaseController
{
    use HasWorkingHours;
    /**
     * Lists all entity entities.
     */
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Entity')->findAll();

        return $this->render('AdminBundle:entity:index.html.twig', array(
            'entities' => $entities,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Creates a new entity entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $entity = new Entity();
        $form   = $this->createForm('ApiBundle\Form\EntityType', $entity, [
            'attr' => ['enctype' => 'multipart/form-data']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data    = $request->request->all();
            $entity->setWorkingHours($this->daysProcessing($data));
            if (isset($_FILES['logoFile'])) {
                $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/organisation/entities/';
                $array = explode('.', $_FILES['logoFile']['name']);
                $extension = end($array);
                $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $targetFile = $targetDir . $fileName;
                $path = '/files/organisation/entities/' . $fileName;

                if (move_uploaded_file($_FILES['logoFile']['tmp_name'], $targetFile)) {
                    $entity->setLogo($path);
                }
            }

            if (isset($data['signatureFile']) && $data['signatureFile'] != '') {
                $entity->setSignature($data['signatureFile']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('admin_entity_show', array('id' => $entity->getId()));
        }

        return $this->render('AdminBundle:entity:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Finds and displays a entity entity.
     *
     * @param Entity $entity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Entity $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AdminBundle:entity:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing entity entity.
     *
     * @param Request $request
     * @param Entity $entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Entity $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);
        $editForm   = $this->createForm('ApiBundle\Form\EntityType', $entity, [
            'attr' => ['enctype' => 'multipart/form-data']
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $data    = $request->request->all();
            $entity->setWorkingHours($this->daysProcessing($data));
            if (isset($_FILES['logoFile'])) {
                $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/organisation/entities/';
                $array = explode('.', $_FILES['logoFile']['name']);
                $extension = end($array);
                $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $targetFile = $targetDir . $fileName;
                $path = '/files/organisation/entities/' . $fileName;

                if (move_uploaded_file($_FILES['logoFile']['tmp_name'], $targetFile)) {
                    $entity->setLogo($path);
                }
            }

            if (isset($data['signatureFile']) && $data['signatureFile'] != '') {
                $entity->setSignature($data['signatureFile']);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_entity_edit', array('id' => $entity->getId()));
        }

        return $this->render('AdminBundle:entity:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit'      => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Deletes a entity entity.
     *
     * @param Request $request
     * @param Entity $entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Entity $entity)
    {
        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            $flashBag = $this->get('session')->getFlashBag();
            $messages = $this->deleteEntity($entity, $translator);
            if (!$messages) {
                $flashBag->add('warning', $translator->trans('global.entity_delete_success', ['%entity%' => $entity->getDesignation()]));
                $em = $this->getDoctrine()->getManager();
                $em->remove($entity);
                $em->flush();
            } else {
                $flashBag->add('success', $messages);
            }
        }

        return $this->redirectToRoute('admin_entity_index');
    }

    public function deleteEntitiesAction(Request $request)
    {
        $translator = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $entityIds = $request->request->get('selectedEntities');
        $flashbag = $this->get('session')->getFlashBag();
        foreach ($entityIds as $id){
            $messages = '';
            $entity    = $em->getRepository('ApiBundle:Entity')->find($id);
            if ($type == 1) {
                try {
                    $messages = $this->deleteEntity($entity, $translator);
                    if (!$messages) {
                        $flashbag->add('warning', $translator->trans('global.entity_delete_success', ['%entity%' => $entity->getDesignation()]));
                        $em->remove($entity);
                        $em->flush();
                    } else {
                        $flashbag->add('success', $messages);
                    }
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        return new JsonResponse($messages, 200);
    }

    /**
     * Delete a entity.
     *
     * @param Entity $entity The entity
     *
     * @return Boolean
     */
    private function deleteEntity(Entity $entity, $translator)
    {
        $errors = '';
        $em                     = $this->getDoctrine()->getManager();
        $profiles               = $em->getRepository(Profile::class)->findBy(['entity' => $entity]);
        $interventions          = $em->getRepository(Intervention::class)->findBy(['entity' => $entity]);
        $interventionSteps      = $em->getRepository(InterventionStep::class)->findBy(['entity' => $entity]);
        $regionals              = $em->getRepository(Regional::class)->findBy(['entity' => $entity]);
        $groups                 = $em->getRepository(Group::class)->findBy(['entity' => $entity]);

        if ($profiles || $interventions || $interventionSteps || $regionals || $groups) {

            $errors .= $translator->trans('global.entity_can_not_delete_success', ['%entity%' => $entity->getDesignation()]);
        }

        if ($profiles) {
            $errors .= $translator->trans('global.entity_has_total_profile', ['%profile%' => count($profiles)]);
        }

        if ($interventions) {
            $errors .= $translator->trans('global.entity_has_total_course', ['%course%' => count($interventions)]);
        }

        if ($interventionSteps) {
            $errors .= $translator->trans('global.entity_has_total_draft', ['%draft%' => count($interventionSteps)]);
        }

        if ($regionals) {
            $errors .= $translator->trans('global.entity_has_total_region', ['%region%' => count($regionals)]);
        }

        if ($groups) {
            $errors .= $translator->trans('global.entity_has_total_group', ['%group%' => count($groups)]);
        }
        return $errors;
    }

    public function saveSignatureAction(Request $request)
    {
        $filePath       = '';
        $data_uri       = $request->request->get('signature');

        if ($data_uri) {
            $subDir = '/files/organisation/entities/signature/';
            $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . $subDir;

            $fileName   = 'signature-' . sha1(uniqid(mt_rand(), true)) . '.png';
            $targetFile = $targetDir . $fileName;

            $encoded_image = explode(",", $data_uri)[1];
            $decoded_image = base64_decode($encoded_image);

            if (file_put_contents($targetFile, $decoded_image)) {
                $filePath = $subDir . $fileName;
            }
        }

        $response = new Response(json_encode(array('filePath' => $filePath)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Creates a form to delete a entity entity.
     *
     * @param Entity $entity The entity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Entity $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_entity_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
