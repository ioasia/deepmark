<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\GroupOrganization;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Workplace;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupOrganizationController
 * @package AdminBundle\Controller
 */
class GroupOrganizationController extends BaseController implements AdminControllerInterface
{
    /**
     * @param Request $request
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AdminBundle:groupOrganization:index.html.twig', array(
            'groups' => $this->getDoctrine()->getManager()->getRepository('ApiBundle:GroupOrganization')->findAll(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Creates a new Group Organization entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $group = new GroupOrganization();
        $form  = $this->createForm('ApiBundle\Form\GroupOrganizationType', $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('admin_group_organization_show', array('id' => $group->getId(),
                'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
            ));
        }

        return $this->render('AdminBundle:groupOrganization:formGroupOrganization.html.twig', array(
            'group' => $group,
            'form'  => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Finds and displays a Group Organization entity.
     *
     * @param GroupOrganization $group
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(GroupOrganization $group)
    {
        return $this->render('AdminBundle:groupOrganization:show.html.twig', array(
            'group'       => $group,
            'delete_form' => $this->createDeleteForm($group)->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Displays a form to edit an existing Group Organization  entity.
     *
     * @param Request $request
     * @param GroupOrganization $group
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, GroupOrganization $group)
    {
        $deleteForm = $this->createDeleteForm($group);
        $editForm   = $this->createForm('ApiBundle\Form\GroupOrganizationType', $group);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_group_organization_edit', array('id' => $group->getId()));
        }

        return $this->render('AdminBundle:groupOrganization:formGroupOrganization.html.twig', array(
            'group'       => $group,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit' => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Deletes a Group Organization  entity.
     *
     * @param Request $request
     * @param GroupOrganization $group
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, GroupOrganization $group)
    {
        $form = $this->createDeleteForm($group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            $flashBag = $this->get('session')->getFlashBag();
            $messages = $this->deleteGroupOrganization($group, $translator);
            if (!$messages) {
                $flashBag->add('warning', $translator->trans('global.sector_delete_success', ['%sector%' => $group->getDesignation()]));
                $em = $this->getDoctrine()->getManager();
                $em->remove($group);
                $em->flush();
            } else {
                $flashBag->add('success', $messages);
            }
        }

        return $this->redirectToRoute('admin_group_organization_index');
    }

    public function deleteSectorsAction(Request $request)
    {
        $translator = $this->get('translator');
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $sectorIds = $request->request->get('selectedSectors');
        $flashBag = $this->get('session')->getFlashBag();
        foreach ($sectorIds as $id){
            $messages = '';
            $sector = $em->getRepository('ApiBundle:GroupOrganization')->find($id);
            if ($type == 1) {
                try {
                    $messages = $this->deleteGroupOrganization($sector, $translator);
                    if (!$messages) {
                        $flashBag->add('warning', $translator->trans('global.sector_delete_success', ['%sector%' => $sector->getDesignation()]));
                        $em->remove($sector);
                        $em->flush();
                    } else {
                        $flashBag->add('success', $messages);
                    }
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        }

        return new JsonResponse($messages, 200);
    }

    /**
     * Delete a group organization.
     *
     * @param GroupOrganization $groupOrganization The group organization
     *
     * @return Boolean
     */
    private function deleteGroupOrganization(GroupOrganization $groupOrganization, $translator)
    {
        $errors = '';
        $em             = $this->getDoctrine()->getManager();
        $profiles       = $em->getRepository(Profile::class)->findBy(['groupOrganization' => $groupOrganization]);
        $workplaces     = $em->getRepository(Workplace::class)->findBy(['groupOrganization' => $groupOrganization]);

        if ($profiles || $workplaces) {
            $errors .= $translator->trans('global.sector_can_not_delete_success', ['%sector%' => $groupOrganization->getDesignation()]);
        }

        if ($profiles) {
            $errors .= $translator->trans('global.sector_has_total_profile', ['%profile%' => count($profiles)]);
        }

        if ($workplaces) {
            $errors .= $translator->trans('global.sector_has_total_workplace', ['%workplace%' => count($workplaces)]);
        }

        return $errors;
    }

    /**
     * Creates a form to delete a Group Organization entity.
     * @param GroupOrganization $group
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(GroupOrganization $group)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_group_organization_delete', array('id' => $group->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
