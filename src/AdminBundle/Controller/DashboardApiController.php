<?php

namespace AdminBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Repository\ModuleReportsRepository;
use ApiBundle\Repository\SkillResourceVarietyProfileRepository;
use AppBundle\Service\CalendarApi;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Utils\Email;

/**
 * Class DashboardApiController
 * @package AdminBundle\Controller
 */
class DashboardApiController extends RestController
{
    /**
     * @Rest\Get("calendars")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=false, description="Date End")
     */
    public function getCalendarsAdminAction(ParamFetcher $paramFetcher)
    {
        $dateStart      = new \DateTime($paramFetcher->get('dateStart'));
        $dateEnd        = new \DateTime($paramFetcher->get('dateEnd'));
        $calendar_array = [];

        $calendarApi = $this->get(CalendarApi::class);

        $em = $this->getDoctrine()->getManager();

        $person  = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        if (!$profile) {
            return $this->errorHandler();
        }

        $status = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);

        $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(array(
            'status' => $status,
        ));

        foreach ($learnerInterventions as $learnerIntervention) {
            $intervention = $learnerIntervention->getIntervention();
            $modules      = $em->getRepository('ApiBundle:Module')->findBy(array(
                'intervention' => $intervention,
            ));

            foreach ($modules as $module) {
                if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                    $calendar_my_booking = $calendarApi->getBookingsByModule($module, $dateStart, $dateEnd);
                    $calendar_array      = array_merge($calendar_array, $calendar_my_booking);
                }
            }
        }

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("dayActivities")
     * @Rest\QueryParam(name="day", description="Day to search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDayActivitiesAdminAction(ParamFetcher $paramFetcher)
    {
        $person        = $this->getUser();
        $profile       = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $date          = $paramFetcher->get('day');
        $interventions = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findBookingForToday($profile, $date);

        $view = View::create();

        $view->setData(array(
            'bookings' => $interventions,
            'modal'    => false,
        ));

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("validateSkill")
     *
     * @Rest\QueryParam(name="data")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function validateSkillsAction(ParamFetcher $paramFetcher)
    {
        return $this->changeSkillStatuses($paramFetcher, SkillResourceVarietyProfile::STATUS_APPROVED);
    }

    /**
     * @Rest\Get("validatedReports")
     *
     * @Rest\QueryParam(name="data")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function validatedReportsAction(ParamFetcher $paramFetcher)
    {
        return $this->changeReportStatuses($paramFetcher, ModuleReports::STATUS_APPROVED);
    }

    /**
     * @Rest\Get("rejectedReports")
     *
     * @Rest\QueryParam(name="data")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rejectedReportsAction(ParamFetcher $paramFetcher)
    {
        return $this->changeReportStatuses($paramFetcher, ModuleReports::STATUS_REJECTED);
    }

    /**
     * @Rest\Get("denySkill")
     *
     * @Rest\QueryParam(name="data")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function denySkillsAction(ParamFetcher $paramFetcher)
    {
        return $this->changeSkillStatuses($paramFetcher, SkillResourceVarietyProfile::STATUS_DENY);
    }

    /**
     * @Rest\Get("denyReplace")
     *
     * @Rest\QueryParam(name="data")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function denyReplacementAction(ParamFetcher $paramFetcher)
    {
        $em                         = $this->getDoctrine()->getManager();
        $requestReplacementResource = $this->getDoctrine()
            ->getRepository('ApiBundle:RequestReplacementResource')
            ->findBy(['id' => $paramFetcher->get('data') ?? []]);

        foreach ($requestReplacementResource as $replace) {
            $replace->setStatus(RequestReplacementResource::STATUS_DENY);
            $replace->setValidationDate(new \DateTime());
            $em->persist($replace);

            $this->get(Email::class)->sendMailReplacementDeny($replace);
        }

        $em->flush();

        $view = View::create();
        $view->setData(array(
            'status' => 'success',
        ));

        return $this->handleView($view);
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param $status
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    protected function changeSkillStatuses(ParamFetcher $paramFetcher, $status)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var SkillResourceVarietyProfileRepository $skillsResourceVarietyProfileRepository */
        $skillsResourceVarietyProfileRepository = $this->getDoctrine()
            ->getRepository('ApiBundle:SkillResourceVarietyProfile');
        $skillsResourceVarietyProfile           = $skillsResourceVarietyProfileRepository
            ->findBy(['id' => $paramFetcher->get('data') ?? []]);
        /** @var SkillResourceVarietyProfile $skill */
        foreach ($skillsResourceVarietyProfile as $skill) {
            $skill->setStatus($status);
            $skill->setValidationDate(new \DateTime());
            $em->persist($skill);
        }

        $em->flush();

        $view = View::create();

        $view->setData(array(
            'status' => 'success',
        ));

        return $this->handleView($view);
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param $status
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    protected function changeReportStatuses(ParamFetcher $paramFetcher, $status)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var ModuleReportsRepository $moduleReportsRepository */
        $moduleReportsRepository = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleReports');
        $moduleReports           = $moduleReportsRepository
            ->findBy(['id' => $paramFetcher->get('data') ?? []]);
        /** @var SkillResourceVarietyProfile $skill */
        foreach ($moduleReports as $report) {
            $report->setStatus($status);
            $report->setValidationDate(new \DateTime());
            $em->persist($report);
        }

        $em->flush();

        $view = View::create();

        $view->setData(array(
            'status' => 'success',
        ));

        return $this->handleView($view);
    }
}
