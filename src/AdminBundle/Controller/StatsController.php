<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Group;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\ModuleTrainerResponse;
use ApiBundle\Entity\Organisation;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\NativeQuizResult;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use Controller\BaseController;
use DateTime;
use Doctrine\Common\Collections\Collection;
use PhpOffice\PhpWord\Writer\PDF;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Statistics\BaseStatistics;
use Utils\Statistics\KeyNameConst;
use Traits\IsScorm;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ApiBundle\Entity\StatsExportTemplate;
use ApiBundle\Entity\StatsExportTemplateXref;
use ApiBundle\Service\QuizServiceAPI;
use AdminBundle\Services\QuizStatsService;

/**
 * Class StatsController
 * @package AdminBundle\Controller
 */
class StatsController extends BaseController {

    use IsScorm;

    private $percentStringCharacter = '%';

    const STATISTIC_BY_COURSE = 1;
    const STATISTIC_BY_REGISTERED_USERS = 2;
    const STATISTIC_BY_COURSE_MODULE = 3;
    const STATISTIC_BY_MODULE = 4;
    const STATISTIC_BY_LOGGED_USERS = 5;
    const STATISTIC_BY_QUIZ_RESULTS = 6;
    const STATISTIC_BY_MODULE_RESULTS = 7;
    const STATISTIC_BY_LEARNER_PROGRESSION = 8;
    const STATISTIC_BY_GLOBAL = 9;
    const STATISTIC_BY_SKILLS = 10;
    const STATISTIC_QUIZ = 11;
    const STATISTIC_BY_ALL = 'all';

    private $translations_key = array(
        StatsController::STATISTIC_QUIZ => 'quiz'
    );

    public function indexAction() {
        $translator = $this->get('translator');
        $profile = $this->getCurrentProfile();
        $isSupervisor = $profile->getProfileType()->getAppId() == ProfileVariety::SUPERVISOR ? 1 : 0;
        $interventions = $this->getDoctrine()->getRepository(Intervention::class)
                ->findBy([KeyNameConst::KEY_ENTITY => $this->getEntityFromProfile()]);
        $statisticsSearchParam = [];
        $statisticsSearchParam['organization'] = [];
        $organization = $this->getEntityFromProfile()->getOrganisation();
        $statisticsSearchParam['organization'] = $organization;
        $status = $this->getDoctrine()->getRepository('ApiBundle:Status')
                ->findBy(['id' => [Status::BOOKED, Status::DONE, Status::LEARNER_ABSENT, Status::CANCELED]]);
        $statisticsSearchParam[KeyNameConst::KEY_STATUS] = $status;
        $quizes = $this->getDoctrine()->getRepository('ApiBundle:Quizes')
                ->findAll();
        $statisticsSearchParam[KeyNameConst::KEY_QUIZ] = $quizes;
        $moduleType = $this->getDoctrine()->getRepository('ApiBundle:StoredModule')
                ->findBy(
                ['appId' => [StoredModule::ONLINE, StoredModule::VIRTUAL_CLASS, StoredModule::PRESENTATION_ANIMATION]]
        );
        $statisticsSearchParam[KeyNameConst::KEY_MODULE_TYPE] = $moduleType;
        /**
         * @TODO Only get users under current user permissions
         */
        return $this->render(
                        'statistics/index.html.twig', [
                    'isSupervisor' => $isSupervisor,
                    'profileEntity' => $profile->getEntity(),
                    'title' => $translator->trans('admin.statistic.table_title'),
                    'statisticsSearchParam' => $statisticsSearchParam,
                    'supervisors' => $this->get(BaseStatistics::class)->getSupervisors(),
                    'learners' => $this->get(BaseStatistics::class)->getLearners(),
                    'trainers' => $this->get(BaseStatistics::class)->getTrainers(),
                    'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
                        ]
        );
    }

    public function sectorWorkplaceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $workplaces = [];
        $entityId = $request->request->get('entityId');
        $organizationId = $request->request->get('organizationId');
        $regionalId = $request->request->get('regionalId');
        $sectorId = $request->request->get('sectorId');
        if ($sectorId == 0) {
            $sectors = [];

            if ($regionalId == 0) {
                $regions = [];
                if ($entityId == 0) {
                    $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                    $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                    foreach ($entityLists as $entity) {
                        $regionalLists = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
                        $regions = array_merge($regions, $regionalLists != null ? $regionalLists : []);
                    }
                } else {
                    $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
                    $regions = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
                }
                foreach ($regions as $region) {
                    $sectorLists = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $region));
                    $sectors = array_merge($sectors, $sectorLists != null ? $sectorLists : []);
                }
            } else {
                $regional = $em->getRepository('ApiBundle:Regional')->find($regionalId);
                $sectors = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));
            }
            if ($sectors) {
                foreach ($sectors as $sector) {
                    $workplaceLists = $em->getRepository('ApiBundle:Workplace')->findBy(array('groupOrganization' => $sector));
                    $workplaces = array_merge($workplaces, $workplaceLists != null ? $workplaceLists : []);
                }
            }
        } else {
            $sector = $em->getRepository('ApiBundle:GroupOrganization')->find($sectorId);
            $workplaces = $em->getRepository('ApiBundle:Workplace')->findBy(array('groupOrganization' => $sector));
        }
        $result = [];
        foreach ($workplaces as $workplace) {
            $result[] = array('id' => $workplace->getId(), 'designation' => $workplace->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function regionSectorAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $sectors = [];
        $entityId = $request->request->get('entityId');
        $organizationId = $request->request->get('organizationId');
        $regionalId = $request->request->get('regionalId');

        if ($regionalId == 0) {
            $regions = [];
            if ($entityId == 0) {
                $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                foreach ($entityLists as $entity) {
                    $regionalLists = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
                    $regions = array_merge($regions, $regionalLists != null ? $regionalLists : []);
                }
            } else {
                $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
                $regions = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
            }
            foreach ($regions as $region) {
                $sectorLists = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $region));
                $sectors = array_merge($sectors, $sectorLists != null ? $sectorLists : []);
            }
        } else {
            $regional = $em->getRepository('ApiBundle:Regional')->find($regionalId);
            $sectors = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));
        }
        $result = [];
        foreach ($sectors as $sector) {
            $result[] = array('id' => $sector->getId(), 'designation' => $sector->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function entityRegionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $organizationId = $request->request->get('organizationId');
        $regions = [];
        if ($entityId == 0) {
            $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity) {
                $regionalLists = $entity->getRegionals();
                $regions = array_merge($regions, $regionalLists != null ? $regionalLists->toArray() : []);
            }
        } else {
            $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $regions = $em->getRepository('ApiBundle:Regional')->findBy(array('entity' => $entity));
        }

        $result = [];
        foreach ($regions as $region) {
            $result[] = array('id' => $region->getId(), 'designation' => $region->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function organizationEntityAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $organizationId = $request->request->get('organizationId');
        $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
        $entityLists = $em->getRepository('ApiBundle:Entity')->findBy(array('organisation' => $organization));
        $result = [];
        foreach ($entityLists as $entity) {
            $result[] = array('id' => $entity->getId(), 'designation' => $entity->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function entityCourseAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $courses = [];
        $organizationId = $request->request->get('organizationId');
        if ($entityId == 0) {
            $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity) {
                $courseLists = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
                $courses = array_merge($courses, $courseLists != null ? $courseLists : []);
            }
        } else {
            $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $courses = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
        }
        $result = [];
        foreach ($courses as $course) {
            $result[] = array('id' => $course->getId(), 'designation' => $course->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function courseModuleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $modules = [];
        $organizationId = $request->request->get('organizationId');
        $courseId = $request->request->get('courseId');
        $courses = [];
        if ($courseId == 0) {

            if ($entityId == 0) {
                $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                foreach ($entityLists as $entity) {
                    $courseLists = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
                    $courses = array_merge($courses, $courseLists != null ? $courseLists : []);
                }
            } else {
                $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
                $courses = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
            }

            foreach ($courses as $course) {
                $moduleList = $em->getRepository('ApiBundle:Module')->findBy(array('intervention' => $course));
                $modules = array_merge($modules, $moduleList != null ? $moduleList : []);
            }
        } else {
            $course = $em->getRepository('ApiBundle:Intervention')->find($courseId);
            $modules = $em->getRepository('ApiBundle:Module')->findBy(array('intervention' => $course));
        }
        $result = [];
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $result[] = array('id' => $module->getId(), 'designation' => $module->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function entityGroupAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $groups = [];
        $organizationId = $request->request->get('organizationId');
        if ($entityId == 0) {
            $organization = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity) {
                $groupLists = $em->getRepository('ApiBundle:Group')->findBy(array('entity' => $entity));
                $groups = array_merge($groups, $groupLists != null ? $groupLists : []);
            }
        } else {
            $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $groups = $em->getRepository('ApiBundle:Group')->findBy(array('entity' => $entity));
        }
        $result = [];
        foreach ($groups as $group) {
            $result[] = array('id' => $group->getId(), 'designation' => $group->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Get quizes for filters
     * @param Request $request
     * @return Response
     */
    public function quizesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $isSurvey = $request->request->get('is_survey');
        if ($isSurvey == 'all') {
            $list = $em->getRepository('ApiBundle:Quizes')->findAll();
        } else {
            $list = $em->getRepository('ApiBundle:Quizes')->findBy(array('is_survey' => $isSurvey));
        }
        $result = [];
        foreach ($list as $quiz) {
            $result[] = array(
                'id' => $quiz->getId(),
                'title' => $quiz->getTitle()
            );
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Configure exports
     * @param string $type
     */
    public function configureExportsAction($type = 'quiz') {
        // Load
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        // Get profile
        $profile = $em->getRepository('ApiBundle:Profile')->findOneBy(array('person' => parent::getUser()));

        // Get XLSX template in DB
        $template = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => $type));
        // Get configuration for this user
        $configuration = $em->getRepository('ApiBundle:StatsExportTemplateXref')->findBy(array('admin' => $profile));

        // Create array for configuration datatable
        $result = [];
        if ($template) {
            foreach ($template as $entry) {
                // Check if tab already added
                $tabExists = false;
                foreach ($result as $key => $tab) {
                    if ($tab['tab_pos'] === $entry->getTab()) {
                        $tabExists = $key;
                    }
                }

                if ($tabExists === false) {
                    // Check if tab in configuration
                    $tabSelected = false;
                    foreach ($configuration as $conf) {
                        if ($conf->getTemplate()->getTab() == $entry->getTab()) {
                            $tabSelected = true;
                        }
                    }
                    // Add tab
                    $result[] = array(
                        'tab_pos' => $entry->getTab(),
                        'tab_title' => $translator->trans('admin.statistic.' . $type . '.export.tab_' . $entry->getTab() . '.title'),
                        'selected' => $tabSelected
                    );
                    $tabExists = count($result) - 1;
                }

                // Check if col in configuration
                $colSelected = false;
                foreach ($configuration as $conf) {
                    if ($conf->getTemplate()->getId() == $entry->getId()) {
                        $colSelected = true;
                    }
                }
                // Add col
                $translateString = 'admin.statistic.' . $type . '.export.tab_' . $entry->getTab() . '.col_' . $entry->getCol();
                $result[$tabExists]['cols'][] = array(
                    'template_id' => $entry->getId(),
                    'col_title' => ($translator->trans($translateString) == $translateString ? $translator->trans($translateString . '.title') : $translator->trans($translateString)),
                    'selected' => $colSelected
                );
            }
        }

        $response = new Response(json_encode(array('data' => $result)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Configure exports save
     * 
     * @param string $type
     * @param Request $request
     */
    public function configureExportsSaveAction($type = 'quiz', Request $request) {
        // Load
        $em = $this->getDoctrine()->getManager();
        // Get profile
        $profile = $em->getRepository('ApiBundle:Profile')->findOneBy(array('person' => parent::getUser()));
        // Datas JS
        $cols = $request->request->get('cols');

        if ($type) {
            // Check existing configuration for this type & delete it
            $colsSaved = $em->getRepository('ApiBundle:StatsExportTemplateXref')->findBy(array('admin' => $profile));

            if ($colsSaved) {
                foreach ($colsSaved as $col) {
                    if ($col->getTemplate()->getType() == $type) {
                        $em->remove($col);
                    }
                }
                $em->flush();
            }


            // Save current configuration
            if (is_array($cols)) {
                foreach ($cols as $col) {
                    $template = $em->getRepository('ApiBundle:StatsExportTemplate')->findOneBy(array('id' => (int)$col));

                    if ($template) {
                        $colAdd = new \ApiBundle\Entity\StatsExportTemplateXref();
                        $colAdd->setTemplate($template);
                        $colAdd->setAdmin($profile);

                        $em->persist($colAdd);
                        $em->flush();
                    }
                }
            }
        }


        $response = new Response(json_encode(array('status' => 'success')));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function getColumnsExportByType(string $type)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        // Get profile
        $profile = $em->getRepository('ApiBundle:Profile')->findOneBy(array('person' => parent::getUser()));

        // Get XLSX template in DB
        $template = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => $type));
        // Get configuration for this user
        $configuration = $em->getRepository('ApiBundle:StatsExportTemplateXref')->findBy(array('admin' => $profile));
        $result = [];
        if($template){
            foreach ($template as $entry) {
                // Check if col in configuration
                $colSelected = false;
                foreach ($configuration as $conf) {
                    if ($conf->getTemplate()->getId() == $entry->getId()) {
                        $colSelected = true;
                        break;
                    }
                }
                if($colSelected){
                    $translateString = 'admin.statistic.' . $type . '.export.tab_' . $entry->getTab() . '.col_' . $entry->getCol();
                    $result[$entry->getTab()][$entry->getCol()-1] = ($translator->trans($translateString) == $translateString ? $translator->trans($translateString . '.title') : $translator->trans($translateString));
                }
            }
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getStatisticByTypeAction(Request $request, QuizServiceAPI $quizServiceAPI, QuizStatsService $quizStatService) {
        $type = $request->get('type');
        $options = $request->query->all();
        $from = $request->get('from') ? (new \DateTime($request->get('from'))) : (new \DateTime('first day of this month'));
        $to = $request->get('to') ? (new \DateTime($request->get('to'))) : new \DateTime('last day of this month');
        $data = [];
        $organizationId = $request->get('organizationId');
        $organisation = $this->getDoctrine()->getRepository(Organisation::class)
                ->findOneBy([KeyNameConst::KEY_ID => $organizationId]);
        $options[KeyNameConst::KEY_ORGANISATION] = $organisation;
        //$options[KeyNameConst::KEY_ORGANISATION] = $this->getEntityFromProfile()->getOrganisation();
        $headers = [];
        $columnsExport = [];
        switch ($type) {
            case StatsController::STATISTIC_QUIZ:
                $options['moduleTypeId'] = 5;
                if ($options['isExport'] == 'true') {
                    // Export XLSX
                    $options['tabs'] = [1, 2, 3, 4];
                    $options[KeyNameConst::KEY_GROUP_ID] = 0;
                    list($headers, $templates, $personnalExport) = $this->getStatisticheaders($type, $options);
                    list($data, $headers) = $this->getStatisticProcessing($from, $to, $options, $templates, $quizServiceAPI, $quizStatService, $headers, $personnalExport);
                    return $this->exportXlsx($templates, $headers, $data, $options);
                } else {
                    // Web view
                    list($headers, $templates, $personnalExport) = $this->getStatisticheaders($type, $options);
                    list($data, $headers) = $this->getStatisticProcessing($from, $to, $options, $templates, $quizServiceAPI, $quizStatService, $headers, $personnalExport);
                    // Get only 1 tab for web view
                    if (!empty($data)) {
                        $data = $data[$options['tabs'][0]];
                    }
                }
                break;
            case StatsController::STATISTIC_BY_COURSE:
                $data = $this->getStatisticCourseInRangeTime($from, $to, $options);
                $columnsExport = $this->getColumnsExportByType('course');
                break;
            case StatsController::STATISTIC_BY_REGISTERED_USERS:
                $data = $this->getStatisticByRegisteredUsersInRangeTime($from, $to, $options);
                break;
            case StatsController::STATISTIC_BY_COURSE_MODULE:
                $data = $this->getStatisticCourseModuleInRangeTime($from, $to, $options);
                $columnsExport = $this->getColumnsExportByType('course');
                break;
            case StatsController::STATISTIC_BY_MODULE:
                $data = $this->getStatisticByModulesInRangeTime($from, $to, $options);
                $columnsExport = $this->getColumnsExportByType('course');
                break;
            case StatsController::STATISTIC_BY_LOGGED_USERS:
                $data = $this->getStatisticByLoggedUsersInRangeTime($from, $to, $options);
                break;
            case StatsController::STATISTIC_BY_QUIZ_RESULTS:
                $data = $this->getStatisticByQuizResultsInRangeTime($from, $to, $options);
                break;
            case StatsController::STATISTIC_BY_MODULE_RESULTS:
                $data = $this->getStatisticByModuleResultInRangeTime($from, $to, $options);
                break;
            case StatsController::STATISTIC_BY_LEARNER_PROGRESSION:
                $data = $this->getStatisticByLearnerProgressionInRangeTime($from, $to, $options);
                $columnsExport = $this->getColumnsExportByType('course');
                break;
            case StatsController::STATISTIC_BY_GLOBAL:
                $data = $this->getStatisticByGlobal($from, $to, $options);
                if ($options['isExport'] == 'true') {
                    return $this->getExportData(reset($data), $options);
                }
                break;
            case StatsController::STATISTIC_BY_ALL:
                $statisticCourse = $this->getStatisticCourseInRangeTime($from, $to, $options);
                $keys = array_column($statisticCourse, 'courseName');
                array_multisort($keys, SORT_ASC, $statisticCourse);

                $statisticCourseModule = $this->getStatisticCourseModuleInRangeTime($from, $to, $options);
                $keys = array_column($statisticCourseModule, 'courseName');
                array_multisort($keys, SORT_ASC, $statisticCourseModule);

                $statisticModule = $this->getStatisticByModulesInRangeTime($from, $to, $options);
                $keys = array_column($statisticModule, 'lastName');
                array_multisort($keys, SORT_ASC, $statisticModule);

                $statisticLearnerProgression = $this->getStatisticByLearnerProgressionInRangeTime($from, $to, $options);
                $keys = array_column($statisticLearnerProgression, 'lastName');
                array_multisort($keys, SORT_ASC, $statisticLearnerProgression);

                $data = [
                    'statisticCourse' => $statisticCourse,
                    'statisticCourseModule' => $statisticCourseModule,
                    'statisticModule' => $statisticModule,
                    'statisticLearnerProgression' => $statisticLearnerProgression,
                ];
                return $this->getExportAllData($data, $options);
                break;
            case StatsController::STATISTIC_BY_SKILLS:
                $data = $this->getTrainersBySkills($from, $to, $options);
                break;
            default:
                break;
        }

        $view = $this->renderView('statistics/module_table.html.twig', ['statisticType' => $type, 'statisticData' => $data, 'statisticHeaders' => $headers, 'columnsExport' => $columnsExport]);
        return new JsonResponse($view);
    }

    /**
     * @param array $data
     * @return attachment
     * @throws \Exception
     */
    private function getExportData($data, $options) {
        $translator = $this->get('translator');
        $spreadsheet = new Spreadsheet();
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        /* $sheet->setCellValue('A1', $translator->trans('global.from'));
          $sheet->getStyle('A1')->getFont()->setBold(true);
          $sheet->setCellValue('B1', date("d/m/Y", strtotime($options['from'])));
          $sheet->setCellValue('C1', $translator->trans('global.to'));
          $sheet->getStyle('C1')->getFont()->setBold(true);
          $sheet->setCellValue('D1', date("d/m/Y", strtotime($options['to']))); */

        $sheet->setCellValue('A1', $translator->trans('trainers.statistics.organization'));
        $sheet->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValue('B1', isset($options['organisation']) ? $options['organisation']->getDesignation() : $translator->trans('trainers.statistics.export.all'));

        $sheet->setCellValue('A2', $translator->trans('trainers.statistics.entity'));
        $sheet->getStyle('A2')->getFont()->setBold(true);
        $entityName = '';
        if ($options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $entity = $this->getDoctrine()->getRepository(Entity::class)
                    ->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_ENTITY_ID]]);
            $entityName = $entity->getDesignation();
        }
        $sheet->setCellValue('B2', !empty($entityName) ? $entityName : $translator->trans('trainers.statistics.export.all'));

        $sheet->setCellValue('A3', $translator->trans('trainers.statistics.course_name'));
        $sheet->getStyle('A3')->getFont()->setBold(true);
        $courseName = '';
        if ($options[KeyNameConst::KEY_COURSE_ID] != 0) {
            $intervention = $this->getDoctrine()->getRepository(Intervention::class)
                    ->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_COURSE_ID]]);
            $courseName = $intervention->getDesignation();
        }
        $sheet->setCellValue('B3', !empty($courseName) ? $courseName : $translator->trans('trainers.statistics.export.all'));

        $sheet->setCellValue('A6', $translator->trans('sidebar.learners'));
        $sheet->getStyle('A6')->getFont()->setBold(true);

        $sheet->setCellValue('A7', $translator->trans('trainers.statistics.export.total_number_of_learners'));
        $sheet->setCellValue('B7', $translator->trans('trainers.statistics.export.active'));
        $sheet->setCellValue('C7', $translator->trans('trainers.statistics.export.inactive'));
        $sheet->setCellValue('D7', $translator->trans('trainers.statistics.export.new'));
        $sheet->setCellValue('E7', $translator->trans('trainers.statistics.export.total_time_spent'));
        $sheet->setCellValue('F7', $translator->trans('trainers.statistics.export.average_time_spent'));
        $sheet->getStyle('A7:F7')->getFont()->setBold(true);

        $sheet->setCellValue('A8', $data['learners']['total']);
        $sheet->setCellValue('B8', $data['learners']['active']);
        $sheet->setCellValue('C8', $data['learners']['inactive']);
        $sheet->setCellValue('D8', $data['learners']['new']);
        $sheet->setCellValue('E8', $data['learners']['totalSpentTime']);
        $sheet->setCellValue('F8', $data['learners']['averageSpentTime']);

        $sheet->setCellValue('A10', $translator->trans('sidebar.trainers'));
        $sheet->getStyle('A10')->getFont()->setBold(true);

        $sheet->setCellValue('A11', $translator->trans('trainers.statistics.export.total_number_of_trainers'));
        $sheet->setCellValue('B11', $translator->trans('trainers.statistics.export.active'));
        $sheet->setCellValue('C11', $translator->trans('trainers.statistics.export.inactive'));
        $sheet->setCellValue('D11', $translator->trans('trainers.statistics.export.new'));
        $sheet->setCellValue('E11', $translator->trans('trainers.statistics.export.average_trainer_grade'));
        $sheet->getStyle('A11:E11')->getFont()->setBold(true);

        $sheet->setCellValue('A12', $data['trainers']['total']);
        $sheet->setCellValue('B12', $data['trainers']['active']);
        $sheet->setCellValue('C12', $data['trainers']['inactive']);
        $sheet->setCellValue('D12', $data['trainers']['new']);
        $sheet->setCellValue('E12', $data['trainers']['averageTrainerGrade']);

        $sheet->setCellValue('A14', $translator->trans('sidebar.supervisor'));
        $sheet->getStyle('A14')->getFont()->setBold(true);

        $sheet->setCellValue('A15', $translator->trans('trainers.statistics.export.total_number_of_supervisors'));
        $sheet->setCellValue('B15', $translator->trans('trainers.statistics.export.active'));
        $sheet->setCellValue('C15', $translator->trans('trainers.statistics.export.inactive'));
        $sheet->setCellValue('D15', $translator->trans('trainers.statistics.export.new'));
        $sheet->getStyle('A15:D15')->getFont()->setBold(true);

        $sheet->setCellValue('A16', isset($data['supervisors']) ? $data['supervisors']['total'] : '');
        $sheet->setCellValue('B16', isset($data['supervisors']) ? $data['supervisors']['active'] : '');
        $sheet->setCellValue('C16', isset($data['supervisors']) ? $data['supervisors']['inactive'] : '');
        $sheet->setCellValue('D16', isset($data['supervisors']) ? $data['supervisors']['new'] : '');

        $sheet->setCellValue('A18', $translator->trans('sidebar.interventions'));
        $sheet->getStyle('A18')->getFont()->setBold(true);

        $sheet->setCellValue('A19', $translator->trans('trainers.statistics.export.total_number_of_courses'));
        $sheet->setCellValue('B19', $translator->trans('trainers.statistics.export.new_published'));
        $sheet->setCellValue('C19', $translator->trans('trainers.statistics.export.started'));
        $sheet->setCellValue('D19', $translator->trans('trainers.statistics.export.ended'));
        $sheet->setCellValue('E19', $translator->trans('trainers.statistics.export.average_course_grade'));
        $sheet->getStyle('A19:E19')->getFont()->setBold(true);

        $sheet->setCellValue('A20', isset($data['courses']) ? $data['courses']['total'] : '');
        $sheet->setCellValue('B20', isset($data['courses']) ? $data['courses']['new'] : '');
        $sheet->setCellValue('C20', isset($data['courses']) ? $data['courses']['started'] : '');
        $sheet->setCellValue('D20', isset($data['courses']) ? $data['courses']['ended'] : '');
        $sheet->setCellValue('E20', isset($data['courses']) ? $data['courses']['averageGrade'] : '');

        $sheet->setCellValue('A22', $translator->trans('sidebar.training_group'));
        $sheet->getStyle('A22')->getFont()->setBold(true);

        $sheet->setCellValue('A23', $translator->trans('trainers.statistics.export.total_number_of_training_groups'));
        $sheet->setCellValue('B23', $translator->trans('trainers.statistics.export.new'));
        $sheet->getStyle('A23:B23')->getFont()->setBold(true);

        $sheet->setCellValue('A24', isset($data['groups']) ? $data['groups']['total'] : '');
        $sheet->setCellValue('B24', isset($data['groups']) ? $data['groups']['new'] : '');

        $sheet->setCellValue('A26', $translator->trans('sidebar.stokage_information'));
        $sheet->getStyle('A26')->getFont()->setBold(true);

        $sheet->setCellValue('A27', $translator->trans('trainers.statistics.export.stockage_available'));
        $sheet->setCellValue('B27', $translator->trans('trainers.statistics.export.stokage_used'));
        $sheet->setCellValue('C27', $translator->trans('trainers.statistics.export.stockage_remaining'));
        $sheet->getStyle('A27:C27')->getFont()->setBold(true);

        $sheet->setCellValue('A28', isset($data['stokage']) ? $data['stokage']['total'] : '');
        $sheet->setCellValue('B28', isset($data['stokage']) ? $data['stokage']['used'] : '');
        $sheet->setCellValue('C28', isset($data['stokage']) ? $data['stokage']['remaining'] : '');

        $sheet->setCellValue('A31', $translator->trans('sidebar.modules'));
        $sheet->getStyle('A31')->getFont()->setBold(true);

        $sheet->setCellValue('A32', $translator->trans('trainers.statistics.export.module_type'));
        $sheet->setCellValue('B32', $translator->trans('trainers.statistics.export.total'));
        $sheet->setCellValue('C32', $translator->trans('trainers.statistics.export.booked'));
        $sheet->setCellValue('D32', $translator->trans('trainers.statistics.export.cancelled'));
        $sheet->setCellValue('E32', $translator->trans('trainers.statistics.export.ended'));
        $sheet->getStyle('A32:E32')->getFont()->setBold(true);

        $sheet->setCellValue('A33', $translator->trans('trainers.statistics.export.eLearning'));
        $sheet->setCellValue('B33', isset($data['modules']['eLearning']) ? $data['modules']['eLearning']['total'] : '');
        $sheet->setCellValue('C33', isset($data['modules']['eLearning']) ? $data['modules']['eLearning']['booked'] : '');
        $sheet->setCellValue('D33', isset($data['modules']['eLearning']) ? $data['modules']['eLearning']['cancelled'] : '');
        $sheet->setCellValue('E33', isset($data['modules']['eLearning']) ? $data['modules']['eLearning']['ended'] : '');

        $sheet->setCellValue('A34', $translator->trans('trainers.statistics.export.quiz'));
        $sheet->setCellValue('B34', isset($data['modules']['quiz']) ? $data['modules']['quiz']['total'] : '');
        $sheet->setCellValue('C34', isset($data['modules']['quiz']) ? $data['modules']['quiz']['booked'] : '');
        $sheet->setCellValue('D34', isset($data['modules']['quiz']) ? $data['modules']['quiz']['cancelled'] : '');
        $sheet->setCellValue('E34', isset($data['modules']['quiz']) ? $data['modules']['quiz']['ended'] : '');

        $sheet->setCellValue('A35', $translator->trans('trainers.statistics.export.micro_learning'));
        $sheet->setCellValue('B35', isset($data['modules']['microLearning']) ? $data['modules']['microLearning']['total'] : '');
        $sheet->setCellValue('C35', isset($data['modules']['microLearning']) ? $data['modules']['microLearning']['booked'] : '');
        $sheet->setCellValue('D35', isset($data['modules']['microLearning']) ? $data['modules']['microLearning']['cancelled'] : '');
        $sheet->setCellValue('E35', isset($data['modules']['microLearning']) ? $data['modules']['microLearning']['ended'] : '');

        $sheet->setCellValue('A36', $translator->trans('trainers.statistics.export.assignment'));
        $sheet->setCellValue('B36', isset($data['modules']['assignment']) ? $data['modules']['assignment']['total'] : '');
        $sheet->setCellValue('C36', isset($data['modules']['assignment']) ? $data['modules']['assignment']['booked'] : '');
        $sheet->setCellValue('D36', isset($data['modules']['assignment']) ? $data['modules']['assignment']['cancelled'] : '');
        $sheet->setCellValue('E36', isset($data['modules']['assignment']) ? $data['modules']['assignment']['ended'] : '');

        $sheet->setCellValue('A37', $translator->trans('trainers.statistics.export.individual'));
        $sheet->setCellValue('B37', isset($data['modules']['individual']) ? $data['modules']['individual']['total'] : '');
        $sheet->setCellValue('C37', isset($data['modules']['individual']) ? $data['modules']['individual']['booked'] : '');
        $sheet->setCellValue('D37', isset($data['modules']['individual']) ? $data['modules']['individual']['cancelled'] : '');
        $sheet->setCellValue('E37', isset($data['modules']['individual']) ? $data['modules']['individual']['ended'] : '');

        $sheet->setCellValue('A38', $translator->trans('trainers.statistics.export.virtual'));
        $sheet->setCellValue('B38', isset($data['modules']['virtual']) ? $data['modules']['virtual']['total'] : '');
        $sheet->setCellValue('C38', isset($data['modules']['virtual']) ? $data['modules']['virtual']['booked'] : '');
        $sheet->setCellValue('D38', isset($data['modules']['virtual']) ? $data['modules']['virtual']['cancelled'] : '');
        $sheet->setCellValue('E38', isset($data['modules']['virtual']) ? $data['modules']['virtual']['ended'] : '');

        $sheet->setCellValue('A39', $translator->trans('trainers.statistics.export.workshop'));
        $sheet->setCellValue('B39', isset($data['modules']['workshop']) ? $data['modules']['workshop']['total'] : '');
        $sheet->setCellValue('C39', isset($data['modules']['workshop']) ? $data['modules']['workshop']['booked'] : '');
        $sheet->setCellValue('D39', isset($data['modules']['workshop']) ? $data['modules']['workshop']['cancelled'] : '');
        $sheet->setCellValue('E39', isset($data['modules']['workshop']) ? $data['modules']['workshop']['ended'] : '');

        $sheet->setCellValue('A40', $translator->trans('trainers.statistics.export.presentation'));
        $sheet->setCellValue('B40', isset($data['modules']['presentation']) ? $data['modules']['presentation']['total'] : '');
        $sheet->setCellValue('C40', isset($data['modules']['presentation']) ? $data['modules']['presentation']['booked'] : '');
        $sheet->setCellValue('D40', isset($data['modules']['presentation']) ? $data['modules']['presentation']['cancelled'] : '');
        $sheet->setCellValue('E40', isset($data['modules']['presentation']) ? $data['modules']['presentation']['ended'] : '');

        $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_global'));

        if ($options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $entity = $this->getDoctrine()->getRepository(Entity::class)
                    ->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_ENTITY_ID]]);
            $entityName = $entity->getDesignation();
        }

        if (isset($options['isPdf']) && $options['isPdf'] == 'true') {
            // Apply border for sheet
            $styleArray = array(
                'borders' => array(
                    'outline' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOTDOT,
                        'color' => array('argb' => '00000000'),
                    ),
                ),
            );

            $sheet->getStyle('A1:H40')->applyFromArray($styleArray);

            // Create a Temporary file in the system
            if (isset($entityName) && !empty($entityName)) {
                $fileName = $entityName . '_global_statistics (' . date('d-m-Y') . ').pdf';
            } else {
                $fileName = 'entity_all_global_statistics (' . date('d-m-Y') . ').pdf';
            }
            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            // Create the pdf file in the tmp directory of the system
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf($spreadsheet);
            $writer->save($temp_file);
        } else {
            // Create your Office 2007 Excel (XLSX Format)
            $writer = new Xlsx($spreadsheet);

            // Create a Temporary file in the system
            if (isset($entityName) && !empty($entityName)) {
                $fileName = $entityName . '_global_statistics (' . date('d-m-Y') . ').xlsx';
            } else {
                $fileName = 'entity_all_global_statistics (' . date('d-m-Y') . ').xlsx';
            }
            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            // Create the excel file in the tmp directory of the system
            $writer->save($temp_file);
        }
        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * map keys name for export type course
     * @return array
     */
    private function mapExportKeys()
    {
        $keys = [];
        $keys['statisticCourse'] = [
            'course_name' => KeyNameConst::KEY_COURSE_NAME,
            'organization' => KeyNameConst::KEY_ORGANIZATION,
            'entity' => KeyNameConst::KEY_ENTITY,
            'number_of_total_modules' => KeyNameConst::KEY_NUM_OF_MODULES,
            'number_of_booking_modules' => KeyNameConst::KEY_NUM_OF_BOOKING,
            'number_of_without_booking_modules' => KeyNameConst::KEY_NUM_OF_WITHOUT_BOOKING,
            'number_of_assessment_modules' => KeyNameConst::KEY_NUM_OF_ASSESSMENT,
            'number_of_learners' => KeyNameConst::KEY_NUM_OF_LEARNERS,
            'number_of_not_started_learners' => KeyNameConst::KEY_NUM_OF_NOT_STARTED,
            'number_of_started_learners' => KeyNameConst::KEY_NUM_OF_STARTED,
            'number_of_done_learners' => KeyNameConst::KEY_NUM_OF_FINISHED,
            'progression' => KeyNameConst::KEY_PROGRESSING,
            'number_of_training_groups_registered' => KeyNameConst::KEY_NUM_OF_TRAINING_GROUPS,
            'publication_date' => KeyNameConst::KEY_CREATION_DATE,
            'start_date_course' => KeyNameConst::KEY_START_DATE_COURSE,
            'end_date_course' => KeyNameConst::KEY_END_DATE_COURSE,
            'course_rating' => KeyNameConst::KEY_RATING,
            'module_duration_in_hour' => KeyNameConst::KEY_NUM_DURATIONS,
            'average_time_spent_course' => KeyNameConst::KEY_TOTAL_SPENT_TIME,
            'total_used_space_course' => KeyNameConst::KEY_TOTAL_SIZE_COURSE,
        ];
        $keys['statisticCourseModule'] = [
            'course_name' => KeyNameConst::KEY_COURSE_NAME,
            'module_name' => KeyNameConst::KEY_MODULE_NAME,
            'module_type' => KeyNameConst::KEY_MODULE_TYPE,
            'count_assessment' => KeyNameConst::KEY_IS_ASSESSMENT,
            'module_duration' => KeyNameConst::KEY_MODULE_DURATION,
            'average_real_time_spent' => KeyNameConst::KEY_REAL_TIME,
            'progression' => KeyNameConst::KEY_PROGRESSING,
            'not_started' => KeyNameConst::KEY_NOT_STARTED,
            'booked' => KeyNameConst::KEY_BOOKED,
            'started' => KeyNameConst::KEY_STARTED,
            'completed' => KeyNameConst::KEY_COMPLETED,
            'rating_module' => KeyNameConst::KEY_MODULE_RATING,
            'score_to_pass_percent' => KeyNameConst::KEY_SCORE_TO_PASS,
            'average_score_percent' => KeyNameConst::KEY_AVERAGE_SCORE,
            'completion_of_learners' => KeyNameConst::KEY_STATUS_PASSED,
        ];
        $keys['statisticLearnerProgression'] = [
            'name' => KeyNameConst::KEY_LAST_NAME,
            'fist_name' => KeyNameConst::KEY_FIRST_NAME,
            'email' => KeyNameConst::KEY_EMAIL,
            'position_id' => KeyNameConst::KEY_POSITION_ID,
            'employee_id' => KeyNameConst::KEY_EMPLOYEE_ID,
            'organization' => KeyNameConst::KEY_ORGANIZATION,
            'entity' => KeyNameConst::KEY_ENTITY,
            'regional_direction' => KeyNameConst::KEY_REGIONAL,
            'group' => KeyNameConst::KEY_GROUP,
            'workplace' => KeyNameConst::KEY_WORKPLACE,
            'training_group' => KeyNameConst::KEY_GROUP_NAME,
            'course_name' => KeyNameConst::KEY_COURSE_NAME,
            'registration_date' => KeyNameConst::KEY_REGISTRATION_DATE,
            'status' => KeyNameConst::KEY_STATUS,
            'number_completed_modules' => KeyNameConst::KEY_NUM_COMPLETED_MODULE,
            'number_validated_modules' => KeyNameConst::KEY_NUM_VALIDATED_MODULE,
            'number_to_finish_modules' => KeyNameConst::KEY_NUM_TO_FINISH_MODULE,
            'progression' => KeyNameConst::KEY_PROGRESSING,
            'real_time_spent' => KeyNameConst::KEY_TOTAL_SPENT_TIME,
            'start_date_course' => KeyNameConst::KEY_START_DATE_COURSE,
            'end_date_course' => KeyNameConst::KEY_END_DATE_COURSE,
            'last_login' => KeyNameConst::KEY_LAST_LOGIN,
            'number_login' => KeyNameConst::KEY_NUM_LOGIN,
        ];
        $keys['statisticModule'] = [
            'name' => KeyNameConst::KEY_LAST_NAME,
            'fist_name' => KeyNameConst::KEY_FIRST_NAME,
            'email' => KeyNameConst::KEY_EMAIL,
            'position_id' => KeyNameConst::KEY_POSITION_ID,
            'employee_id' => KeyNameConst::KEY_EMPLOYEE_ID,
            'organization' => KeyNameConst::KEY_ORGANIZATION,
            'entity' => KeyNameConst::KEY_ENTITY,
            'regional_direction' => KeyNameConst::KEY_REGIONAL,
            'group' => KeyNameConst::KEY_GROUP,
            'workplace' => KeyNameConst::KEY_WORKPLACE,
            'training_group' => KeyNameConst::KEY_GROUP_NAME,
            'course_name' => KeyNameConst::KEY_COURSE_NAME,
            'module_name' => KeyNameConst::KEY_MODULE_NAME,
            'status' => KeyNameConst::KEY_STATUS,
            'module_duration' => KeyNameConst::KEY_DURATION,
            'real_time_spent' => KeyNameConst::KEY_REAL_TIME,
            'score' => KeyNameConst::KEY_SCORE,
            'score_to_pass' => KeyNameConst::KEY_SCORE_TO_PASS,
            'result' => KeyNameConst::KEY_RESULT,
            'end_date' => KeyNameConst::KEY_COMPLETION_DATE,
        ];
        
        return $keys;
    }

    /**
     * @param array $data
     * @param array $options
     * @return attachment
     * @throws \Exception
     */
    private function getExportAllData($data, $options) {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $spreadsheet = new Spreadsheet();

        // add more if we have extra cols
        $cells = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'
                    , 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W'];
        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();

        if(isset($options['isPersonnalExport']) && $options['isPersonnalExport'] === 'true'){
            $statisticCourseKeys = $this->mapExportKeys()['statisticCourse'];
            // Get profile
            $profile = $em->getRepository('ApiBundle:Profile')->findOneBy(array('person' => parent::getUser()));
            $personalExport = $em->getRepository('ApiBundle:StatsExportTemplateXref')->findBy(array('admin' => $profile));

            //statisticCourse is tab one in select box
            $statisticCourseTab = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => 'course', 'tab' => 1));
            $cellNumber = 0;
            $colNameArray = [];
            foreach ($statisticCourseTab as $key => $template){
                foreach ($personalExport as $colSelected){
                    if ($template->getId() == $colSelected->getTemplate()->getId()){
                        $colName = $cells[$cellNumber];
                        $keyName = json_decode($template->getParams());
                        $sheet->setCellValue($colName.'1', $translator->trans('trainers.statistics.'.$keyName->key));
                        $colNameArray[$colName] = $keyName->key;
                        $cellNumber++;
                    }
                }
            }
            $sheet->getStyle('A1:T1')->getFont()->setBold(true);
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_course'));
            if($cellNumber > 0) {
                if ($data['statisticCourse']) {
                    foreach ($data['statisticCourse'] as $k => $item) {
                        $key = $k + 2;
                        foreach ($colNameArray as $name => $keyTrans) {
                            $colValue = $item[$statisticCourseKeys[$keyTrans]];
                            if (in_array($statisticCourseKeys[$keyTrans], [KeyNameConst::KEY_CREATION_DATE, KeyNameConst::KEY_START_DATE_COURSE, KeyNameConst::KEY_END_DATE_COURSE])) {
                                $colValue = $item[$statisticCourseKeys[$keyTrans]]->format('d/m/Y');
                            }
                            $sheet->setCellValue($name . $key, $colValue);
                        }
                    }
                }
            }else{
                $sheetIndex = $spreadsheet->getIndex(
                    $spreadsheet->getSheetByName($translator->trans('trainers.statistics.type_report_by_course'))
                );
                $spreadsheet->removeSheetByIndex($sheetIndex);
            }

            // statisticCourseModule is tab two in select box
            $sheet = $spreadsheet->createSheet();
            $statisticCourseKeys = $this->mapExportKeys()['statisticCourseModule'];
            $statisticCourseTab = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => 'course', 'tab' => 2));
            $cellNumber = 0;
            $colNameArray = [];
            foreach ($statisticCourseTab as $key => $template){
                foreach ($personalExport as $colSelected){
                    if ($template->getId() == $colSelected->getTemplate()->getId()){
                        $colName = $cells[$cellNumber];
                        $keyName = json_decode($template->getParams());
                        $sheet->setCellValue($colName.'1', $translator->trans('trainers.statistics.'.$keyName->key));
                        $colNameArray[$colName] = $keyName->key;
                        $cellNumber++;
                    }
                }
            }
            $sheet->getStyle('A1:T1')->getFont()->setBold(true);
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_course_module'));
            if ($cellNumber > 0) {
                if ($data['statisticCourseModule']) {
                    foreach ($data['statisticCourseModule'] as $k => $item) {
                        $key = $k + 2;
                        foreach ($colNameArray as $name => $keyTrans) {
                            $colValue = $item[$statisticCourseKeys[$keyTrans]];
                            if ($statisticCourseKeys[$keyTrans] === KeyNameConst::KEY_PROGRESSING) {
                                $colValue .= $this->percentStringCharacter;
                            }
                            $sheet->setCellValue($name . $key, $colValue);
                        }
                    }
                }
            }else{
                $sheetIndex = $spreadsheet->getIndex(
                    $spreadsheet->getSheetByName($translator->trans('trainers.statistics.type_report_by_course_module'))
                );
                $spreadsheet->removeSheetByIndex($sheetIndex);
            }

            // statisticLearnerProgression is tab three in select box
            $sheet = $spreadsheet->createSheet();
            $statisticCourseKeys = $this->mapExportKeys()['statisticLearnerProgression'];
            $statisticCourseTab = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => 'course', 'tab' => 3));
            $cellNumber = 0;
            $colNameArray = [];
            foreach ($statisticCourseTab as $key => $template){
                foreach ($personalExport as $colSelected){
                    if ($template->getId() == $colSelected->getTemplate()->getId()){
                        $colName = $cells[$cellNumber];
                        $keyName = json_decode($template->getParams());
                        $sheet->setCellValue($colName.'1', $translator->trans('trainers.statistics.'.$keyName->key));
                        $colNameArray[$colName] = $keyName->key;
                        $cellNumber++;
                    }
                }
            }
            $sheet->getStyle('A1:W1')->getFont()->setBold(true);
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_learner_progression'));
            if ($cellNumber > 0) {
                if ($data['statisticLearnerProgression']) {
                    foreach ($data['statisticLearnerProgression'] as $k => $item) {
                        $key = $k + 2;
                        foreach ($colNameArray as $name => $keyTrans) {
                            $colValue = $item[$statisticCourseKeys[$keyTrans]];
                            if (in_array($statisticCourseKeys[$keyTrans], [KeyNameConst::KEY_REGISTRATION_DATE, KeyNameConst::KEY_START_DATE_COURSE, KeyNameConst::KEY_END_DATE_COURSE, KeyNameConst::KEY_LAST_LOGIN])) {
                                $colValue = $colValue ? $colValue->format('d/m/Y') : '';
                            }

                            if ($statisticCourseKeys[$keyTrans] === KeyNameConst::KEY_PROGRESSING) {
                                $colValue .= $this->percentStringCharacter;
                            }

                            if ($statisticCourseKeys[$keyTrans] === KeyNameConst::KEY_STATUS) {
                                if ($colValue == 'Finished') {
                                    $colValue = $translator->trans('learners.courses.status.finished');
                                } else if ($colValue == 'In Progress') {
                                    $colValue = $translator->trans('learners.courses.status.in_progress');
                                } else if ($colValue == 'Created') {
                                    $colValue = $translator->trans('learners.courses.status.created');
                                }
                            }
                            $sheet->setCellValue($name . $key, $colValue);
                        }
                    }
                }
            }else{
                $sheetIndex = $spreadsheet->getIndex(
                    $spreadsheet->getSheetByName($translator->trans('trainers.statistics.type_report_by_learner_progression'))
                );
                $spreadsheet->removeSheetByIndex($sheetIndex);
            }

            // statisticModule is tab four in select box
            $sheet = $spreadsheet->createSheet();
            $statisticCourseKeys = $this->mapExportKeys()['statisticModule'];
            $statisticCourseTab = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => 'course', 'tab' => 4));
            $cellNumber = 0;
            $colNameArray = [];
            foreach ($statisticCourseTab as $key => $template){
                foreach ($personalExport as $colSelected){
                    if ($template->getId() == $colSelected->getTemplate()->getId()){
                        $colName = $cells[$cellNumber];
                        $keyName = json_decode($template->getParams());
                        $sheet->setCellValue($colName.'1', $translator->trans('trainers.statistics.'.$keyName->key));
                        $colNameArray[$colName] = $keyName->key;
                        $cellNumber++;
                    }
                }
            }
            $sheet->getStyle('A1:T1')->getFont()->setBold(true);
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_module'));
            if ($cellNumber > 0) {
                if ($data['statisticModule']) {
                    foreach ($data['statisticModule'] as $k => $item) {
                        $key = $k + 2;
                        foreach ($colNameArray as $name => $keyTrans) {
                            $colValue = $item[$statisticCourseKeys[$keyTrans]];
                            if ($statisticCourseKeys[$keyTrans] === KeyNameConst::KEY_STATUS) {
                                if ($colValue == 'Done') {
                                    $colValue = $translator->trans('learners.courses.status.done');
                                } else if ($colValue == 'In Progress') {
                                    $colValue = $translator->trans('learners.courses.status.in_progress');
                                } else if ($colValue == 'Created') {
                                    $colValue = $translator->trans('learners.courses.status.created');
                                }
                            }
                            if ($statisticCourseKeys[$keyTrans] == KeyNameConst::KEY_RESULT) {
                                $colValue = $colValue == 1 ? $translator->trans('trainers.statistics.result_passed') : $translator->trans('trainers.statistics.result_failed');
                            }
                            if ($statisticCourseKeys[$keyTrans] == KeyNameConst::KEY_COMPLETION_DATE) {
                                $colValue = $item[KeyNameConst::KEY_COMPLETION_DATE] && $item[KeyNameConst::KEY_RESULT] ? $item[KeyNameConst::KEY_COMPLETION_DATE]->format('d/m/Y') : '';
                            }
                            $sheet->setCellValue($name . $key, $colValue);
                        }
                    }
                }
            }else{
                $sheetIndex = $spreadsheet->getIndex(
                    $spreadsheet->getSheetByName($translator->trans('trainers.statistics.type_report_by_module'))
                );
                $spreadsheet->removeSheetByIndex($sheetIndex);
            }
        }else {
            $sheet->setCellValue('A1', $translator->trans('trainers.statistics.course_name'));
            $sheet->setCellValue('B1', $translator->trans('trainers.statistics.organization'));
            $sheet->setCellValue('C1', $translator->trans('trainers.statistics.entity'));
            $sheet->setCellValue('D1', $translator->trans('trainers.statistics.number_of_total_modules'));
            $sheet->setCellValue('E1', $translator->trans('trainers.statistics.number_of_booking_modules'));
            $sheet->setCellValue('F1', $translator->trans('trainers.statistics.number_of_without_booking_modules'));
            $sheet->setCellValue('G1', $translator->trans('trainers.statistics.number_of_assessment_modules'));
            $sheet->setCellValue('H1', $translator->trans('trainers.statistics.number_of_learners'));
            $sheet->setCellValue('I1', $translator->trans('trainers.statistics.number_of_not_started_learners'));
            $sheet->setCellValue('J1', $translator->trans('trainers.statistics.number_of_started_learners'));
            $sheet->setCellValue('K1', $translator->trans('trainers.statistics.number_of_done_learners'));
            $sheet->setCellValue('L1', $translator->trans('trainers.statistics.progression'));
            $sheet->setCellValue('M1', $translator->trans('trainers.statistics.number_of_training_groups_registered'));
            $sheet->setCellValue('N1', $translator->trans('trainers.statistics.publication_date'));
            $sheet->setCellValue('O1', $translator->trans('trainers.statistics.start_date_course'));
            $sheet->setCellValue('P1', $translator->trans('trainers.statistics.end_date_course'));
            $sheet->setCellValue('Q1', $translator->trans('trainers.statistics.course_rating'));
            $sheet->setCellValue('R1', $translator->trans('trainers.statistics.module_duration_in_hour'));
            $sheet->setCellValue('S1', $translator->trans('trainers.statistics.average_time_spent_course'));
            $sheet->setCellValue('T1', $translator->trans('trainers.statistics.total_used_space_course'));
            $sheet->getStyle('A1:T1')->getFont()->setBold(true);

            if ($data['statisticCourse']) {
                foreach ($data['statisticCourse'] as $k => $item) {
                    $key = $k + 2;
                    $sheet->setCellValue('A' . $key, $item[KeyNameConst::KEY_COURSE_NAME]);
                    $sheet->setCellValue('B' . $key, $item[KeyNameConst::KEY_ORGANIZATION]);
                    $sheet->setCellValue('C' . $key, $item[KeyNameConst::KEY_ENTITY]);
                    $sheet->setCellValue('D' . $key, $item[KeyNameConst::KEY_NUM_OF_MODULES]);
                    $sheet->setCellValue('E' . $key, $item[KeyNameConst::KEY_NUM_OF_BOOKING]);
                    $sheet->setCellValue('F' . $key, $item[KeyNameConst::KEY_NUM_OF_WITHOUT_BOOKING]);
                    $sheet->setCellValue('G' . $key, $item[KeyNameConst::KEY_NUM_OF_ASSESSMENT]);
                    $sheet->setCellValue('H' . $key, $item[KeyNameConst::KEY_NUM_OF_LEARNERS]);
                    $sheet->setCellValue('I' . $key, $item[KeyNameConst::KEY_NUM_OF_NOT_STARTED]);
                    $sheet->setCellValue('J' . $key, $item[KeyNameConst::KEY_NUM_OF_STARTED]);
                    $sheet->setCellValue('K' . $key, $item[KeyNameConst::KEY_NUM_OF_FINISHED]);
                    $sheet->setCellValue('L' . $key, $item[KeyNameConst::KEY_PROGRESSING] . '%');
                    $sheet->setCellValue('M' . $key, $item[KeyNameConst::KEY_NUM_OF_TRAINING_GROUPS]);
                    $sheet->setCellValue('N' . $key, $item[KeyNameConst::KEY_CREATION_DATE]->format('d/m/Y'));
                    $sheet->setCellValue('O' . $key, $item[KeyNameConst::KEY_START_DATE_COURSE]->format('d/m/Y'));
                    $sheet->setCellValue('P' . $key, $item[KeyNameConst::KEY_END_DATE_COURSE]->format('d/m/Y'));
                    $sheet->setCellValue('Q' . $key, $item[KeyNameConst::KEY_RATING]);
                    $sheet->setCellValue('R' . $key, $item[KeyNameConst::KEY_NUM_DURATIONS]);
                    $sheet->setCellValue('S' . $key, $item[KeyNameConst::KEY_TOTAL_SPENT_TIME]);
                    $sheet->setCellValue('T' . $key, $item[KeyNameConst::KEY_TOTAL_SIZE_COURSE]);
                }
            }
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_course'));

            $sheet = $spreadsheet->createSheet();

            $sheet->setCellValue('A1', $translator->trans('trainers.statistics.course_name'));
            $sheet->setCellValue('B1', $translator->trans('trainers.statistics.module_name'));
            $sheet->setCellValue('C1', $translator->trans('trainers.statistics.module_type'));
            $sheet->setCellValue('D1', $translator->trans('trainers.statistics.count_assessment'));
            $sheet->setCellValue('E1', $translator->trans('trainers.statistics.module_duration'));
            $sheet->setCellValue('F1', $translator->trans('trainers.statistics.average_real_time_spent'));
            $sheet->setCellValue('G1', $translator->trans('trainers.statistics.progression'));
            $sheet->setCellValue('H1', $translator->trans('trainers.statistics.not_started'));
            $sheet->setCellValue('I1', $translator->trans('trainers.statistics.booked'));
            $sheet->setCellValue('J1', $translator->trans('trainers.statistics.started'));
            $sheet->setCellValue('K1', $translator->trans('trainers.statistics.completed'));
            $sheet->setCellValue('L1', $translator->trans('trainers.statistics.rating_module'));
            $sheet->setCellValue('M1', $translator->trans('trainers.statistics.score_to_pass_percent'));
            $sheet->setCellValue('N1', $translator->trans('trainers.statistics.average_score_percent'));
            $sheet->setCellValue('O1', $translator->trans('trainers.statistics.completion_of_learners'));
            $sheet->getStyle('A1:O1')->getFont()->setBold(true);
            if ($data['statisticCourseModule']) {
                foreach ($data['statisticCourseModule'] as $k => $item) {
                    $key = $k + 2;
                    $sheet->setCellValue('A' . $key, $item[KeyNameConst::KEY_COURSE_NAME]);
                    $sheet->setCellValue('B' . $key, $item[KeyNameConst::KEY_MODULE_NAME]);
                    $sheet->setCellValue('C' . $key, $item[KeyNameConst::KEY_MODULE_TYPE]);
                    $sheet->setCellValue('D' . $key, $item[KeyNameConst::KEY_IS_ASSESSMENT]);
                    $sheet->setCellValue('E' . $key, $item[KeyNameConst::KEY_MODULE_DURATION]);
                    $sheet->setCellValue('F' . $key, $item[KeyNameConst::KEY_REAL_TIME]);
                    $sheet->setCellValue('G' . $key, $item[KeyNameConst::KEY_PROGRESSING] . $this->percentStringCharacter);
                    $sheet->setCellValue('H' . $key, $item[KeyNameConst::KEY_NOT_STARTED]);
                    $sheet->setCellValue('I' . $key, $item[KeyNameConst::KEY_BOOKED]);
                    $sheet->setCellValue('J' . $key, $item[KeyNameConst::KEY_STARTED]);
                    $sheet->setCellValue('K' . $key, $item[KeyNameConst::KEY_COMPLETED]);
                    $sheet->setCellValue('L' . $key, $item[KeyNameConst::KEY_MODULE_RATING]);
                    $sheet->setCellValue('M' . $key, $item[KeyNameConst::KEY_SCORE_TO_PASS]);
                    $sheet->setCellValue('N' . $key, $item[KeyNameConst::KEY_AVERAGE_SCORE]);
                    $sheet->setCellValue('O' . $key, $item[KeyNameConst::KEY_STATUS_PASSED]);
                }
            }
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_course_module'));

            $sheet = $spreadsheet->createSheet();

            $sheet->setCellValue('A1', $translator->trans('trainers.statistics.name'));
            $sheet->setCellValue('B1', $translator->trans('trainers.statistics.fist_name'));
            $sheet->setCellValue('C1', $translator->trans('trainers.statistics.email'));
            $sheet->setCellValue('D1', $translator->trans('trainers.statistics.position_id'));
            $sheet->setCellValue('E1', $translator->trans('trainers.statistics.employee_id'));
            $sheet->setCellValue('F1', $translator->trans('trainers.statistics.organization'));
            $sheet->setCellValue('G1', $translator->trans('trainers.statistics.entity'));
            $sheet->setCellValue('H1', $translator->trans('trainers.statistics.regional_direction'));
            $sheet->setCellValue('I1', $translator->trans('trainers.statistics.group'));
            $sheet->setCellValue('J1', $translator->trans('trainers.statistics.workplace'));
            $sheet->setCellValue('K1', $translator->trans('trainers.statistics.training_group'));
            $sheet->setCellValue('L1', $translator->trans('trainers.statistics.course_name'));
            $sheet->setCellValue('M1', $translator->trans('trainers.statistics.registration_date'));
            $sheet->setCellValue('N1', $translator->trans('trainers.statistics.status'));
            $sheet->setCellValue('O1', $translator->trans('trainers.statistics.number_completed_modules'));
            $sheet->setCellValue('P1', $translator->trans('trainers.statistics.number_validated_modules'));
            $sheet->setCellValue('Q1', $translator->trans('trainers.statistics.number_to_finish_modules'));
            $sheet->setCellValue('R1', $translator->trans('trainers.statistics.progression'));
            $sheet->setCellValue('S1', $translator->trans('trainers.statistics.real_time_spent'));
            $sheet->setCellValue('T1', $translator->trans('trainers.statistics.start_date_course'));
            $sheet->setCellValue('U1', $translator->trans('trainers.statistics.end_date_course'));
            $sheet->setCellValue('V1', $translator->trans('trainers.statistics.last_login'));
            $sheet->setCellValue('W1', $translator->trans('trainers.statistics.number_login'));
            $sheet->getStyle('A1:W1')->getFont()->setBold(true);

            if ($data['statisticLearnerProgression']) {
                foreach ($data['statisticLearnerProgression'] as $k => $item) {
                    $key = $k + 2;
                    $sheet->setCellValue('A' . $key, $item[KeyNameConst::KEY_LAST_NAME]);
                    $sheet->setCellValue('B' . $key, $item[KeyNameConst::KEY_FIRST_NAME]);
                    $sheet->setCellValue('C' . $key, $item[KeyNameConst::KEY_EMAIL]);
                    $sheet->setCellValue('D' . $key, $item[KeyNameConst::KEY_POSITION_ID]);
                    $sheet->setCellValue('E' . $key, $item[KeyNameConst::KEY_EMPLOYEE_ID]);
                    $sheet->setCellValue('F' . $key, $item[KeyNameConst::KEY_ORGANIZATION]);
                    $sheet->setCellValue('G' . $key, $item[KeyNameConst::KEY_ENTITY]);
                    $sheet->setCellValue('H' . $key, $item[KeyNameConst::KEY_REGIONAL]);
                    $sheet->setCellValue('I' . $key, $item[KeyNameConst::KEY_GROUP]);
                    $sheet->setCellValue('J' . $key, $item[KeyNameConst::KEY_WORKPLACE]);
                    $sheet->setCellValue('K' . $key, $item[KeyNameConst::KEY_GROUP_NAME]);
                    $sheet->setCellValue('L' . $key, $item[KeyNameConst::KEY_COURSE_NAME]);
                    $sheet->setCellValue('M' . $key, $item[KeyNameConst::KEY_REGISTRATION_DATE]->format('d/m/Y'));
                    if ($item[KeyNameConst::KEY_STATUS] == 'Finished') {
                        $status = $translator->trans('learners.courses.status.finished');
                    } else if ($item[KeyNameConst::KEY_STATUS] == 'In Progress') {
                        $status = $translator->trans('learners.courses.status.in_progress');
                    } else if ($item[KeyNameConst::KEY_STATUS] == 'Created') {
                        $status = $translator->trans('learners.courses.status.created');
                    } else {
                        $status = $item[KeyNameConst::KEY_STATUS];
                    }
                    $sheet->setCellValue('N' . $key, $status);
                    $sheet->setCellValue('O' . $key, $item[KeyNameConst::KEY_NUM_COMPLETED_MODULE]);
                    $sheet->setCellValue('P' . $key, $item[KeyNameConst::KEY_NUM_VALIDATED_MODULE]);
                    $sheet->setCellValue('Q' . $key, $item[KeyNameConst::KEY_NUM_TO_FINISH_MODULE]);
                    $sheet->setCellValue('R' . $key,$item[KeyNameConst::KEY_PROGRESSING] . $this->percentStringCharacter);
                    $sheet->setCellValue('S' . $key, $item[KeyNameConst::KEY_TOTAL_SPENT_TIME]);
                    $sheet->setCellValue('T' . $key,$item[KeyNameConst::KEY_START_DATE_COURSE] ? $item[KeyNameConst::KEY_START_DATE_COURSE]->format('d/m/Y') : '');
                    $sheet->setCellValue('U' . $key,$item[KeyNameConst::KEY_END_DATE_COURSE] ? $item[KeyNameConst::KEY_END_DATE_COURSE]->format('d/m/Y') : '');
                    $sheet->setCellValue('V' . $key, $item[KeyNameConst::KEY_LAST_LOGIN] ? $item[KeyNameConst::KEY_LAST_LOGIN]->format('d/m/Y'): '');
                    $sheet->setCellValue('W' . $key, $item[KeyNameConst::KEY_NUM_LOGIN]);
                }
            }

            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_learner_progression'));

            $sheet = $spreadsheet->createSheet();

            $sheet->setCellValue('A1', $translator->trans('trainers.statistics.name'));
            $sheet->setCellValue('B1', $translator->trans('trainers.statistics.fist_name'));
            $sheet->setCellValue('C1', $translator->trans('trainers.statistics.email'));
            $sheet->setCellValue('D1', $translator->trans('trainers.statistics.position_id'));
            $sheet->setCellValue('E1', $translator->trans('trainers.statistics.employee_id'));
            $sheet->setCellValue('F1', $translator->trans('trainers.statistics.organization'));
            $sheet->setCellValue('G1', $translator->trans('trainers.statistics.entity'));
            $sheet->setCellValue('H1', $translator->trans('trainers.statistics.regional_direction'));
            $sheet->setCellValue('I1', $translator->trans('trainers.statistics.group'));
            $sheet->setCellValue('J1', $translator->trans('trainers.statistics.workplace'));
            $sheet->setCellValue('K1', $translator->trans('trainers.statistics.training_group'));
            $sheet->setCellValue('L1', $translator->trans('trainers.statistics.course_name'));
            $sheet->setCellValue('M1', $translator->trans('trainers.statistics.module_name'));
            $sheet->setCellValue('N1', $translator->trans('trainers.statistics.status'));
            $sheet->setCellValue('O1', $translator->trans('trainers.statistics.module_duration'));
            $sheet->setCellValue('P1', $translator->trans('trainers.statistics.real_time_spent'));
            $sheet->setCellValue('Q1', $translator->trans('trainers.statistics.score'));
            $sheet->setCellValue('R1', $translator->trans('trainers.statistics.score_to_pass'));
            $sheet->setCellValue('S1', $translator->trans('trainers.statistics.result'));
            $sheet->setCellValue('T1', $translator->trans('trainers.statistics.end_date'));
            $sheet->getStyle('A1:T1')->getFont()->setBold(true);

            if ($data['statisticModule']) {
                foreach ($data['statisticModule'] as $k => $item) {
                    $key = $k + 2;
                    $sheet->setCellValue('A' . $key, $item[KeyNameConst::KEY_LAST_NAME]);
                    $sheet->setCellValue('B' . $key, $item[KeyNameConst::KEY_FIRST_NAME]);
                    $sheet->setCellValue('C' . $key, $item[KeyNameConst::KEY_EMAIL]);
                    $sheet->setCellValue('D' . $key, $item[KeyNameConst::KEY_POSITION_ID]);
                    $sheet->setCellValue('E' . $key, $item[KeyNameConst::KEY_EMPLOYEE_ID]);
                    $sheet->setCellValue('F' . $key, $item[KeyNameConst::KEY_ORGANIZATION]);
                    $sheet->setCellValue('G' . $key, $item[KeyNameConst::KEY_ENTITY]);
                    $sheet->setCellValue('H' . $key, $item[KeyNameConst::KEY_REGIONAL]);
                    $sheet->setCellValue('I' . $key, $item[KeyNameConst::KEY_GROUP]);
                    $sheet->setCellValue('J' . $key, $item[KeyNameConst::KEY_WORKPLACE]);
                    $sheet->setCellValue('K' . $key, $item[KeyNameConst::KEY_GROUP_NAME]);
                    $sheet->setCellValue('L' . $key, $item[KeyNameConst::KEY_COURSE_NAME]);
                    $sheet->setCellValue('M' . $key, $item[KeyNameConst::KEY_MODULE_NAME]);
                    if ($item[KeyNameConst::KEY_STATUS] == 'Done') {
                        $status = $translator->trans('learners.courses.status.done');
                    } else if ($item[KeyNameConst::KEY_STATUS] == 'In Progress') {
                        $status = $translator->trans('learners.courses.status.in_progress');
                    } else if ($item[KeyNameConst::KEY_STATUS] == 'Created') {
                        $status = $translator->trans('learners.courses.status.created');
                    } else {
                        $status = $item[KeyNameConst::KEY_STATUS];
                    }
                    $sheet->setCellValue('N' . $key, $status);
                    $sheet->setCellValue('O' . $key, $item[KeyNameConst::KEY_DURATION]);
                    $sheet->setCellValue('P' . $key, $item[KeyNameConst::KEY_REAL_TIME]);
                    $sheet->setCellValue('Q' . $key, $item[KeyNameConst::KEY_SCORE]);
                    $sheet->setCellValue('R' . $key, $item[KeyNameConst::KEY_SCORE_TO_PASS]);
                    if ($item[KeyNameConst::KEY_RESULT] == 1) {
                        $result = $translator->trans('trainers.statistics.result_passed');
                    } else {
                        $result = $translator->trans('trainers.statistics.result_failed');
                    }
                    $sheet->setCellValue('S' . $key, $result);

                    if ($item[KeyNameConst::KEY_COMPLETION_DATE] && $item[KeyNameConst::KEY_RESULT]) {
                        $completionDate = $item[KeyNameConst::KEY_COMPLETION_DATE]->format('d/m/Y');
                    } else {
                        $completionDate = '';
                    }
                    $sheet->setCellValue('T' . $key, $completionDate);
                }
            }
            $sheet->setTitle($translator->trans('trainers.statistics.type_report_by_module'));
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        if ($options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $entity = $this->getDoctrine()->getRepository(Entity::class)
                    ->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_ENTITY_ID]]);
            $entityName = $entity->getDesignation();
            $fileName = $entityName . '_statistics (' . date('d-m-Y') . ').xlsx';
        } else {
            $fileName = 'entity_all_statistics (' . date('d-m-Y') . ').xlsx';
        }
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     * @throws \Exception
     */
    private function getStatisticByLoggedUsersInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $learnersIntervention = $intervention->getLearners();
            foreach ($learnersIntervention as $learnerIntervention) {
                $learner = $learnerIntervention->getLearner();
                if ($this->checkUserByConditionFilters($learner, $options)) {
                    $email = $learner->getPerson()->getEmail();
                    $lastLogin = $learner->getPerson()->getLastLogin() != null ? $learner->getPerson()->getLastLogin()->format('d/m/Y H:i') : KeyNameConst::VALUE_N_A;
                    $data[] = array(KeyNameConst::KEY_EMAIL => $email,
                        KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                        KeyNameConst::KEY_ACTION_TYPE => KeyNameConst::VALUE_N_A,
                        KeyNameConst::KEY_DATE => $lastLogin,
                        KeyNameConst::KEY_DURATION => KeyNameConst::VALUE_N_A);
                }
            }
        }
        return $data;
    }

    /**
     * @param Profile $leaner
     * @param array $options
     * @return bool is learner match with filter condition
     */
    public function checkUserByConditionFilters(Profile $leaner, array $options) {
        $learnerArr = $this->buildFilterArrayFromProfile($leaner);
        return (($options[KeyNameConst::KEY_ENTITY_ID] == $learnerArr[KeyNameConst::KEY_ENTITY_ID] || $options[KeyNameConst::KEY_ENTITY_ID] == 0) &&
                ($options[KeyNameConst::KEY_REGIONAL_ID] == $learnerArr[KeyNameConst::KEY_REGIONAL_ID] || $options[KeyNameConst::KEY_REGIONAL_ID] == 0) &&
                ($options[KeyNameConst::KEY_SECTOR_ID] == $learnerArr[KeyNameConst::KEY_SECTOR_ID] || $options[KeyNameConst::KEY_SECTOR_ID] == 0) &&
                ($options[KeyNameConst::KEY_WORKPLACE_ID] == $learnerArr[KeyNameConst::KEY_WORKPLACE_ID] || $options[KeyNameConst::KEY_WORKPLACE_ID] == 0));
    }

    /**
     * @param Profile $leaner
     * @return array condition value from learner for compare with filter by statistics
     */
    private function buildFilterArrayFromProfile(Profile $leaner) {
        return [
            KeyNameConst::KEY_ENTITY_ID => $leaner->getEntity() != null ? $leaner->getEntity()->getId() : 0,
            KeyNameConst::KEY_REGIONAL_ID => $leaner->getRegional() != null ? $leaner->getRegional()->getId() : 0,
            KeyNameConst::KEY_SECTOR_ID => $leaner->getGroupOrganization() != null ? $leaner->getGroupOrganization()->getId() : 0,
            KeyNameConst::KEY_WORKPLACE_ID => $leaner->getWorkplace() != null ? $leaner->getWorkplace()->getId() : 0
        ];
    }

    /**
     * @param Profile $learner
     * @param Module $module
     * @param array $options
     * @return bool is learner match with filter condition
     */
    public function checkBookingStatusByConditionFilters(Profile $learner, Module $module, array $options) {
        if (isset($options[KeyNameConst::KEY_STATUS_ID]) && $options[KeyNameConst::KEY_STATUS_ID] == 0)
            return true;
        $statusArr = $this->buildFilterArrayFromBookingStatus($learner, $module);
        return ($options[KeyNameConst::KEY_STATUS_ID] == $statusArr[KeyNameConst::KEY_STATUS_ID]);
    }

    /**
     * @param Profile $learner
     * @param Module $module
     * @return array condition value from learner for compare with filter by statistics
     */
    private function buildFilterArrayFromBookingStatus(Profile $learner, Module $module) {
        $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)
                ->findBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner));
        return [KeyNameConst::KEY_STATUS_ID => $bookingAgenda != null && $bookingAgenda->getStatus() != null ? $bookingAgenda->getStatus()->getId() : 0];
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     * @throws \Exception
     */
    private function getStatisticByQuizResultsInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $maximumQuestion = 0;
        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            foreach ($modules as $module) {
                if ($module->getStoredModule()->getAppId() == StoredModule::QUIZ) {
                    $moduleIterations = $this->getDoctrine()->getRepository(ModuleIteration::class)
                            ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                    $endDate = $module->getEnding();
                    $scoreArr = $this->getScoreByModule($module);
                    foreach ($moduleIterations as $iteration) {
                        $quiz = [];
                        $learner = $iteration->getLearner();
                        if ($this->checkUserByConditionFilters($learner, $options)) {
                            $numberTrials = $module->getIteration();
                            $numOfQuestions = $module->getQuiz() != null ? $module->getQuiz()->getQuestionCount() : 0;
                            $maximumQuestion = $numOfQuestions > $maximumQuestion ? $numOfQuestions : $maximumQuestion;
                            $quizName = $module->getQuiz() != null ? $module->getQuiz()->getTitle() : '';
                            $status = $iteration->getStatus() ? $iteration->getStatus()->getDesignation() : '';
                            $learnerInfo = $this->getLearnerInfo($learner);
                            $quizResults = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                                    ->findBy([
                                KeyNameConst::KEY_STUDENT => $learner,
                                KeyNameConst::KEY_QUIZ => $module->getQuiz(),
                                KeyNameConst::KEY_MODULE => $module->getId(),
                                    ], array(KeyNameConst::KEY_SCORE => 'DESC'), 1);
                            $questions = $module->getQuiz()->getQuestions();
                            $index = 0;
                            foreach ($questions as $question) {
                                $index++;
                                $score = 0;
                                $status = -1;
                                $percentScore = KeyNameConst::VALUE_N_A;
                                $totalPoints = $question->getPoints();
                                if ($quizResults != null && count($quizResults) > 0) {
                                    $answers = json_decode($quizResults[0]->getAnswers());
                                    if (false == empty($answers->{$question->getId()}) && is_array($answers->{$question->getId()})) {
                                        $score = $this->countQuestionScore($answers->{$question->getId()}, $question->getAnswers());
                                    }
                                    $status = $score > 0 ? 1 : 0;
                                    $percentScore = ($score > 0 ? round(($score * 100 / $totalPoints), 2) : 0) . $this->percentStringCharacter;
                                }
                                $quiz[] = [
                                    KeyNameConst::KEY_QUESTION => $question->getText(),
                                    KeyNameConst::KEY_STATUS => $status,
                                    KeyNameConst::KEY_QUESTION_SCORE => $percentScore
                                ];
                            }
                            $dataRow = [
                                KeyNameConst::KEY_COURSE_NAME => $module->getIntervention()->getDesignation(),
                                KeyNameConst::KEY_QUIZ_NAME => $quizName,
                                KeyNameConst::KEY_STATUS => $status,
                                KeyNameConst::KEY_END_DATE => $endDate->format('d/m/Y'),
                                KeyNameConst::KEY_NUM_TRIAL => $numberTrials,
                                KeyNameConst::KEY_NUM_QUESTION => $numOfQuestions,
                                KeyNameConst::KEY_NUM_RESULTS => $quiz,
                            ];
                            $data[] = array_merge($dataRow, $scoreArr, $learnerInfo);
                        }
                    }
                }
            }
        }

        return array('maximumHeader' => $maximumQuestion, 'data' => $data);
    }

    /**
     * Get headers for template requested
     * 
     * @param string $type
     * @param array $options
     * 
     * @return array
     */
    private function getStatisticheaders($type = false, $options = false) {
        // Load & init
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $headers = [];

        // Check personnal tabs / cols choosed
        $personnalExport = false;
        if (isset($options['isPersonnalExport']) && $options['isPersonnalExport'] == 'true') {
            $isPerso = true;
            // Get profile
            $profile = $em->getRepository('ApiBundle:Profile')->findOneBy(array('person' => parent::getUser()));
            $personnalExport = $em->getRepository('ApiBundle:StatsExportTemplateXref')->findBy(array('admin' => $profile));
        } else {
            $isPerso = false;
        }

        // Get template 
        if ($options && isset($options['tabs'])) {
            foreach ($options['tabs'] as $tab) {
                $templates[$tab] = $em->getRepository('ApiBundle:StatsExportTemplate')->findBy(array('type' => $this->translations_key[$options['type']], 'tab' => $tab));
            }

            if (is_array($templates)) {
                foreach ($templates as $tab => $template) {
                    foreach ($template as $keyCol => $col) {
                        $params = json_decode($col->getParams());
                        $colAdded = false;
                        $stringTranslate = 'admin.statistic.' . $this->translations_key[$type] . '.export.tab_' . $col->getTab() . '.col_' . $col->getCol();
                        // Check if we are in multi column mode or not, we only add headers in normal mode (1 header = 1 data), multi-col are added when we add datas
                        if ($translator->trans($stringTranslate) != $stringTranslate) {
                            if (!$isPerso) {
                                // Normal mode 1 col = 1 header
                                $headers[$tab][] = $translator->trans($stringTranslate);
                                $colAdded = true;
                            } else {
                                foreach ($personnalExport as $colSelected) {
                                    if ($colSelected->getTemplate()->getId() == $col->getId()) {
                                        $headers[$tab][] = $translator->trans($stringTranslate);
                                        $colAdded = true;
                                    }
                                }
                            }
                        }

                        // Delete in template if no column (most case is when we are in export personnal) or column translation not defined
                        if ($colAdded == false) {
                            if (!isset($params->multi_col)) {
                                unset($templates[$tab][$keyCol]);
                            }
                        }
                    }

                    // If no column in this tab
                    if (empty($templates[$tab])) {
                        unset($templates[$tab]);
                    }
                }
            }
        }

        return array($headers, $templates, $personnalExport);
    }

    /**
     * getStatisticProcessing
     * 
     * Used for Quiz tab, but could be reused for other tabs with same logic
     * 1 - Define tabs & column in DB & Translations
     * 2 - Define action & tab in "typeReport" sended by ajax
     * 3 - Switch between tabs here
     * 4 - Add your column specific treatment in "fillColData()" method with an UNIQUE name, and add the name of this specific treatment in "stats_export_template" table > "params" col > like that '{"key":"course_name"}' (where course_name = unique key for unique treatment)
     * 
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     * @throws \Exception
     */
    private function getStatisticProcessing(DateTime $from, DateTime $to, array $options = [], $template, $quizServiceAPI, $quizStatService, $headers, $personnalExport = false) {
        $translator = $this->get('translator');

        $data = [];
        $numAssessment = 0;

        // Get all intervention > module for filters choosed
        $interventions = $this->getInterventionLists($from, $to, $options, true);
        foreach ($interventions as $intervention) {
            $learnersInterventions = $intervention->getLearners();
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            foreach ($learnersInterventions as $learnerIntervention) {
                $learner = $learnerIntervention->getLearner();
                if ($this->checkUserByConditionFilters($learner, $options)) {
                    foreach ($modules as $module) {
                        if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                            StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                            StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()
                        ) {
                            continue;
                        }
                        $learnerInfo = $this->getLearnerInfo($learner);
                        if (in_array($options[KeyNameConst::KEY_GROUP_ID], $learnerInfo[KeyNameConst::KEY_GROUP_ID]) || $options[KeyNameConst::KEY_GROUP_ID] == 0) {
                            $moduleName = $module->getDesignation();
                            $moduleDuration = $module->getDuration()->format('H:i:s');


                            $moduleType = $module->getStoredModule()->getAppId();
                            if ($moduleType == StoredModule::ASSESSMENT) {
                                $numAssessment++;
                            }

                            if ($module->getQuiz() &&
                                    ($options['quizIsSurvey'] == 'all' || $options['quizIsSurvey'] == $module->getQuiz()->getIsSurvey()) &&
                                    ($options['quizId'] == 'all' || $options['quizId'] == $module->getQuiz()->getId())) {
                                // Rating
                                $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                                        ->getResultsByIntervention($intervention->getId());
                                $nbNotation = 0;
                                $notation = 0;
                                foreach ($learnersNotation as $learnerNotation) {
                                    if ($learnerNotation->getNotation() > 0) {
                                        ++$nbNotation;
                                        $notation += $learnerNotation->getNotation();
                                    }
                                }
                                $totalNotation = $nbNotation == 0 || $notation == 0 ? 0 : round($notation / $nbNotation, 2);
                                // End rating
                                $dataRow = [
                                    KeyNameConst::KEY_MODULE => $module,
                                    KeyNameConst::KEY_ORGANIZATION => $intervention->getEntity()->getOrganisation()->getDesignation(),
                                    KeyNameConst::KEY_ENTITY => $intervention->getEntity()->getDesignation(),
                                    KeyNameConst::KEY_LEARNERS => $intervention->getLearners(),
                                    KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                                    KeyNameConst::KEY_MODULE_NAME => $moduleName,
                                    KeyNameConst::KEY_QUIZ => $module->getQuiz(),
                                    KeyNameConst::KEY_QUIZ_STEPS => $quizServiceAPI->sanitizeQuiz($module->getQuiz()),
                                    KeyNameConst::KEY_QUIZ_STATS_BY_MODULE => $quizStatService->getByModule($module),
                                    KeyNameConst::KEY_NUM_OF_ASSESSMENT => $numAssessment,
                                    KeyNameConst::KEY_DURATION => $moduleDuration,
                                    KeyNameConst::KEY_RATING => $totalNotation
                                ];
                                $lines[$intervention->getId() . '-' . $module->getId()] = $dataRow;
                            }
                        }
                    }
                }
            }
        }

        // Keep headers
        $headersBase = $headers;
        // Check tab selected
        $data = [];
        if ($options['tabs'] && isset($lines)) {
            foreach ($options['tabs'] as $tab) {
                if (isset($headers[$tab])) {
                    $data[$tab] = [];
                    // Specific process by tab
                    switch ($tab) {
                        // By assignment
                        case 1 :
                            // Loop modules
                            $lineCompt = 0;
                            foreach ($lines as $index => $line) {
                                $lineCompt++;
                                $data[$tab][] = [];
                                // Loop cols
                                foreach ($template[$tab] as $col) {
                                    $params = json_decode($col->getParams());
                                    if (isset($params->key)) {
                                        $colKey = $params->key;
                                        // MULTI COL
                                        if (isset($params->multi_col)) {

                                            if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                $showCol = true;
                                                // Check for personnal export
                                                if ($personnalExport) {
                                                    $showCol = false;
                                                    foreach ($personnalExport as $perso) {
                                                        if ($perso->getTemplate()->getId() == $col->getId()) {
                                                            $showCol = true;
                                                        }
                                                    }
                                                }
                                                $headersToUse = [];
                                                if ($showCol == true) {
                                                    for ($i = 1; $i <= $params->multi_col; $i++) {
                                                        // Generate headers multi translations
                                                        $stringTranslate = 'admin.statistic.' . $col->getType() . '.export.tab_' . $col->getTab() . '.col_' . $col->getCol() . '.col_' . ($i);
                                                        $headersToUse[] = $translator->trans($stringTranslate);
                                                    }
                                                    // Process
                                                    list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line, $headersBase[$tab], $headers[$tab], $headersToUse);
                                                    // Apply headers
                                                    $headers[$tab] = $dataHeaders;

                                                    // Apply data cols
                                                    $data[$tab][count($data[$tab]) - 1] = array_merge($data[$tab][count($data[$tab]) - 1], $dataCol);
                                                }
                                            }
                                        }
                                        // NORMAL COL
                                        else {
                                            if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                // Fill normal column
                                                list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line);
                                                $data[$tab][count($data[$tab]) - 1][] = $dataCol;
                                            } else {
                                                $data[$tab][count($data[$tab]) - 1][] = '';
                                            }
                                        }
                                    } else {
                                        $data[$tab][count($data[$tab]) - 1][] = '';
                                    }
                                }
                            }
                            break;
                        // By question
                        case 2 :
                            // Loop modules
                            $lineCompt = 0;
                            foreach ($lines as $index => $line) {
                                foreach ($line[KeyNameConst::KEY_QUIZ_STEPS] as $step) {
                                    $line[KeyNameConst::KEY_QUIZ_STEP] = $step;
                                    foreach ($step['questions'] as $question) {
                                        $line[KeyNameConst::KEY_QUIZ_QUESTION] = $question;
                                        $lineCompt++;
                                        $data[$tab][] = [];
                                        // Loop cols
                                        foreach ($template[$tab] as $col) {
                                            $params = json_decode($col->getParams());
                                            if (isset($params->key)) {
                                                $colKey = $params->key;
                                                // MULTI COL
                                                if (isset($params->multi_col)) {
                                                    if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                        $showCol = true;
                                                        // Check for personnal export
                                                        if ($personnalExport) {
                                                            $showCol = false;
                                                            foreach ($personnalExport as $perso) {
                                                                if ($perso->getTemplate()->getId() == $col->getId()) {
                                                                    $showCol = true;
                                                                }
                                                            }
                                                        }

                                                        if ($showCol == true) {
                                                            $headersToUse = [];
                                                            for ($i = 1; $i <= $params->multi_col; $i++) {
                                                                // Generate headers multi translations
                                                                $stringTranslate = 'admin.statistic.' . $col->getType() . '.export.tab_' . $col->getTab() . '.col_' . $col->getCol() . '.col_' . ($i);
                                                                $headersToUse[] = $translator->trans($stringTranslate);
                                                            }
                                                            // Process
                                                            list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line, $headersBase[$tab], $headers[$tab], $headersToUse);
                                                            // Apply headers
                                                            $headers[$tab] = $dataHeaders;

                                                            // Apply data cols
                                                            $data[$tab][count($data[$tab]) - 1] = array_merge($data[$tab][count($data[$tab]) - 1], $dataCol);
                                                        }
                                                    }
                                                }
                                                // NORMAL COL
                                                else {
                                                    // Fill normal column
                                                    if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                        list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line);
                                                        $data[$tab][count($data[$tab]) - 1][] = $dataCol;
                                                    } else {
                                                        $data[$tab][count($data[$tab]) - 1][] = '';
                                                    }
                                                }
                                            } else {
                                                $data[$tab][count($data[$tab]) - 1][] = '';
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        // By user
                        case 3 :
                            // Loop modules
                            $lineCompt = 0;
                            foreach ($lines as $index => $line) {
                                foreach ($line[KeyNameConst::KEY_LEARNERS] as $learner) {
                                    $line[KeyNameConst::KEY_LEARNER] = $learner;
                                    $line = array_merge($line, $this->getLearnerInfo($learner->getLearner()));
                                    $lineCompt++;
                                    $data[$tab][] = [];
                                    // Loop cols
                                    foreach ($template[$tab] as $col) {
                                        $params = json_decode($col->getParams());
                                        if (isset($params->key)) {
                                            $colKey = $params->key;
                                            // MULTI COL
                                            if (isset($params->multi_col)) {
                                                if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                    $showCol = true;
                                                    // Check for personnal export
                                                    if ($personnalExport) {
                                                        $showCol = false;
                                                        foreach ($personnalExport as $perso) {
                                                            if ($perso->getTemplate()->getId() == $col->getId()) {
                                                                $showCol = true;
                                                            }
                                                        }
                                                    }

                                                    if ($showCol == true) {
                                                        $headersToUse = [];
                                                        for ($i = 1; $i <= $params->multi_col; $i++) {
                                                            // Generate headers multi translations
                                                            $stringTranslate = 'admin.statistic.' . $col->getType() . '.export.tab_' . $col->getTab() . '.col_' . $col->getCol() . '.col_' . ($i);
                                                            $headersToUse[] = $translator->trans($stringTranslate);
                                                        }
                                                        // Process
                                                        list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line, $headersBase[$tab], $headers[$tab], $headersToUse);
                                                        // Apply headers
                                                        $headers[$tab] = $dataHeaders;

                                                        // Apply data cols
                                                        $data[$tab][count($data[$tab]) - 1] = array_merge($data[$tab][count($data[$tab]) - 1], $dataCol);
                                                    }
                                                }
                                            }
                                            // NORMAL COL
                                            else {
                                                if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                    // Fill normal column
                                                    list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line);
                                                    $data[$tab][count($data[$tab]) - 1][] = $dataCol;
                                                } else {
                                                    $data[$tab][count($data[$tab]) - 1][] = '';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        // By reply
                        case 4 :
                            // Loop modules
                            $lineCompt = 0;
                            foreach ($lines as $index => $line) {
                                foreach ($line[KeyNameConst::KEY_LEARNERS] as $learner) {
                                    $line[KeyNameConst::KEY_LEARNER] = $learner;
                                    $line = array_merge($line, $this->getLearnerInfo($learner->getLearner()));

                                    if (isset($line[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer']) && isset($line[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'][$learner->getLearner()->getPerson()->getId()])) {
                                        foreach ($line[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'][$learner->getLearner()->getPerson()->getId()] as $userPlayed) {
                                            foreach ($userPlayed['answers']['answers'] as $answer) {
                                                $lineCompt++;
                                                $data[$tab][] = [];
                                                $line[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER] = $answer;
                                                $step = $answer->getQuestion()->getQuestionXref()[0]->getStep();
                                                $step_params = json_decode($step->getParams());
                                                $line[KeyNameConst::KEY_QUIZ_STEP] = array(
                                                    'step_pos' => $step_params->position,
                                                    'step' => $step
                                                );
                                                $line[KeyNameConst::KEY_QUIZ_QUESTION] = $answer->getQuestion();
                                                // Loop cols
                                                foreach ($template[$tab] as $col) {
                                                    $params = json_decode($col->getParams());
                                                    if (isset($params->key)) {
                                                        $colKey = $params->key;

                                                        // MULTI COL            
                                                        if (isset($params->multi_col)) {
                                                            if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                                $showCol = true;
                                                                // Check for personnal export
                                                                if ($personnalExport) {
                                                                    $showCol = false;
                                                                    foreach ($personnalExport as $perso) {
                                                                        if ($perso->getTemplate()->getId() == $col->getId()) {
                                                                            $showCol = true;
                                                                        }
                                                                    }
                                                                }

                                                                if ($showCol == true) {
                                                                    $headersToUse = [];
                                                                    for ($i = 1; $i <= $params->multi_col; $i++) {
                                                                        // Generate headers multi translations
                                                                        $stringTranslate = 'admin.statistic.' . $col->getType() . '.export.tab_' . $col->getTab() . '.col_' . $col->getCol() . '.col_' . ($i);
                                                                        $headersToUse[] = $translator->trans($stringTranslate);
                                                                    }
                                                                    // Process
                                                                    list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line, $headersBase[$tab], $headers[$tab], $headersToUse);
                                                                    // Apply headers
                                                                    $headers[$tab] = $dataHeaders;

                                                                    // Apply data cols
                                                                    $data[$tab][count($data[$tab]) - 1] = array_merge($data[$tab][count($data[$tab]) - 1], $dataCol);
                                                                }
                                                            }
                                                        }
                                                        // NORMAL COL
                                                        else {
                                                            if (!isset($params->hideForSurvey) || $line[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0) {
                                                                // Fill normal column
                                                                list($dataCol, $dataHeaders) = $this->fillColData($colKey, $line);
                                                                $data[$tab][count($data[$tab]) - 1][] = $dataCol;
                                                            } else {
                                                                $data[$tab][count($data[$tab]) - 1][] = '';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

        return array($data, $headers);
    }

    /**
     *  Fill col data
     * 
     * @param string $colKey name of action to do to get data
     * @param array $data datas to use
     * @param array $headersTab headers 
     * @param array $headersTab headers updated
     * @param array $headersToUse headers to use for multi-col generation
     * @return data to fill
     */
    private function fillColData($colKey, $data, $headersBase = false, $headersTab = false, $headersToUse = false) {
        $translator = $this->get('translator');

        switch ($colKey) {
            // Tab 1
            case 'organization_name':
                $col = $data[KeyNameConst::KEY_ORGANIZATION];
                break;
            case 'entity_name':
                $col = $data[KeyNameConst::KEY_ENTITY];
                break;
            case 'learner_regional' :
                $col = $data[KeyNameConst::KEY_REGIONAL];
                break;
            case 'learner_workplace' :
                $col = $data[KeyNameConst::KEY_WORKPLACE];
                break;
            case 'learner_group' :
                $col = $data[KeyNameConst::KEY_GROUP];
                break;
            case 'learner_group_name' :
                $col = $data[KeyNameConst::KEY_GROUP_NAME];
                break;
            case 'course_name' :
                $col = $data[KeyNameConst::KEY_COURSE_NAME];
                //$col = $data[KeyNameConst::KEY_QUIZ_STEPS];
                break;
            case 'module_name' :
                $col = $data[KeyNameConst::KEY_MODULE_NAME];
                break;
            case 'quiz_is_survey' :
                $col = $translator->trans('admin.statistic.quiz.is_survey.' . ($data[KeyNameConst::KEY_QUIZ]->getIsSurvey() == 0 ? 'no' : 'yes'));
                break;
            case 'quiz_count_steps' :
                $col = count($data[KeyNameConst::KEY_QUIZ_STEPS]);
                break;
            case 'quiz_count_question' :
                $col = count($data[KeyNameConst::KEY_QUIZ]->getQuestions());
                break;
            case 'quiz_question_total_points' :
                $questions = $data[KeyNameConst::KEY_QUIZ]->getQuestions();
                $total = 0;
                if ($questions) {
                    foreach ($questions as $question) {
                        $total += $question->getQuestion()->getScore_to_pass();
                    }
                }
                $col = $total;
                break;
            case 'num_of_assesments' :
                $col = ($data[KeyNameConst::KEY_NUM_OF_ASSESSMENT] == 0 ? $translator->trans('admin.statistic.quiz.no') : $translator->trans('admin.statistic.quiz.yes'));
                break;
            case 'module_duration' :
                $col = $data[KeyNameConst::KEY_DURATION];
                break;
            case 'module_rating' :
                $col = $data[KeyNameConst::KEY_RATING] == KeyNameConst::VALUE_N_A ? KeyNameConst::VALUE_N_A : $data[KeyNameConst::KEY_RATING] . '/5';
                break;
            case 'quiz_score_to_pass' :
                $col = $data[KeyNameConst::KEY_MODULE]->getScoreQuiz() . '%';
                break;
            case 'quiz_by_module_step_details' :
                $steps = $data[KeyNameConst::KEY_QUIZ_STEPS];

                $col = [];
                foreach ($steps as $k => $step) {
                    // Add headers 
                    foreach ($headersToUse as $header) {
                        $headersBase[] = str_replace('%s', ((int) $step['step_pos'] + 1), $header);
                    }
                    // Update headers only if new line generate more headers
                    if (count($headersBase) > count($headersTab)) {
                        $headersTab = $headersBase;
                    }
                    $col[] = $step['step']->getTitle();
                    if ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_step_details']) {
                        foreach ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_step_details'] as $step_id => $stepScoring) {
                            if ($step_id == $step['step']->getId()) {
                                $totalExpected = 0;
                                foreach ($stepScoring['questionsScores'] as $questionScoring) {
                                    $totalExpected += $questionScoring['expected'];
                                }
                                // Score moy
                                $scoreMoy = ($stepScoring['totalScore'] / ($totalExpected * $stepScoring['played']) * 100);

                                // Score pass
                                $stepPassed = 0;
                                foreach ($stepScoring['usersDetails'] as $user) {
                                    $totalUser = 0;
                                    foreach ($user['questions'] as $question) {
                                        $totalUser += $question;
                                    }
                                    if ($totalUser >= $totalExpected) {
                                        $stepPassed++;
                                    }
                                }
                                $scorePass = $stepPassed / $stepScoring['played'] * 100;

                                // Data to show
                                $col[] = number_format($scoreMoy, 2) . '%';
                                $col[] = number_format($scorePass, 2) . '%';
                                if (isset($stepScoring['totalTime'])) {
                                    $col[] = gmdate('H:i:s', $stepScoring['totalTime'] / count($stepScoring['usersDetails']));
                                } else {
                                    $col[] = KeyNameConst::VALUE_N_A;
                                }
                            }
                        }
                    } else {
                        $col[] = KeyNameConst::VALUE_N_A;
                    }
                }
                break;

            // Tab 2
            case 'quiz_step_pos' :
                $col = $data[KeyNameConst::KEY_QUIZ_STEP]['step_pos'] + 1;
                break;
            case 'quiz_step_name' :
                $col = $data[KeyNameConst::KEY_QUIZ_STEP]['step']->getTitle();
                break;
            case 'quiz_question_pos' :
                $params = $data[KeyNameConst::KEY_QUIZ_QUESTION]->getParams();
                if ($params) {
                    $params = json_decode($params);
                    $col = ($data[KeyNameConst::KEY_QUIZ_STEP]['step_pos'] + 1) . '.' . ((int) $params->position + 1);
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_question_type' :
                $col = $translator->trans('admin.quiz.create.types.' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getType());
                break;
            case 'quiz_question_libelle' :
                $col = $data[KeyNameConst::KEY_QUIZ_QUESTION]->getQuestion();

                if ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getType() == 'text_to_fill') {
                    $col .= '<br />-------------<br />' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->items[0]->getValue();
                }
                break;
            case 'quiz_question_answer' :
                if ($data[KeyNameConst::KEY_QUIZ_QUESTION]->items) {
                    $col = [];
                    foreach ($data[KeyNameConst::KEY_QUIZ_QUESTION]->items as $key => $item) {
                        switch ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getType()) {
                            case 'qcu' :
                                if ((int) $item->getValue() == 1) {
                                    $col[] = $item->getName();
                                }
                                break;
                            case 'qcm' :
                                if ((int) $item->getValue() == 1) {
                                    $col[] = $item->getName();
                                }
                                break;
                            case 'drag_and_drop' :
                                $specifics = json_decode($item->getSpecifics());
                                if ($item->getName() == 'image_to_text') {
                                    $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $item->getValue() . '">img</a> => ' . $specifics->match;
                                } else {
                                    $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $item->getValue() . '">img</a> => <a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $specifics->match . '">img</a>';
                                }
                                break;
                            case 'ordering' :
                                $col[] = ($key + 1) . ' => ' . $item->getName();
                                break;
                            case 'category' :
                                $col[] = $item->getName() . ' => ' . $item->getValue();
                                break;
                            case 'text_to_fill' :
                                $col = (array) json_decode($item->getSpecifics())->options;
                                break;
                            case 'open' :
                                if ($col == []) {
                                    $col[] = $translator->trans('admin.statistic.quiz.answer.open');
                                }
                                break;
                            case 'scoring' :
                                $col[] = $item->getValue();
                                break;
                        }
                    }
                    if (!empty($col)) {
                        $col = implode(' // ', $col);
                    } else {
                        $col = $data[KeyNameConst::KEY_QUIZ_QUESTION]->items;
                    }
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_question_success' :
                if ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'] && isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()])) {
                    $currScoring = $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()];
                    $col = number_format(($currScoring['score'] / $currScoring['played']) / $data[KeyNameConst::KEY_QUIZ_QUESTION]->getScore_to_pass() * 100, 2) . '%';
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_question_duration' :
                if ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer']) {
                    $timersCumulate = 0;
                    $nbTimer = 0;
                    foreach ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'] as $user_id => $played) {
                        foreach ($played as $play_id => $play) {
                            if (isset($play['answers']['answers'])) {
                                foreach ($play['answers']['answers'] as $play_id => $answer) {
                                    if ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() == $answer->getQuestion()->getId()) {
                                        if (isset($answer->getParams()->timer)) {
                                            $timersCumulate += (int) $answer->getParams()->timer;
                                            $nbTimer++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($nbTimer) {
                        $col = gmdate('H:i:s', ($timersCumulate / $nbTimer));
                    } else {
                        $col = KeyNameConst::VALUE_N_A;
                    }
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_by_module_question_details' :
                $col = [];
                $showedReplies = 0;
                foreach ($data[KeyNameConst::KEY_QUIZ_QUESTION]->items as $key => $item) {
                    // Add headers 
                    foreach ($headersToUse as $header) {
                        $headersBase[] = str_replace('%s', ((int) $key + 1), $header);
                    }
                    // Update headers only if new line generate more headers
                    if (count($headersBase) > count($headersTab)) {
                        $headersTab = $headersBase;
                    }
                    switch ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getType()) {
                        case 'qcu' :
                            // Qcu
                            $col[] = $item->getName();
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId()])) {
                                $col[] = number_format($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId()] / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                            } else {
                                $col[] = '0%';
                            }
                            break;
                        case 'qcm' :
                            // Qcm
                            $col[] = $item->getName();
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId()])) {
                                $col[] = number_format($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId()] / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                            } else {
                                $col[] = '0%';
                            }
                            break;
                        case 'drag_and_drop' :
                            // Drag And Drop
                            $specifics = json_decode($item->getSpecifics());
                            if ($item->getName() == 'image_to_text') {
                                $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $item->getValue() . '">img</a>' . ' => ' . $specifics->match;
                            } else {
                                $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $item->getValue() . '">img</a>' . ' => <a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $specifics->match . '">img</a>';
                            }
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getValue() . '-' . $specifics->match])) {
                                $col[] = number_format($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getValue() . '-' . $specifics->match] / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                                $showedReplies += $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getValue() . '-' . $specifics->match];
                            } else {
                                $col[] = '0%';
                            }
                            break;
                        case 'ordering' :
                            // Ordering
                            $col[] = $translator->trans('admin.statistic.quiz.answer.ordering_position') . ' ' . ($key + 1);
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getName()])) {
                                $col[] = number_format($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getName()] / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                            } else {
                                $col[] = '0%';
                            }
                            break;
                        case 'category' :
                            // Category
                            $col[] = $item->getName() . ' => ' . $item->getValue();
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId() . '-' . $item->getValue()])) {
                                $col[] = number_format($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId() . '-' . $item->getValue()] / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                                $showedReplies += (int) $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'][(string) $item->getId() . '-' . $item->getValue()];
                            } else {
                                $col[] = '0%';
                            }
                            break;
                        case 'text_to_fill' :
                            // Nothind to do
                            break;
                        case 'open' :
                            // Nothind to do
                            break;
                        case 'scoring' :
                            // Scoring
                            if (isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'])) {
                                $currItems = $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['items'];
                                asort($currItems);
                                foreach ($currItems as $itemVal => $itemNbreply) {
                                    $col[] = $itemVal;
                                    $col[] = number_format($itemNbreply / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                                    $showedReplies += $itemNbreply;
                                }
                            }
                            break;
                    }
                }

                // Other replies for some question types
                if ($showedReplies > 0) {
                    // Other replies
                    $headersBase[] = $translator->trans('admin.statistic.quiz.other');
                    if (count($headersBase) > count($headersTab)) {
                        $headersTab = $headersBase;
                    }
                    $col[] = $translator->trans('admin.statistic.quiz.other') . ' => ' . number_format(($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] - $showedReplies) / $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_by_question'][$data[KeyNameConst::KEY_QUIZ_QUESTION]->getId()]['itemsCount'] * 100, 2) . '%';
                }
                break;

            // Tab 3
            case 'learner_lastname' :
                $col = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getLastName();
                break;
            case 'learner_firstname' :
                $col = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getFirstName();
                break;
            case 'learner_email' :
                $col = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getOtherEmail();
                break;
            case 'quiz_by_module_learner_duration_total' :
            case 'quiz_by_module_learner_duration_moy' :
            case 'quiz_by_module_learner_nb_played' :
                $user = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getPerson()->getId();
                if ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'] && isset($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'][$user])) {
                    $timersTotal = [];
                    foreach ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_by_question_answer'] as $user_id => $played) {
                        if ($user == $user_id) {
                            foreach ($played as $play_id => $play) {
                                if (isset($play['answers']['answers'])) {
                                    foreach ($play['answers']['answers'] as $answer) {
                                        if (isset($answer->getParams()->timer_total)) {
                                            if (!isset($timersTotal[$play_id])) {
                                                $timersTotal[$play_id] = (int) $answer->getParams()->timer_total;
                                            }
                                            if ($timersTotal[$play_id] <= (int) $answer->getParams()->timer_total) {
                                                $timersTotal[$play_id] = (int) $answer->getParams()->timer_total;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($timersTotal)) {
                        $timerCumulate = 0;
                        foreach ($timersTotal as $timer) {
                            $timerCumulate += $timer;
                        }
                        if ($colKey == 'quiz_by_module_learner_nb_played') {
                            $col = count($timersTotal);
                        } else if ($colKey == 'quiz_by_module_learner_duration_total') {
                            // quiz_by_module_learner_duration_total
                            $col = gmdate('H:i:s', $timerCumulate);
                        } else {
                            // quiz_by_module_learner_duration_moy
                            $col = gmdate('H:i:s', ($timerCumulate / count($timersTotal)));
                        }
                    } else {
                        if ($colKey == 'quiz_by_module_learner_nb_played') {
                            $col = 0;
                        } else {
                            $col = KeyNameConst::VALUE_N_A;
                        }
                    }
                } else {
                    if ($colKey == 'quiz_by_module_learner_nb_played') {
                        $col = 0;
                    } else {
                        $col = KeyNameConst::VALUE_N_A;
                    }
                }
                break;
            case 'quiz_by_module_learner_score_best' :
            case 'quiz_by_module_learner_score_best_date' :
            case 'quiz_by_module_learner_score_last' :
            case 'quiz_by_module_learner_score_last_date' :
            case 'module_status' :
                $status = false;
                if ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_score']) {
                    $userScoreBest = 0;
                    $userScoreLast = 0;
                    $userDateBest = '';
                    $userDateLast = '';
                    foreach ($data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE]['quiz_by_module_learner_score'] as $user_id => $scores) {
                        if ($user_id == $data[KeyNameConst::KEY_LEARNER]->getLearner()->getPerson()->getId()) {
                            foreach ($scores as $score) {
                                // Last score
                                $userScoreLast = $score['score'];
                                $userDateLast = $score['date'];
                                // Best score
                                if ($score['score'] > $userScoreBest) {
                                    $userScoreBest = $score['score'];
                                    $userDateBest = $score['date'];
                                }
                                // Status module ok / nok
                                if ($score['score'] > $data[KeyNameConst::KEY_MODULE]->getScoreQuiz()) {
                                    $status = true;
                                }
                            }
                        }
                    }
                    if ($userScoreLast != 0) {
                        switch ($colKey) {
                            case 'quiz_by_module_learner_score_best' :
                                $col = $userScoreBest . '%';
                                break;
                            case 'quiz_by_module_learner_score_last' :
                                $col = $userScoreLast . '%';
                                break;
                            case 'quiz_by_module_learner_score_best_date' :
                                $col = $userDateBest;
                                break;
                            case 'quiz_by_module_learner_score_last_date' :
                                $col = $userDateLast;
                                break;
                        }
                    } else {
                        $col = KeyNameConst::VALUE_N_A;
                    }
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                if ($colKey == 'module_status') { 
                    $col = ($status == false ? $translator->trans('admin.statistic.quiz.failed') : $translator->trans('admin.statistic.quiz.success'));
                }
                break;

            case 'quiz_by_module_learner_answer' :
                $col = [];
                break;

            case 'quiz_by_module_learner_score_best_date' :
                $col = KeyNameConst::VALUE_N_A;
                break;
            case 'quiz_by_module_learner_score_last_date' :
                $col = KeyNameConst::VALUE_N_A;
                break;

            // Tab 4
            case 'learner_position' :
                $col = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getPosition();
                break;
            case 'learner_employee_id' :
                $col = $data[KeyNameConst::KEY_LEARNER]->getLearner()->getEmployeeId();
                break;
            case 'quiz_by_module_learner_by_question_answer' :
                if ($data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]) {
                    $answer = $data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER];
                    $params = $answer->getParams();
                    switch ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getType()) {
                        case 'qcu' :
                            if (isset($params->answer->value)) {
                                foreach ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getItems() as $item) {
                                    if ((int) $params->answer->value == $item->getId()) {
                                        $col = $item->getName();
                                    }
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            }
                            break;
                        case 'qcm' :
                            if (isset($params->answer->value)) {
                                foreach ($params->answer->value as $value) {
                                    foreach ($data[KeyNameConst::KEY_QUIZ_QUESTION]->getItems() as $item) {
                                        if ((int) $value == $item->getId()) {
                                            $col[] = $item->getName();
                                        }
                                    }
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            } else {
                                $col = implode(' // ', $col);
                            }
                            break;
                        case 'drag_and_drop' :
                            if (isset($params->answer->value)) {
                                foreach ($params->answer->value as $valueDD) {
                                    if (strpos($valueDD[1], '.') !== false) {
                                        $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $valueDD[0] . '">img</a>' . ' => ' . '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $valueDD[1] . '">img</a>';
                                    } else {
                                        $col[] = '<a target="_blank" href="/files/quiz/' . $data[KeyNameConst::KEY_QUIZ_QUESTION]->getId() . '/drag-and-drop/' . $valueDD[0] . '">img</a>' . ' => ' . $valueDD[1];
                                    }
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            } else {
                                $col = implode(' // ', $col);
                            }
                            break;
                        case 'ordering' :
                            if (isset($params->answer->value)) {
                                foreach ($params->answer->value as $keyOrdering => $valueOrdering) {
                                    $col[] = $keyOrdering + 1 . ' => ' . $valueOrdering;
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            } else {
                                $col = implode(' // ', $col);
                            }
                            break;
                        case 'category' :
                            if (isset($params->answer->value)) {
                                if ($data[KeyNameConst::KEY_QUIZ_QUESTION]->items) {
                                    foreach ($data[KeyNameConst::KEY_QUIZ_QUESTION]->items as $key => $item) {
                                        if ($params->items_ids) {
                                            foreach ($params->items_ids as $keyItemChoosed => $itemChoosed) {
                                                if ((int) $itemChoosed == $item->getId()) {
                                                    if ($params->answer->value[$keyItemChoosed] != $translator->trans('learners.courses.quiz.question.category.choose')) {
                                                        $col[] = $item->getName() . ' => ' . $params->answer->value[$keyItemChoosed];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            } else {
                                $col = implode(' // ', $col);
                            }
                            break;
                        case 'text_to_fill' :
                            if (isset($params->answer->value)) {
                                foreach ($params->answer->value as $valueTTF) {
                                    $col[] = $valueTTF[1];
                                }
                            }

                            if (!isset($col)) {
                                $col = KeyNameConst::VALUE_N_A;
                            } else {
                                $col = implode(' // ', $col);
                            }
                            break;
                        case 'open' :
                            if (isset($params->answer->free)) {
                                $col = $params->answer->free;
                            } else {
                                $col = KeyNameConst::VALUE_N_A;
                            }
                            break;
                        case 'scoring' :
                            if (isset($params->answer->value)) {
                                $col = $params->answer->value;
                            } else {
                                $col = KeyNameConst::VALUE_N_A;
                            }
                            break;
                        default :
                            $col = KeyNameConst::VALUE_N_A;
                            break;
                    }
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_by_module_learner_by_question_duration' :
                if ($data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]->getParams() && isset($data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]->getParams()->timer)) {
                    $col = gmdate('H:i:s', $data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]->getParams()->timer);
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_by_module_learner_by_question_status' :
                if ($data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]) {
                    $status = $data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]->getStatus();
                    $col = ($status == 0 ? $translator->trans('admin.statistic.quiz.failed') : $translator->trans('admin.statistic.quiz.success'));
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
            case 'quiz_by_module_learner_by_question_score' :
                $col = $data[KeyNameConst::KEY_QUIZ_LEARNER_ANSWER]->getScores();
                break;

            default :
                if (strpos($colKey, 'quiz_by_module_') !== false) {
                    $col = $data[KeyNameConst::KEY_QUIZ_STATS_BY_MODULE][$colKey];
                } else {
                    $col = KeyNameConst::VALUE_N_A;
                }
                break;
        }
        return array($col, $headersTab);
    }

    /**
     * Generate an export XLSX with header & datas
     * 
     * @param array $templates
     * @param array $headers
     * @param array $datas
     */
    private function exportXlsx($templates, $headers, $datas, $options) {
        // Load
        $translator = $this->get('translator');
        //$wizard = new \PhpOffice\PhpSpreadsheet\Helper\Html();
        // Create Spreadsheet
        $spreadsheet = new Spreadsheet();

        $comptTabs = 0;
        foreach ($headers as $tab => $headersTab) {
            $comptTabs++;
            // Create new sheet if tab > 1
            if ($comptTabs > 1) {
                $sheet = $spreadsheet->createSheet();
            } else {
                $sheet = $spreadsheet->getActiveSheet();
            }
            // Set title
            $sheet->setTitle(substr($translator->trans('admin.statistic.' . $this->translations_key[$options['type']] . '.export.tab_' . $tab . '.title'), 0, 31));
            // Set Style
            $sheet->getStyle('1:1')->getFont()->setBold(true);
            // Create sheetArray
            $sheetArray = [];

            // Add headers to sheetArray
            foreach ($headersTab as $col => $header) {
                $sheetArray[0][] = $translator->trans($header);
            }
            if (isset($datas[$tab])) {
                foreach ($datas[$tab] as $data) {
                    $sheetArray[] = [];
                    foreach ($data as $cell) {
                        //$sheetArray[count($sheetArray) - 1][] = $wizard->toRichTextObject($cell);
                        $sheetArray[count($sheetArray) - 1][] = (empty($cell)) ? KeyNameConst::VALUE_N_A : (string) html_entity_decode(preg_replace('~[\r\n]+~', ' ', strip_tags($cell)), ENT_QUOTES | ENT_HTML5);
                    }
                }
            }

            $sheet->fromArray($sheetArray, NULL, 'A1');
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);
        // Create a Temporary file in the system
        if ($options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_ENTITY_ID]]);
            $entityName = $entity->getDesignation();
            $fileName = $entityName . '_assignment_statistics' . (isset($options['isPersonnalExport']) && $options['isPersonnalExport'] == 'true' ? '#custom' : '') . ' (' . date('d-m-Y') . ').xlsx';
        } else {
            $fileName = 'entity_all_assignment_statistics' . (isset($options['isPersonnalExport']) && $options['isPersonnalExport'] == 'true' ? '#custom' : '') . ' (' . date('d-m-Y') . ').xlsx';
        }
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @param $answers
     * @param Collection $possibleAnswers
     * @return int
     */
    private function countQuestionScore($answers, Collection $possibleAnswers) {
        $score = 0;
        $answers = array_combine($answers, $answers);
        foreach ($possibleAnswers as $possibleAnswer) {
            if (isset($answers[$possibleAnswer->getId()])) {
                $score += $possibleAnswer->getPoints();
            }
        }

        return $score;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     * @throws \Exception
     */
    private function getStatisticByModuleResultInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $interventions = $this->getInterventionLists($from, new \DateTime('last day of December this year +1 years'), $options, true);
        foreach ($interventions as $intervention) {
            $learnersInterventions = $intervention->getLearners();
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            foreach ($learnersInterventions as $learnerIntervention) {
                $learner = $learnerIntervention->getLearner();
                if ($this->checkUserByConditionFilters($learner, $options)) {
                    foreach ($modules as $module) {
                        if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                            if ($this->checkBookingStatusByConditionFilters($learner, $module, $options)) {
                                $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)
                                        ->findOneBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner));
                                if ($bookingAgenda && ($from <= $bookingAgenda->getBookingDate() && $bookingAgenda->getBookingDate() <= $to)) {
                                    $learnerInfo = $this->getLearnerInfo($learner);
                                    if (in_array($options[KeyNameConst::KEY_GROUP_ID], $learnerInfo[KeyNameConst::KEY_GROUP_ID]) || $options[KeyNameConst::KEY_GROUP_ID] == 0) {
                                        $status = $bookingAgenda != null && $bookingAgenda->getStatus() ? $bookingAgenda->getStatus()->getDesignation() : KeyNameConst::VALUE_N_A;
                                        $scoreArr = $this->getScoreAndBookingInfoByLearnerAndModule($module, $learner);
                                        $moduleName = $module->getDesignation();
                                        $moduleType = $module->getStoredModule()->getDesignation();
                                        $moduleDuration = $module->getDuration()->format('H:i:s');

                                        $end = clone $bookingAgenda->getBookingDate();
                                        $hour = (int) $module->getDuration()->format('G') + $bookingAgenda->getBookingDate()->format('G');
                                        $minutes = (int) $module->getDuration()->format('i') + $bookingAgenda->getBookingDate()->format('i');
                                        $end->setTime($hour, $minutes, 0);

                                        $dataRow = [
                                            KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                                            KeyNameConst::KEY_STATUS => $status,
                                            KeyNameConst::KEY_MODULE_NAME => $moduleName,
                                            KeyNameConst::KEY_MODULE_TYPE => $moduleType,
                                            KeyNameConst::KEY_TRAINER => !empty($bookingAgenda->getTrainer()) ? $bookingAgenda->getTrainer()->getDisplayName() : KeyNameConst::VALUE_N_A,
                                            KeyNameConst::KEY_DURATION => $moduleDuration,
                                            KeyNameConst::KEY_BOOKED_DATE => $bookingAgenda->getBookingDate(),
                                            KeyNameConst::KEY_BOOKING_SESSION => $bookingAgenda->getBookingDate()->format('H:i') . ' - ' . $end->format('H:i'),
                                        ];
                                        $data[] = array_merge($dataRow, $learnerInfo, $scoreArr);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @param Profile $learner
     * @return array learner information
     * @throws \Exception
     */
    public function getLearnerInfo(Profile $learner) {
        if ($learner == null) {
            throw new \Exception('Learner need to be a profile');
        }
        $groupNames = [];
        $groupIds = [];
        $learnerGroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));
        if ($learnerGroups) {
            foreach ($learnerGroups as $learnerGroup) {
                $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                $groupIds[] = $learnerGroup->getGroup()->getId();
            }
        }
        $groupName = implode(", ", $groupNames);
        $regional = $learner->getRegional() ? $learner->getRegional()->getDesignation() : '';
        $group = $learner->getGroupOrganization() ? $learner->getGroupOrganization()->getDesignation() : '';
        $workplace = $learner->getWorkplace() ? $learner->getWorkplace()->getDesignation() : '';
        return [
            KeyNameConst::KEY_FIRST_NAME => $learner->getFirstName(),
            KeyNameConst::KEY_LAST_NAME => $learner->getLastName(),
            KeyNameConst::KEY_EMAIL => $learner->getPerson()->getEmail(),
            KeyNameConst::KEY_POSITION_ID => $learner->getPosition(),
            KeyNameConst::KEY_EMPLOYEE_ID => $learner->getEmployeeId(),
            KeyNameConst::KEY_ORGANIZATION => !empty($learner->getEntity()) ? $learner->getEntity()->getOrganisation()->getDesignation() : null,
            KeyNameConst::KEY_ENTITY => !empty($learner->getEntity()) ? $learner->getEntity()->getDesignation() : null,
            KeyNameConst::KEY_REGIONAL => $regional,
            KeyNameConst::KEY_GROUP => $group,
            KeyNameConst::KEY_WORKPLACE => $workplace,
            KeyNameConst::KEY_GROUP_NAME => $groupName,
            KeyNameConst::KEY_GROUP_ID => $groupIds
        ];
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     */
    private function getStatisticByModulesInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $em = $this->getDoctrine();
        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            $courseName = $intervention->getDesignation();
            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                        ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                $moduleName = $module->getDesignation();
                $moduleType = $module->getStoredModule()->getDesignation();
                $duration = $module->getDuration()->format('H:i:s');
                foreach ($moduleIterations as $iteration) {
                    $learner = $iteration->getLearner();
                    // add more fields
                    $groupNames = [];
                    $learnerGroups = $em->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));

                    if ($learnerGroups) {
                        foreach ($learnerGroups as $learnerGroup) {
                            $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                        }
                    }

                    $groupName = implode(", ", $groupNames);
                    $regional = $learner->getRegional() ? $learner->getRegional()->getDesignation() : '';
                    $group = $learner->getGroupOrganization() ? $learner->getGroupOrganization()->getDesignation() : '';
                    $workplace = $learner->getWorkplace() ? $learner->getWorkplace()->getDesignation() : '';

                    $secondFormat = !empty($iteration->getLearnedTime()) ? $iteration->getLearnedTime() : 0;

                    if ($this->checkUserByConditionFilters($learner, $options)) {
                        $status = $iteration->getStatus() ? $iteration->getStatus()->getDesignation() : '';
                        $bookInfo = $this->getScoreAndBookingInfoByLearnerAndModule($module, $learner);
                        $dataRow = [
                            KeyNameConst::KEY_FIRST_NAME => $learner->getFirstName(),
                            KeyNameConst::KEY_LAST_NAME => $learner->getLastName(),
                            KeyNameConst::KEY_EMAIL => $learner->getPerson()->getEmail(),
                            KeyNameConst::KEY_POSITION_ID => $learner->getPosition(),
                            KeyNameConst::KEY_EMPLOYEE_ID => $learner->getEmployeeId(),
                            KeyNameConst::KEY_ORGANIZATION => !empty($learner->getEntity()) && !empty($learner->getEntity()->getOrganisation()) ? $learner->getEntity()->getOrganisation()->getDesignation() : null,
                            KeyNameConst::KEY_ENTITY => !empty($learner->getEntity()) ? $learner->getEntity()->getDesignation() : null,
                            KeyNameConst::KEY_REGIONAL => $regional,
                            KeyNameConst::KEY_GROUP => $group,
                            KeyNameConst::KEY_WORKPLACE => $workplace,
                            KeyNameConst::KEY_GROUP_NAME => $groupName,
                            KeyNameConst::KEY_COURSE_NAME => $courseName,
                            KeyNameConst::KEY_MODULE_NAME => $moduleName,
                            KeyNameConst::KEY_MODULE_TYPE => $moduleType,
                            KeyNameConst::KEY_STATUS => $status,
                            KeyNameConst::KEY_COMPLETION_DATE => $iteration->getExecutionDate(),
                            KeyNameConst::KEY_DURATION => $duration,
                            KeyNameConst::KEY_REAL_TIME => $this->secondsToTime($secondFormat),
                        ];
                        $data[] = array_merge($dataRow, $bookInfo);
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     */
    private function getStatisticByLearnerProgressionInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $em = $this->getDoctrine();
        $doneStatus = $em->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_DONE,
                ))->getId();
        $data = [];
        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                    ->getLearnerInterventionByEntity($intervention->getId(), $options[KeyNameConst::KEY_ENTITY_ID]);
            foreach ($learnerInterventions as $learnerIntervention) {
                $learner = $learnerIntervention->getLearner();
                if ($this->checkUserByConditionFilters($learner, $options)) {
                    // get end date course when the learner has finished the course
                    $startDateCourse = '';
                    $endDateCourse = $learnerIntervention->getProgression() == 100 ? $learnerIntervention->getUpdated() : '';
                    $groupNames = [];
                    $learnerGroups = $em->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));

                    if ($learnerGroups) {
                        foreach ($learnerGroups as $learnerGroup) {
                            $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                        }
                    }

                    $groupName = implode(", ", $groupNames);
                    $regional = $learner->getRegional() ? $learner->getRegional()->getDesignation() : '';
                    $group = $learner->getGroupOrganization() ? $learner->getGroupOrganization()->getDesignation() : '';
                    $workplace = $learner->getWorkplace() ? $learner->getWorkplace()->getDesignation() : '';

                    // calculation the total of done modules
                    $countDone = $em->getRepository('ApiBundle:ModuleIteration')
                            ->countLearnerModuleIterationByInterventionId($intervention->getId(), $learner->getId(), ['statusId' => $doneStatus, 'operation' => 'eq']);

                    // calculation the total of validated modules
                    $totalValidatedModules = 0;
                    $moduleIterations = $em->getRepository('ApiBundle:ModuleIteration')
                            ->getLearnerModuleIterationByInterventionId($intervention->getId(), $learner->getId());
                    if ($moduleIterations) {
                        foreach ($moduleIterations as $moduleIteration) {
                            if ($startDateCourse == '') {
                                $startDateCourse = $moduleIteration->getCreated();
                            }
                            $totalValidatedModules += $this->getScoreAndBookingInfoByLearnerAndModule($moduleIteration->getModule(), $learner, true);
                        }
                    }

                    // calculation the total of to finish modules
                    $totalModules = $em->getRepository('ApiBundle:Module')->countModuleByInterventionId($intervention->getId());
                    $countToFinish = $totalModules - $countDone;

                    $totalSpentTime = $em->getRepository('ApiBundle:ModuleIteration')
                            ->countTotalSpentByInterventionId($intervention->getId(), $learner->getId());

                    $numOfLogin = $em->getRepository(Activities::class)
                            ->findOneBy(array(
                        'profile' => $learner,
                        'activityName' => Activities::NUMBER_OF_LOGIN,
                    ));
                    $progressing = ($countDone / $totalModules) * 100;

                    $data[] = [
                        KeyNameConst::KEY_FIRST_NAME => $learner->getFirstName(),
                        KeyNameConst::KEY_LAST_NAME => $learner->getLastName(),
                        KeyNameConst::KEY_EMAIL => $learner->getPerson()->getEmail(),
                        KeyNameConst::KEY_POSITION_ID => $learner->getPosition(),
                        KeyNameConst::KEY_EMPLOYEE_ID => $learner->getEmployeeId(),
                        KeyNameConst::KEY_ORGANIZATION => !empty($learner->getEntity()) ? $learner->getEntity()->getOrganisation()->getDesignation() : null,
                        KeyNameConst::KEY_ENTITY => !empty($learner->getEntity()) ? $learner->getEntity()->getDesignation() : null,
                        KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                        KeyNameConst::KEY_REGIONAL => $regional,
                        KeyNameConst::KEY_GROUP => $group,
                        KeyNameConst::KEY_WORKPLACE => $workplace,
                        KeyNameConst::KEY_GROUP_NAME => $groupName,
                        KeyNameConst::KEY_REGISTRATION_DATE => $learnerIntervention->getCreated(),
                        KeyNameConst::KEY_STATUS => $learnerIntervention->getStatus()->getDesignation(),
                        KeyNameConst::KEY_NUM_COMPLETED_MODULE => $countDone,
                        KeyNameConst::KEY_NUM_VALIDATED_MODULE => $totalValidatedModules,
                        KeyNameConst::KEY_NUM_TO_FINISH_MODULE => $countToFinish,
                        KeyNameConst::KEY_PROGRESSING => is_float($progressing) ? number_format($progressing, 2, '.', ',') : $progressing,
                        KeyNameConst::KEY_TOTAL_SPENT_TIME => $this->secondsToTime($totalSpentTime),
                        KeyNameConst::KEY_START_DATE_COURSE => $startDateCourse,
                        KeyNameConst::KEY_END_DATE_COURSE => $endDateCourse,
                        KeyNameConst::KEY_LAST_LOGIN => !empty($learner->getPerson()->getLastLogin()) ? $learner->getPerson()->getLastLogin() : '',
                        KeyNameConst::KEY_NUM_LOGIN => $numOfLogin ? $numOfLogin->getActivityValue() : ($totalSpentTime > 0 ? 1 : 0),
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @param Module $module
     * @param Profile $learner
     * @return array get score, score to pass, module rating, module comment and trainer information
     */
    public function getScoreAndBookingInfoByLearnerAndModule(Module $module, Profile $learner, $onlyResult = null) {
        $moduleType = $module->getStoredModule()->getAppId();
        $score = 0;
        $scoreToPass = 0;
        $trainer = KeyNameConst::VALUE_N_A;

        if (!$onlyResult) {
            $rating = $this->getDoctrine()->getRepository(ModuleResponse::class)
                    ->findOneBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner));
            $moduleRating = $rating != null ? $rating->getNotation() : KeyNameConst::VALUE_N_A;
            $moduleComment = $rating != null ? $rating->getDesignation() : KeyNameConst::VALUE_N_A;
        }

        $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
            'learner' => $learner,
            'module' => $module,
        ));

        switch ($moduleType) {
            case StoredModule::ONLINE:
            case StoredModule::VIRTUAL_CLASS:
            case StoredModule::ONLINE_WORKSHOP:
            case StoredModule::PRESENTATION_ANIMATION:
                $booking = $this->getDoctrine()->getRepository(BookingAgenda::class)
                        ->findOneBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner));
                if ($booking != null) {
                    if (!$onlyResult) {
                        $ratingTrainer = $this->getDoctrine()->getRepository(ModuleTrainerResponse::class)
                                ->findOneBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner, KeyNameConst::KEY_TRAINER => $booking->getTrainer()));
                        $moduleTrainerRating = $ratingTrainer != null ? $ratingTrainer->getNotation() : KeyNameConst::VALUE_N_A;
                        $moduleTrainerComment = $ratingTrainer != null ? $ratingTrainer->getDesignation() : KeyNameConst::VALUE_N_A;
                    }
                    $score = $booking->getLearnerGrade() ?? 0;
                    $trainer = $booking->getTrainer()->getFirstName() . ' ' . $booking->getTrainer()->getLastName();
                }
                $scoreToPass = $module->getMinScore();
                break;
            case StoredModule::QUIZ:
                if ($module->getQuiz()) {
                    $score = !empty($module->getQuiz()->getScore_to_pass()) ? $module->getQuiz()->getScore_to_pass() : 0;
                    $scoreToPass = $module->getScoreQuiz();
                }
                break;
            default:
                // for SCORM data
                $scormData = null;
                if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId() && !empty($module->getMediaDocument()) && !empty($module->getMediaDocument()->getFileDescriptor()) && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    // Scrom synthesis
                    $scoreToPass = $module->getisNeedScoreToPass() && $module->getMinScore() ? $module->getMinScore() : null;
                    $scormData = $this->getScormSynthesis($module->getId(), $moduleIteration, $scoreToPass, $learner->getId());
                    if ($scormData) {
                        $score = $scormData['score'];
                    }
                }
                break;
        }

        if ($scoreToPass != 0 && $score != 0 && $score >= $scoreToPass) {
            $result = 1;
        } else if ($scoreToPass == 0 && $moduleIteration->getStatus() && $moduleIteration->getStatus()->getAppId() == Status::WITH_BOOKING_DONE && $moduleIteration->getLearnedTime()) {
            $result = 1;
        } else {
            $result = 0;
        }

        if (!$onlyResult) {
            return array(
                KeyNameConst::KEY_SCORE => $score,
                KeyNameConst::KEY_SCORE_TO_PASS => $scoreToPass,
                KeyNameConst::KEY_RESULT => $result,
                KeyNameConst::KEY_COMPLETION_DATE => $moduleIteration ? $moduleIteration->getExecutionDate() : null,
                KeyNameConst::KEY_TRAINER => $trainer,
                KeyNameConst::KEY_RATING => isset($moduleRating) ? $moduleRating : KeyNameConst::VALUE_N_A,
                KeyNameConst::KEY_COMMENT => isset($moduleComment) ? $moduleComment : KeyNameConst::VALUE_N_A,
                KeyNameConst::KEY_RATING_TRAINER => isset($moduleTrainerRating) ? $moduleTrainerRating : KeyNameConst::VALUE_N_A,
                KeyNameConst::KEY_COMMENT_TRAINER => isset($moduleTrainerComment) ? $moduleTrainerComment : KeyNameConst::VALUE_N_A
            );
        } else {
            return $result;
        }
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array statistic data by registered user by range time
     */
    private function getStatisticByRegisteredUsersInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $filter = ['creation_date_max' => $to, 'creation_date' => $from];
        $listLearnersRegistered = $this->getDoctrine()->getRepository(Profile::class)
                ->findLearnerByCondition(array_merge($filter, $options));
        foreach ($listLearnersRegistered as $learner) {
            $groupNames = [];
            $learnerGroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));
            if ($learnerGroups) {
                foreach ($learnerGroups as $learnerGroup) {
                    $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                }
            }
            $groupName = implode(", ", $groupNames);
            $lastLogin = $learner->getPerson()->getLastLogin();
            $regional = $learner->getRegional() ? $learner->getRegional()->getDesignation() : '';
            $group = $learner->getGroupOrganization() ? $learner->getGroupOrganization()->getDesignation() : '';
            $data[] = [
                KeyNameConst::KEY_FIRST_NAME => $learner->getFirstName(),
                KeyNameConst::KEY_LAST_NAME => $learner->getLastName(),
                KeyNameConst::KEY_EMAIL => $learner->getPerson()->getEmail(),
                KeyNameConst::KEY_EMPLOYEE_ID => $learner->getEmployeeId(),
                KeyNameConst::KEY_ORGANIZATION => !empty($learner->getEntity()) ? $learner->getEntity()->getOrganisation()->getDesignation() : null,
                KeyNameConst::KEY_ENTITY => !empty($learner->getEntity()) ? $learner->getEntity()->getDesignation() : null,
                KeyNameConst::KEY_REGIONAL => $regional,
                KeyNameConst::KEY_GROUP => $group,
                KeyNameConst::KEY_GROUP_NAME => $groupName,
                KeyNameConst::KEY_REGISTRATION_DATE => $learner->getPerson()->getCreatedAt(),
                KeyNameConst::KEY_LAST_LOGIN => $lastLogin,
                KeyNameConst::KEY_STATUS => $lastLogin ? 1 : 0,
            ];
        }

        return $data;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @param Boolean $until
     * @return mixed
     */
    public function getInterventionLists(DateTime $from, DateTime $to, array $options, $until = false) {
        $entity = null;
        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $entity = $this->getDoctrine()->getRepository(Entity::class)
                    ->findOneBy([KeyNameConst::KEY_ID => $options[KeyNameConst::KEY_ENTITY_ID]]);
        }
        if ($until) {
            return $this->getDoctrine()->getRepository(Intervention::class)
                            ->getEntityInterventionByTo($entity, $to, $options);
        } else {
            return $this->getDoctrine()->getRepository(Intervention::class)
                            ->getEntityInterventionByRangeDate($entity, $from, $to, $options);
        }
    }

    /**
     * @param Intervention $intervention
     * @param array $options
     * @return array list modules
     */
    public function getModuleListByInterventionAndConditionFilter(Intervention $intervention, array $options = []) {
        $condition = array(KeyNameConst::KEY_INTERVENTION => $intervention);
        if (key_exists(KeyNameConst::KEY_MODULE_ID, $options) && $options[KeyNameConst::KEY_MODULE_ID] != 0) {
            $condition[KeyNameConst::KEY_ID] = $options[KeyNameConst::KEY_MODULE_ID];
        }
        if (key_exists(KeyNameConst::KEY_MODULE_TYPE_ID, $options) && $options[KeyNameConst::KEY_MODULE_TYPE_ID] != 0) {
            $condition[KeyNameConst::KEY_STORED_MODULE] = $options[KeyNameConst::KEY_MODULE_TYPE_ID];
        }
        return $this->getDoctrine()->getRepository(Module::class)
                        ->findBy($condition);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array data of statistics by course information
     */
    private function getStatisticCourseInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];

        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $numNotStarted = 0;
            $numStarted = 0;
            $numFinished = 0;
            $avgSpentTime = 0;
            $numBooking = 0;
            $numWithoutBooking = 0;
            $numAssessment = 0;
            $totalTrainingGroups = 0;

            $numOfLearners = $intervention->getLearners();
            foreach ($numOfLearners as $leanerIntervention) {
                if ($leanerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $numNotStarted++;
                } elseif ($leanerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_IN_PROGRESS) {
                    $numStarted++;
                } elseif ($leanerIntervention->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_FINISHED) {
                    $numFinished++;
                }
            }

            $totalDurations = 0;
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            $totalModule = count($modules);
            $totalSize = 0;
            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $educationalDocuments = $module->getEducationalDocument();
                $trainerDocuments = $module->getTrainerDocument();
                $mediaDocument = $module->getMediaDocument();

                if ($educationalDocuments) {
                    foreach ($educationalDocuments as $educationalDocument) {
                        if ($educationalDocument) {
                            $totalSize += $educationalDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($trainerDocuments) {
                    foreach ($trainerDocuments as $trainerDocument) {
                        if ($trainerDocument) {
                            $totalSize += $trainerDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($mediaDocument && !empty($mediaDocument->getFileDescriptor())) {
                    $totalSize += $mediaDocument->getFileDescriptor()->getSize();
                }

                $moduleType = $module->getStoredModule()->getAppId();
                if (in_array($moduleType, StoredModule::MODULE_REQUIRE_BOOKING)) {
                    $numBooking++;
                } elseif ($moduleType == StoredModule::ASSESSMENT) {
                    $numAssessment++;
                } else {
                    $numWithoutBooking++;
                }
                $numDurations = $module->getDuration()->format('H:i:s');
                $totalDurations += $this->convertTimeToDecimal($numDurations);

                // calculation the avg time spent
                $realTimeSpentModule = 0;
                $numOfStatusCompleted = 0;
                $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                        ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                foreach ($moduleIterations as $iteration) {
                    $status = $iteration->getStatus() ? $iteration->getStatus()->getAppId() : null;
                    switch ($status) {
                        case Status::WITH_BOOKING_DONE:
                            $realTimeSpentModule += $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 0;
                            $numOfStatusCompleted++;
                            break;
                        default:
                            break;
                    }
                }

                $avgSpentTime += $numOfStatusCompleted > 0 ? $realTimeSpentModule / $numOfStatusCompleted : 0;
            }

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                    ->getResultsByIntervention($intervention->getId());
            $nbNotation = 0;
            $notation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                if ($learnerNotation->getNotation() > 0) {
                    ++$nbNotation;
                    $notation += $learnerNotation->getNotation();
                }
            }

            $totalNotation = $nbNotation == 0 || $notation == 0 ? 0 : round($notation / $nbNotation, 2);

            // Training Groups
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
                    ->getLearnerInterventionByEntity($intervention->getId(), $options[KeyNameConst::KEY_ENTITY_ID]);
            foreach ($learnerInterventions as $learnerIntervention) {
                $learner = $learnerIntervention->getLearner();
                if ($this->checkUserByConditionFilters($learner, $options)) {
                    $learnerGroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));
                    if ($learnerGroups) {
                        $totalTrainingGroups += count($learnerGroups);
                    }
                }
            }

            $percentProgressingOfCourse = $numFinished == 0 || count($numOfLearners) == 0 ? 0 : ($numFinished * 100 / count($numOfLearners));

            $data[] = [
                KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                KeyNameConst::KEY_ORGANIZATION => $intervention->getEntity()->getOrganisation()->getDesignation(),
                KeyNameConst::KEY_ENTITY => $intervention->getEntity()->getDesignation(),
                KeyNameConst::KEY_NUM_OF_MODULES => $totalModule,
                KeyNameConst::KEY_NUM_OF_BOOKING => $numBooking,
                KeyNameConst::KEY_NUM_OF_WITHOUT_BOOKING => $numWithoutBooking,
                KeyNameConst::KEY_NUM_OF_ASSESSMENT => $numAssessment,
                KeyNameConst::KEY_NUM_OF_LEARNERS => count($numOfLearners),
                KeyNameConst::KEY_NUM_OF_NOT_STARTED => $numNotStarted,
                KeyNameConst::KEY_NUM_OF_STARTED => $numStarted,
                KeyNameConst::KEY_NUM_OF_FINISHED => $numFinished,
                KeyNameConst::KEY_PROGRESSING => is_float($percentProgressingOfCourse) ? number_format($percentProgressingOfCourse, 2, '.', ',') : $percentProgressingOfCourse,
                KeyNameConst::KEY_NUM_OF_TRAINING_GROUPS => $totalTrainingGroups,
                KeyNameConst::KEY_CREATION_DATE => $intervention->getCreated(),
                KeyNameConst::KEY_START_DATE_COURSE => $intervention->getbeginning(),
                KeyNameConst::KEY_END_DATE_COURSE => $intervention->getending(),
                KeyNameConst::KEY_RATING => $totalNotation . '/5',
                KeyNameConst::KEY_NUM_DURATIONS => $totalDurations > 0 ? number_format($totalDurations / 60, 2, '.', ',') : '',
                KeyNameConst::KEY_TOTAL_SPENT_TIME => $avgSpentTime > 0 ? number_format($avgSpentTime / 3600, 2, '.', ',') : '',
                KeyNameConst::KEY_TOTAL_SIZE_COURSE => $this->convertFromBytes($totalSize),
            ];
        }
        return $data;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array data of statistics by course module
     */
    private function getStatisticCourseModuleInRangeTime(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $interventions = $this->getInterventionLists($from, $to, $options);
        foreach ($interventions as $intervention) {
            $modules = $this->getModuleListByInterventionAndConditionFilter($intervention, $options);
            $numOfLearners = count($intervention->getLearners());
            foreach ($modules as $module) {
                if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                    StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                    StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()
                ) {
                    continue;
                }
                $courseName = $intervention->getDesignation();
                $moduleName = $module->getDesignation();
                $moduleType = $module->getStoredModule()->getDesignation();
                $isAssessment = $moduleType == StoredModule::ASSESSMENT ? 1 : 0;
                $moduleEnd = $module->getEnding()->format('d/m/Y');
                $moduleDuration = $module->getDuration()->format('H:i:s');
                $learnerStatus = $this->getLearnerStatusByModule($module, $numOfLearners);
                $rating = $this->getRatingByModule($module);
                $scoreArr = $this->getMinScoreByModule($module);

                $dataRow = [
                    KeyNameConst::KEY_COURSE_NAME => $courseName,
                    KeyNameConst::KEY_MODULE_NAME => $moduleName,
                    KeyNameConst::KEY_MODULE_TYPE => $moduleType,
                    KeyNameConst::KEY_IS_ASSESSMENT => $isAssessment,
                    KeyNameConst::KEY_MODULE_DURATION => $moduleDuration,
                    KeyNameConst::KEY_MODULE_END => $moduleEnd,
                    KeyNameConst::KEY_MODULE_RATING => $rating . '/5'
                ];
                $data[] = array_merge($dataRow, $learnerStatus, $scoreArr);
            }
        }
        return $data;
    }

    /**
     * @param array $options
     * @return array data of statistics by skills
     */
    private function getTrainersBySkills(DateTime $from, DateTime $to, array $options = []) {
        $arrTrainer = [];
        $skillsSelected = isset($options['skillsSelected']) ? $options['skillsSelected'] : null;

        if ($skillsSelected) {
            if ($options['skillOperator'] == 1) {
                $skills = $this->getDoctrine()->getRepository('ApiBundle:Skill')
                        ->findBy(array('id' => $skillsSelected));

                foreach ($skills as $skill) {
                    $skillsName[] = $skill->getDesignation();
                }
                $skillsName = implode(", ", $skillsName);

                // calculation the total trainer
                $db = $this->getDoctrine()->getManager();
                // Write your raw SQL
                $query = "SELECT profile_id, GROUP_CONCAT(skill_id SEPARATOR ',') AS skills 
                  FROM `skill_resource_variety_profile` 
                  WHERE status = 'APPROVED'
                  GROUP BY profile_id";
                // Prepare the query from $db
                $statementDB = $db->getConnection()->prepare($query);
                // Execute both queries
                $statementDB->execute();
                $results = $statementDB->fetchAll();
                foreach ($results as $item) {
                    $all = explode(",", $item['skills']);
                    sort($all);
                    if (!array_diff($skillsSelected, $all)) {
                        $trainer = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($item['profile_id']);
                        $ranking = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($trainer);
                        $arrTrainer[] = array('ranking' => $ranking, 'trainer' => $trainer, 'skills' => $skillsName);
                    }
                }
            } else {
                $skillResourceVarietyProfiles = $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile')->findBy(array('skill' => $skillsSelected, 'status' => SkillResourceVarietyProfile::STATUS_APPROVED));
                foreach ($skillResourceVarietyProfiles as $skillResourceVarietyProfile) {
                    $trainer = $skillResourceVarietyProfile->getProfile();
                    $skill = $skillResourceVarietyProfile->getSkill()->getDesignation();
                    $ranking = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($trainer);
                    $arrTrainer[] = array('ranking' => $ranking, 'trainer' => $trainer, 'skills' => $skill);
                }
            }
        }

        return $arrTrainer;
    }

    /**
     * @param array $options
     * @return array data of statistics by skills
     */
    private function getStatisticBySkills(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $skillsName = null;
        $totalTrainers = 0;
        $totalCourses = 0;
        $totalQuizzes = 0;
        $totalIndividualRemoteSession = 0;
        $totalClassroomTraining = 0;
        $totalVirtualClassroom = 0;
        $totalExercises = 0;

        $skillsSelected = isset($options['skillsSelected']) ? $options['skillsSelected'] : null;
        if ($skillsSelected) {
            $skills = $this->getDoctrine()->getRepository('ApiBundle:Skill')
                    ->findBy(array('id' => $skillsSelected));

            foreach ($skills as $skill) {
                $skillsName[] = $skill->getDesignation();
            }
            $skillsName = implode(", ", $skillsName);
            // calculation the total trainer
            $db = $this->getDoctrine()->getManager();
            // Write your raw SQL
            $query = "SELECT profile_id, GROUP_CONCAT(skill_id SEPARATOR ',') AS skills 
                  FROM `skill_resource_variety_profile` 
                  WHERE status = 'APPROVED'
                  GROUP BY profile_id";
            // Prepare the query from $db
            $statementDB = $db->getConnection()->prepare($query);
            // Execute both queries
            $statementDB->execute();
            $results = $statementDB->fetchAll();

            foreach ($results as $item) {
                $all = explode(",", $item['skills']);
                sort($all);
                if (!array_diff($skillsSelected, $all)) {
                    $totalTrainers++;
                }
            }

            // Write your raw SQL
            $query = "SELECT module_id, GROUP_CONCAT(skill_id SEPARATOR ',') AS skills 
                  FROM `module_skills` 
                  GROUP BY module_id";
            // Prepare the query from $db
            $statementDB = $db->getConnection()->prepare($query);
            // Execute both queries
            $statementDB->execute();
            $results = $statementDB->fetchAll();
            $interventions = [];
            foreach ($results as $item) {
                $all = explode(",", $item['skills']);
                sort($all);
                if (!array_diff($skillsSelected, $all)) {
                    $module = $this->getDoctrine()->getRepository('ApiBundle:Module')
                            ->find($item['module_id']);
                    // if this intervention is not in the interventions array then plus one
                    if (!in_array($module->getIntervention()->getId(), $interventions)) {
                        $totalCourses++;
                        $interventions[] = $module->getIntervention()->getId();
                    }
                    if ($module->getStoredModule()->getAppId() == StoredModule::ONLINE) {
                        $totalIndividualRemoteSession++;
                    } else if ($module->getStoredModule()->getAppId() == StoredModule::VIRTUAL_CLASS) {
                        $totalVirtualClassroom++;
                    } else if ($module->getStoredModule()->getAppId() == StoredModule::PRESENTATION_ANIMATION) {
                        $totalClassroomTraining++;
                    }
                }
            }
            // calculation the quiz
            $query = "SELECT quiz_id, GROUP_CONCAT(skill_id SEPARATOR ',') AS skills 
                      FROM `quiz_skills` 
                      GROUP BY quiz_id";
            // Prepare the query from $db
            $statementDB = $db->getConnection()->prepare($query);
            // Execute both queries
            $statementDB->execute();
            $results = $statementDB->fetchAll();
            foreach ($results as $item) {
                $all = explode(",", $item['skills']);
                sort($all);
                if (!array_diff($skillsSelected, $all)) {
                    $quiz = $this->getDoctrine()->getRepository('ApiBundle:Quizes')
                            ->find($item['quiz_id']);

                    $module = $this->getDoctrine()->getRepository('ApiBundle:Module')
                            ->findOneBy(array('quiz' => $quiz));
                    if ($module) {
                        $totalQuizzes++;
                        // if this intervention is not in the interventions array then plus one
                        if (!in_array($module->getIntervention()->getId(), $interventions)) {
                            $totalCourses++;
                            $interventions[] = $module->getIntervention()->getId();
                        }
                    }
                }
            }

            $totalModules = $totalIndividualRemoteSession + $totalVirtualClassroom + $totalClassroomTraining + $totalQuizzes;
            $data[] = [
                'skills' => $skillsName,
                'totalTrainers' => $totalTrainers,
                'totalCourses' => $totalCourses,
                'totalModules' => $totalModules,
                'totalQuizzes' => $totalQuizzes,
                'totalIndividualRemoteSession' => $totalIndividualRemoteSession,
                'totalClassroomTraining' => $totalClassroomTraining,
                'totalVirtualClassroom' => $totalVirtualClassroom,
                'totalExercises' => $totalExercises,
            ];
        }

        return $data;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array data of statistics by course module
     */
    private function getStatisticByGlobal(DateTime $from, DateTime $to, array $options = []) {
        $data = [];
        $now = new \DateTime('now');

        $numActivePerson = 0;
        $numInactivePerson = 0;
        $numNewPerson = 0;
        $totalSpentTime = 0;
        $averageSpentTime = 0;

        $numActiveTrainer = 0;
        $numInactiveTrainer = 0;
        $numNewTrainer = 0;
        $averageTrainerGrade = 0;

        $numActiveSupervisor = 0;
        $numInactiveSupervisor = 0;
        $numNewSupervisor = 0;

        $totalCourses = 0;
        $numCourseInProgress = 0;
        $numCourseNew = 0;
        $totalCourseGrade = 0;
        $numCourseEnd = 0;
        $totalTrainingGroups = 0;
        $newTrainingGroups = 0;
        $totalSize = 0;

        $numOfIndividualClass = 0;
        $numOfVirtualClassroom = 0;
        $numOfWorkshop = 0;
        $numOfClassroom = 0;
        $numOfQuiz = 0;
        $numOfElearning = 0;
        $numOfAssessment = 0;
        $numOfIndividualClassBooked = 0;
        $numOfIndividualClassCancelled = 0;
        $numOfIndividualClassEnd = 0;

        $numOfVirtualClassroomBooked = 0;
        $numOfVirtualClassroomCancelled = 0;
        $numOfVirtualClassroomEnd = 0;

        $numOfWorkshopBooked = 0;
        $numOfWorkshopCancelled = 0;
        $numOfWorkshopEnd = 0;

        $numOfClassroomBooked = 0;
        $numOfClassroomCancelled = 0;
        $numOfClassroomEnd = 0;

        $numOfQuizBooked = 0;
        $numOfQuizCancelled = 0;
        $numOfQuizEnd = 0;

        $numOfElearningBooked = 0;
        $numOfElearningCancelled = 0;
        $numOfElearningEnd = 0;

        $numOfAssessmentBooked = 0;
        $numOfAssessmentCancelled = 0;
        $numOfAssessmentEnd = 0;

        $numOfMicroLearning = 0;
        $numOfMicroLearningEnd = 0;

        $profileType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => ProfileVariety::SUPERVISOR]);

        // Learners
        $learnerType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => ProfileVariety::LEARNER]);

        $totalLearners = $this->getDoctrine()->getRepository(Profile::class)
                ->totalProfileByRole($to, $learnerType->getId(), $options);

        $totalActivePerson = $this->getDoctrine()->getRepository(Profile::class)
                ->totalActiveProfileByRole($to, $learnerType->getId(), $options);

        $totalInActivePerson = $this->getDoctrine()->getRepository(Profile::class)
                ->totalInActiveProfileByRole($to, $learnerType->getId(), $options);

        $totalNewPerson = $this->getDoctrine()->getRepository(Profile::class)
                ->totalNewProfileByRole($from, $to, $learnerType->getId(), $options);

        // Trainers
        $trainerType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => ProfileVariety::TRAINER]);

        $totalTrainers = $this->getDoctrine()->getRepository(Profile::class)
                ->totalProfileByRole($to, $trainerType->getId(), $options);

        $totalActiveTrainers = $this->getDoctrine()->getRepository(Profile::class)
                ->totalActiveProfileByRole($to, $trainerType->getId(), $options);

        $totalInActiveTrainers = $this->getDoctrine()->getRepository(Profile::class)
                ->totalInActiveProfileByRole($to, $trainerType->getId(), $options);

        $totalNewTrainers = $this->getDoctrine()->getRepository(Profile::class)
                ->totalNewProfileByRole($from, $to, $trainerType->getId(), $options);

        // Supervisor
        $supervisorType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => ProfileVariety::SUPERVISOR]);

        $totalSupervisors = $this->getDoctrine()->getRepository(Profile::class)
                ->totalProfileByRole($to, $supervisorType->getId(), $options);

        $totalActiveSupervisors = $this->getDoctrine()->getRepository(Profile::class)
                ->totalActiveProfileByRole($to, $supervisorType->getId(), $options);

        $totalInActiveSupervisors = $this->getDoctrine()->getRepository(Profile::class)
                ->totalInActiveProfileByRole($to, $supervisorType->getId(), $options);

        $totalNewSupervisors = $this->getDoctrine()->getRepository(Profile::class)
                ->totalNewProfileByRole($from, $to, $supervisorType->getId(), $options);


        // Training Groups
        $totalTrainingGroups = $this->getDoctrine()->getRepository(Group::class)
                ->countTotalGroupByEntity($to, $options);

        $newTrainingGroups = $this->getDoctrine()->getRepository(Group::class)
                ->countNewGroup($from, $to, $options);

        $interventions = $this->getInterventionLists($from, $to, $options);
        $numCourseNew = count($interventions);

        $interventionUntilTo = $this->getInterventionLists($from, $to, $options, true);
        foreach ($interventionUntilTo as $intervention) {
            // Modules
            $modules = $this->getDoctrine()->getRepository(Module::class)
                    ->findBy(array(KeyNameConst::KEY_INTERVENTION => $intervention));
            foreach ($modules as $module) {
                $moduleType = $module->getStoredModule()->getAppId();
                switch ($moduleType) {
                    case StoredModule::ONLINE:
                        $numOfIndividualClass++;
                        $numOfIndividualClassBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfIndividualClassCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfIndividualClassEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::VIRTUAL_CLASS:
                        $numOfVirtualClassroom++;
                        $numOfVirtualClassroomBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfVirtualClassroomCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfVirtualClassroomEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ONLINE_WORKSHOP:
                        $numOfWorkshop++;
                        $numOfWorkshopBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfWorkshopCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfWorkshopEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::PRESENTATION_ANIMATION:
                        $numOfClassroom++;
                        $numOfClassroomBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfClassroomCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfClassroomEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                                ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::QUIZ:
                        $numOfQuiz++;
                        $numOfQuizEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                                ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ELEARNING:
                        $numOfElearning++;
                        $numOfElearningEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                                ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ASSESSMENT:
                        $numOfAssessment++;
                        $numOfAssessmentEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                                ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::EVALUATION:
                        $numOfMicroLearning++;
                        $numOfMicroLearningEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                                ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    default:
                        break;
                }
                // Stokage information
                $educationalDocuments = $module->getEducationalDocument();
                $trainerDocuments = $module->getTrainerDocument();
                $mediaDocument = $module->getMediaDocument();

                if ($educationalDocuments) {
                    foreach ($educationalDocuments as $educationalDocument) {
                        if ($educationalDocument) {
                            $totalSize += $educationalDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($trainerDocuments) {
                    foreach ($trainerDocuments as $trainerDocument) {
                        if ($trainerDocument) {
                            $totalSize += $trainerDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($mediaDocument && !empty($mediaDocument->getFileDescriptor())) {
                    $totalSize += $mediaDocument->getFileDescriptor()->getSize();
                }
            }
            /* // Training Groups
              $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
              ->getLearnerInterventionByEntity($intervention->getId(), $options[KeyNameConst::KEY_ENTITY_ID]);
              foreach ($learnerInterventions as $learnerIntervention) {
              $learner = $learnerIntervention->getLearner();
              if ($this->checkUserByConditionFilters($learner, $options)) {
              $learnerGroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array(KeyNameConst::KEY_LEARNER => $learner->getId()));
              if ($learnerGroups) {
              $totalTrainingGroups += count($learnerGroups);
              }
              }
              } */
            // Courses
            $totalCourses ++;
            $numCourseInProgress += ($intervention->getBeginning() <= $now && $intervention->getEnding() >= $now) ? 1 : 0;
            $numCourseEnd += ($now > $intervention->getEnding()) ? 1 : 0;

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                    ->getResultsByIntervention($intervention->getId());

            $nbNotation = 0;
            $notation = 0;
            $totalNotation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                ++$nbNotation;
                $notation += $learnerNotation->getNotation();
            }

            if ($nbNotation) {
                $totalNotation = $notation / $nbNotation;
            }

            $intervention->setLearnersNotation($totalNotation);

            $totalCourseGrade += $intervention->getlearnersNotation();

            // Learners
            $numActivePerson += $this->getDoctrine()->getRepository(LearnerIntervention::class)
                    ->countLearnerByStatus($intervention->getId(), 1);

            /* $numInactivePerson += $this->getDoctrine()->getRepository(LearnerIntervention::class)
              ->countLearnerByStatus($intervention->getId(), 0);
              $numNewPerson = $this->getDoctrine()->getRepository(LearnerIntervention::class)
              ->countLearnerByStatus($intervention->getId(), 1, 1); */

            $totalSpentTime += $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->countTotalSpentTimeByInterventionId($intervention->getId());
            $averageSpentTime = $numActivePerson > 0 ? $totalSpentTime / $numActivePerson : 0;

            // Trainers
            /* $numActiveTrainer += $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
              ->countTrainersByInterventionId($intervention->getId(), 1);

              $numInactiveTrainer += $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
              ->countTrainersByInterventionId($intervention->getId(), 0);

              $numNewTrainer += $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
              ->countTrainersByInterventionId($intervention->getId(), 1, 1); */

            // calculation the average trainer grade for each intervention
            $assigmentResources = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                    ->getTrainersByInterventionId($intervention->getId());
            if ($assigmentResources) {
                $grade = 0;
                foreach ($assigmentResources as $assigmentResource) {
                    $trainer = $assigmentResource->getLiveResource();
                    if ($trainer) {
                        $grade += $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
                                ->getRankingByTrainer($trainer);
                    }
                }
                $averageTrainerGrade += $grade > 0 ? $grade / count($assigmentResources) : 0;
            }

            // Supervisors
            $numActiveSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 1);

            $numInactiveSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 0);

            $numNewSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 1, 1);
        }
        //$totalLearners = $numActivePerson + $numInactivePerson + $numNewPerson;
        //$totalTrainers = $numActiveTrainer + $numInactiveTrainer + $numNewTrainer;
        $averageTrainerGrade = $averageTrainerGrade > 0 ? $averageTrainerGrade / $totalTrainers : 0;
        //$totalSupervisors = $numActiveSupervisor + $numInactiveSupervisor + $numNewSupervisor;
        $data[] = [
            'learners' => [
                'total' => $totalLearners,
                'active' => $totalActivePerson,
                'inactive' => $totalInActivePerson,
                'new' => $totalNewPerson,
                'totalSpentTime' => number_format($totalSpentTime / 3600, 2, '.', ','),
                'averageSpentTime' => number_format($averageSpentTime / 3600, 2, '.', ','),
            ],
            'trainers' => [
                'total' => $totalTrainers,
                'active' => $totalActiveTrainers,
                'inactive' => $totalInActiveTrainers,
                'new' => $totalNewTrainers,
                'averageTrainerGrade' => $averageTrainerGrade,
            ],
            'supervisors' => [
                'total' => $totalSupervisors,
                'active' => $totalActiveSupervisors,
                'inactive' => $totalInActiveSupervisors,
                'new' => $totalNewSupervisors,
            ],
            'courses' => [
                'total' => $totalCourses,
                'started' => $numCourseInProgress,
                'ended' => $numCourseEnd,
                'new' => $numCourseNew,
                'averageGrade' => $totalCourses > 0 ? number_format($totalCourseGrade / $totalCourses, 2, '.', ',') : 0,
            ],
            'groups' => [
                'total' => $totalTrainingGroups,
                'new' => $newTrainingGroups,
            ],
            'stokage' => [
                'total' => '50 GB',
                'used' => $this->convertFromBytes($totalSize),
                'remaining' => $this->convertFromBytes(53687091200 - $totalSize),
            ],
            'modules' => [
                'eLearning' => ['total' => $numOfElearning, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfElearningEnd],
                'quiz' => ['total' => $numOfQuiz, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfQuizEnd],
                'microLearning' => ['total' => $numOfMicroLearning, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfMicroLearningEnd],
                'assignment' => ['total' => $numOfAssessment, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfAssessmentEnd],
                'individual' => ['total' => $numOfIndividualClass, 'booked' => $numOfIndividualClassBooked, 'cancelled' => $numOfIndividualClassCancelled, 'ended' => $numOfIndividualClassEnd],
                'virtual' => ['total' => $numOfVirtualClassroom, 'booked' => $numOfVirtualClassroomBooked, 'cancelled' => $numOfVirtualClassroomCancelled, 'ended' => $numOfVirtualClassroomEnd],
                'workshop' => ['total' => $numOfWorkshop, 'booked' => $numOfWorkshopBooked, 'cancelled' => $numOfWorkshopCancelled, 'ended' => $numOfWorkshopEnd],
                'presentation' => ['total' => $numOfClassroom, 'booked' => $numOfClassroomBooked, 'cancelled' => $numOfClassroomCancelled, 'ended' => $numOfClassroomEnd],
            ],
        ];
        return $data;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function updateStatisticsAction(Request $request) {
        $task = $request->get('task');

        switch ($task) {
            case 'getStatistics':
                $from = $request->get('from') ? (new \DateTime())->setTimestamp($request->get('from')) : (new \DateTime())->setTimestamp(0);
                $to = $request->get('to') ? (new \DateTime())->setTimestamp($request->get('to')) : new \DateTime();
                $conditions = [];
                $timeConditions = [];

                $timeConditions[] = [
                    'operate' => '>=',
                    'value' => $from->format('Y-m-d') . ' 00:00:00'
                ];

                $timeConditions[] = [
                    'operate' => '<=',
                    'value' => $to->format('Y-m-d') . ' 23:59:59'
                ];

                if (!empty($timeConditions)) {
                    $conditions = [
                        'person' => [KeyNameConst::KEY_LAST_LOGIN => $timeConditions]
                    ];
                }

                return $this->json([
                            'supervisors' => $this->get(BaseStatistics::class)->getSupervisors($conditions),
                            'learners' => $this->get(BaseStatistics::class)->getLearners($conditions),
                            'trainers' => $this->get(BaseStatistics::class)->getTrainers($conditions),
                ]);
            case 'getNumberInformation':
                $from = $request->get('from') ? (new \DateTime($request->get('from'))) : (new \DateTime('first day of this month'));
                $to = $request->get('to') ? (new \DateTime($request->get('to'))) : new \DateTime('last day of this month');
                $courseInProgress = 0;
                $numOfIndividualClass = 0;
                $numOfVirtualClassroom = 0;
                $numOfWorkshop = 0;
                $numOfClassroom = 0;
                $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')
                        ->getEntityInterventionByRangeDate($this->getEntityFromProfile(), $from, $to);
                foreach ($interventions as $intervention) {
                    $modules = $this->getDoctrine()->getRepository(Module::class)
                            ->findBy(array(KeyNameConst::KEY_INTERVENTION => $intervention));
                    $courseInProgress += $this->getLearnerStatusByCourse($intervention);
                    foreach ($modules as $module) {
                        $moduleType = $module->getStoredModule()->getAppId();
                        switch ($moduleType) {
                            case StoredModule::ONLINE:
                                $numOfIndividualClass++;
                                break;
                            case StoredModule::VIRTUAL_CLASS:
                                $numOfVirtualClassroom++;
                                break;
                            case StoredModule::ONLINE_WORKSHOP:
                                $numOfWorkshop++;
                                break;
                            case StoredModule::PRESENTATION_ANIMATION:
                                $numOfClassroom++;
                                break;
                            default:
                                break;
                        }
                    }
                }
                return $this->json([
                            'courseInProgress' => $courseInProgress,
                            'numOfIndividualClass' => $numOfIndividualClass,
                            'numOfVirtualClassroom' => $numOfVirtualClassroom,
                            'numOfWorkshop' => $numOfWorkshop,
                            'numOfClassroom' => $numOfClassroom
                ]);
        }
    }

    /**
     * @param Module $module
     * @param integer $numOfLearners
     * @return array learner status and percent progressing on that module
     */
    public function getLearnerStatusByModule(Module $module, $numOfLearners) {
        $realTimeSpent = 0;
        $numberIteration = 0;
        $numOfProgressing = 0;
        $numOfStatusBooked = 0;
        $numOfStatusNotStarted = 0;
        $numOfStatusStarted = 0;
        $numOfStatusCompleted = 0;
        $totalScoreOfStatusCompleted = 0;
        $totalScoreOfStatusPassed = 0;
        $data = null;

        $moduleTypeBooking = StoredModule::MODULE_REQUIRE_BOOKING;
        $moduleType = $module->getStoredModule()->getAppId();
        $moduleIterations = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findBy(array(KeyNameConst::KEY_MODULE => $module));
        foreach ($moduleIterations as $iteration) {
            $status = $iteration->getStatus() ? $iteration->getStatus()->getAppId() : null;
            switch ($status) {
                case Status::LEARNER_INTERVENTION_IN_PROGRESS:
                    $numOfStatusStarted++;
                    break;
                case Status::WITH_BOOKING_DONE:
                    $data = $this->getScoreAndBookingInfoByLearnerAndModule($module, $iteration->getLearner());
                    $totalScoreOfStatusCompleted += $data[KeyNameConst::KEY_SCORE];
                    if ($data[KeyNameConst::KEY_RESULT]) {
                        $totalScoreOfStatusPassed ++;
                    }
                    $realTimeSpent += $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 0;
                    $numOfStatusCompleted++;
                    break;
                case Status::WITH_BOOKING_BOOKED:
                    $numOfStatusBooked++;
                    break;
                default:
                    break;
            }
            $numberIteration++;
        }
        $percentProgressingOfModule = $numOfStatusCompleted == 0 || $numOfLearners == 0 ? 0 : ($numOfStatusCompleted * 100 / $numOfLearners);
        $scoreOfStatusCompleted = ($numOfStatusCompleted > 0 ? ($totalScoreOfStatusCompleted / $numOfStatusCompleted) : 0);
        $scoreOfStatusPassed = ($numOfStatusCompleted > 0 ? $totalScoreOfStatusPassed * 100 / $numOfStatusCompleted : 0);
        return array(KeyNameConst::KEY_REAL_TIME => $this->secondsToTime($numOfStatusCompleted > 0 ? $realTimeSpent / $numOfStatusCompleted : 0),
            KeyNameConst::KEY_PROGRESSING => is_float($percentProgressingOfModule) ? number_format($percentProgressingOfModule, 2, '.', ',') : $percentProgressingOfModule,
            KeyNameConst::KEY_BOOKED => in_array($moduleType, $moduleTypeBooking) ? $numOfStatusBooked : 'N/A',
            KeyNameConst::KEY_NOT_STARTED => $numOfLearners - ($numOfStatusCompleted + $numOfStatusStarted),
            KeyNameConst::KEY_COMPLETED => $numOfStatusCompleted,
            KeyNameConst::KEY_STARTED => $numOfStatusStarted,
            KeyNameConst::KEY_AVERAGE_SCORE => (is_float($scoreOfStatusCompleted) ? number_format($scoreOfStatusCompleted, 2, '.', ',') : $scoreOfStatusCompleted) . $this->percentStringCharacter,
            KeyNameConst::KEY_STATUS_PASSED => (is_float($scoreOfStatusPassed) ? number_format($scoreOfStatusPassed, 2, '.', ',') : $scoreOfStatusPassed) . $this->percentStringCharacter,
        );
    }

    function secondsToTime($seconds) {
        if ($seconds <= 0)
            return '';

        $ret = "";

        /*         * * get the days ** */
        $days = intval(intval($seconds) / (3600 * 24));
        if ($days > 0) {
            $ret .= $days > 10 ? "$days:" : "0$days:";
        }

        /*         * * get the hours ** */
        $hours = (intval($seconds) / 3600) % 24;
        if ($hours > 0) {
            $ret .= $hours > 9 ? "$hours:" : "0$hours:";
        } else {
            $ret .= "00:";
        }

        /*         * * get the minutes ** */
        $minutes = (intval($seconds) / 60) % 60;
        if ($minutes > 0) {
            $ret .= $minutes > 9 ? "$minutes:" : "0$minutes:";
        } else {
            $ret .= "00:";
        }

        /*         * * get the seconds ** */
        $seconds = intval($seconds) % 60;
        if ($seconds > 0) {
            $ret .= $seconds > 9 ? "$seconds" : "0$seconds";
        } else {
            $ret .= "00";
        }

        return $ret;
    }

    /**
     * @param Module $module
     * @return number total learner rating per 5
     */
    private function getRatingByModule(Module $module) {
        $rating = 0;
        $total = 0;
        $moduleResponse = $this->getDoctrine()->getRepository('ApiBundle:ModuleResponse')
                ->findBy(array(KeyNameConst::KEY_MODULE => $module));
        foreach ($moduleResponse as $moduleRating) {
            if ($moduleRating->getNotation() > 0) {
                $total++;
                $rating += $moduleRating->getNotation();
            }
        }

        return $total == 0 || $rating == 0 ? 0 : round($rating / $total, 2);
    }

    /**
     * @param Intervention $course
     * @return boolean return status of the course
     */
    private function getLearnerStatusByCourse(Intervention $course) {
        $learnerIntervention = $this->getDoctrine()->getRepository(LearnerIntervention::class)
                ->countStatusInProgressByCourse($course);
        return $learnerIntervention > 0 ? 1 : 0;
    }

    /**
     * @param Module $module
     * @return array score of leaners
     */
    private function getScoreByModule(Module $module) {
        $moduleType = $module->getStoredModule()->getAppId();
        $score = 0;
        $total = 0;
        $scoreToPass = 0;
        switch ($moduleType) {
            case StoredModule::ONLINE:
            case StoredModule::VIRTUAL_CLASS:
            case StoredModule::ONLINE_WORKSHOP:
            case StoredModule::PRESENTATION_ANIMATION:
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                        ->findBy(array(KeyNameConst::KEY_MODULE => $module));
                foreach ($bookingAgendas as $booking) {
                    $score += $booking->getLearnerGrade() ?? 0;
                    $total++;
                }
                $scoreToPass = $module->getMinScore();
                break;
            case StoredModule::QUIZ:
                if ($module->getQuiz()) {
                    if (KeyNameConst::KEY_NATIVE === $module->getQuiz()->getType()) {
                        $quizResult = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                                ->findBy([
                            KeyNameConst::KEY_MODULE => $module,
                        ]);
                        foreach ($quizResult as $result) {
                            $score += $result ? round(($result->getScore() / $module->getQuiz()->getTotalPoints()) * 100, 2) : 0;
                            $total++;
                        }
                    }
                    $scoreToPass = $module->getScoreQuiz();
                }
                break;
            default:
                break;
        }

        return array(KeyNameConst::KEY_SCORE => ($score == 0 || $total == 0 ? 0 : round($score / $total, 2)) . $this->percentStringCharacter,
            KeyNameConst::KEY_SCORE_TO_PASS => ($scoreToPass ? $scoreToPass : '0') . $this->percentStringCharacter);
    }

    /**
     * @param Module $module
     * @return array score to passed of module
     */
    public function getMinScoreByModule(Module $module) {
        $moduleType = $module->getStoredModule()->getAppId();
        $scoreToPass = 0;
        switch ($moduleType) {
            case StoredModule::ONLINE:
            case StoredModule::VIRTUAL_CLASS:
            case StoredModule::ONLINE_WORKSHOP:
            case StoredModule::PRESENTATION_ANIMATION:
                $scoreToPass = $module->getMinScore();
                break;
            case StoredModule::QUIZ:
                if ($module->getQuiz()) {
                    $scoreToPass = $module->getScoreQuiz();
                }
                break;
            default:
                // for SCORM data
                if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId() && !empty($module->getMediaDocument()) && !empty($module->getMediaDocument()->getFileDescriptor()) && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    $scoreToPass = $module->getisNeedScoreToPass() && $module->getMinScore() ? $module->getMinScore() : null;
                }
                break;
        }

        return array(KeyNameConst::KEY_SCORE_TO_PASS => ($scoreToPass ? $scoreToPass : '0') . $this->percentStringCharacter);
    }

    /**
     * Convert decimal time into time in the format hh:mm:ss
     *
     * @param integer The time as a decimal value.
     *
     * @return string $time The converted time value.
     */
    function convertDecimalToTime($decimal) {
        $hours = floor($decimal / 60);
        $minutes = floor($decimal % 60);
        $seconds = $decimal - (int) $decimal;
        $seconds = round($seconds * 60);

        return str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":" . str_pad($seconds, 2, "0", STR_PAD_LEFT);
    }

    /**
     * Convert time into decimal time (minute).
     *
     * @param string $time The time in the format hh:mm:ss to convert
     *
     * @return integer The time as a decimal value.
     */
    function convertTimeToDecimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60);

        return $decTime;
    }

    function convertFromBytes($bytes) {
        $bytes /= 1024;
        if ($bytes >= 1024 * 1024) {
            $bytes /= 1024;
            return number_format($bytes / 1024, 1) . ' GB';
        } elseif ($bytes >= 1024 && $bytes < 1024 * 1024) {
            return number_format($bytes / 1024, 1) . ' MB';
        } else {
            return number_format($bytes, 1) . ' KB';
        }
    }

}
