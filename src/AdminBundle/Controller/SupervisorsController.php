<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileDocument;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\ScormSynthesis;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\WorkingHours;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Entity\Person;
use AppBundle\Service\FileApi;
use Controller\AdminController;
use DateTime;
use ImportBundle\Entity\ImportUser;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Email;

/**
 * Class SupervisorsController
 * @package AdminBundle\Controller
 */
class SupervisorsController extends AdminController
{
    /**
     * @var array
     */
    protected $headers = [
        'table.interventions.entity' => [
            'field' => [
                'entity' => 'designation'
            ]
        ],
        'table.asked_by'             => [
            'field' => 'displayName'
        ],
        'table.function'             => [
            'field' => 'position'
        ],
        'table.email'                => [
            'field' => 'otherEmail'
        ],
        'table.phones'               => [
            'field' => 'fixedPhone'
        ],
        'table.trainers.state'       => [
            'field' => [
                'person' => 'enabled'
            ]
        ]];

    /**
     * @var array
     */
    protected $actions = [
        'admin_supervisors_show'   => [
            'icon' => 'fal fa-eye'
        ],
        'admin_supervisors_edit'   => [
            'icon' => 'fal fa-pen'
        ],
        'admin_supervisors_delete' => [
            'icon'    => 'fal fa-trash',
            'confirm' => true
        ]
    ];

    /**
     * @return Profile[]|array|object[]
     */
    protected function getData()
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Person $user
         */
        $user         = $this->getUser();
        $profileType  = $em->getRepository('ApiBundle:ProfileVariety')
            ->findOneBy(['appId' => ProfileVariety::SUPERVISOR]);
        $conditionArr = ['profileType' => $profileType];

        if (!$user->isRole('ROLE_SUPER_ADMIN')) {
            $conditionArr['entity'] = $em->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $user->getId()])->getEntity()->getId();
        }

        return $em->getRepository('ApiBundle:Profile')->findBy(
            $conditionArr
        );
    }

    /**
     *
     */
    protected function loadNavItems()
    {
        $this
            ->addNavigatorItem($this->get('translator')->trans('admin.supervisor.nav.new'), 'admin_supervisors_new', 'create-link', 'fal fa-plus')
            ->addNavigatorItem($this->get('translator')->trans('admin.supervisor.nav.upload'), 'admin_supervisors_upload', null, 'fal fa-file-upload');
    }

    /**
     * Creates a new profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form    = $this->createForm('ApiBundle\Form\ProfileSupervisorType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailProfile = $profile->getPerson()->getEmail();
            $em           = $this->getDoctrine()->getManager();
            $profileType  = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::SUPERVISOR,
                )
            );

            $profile->setEntity($form->getNormData()->getEntity());
            $profile->setProfileType($profileType);
            $profile->getPerson()->setRoles(array('ROLE_SUPERVISOR'));
            $profile->getPerson()->setUsername($emailProfile);
            $profile->getPerson()->setEnabled(true);
            $profile->setCreationDate(new DateTime());
            $profile->setOtherEmail($emailProfile);
            $profile->setUpdateDate(new DateTime());
            $profile->setBillingCode('');
            $profile->setStreet('');

            // Setup reset token
            $token        = $this->container->get('fos_user.util.token_generator');
            $personEntity = $profile->getPerson();
            $personEntity->setPasswordRequestedAt(new \DateTime());
            $personEntity->setConfirmationToken($token->generateToken());
            $em->persist($personEntity);
            $em->persist($profile);
            $em->flush();
            $this->get(Email::class)->sendMailNewUserRegistration($profile);
            return $this->redirectToRoute('admin_supervisors_show', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:supervisors:formSupervisors.html.twig', array(
            'profile' => $profile,
            'form'    => $form->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Upload profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function uploadAction(Request $request)
    {
        $profile = $this->getCurrentProfile();

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file           = $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            $fileDescriptor->getName();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);
            $document->setProfile($profile);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_supervisors_index'));
        }

        return $this->render('AdminBundle:supervisors:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     * @param Profile $profile
     * @return Response
     */
    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('AdminBundle:supervisors:show.html.twig', array(
            'profile'     => $profile,
            'delete_form' => $deleteForm->createView(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     * @param Request $request
     * @param Profile $profile
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm   = $this->createForm('ApiBundle\Form\ProfileSupervisorEditType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $profile->setUpdateDate(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('admin_supervisors_edit', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:supervisors:formSupervisors.html.twig', array(
            'profile'     => $profile,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit' => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param $id
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $isDelete   = $request->request->get('isDelete');

        $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
        $person     = $profile->getPerson();

        if ($isDelete) {
            $em->remove($profile);
            $em->remove($profile->getPerson());
            $em->flush();
        } else {
            $person->setEnabled(0);
            $em->persist($person);
            $em->flush();
        }

        return $this->redirectToRoute('admin_supervisors_index');
    }

    public function deleteSupervisorsAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $trainerIds = $request->request->get('selectedProfiles');

        foreach ($trainerIds as $id){
            $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
            $person     = $profile->getPerson();
            if ($type == 1) {
                $this->deleteSupervisor($person);
            } else {
                $person->setEnabled(0);
                $em->persist($person);
                $em->flush();
            }
        }
        return $this->redirectToRoute('admin_supervisors_index');
    }

    /**
     * Delete a learner.
     *
     * @param Profile $profile The profile entity
     *
     * @return Boolean
     */
    private function deleteSupervisor(Person $user)
    {
        $em      = $this->getDoctrine()->getManager();
        $profile               = $em->getRepository(Profile::class)->findOneBy(['person' => $user]);
        $learnerInterventions  = $em->getRepository(LearnerIntervention::class)->findBy(['learner' => $profile]);
        $workHours             = $em->getRepository(WorkingHours::class)->findBy(['profile' => $profile]);
        $calendars             = $em->getRepository(AvailableCalendar::class)->findBy(['profile' => $profile]);
        $learnerGroups         = $em->getRepository(LearnerGroup::class)->findBy(['learner' => $profile]);
        $learnerVerieties      = $em->getRepository(SkillResourceVarietyProfile::class)->findBy(['profile' => $profile]);
        $learnerDocuments      = $em->getRepository(ProfileDocument::class)->findBy(['profile' => $profile]);
        $learnerScormSynthesis = $em->getRepository(ScormSynthesis::class)->findBy(['profile' => $profile]);
        $learnerActivities     = $em->getRepository(Activities::class)->findBy(['profile' => $profile]);
        $learnerRespones       = $em->getRepository(ModuleResponse::class)->findBy(['learner' => $profile]);
        $moduleIterations      = $em->getRepository(ModuleIteration::class)->findBy(['learner' => $profile]);
        $bookingAgendas         = $em->getRepository(BookingAgenda::class)->findBy(['learner' => $profile]);

        foreach ($learnerInterventions as $learnerIntervention) {
            if ($learnerIntervention) {
                $em->remove($learnerIntervention);
            }
        }

        foreach ($workHours as $workHour) {
            if ($workHour) {
                $em->remove($workHour);
            }
        }

        foreach ($calendars as $calendar) {
            if ($calendar) {
                $em->remove($calendar);
            }
        }

        foreach ($bookingAgendas as $bookingAgenda) {
            if ($bookingAgenda) {
                $em->remove($bookingAgenda);
            }
        }

        foreach ($learnerGroups as $learnerGroup) {
            if ($learnerGroup) {
                $em->remove($learnerGroup);
            }
        }

        foreach ($learnerVerieties as $learnerVeriety) {
            if ($learnerVeriety) {
                $em->remove($learnerVeriety);
            }
        }

        foreach ($learnerDocuments as $learnerDocument) {
            if ($learnerDocument) {
                $em->remove($learnerDocument);
            }
        }

        foreach ($learnerScormSynthesis as $learnerScormSynthesi) {
            if ($learnerScormSynthesi) {
                $em->remove($learnerScormSynthesi);
            }
        }

        foreach ($learnerActivities as $learnerActivity) {
            if ($learnerActivity) {
                $em->remove($learnerActivity);
            }
        }

        foreach ($learnerRespones as $learnerRespone) {
            if ($learnerRespone) {
                $em->remove($learnerRespone);
            }
        }

        foreach ($moduleIterations as $moduleIteration) {
            if ($moduleIteration) {
                $em->remove($moduleIteration);
            }
        }

        $em->remove($user);
        $em->remove($profile);
        $em->flush();
    }

    /**
     * Creates a form to delete a profile entity.
     * @param Profile $profile
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_supervisors_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param $role
     * @return Response
     */
    public function showImportAction($role)
    {
        $importUsers = $this->getDoctrine()->getManager()->getRepository(ImportUser::class)->findBy([
            'importType' => $role,
        ]);

        return $this->render('AdminBundle:import:import.html.twig', ['importedUsers' => $importUsers]);
    }
}
