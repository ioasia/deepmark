<?php

namespace AdminBundle\Helper\Traits;

/**
 * Trait HelperTrait
 * @package AdminBundle\Helper\Traits
 */
trait HelperTrait
{
    abstract protected function getCurrentUser();

    /**
     * @return mixed
     */
    public function getEntityFromProfile()
    {
        $profile = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $this->getCurrentUser()->getId(),
        ));
        return $profile->getEntity();
    }
}
