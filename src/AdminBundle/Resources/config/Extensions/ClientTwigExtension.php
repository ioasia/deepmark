<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 13.02.19
 * Time: 10:53.
 */

namespace AdminBundle\Resources\config\Extensions;

use AdminBundle\Services\EntityManagerService;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;

class ClientTwigExtension extends \Twig_Extension
{
    protected $doctrine;

    public function __construct(EntityManagerService $entityManager)
    {
        $this->doctrine = $entityManager->getDoctrine();
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('findClients', array($this, 'findClients')),
        ];
    }

    public function findClients()
    {
        $clients = $this->doctrine->getRepository(Profile::class)->findBy(array(
            'profileType' => $this->doctrine->getRepository('ApiBundle:ProfileVariety')->findOneBy(array(
                'appId' => ProfileVariety::ADMIN,
            )),
        ));

        return $clients;
    }
}
