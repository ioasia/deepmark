<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 23.01.19
 * Time: 13:32.
 */

namespace AdminBundle\Exception;

class FileException extends InterventionBaseException
{
}
