<?php

namespace AdminBundle\Exception;

class IncorrectArgumentException extends InterventionBaseException
{
}
