<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 04.03.19
 * Time: 15:25.
 */

namespace AdminBundle\Utils\FileUplaod;

class FileConstantMap
{
    public const LEARNER = 'learner';
    public const TRAINER = 'trainer';
    public const E_LEARNING = 'eLearning';
    public const TRAINER_REPORT_TEMPLATE = 'trainerReportTemplate';
    public const ATTENDANCE_SHEET_TEMPLATE = 'attendanceSheetTemplate';
    public const CERTIFICATE_TEMPLATE = 'certificateTemplate';
}
