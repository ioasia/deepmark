<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 05.02.19
 * Time: 10:31.
 */

namespace AdminBundle\Utils\FileUplaod;

use AdminBundle\Exception\FileException;
use AdminBundle\Utils\Contracts\InterventionUpload;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class UploadFile extends Filesystem
{
    protected $request;

    public const UPLOADER_ID = 'temporary';

    protected $dirToUpload;

    protected $filePath = '';

    protected $fileName = '';

    protected $personRole = '';

    public function __construct(InterventionUpload $uploader)
    {
        $this->request     = $uploader->getRequest();
        $this->dirToUpload = $uploader->getUploadFolder(self::UPLOADER_ID);
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getPersonRole()
    {
        return $this->personRole;
    }

    /**
     *
     */
    public function saveFile(): void
    {
        foreach ($this->request->files as $file) {
            if (!is_array($file)) {
                continue;
            }

            foreach ($file as $folderName => $fileArray) {
                foreach ($fileArray['file'] as $personRole => $fileObject) {
                    $filePath = $this->dirToUpload . '/' . $folderName . '/' . $personRole . '/' . $fileObject->getClientOriginalName();

                    $fileObject->move(
                        $this->dirToUpload . '/' . $folderName . '/' . $personRole,
                        $fileObject->getClientOriginalName()
                    );

                    $this->filePath   = $filePath;
                    $this->fileName   = $fileObject->getClientOriginalName();
                    $this->personRole = $personRole;
                }
            }
        }
    }

    public function getFiles($moduleId, $personFolder)
    {
        $isModulefolder = $this->exists($this->dirToUpload . '/' . $moduleId);
        if (!$isModulefolder) {
            throw new FileException('No Folder Found');
        }

        $finalDir     = $this->dirToUpload . '/' . $moduleId . '/' . $personFolder;
        $isRoleFolder = $this->exists($finalDir);

        if (!$isRoleFolder) {
            throw new FileException('No Folder Found');
        }

        $finder = new Finder();
        $dir    = $finder->in($finalDir);

        if (!$dir->files()->hasResults()) {
            throw new FileException('No Folder Found');
        }

        $files = [];
        foreach ($dir->files() as $file) {
            $files[] = [
                'name'      => $file->getFileName(),
                'path'      => '/' . $moduleId . '/' . $personFolder . '/' . $file->getFileName(),
                'pesonRole' => $personFolder,
            ];
        }

        return $files;
    }

    public function deleteFile()
    {
        $requestParams = $this->request->attributes->all()['_route_params'];

        $finalDir = $this->dirToUpload . '/' . $requestParams['folderModule'] . '/' . $requestParams['personRole'];
        $finder   = new Finder();
        $finder->name($requestParams['fileName']);
        $dir = $finder->in($finalDir);

        if (empty($dir)) {
            throw new FileException('No file found');
        }

        foreach ($dir->files() as $file) {
            $this->remove($file->getPathName());
        }

        return true;
    }
}
