<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 16.02.19
 * Time: 14:49.
 */

namespace AdminBundle\Utils\FileUplaod;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Utils\Contracts\InterventionUpload;
use Symfony\Component\Filesystem\Filesystem;

class LogoUpload extends Filesystem
{
    private $request;

    private const UPLOADER_ID = 'logo';

    private $dirToUpload;

    private $filePath = '';

    private $fileName = '';

    private $fileExtensionNotValid = [
    ];

    private $personRole = '';

    public function __construct(InterventionUpload $uploader)
    {
        $this->request = $uploader->getRequest();
        $this->dirToUpload = $uploader->getUploadFolder(self::UPLOADER_ID);
    }

    public function saveFile(): void
    {
        $uploadMaxSize = (int) ini_get("upload_max_filesize") * 1024 * 1024;
        if(!$this->request->files->all()) {
            throw new IncorrectArgumentException('File size exceeds the maximum limit of '.$uploadMaxSize.' MB');
        }
        foreach ($this->request->files as $file) {
            if (is_array($file)) {
                foreach ($file as $folderName => $fileArray) {
                    foreach ($fileArray['file'] as $personRole => $fileObject) {
                        if (in_array($fileObject->getMimeType(), $this->fileExtensionNotValid)) {
                            throw new IncorrectArgumentException('file type has to be of type media');
                        }

                        if($fileObject->getSize() > $uploadMaxSize){
                            throw new IncorrectArgumentException('File size exceeds the maximum limit of '.$uploadMaxSize.' MB');
                        }

                        $filePath = $this->dirToUpload.'/'.$folderName.'/'.$personRole.'/'.$fileObject->getClientOriginalName();

                        $fileObject->move($this->dirToUpload.'/'.$folderName.'/'.$personRole, $fileObject->getClientOriginalName());

                        $this->filePath = $filePath;
                        $this->fileName = $fileObject->getClientOriginalName();
                        $this->personRole = $personRole;
                    }
                }
            }
        }
    }

    public function deleteFile()
    {
        $requestParams = $this->request->request->all();
        if (!isset($requestParams['filePath'])) {
            throw new IncorrectArgumentException('No file path provided');
        }

        if (file_exists($requestParams['filePath'])) {
            $regexForFolder = '/(.+?(?=\/logo))/';
            preg_match($regexForFolder, $requestParams['filePath'], $result);

            if (empty($result)) {
                throw new IncorrectArgumentException('no such directory');
            }

            $this->remove($result[0]);
        }

        return true;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getPersonRole()
    {
        return $this->personRole;
    }
}
