<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 13.02.19
 * Time: 16:37.
 */

namespace AdminBundle\Utils\FileUplaod;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class FileRetrieval extends Filesystem
{
    public const FILE_DOR = __DIR__.'/../../../../web/files/InterventionFiles/Temp';

    public function getFiles($moduleId, $personFolder)
    {
        $isModulefolder = $this->exists(self::FILE_DOR.'/'.$moduleId);
        if (!$isModulefolder) {
            return [];
        }

        $finalDir = self::FILE_DOR.'/'.$moduleId.'/'.$personFolder;
        $isRoleFolder = $this->exists($finalDir);

        if (!$isRoleFolder) {
            return [];
        }

        $finder = new Finder();
        $dir = $finder->in($finalDir);

        if (!$dir->files()->hasResults()) {
            return [];
        }

        $files = [];
        foreach ($dir->files() as $file) {
            $files[] = [
                'name' => $file->getFileName(),
                'path' => '/'.$moduleId.'/'.$personFolder.'/'.$file->getFileName(),
                'file' => $file,
                'personRole' => $personFolder,
            ];
        }

        return $files;
    }
}
