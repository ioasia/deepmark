<?php

namespace AdminBundle\Utils\FileUplaod;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Utils\Contracts\InterventionUpload;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FileUploadFactory
 * @package AdminBundle\Utils\FileUplaod
 */
class FileUploadFactory implements InterventionUpload
{
    public const FILE_DOR = __DIR__.'/../../../../web/files/InterventionFiles/';

    protected $request;

    protected $uploaders = [
        'temporary' => UploadFile::class,
        'published' => '',
        'media'     => MediaUpload::class,
        'logo'      => LogoUpload::class,
    ];

    protected $uploadFolders = [
        'temporary' => self::FILE_DOR . 'Temp',
        'media'     => self::FILE_DOR . 'Temp',
        'logo'      => self::FILE_DOR . 'Logo',
        'published' => self::FILE_DOR . 'Published',
    ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getUploadFolder($folderKey): string
    {
        return isset($this->uploadFolders[$folderKey]) ? $this->uploadFolders[$folderKey] : '';
    }

    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param string $factory
     *
     * @return UploadFile|MediaUpload|LogoUpload
     *
     * @throws IncorrectArgumentException
     */
    public function get(string $factory)
    {
        if (array_key_exists($factory, $this->uploaders)) {
            $uploader = new $this->uploaders[$factory]($this);

            return $uploader;
        }

        throw new IncorrectArgumentException('No uploaders defined');
    }
}
