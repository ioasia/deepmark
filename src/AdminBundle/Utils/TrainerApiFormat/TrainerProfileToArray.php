<?php

namespace AdminBundle\Utils\TrainerApiFormat;

use ApiBundle\Entity\Profile;

class TrainerProfileToArray
{
    /**
     * @param Profile[] $profileArray
     *
     * @return string[][]
     */
    public function __invoke(array $profileArray): array
    {
        /**
         * There should only be instances of profile allowed. Otherwise return untouched
         * results.
         */
        $formatted = [];
        foreach ($profileArray as $profile) {
            if (!$profile instanceof Profile) {
                return $profileArray;
            }
            $formatted[] = $profile->__toArray();
        }

        return $formatted;
    }
}
