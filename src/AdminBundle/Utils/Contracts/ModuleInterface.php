<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 25.01.19
 * Time: 10:35.
 */

namespace AdminBundle\Utils\Contracts;

interface ModuleInterface
{
    public function setVariables(): void;
}
