<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 05.02.19
 * Time: 10:32.
 */

namespace AdminBundle\Utils\Contracts;

interface InterventionUpload
{
    public function getRequest();

    public function getUploadFolder($folderKey): string;
}
