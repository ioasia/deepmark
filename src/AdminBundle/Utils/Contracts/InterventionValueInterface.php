<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 23.01.19
 * Time: 13:17.
 */

namespace AdminBundle\Utils\Contracts;

interface InterventionValueInterface
{
    public function setValues();

    public function __toArray();
}
