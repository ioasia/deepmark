<?php

namespace AdminBundle\Utils\Contracts;

interface TemplateRenderer
{
    public function getTemplate($sessionId, $templateValues);
}
