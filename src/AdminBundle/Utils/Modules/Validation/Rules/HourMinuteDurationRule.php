<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 15:53.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

class HourMinuteDurationRule extends RuleAbstract
{
    public function validate(): RuleAbstract
    {
        $fieldValue = $this->ruleResolver->getValue();

        if (0 == $fieldValue[0] && 0 == $fieldValue[1]) {
            $this->validationResult = 'Both values cannot be 0';
        }

        return $this;
    }
}
