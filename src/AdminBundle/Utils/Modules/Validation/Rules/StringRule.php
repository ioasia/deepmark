<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 14:36.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

class StringRule extends RuleAbstract
{
    const MESSAGE = 'FIELD CANNOT BE EMPTY';

    public function validate(): RuleAbstract
    {
        $fieldValue = $this->ruleResolver->getValue();
        $validationRules = $this->ruleResolver->getValidationRule();

        if ('' == trim($fieldValue)) {
            $this->validationResult = self::MESSAGE;
        }

        return $this;
    }
}
