<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 14:48.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

interface RuleResolver
{
    public function getValidator(): ?RuleAbstract;
}
