<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 14:33.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

/**
 * Class RuleAbstract.
 */
abstract class RuleAbstract
{
    /** string $value **/
    protected $value;

    /** @var ValidationRuleResolver $value */
    protected $ruleResolver;

    /** @var array $results */
    protected $results = [];

    /** @var array $validationRule */
    protected $validationRule;

    /** @var mixed|null $validationResult */
    protected $validationResult = null;

    protected $passedValidation = false;

    /**
     * @return mixed
     */
    abstract public function validate(): self;

    public function getValidationResult()
    {
        return $this->validationResult;
    }

    public function passedValidation(): bool
    {
        return !is_null($this->getValidationResult()) ? false : $this->passedValidation;
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param RuleResolver $ruleResolver
     */
    public function setRuleResolver(RuleResolver $ruleResolver)
    {
        $this->ruleResolver = $ruleResolver;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValidationRule(array $rule)
    {
        $this->validationRule = $rule;
    }

    public function getValidationRule(): ?array
    {
        return $this->validationRule;
    }
}
