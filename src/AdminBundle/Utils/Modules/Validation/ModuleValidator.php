<?php

namespace AdminBundle\Utils\Modules\Validation;

use AdminBundle\Utils\Modules\Validation\Rules\StringRule;
use AdminBundle\Utils\Modules\Validation\Rules\ValidationRuleResolver;
use ApiBundle\Entity\StoredModule;
use Symfony\Component\Yaml\Yaml;

class ModuleValidator
{
    private $session;

    private $validationRules = [];

    private $validationBag = [];

    private $ruleResolver;

    public function __construct($session)
    {
        $this->session = $session;
        $this->setValidationRules();

        $this->ruleResolver = new ValidationRuleResolver();
    }

    public function validate()
    {
        $this->validateStep1();
        $this->validateStep2();
    }

    public function isEmpty()
    {
        return empty($this->validationBag);
    }

    public function getValidationBag()
    {
        return $this->validationBag;
    }

    public function getSerialziedValidationBag()
    {
        return serialize($this->validationBag);
    }

    private function validateStep1()
    {
        if (!isset($this->session['step']['1'])) {
            return;
        }

        if (array_key_exists('module', $this->session['step']['1'])) {
            $modulesSelected = $this->validationRules['rules']['step'][1]['module'];
            foreach ($this->session['step']['1']['module'] as $module) {
                if (in_array($module['appId'], [1, 2])) {
                    continue;
                }

                foreach ($module as $fieldName => $fieldValue) {
                    // don't validate the duration for ASSESSMENT module
                    if (($module['appId'] == StoredModule::ASSESSMENT || $module['appId'] == StoredModule::TEAM_PREPARATION || $module['appId'] == StoredModule::PILOTAGE || $module['appId'] == StoredModule::CONCEPTION || $module['appId'] == StoredModule::CHAPTER)
                        && $fieldName == 'duration') {
                        continue;
                    }
                    if (array_key_exists($fieldName, $modulesSelected)) {
                        $validationRule = $modulesSelected[$fieldName];
                        $validation = $this->validateFieldWithRule($validationRule, $fieldValue, $fieldName);
                        if (!empty($validation)) {
                            foreach ($validation as $validationKey => $validationValue) {
                                $this->validationBag['step'][1][$module['appId']][$validationKey]['error'] = $validationValue;
                                $this->validationBag['step'][1][$module['appId']][$validationKey]['moduleName'] = $module['title'];
                            }
                        }
                    }
                }
            }
        }
    }

    private function validateStep2()
    {
        if (!isset($this->session['step']['2']) || false == $this->session['step'][2]) {
            return;
        }

        $session = $this->session['step']['2']['module'] ?? [];

        foreach ($session as $moduleKey => $modules) {
            $moduleTitle = $this->session['step']['1']['module'][$moduleKey]['title'];
            // don't validate the durationHours and durationMinutes for ASSESSMENT module
            if ($modules['appId'] != StoredModule::ASSESSMENT) {
                if (isset($modules['durationHours']) && isset($modules['durationMinutes'])) {
                    $hoursMinute = [$modules['durationHours'], $modules['durationMinutes']];
                    $validationHourMinute = $this->validateFieldWithRule(['not_empty', 'hourMinuteDuration'], $hoursMinute, 'durationHour/Minute');

                    if (!empty($validationHourMinute)) {
                        foreach ($validationHourMinute as $val) {
                            $this->validationBag['step'][2][$moduleKey]['hourMinuteDuration'] =
                                ['moduleName' => $moduleTitle, 'error' => $val];
                        }
                    }
                }
            }
            $this->validateModulesForStep2($modules, $moduleKey);
        }
    }

    private function validateModulesForStep2(array $modules, string $moduleKey)
    {
        $rulesForEveryStep = $this->validationRules['rules']['step'][2]['overall'];
        $moduleTitle = $this->session['step']['1']['module'][$moduleKey]['title'];

        foreach ($modules as $key => $val) {
            if (isset($modules['bookable'])) {
                if (!isset($modules['skills'])) {
                    $this->validationBag['step'][2][$moduleKey]['skills']['error'] = StringRule::MESSAGE;
                    $this->validationBag['step'][2][$moduleKey]['skills']['moduleName'] = $moduleTitle;
                }
            }

            if (!is_array($val)) {
                if (isset($rulesForEveryStep[$key])) {
                    $validationRule = $rulesForEveryStep[$key];
                    $this->validateValueAndKeepResult(
                        $validationRule,
                        $val,
                        $key,
                        $moduleKey,
                        $moduleTitle
                    );
                }
            } else {
                if ('file' == $key || 'profileSendTo' == $key || 'skills' == $key) {
                    continue;
                }

                foreach ($val as $sessionValues) {
                    foreach ($sessionValues as $valueKey => $valueSession) {
                        if (isset($rulesForEveryStep[$valueKey])) {
                            $validationRule = $rulesForEveryStep[$valueKey];
                            $this->validateValueAndKeepResult(
                                $validationRule,
                                $valueSession,
                                $valueKey,
                                $moduleKey,
                                $moduleTitle
                            );
                        }
                    }
                }
            }
        }
    }

    private function validateValueAndKeepResult($validationRule, $valueSession, $valueKey, $moduleKey, $moduleTitle)
    {
        $validation = $this->validateFieldWithRule($validationRule, $valueSession, $valueKey);
        if (!empty($validation)) {
            foreach ($validation as $validationKey => $validationValue) {
                $this->validationBag['step'][2][$moduleKey][$validationKey]['error'] = $validationValue;
                $this->validationBag['step'][2][$moduleKey][$validationKey]['moduleName'] = $moduleTitle;
            }
        }
    }

    private function validateFieldWithRule($validationRule, $fieldValue, $fieldName)
    {
        $this->ruleResolver->setValidationRule($validationRule);
        $this->ruleResolver->setValue($fieldValue);

        $validator = $this->ruleResolver->getValidator()->validate();
        $result = $validator->getValidationResult();

        $result = !is_null($result) ? [$fieldName => $result] : [];

        return $result;
    }

    private function setValidationRules()
    {
        $this->validationRules = Yaml::parseFile(__DIR__.'/validation_rules.yml');
    }
}
