<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 25.01.19
 * Time: 10:34.
 */

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use AdminBundle\Utils\Modules\Traits\ModuleTime;
use AdminBundle\Utils\Modules\Traits\TrainerNotRequired;

class ElearningModule extends ModuleAbstract implements InterventionValueInterface
{
    use ModuleTime;
    use TrainerNotRequired;
    private $instruction = '';
    protected $minScore;
    protected $maxIteration;
    protected $uploadType;
    protected $isNeedScoreToPass;
    protected $url;
    protected $elearningScore;

    public function setValues(): void
    {
        $this->setTitle();
        $this->setInstructions();
        $this->setStartDate();
        $this->setEndDate();
        $this->setHours();
        $this->setMinutes();
        $this->setMinScore();
        $this->setMaxIteration();
        $this->setUploadType();
        $this->setIsNeedScoreToPass();
        $this->setUrl();
        $this->setElearningScore();
    }

    public function setElearningScore(): void
    {
        if (isset($this->values['elearningScore'])) {
            $this->elearningScore = $this->values['elearningScore'];
        }
    }
    public function setUrl(): void
    {
        if (isset($this->values['url'])) {
            $this->url = $this->values['url'];
        }
    }
    public function setIsNeedScoreToPass(): void
    {
        if (isset($this->values['isNeedScoreToPass'])) {
            $this->isNeedScoreToPass = $this->values['isNeedScoreToPass'];
        }
    }
    private function setInstructions(): void
    {
        if (isset($this->values['instruction'])) {
            $this->instruction = $this->values['instruction'];
        }
    }

    public function setMaxIteration(): void
    {
        if (isset($this->values['maxIteration'])) {
            $this->maxIteration = $this->values['maxIteration'];
        }
    }

    public function setMinScore(): void
    {
        if (isset($this->values['min_score'])) {
            $this->minScore = $this->values['min_score'];
        }
    }

    public function setUploadType(): void
    {
        if (isset($this->values['uploadType'])) {
            $this->uploadType = $this->values['uploadType'];
        }
    }

    public function __toArray(): void
    {
        $this->arrayOutcome = [
            'title' => $this->title,
            'instruction' => $this->instruction,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'hours' => $this->hours,
            'minutes' => $this->minutes,
            'uniqueId' => $this->uniqueId,
            'appId' => $this->appId,
            'maxIteration' => $this->maxIteration,
            'minScore' => $this->minScore,
            'uploadType' => $this->uploadType,
            'isNeedScoreToPass' => $this->isNeedScoreToPass,
            'url' => $this->url,
            'elearningScore' => $this->elearningScore,
        ];
    }
}
