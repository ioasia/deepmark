<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 31.01.19
 * Time: 12:05.
 */

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use AdminBundle\Utils\Modules\Traits\ModuleParticipant;
use AdminBundle\Utils\Modules\Traits\ModuleSession;
use AdminBundle\Utils\Modules\Traits\ModuleTime;
use AdminBundle\Utils\Modules\Traits\TrainerSkill;

class AssessmentModule extends ModuleAbstract implements InterventionValueInterface
{
    use ModuleTime;
    use ModuleSession;
    use ModuleParticipant;
    use TrainerSkill;

    public function setValues()
    {
        $this->setTitle();
        $this->setInstructions();
        $this->setSkills();
        $this->setHours();
        $this->setMinutes();
        $this->setStartDate();
        $this->setEndDate();
        $this->setSessions();
        $this->setAndOr();
        $this->setQuantity();

        $this->setSignatureNumber();
        $this->setSignatureAfternoon();
        $this->setSignature();
        $this->setIsPersonalBooking();
        $this->setIsNeedReport();
        $this->setProfileSendTo();
        $this->setSignatures();
    }

    public function __toArray()
    {
        $this->arrayOutcome = [
            'title' => $this->title,
            'instruction' => $this->instruction,
            'quantity' => $this->quantity,
            'skills' => $this->skills,
            'uniqueId' => $this->uniqueId,
            'hours' => $this->hours,
            'minutes' => $this->minutes,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'appId' => $this->appId,
            'sessions' => $this->sessions,
            'allSkills' => $this->andOr,
            'signature' => $this->signature,
            'signatureNumber' => $this->signatureNumber,
            'signatureAfternoon' => $this->signatureAfternoon,
            'isPersonalBooking' => $this->isPersonalBooking,
            'isNeedReport' => $this->isNeedReport,
            'profileSendTo' => $this->profileSendTo,
            'signatures' => $this->signatures,
        ];
    }
}
