<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 25.01.19
 * Time: 11:29.
 */

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use AdminBundle\Utils\Modules\Traits\ModuleFiles;
use AdminBundle\Utils\Modules\Traits\ModuleTime;
use AdminBundle\Utils\Modules\Traits\TrainerNotRequired;
use AdminBundle\Utils\Modules\Traits\ModuleSession;

class QuizModule extends ModuleAbstract implements InterventionValueInterface
{
    use ModuleTime;
    use ModuleSession;
    use ModuleFiles;
    use TrainerNotRequired;

    private $maxIteration = '';

    private $quiz = [];

    private $minValidation = [];

    private $instruction;

    private $allQuizOptions = [];
    private $allQuizSkills = [];

    protected $minScore;
    protected $bestPracticeScore;

    public function setValues()
    {
        $this->setTitle();
        $this->setInstructions();
        $this->setMaxIteration();
        $this->setQuiz();
        $this->setMinimumValidation();
        $this->setStartDate();
        $this->setEndDate();
        $this->setHours();
        $this->setMinutes();

        $this->setMinScore();
        $this->setBestPracticeScore();
         // Skills
        $this->setSkills();
    }

    private function setInstructions(): void
    {
        if (isset($this->values['instruction'])) {
            $this->instruction = $this->values['instruction'];
        }
    }

    private function setMaxIteration(): void
    {
        if (isset($this->values['maxIteration'])) {
            $this->maxIteration = $this->values['maxIteration'];
        }
    }

    private function setQuiz(): void
    {
        if (isset($this->values['quiz'])) {
            $this->quiz = $this->values['quiz'];
        }
    }

    private function setMinimumValidation()
    {
        if (isset($this->values['minValidation'])) {
            $this->minValidation = $this->values['minValidation'];
        }
    }

    public function setQuizOptions($quiz)
    {
        foreach ($quiz as $singleQuiz) {
            $this->allQuizOptions[$singleQuiz->getId()] = $singleQuiz;
            $allSkills = [];
            if($singleQuiz->getSkills()) {
                foreach($singleQuiz->getSkills() as $skill) {
                    $allSkills[] = $skill->getId();
                }
            }
            $this->allQuizSkills[$singleQuiz->getId()] = implode(',', $allSkills);
        }
    }

    public function setMinScore(): void
    {
        if (isset($this->values['min_score'])) {
            $this->minScore = $this->values['min_score'];
        }
    }

    public function setBestPracticeScore(): void
    {
        if (isset($this->values['best_practice_score'])) {
            $this->bestPracticeScore = $this->values['best_practice_score'];
        }
    }


    public function __toArray()
    {
        $this->arrayOutcome = [
            'title' => $this->title,
            'instruction' => $this->instruction,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'hours' => $this->hours,
            'minutes' => $this->minutes,
            'maxIteration' => $this->maxIteration,
            'quiz' => $this->quiz,
            'minValidation' => $this->minValidation,
            'uniqueId' => $this->uniqueId,
            'appId' => $this->appId,
            'allQuizzes' => $this->allQuizOptions,
            'allQuizSkills' => $this->allQuizSkills,
            'minScore' => $this->minScore,
            'bestPracticeScore' => $this->bestPracticeScore,
            // Skills
            'skills' => $this->skills,
            'allSkills' => $this->andOr,
        ];
    }
}
