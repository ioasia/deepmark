<?php

namespace AdminBundle\Utils\Modules;

abstract class ModuleAbstract
{
    protected $values;

    protected $database = false;

    protected $uniqueId;

    protected $appId = 0;

    protected $title;

    protected $endDate;

    protected $startDate;

    protected $arrayOutcome = [];

    protected $trainer;

    protected $minScore;

    protected $signature;

    protected $signatureNumber;

    protected $signatureAfternoon;

    protected $isPersonalBooking;

    protected $isNeedReport;

    protected $certificationTemplate;


    public function __construct(array $values, string $uniqueId, bool $database = false)
    {
        $this->values = $values;

        $this->database = $database;

        $this->uniqueId = $uniqueId;

        $this->setAppId();
    }

    public function getData(): array
    {
        return $this->arrayOutcome;
    }

    protected function setTitle(): void
    {
        if (isset($this->values['title'])) {
            $this->title = $this->values['title'];
        }
    }

    protected function setStartDate(): void
    {
        if (isset($this->values['startDate'])) {
            $this->startDate = $this->values['startDate'];
        }
    }

    protected function setEndDate(): void
    {
        if (isset($this->values['endDate'])) {
            $this->endDate = $this->values['endDate'];
        }
    }

    protected function setAppId()
    {
        if (isset($this->values['appId'])) {
            $this->appId = $this->values['appId'];
        }
    }

    public function setCalendarRepository()
    {
    }

    public function getTrainer()
    {
        return $this->trainer;
    }

    protected function setMinScore(): void
    {
        if (isset($this->values['minScore'])) {
            $this->minScore = $this->values['minScore'];
        }
    }

    protected function setSignatureNumber(): void
    {
        if (isset($this->values['signatureNumber'])) {
            $this->signatureNumber = $this->values['signatureNumber'];
        }
    }

    protected function setSignatureAfternoon(): void
    {
        if (isset($this->values['signatureAfternoon'])) {
            $this->signatureAfternoon = $this->values['signatureAfternoon'];
        }
    }

    protected function setIsPersonalBooking(): void
    {
        if (isset($this->values['isPersonalBooking'])) {
            $this->isPersonalBooking = $this->values['isPersonalBooking'];
        }
    }

    protected function setIsNeedReport(): void
    {
        if (isset($this->values['isNeedReport'])) {
            $this->isNeedReport = $this->values['isNeedReport'];
        }
    }

    protected function setSignature(): void
    {
        if (isset($this->values['signature'])) {
            $this->signature = $this->values['signature'];
        }
    }

    protected function setCertificationTemplate(): void
    {
        if (isset($this->values['certificateTemplate'])) {
            $this->certificationTemplate = $this->values['certificateTemplate'];
        }
    }

    abstract public function __toArray();
}
