<?php

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\TemplateRenderer;
use AdminBundle\Utils\Intervention\StepValueInterceptor;

class ModuleValueCommand
{
    const FIRST_CHOOSE_MODULE_STEP = 0;
    const SECOND_MODULE_DETAILS = 2;
    const THIRD_CONSTRAINT_SELECTION = 3;
    const FOURTH_TRAINER_CHOICE = 4;

    private $step;

    private $moduleFactory;

    private $noModuleSteps = [
        self::FIRST_CHOOSE_MODULE_STEP,
        self::THIRD_CONSTRAINT_SELECTION,
        self::FOURTH_TRAINER_CHOICE,
    ];

    private $modules;

    private $views = [];

    /** @var TemplateRenderer */
    private $templateRenderer;

    private $sessionId;

    private $session;

    public function setModuleFactory($moduleFactory)
    {
        $this->moduleFactory = $moduleFactory;
    }

    public function setModules($modules)
    {
        $this->modules = $modules;
    }

    public function setStep($step)
    {
        $this->step = $step;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    public function command()
    {
        if (!empty($this->step) || !in_array($this->step, $this->noModuleSteps)) {
            $this->moduleFactory->setRawModules($this->modules);
            $this->moduleFactory->makeModuleTemplates();
            $this->views = $this->moduleFactory->getModuleTemplates();

            $stepInterceptor = new StepValueInterceptor($this->step, $this->session, $this->views);

            $stepInterceptor->setValues();
            $this->views = $stepInterceptor->getValues();

            return;
        }

        if (!empty($this->modules)) {
            $this->views = $this->modules;
        }
    }

    public function setTemplateRenderer(TemplateRenderer $templateRenderer)
    {
        $templateRenderer->setStepNr($this->step);
        $this->templateRenderer = $templateRenderer;
    }

    public function getViews()
    {
        return $this->views;
    }

    public function getViewsWithValues()
    {
        return $this->templateRenderer->getTemplate($this->sessionId, $this->views, $this->session);
    }
}
