<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 31.01.19
 * Time: 11:14.
 */

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use AdminBundle\Utils\Modules\Traits\ModuleSession;
use AdminBundle\Utils\Modules\Traits\ModuleTime;
use AdminBundle\Utils\Modules\Traits\TrainerModule;

class OnlineSessionModule extends ModuleAbstract implements InterventionValueInterface
{
    use ModuleTime;
    use ModuleSession;
    use TrainerModule;

    public function setValues()
    {
        $this->setTitle();
        $this->setInstructions();
        $this->setSkills();

        $this->setHours();
        $this->setMinutes();

        $this->setStartDate();
        $this->setEndDate();

        $this->setSessions();
        $this->setAndOr();

        $this->setMinScore();
        $this->setSignature();
        $this->setSignatureNumber();
        $this->setSignatureAfternoon();
        $this->setIsPersonalBooking();
        $this->setIsNeedReport();
        $this->setProfileSendTo();
        $this->setSignatures();
    }

    public function __toArray()
    {
        $this->arrayOutcome = [
            'title' => $this->title,
            'instruction' => $this->instruction,
            'skills' => $this->skills,
            'uniqueId' => $this->uniqueId,
            'hours' => $this->hours,
            'minutes' => $this->minutes,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'appId' => $this->appId,
            'sessions' => $this->sessions,
            'allSkills' => $this->andOr,
            'minScore' => $this->minScore,
            'signature' => $this->signature,
            'signatureNumber' => $this->signatureNumber,
            'signatureAfternoon' => $this->signatureAfternoon,
            'isPersonalBooking' => $this->isPersonalBooking,
            'isNeedReport' => $this->isNeedReport,
            'profileSendTo' => $this->profileSendTo,
            'signatures' => $this->signatures,
        ];
    }
}
