<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 25.01.19
 * Time: 11:29.
 */

namespace AdminBundle\Utils\Modules;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use AdminBundle\Utils\Modules\Traits\ModuleFiles;
use AdminBundle\Utils\Modules\Traits\ModuleTime;
use AdminBundle\Utils\Modules\Traits\TrainerNotRequired;

class EvaluationModule extends ModuleAbstract implements InterventionValueInterface
{
    use ModuleTime;
    use ModuleFiles;
    use TrainerNotRequired;

    private $quiz = [];

    private $instruction;

    private $allQuizOptions = [];
    private $allQuizSkills = [];

    protected $minScore;

    public function setValues()
    {
        $this->setTitle();
        $this->setInstructions();
        $this->setQuiz();
        $this->setStartDate();
        $this->setEndDate();
        $this->setHours();
        $this->setMinutes();
    }

    private function setInstructions(): void
    {
        if (isset($this->values['instruction'])) {
            $this->instruction = $this->values['instruction'];
        }
    }

    private function setQuiz(): void
    {
        if (isset($this->values['quiz'])) {
            $this->quiz = $this->values['quiz'];
        }
    }

    public function setQuizOptions($quiz)
    {
        foreach ($quiz as $singleQuiz) {
            $this->allQuizOptions[$singleQuiz->getId()] = $singleQuiz->getTitle();
            $this->allQuizSkills[$singleQuiz->getId()] = $singleQuiz->getSkills();
        }
    }
    
    public function setMinScore(): void
    {
        if (isset($this->values['min_score'])) {
            $this->minScore = $this->values['min_score'];
        }
    }

    public function __toArray()
    {
        $this->arrayOutcome = [
            'title' => $this->title,
            'instruction' => $this->instruction,
            'startDate' => $this->startDate,
            'hours' => $this->hours,
            'minutes' => $this->minutes,
            'endDate' => $this->endDate,
            'quiz' => $this->quiz,
            'uniqueId' => $this->uniqueId,
            'appId' => $this->appId,
            'allQuizzes' => $this->allQuizOptions,
            'allQuizzesSkills' => $this->allQuizSkills,
            'minScore' => $this->minScore,
        ];
    }
}
