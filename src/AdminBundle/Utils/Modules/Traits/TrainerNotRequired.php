<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 02.02.19
 * Time: 15:36.
 */

namespace AdminBundle\Utils\Modules\Traits;

trait TrainerNotRequired
{
    public function assignLiveTrainer()
    {
        $this->trainer = null;
    }
}
