<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 31.01.19
 * Time: 12:07.
 */

namespace AdminBundle\Utils\Modules\Traits;

trait ModuleParticipant
{
    private $participants;

    private function setParticipants(): void
    {
        if (isset($this->values['participants'])) {
            $this->participants = $this->values['participants'];
        }
    }
}
