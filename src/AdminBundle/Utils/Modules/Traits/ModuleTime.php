<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 31.01.19
 * Time: 11:37.
 */

namespace AdminBundle\Utils\Modules\Traits;

trait ModuleTime
{
    private $hours;

    private $minutes;

    private function setHours()
    {
        if (isset($this->values['durationHours'])) {
            $this->hours = $this->values['durationHours'];

            return;
        }

        $this->setTimeFormStepOne();
    }

    private function setMinutes()
    {
        if (isset($this->values['durationMinutes'])) {
            $this->minutes = $this->values['durationMinutes'];

            return;
        }

        $this->setTimeFormStepOne();
    }

    private function setTimeFormStepOne()
    {
        if (!empty($this->hours) || !empty($this->minutes)) {
            return;
        }

        $timeValueMap = [
            0 => 'min',
            1 => 'hour',
            2 => 'day',
        ];

        if (isset($this->values['time_param']) && 'day' == $timeValueMap[$this->values['time_param']]) {
            $this->minutes = 0;
            $this->hours = ($this->values['duration'] * 7);

            return;
        }

        if (isset($this->values['time_param']) && 'hour' == $timeValueMap[$this->values['time_param']]) {
            $this->hours = $this->values['duration'];

            return;
        }

        if (isset($this->values['time_param']) && 'min' == $timeValueMap[$this->values['time_param']]) {
            $hours = floor($this->values['duration'] / 60);
            if (0 == $hours) {
                $this->minutes = $this->values['duration'];
                $this->hours = 0;

                return;
            }

            $this->hours = $hours;
            $this->minutes = $this->values['duration'] - ($this->hours * 60);

            return;
        }
    }
}
