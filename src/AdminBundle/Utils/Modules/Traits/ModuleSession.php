<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 31.01.19
 * Time: 11:51.
 */

namespace AdminBundle\Utils\Modules\Traits;

trait ModuleSession
{
    private $skills = [];

    private $instruction;

    private $sessions;

    private $andOr;

    private $quantity;

    private $profileSendTo = [];

    private $signatures;


    private function setQuantity(): void
    {
        if (isset($this->values['quantity'])) {
            $this->quantity = $this->values['quantity'];
        }
    }

    private function setInstructions(): void
    {
        if (isset($this->values['instruction'])) {
            $this->instruction = $this->values['instruction'];
        }
    }

    private function setSkills(): void
    {
        if (isset($this->values['skills'])) {
            $this->skills = $this->values['skills'];
        }
    }

    private function setAndOr(): void
    {
        if (isset($this->values['allSkills'])) {
            $this->andOr = $this->values['allSkills'];
        }
    }

    private function setSessions()
    {
        if (isset($this->values['session'])) {
            $this->sessions = $this->values['session'];
        }
    }

    private function setProfileSendTo(): void
    {
        if (isset($this->values['profileSendTo'])) {
            $this->profileSendTo = $this->values['profileSendTo'];
        }
    }

    private function setSignatures()
    {
        if (isset($this->values['signatures'])) {
            $this->signatures = $this->values['signatures'];
        }
    }
}
