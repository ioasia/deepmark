<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 30.01.19
 * Time: 15:08.
 */

namespace AdminBundle\Utils\Modules;

use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\StoredModule;
use Psr\Container\ContainerInterface;

class ModuleValueFactory
{
    private $templateMap = [
        4  => [
            'class'    => ElearningModule::class,
            'template' => 'card-elearning.html.twig',],
        5  => [
            'class'    => QuizModule::class,
            'template' => 'card-quiz.html.twig',],
        6  => [
            'class'    => OnlineSessionModule::class,
            'template' => 'card-onlineseance.html.twig',],
        7  => [
            'class'    => VirtualModule::class,
            'template' => 'card-virtualclass.html.twig',],
        8  => [
            'class'    => WorkshopModule::class, //online workshop
            'template' => 'card-atelier.html.twig',
        ],
        9  => [
            'class'    => AnimationModule::class,
            'template' => 'card-animation.html.twig',],
        10 => [
            'class'    => EvaluationModule::class,
            'template' => 'card-evaluation.html.twig',],
        11 => [
            'class'    => VirtualModule::class,
            'template' => 'card-assessment.html.twig',],
        12 => [
            'class'    => ChapterModule::class,
            'template' => 'card-chapter.html.twig',],
    ];

    private $modules;

    private $moduleResults = [];

    private $container;

    private $sessionId;

    public function __construct(ContainerInterface $container, $sessionId)
    {
        $this->container = $container;
        $this->sessionId = $sessionId;
    }

    public function setRawModules($modules)
    {
        $this->modules = $modules;
    }

    /**
     * @throws \Twig\Error\Error
     */
    public function makeModuleTemplates(): void
    {
        // Add get `steps` of intervention. dump(steps) in card-... twig template
        $interventionStep = $this->container->get('doctrine')->getRepository(InterventionStep::class);
        /**
         * @var InterventionStep $sessionEntity
         */
        $sessionEntity = $interventionStep->findOneBy(['sessionId' => $this->sessionId]);
        $session       = $sessionEntity->__toArray();
        $steps = isset($session['step']) ? $session['step'] : null;
        // Add get `steps` of intervention. dump(steps) in twig template

        $skills         = $this->container->get('doctrine')->getRepository('ApiBundle:Skill')->getSkills();
        $quiz           = $this->container->get('doctrine')->getRepository(Quizes::class)->findBy(['status' => 1]);
        $evaluation     = $this->container->get('doctrine')->getRepository(Quizes::class)->findBy(['status' => 1]);
        $adminType      = $this->container->get('doctrine')->getRepository(ProfileVariety::class)->findBy([
            'appId' => ProfileVariety::ADMIN,
        ]);
        $supervisorType = $this->container->get('doctrine')->getRepository(ProfileVariety::class)->findBy([
            'appId' => ProfileVariety::SUPERVISOR,
        ]);
        $admins         = $this->container->get('doctrine')->getRepository(Profile::class)->findBy([
            'profileType' => $adminType
        ]);
        $supervisors    = $this->container->get('doctrine')->getRepository(Profile::class)->findBy([
            'profileType' => $supervisorType
        ]);
        foreach ($this->modules as $uniqueId => $module) {
            if (isset($this->templateMap[$module['appId']])) {
                $data = [];
                $map  = $this->templateMap[$module['appId']];
                if (!empty($map['class'])) {
                    $moduleClass = new $map['class']($module, $uniqueId);
                    $moduleClass->setValues();
                    if (5 == $module['appId']) {
                        $moduleClass->setQuizOptions($quiz);
                    }
                    if (StoredModule::EVALUATION == $module['appId']) {
                        $moduleClass->setQuizOptions($evaluation);
                    }

                    $moduleClass->__toArray();
                    $data = $moduleClass->getData();
                }
                if (StoredModule::ONLINE == $module['appId'] || StoredModule::PRESENTATION_ANIMATION == $module['appId']) {
                    $data['admins']      = $admins;
                    $data['supervisors'] = $supervisors;
                }
                $this->moduleResults[] = $this->container->get('templating')
                    ->render(
                        'AdminBundle:Intervention/ModuleCard:' . $map['template'],
                        ['data' => $data, 'skills' => $skills, 'steps' => $steps]
                    );
            }
        }
    }

    public function getModuleTemplates(): array
    {
        return $this->moduleResults;
    }
}
