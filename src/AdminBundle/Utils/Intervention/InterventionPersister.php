<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 24.01.19
 * Time: 09:56.
 */

namespace AdminBundle\Utils\Intervention;

use Psr\Container\ContainerInterface;

class InterventionPersister
{
    private $container;

    private $entities = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setEntities(array $entities)
    {
        $this->entities = $entities;
    }

    public function persist()
    {
        $em = $this->container->get('doctrine')->getManager();
        foreach ($this->entities as $entity) {
            if (!is_array($entity)) {
                $em->persist($entity);
            } else {
                foreach ($entity as $single) {
                    $em->persist($single);
                }
            }
        }
    }
}
