<?php

declare(strict_types=1);

namespace AdminBundle\Utils\Intervention;

use AdminBundle\Exception\IncorrectArgumentException;

class ValueSwap
{
    const SESSION_NR = 1;

    private $session;

    public function __construct(array $step, array $request)
    {
        if ((!isset($step['sessionId']) || !isset($request['sessionId']))
            || ($step['sessionId'] != $request['sessionId'])) {
            throw new IncorrectArgumentException('Sessions have to match');
        }

        $this->session = $step;
    }

    public function updateValues(): void
    {
        // check if there should be more modules that should be added in step 2
        if (array_key_exists('module', $this->session['step'][self::SESSION_NR])) {
            foreach ($this->session['step'][self::SESSION_NR]['module'] as $moduleUniqueId => $value) {
                if (!isset($this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId])) {
                    $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId] = $value;
                } else {
                    $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]['title'] = $value['title'];
                    // issue-248: save duration from step 1 to step 2 again
                    if($value['appId'] != 11 && $value['appId'] != 12) {
                        if ($value['time_param'] == 1) {
                            $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]['durationHours'] = $value['duration'];
                            $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]['durationMinutes'] = 0;
                        } elseif ($value['time_param'] == 0) {
                            $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]['durationHours'] = 0;
                            $this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]['durationMinutes'] = $value['duration'];
                        }
                    }
                }
            }
        }
        // delete those that are not used
        if (array_key_exists('module', $this->session['step'][self::SESSION_NR + 1])) {
            foreach ($this->session['step'][self::SESSION_NR + 1]['module'] as $moduleUniqueId => $value) {
                if (!isset($this->session['step'][self::SESSION_NR]['module'][$moduleUniqueId])) {
                    unset($this->session['step'][self::SESSION_NR + 1]['module'][$moduleUniqueId]);
                }
            }
        }
    }

    public function getModifiedStep(): int
    {
        return self::SESSION_NR + 1;
    }

    public function getMofidiedContent(): array
    {
        $swapedValues = $this->getSwapedValue();

        return $swapedValues['step'][$this->getModifiedStep()];
    }

    public function getSwapedValue(): array
    {
        return $this->session;
    }
}
