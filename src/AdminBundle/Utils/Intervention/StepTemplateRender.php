<?php

namespace AdminBundle\Utils\Intervention;

use AdminBundle\Exception\IncorrectArgumentException;
use AdminBundle\Utils\Contracts\TemplateRenderer;
use Psr\Container\ContainerInterface;

class StepTemplateRender implements TemplateRenderer
{
    /** @var ContainerInterface $container */
    private $container;
    private $stepNumber = null;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    private $stepTemplateMap = [
        0 => 'AdminBundle:Intervention:main.html.twig',
        1 => 'AdminBundle:Intervention:main_2.html.twig',
        2 => 'AdminBundle:Intervention:main_3.html.twig',
        3 => 'AdminBundle:Intervention:main_4.html.twig',
    ];

    public function setStepNr(int $stepNr)
    {
        $this->stepNumber = $stepNr;
    }

    public function getTemplate($sessionId, $templateValues, $session = null)
    {
        if (!array_key_exists($this->stepNumber, $this->stepTemplateMap)) {
            return new IncorrectArgumentException('No step defined');
        }
        if (isset($templateValues['modules'])) {
            foreach ($templateValues['modules'] as $key => $module){
                if (isset($module['skills'])) {
                    $skillNames = [];
                    foreach ($module['skills'] as $skillId){
                        $skill = $this->container->get('doctrine')->getRepository('ApiBundle:Skill')->find($skillId);
                        $skillNames[] = $skill->getDesignation();
                    }
                    $skills_join_operator = ", ";
                    if($module['skills_join_operator']){
                        $skills_join_operator = " ".$module['skills_join_operator']." ";
                    }
                    $templateValues['modules'][$key]['skillNames'] = implode($skills_join_operator, $skillNames);
                }
            }
        }

        return [
            'template' => $this->stepTemplateMap[$this->stepNumber],
            'templateValues' => [
                    'modules' => $templateValues,
                    'session_id' => $sessionId,
                    'session' => $session,
                ],
        ];
    }
}
