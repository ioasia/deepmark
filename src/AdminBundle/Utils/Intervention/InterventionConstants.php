<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 28.03.19
 * Time: 11:11.
 */

namespace AdminBundle\Utils\Intervention;

class InterventionConstants
{
    const DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    const TRAINER_PROFILE_TABLE = ['management', 'blockchain', 'statistics'];
    const START_TIME_MORNING_START = 8;
    const START_TIME_MORNING_END = 11;
    const END_TIME_MORNING_START = 12;
    const END_TIME_MORNING_END = 13;
    const START_TIME_AFTERNOON_START = 12;
    const START_TIME_AFTERNOON_END = 16;
    const END_TIME_AFTERNOON_START = 17;
    const END_TIME_AFTERNOON_END = 20;

    const STUDENT_PROGRESS_PERCENT = 0;
    const STUDENT_MIN_GRADE = 0;
    const STUDENT_MAX_GRADE = 5;
}
