<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 23.01.19
 * Time: 11:04.
 */

namespace AdminBundle\Utils\Intervention;

use AdminBundle\Utils\Contracts\InterventionValueInterface;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Validator\Constraints\DateTime;

//TODO check if this class is still needed
class StepOneValue implements \IteratorAggregate, InterventionValueInterface
{
    protected $paramFetcher;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var int
     */
    private $clientId;

    /**
     * @var \DateTime
     */
    private $beginnig;

    /**
     * @var \DateTime
     */
    private $ending;

    /**
     * @var array
     */
    private $modules = [];

    private $observation;

    /**
     * @var int
     */
    private $adminId;

    public function __construct(ParamFetcher $paramFetcher)
    {
        $this->paramFetcher = $paramFetcher;
    }

    public function setValues(): void
    {
        $this->setDesignation($this->paramFetcher->get('designation'));
        $this->setClientId((int) $this->paramFetcher->get('client_id'));
        $this->setAdminId((int) $this->paramFetcher->get('admin_id'));
        $this->setBeginnig($this->paramFetcher->get('beginning'));
        $this->setEnding($this->paramFetcher->get('ending'));
        $this->setModules($this->paramFetcher->get('modules'));
        $this->setObservation($this->paramFetcher->get('observation'));
    }

    /**
     * @return int
     */
    public function getAdminId(): int
    {
        return $this->adminId;
    }

    /**
     * @param int $adminId
     */
    public function setAdminId(int $adminId): void
    {
        $this->adminId = $adminId;
    }

    /**
     * @return string
     */
    public function getDesignation(): string
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     */
    public function setDesignation(string $designation): void
    {
        $this->designation = $designation;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     */
    public function setClientId(int $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return \DateTime
     */
    public function getBeginning(): \DateTime
    {
        return $this->beginnig;
    }

    /**
     * @param string $beginnig
     *
     * @throws \Exception
     */
    public function setBeginnig(string $beginnig): void
    {
        if ($beginnig instanceof \DateTime) {
            $this->beginnig = $beginnig;

            return;
        }

        $beginnig = str_replace('/', '.', $beginnig);
        $this->beginnig = new \DateTime($beginnig);
    }

    /**
     * @return DateTime
     */
    public function getEnding(): \DateTime
    {
        return $this->ending;
    }

    /**
     * @param string $ending
     *
     * @throws \Exception
     */
    public function setEnding(string $ending): void
    {
        if ($ending instanceof \DateTime) {
            $this->ending = $ending;

            return;
        }

        $ending = str_replace('/', '.', $ending);
        $this->ending = new \DateTime($ending);
    }

    /**
     * @return array
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     */
    public function setModules(array $modules): void
    {
        $this->modules = $modules;
    }

    /**
     * @return mixed
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param mixed $observation
     */
    public function setObservation($observation): void
    {
        $this->observation = $observation;
    }

    public function getIterator()
    {
        $toTraverse = [];

        $toTraverse['designation'] = $this->getDesignation();
        $toTraverse['adminId'] = $this->getAdminId();
        $toTraverse['client_id'] = $this->getClientId();
        $toTraverse['modules'] = $this->getModules();
        $toTraverse['observation'] = $this->getObservation();
        $toTraverse['beginning'] = $this->getBeginning();
        $toTraverse['ending'] = $this->getEnding();

        return new \ArrayIterator($toTraverse);
    }

    public function __toArray()
    {
        // TODO: Implement __toArray() method.
    }
}
