<?php

namespace AdminBundle\Utils\Intervention;

use ApiBundle\Entity\StoredModule;
use AppBundle\Utils\UserTimeZone;
use DateInterval;
use DateTime;

class StepValueInterceptor
{
    private $step;

    private $session;

    private $module;

    public function __construct($step, $session, $modules)
    {
        $this->step = $step;

        $this->session = $session;

        $this->module = $modules;
    }

    public function setValues()
    {
        if (3 === $this->step) {
            $sessionId = $this->session['sessionId'];
            $steps = $this->session['step'];
            $data = [];

            ksort($steps);
            $data['sessionId'] = $sessionId;

            foreach ($steps as $step) {
                $stepNumber = isset($step['step']) ? (int) $step['step'] : '';
                if (1 === $stepNumber) {
                    $data = $this->valuesForStep1($step, $data);
                }
                if (2 === $stepNumber) {
                    $data = $this->valuesForStep2($step, $data);
                }
            }
            $this->module = $data;
        }
    }

    private function valuesForStep1(array $step, array $data): array
    {
        $intervention = array(
            'title' => $step['title'],
            'logo' => $step['storedLogo'],
            'description' => $step['description'],
            'participant' => isset($step['participant']) ? $step['participant'] : null,
            'start' => DateTime::createFromFormat('d/m/Y', $step['intervention_start_date']),
            'end' => DateTime::createFromFormat('d/m/Y', $step['intervention_end_date']),
        );
        $data['intervention'] = $intervention;
        $data['modules'] = [];

        $modules = $step['module'];

        foreach ($modules as $key => $module) {
            $appId = $module['appId'];
            $conceptionsModules = [StoredModule::CONCEPTION, StoredModule::PILOTAGE, StoredModule::TEAM_PREPARATION];
            if (!in_array($appId, $conceptionsModules)) {
                $data['modules'][$key] = array(
                    'id' => $key,
                    'title' => $module['title'],
                    'appId' => (int) $module['appId'],
                    'quantity' => (int) $module['quantity'],
                );
            }
        }

        return $data;
    }

    private function valuesForStep2(array $step, array $data): array
    {
        $modules = $step['module'] ?? [];
        foreach ($modules as $key => $module) {
            if (array_key_exists($key, $data['modules'])) {
                $data['modules'][$key]['start'] = isset($module['startDate']) ? DateTime::createFromFormat('d/m/Y', $module['startDate']) : null;
                $data['modules'][$key]['end'] = isset($module['endDate']) ? DateTime::createFromFormat('d/m/Y', $module['endDate']) : null;
                if (isset($module['durationHours']) && isset($module['durationMinutes'])) {
                    $now = new DateTime();
                    $now->setTime((int) $module['durationHours'], (int) $module['durationMinutes']);
                    $data['modules'][$key]['duration'] = $now;
                } else {
                    $now = new DateTime();
                    $now->setTime(1, 32);
                    $data['modules'][$key]['duration'] = $now;
                }

                $durationHours = $module['durationHours'] ?? 0;
                $durationMinutes = $module['durationMinutes'] ?? 0;
                $moduleQuantity = $data['modules'][$key]['quantity'] ?? 0;

                $data['modules'][$key]['totalDurationInSec'] = $moduleQuantity * ($durationHours * 60 + $durationMinutes) * 60;

                $appId = $data['modules'][$key]['appId'];
                $minDate = null;
                $maxDate = null;

                if (in_array($appId, StoredModule::MODULE_REQUIRE_BOOKING, true) && array_key_exists('skills', $module)) {
                    $data['modules'][$key]['skills'] = $module['skills'];
                    $queryJoinOperator = count($module['skills']) > 1 ? $module['allSkills'] : null;
                    $data['modules'][$key]['skills_join_operator'] = $queryJoinOperator;
                }

                if (
                    in_array($appId, [StoredModule::VIRTUAL_CLASS, StoredModule::ONLINE_WORKSHOP, StoredModule::PRESENTATION_ANIMATION])
                    && array_key_exists('session', $module)) {
                    $sessions = $module['session'];
                    foreach ($sessions as $session) {
                        $participants = isset($session['minParticipant']) ? $session['minParticipant'] : 0;
                        //$dateSession = new \DateTime($session['startDate']);
                        $dateSession = DateTime::createFromFormat('d/m/Y', $session['startDate'], UserTimeZone::getTimeZoneObject());

                        if (isset($session['startTime'])) {
                            $startTime = new DateTime($session['startTime']);
                            $dateSession->setTime($startTime->format('H'), $startTime->format('i'));
                        }

                        if (!$minDate || $dateSession <= $minDate) {
                            $minDate = clone $dateSession;
                        }

                        $endTime = clone $dateSession;
                        $interval = new DateInterval(
                                'PT'.
                                $module['durationHours'].
                                'H'.
                                $module['durationMinutes'].
                                'M'
                            );
                        $endTime->add($interval);

                        if (!$maxDate || $endTime >= $maxDate) {
                            $maxDate = clone $endTime;
                        }
                        $data['modules'][$key]['sessions'] = array(
                                'session_date' => $dateSession,
                                'participants' => (int) $participants,
                            );
                    }
                }
                $data['modules'][$key]['minDate'] = $minDate ? $minDate->setTime(0, 0) : null;
                $data['modules'][$key]['maxDate'] = $maxDate ? $maxDate->setTime(23, 59) : null;
            }
        }

        return $data;
    }

    public function getValues()
    {
        return $this->module;
    }
}
