<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 01.02.19
 * Time: 12:07.
 */

namespace AdminBundle\Utils\Intervention;

use Symfony\Component\HttpFoundation\Session\Session;

class SessionHandler
{
    const SESSION_NAME = 'intervention';
    private $session;

    private $sessionObject;

    private $sessionFound = false;

    private $isRemake = false;

    private $remakeSession = [];

    private $remakeSessionStep;

    public function __construct()
    {
        $session = new Session();
        $this->sessionObject = $session;
        $this->session = $session->get(self::SESSION_NAME);
    }

    public function isSession()
    {
        $found = false;
        if (!empty($this->session)) {
            $this->sessionFound = $this->session;
            $found = true;
        }

        return $found;
    }

    public function isRemake(bool $remake)
    {
        $this->isRemake = $remake;
    }

    public function clearSession()
    {
        $this->sessionObject->remove(self::SESSION_NAME);
    }

    public function getSession()
    {
        $this->isSession();

        return $this->sessionFound;
    }

    public function getRemakeSession()
    {
        return $this->remakeSession;
    }

    public function getRemakeSessionStep()
    {
        return $this->remakeSessionStep;
    }

    public function setSession($stepNr, $data)
    {
        if (isset($this->sessionFound['sessionId'])) {
            if (isset($data['sessionId'])) {
                if ($this->sessionFound['sessionId'] == $data['sessionId']) {
                    $this->sessionFound['step'][$stepNr] = $data;
                    $this->sessionObject->set('intervention', $this->sessionFound);

                    if ($this->isRemake) {
                        foreach ($this->sessionFound['step'][$stepNr]['module'] as $moduleUniqueId => $value) {
                            if (!isset($this->sessionFound['step'][$stepNr + 1]['module'][$moduleUniqueId])) {
                                $this->sessionFound['step'][$stepNr + 1]['module'][$moduleUniqueId] = $value;
                            }
                        }

                        foreach ($this->sessionFound['step'][$stepNr + 1]['module'] as $moduleUniqueId => $value) {
                            if (!isset($this->sessionFound['step'][$stepNr]['module'][$moduleUniqueId])) {
                                unset($this->sessionFound['step'][$stepNr + 1]['module'][$moduleUniqueId]);
                            }
                        }

                        $this->remakeSession = $this->sessionFound['step'][$stepNr + 1];
                        $this->remakeSession['step'] = $stepNr + 1;
                        $this->remakeSession['sessionId'] = $this->sessionFound['sessionId'];
                        $this->remakeSessionStep = $stepNr + 1;
                    }
                }

                return;
            }
        }

        // No hash matching then delete any hash
        if (!empty($this->session)) {
            $this->sessionObject->remove(self::SESSION_NAME);
        }

        // Set new session
        $this->sessionFound['sessionId'] = $data['session_unique_id'];
        $this->sessionFound['step'][$stepNr] = $data;
        $this->sessionObject->set('intervention', $this->sessionFound);
    }
}
