<?php

namespace Utils;

use Psr\Container\ContainerInterface;
use Traits\Symfony\HasLogger;
use Traits\Symfony\HasTranslate;
use Traits\Symfony\HasTwig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Utils\Email\HasEmailAlertPending;
use Utils\Email\HasEmailBookingCancel;
use Utils\Email\HasEmailBookingConfirm;
use Utils\Email\HasEmailCourseAssignment;
use Utils\Email\HasEmailCourseRegistration;
use Utils\Email\HasEmailLeanerNotCompleteCourse;
use Utils\Email\HasEmailLeanerNotStartCourse;
use Utils\Email\HasEmailAssignedToExercise;
use Utils\Email\HasEmailNewUser;
use Utils\Email\HasEmailResetPassword;
use Utils\Email\HasEmailWarningLate;
use Utils\Email\HasEmailNextSession;
use Utils\Email\HasEmailPlaningRequest;
use Utils\Email\HasEmailReplacementConfirmed;
use Utils\Email\HasEmailReplacementDeny;
use Utils\Email\HasEmailReplacementPending;

/**
 * Class Email
 * @package Utils
 */
class Email
{
    use HasTwig;
    use HasLogger;
    use HasTranslate;

    use HasEmailNewUser;
    use HasEmailResetPassword;
    use HasEmailWarningLate;
    use HasEmailReplacementDeny;
    use HasEmailCourseAssignment;
    use HasEmailReplacementConfirmed;
    use HasEmailBookingCancel;
    use HasEmailNextSession;
    use HasEmailLeanerNotStartCourse;
    use HasEmailLeanerNotCompleteCourse;
    use HasEmailAssignedToExercise;
    use HasEmailPlaningRequest;
    use HasEmailAlertPending;
    use HasEmailReplacementPending;
    use HasEmailCourseRegistration;
    use HasEmailBookingConfirm;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Email constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return \Swift_SmtpTransport
     */
    protected function getTransport()
    {
        static $transport;

        if (isset($transport)) {
            return $transport;
        }

        $mailHost        = $this->container->getParameter('mailer_host');
        $mailPort        = $this->container->getParameter('mailer_port');
        $emailEncryption = $this->container->getParameter('mailer_encryption');
        $emailUsername   = $this->container->getParameter('mailer_user');
        $emailPassword   = $this->container->getParameter('mailer_password');

        /**
         * @TODO
         * Move parameters to config yaml
         */
        $transport = \Swift_SmtpTransport::newInstance($mailHost, $mailPort, $emailEncryption)
            ->setUsername($emailUsername)
            ->setPassword($emailPassword);

        return $transport;
    }

    /**
     * @return \Swift_Mailer
     */
    protected function getMailer()
    {
        static $mailer;

        if (isset($mailer)) {
            return $mailer;
        }

        $mailer = \Swift_Mailer::newInstance($this->getTransport());

        return $mailer;
    }

    /**
     * @param array $options
     * @return string
     */
    protected function loadBody(array $options)
    {
        try {
            $template = 'email/' . $options['template'] . '.html.twig';
            $this->getLogger()->log('debug', 'Fetching template ' . $template);

            return $this->getTwig()->render($template, $options);
        } catch (\Exception|LoaderError|SyntaxError|RuntimeError $exception) {
            $this->getLogger()->log('error', $exception->getMessage());

            return $exception->getMessage();
        }
    }

    /**
     * @param $email
     * @return bool
     */
    private function validateEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        $this->getLogger()->log('error', 'Invalid email address');

        return false;
    }

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    public function send($to, $options = [])
    {
        if (!$this->validateEmail($to)) {
            return false;
        }

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $this->getLogger()->log('error', $this->trans('Invalid email address'));

            return false;
        }

        /**
         * @TODO
         * Move all parameters to config yaml
         */
        if (!isset($options['from'])) {
            $this->getLogger()->log('warning', 'Email sent from is missed');
            $options['from'] = $this->container->getParameter('mailer_user');
        }

        if (!$this->validateEmail($options['from'])) {
            return false;
        }

        if (!isset($options['template'])) {
            $this->getLogger()->log('warning', 'Email template is missed');
            $options['template'] = 'default';
        }

        if (!isset($options['subject'])) {
            $this->getLogger()->log('warning', 'Email subject is missed');
            $options['subject'] = $this->trans('From Matchlearning');
        }

        try {
            $deliveryAddresses  = $this->container->getParameter('delivery_addresses');
            $to = isset($deliveryAddresses) && !empty($deliveryAddresses) ? $deliveryAddresses : $to;
            $this->getLogger()->log('info', 'Send mail to: ' . $to);
            $message = (new \Swift_Message($options['subject']))
                ->setFrom($options['from'], $this->trans('Matchlearning'))
                ->setTo($to)
                ->setContentType('text/html')
                ->setBody($this->loadBody($options));
            if (isset($options['icsContent'])) {
                if (isset($options['icsMethod']) && $options['icsMethod'] == 'REQUEST') {
                    $eventMethod = $options['icsMethod'];
                    $fileName = "ajouter_invitation_dans_agenda.ics";
                } else {
                    $eventMethod = 'CANCEL';
                    $fileName = "annuler_invitation_dans_agenda.ics";
                }
                $attachment = \Swift_Attachment::newInstance()
                    ->setFilename($fileName)
                    ->setContentType('multipart/alternative;charset=UTF-8;name="'.$fileName.'";method='.$eventMethod)
                    ->setBody($options['icsContent'])
                    ->setDisposition('inline;filename='.$fileName);
                $message->attach($attachment);
            }

            return $this->getMailer()->send($message);
        } catch (\Exception $exception) {
            $this->getLogger()->log('error', $exception->getMessage());

            return $exception->getMessage();
        }
    }
}
