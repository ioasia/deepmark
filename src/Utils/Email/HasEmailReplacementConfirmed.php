<?php

namespace Utils\Email;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\RequestReplacementResource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailReplacementConfirmed
 * @package Utils\Email
 */
trait HasEmailReplacementConfirmed
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $oldTrainer
     * @param Profile $newTrainer
     * @param RequestReplacementResource $replacementResource
     */
    public function sendEmailReplacementConfirmed(
        Profile $oldTrainer,
        Profile $newTrainer,
        RequestReplacementResource $replacementResource
    ) {
        $options = [
            'subject'  => $newTrainer->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.replacement_confirmed'),
            'profile'  => $newTrainer,
            'booked'   => $replacementResource->getBookingAgenda(),
            'joinUrl'  => $this->container->get('router')->generate(
                'learner_courses_module',
                ['id' => $replacementResource->getBookingAgenda()->getModule()->getId()],
                0
            ),
            'template' => 'emailReplacementConfirm',
        ];

        $this->send(
            $oldTrainer->getPerson()->getEmail(),
            $options
        );

        $this->send(
            $newTrainer->getPerson()->getEmail(),
            $options
        );
    }
}
