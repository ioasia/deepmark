<?php

namespace Utils\Email;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailCourseAssignment
 * @package Utils\Email
 */
trait HasEmailCourseAssignment
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param AssigmentResourceSystem $assignment
     * @param Intervention $intervention
     * @param array $duration
     * @return bool|int
     */
    public function sendMailCourseAssignment(
        AssigmentResourceSystem $assignment,
        Intervention $intervention,
        array $duration
    ) {
        return $this->send(
            $assignment->getLiveResource()->getPerson()->getEmail(),
            [
                'subject'      => $assignment->getLiveResource()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('email.course_modification'),
                'profile'      => $assignment->getLiveResource(),
                'intervention' => $intervention,
                'assignment'   => $assignment,
                'joinUrl'      => $this->container->get('router')->generate('trainer_planning_index', [], 0),
                'duration'     => implode(':', $duration),
                'template'     => 'emailCourseAssignment',
            ]
        );
    }

    /**
     * @param AssigmentResourceSystem $assignment
     * @param Intervention $intervention
     * @param array $duration
     * @return bool|int
     */
    public function sendMailSessionAssignment(
        AssigmentResourceSystem $assignment,
        Intervention $intervention,
        array $duration
    ) {
        return $this->send(
            $assignment->getLiveResource()->getPerson()->getEmail(),
            [
                'subject'      => $assignment->getLiveResource()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('email.new_session_assignment'),
                'profile'      => $assignment->getLiveResource(),
                'intervention' => $intervention,
                'assignment'   => $assignment,
                'joinUrl'      => $this->container->get('router')->generate('trainer_planning_index', [], 0),
                'duration'     => implode(':', $duration),
                'template'     => 'emailSessionAssignment',
            ]
        );
    }
}
