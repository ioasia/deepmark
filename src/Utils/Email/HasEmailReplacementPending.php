<?php

namespace Utils\Email;

use ApiBundle\Entity\BookingAgenda;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailReplacementPending
 * @package Utils\Email
 */
trait HasEmailReplacementPending
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param BookingAgenda $bookingAgenda
     */
    public function sendMailReplacementPending(BookingAgenda $bookingAgenda)
    {
        $options = [
            'subject'  => $bookingAgenda->getTrainer()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.replacement_pending'),
            'profile'  => $bookingAgenda->getTrainer(),
            'booked'   => $bookingAgenda,
            'joinUrl'  => $this->container->get('router')->generate(
                'learner_courses_module',
                ['id' => $bookingAgenda->getModule()->getId()],
                0
            ),
            'template' => 'emailReplacementPending',
        ];

        $this->send(
            $bookingAgenda->getTrainer()->getPerson()->getEmail(),
            $options
        );

        $this->send(
            $bookingAgenda->getModule()->getIntervention()->getAdmin()->getPerson()->getEmail(),
            $options
        );
    }
}