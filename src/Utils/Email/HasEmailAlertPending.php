<?php

namespace Utils\Email;

use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailAlertPending
 * @package Utils\Email
 */
trait HasEmailAlertPending
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @param $domain
     * @param $alerts
     * @return bool|int
     */
    public function sendMailAlertPending(Profile $profile, $domain, $alerts)
    {
        return $this->send(
            $profile->getPerson()->getEmail(),
            [
                'subject'   => $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('email.alerts_pending'),
                'profile'   => $profile,
                'alerts'    => $alerts,
                'domainUrl' => $domain,
                'joinUrl'   => $this->container->get('router')->generate('admin_dashboard', [], 0),
                'template'  => 'emailAlertPending',
            ]
        );
    }
}
