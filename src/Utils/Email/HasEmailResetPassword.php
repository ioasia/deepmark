<?php

namespace Utils\Email;

use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Trait HasEmailResetPassword
 * @package Utils\Email
 */
trait HasEmailResetPassword
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @return bool
     */
    public function sendMailNewHasEmailResetPassword(Profile $profile)
    {
        $confirmToken = $profile->getPerson()->getConfirmationToken();

        if ($confirmToken === null || empty($confirmToken)) {
            return false;
        }

        $options['joinUrl'] = $this->container->get('router')
            ->generate(
                'fos_user_resetting_reset',
                array('token' => $confirmToken),
                UrlGeneratorInterface::ABSOLUTE_URL
            );

        // default as the registration email
        $options['subject']  = $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('email.reset_password');
        $options['profile']  = $profile;
        $options['template'] = 'emailResetPassword';

        return $this->send($profile->getPerson()->getEmail(), $options);
    }
}
