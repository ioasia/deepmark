<?php

namespace Utils\Email;

use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailPlaningRequest
 * @package Utils\Email
 */
trait HasEmailPlaningRequest
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @param $domain
     * @return bool|int
     */
    public function sendMailPlaningRequest(Profile $profile, $domain)
    {
        return $this->send(
            $profile->getPerson()->getEmail(),
            [
                'subject'   => $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.planning_update_requested'),
                'profile'   => $profile,
                'domainUrl' => $domain,
                'joinUrl'   => $this->container->get('router')->generate('trainer_planning_index', [], 0),
                'template'  => 'emailPlanningRequest',
            ]
        );
    }
}
