<?php

namespace Utils\Email;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\LearnerIntervention;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailLeanerNotCompleteCourse
 * @package Utils\Email
 */
trait HasEmailLeanerNotCompleteCourse
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param LearnerIntervention $learnerIntervention
     * @param $domain
     */
    public function sendEmailLeanerNotCompleteCourse(LearnerIntervention $learnerIntervention, $domain)
    {
        $options = [
            'subject'   => $learnerIntervention->getLearner()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.alerts_learner_not_complete_course'),
            'profile'   => $learnerIntervention->getLearner(),
            'intervention' => $learnerIntervention->getIntervention(),
            'domainUrl' => $domain,
            'language'  => $this->container->getParameter('locale'),
            'joinUrl'   => $this->container->get('router')->generate(
                'learner_courses_show',
                ['id' => $learnerIntervention->getIntervention()->getId()],
                1
            ),
            'template'  => 'emailLeanerNotCompleteCourse',
        ];

        $this->send($learnerIntervention->getLearner()->getPerson()->getEmail(), $options);
    }
}
