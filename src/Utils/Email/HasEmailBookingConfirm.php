<?php

namespace Utils\Email;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\StoredModule;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailBookingConfirm
 * @package Utils\Email
 */
trait HasEmailBookingConfirm
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @param Module $module
     * @param BookingAgenda $bookingAgenda
     * @return bool|int
     */
    public function sendMailBookingConfirm(Profile $profile, Module $module, BookingAgenda $bookingAgenda)
    {
        $hours = $minutes = 0;
        $timeZone = $profile->getPerson()->getTimeZone();
        // The timezone of $start variable is UTC timezone
        $start  = $bookingAgenda->getBookingDate();
        $end    = clone $start;
        $duration   = clone $bookingAgenda->getModule()->getDuration();
        $hours      += intval($duration->format('H'));
        $minutes    += intval($duration->format('i'));
        $end->add(new \DateInterval('PT' . $hours . 'H' . $minutes . 'M'));

        // Convert to UTC: Outlook will convert the UTC time to the timezone of the Outlook
        $start->setTimezone(new \DateTimeZone('UTC'));
        $end->setTimezone(new \DateTimeZone('UTC'));

        $summary = '';
        if ($module->getStoredModule()->getAppId() === StoredModule::ONLINE)
            $summary = $this->trans('admin.interventions.mods.onlineseance');
        elseif ($module->getStoredModule()->getAppId() === StoredModule::VIRTUAL_CLASS)
            $summary = $this->trans('admin.interventions.mods.virtualclass');
        elseif ($module->getStoredModule()->getAppId() === StoredModule::PRESENTATION_ANIMATION)
            $summary = $this->trans('admin.interventions.mods.animation');

        $joinUrl = $profile->getProfileType()->getAppId() == ProfileVariety::LEARNER ? $this->container->get('router')->generate(
            'learner_courses_module',
            ['id' => $bookingAgenda->getModule()->getId()],
            0
        ) : $this->container->get('router')->generate(
            'trainer_exercices_video_booking_agenda',
            ['id' => $bookingAgenda->getModule()->getId(), 'bid' => $bookingAgenda->getId()],
            0
        );

        if ($profile->getProfileType()->getAppId() == ProfileVariety::LEARNER) {
            $with = $this->trans('admin.interventions.create.step1.trainer');
            $displayName = strip_tags($bookingAgenda->getTrainer()->getDisplayName());
            $email = $bookingAgenda->getTrainer()->getPerson()->getEmail();
        } else {
            $with = $this->trans('admin.interventions.create.step1.learner');
            $displayName = strip_tags($bookingAgenda->getLearner()->getDisplayName());
            $email = $bookingAgenda->getLearner()->getPerson()->getEmail();
        }

        $mail[]  = "BEGIN:VCALENDAR";
        $mail[] = "PRODID:-//hacksw/handcal//NONSGML v1.0//EN";
        $mail[] = "VERSION:2.0";
        $mail[] = "CALSCALE:GREGORIAN";
        $mail[] = "METHOD:REQUEST";
        $mail[] = "BEGIN:VEVENT";
        $mail[] = "DTSTART:".$start->format('Ymd\THis\Z');
        $mail[] = "DTEND:".$end->format('Ymd\THis\Z');
        $mail[] = "DTSTAMP:".$start->format('Ymd\THis\Z');
        $mail[] = "UID:" . $bookingAgenda->getId();
        $mail[] = "ORGANIZER;CN=".$profile->getEntity()->getOrganisation()->getDesignation().":MAILTO:".$this->container->getParameter('mailer_user');

        // process the html format
        $temp = str_replace(array("\r\n"),"\n",'<strong>'.$this->trans('email.intervention_name') . '</strong>' . $bookingAgenda->getModule()->getIntervention()->getDesignation() . '<br /><br /><strong>' . $this->trans('email.module_description') . '</strong><br />' . $bookingAgenda->getModule()->getDescription() . '<br /><strong>' . $this->trans('email.join_session') . '</strong><br />' . $joinUrl);
        $lines = explode("\n",$temp);
        $new_lines =array();
        foreach($lines as $i => $line)
        {
            if(!empty($line))
                $new_lines[]=trim($line);
        }
        $desc = implode("\r\n ",$new_lines);
        $desc = str_replace('<p', '<p style="margin:0"', $desc);

        $mail[] = 'DESCRIPTION:'.$desc;
        $mail[] = 'X-ALT-DESC;FMTTYPE=text/html:'.$desc;

        if (StoredModule::PRESENTATION_ANIMATION === $module->getStoredModule()->getAppId()) {
            if ($bookingAgenda->getModuleSession()) {
                $moduleSession = $bookingAgenda->getModuleSession();
                if ($moduleSession->getSessionPlace()) {
                    $mail[] = "LOCATION:".$moduleSession->getSessionPlace()->getAddress() . " - " . $moduleSession->getSessionPlace()->getPlace();
                }
            }
        } else {
            $mail[] = "LOCATION:".$this->trans('global.video_call_meeting');
        }
        $mail[] = "SEQUENCE:0";
        $mail[] = "STATUS:CONFIRMED";
        $mail[] = "SUMMARY:".$summary.': '.$bookingAgenda->getModule()->getDesignation();
        $mail[] = "TRANSP:OPAQUE";
        $mail[] = "BEGIN:VALARM";
        $mail[] = "DESCRIPTION:REMINDER";
        $mail[] = "TRIGGER:-P0DT0H10M0S";
        $mail[] = "ACTION:DISPLAY";
        $mail[] = "END:VALARM";
        $mail[] = "END:VEVENT";
        $mail[] = "END:VCALENDAR";

        $icsContent = implode("\r\n", $mail);
        return $this->send(
            $profile->getPerson()->getEmail(),
            [
                'subject' => $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.booking_confirm'),
                'profile' => $profile,
                'joinUrl' => $joinUrl,
                'booked' => $bookingAgenda,
                'icsContent' => $icsContent,
                'icsMethod' => 'REQUEST',
                'template' => 'emailBookingConfirm',
            ]
        );
    }
}
