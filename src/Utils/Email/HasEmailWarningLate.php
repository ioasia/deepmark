<?php

namespace Utils\Email;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Trait HasEmailWarningLate
 * @package Utils\Email
 */
trait HasEmailWarningLate
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @param Module $module
     * @param BookingAgenda $bookingAgenda
     * @param string $subject
     * @return bool|int
     */
    public function sendMailNewHasEmailWarningLate(Profile $profile, Module $module, BookingAgenda $bookingAgenda, $subject, $late)
    {
        $time =  $late . $this->trans('email.minutes');
        return $this->send(
        $profile->getPerson()->getEmail(),
            [
                'subject' => $subject,
                'profile' => $profile,
                'joinUrl' => $profile->getProfileType()->getAppId() == ProfileVariety::LEARNER ? $this->container->get('router')->generate(
                    'learner_courses_module',
                    ['id' => $bookingAgenda->getModule()->getId()],
                    0
                ) : $this->container->get('router')->generate(
                    'trainer_exercices_video_booking_agenda',
                    ['id' => $bookingAgenda->getModule()->getId(), 'bid' => $bookingAgenda->getId()],
                    0
                ),
                'booked' => $bookingAgenda,
                'template' => 'emailWarningLate',
                'late'   => $late,
                'time'   => $time,
                'language'  => $this->container->getParameter('locale'),
            ]
        );
    }
}
