<?php

namespace Utils\Email;

use ApiBundle\Entity\RequestReplacementResource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailReplacementDeny
 * @package Utils\Email
 */
trait HasEmailReplacementDeny
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param RequestReplacementResource $replacement
     * @return bool|int
     */
    public function sendMailReplacementDeny(RequestReplacementResource $replacement)
    {
        return $this->send(
            $replacement->getBookingAgenda()->getTrainer()->getPerson()->getEmail(),
            [
                'subject'  => $replacement->getBookingAgenda()->getTrainer()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.replacement_deny'),
                'profile'  => $replacement->getBookingAgenda()->getTrainer(),
                'booked'   => $replacement->getBookingAgenda(),
                'joinUrl'  => $this->container->get('router')->generate(
                    'learner_courses_module',
                    ['id' => $replacement->getBookingAgenda()->getModule()->getId()],
                    0
                ),
                'template' => 'emailReplacementDeny',
            ]
        );
    }
}
