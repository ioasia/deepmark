<?php

namespace Utils\Email;

use ApiBundle\Entity\BookingAgenda;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailNextSession
 * @package Utils\Email
 */
trait HasEmailNextSession
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param BookingAgenda $bookingAgenda
     * @param $hours
     * @param $domain
     */
    public function sendEmailNextSession(BookingAgenda $bookingAgenda, $hours, $minutes, $userType = 1, $domain)
    {
        $time = $hours ? $hours . $this->trans('email.hours') : $minutes . $this->trans('email.minutes');
        $options = [
            'subject'   => $bookingAgenda->getLearner()->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.save_the_day',
                    [
                        '%module_type%' => $bookingAgenda->getModule()->getDesignation(),
                        '%module_name%' => $bookingAgenda->getModule()->getIntervention()->getDesignation(),
                        '%time%' => $time,
                    ]),
            'time'      => $time,
            'booked'    => $bookingAgenda,
            'profile'   => $bookingAgenda->getLearner(),
            'type'      => $userType,
            'hours'     => $hours,
            'minutes'   => $minutes,
            'domainUrl' => $domain,
            'language'  => $this->container->getParameter('locale'),
            'joinUrl'   => $this->container->get('router')->generate(
                'learner_courses_video',
                ['id' => $bookingAgenda->getId()],
                1
            ),
            'template'  => 'emailNextSession',
        ];

        if ($userType == 1) {
            // send email to the learner
            $this->send($bookingAgenda->getLearner()->getPerson()->getEmail(), $options);
        } else if ($userType == 2) {
            // send email to the trainer
            $options['profile'] = $bookingAgenda->getTrainer();
            $options['joinUrl'] = $this->container->get('router')->generate(
                'trainer_exercices_video',
                ['id' => $bookingAgenda->getId()],
                1
            );
            $this->send($bookingAgenda->getTrainer()->getPerson()->getEmail(), $options);
        }
    }
}
