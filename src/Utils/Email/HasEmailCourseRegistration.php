<?php

namespace Utils\Email;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailCourseRegistration
 * @package Utils\Email
 */
trait HasEmailCourseRegistration
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    abstract protected function trans($id, array $parameters = [], $domain = null, $locale = null);

    /**
     * @param Profile $profile
     * @param Intervention $intervention
     * @param array $duration
     * @return bool|int
     */
    public function sendMailCourseRegistration(Profile $profile, Intervention $intervention, array $duration)
    {
        return $this->send(
            $profile->getPerson()->getEmail(),
            [
                'subject'      => $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.new_course_registration'),
                'profile'      => $profile,
                'intervention' => $intervention,
                'joinUrl'      => $this->container->get('router')->generate(
                    'learner_courses_show',
                    ['id' => $intervention->getId()],
                    0
                ),
                'duration'     => implode(':', $duration),
                'template'     => 'emailCourseRegistration',
            ]
        );
    }
}
