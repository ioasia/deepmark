<?php

namespace Utils\Email;

use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait HasEmailAssignedToExercise
 * @package Utils\Email
 */
trait HasEmailAssignedToExercise
{
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param $to
     * @param array $options
     * @return bool|int
     */
    abstract public function send($to, $options = []);

    /**
     * @param LearnerIntervention $learnerIntervention
     * @param $domain
     */
    public function sendMailAssignedToExercise(Profile $profile, Exercises $exercises)
    {
        $options = [
            'subject'   => $profile->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $this->trans('admin.alerts.alerts_new_exercises_for_correction'),
            'profile'   => $profile,
            'exercises' => $exercises,
            'language'  => $this->container->getParameter('locale'),
            'joinUrl'   => $this->container->get('router')->generate(
                'trainer_exercices_mark',
                ['id' => $exercises->getAnswer()->getId()],
                0
            ),
            'template'  => 'emailAssignedToExercise',
        ];

        $this->send($profile->getPerson()->getEmail(), $options);
    }
}
