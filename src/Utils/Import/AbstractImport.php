<?php

namespace Utils\Import;

use ApiBundle\Entity\ProfileVariety;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Utils\Import\Adapter\PhpSpreadsheet;

/**
 * Class BaseImport
 * @package Utils\Import
 */
abstract class AbstractImport
{
    /**
     * @var PhpSpreadsheet
     */
    protected $processor;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $mappedFields = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * AbstractImport constructor.
     * @param EntityManagerInterface $entityManager
     * @param Container $container
     */
    public function __construct(EntityManagerInterface $entityManager, Container $container)
    {
        $this->em        = $entityManager;
        $this->container = $container;
    }

    /**
     * @param string $inputFile
     * @param string $delimiter
     * @return $this|void
     * @throws Exception
     */
    public function loadFile(string $inputFile, $delimiter = ',')
    {
        if (!file_exists($inputFile)) {
            $this->errors[] = 'File not found';

            return;
        }

        $this->processor = new PhpSpreadsheet($inputFile, ['delimiter' => $delimiter]);

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array_merge(['--No data--'], $this->processor->getHeaders());
    }

    /**
     * @param array $mappedFields
     */
    public function setMappedFields(array $mappedFields)
    {
        $this->mappedFields = $mappedFields;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->processor->getData();
    }

    /**
     * @param integer $role
     * @return boolean
     */
    public function process($role = ProfileVariety::LEARNER)
    {
        return $this->processComplete($this->processData($role));
    }

    /**
     * @param boolean $succeed
     * @return boolean
     */
    protected function processComplete(bool $succeed)
    {
        return $succeed;
    }

    /**
     * @param integer $role
     * @return boolean
     */
    abstract protected function processData($role): bool;

    abstract public function getFields(): array;

    /**
     * @param $type
     * @param $msg
     * @param bool $useFlash
     * @return $this
     */
    public function log($type, $msg, $useFlash = false)
    {
        $msg = $this->container->get('translator')->trans($msg);

        if ($useFlash) {
            $flash = $this->container->get('session')->getFlashBag();
            $flash->add($type, $msg);
        }

        $this->errors[] = [
            'type'    => $type,
            'message' => $msg,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
