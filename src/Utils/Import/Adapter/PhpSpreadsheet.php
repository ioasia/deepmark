<?php

namespace Utils\Import\Adapter;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

/**
 * Class PhpSpreadsheet
 * @package Utils\Import\Adapter
 */
class PhpSpreadsheet
{
    private $inputFile;
    private $options;
    private $processor;
    private $data;

    /**
     * PhpSpreadsheet constructor.
     * @param string $inputFile
     * @param array $options
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function __construct(string $inputFile = '', array $options = ['delimiter' => ','])
    {
        if (empty($inputFile)) {
            throw new \Exception('File name is required');
        }

        $this->inputFile = $inputFile;
        $this->options   = $options;

        $reader = IOFactory::createReaderForFile($this->inputFile);

        if ($reader instanceof Csv) {
            $encoding = mb_detect_encoding(
                file_get_contents($this->inputFile),
                'ASCII,UTF-8,ISO-8859-15'
            );
            $reader->setDelimiter($this->options['delimiter']);
            $reader->setInputEncoding($encoding);
        }

        $this->processor = $reader->load($this->inputFile);
        $this->load();
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function load()
    {
        $worksheet = $this->processor->getActiveSheet();
        $rowIndex  = 0;
        $headers   = [];

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            // This loops through all cells
            $cellIterator->setIterateOnlyExistingCells(false);
            $colIndex = 0;

            foreach ($cellIterator as $cellIndex => $cell) {
                $cellValue = $cell->getValue();
                if ($rowIndex === 0) {
                    $this->data[$cellValue] = null;
                    $headers[$cellIndex]    = $cellValue;
                    continue;
                }

                $this->data[$headers[$cellIndex]][] = $cellValue;
                $colIndex++;
            }

            $rowIndex++;
        }
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array_keys($this->data);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
