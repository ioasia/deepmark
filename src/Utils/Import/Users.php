<?php

namespace Utils\Import;

use ApiBundle\Entity\Group;
use ApiBundle\Entity\InterventionPerimeter;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\LiveResourceOrigin;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Skill;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use AppBundle\Entity\Person;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException as ORMExceptionAlias;
use Traits\Users\HasOrganizations;
use Utils\User;

/**
 * Class Learner
 * @package Utils\Import
 */
class Users extends AbstractImport
{
    use HasOrganizations;

    /**
     * @var null
     */
    protected $profileEntity = null;

    /**
     * @var array
     */
    protected $excludeProfileFields = [
        'id',
        'creationDate',
        'updateDate',
        'billingCode',
        'created',
        'updated',
        'number',
        'street',
        'district',
        'city',
        'region',
        'avatar',
        'pitch',
        'video',
        'curriculumVitae',
        'scannedIban',
        'iban',
        'cv',
        'bicCode',
        'lunchStartTime',
        'lunchEndTime',
        'workingHours',
        'organisationRegistrationNumber',
        'bankName',
        'accountCurrency',
        'bic',
        'bankCode',
        'agencyCode',
        'accountNum',
        'ribKey',
        'domiciliationAgency',
        'interventionPerimeter',
        'liveResourceOrigin'
    ];

    /**
     * @var array
     */
    protected $excludePersonFields = [
        'id',
        'username',
        'usernameCanonical',
        'emailCanonical',
        'password',
        'roles',
        'createdAt',
        'updatedAt',
        'salt',
        'lastLogin',
        'confirmationToken',
        'passwordRequestedAt',
        'enabled',
    ];

    /**
     * @param integer $role
     * @return boolean
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function processData($role): bool
    {
        $data      = $this->getData();
        $totalRows = count($data[array_key_first($data)]);
        $users     = [];

        for ($index = 0; $index < $totalRows; $index++) {
            foreach ($this->mappedFields as $fieldName => $mappedField) {
                $users[$index][$fieldName] = isset($data[$mappedField]) ? $data[$mappedField][$index] : null;
            }
        }

        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();

        foreach ($users as $user) {
            $this->log('info', 'Import user ' . serialize($user));

            if (!$currentUser->isRole('ROLE_SUPER_ADMIN')) {
                // Extra validate for none super admin
            }

            /**
             * @var Profile $profileEntity
             */
            $profileEntity = $this->container->get(User::class)
                ->createNewUser(
                    array_merge($user, ['entity' => $this->profileEntity, 'plainPassword' => Profile::DEFAULT_PASSWORD]),
                    $role
                );

            if (!$profileEntity) {
                $this->log('error', 'Can not import user ' . $user[array_key_first($user)]);
                continue;
            }

            // add skill for trainer
            if (isset($user['skill_1']) && !empty($user['skill_1'])) {
                $this->addSkillResourceVarietyProfile($user['skill_1'], $profileEntity);
            }
            if (isset($user['skill_2']) && !empty($user['skill_2'])) {
                $this->addSkillResourceVarietyProfile($user['skill_2'], $profileEntity);
            }
            if (isset($user['skill_3']) && !empty($user['skill_3'])) {
                $this->addSkillResourceVarietyProfile($user['skill_3'], $profileEntity);
            }
            if (isset($user['skill_4']) && !empty($user['skill_4'])) {
                $this->addSkillResourceVarietyProfile($user['skill_4'], $profileEntity);
            }
            if (isset($user['skill_5']) && !empty($user['skill_5'])) {
                $this->addSkillResourceVarietyProfile($user['skill_5'], $profileEntity);
            }

            // add liveResourceOrigin for trainer
            if (isset($user['liveResource']) && !empty($user['liveResource'])) {
                if (!$liveResource = $this->em->getRepository(LiveResourceOrigin::class)->findOneBy(['designation' => $user['liveResource']])) {
                    $this->log('warning', 'liveResource not found');
                }
                if ($liveResource) {
                    $profileEntity->setLiveResourceOrigin($liveResource);
                }
            }

            // add interventionPerimeter for trainer
            if (isset($user['perimeter']) && !empty($user['perimeter'])) {
                if (!$perimeter = $this->em->getRepository(InterventionPerimeter::class)->findOneBy(['designation' => $user['perimeter']])) {
                    $this->log('warning', 'perimeter not found');
                } else {
                    $profileEntity->setInterventionPerimeter($perimeter);
                }
            }

            $groupName     = isset($user['group']) ? trim($user['group']) : '';
            $entityName    = isset($user['entity']) ? trim($user['entity']) : '';
            $orgName       = isset($user['organization']) ? trim($user['organization']) : '';
            $regionalName  = isset($user['regional']) ? trim($user['regional']) : '';
            $groupOrgName  = isset($user['group_org']) ? trim($user['group_org']) : '';
            $workplaceName = isset($user['workplace']) ? trim($user['workplace']) : '';

            $now = new \DateTime();

            if (!empty($groupName)) {
                if ($role != ProfileVariety::LEARNER) {
                    continue;
                }

                // Group is not exists than create it
                if (!$groupEntity = $this->em->getRepository(Group::class)->findOneBy(['designation' => $groupName])) {
                    $groupEntity = new Group;
                    $groupEntity->setDesignation($groupName);
                    $groupEntity->setCreated($now);
                    $groupEntity->setUpdated($now);
                    $this->log('info', 'Creating new UserGroup ' . $groupName);
                }
                // Assign profile to this group
                $groupEntity->setEntity($this->profileEntity);
                $this->em->persist($groupEntity);

                /**
                 * Assign learner to group
                 * @TODO Use dispatcher instead
                 */
                if (!$learnerGroupEntity = $this->em->getRepository(LearnerGroup::class)
                    ->findOneBy(['group' => $groupEntity, 'learner' => $profileEntity])) {
                    $learnerGroupEntity = new LearnerGroup;
                    $learnerGroupEntity->setGroup($groupEntity);
                    $learnerGroupEntity->setCreated(new \DateTime());
                    $learnerGroupEntity->setUpdated(new \DateTime());

                    $this->em->persist($learnerGroupEntity);
                }

                $learnerGroupEntity->setLearner($profileEntity);
                $this->log('secondary', 'Assigned user to UserGroup ' . $groupName);
            }

            $defaultEntity = $this->em->getRepository(Profile::class)->findOneBy(['person' => $currentUser])->getEntity();

            if (!empty($orgName) && !empty($entityName)) {
                /**
                 * @TODO
                 * - Check if entity is created success
                 * - Show log if all cases
                 * - Bind data automation
                 */
                $orgEntity = $this->createOrganisation(['designation' => $orgName], $isNew);
                if ($isNew) {
                    $this->log('success', 'Created new Organisation ' . $orgEntity->getDesignation());
                }

                $entityEntity = $this->createEntity(['designation' => $entityName], $isNew);
                if ($isNew) {
                    $this->log('success', 'Created new Entity ' . $entityEntity->getDesignation());
                }

                $regionalEntity = $this->createRegional(
                    ['designation' => $regionalName, 'entity' => $entityEntity],
                    $isNew
                );
                if ($isNew) {
                    $this->log('success', 'Created new Regional ' . $regionalEntity->getDesignation());
                }

                $groupOrgEntity = $this->createGroupOrganization([
                    'designation' => $groupOrgName,
                    'regional'    => $regionalEntity,
                ], $isNew);
                if ($isNew) {
                    $this->log('success', 'Created new Group Organization ' . $groupOrgEntity->getDesignation());
                }

                $workplaceEntity = $this->createWorkplace(
                    ['designation' => $workplaceName, 'group' => $groupOrgEntity],
                    $isNew
                );
                if ($isNew) {
                    $this->log('success', 'Created new Workplace ' . $workplaceEntity->getDesignation());
                }

                $entityEntity->setOrganisation($orgEntity);
            } else {
                $entityEntity = $defaultEntity;
            }

            /**
             * Validate allowed entity
             */
            $profileEntity->setEntity($entityEntity);

            // set default working hours for profile
            $workingHours = $entityEntity->getWorkingHours() ? $entityEntity->getWorkingHours() : (isset($orgEntity) && $orgEntity->getWorkingHours() ? $orgEntity->getWorkingHours() : '');
            $profileEntity->setWorkingHours($workingHours);

            $this->log('secondary', 'Assigned user to Entity ' . $entityEntity->getDesignation());

            if (isset($regionalEntity) && !is_null($regionalEntity)) {
                $profileEntity->setRegional($regionalEntity);
                $this->log('secondary', 'Assigned user to Regional ' . $regionalEntity->getDesignation());
            }

            if (isset($groupOrgEntity) && !is_null($groupOrgEntity)) {
                $profileEntity->setGroupOrganization($groupOrgEntity);
                $this->log('secondary', 'Assigned user to Group Organization ' . $groupOrgEntity->getDesignation());
            }

            if (isset($workplaceEntity) && !is_null($workplaceEntity)) {
                $profileEntity->setWorkplace($workplaceEntity);
                $this->log('secondary', 'Assigned user to Group Organization ' . $workplaceEntity->getDesignation());
            }

            if ($defaultEntity->getId() !== $entityEntity->getId()) {
                $this->log('warning', 'Assigned user to Entity not under permission');
            }

            $this->em->flush();
            $this->log('success', 'Succeed');
        }

        return true;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        $fieldNames        = [];
        $metadata          = $this->em->getClassMetadata(Profile::class);
        $profileFieldNames = $metadata->getFieldNames();

        foreach ($profileFieldNames as $index => $fieldName) {
            if (in_array($fieldName, $this->excludeProfileFields)) {
                continue;
            }
            $fieldNames[$fieldName] = !$metadata->isNullable($fieldName);
        }

        $metadata     = $this->em->getClassMetadata(Person::class);
        $personFields = $metadata->getFieldNames();

        foreach ($personFields as $index => $fieldName) {
            if (in_array($fieldName, $this->excludePersonFields)) {
                continue;
            }

            $fieldNames[$fieldName] = !$metadata->isNullable($fieldName);
        }

        $fieldNames['group']        = false;
        $fieldNames['entity']       = true;
        $fieldNames['organization'] = true;

        return $fieldNames;
    }

    /**
     * @TODO Get current user instead use this method
     * @param $entityId
     */
    public function setProfileEntity($entityId)
    {
        $this->profileEntity = $entityId;
    }

    /**
     * @param $skillName
     * @param Skill $skillEntity
     * @param Profile $profileEntity
     * @throws ORMExceptionAlias
     */
    public function addSkillResourceVarietyProfile($skillName, Profile $profileEntity)
    {
        if (!$skillEntity = $this->em->getRepository(Skill::class)->findOneBy(['designation' => $skillName])) {
            $this->log('warning', 'Skill not found and create new skill');

            $skillEntity = new Skill();
            $skillEntity->setDesignation($skillName);
            $this->em->persist($skillEntity);
        }

        if ($skillEntity) {
            $skillResourceEntity = new SkillResourceVarietyProfile;
            $skillResourceEntity->setSkill($skillEntity);
            $skillResourceEntity->setScore(0);
            $skillResourceEntity->setProfile($profileEntity);
            $skillResourceEntity->setStatus(SkillResourceVarietyProfile::STATUS_APPROVED);
            $liveResourceVariety = $this->em->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
                'appId' => LiveResourceVariety::LIVE_TRAINER,
            ));
            $skillResourceEntity->setLiveResourceVariety($liveResourceVariety);
            $skillResourceEntity->setDateAdd(new \DateTime());
            $this->em->persist($skillResourceEntity);
        }
    }
}
