<?php

namespace Utils\Validations\Course;

use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\TrainerReservation;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Modules
 * @package Utils\Validations
 */
class Modules
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Modules constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return Registry|object|null
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    protected function addFlash($type, $message)
    {
        $this->container->get('session')->getFlashBag()->add($type, $message);
    }

    /**
     * @param array $modules
     */
    public function validate(array $modules)
    {
        /**
         * Only use for course creating
         * @TODO Separate module validation class and apply dispatcher
         */
        foreach ($modules as $moduleId => $module) {
            switch ($module['appId']) {
                case StoredModule::QUIZ:
                    if (!isset($module['quiz']) || empty($module['quiz'])) {
                        $this->addFlash(
                            'warning',
                            'Missing quiz ' . $module['displayTitle']
                        );
                    }
                    break;
                case in_array($module['appId'], StoredModule::MODULE_REQUIRE_BOOKING):
                    if (!$trainerReservation = $this->getDoctrine()->getRepository(TrainerReservation::class)
                        ->findBy(['module_unique' => $moduleId])) {
                        $this->addFlash(
                            'warning',
                            'Missing trainers for module ' . $module['displayTitle']
                        );
                    }
                    break;
            }
        }
    }

}
