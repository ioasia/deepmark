<?php

namespace Utils;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Regional;
use ApiBundle\Entity\WorkingHours;
use ApiBundle\Entity\Workplace;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonTimeZone;
use Psr\Container\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class User
 * @package Utils
 */
class User
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * User constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @param int $profileVariety
     * @return Profile|bool
     * @throws \Exception
     */
    public function createNewUser(array $data, $profileVariety = ProfileVariety::LEARNER)
    {
        $em = $this->container->get('doctrine')->getManager();

        // Init default value
        $data['creationDate']  = $data['creationDate'] ?? new \DateTime();
        $data['updateDate']    = $data['updateDate'] ?? new \DateTime();
        $data['billingCode']   = $data['billingCode'] ?? '';
        $data['street']        = $data['street'] ?? '';
        $data['enabled']       = $data['enabled'] ?? 1;
        $data['plainPassword'] = $data['plainPassword'] ?? Profile::DEFAULT_PASSWORD;

        /**
         * Validate
         */

        if (!isset($data['email'])) {
            return false;
        }

        $person = $em->getRepository(Person::class)->createQueryBuilder('p')
            ->where('p.username = :email')
            ->orWhere('p.usernameCanonical = :email')
            ->orWhere('p.email = :email')
            ->orWhere('p.emailCanonical = :email')
            ->setParameter('email', $data['email'])
            ->getQuery()->getResult();

        if ($person) {
            return false;
        }

        $profileEntity = new Profile();
        $personEntity  = new Person();

        foreach ($data as $fieldName => $value) {
            $method = 'set' . ucfirst($fieldName);

            if (!is_callable(array($profileEntity, $method))) {
                switch ($fieldName) {
                    case 'email':
                        $personEntity->setEmail($value);
                        $personEntity->setEmailCanonical($value);
                        $personEntity->setUsernameCanonical($value);
                        $personEntity->setUsername($value);
                        $personEntity->setTimeZone($this->container->get('doctrine')->getManager()->getRepository(PersonTimeZone::class)->findOneBy(['timeZone' => 'Europe/Paris']));
                        break;
                    case 'enabled':
                        $personEntity->setEnabled((int)$value);
                        break;
                    default:
                        if (!is_callable(array($personEntity, $method))) {
                            break;
                        }
                        call_user_func(array($personEntity, $method), $value);
                        break;
                }

                continue;
            }

            switch ($fieldName) {
                case 'workplace':
                    if ($value instanceof Workplace) {
                        $profileEntity->setWorkplace($value);
                    }
                    break;
                case 'regional':
                    if ($value instanceof Regional) {
                        $profileEntity->setRegional($value);
                    }
                    break;
                default:
                    /**
                     * @TODO
                     * Find a better way for bind data
                     */
                    try {
                        call_user_func(array($profileEntity, $method), $value);
                    } catch (\Exception $exception) {
                    }
            }
        }

        /**
         * Person data
         */
        $personEntity->setPlainPassword($data['plainPassword']);

        switch ($profileVariety) {
            case ProfileVariety::LEARNER:
                $personEntity->setRoles(['ROLE_LEARNER']);
                break;
            case ProfileVariety::TRAINER:
                $personEntity->setRoles(['ROLE_TRAINER']);
                break;
            case ProfileVariety::ADMIN:
                $personEntity->setRoles(['ROLE_ADMIN']);
                break;
            case ProfileVariety::SUPER_ADMIN:
                $personEntity->setRoles(['ROLE_SUPER_ADMIN']);
                break;
        }

        // Save person
        $em->persist($personEntity);

        // Save profile
        $profileEntity->setPerson($personEntity);
        $profileEntity->setProfileType(
            $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(['appId' => $profileVariety])
        );

        $em->persist($profileEntity);

        // Save working hours
        //self::setDefaultWorkingHours($profileEntity);

        // Setup reset token
        $token = $this->container->get('fos_user.util.token_generator');
        $personEntity->setPasswordRequestedAt(new \DateTime());
        $personEntity->setConfirmationToken($token->generateToken());

        $em->flush();

        // Send email
        $email = $this->container->get(Email::class);
        $email->send(
            $profileEntity->getPerson()->getEmail(),
            [
                'subject'  => $this->container->get('translator')->trans('admin.alerts.your account_activated'),
                'profile'  => !empty($profileEntity->getEntity()) ? $profileEntity : $this->container->get('doctrine')->getManager()->getRepository('ApiBundle:Profile')->findOneBy(array(
                    'person' => $this->container->get('security.token_storage')->getToken()->getUser()->getId(),
                )),
                'email'    => $profileEntity->getPerson()->getEmail(),
                'joinUrl'  => $this->container->get('router')
                    ->generate(
                        'fos_user_resetting_reset',
                        array('token' => $personEntity->getConfirmationToken()),
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                'template' => 'emailRegistration',
            ]
        );

        return $profileEntity;
    }

    /**
     * @param $profileEntity
     */
    public function setDefaultWorkingHours($profileEntity)
    {
        $em = $this->container->get('doctrine')->getManager();
        $workingHours = $profileEntity->getEntity() && $profileEntity->getEntity()->getWorkingHours() ? $profileEntity->getEntity()->getWorkingHours() : null;
        $profileEntity->setWorkingHours($workingHours);
        $em->persist($profileEntity);
    }
}
