<?php


namespace Utils;

use ApiBundle\Entity\FileDescriptor;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class Filesystem
 * @package Utils
 */
class Filesystem
{
    /**
     * Respond download file
     *
     * @param \SplFileInfo $splFile
     * @param FileDescriptor $fileDescriptionEntity
     * @return BinaryFileResponse|Response
     */
    public static function respondDownload($splFile, $fileDescriptionEntity)
    {
        if (!file_exists($splFile->getPathname())) {
            return new Response('File does not exist.');
        }

        $response = new BinaryFileResponse($splFile);

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Type', $fileDescriptionEntity->getMimeType());
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            mb_convert_encoding($fileDescriptionEntity->getName(), 'ASCII')
        ));

        return $response;
    }
}