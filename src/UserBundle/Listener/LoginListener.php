<?php
/**
 * Created by PhpStorm.
 * User: atrax
 * Date: 8/22/18
 * Time: 2:53 PM.
 */

namespace UserBundle\Listener;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\Profile;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Traits\HasAvailableCalendar;

class LoginListener
{
    use HasAvailableCalendar;
    /** @var Router */
    protected $router;

    /** @var TokenStorage */
    protected $token;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /** @var Logger */
    protected $logger;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param Router                   $router
     * @param TokenStorage             $token
     * @param EventDispatcherInterface $dispatcher
     * @param Logger                   $logger
     */
    public function __construct(Router $router, TokenStorage $token, EventDispatcherInterface $dispatcher, Logger $logger, ContainerInterface $container)
    {
        $this->router = $router;
        $this->token = $token;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
        $this->container    = $container;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if(!empty($event->getRequest()->getSession()->get('_security.main.target_path'))){
            $refererUrl = $event->getRequest()->getSession()->get('_security.main.target_path');
        }
        $session = new Session();
        $user = $event->getAuthenticationToken()->getUser();
        if ($user->getLastLogin() == null) {
            // set last_login session attributes
            $session->set('last_login', -1);
            // add AvailableCalendar for trainer
            if (in_array('ROLE_TRAINER', $user->getRoles())) {
                $this->saveDefaultAvailableCalendar($user);
            }
        }
        $session->set('last_activity', 1);
        if ($user) {
            if(isset($refererUrl)){
                $session->set('referer_url', $refererUrl);
            }
            $doctrine = $this->container->get('doctrine');
            $profile    = $doctrine->getRepository(Profile::class)->findOneBy([
                'person' => $user
            ]);
            $profileActivity = $doctrine->getRepository(Activities::class)
                ->findOneBy(array(
                    'profile'      => $profile,
                    'activityName' => Activities::NUMBER_OF_LOGIN,
                ));
            if (!isset($profileActivity)) {
                $profileActivity = new Activities();
                $profileActivity->setProfile($profile);
                $profileActivity->setActivityName(Activities::NUMBER_OF_LOGIN);
                $profileActivity->setActivityValue(1);
            } else {
                $profileActivity->setActivityValue($profileActivity->getActivityValue() + 1);
            }
            $doctrine->getManager()->persist($profileActivity);
        }
        $this->dispatcher->addListener(KernelEvents::RESPONSE, [$this, 'onKernelResponse']);
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {

        $roles = $this->token->getToken()->getRoles();

        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);

        $this->logger->info('roles_tab', ['roles_tab' => $rolesTab]);
        if($event->getRequest()->getSession()->get('referer_url')){
            $route = $event->getRequest()->getSession()->get('referer_url');
            $event->getRequest()->getSession()->remove('referer_url');
        }else {
            if (in_array('ROLE_SUPER_ADMIN', $rolesTab) || in_array('ROLE_ADMIN', $rolesTab) || in_array('ROLE_MANAGER', $rolesTab)) {
                $route = $this->router->generate('admin_dashboard');
            } elseif (in_array('ROLE_SUPERVISOR', $rolesTab)) {
                $route = $this->router->generate('admin_stats_index');
            } elseif (in_array('ROLE_CLIENT', $rolesTab)) {
                $route = $this->router->generate('client_dashboard');
            } elseif (in_array('ROLE_LEARNER', $rolesTab)) {
                $route = $this->router->generate('learner_dashboard') . '?login=1';
            } elseif (in_array('ROLE_TRAINER', $rolesTab)) {
                $route = $this->router->generate('trainer_dashboard');
            }
        }

        $event->getResponse()->headers->set('Location', $route);
    }
}
