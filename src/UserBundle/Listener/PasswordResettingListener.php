<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 14.03.19
 * Time: 16:53.
 */

namespace UserBundle\Listener;

use ApiBundle\Entity\Profile;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Security\LoginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Utils\Email;

/**
 * Class PasswordResettingListener
 * Listener responsible to change the redirection at the end of the password resetting
 * @package UserBundle\Listener
 */
class PasswordResettingListener implements EventSubscriberInterface
{
    /** @var Router */
    private $router;

    /** @var TokenStorage */
    protected $token;

    /**
     * @var LoginManagerInterface
     */
    protected $loginManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * PasswordResettingListener constructor.
     * @param UrlGeneratorInterface $router
     * @param TokenStorage $token
     * @param LoginManagerInterface $loginManager
     * @param ContainerInterface $container
     */
    public function __construct(
        UrlGeneratorInterface $router,
        TokenStorage $token,
        LoginManagerInterface $loginManager,
        ContainerInterface $container
    ) {
        $this->router       = $router;
        $this->token        = $token;
        $this->loginManager = $loginManager;
        $this->container    = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::RESETTING_RESET_SUCCESS      => 'onPasswordResettingSuccess',
            FOSUserEvents::RESETTING_RESET_COMPLETED    => 'authenticate',
            FOSUserEvents::RESETTING_SEND_EMAIL_CONFIRM => 'onSendMailResettingPassword',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPasswordResettingSuccess(FormEvent $event)
    {
        if ($event->getForm()->getNormData()->getLastLogin() == null) {
            $session = new Session();
            // set last_login session attributes
            $session->set('last_login', -1);
        }

        $rolesTab = $event->getForm()->getNormData()->getRoles();
        $route    = '';
        if (in_array('ROLE_SUPER_ADMIN', $rolesTab) || in_array('ROLE_ADMIN', $rolesTab) || in_array('ROLE_SUPERVISOR',
                $rolesTab) || in_array('ROLE_MANAGER', $rolesTab) || in_array('ROLE_CLIENT', $rolesTab)) {
            $route = $this->router->generate('admin_profile_edit');
        } elseif (in_array('ROLE_TRAINER', $rolesTab)) {
            $route = $this->router->generate('trainer_profile_edit');
        } elseif (in_array('ROLE_LEARNER', $rolesTab)) {
            $route = $this->router->generate('learner_profile_edit');
        }
        $event->setResponse(new RedirectResponse($route));
    }

    /**
     * @param FilterUserResponseEvent $event
     * @param string $eventName
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function authenticate(FilterUserResponseEvent $event, $eventName, EventDispatcherInterface $eventDispatcher)
    {
        try {
            if ($event->getUser()->getLastLogin() === null) {
                $this->loginManager->logInUser('fos_user.firewall_name', $event->getUser(), $event->getResponse());

                $eventDispatcher->dispatch(FOSUserEvents::SECURITY_IMPLICIT_LOGIN,
                    new UserEvent($event->getUser(), $event->getRequest()));
            }
            $event->stopPropagation();
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * @param GetResponseUserEvent $event
     * @throws \Exception
     */
    public function onSendMailResettingPassword(GetResponseUserEvent $event)
    {
        $user = $event->getUser();
        $user->setPasswordRequestedAt(new \DateTime());

        $this->container->get('doctrine')->getManager()->persist($user);
        $this->container->get('doctrine')->getManager()->flush();
        /**
         * @var Profile $profile
         */
        $profile = $this->container->get('doctrine')->getRepository(Profile::class)->findOneBy(['person' => $user]);
        $this->container->get(Email::class)->sendMailNewHasEmailResetPassword($profile);

        return $event->setResponse(
            new RedirectResponse(
                $this->container->get('router')->generate(
                    'fos_user_resetting_check_email',
                    array(
                        'username' => $user->getEmail(),
                    )
                )
            )
        );
    }
}
