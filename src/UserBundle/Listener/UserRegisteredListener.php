<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 14.03.19
 * Time: 16:53.
 */

namespace UserBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use UserBundle\CreateUserRegistrationEvent;
use UserBundle\Utils\Registration\UserRole;

class UserRegisteredListener
{
    /**
     * @param CreateUserRegistrationEvent|Event $event
     *
     * @throws \Exception
     */
    public function onUserRegisterAddProfile(Event $event): void
    {
        $userRole = $this->getUserRole();

        /** @var UserRole $userRole */
        $userRole = new $userRole($event->getUserType());
        $newUserRole = $userRole->__invoke();

        $event->getUser()->setRoles($newUserRole);

        $event->getProfile()->setCreationDate(new \DateTime());
        $event->getProfile()->setUpdateDate(new \DateTime());

        $event->getProfile()->setFirstName('');
        $event->getProfile()->setLastName('');

        $event->getProfile()->setBillingCode('');
        $event->getProfile()->setStreet('');

        $profileVarietyRepository = $event->getProfileVarietyRepository();
        $profileVariety = $profileVarietyRepository->findOneBy(['designation' => ucfirst($event->getUserType())]);

        $event->getProfile()->setProfileType($profileVariety);
        $event->getProfile()->setPerson($event->getUser());
    }

    private function getUserRole()
    {
        return UserRole::class;
    }
}
