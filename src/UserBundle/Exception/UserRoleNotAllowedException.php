<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 14.03.19
 * Time: 15:27.
 */

namespace UserBundle\Exception;

class UserRoleNotAllowedException extends \Exception
{
}
