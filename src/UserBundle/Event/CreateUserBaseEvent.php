<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 15.03.19
 * Time: 10:46.
 */

namespace UserBundle\Event;

use AppBundle\Entity\Person;
use Symfony\Component\EventDispatcher\Event;

class CreateUserBaseEvent extends Event
{
    /** @var Person $person */
    private $person;

    public function __construct(Person $person)
    {
        $this->person = $person;
    }

    public function setUser(Person $user): self
    {
        $this->person = $user;

        return $this;
    }

    public function getUser(): ?Person
    {
        return $this->person;
    }
}
