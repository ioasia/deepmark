<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 22.03.19
 * Time: 12:55.
 */

namespace UserBundle\Controller;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use AppBundle\Entity\Person;
use ImportBundle\Entity\ImportStatus;
use ImportBundle\Entity\ImportUser;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use UserBundle\CreateUserRegistrationEvent;
use UserBundle\Event\CreateUserBaseEvent;

class ImportCreateUserController extends Controller
{
    public function createUserAction($standardPassword)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var Person $user */
        $importedUsers = $this->getDoctrine()->getRepository(ImportUser::class)->findBy(
            ['importStatus' => $this->getDoctrine()->getRepository(ImportStatus::class)->find(ImportStatus::IMPORTED)]
        );

        $profileVarietyRepository = $this->getDoctrine()->getRepository(ProfileVariety::class);

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $flashbag = $this->get('session')->getFlashBag();

        if (!empty($importedUsers)) {
            try {
                /** @var ImportUser $import */
                foreach ($importedUsers as $import) {
                    $userEmail = $this->getDoctrine()->getRepository(Person::class)->findOneBy(['email' => $import->getEmail()]);

                    if (!empty($userEmail)) {
                        continue;
                    }
                    $profile = new Profile();
                    $user = $userManager->createUser();

                    $password = $standardPassword ? $this->getParameter('default_user_password') : Uuid::uuid1();
                    $user->setPlainPassword($password);

                    $user->setUsername($import->getEmail());
                    $user->setEmail($import->getEmail());
                    $user->setUsernameCanonical($import->getEmail());
                    $user->setEnabled(1);

                    /** @var CreateUserRegistrationEvent $userEvent */
                    $userEvent = new CreateUserRegistrationEvent($user, $profile, $profileVarietyRepository);
                    $userEvent->setUserType($import->getImportType());
                    $eventDispatcher = $dispatcher->dispatch('user.register.add.profile', $userEvent);

                    $profile->setPerson($user);
                    $profile->setFirstName($import->getFirstName());
                    $profile->setLastName($import->getSurname());
                    $profile->setMobilePhone($import->getPhone());

                    $user = $eventDispatcher->getUser();
                    $profile = $eventDispatcher->getProfile();

                    $userEvent = new CreateUserBaseEvent($user);
                    $eventDispatcher = $dispatcher->dispatch('user.register.add.time.zone', $userEvent);
                    $user = $eventDispatcher->getUser();

                    $import->setImportStatus($this->getDoctrine()->getRepository(ImportStatus::class)->find(ImportStatus::INVITED));
                    $import->setCreated(1);
                    $this->getDoctrine()->getManager()->persist($profile);
                    $this->getDoctrine()->getManager()->persist($import);
                    $userManager->updateUser($user);
                }

                $flashbag->add('success', 'Users have been created');
            } catch (Exception $exception) {
                $flashbag->add('danger', 'Unexpected error occurred. Please contact IT');
            }
        }

        return $this->redirect($this->generateUrl('admin_dashboard'));
    }
}
