<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Controller;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use FOS\UserBundle\Controller\RegistrationController as RegistrationBaseController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\CreateUserRegistrationEvent;
use UserBundle\Event\CreateUserBaseEvent;
use UserBundle\Exception\UserRoleNotAllowedException;

/**
 * Controller managing the registration.
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends RegistrationBaseController
{
    public function registerAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $profileVarietyRepository = $this->getDoctrine()->getRepository(ProfileVariety::class);
                    $userEvent = new CreateUserRegistrationEvent($user, new Profile(), $profileVarietyRepository);

                    $userType = $request->request->get('user-type');
                    $userEvent->setUserType($userType);

                    $eventDispatcher = $dispatcher->dispatch('user.register.add.profile', $userEvent);

                    $user = $eventDispatcher->getUser();
                    $profile = $eventDispatcher->getProfile();

                    $userEvent = new CreateUserBaseEvent($user);
                    $eventDispatcher = $dispatcher->dispatch('user.register.add.time.zone', $userEvent);
                    $user = $eventDispatcher->getUser();

                    $this->getDoctrine()->getManager()->persist($profile);
                } catch (UserRoleNotAllowedException $exception) {
                    $this->addFlash(
                        'user-role-exception',
                        strtoupper('Select your role ')
                    );

                    return $this->render('@FOSUser/Registration/register.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_login');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
