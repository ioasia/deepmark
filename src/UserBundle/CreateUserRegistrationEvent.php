<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 14.03.19
 * Time: 16:45.
 */

namespace UserBundle;

use ApiBundle\Entity\Profile;
use ApiBundle\Repository\ProfileVarietyRepository;
use AppBundle\Entity\Person;
use Symfony\Component\EventDispatcher\Event;

class CreateUserRegistrationEvent extends Event
{
    /** @var Person $person */
    private $person;

    /** @var Profile $profile */
    private $profile;

    /** @var ProfileVarietyRepository $profileVarietyRepository */
    private $profileVarietyRepository;

    /** @var string $userType */
    private $userType;

    public function __construct(Person $person, Profile $profile, ProfileVarietyRepository $profileVarietyRepository)
    {
        $this->person = $person;

        $this->profile = $profile;

        $this->profileVarietyRepository = $profileVarietyRepository;
    }

    public function getUser(): ?Person
    {
        return $this->person;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getProfileVarietyRepository(): ProfileVarietyRepository
    {
        return $this->profileVarietyRepository;
    }

    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    public function getUserType(): ?string
    {
        return $this->userType;
    }

    public function setUser(Person $user): self
    {
        $this->person = $user;

        return $this;
    }
}
