<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 14.03.19
 * Time: 15:24.
 */

namespace UserBundle\Utils\Registration;

use UserBundle\Exception\UserRoleNotAllowedException;

class UserRole
{
    const ALLOWED_USER_TYPES = [
        'learner', 'trainer',
    ];

    private $role;

    public function __construct($role)
    {
        if (!in_array($role, self::ALLOWED_USER_TYPES)) {
            throw new UserRoleNotAllowedException('Role cannot be emtpy');
        }

        $this->role = $role;
    }

    public function __invoke(): array
    {
        $roleFormat = sprintf('ROLE_%s', strtoupper($this->role));

        return [$roleFormat];
    }
}
