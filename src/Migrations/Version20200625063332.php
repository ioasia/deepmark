<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200625063332 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
        INSERT INTO `stats_export_template` (`id`, `type`, `tab`, `col`, `params`, `created`) VALUES
        (null , 'course', 1, 1, '{\"key\":\"course_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 2, '{\"key\":\"organization\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 3, '{\"key\":\"entity\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 4, '{\"key\":\"number_of_total_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 5, '{\"key\":\"number_of_booking_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 6, '{\"key\":\"number_of_without_booking_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 7, '{\"key\":\"number_of_assessment_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 8, '{\"key\":\"number_of_learners\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 9, '{\"key\":\"number_of_not_started_learners\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 10, '{\"key\":\"number_of_started_learners\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 11, '{\"key\":\"number_of_done_learners\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 12, '{\"key\":\"progression\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 13, '{\"key\":\"number_of_training_groups_registered\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 14, '{\"key\":\"publication_date\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 15, '{\"key\":\"start_date_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 16, '{\"key\":\"end_date_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 17, '{\"key\":\"course_rating\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 18, '{\"key\":\"module_duration_in_hour\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 19, '{\"key\":\"average_time_spent_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 1, 20, '{\"key\":\"total_used_space_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 1, '{\"key\":\"course_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 2, '{\"key\":\"module_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 3, '{\"key\":\"module_type\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 4, '{\"key\":\"count_assessment\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 5, '{\"key\":\"module_duration\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 6, '{\"key\":\"average_real_time_spent\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 7, '{\"key\":\"progression\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 8, '{\"key\":\"not_started\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 9, '{\"key\":\"booked\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 10, '{\"key\":\"started\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 11, '{\"key\":\"completed\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 12, '{\"key\":\"rating_module\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 13, '{\"key\":\"score_to_pass_percent\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 14, '{\"key\":\"average_score_percent\"}', '2020-06-25 13:36:57'),
        (null, 'course', 2, 15, '{\"key\":\"completion_of_learners\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 1, '{\"key\":\"name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 2, '{\"key\":\"fist_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 3, '{\"key\":\"email\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 4, '{\"key\":\"position_id\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 5, '{\"key\":\"employee_id\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 6, '{\"key\":\"organization\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 7, '{\"key\":\"entity\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 8, '{\"key\":\"regional_direction\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 9, '{\"key\":\"group\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 10, '{\"key\":\"workplace\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 11, '{\"key\":\"training_group\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 12, '{\"key\":\"course_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 13, '{\"key\":\"registration_date\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 14, '{\"key\":\"status\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 15, '{\"key\":\"number_completed_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 16, '{\"key\":\"number_validated_modules\",\"multi_col\":3}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 17, '{\"key\":\"number_to_finish_modules\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 18, '{\"key\":\"progression\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 19, '{\"key\":\"real_time_spent\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 20, '{\"key\":\"start_date_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 21, '{\"key\":\"end_date_course\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 22, '{\"key\":\"last_login\"}', '2020-06-25 13:36:57'),
        (null, 'course', 3, 23, '{\"key\":\"number_login\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 1, '{\"key\":\"name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 2, '{\"key\":\"fist_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 3, '{\"key\":\"email\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 4, '{\"key\":\"position_id\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 5, '{\"key\":\"employee_id\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 6, '{\"key\":\"organization\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 7, '{\"key\":\"entity\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 8, '{\"key\":\"regional_direction\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 9, '{\"key\":\"group\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 10, '{\"key\":\"workplace\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 11, '{\"key\":\"training_group\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 12, '{\"key\":\"course_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 13, '{\"key\":\"module_name\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 14, '{\"key\":\"status\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 15, '{\"key\":\"module_duration\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 16, '{\"key\":\"real_time_spent\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 17, '{\"key\":\"score\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 18, '{\"key\":\"score_to_pass\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 19, '{\"key\":\"result\"}', '2020-06-25 13:36:57'),
        (null, 'course', 4, 20, '{\"key\":\"end_date\"}', '2020-06-25 13:36:57');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
