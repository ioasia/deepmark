<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200930025114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        /*$this->addSql("CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, tag_name VARCHAR(100) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP
                , PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE best_practice (id INT AUTO_INCREMENT NOT NULL, designation VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, module_id INT NOT NULL, resource_id INT NOT NULL
                , resource_type VARCHAR(100), document_type VARCHAR(10), profile_id INT NOT NULL, status VARCHAR (20), created DATETIME DEFAULT CURRENT_TIMESTAMP, updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE best_practice_tag (best_practice_id INT NOT NULL, tag_id INT NOT NULL
                , PRIMARY KEY(best_practice_id, tag_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE best_practice_tag ADD CONSTRAINT FK_best_practice_tag FOREIGN KEY (best_practice_id) REFERENCES best_practice (id)');
        $this->addSql('ALTER TABLE best_practice_tag ADD CONSTRAINT FK_tag_best_practice FOREIGN KEY (tag_id) REFERENCES tags (id)');

        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_best_practice_module FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_best_practice_profile FOREIGN KEY (profile_id) REFERENCES profile (id)');*/

        // auto generation
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE best_practice (id INT AUTO_INCREMENT NOT NULL, profile_id INT NOT NULL, module_id INT DEFAULT NULL, designation VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, status VARCHAR(255) DEFAULT \'TODO\' NOT NULL, resource_id INT NOT NULL, resource_type VARCHAR(100) DEFAULT NULL, resource_param VARCHAR(100) DEFAULT NULL, document_type VARCHAR(20) DEFAULT NULL, `rating` FLOAT, publish_date DATETIME DEFAULT NULL, comment LONGTEXT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_FE6D461FCCFA12B8 (profile_id), INDEX IDX_FE6D461FAFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE best_practice_tag (best_practice_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_B8D9D233E41BD4E5 (best_practice_id), INDEX IDX_B8D9D233BAD26311 (tag_id), PRIMARY KEY(best_practice_id, tag_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE best_practice_response (id INT AUTO_INCREMENT NOT NULL, profile_id INT DEFAULT NULL, best_practice_id INT DEFAULT NULL, designation VARCHAR(255) NOT NULL, date_response DATETIME NOT NULL, notation DOUBLE PRECISION NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_49C95D1DCCFA12B8 (profile_id), INDEX IDX_49C95D1DE41BD4E5 (best_practice_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE my_favorite (id INT AUTO_INCREMENT NOT NULL, profile_id INT DEFAULT NULL, best_practice_id INT DEFAULT NULL, designation VARCHAR(255) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_5A971B96CCFA12B8 (profile_id), INDEX IDX_5A971B96E41BD4E5 (best_practice_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, tag_name VARCHAR(100) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461FCCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE best_practice ADD CONSTRAINT FK_FE6D461FAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE best_practice_tag ADD CONSTRAINT FK_B8D9D233E41BD4E5 FOREIGN KEY (best_practice_id) REFERENCES best_practice (id)');
        $this->addSql('ALTER TABLE best_practice_tag ADD CONSTRAINT FK_B8D9D233BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE best_practice_response ADD CONSTRAINT FK_49C95D1DCCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE best_practice_response ADD CONSTRAINT FK_49C95D1DE41BD4E5 FOREIGN KEY (best_practice_id) REFERENCES best_practice (id)');
        $this->addSql('ALTER TABLE my_favorite ADD CONSTRAINT FK_5A971B96CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE my_favorite ADD CONSTRAINT FK_5A971B96E41BD4E5 FOREIGN KEY (best_practice_id) REFERENCES best_practice (id)');
    }

    public function down(Schema $schema) : void
    {
        /*$this->addSql('DROP TABLE best_practice_tag');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE best_practice');*/

        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE best_practice_tag DROP FOREIGN KEY FK_B8D9D233E41BD4E5');
        $this->addSql('ALTER TABLE best_practice_response DROP FOREIGN KEY FK_49C95D1DE41BD4E5');
        $this->addSql('ALTER TABLE my_favorite DROP FOREIGN KEY FK_5A971B96E41BD4E5');
        $this->addSql('ALTER TABLE best_practice_tag DROP FOREIGN KEY FK_B8D9D233BAD26311');
        $this->addSql('DROP TABLE best_practice');
        $this->addSql('DROP TABLE best_practice_tag');
        $this->addSql('DROP TABLE best_practice_response');
        $this->addSql('DROP TABLE my_favorite');
        $this->addSql('DROP TABLE tags');

    }

}
