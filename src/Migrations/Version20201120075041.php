<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201120075041 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chapter_module (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, chapter_id INT DEFAULT NULL, position INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_577AC1A3AFC2B591 (module_id), INDEX IDX_577AC1A3579F4768 (chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapters (id INT AUTO_INCREMENT NOT NULL, chapter_name VARCHAR(100) NOT NULL, position INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention_chapter (intervention_id INT NOT NULL, chapter_id INT NOT NULL, INDEX IDX_B26FC14B8EAE3863 (intervention_id), INDEX IDX_B26FC14B579F4768 (chapter_id), PRIMARY KEY(intervention_id, chapter_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE intervention_chapter ADD CONSTRAINT FK_B26FC14B8EAE3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id)');
        $this->addSql('ALTER TABLE intervention_chapter ADD CONSTRAINT FK_B26FC14B579F4768 FOREIGN KEY (chapter_id) REFERENCES chapters (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE intervention_chapter DROP FOREIGN KEY FK_B26FC14B579F4768');
        $this->addSql('DROP TABLE chapter_module');
        $this->addSql('DROP TABLE chapters');
        $this->addSql('DROP TABLE intervention_chapter');
    }
}
