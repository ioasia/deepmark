<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200722043504 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE session_signature (id INT AUTO_INCREMENT NOT NULL, profile_id int NOT NULL, session_id INT NOT NULL, signature INT NOT NULL, signature_type int NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP
                , updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE session_signature ADD CONSTRAINT FK_session_signature_profile FOREIGN KEY (profile_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE session_signature ADD CONSTRAINT FK_session_signature_session FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE session_signature ADD CONSTRAINT FK_session_signature_file_descriptor FOREIGN KEY (signature) REFERENCES file_descriptor (id)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE session_signature');

    }
}
