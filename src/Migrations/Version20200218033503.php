<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218033503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module_trainer_response ADD learner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE module_trainer_response ADD CONSTRAINT FK_38FC3B7A6209CB66 FOREIGN KEY (learner_id) REFERENCES profile (id)');
        $this->addSql('CREATE INDEX IDX_38FC3B7A6209CB66 ON module_trainer_response (learner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module_trainer_response DROP FOREIGN KEY FK_38FC3B7A6209CB66');
        $this->addSql('DROP INDEX IDX_38FC3B7A6209CB66 ON module_trainer_response');
        $this->addSql('ALTER TABLE module_trainer_response DROP learner_id');
    }
}
