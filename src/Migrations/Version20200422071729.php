<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422071729 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exercises (id INT AUTO_INCREMENT NOT NULL, trainer_id INT DEFAULT NULL, answer_id INT DEFAULT NULL, date_add DATETIME NOT NULL, validation_date DATETIME DEFAULT NULL, status VARCHAR(255) DEFAULT \'TODO\' NOT NULL, grade INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_FA14991FB08EDF6 (trainer_id), INDEX IDX_FA14991AA334807 (answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exercises ADD CONSTRAINT FK_FA14991FB08EDF6 FOREIGN KEY (trainer_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE exercises ADD CONSTRAINT FK_FA14991AA334807 FOREIGN KEY (answer_id) REFERENCES quiz_users_answers (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE exercises');
    }
}
