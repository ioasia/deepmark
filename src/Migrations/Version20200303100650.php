<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200303100650 extends AbstractMigration
{
public function getDescription() : string
{
return '';
}

public function up(Schema $schema) : void
{
// this up() migration is auto-generated, please modify it to your needs
$this->addSql('ALTER TABLE `quizes` ADD `duration` INT NOT NULL AFTER `limit_attempt`, ADD `skills` TEXT NOT NULL AFTER `duration`, ADD `skills_or_and` INT NOT NULL AFTER `skills`, ADD `correction_duration` INT NOT NULL AFTER `skills_or_and`;');
$this->addSql('CREATE TABLE quiz_skills (quiz_id INT NOT NULL, skill_id INT NOT NULL, INDEX IDX_3875E629853CD175 (quiz_id), INDEX IDX_3875E6295585C142 (skill_id), PRIMARY KEY(quiz_id, skill_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;');
$this->addSql('ALTER TABLE quiz_skills ADD CONSTRAINT FK_3875E629853CD175 FOREIGN KEY (quiz_id) REFERENCES quizes (id); ALTER TABLE quiz_skills ADD CONSTRAINT FK_3875E6295585C142 FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE;');
$this->addSql('ALTER TABLE `module` ADD `correction_duration` TIME NULL AFTER `duration_active`, ADD `correction_duration_max` TIME NULL AFTER `correction_duration`, ADD `pool_trainers` INT NULL AFTER `correction_duration_max`, ADD `show_correction` INT NULL AFTER `pool_trainers`');
$this->addSql('ALTER TABLE `quizes` DROP `skills`;');

}

public function down(Schema $schema) : void
{
// this down() migration is auto-generated, please modify it to your needs
$this->addSql('ALTER TABLE `quizes` DROP `duration`, DROP `skills`, DROP `skills_or_and`, DROP `correction_duration`;');
$this->addSql('ALTER TABLE quiz_skills DROP FOREIGN KEY FK_3875E6295585C142;');   
$this->addSql('ALTER TABLE quiz_skills DROP FOREIGN KEY FK_3875E629853CD175;');   
$this->addSql('DROP TABLE quiz_skills');   
$this->addSql('ALTER TABLE `module`DROP `correction_duration`,DROP `correction_duration_max`,DROP `pool_trainers`,DROP `show_correction`;');
}
}
