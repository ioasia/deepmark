<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324093914 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }


    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE intervention_perimeter (id INT AUTO_INCREMENT NOT NULL, designation VARCHAR(50) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('INSERT INTO `intervention_perimeter` (`id`, `designation`, `created`, `updated`) VALUES (NULL, \'World\', current_timestamp(), current_timestamp()), (NULL, \'Country\', current_timestamp(), current_timestamp()), (NULL, \'Region\', current_timestamp(), current_timestamp()), (NULL, \'Sector\', current_timestamp(), current_timestamp()), (NULL, \'City\', current_timestamp(), current_timestamp());');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE intervention_perimeter');
    }
}
