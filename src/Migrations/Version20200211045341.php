<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200211045341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alter some table for increase the length of designation field';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `organisation` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `regional` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `group_organization` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `workplace` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `entity` CHANGE `designation` `designation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `organisation` CHANGE `designation` `designation` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `regional` CHANGE `designation` `designation` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `group_organization` CHANGE `designation` `designation` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `workplace` CHANGE `designation` `designation` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
        $this->addSql('ALTER TABLE `entity` CHANGE `designation` `designation` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
    }
}
