<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201026091223 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE best_practice_file (best_practice_id INT NOT NULL, file_descriptor_id INT NOT NULL, INDEX IDX_4745D5F9E41BD4E5 (best_practice_id), INDEX IDX_4745D5F9F6FC9C0 (file_descriptor_id), PRIMARY KEY(best_practice_id, file_descriptor_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE best_practice_file ADD CONSTRAINT FK_4745D5F9E41BD4E5 FOREIGN KEY (best_practice_id) REFERENCES best_practice (id)');
        $this->addSql('ALTER TABLE best_practice_file ADD CONSTRAINT FK_4745D5F9F6FC9C0 FOREIGN KEY (file_descriptor_id) REFERENCES file_descriptor (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE best_practice_file');
    }
}
