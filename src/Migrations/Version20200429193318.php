<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429193318 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("TRUNCATE stats_export_template_xref");
        $this->addSql("TRUNCATE stats_export_template");
        $this->addSql("
INSERT INTO `stats_export_template` (`id`, `type`, `tab`, `col`, `params`, `created`) VALUES
(1, 'quiz', 1, 1, '{\"key\":\"course_name\"}', '2020-04-20 06:09:57'),
(2, 'quiz', 1, 2, '{\"key\":\"organization_name\"}', '2020-04-20 06:09:57'),
(3, 'quiz', 1, 3, '{\"key\":\"entity_name\"}', '2020-04-20 06:09:57'),
(4, 'quiz', 1, 4, '{\"key\":\"module_name\"}', '2020-04-20 06:09:57'),
(5, 'quiz', 1, 5, '{\"key\":\"quiz_is_survey\"}', '2020-04-20 06:09:57'),
(6, 'quiz', 1, 6, '{\"key\":\"quiz_count_steps\"}', '2020-04-20 06:09:57'),
(7, 'quiz', 1, 7, '{\"key\":\"quiz_count_question\"}', '2020-04-20 06:09:57'),
(8, 'quiz', 1, 8, '{\"key\":\"quiz_question_total_points\"}', '2020-04-20 06:09:57'),
(9, 'quiz', 1, 9, '{\"key\":\"num_of_assesments\"}', '2020-04-20 06:09:57'),
(10, 'quiz', 1, 10, '{\"key\":\"module_duration\"}', '2020-04-20 06:09:57'),
(11, 'quiz', 1, 11, '{\"key\":\"quiz_by_module_duration_moy\"}', '2020-04-20 06:09:57'),
(12, 'quiz', 1, 12, '{\"key\":\"quiz_by_module_progress_pourcent\"}', '2020-04-20 06:09:57'),
(13, 'quiz', 1, 13, '{\"key\":\"quiz_by_module_progress_not_started\"}', '2020-04-20 06:09:57'),
(14, 'quiz', 1, 14, '{\"key\":\"quiz_by_module_progress_started\"}', '2020-04-20 06:09:57'),
(15, 'quiz', 1, 15, '{\"key\":\"quiz_by_module_progress_finished\"}', '2020-04-20 06:09:57'),
(16, 'quiz', 1, 16, '{\"key\":\"module_rating\"}', '2020-04-20 06:09:57'),
(17, 'quiz', 1, 17, '{\"key\":\"quiz_score_to_pass\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(18, 'quiz', 1, 18, '{\"key\":\"quiz_by_module_score_moy\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(19, 'quiz', 1, 19, '{\"key\":\"quiz_by_module_nb_played_moy\"}', '2020-04-20 06:09:57'),
(20, 'quiz', 1, 20, '{\"key\":\"quiz_by_module_score_pass\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(21, 'quiz', 1, 21, '{\"key\":\"quiz_by_module_step_details\",\"multi_col\":4, \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(22, 'quiz', 2, 1, '{\"key\":\"course_name\"}', '2020-04-20 06:09:57'),
(23, 'quiz', 2, 2, '{\"key\":\"organization_name\"}', '2020-04-20 06:09:57'),
(24, 'quiz', 2, 3, '{\"key\":\"entity_name\"}', '2020-04-20 06:09:57'),
(25, 'quiz', 2, 4, '{\"key\":\"module_name\"}', '2020-04-20 06:09:57'),
(26, 'quiz', 2, 5, '{\"key\":\"quiz_is_survey\"}', '2020-04-20 06:09:57'),
(27, 'quiz', 2, 6, '{\"key\":\"quiz_step_pos\"}', '2020-04-20 06:09:57'),
(28, 'quiz', 2, 7, '{\"key\":\"quiz_step_name\"}', '2020-04-20 06:09:57'),
(29, 'quiz', 2, 8, '{\"key\":\"quiz_question_pos\"}', '2020-04-20 06:09:57'),
(30, 'quiz', 2, 9, '{\"key\":\"quiz_question_type\"}', '2020-04-20 06:09:57'),
(31, 'quiz', 2, 10, '{\"key\":\"quiz_question_libelle\"}', '2020-04-20 06:09:57'),
(32, 'quiz', 2, 11, '{\"key\":\"quiz_question_answer\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(33, 'quiz', 2, 12, '{\"key\":\"quiz_question_success\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(34, 'quiz', 2, 13, '{\"key\":\"quiz_question_duration\"}', '2020-04-20 06:09:57'),
(35, 'quiz', 2, 14, '{\"key\":\"quiz_by_module_question_details\",\"multi_col\":2}', '2020-04-20 06:09:57'),
(36, 'quiz', 3, 1, '{\"key\":\"learner_lastname\"}', '2020-04-20 06:09:57'),
(37, 'quiz', 3, 2, '{\"key\":\"learner_firstname\"}', '2020-04-20 06:09:57'),
(38, 'quiz', 3, 3, '{\"key\":\"learner_email\"}', '2020-04-20 06:09:57'),
(39, 'quiz', 3, 4, '{\"key\":\"learner_position\"}', '2020-04-20 06:09:57'),
(40, 'quiz', 3, 5, '{\"key\":\"learner_employee_id\"}', '2020-04-20 06:09:57'),
(41, 'quiz', 3, 6, '{\"key\":\"organization_name\"}', '2020-04-20 06:09:57'),
(42, 'quiz', 3, 7, '{\"key\":\"entity_name\"}', '2020-04-20 06:09:57'),
(43, 'quiz', 3, 8, '{\"key\":\"learner_regional\"}', '2020-04-20 06:09:57'),
(44, 'quiz', 3, 9, '{\"key\":\"learner_group\"}', '2020-04-20 06:09:57'),
(45, 'quiz', 3, 10, '{\"key\":\"learner_workplace\"}', '2020-04-20 06:09:57'),
(46, 'quiz', 3, 11, '{\"key\":\"learner_group_name\"}', '2020-04-20 06:09:57'),
(47, 'quiz', 3, 12, '{\"key\":\"course_name\"}', '2020-04-20 06:09:57'),
(48, 'quiz', 3, 13, '{\"key\":\"module_name\"}', '2020-04-20 06:09:57'),
(49, 'quiz', 3, 14, '{\"key\":\"quiz_is_survey\"}', '2020-04-20 06:09:57'),
(50, 'quiz', 3, 15, '{\"key\":\"module_duration\"}', '2020-04-20 06:09:57'),
(51, 'quiz', 3, 16, '{\"key\":\"quiz_by_module_learner_duration_total\"}', '2020-04-20 06:09:57'),
(52, 'quiz', 3, 17, '{\"key\":\"quiz_by_module_learner_duration_moy\"}', '2020-04-20 06:09:57'),
(53, 'quiz', 3, 18, '{\"key\":\"quiz_by_module_learner_nb_played\"}', '2020-04-20 06:09:57'),
(54, 'quiz', 3, 19, '{\"key\":\"quiz_by_module_learner_score_best\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(55, 'quiz', 3, 20, '{\"key\":\"quiz_by_module_learner_score_last\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(56, 'quiz', 3, 21, '{\"key\":\"quiz_score_to_pass\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(57, 'quiz', 3, 22, '{\"key\":\"module_status\"}', '2020-04-20 06:09:57'),
(58, 'quiz', 3, 23, '{\"key\":\"quiz_by_module_learner_score_best_date\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(59, 'quiz', 3, 24, '{\"key\":\"quiz_by_module_learner_score_last_date\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(60, 'quiz', 3, 25, '{\"key\":\"quiz_by_module_learner_answer\",\"multi_col\":3}', '2020-04-20 06:09:57'),
(61, 'quiz', 4, 1, '{\"key\":\"learner_lastname\"}', '2020-04-20 06:09:57'),
(62, 'quiz', 4, 2, '{\"key\":\"learner_firstname\"}', '2020-04-20 06:09:57'),
(63, 'quiz', 4, 3, '{\"key\":\"learner_email\"}', '2020-04-20 06:09:57'),
(64, 'quiz', 4, 4, '{\"key\":\"learner_position\"}', '2020-04-20 06:09:57'),
(65, 'quiz', 4, 5, '{\"key\":\"learner_employee_id\"}', '2020-04-20 06:09:57'),
(66, 'quiz', 4, 6, '{\"key\":\"organization_name\"}', '2020-04-20 06:09:57'),
(67, 'quiz', 4, 7, '{\"key\":\"entity_name\"}', '2020-04-20 06:09:57'),
(68, 'quiz', 4, 8, '{\"key\":\"learner_regional\"}', '2020-04-20 06:09:57'),
(69, 'quiz', 4, 9, '{\"key\":\"learner_group\"}', '2020-04-20 06:09:57'),
(70, 'quiz', 4, 10, '{\"key\":\"learner_workplace\"}', '2020-04-20 06:09:57'),
(71, 'quiz', 4, 11, '{\"key\":\"learner_group_name\"}', '2020-04-20 06:09:57'),
(72, 'quiz', 4, 12, '{\"key\":\"course_name\"}', '2020-04-20 06:09:57'),
(73, 'quiz', 4, 13, '{\"key\":\"module_name\"}', '2020-04-20 06:09:57'),
(74, 'quiz', 4, 14, '{\"key\":\"quiz_is_survey\"}', '2020-04-20 06:09:57'),
(75, 'quiz', 4, 15, '{\"key\":\"quiz_step_pos\"}', '2020-04-20 06:09:57'),
(76, 'quiz', 4, 16, '{\"key\":\"quiz_step_name\"}', '2020-04-20 06:09:57'),
(77, 'quiz', 4, 17, '{\"key\":\"quiz_question_pos\"}', '2020-04-20 06:09:57'),
(78, 'quiz', 4, 18, '{\"key\":\"quiz_question_libelle\"}', '2020-04-20 06:09:57'),
(79, 'quiz', 4, 19, '{\"key\":\"quiz_question_type\"}', '2020-04-20 06:09:57'),
(80, 'quiz', 4, 20, '{\"key\":\"quiz_question_answer\"}', '2020-04-20 06:09:57'),
(81, 'quiz', 4, 21, '{\"key\":\"quiz_by_module_learner_by_question_answer\"}', '2020-04-20 06:09:57'),
(82, 'quiz', 4, 22, '{\"key\":\"quiz_by_module_learner_by_question_duration\"}', '2020-04-20 06:09:57'),
(83, 'quiz', 4, 23, '{\"key\":\"quiz_by_module_learner_by_question_status\", \"hideForSurvey\":true}', '2020-04-20 06:09:57'),
(84, 'quiz', 4, 24, '{\"key\":\"quiz_by_module_learner_by_question_score\", \"hideForSurvey\":true}', '2020-04-20 06:09:57');
");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
