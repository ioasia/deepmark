<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504035245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE marks (id INT AUTO_INCREMENT NOT NULL, exercises_id INT DEFAULT NULL, questions_item_id INT DEFAULT NULL, specifics LONGTEXT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_3C6AFA531AFA70CA (exercises_id), INDEX IDX_3C6AFA5361EA7298 (questions_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE marks ADD CONSTRAINT FK_3C6AFA531AFA70CA FOREIGN KEY (exercises_id) REFERENCES exercises (id)');
        $this->addSql('ALTER TABLE marks ADD CONSTRAINT FK_3C6AFA5361EA7298 FOREIGN KEY (questions_item_id) REFERENCES quiz_questions_items (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE marks');
    }
}
