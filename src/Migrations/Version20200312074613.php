<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200312074613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE profile ADD bank_name VARCHAR(255) DEFAULT NULL, ADD account_currency VARCHAR(255) DEFAULT NULL, ADD bic VARCHAR(255) DEFAULT NULL, ADD bank_code VARCHAR(255) DEFAULT NULL, ADD agency_code VARCHAR(255) DEFAULT NULL, ADD account_num VARCHAR(255) DEFAULT NULL, ADD rib_key VARCHAR(255) DEFAULT NULL, ADD domiciliation_agency VARCHAR(255) DEFAULT NULL, CHANGE street street VARCHAR(255) DEFAULT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE profile DROP bank_name, DROP account_currency, DROP bic, DROP bank_code, DROP agency_code, DROP account_num, DROP rib_key, DROP domiciliation_agency, CHANGE street street VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');

    }
}
