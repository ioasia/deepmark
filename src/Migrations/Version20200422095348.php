<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422095348 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `module_assessments` ADD `time_spent_check` INT AFTER `time_spent`;");
        $this->addSql("ALTER TABLE `module_assessments` ADD `weight` INT AFTER `time_spent_check`;");
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `module_assessments` DROP `time_spent_check`;");
        $this->addSql("ALTER TABLE `module_assessments` DROP `weight`;");
        // this down() migration is auto-generated, please modify it to your needs

    }
}
