<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129084315 extends AbstractMigration {

    public function getDescription(): string {
        return 'Change Quiz System';
    }

    public function up(Schema $schema): void {
        $this->addSql(" CREATE TABLE quiz_questions (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, question LONGTEXT NOT NULL, score_to_pass DOUBLE PRECISION DEFAULT NULL, limit_attempt INT DEFAULT NULL, limit_users INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, status INT DEFAULT NULL, params LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quiz_questions_items (id INT AUTO_INCREMENT NOT NULL, question_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, value LONGTEXT NOT NULL, specifics LONGTEXT DEFAULT NULL, scores INT DEFAULT NULL, status INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_5F62C3601E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quiz_users_answers (id INT AUTO_INCREMENT NOT NULL, play_id INT DEFAULT NULL, question_id INT DEFAULT NULL, answer LONGTEXT DEFAULT NULL, scores INT DEFAULT NULL, status INT DEFAULT NULL, params LONGTEXT DEFAULT NULL, INDEX IDX_8A6960DD25576DBD (play_id), INDEX IDX_8A6960DD1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quiz_steps (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, scores DOUBLE PRECISION DEFAULT NULL, score_to_pass DOUBLE PRECISION DEFAULT NULL, limit_attempt INT DEFAULT NULL, limit_users INT DEFAULT NULL, ordering INT DEFAULT NULL, status INT DEFAULT NULL, params LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quiz_users_played (id INT AUTO_INCREMENT NOT NULL, quiz_id INT DEFAULT NULL, user_id INT DEFAULT NULL, scores INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_B5D3C45F853CD175 (quiz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quizes (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, score_to_pass DOUBLE PRECISION DEFAULT NULL, limit_attempt INT DEFAULT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, created_by INT DEFAULT NULL, params LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     CREATE TABLE quiz_questions_xref (id INT AUTO_INCREMENT NOT NULL, quiz_id INT DEFAULT NULL, step_id INT DEFAULT NULL, question_id INT DEFAULT NULL, INDEX IDX_13DB8DAC853CD175 (quiz_id), INDEX IDX_13DB8DAC73B21E9C (step_id), INDEX IDX_13DB8DAC1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB;
     ALTER TABLE quiz_questions_items ADD CONSTRAINT FK_5F62C3601E27F6BF FOREIGN KEY (question_id) REFERENCES quiz_questions (id);
     ALTER TABLE quiz_questions_xref ADD CONSTRAINT FK_13DB8DAC853CD175 FOREIGN KEY (quiz_id) REFERENCES quizes (id);
     ALTER TABLE quiz_questions_xref ADD CONSTRAINT FK_13DB8DAC73B21E9C FOREIGN KEY (step_id) REFERENCES quiz_steps (id);
     ALTER TABLE quiz_questions_xref ADD CONSTRAINT FK_13DB8DAC1E27F6BF FOREIGN KEY (question_id) REFERENCES quiz_questions (id);
     ALTER TABLE quiz_users_answers ADD CONSTRAINT FK_8A6960DD25576DBD FOREIGN KEY (play_id) REFERENCES quiz_users_played (id);
     ALTER TABLE quiz_users_answers ADD CONSTRAINT FK_8A6960DD1E27F6BF FOREIGN KEY (question_id) REFERENCES quiz_questions (id);
     ALTER TABLE quiz_users_played ADD CONSTRAINT FK_B5D3C45F853CD175 FOREIGN KEY (quiz_id) REFERENCES quizes (id);
       
           
        ALTER TABLE `quiz_questions_xref` ADD UNIQUE( `quiz_id`, `step_id`, `question_id`);
        
        DROP TABLE monkey_quiz_settings;
        DROP TABLE native_quiz_answer;
        DROP TABLE native_quiz_question;
        DROP TABLE native_quiz_result;
        DROP TABLE native_quiz_settings;
        DROP TABLE quiz_api_log;
        UPDATE `module` SET `quiz_id` = NULL WHERE `quiz_id` > 0;
        ALTER TABLE module DROP FOREIGN KEY FK_C242628853CD175;
        DROP TABLE quiz;
        ALTER TABLE module ADD CONSTRAINT FK_C242628853CD175 FOREIGN KEY (quiz_id) REFERENCES quizes (id);
        
        
        ALTER TABLE module ADD type_quiz INT DEFAULT NULL;
        ALTER TABLE module ADD duration_active INT DEFAULT NULL;
        
        ALTER TABLE quiz_users_played ADD module_id INT DEFAULT NULL AFTER `id`;
                    ALTER TABLE quiz_users_played ADD CONSTRAINT FK_B5D3C45FAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id);");
        /*
        // Datas template
       
INSERT INTO `quizes` (`id`, `status`, `title`, `description`, `score_to_pass`, `limit_attempt`, `created`, `created_by`, `params`) VALUES
(1, 1, 'Quiz A', NULL, NULL, NULL, '2019-12-08 22:03:56', NULL, NULL");


INSERT INTO `quiz_questions_items` (`id`, `question_id`, `type`, `name`, `value`, `specifics`, `scores`) VALUES
(4, 1, 'qcm', 'Opt 1', '1', NULL, 0),
(5, 1, 'qcm', 'Opt 2', '0', NULL, 0),
(6, 1, 'qcm', 'Opt 3', '0', NULL, 0),
(8, 3, 'category', 'Opt 1', 'Opt 1', NULL, 0),
(9, 3, 'category', 'Opt 2', 'Opt 2', NULL, 0),
(10, 3, 'category', 'Opt 3', 'Opt 3', NULL, 0),
(16, 5, 'drag_and_drop', 'image_to_image', 'b25c125024f95c665e88db7a4f75f4829cb3349f.png', '{\"match\":\"54ebda9770bec5b9090e92517e11bf08900febb7.png\",\"match_type\":\"image\"}', 0),
(17, 5, 'drag_and_drop', 'image_to_text', 'cb87ed7e225c3b830618991293f15b75b06f6a47.png', '{\"match\":\"Big\",\"match_type\":\"text\"}', 0),
(18, 5, 'drag_and_drop', 'image_to_image', '0895d8ca316d0d4f3319e84efdfebe4775266c18.png', '{\"match\":\"54bf5c04d53eeb67b27dd03b50f7bdd282901eae.png\",\"match_type\":\"image\"}', 0),
(19, 5, 'drag_and_drop', 'image_to_text', 'ace59b1de8344e5c1f2ed45d685df294369996d1.png', '{\"match\":\"Small\",\"match_type\":\"text\"}', 0),
(20, 4, 'qcu', 'Opt 1', '1', NULL, 0),
(21, 4, 'qcu', 'Opt 2', '0', NULL, 0),
(22, 4, 'qcu', 'Opt 3', '0', NULL, 0),
(23, 4, 'qcu', 'Opt 4', '0', NULL, 0),
(24, 4, 'qcu', 'Opt 5', '0', NULL, 0),
(30, 6, 'ordering', 'Opt 1', '0', NULL, 0),
(31, 6, 'ordering', 'Opt 2', '0', NULL, 0),
(32, 6, 'ordering', 'Opt 3', '0', NULL, 0),
(33, 6, 'ordering', 'Opt 4', '0', NULL, 0),
(34, 6, 'ordering', 'Opt 5', '0', NULL, 0);
"


INSERT INTO `quiz_questions` (`id`, `title`, `question`, `score_to_pass`, `limit_attempt`, `limit_users`, `type`, `status`, `params`) VALUES
(1, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tellus a augue lacinia hendrerit. Maecenas at velit a metus scelerisque tristique. Morbi erat nunc, sollicitudin ac velit ac, maximus elementum nunc. Sed iaculis ligula eget diam aliquet, at condimentum lectus sodales. Praesent consequat, est ut feugiat mollis, quam sapien venenatis tortor, ac molestie est magna et nisl. Vivamus volutpat mi sit amet tempor porta. Fusce nec elit nibh. Praesent blandit nulla in nunc volutpat posuere. Suspendisse malesuada diam at massa porttitor, at facilisis nibh pharetra. Quisque vel ornare purus, eu mollis enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla efficitur nunc et massa viverra porta. Nulla sodales a massa varius auctor. Sed cursus nisl vel urna viverra, eu molestie felis maximus.</p>\n', 1, NULL, NULL, 'qcm', 1, '{\"image\":\"\",\"video\":\"790d02467f0a7ed92916a679f1e9b81b461f2061.mp4\",\"msg_ok\":\"Your reply is good !\",\"msg_nok\":\"Your reply is not good...\",\"answer_type\":\"1\",\"position\":\"0\"}'),
(2, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tellus a augue lacinia hendrerit. Maecenas at velit a metus scelerisque tristique. Morbi erat nunc, sollicitudin ac velit ac, maximus elementum nunc. Sed iaculis ligula eget diam aliquet, at condimentum lectus sodales. Praesent consequat, est ut feugiat mollis, quam sapien venenatis tortor, ac molestie est magna et nisl. Vivamus volutpat mi sit amet tempor porta. Fusce nec elit nibh. Praesent blandit nulla in nunc volutpat posuere. Suspendisse malesuada diam at massa porttitor, at facilisis nibh pharetra. Quisque vel ornare purus, eu mollis enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla efficitur nunc et massa viverra porta. Nulla sodales a massa varius auctor. Sed cursus nisl vel urna viverra, eu molestie felis maximus.</p>\n', 1, NULL, NULL, 'text_to_fill', 1, '{\"image\":\"\",\"video\":\"https:\\/\\/vimeo.com\\/242977011\",\"msg_ok\":\"Your reply is good !\",\"msg_nok\":\"Your reply is not good...\",\"answer_type\":\"1\",\"position\":\"0\"}'),
(3, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tellus a augue lacinia hendrerit. Maecenas at velit a metus scelerisque tristique. Morbi erat nunc, sollicitudin ac velit ac, maximus elementum nunc. Sed iaculis ligula eget diam aliquet, at condimentum lectus sodales. Praesent consequat, est ut feugiat mollis, quam sapien venenatis tortor, ac molestie est magna et nisl. Vivamus volutpat mi sit amet tempor porta. Fusce nec elit nibh. Praesent blandit nulla in nunc volutpat posuere. Suspendisse malesuada diam at massa porttitor, at facilisis nibh pharetra. Quisque vel ornare purus, eu mollis enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla efficitur nunc et massa viverra porta. Nulla sodales a massa varius auctor. Sed cursus nisl vel urna viverra, eu molestie felis maximus.</p>\n', 1, NULL, NULL, 'category', 1, '{\"image\":\"\",\"video\":\"\",\"msg_ok\":\"Your reply is good !\",\"msg_nok\":\"Your reply is not good...\",\"answer_type\":\"1\",\"position\":\"0\"}'),
(4, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tellus a augue lacinia hendrerit. Maecenas at velit a metus scelerisque tristique. Morbi erat nunc, sollicitudin ac velit ac, maximus elementum nunc. Sed iaculis ligula eget diam aliquet, at condimentum lectus sodales. Praesent consequat, est ut feugiat mollis, quam sapien venenatis tortor, ac molestie est magna et nisl. Vivamus volutpat mi sit amet tempor porta. Fusce nec elit nibh. Praesent blandit nulla in nunc volutpat posuere. Suspendisse malesuada diam at massa porttitor, at facilisis nibh pharetra. Quisque vel ornare purus, eu mollis enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla efficitur nunc et massa viverra porta. Nulla sodales a massa varius auctor. Sed cursus nisl vel urna viverra, eu molestie felis maximus.</p>\n', 1, NULL, NULL, 'qcu', 1, '{\"image\":\"\",\"video\":\"\",\"msg_ok\":\"Your reply is good !\",\"msg_nok\":\"Your reply is not good...\",\"answer_type\":\"1\",\"position\":\"0\"}'),
(5, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tellus a augue lacinia hendrerit. Maecenas at velit a metus scelerisque tristique. Morbi erat nunc, sollicitudin ac velit ac, maximus elementum nunc. Sed iaculis ligula eget diam aliquet, at condimentum lectus sodales. Praesent consequat, est ut feugiat mollis, quam sapien venenatis tortor, ac molestie est magna et nisl. Vivamus volutpat mi sit amet tempor porta. Fusce nec elit nibh. Praesent blandit nulla in nunc volutpat posuere. Suspendisse malesuada diam at massa porttitor, at facilisis nibh pharetra. Quisque vel ornare purus, eu mollis enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla efficitur nunc et massa viverra porta. Nulla sodales a massa varius auctor. Sed cursus nisl vel urna viverra, eu molestie felis maximus.</p>\n', 1, NULL, NULL, 'drag_and_drop', 1, '{\"image\":\"8d90bfc21aee1cc31c224eba371bc73453c6b0bc.jpeg\",\"video\":\"\",\"msg_ok\":\"Your reply is good !\",\"msg_nok\":\"Your reply is not good...\",\"answer_type\":\"1\",\"position\":\"0\"}');
"

INSERT INTO `quiz_steps` (`id`, `title`, `description`, `scores`, `score_to_pass`, `limit_attempt`, `limit_users`, `ordering`, `status`, `params`) VALUES
(1, 'step-1', NULL, NULL, NULL, NULL, NULL, 0, 1, '{\"msg_ok\":\"You validate this step with success !\",\"msg_nok\":\"You failed this step...\",\"questions_to_show\":\"0\",\"position\":\"0\"}'),
(2, 'step-6', NULL, NULL, NULL, NULL, NULL, 0, 1, '{\"msg_ok\":\"You validate this step with success !\",\"msg_nok\":\"You failed this step...\",\"questions_to_show\":\"0\",\"position\":\"5\"}'),
(3, 'step-5', NULL, NULL, NULL, NULL, NULL, 0, 1, '{\"msg_ok\":\"You validate this step with success !\",\"msg_nok\":\"You failed this step...\",\"questions_to_show\":\"0\",\"position\":\"4\"}'),
(4, 'step-2', NULL, NULL, NULL, NULL, NULL, 0, 1, '{\"msg_ok\":\"You validate this step with success !\",\"msg_nok\":\"You failed this step...\",\"questions_to_show\":\"0\",\"position\":\"1\"}'),
(5, 'step-3', NULL, NULL, NULL, NULL, NULL, 0, 1, '{\"msg_ok\":\"You validate this step with success !\",\"msg_nok\":\"You failed this step...\",\"questions_to_show\":\"0\",\"position\":\"2\"}');
"

INSERT INTO `quiz_questions_xref` (`id`, `quiz_id`, `step_id`, `question_id`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 2),
(3, 1, 3, 3),
(4, 1, 4, 4),
(5, 1, 5, 5);
"*/
        
    }

    public function down(Schema $schema): void {
        // Delete new tables and foreign keys
        $this->addSql("ALTER TABLE `module` DROP `type_quiz`, DROP `duration_active`;
        ALTER TABLE quiz_users_played DROP FOREIGN KEY FK_B5D3C45FAFC2B591;
        ALTER TABLE quiz_users_played DROP FOREIGN KEY FK_B5D3C45F853CD175;
        ALTER TABLE quiz_users_answers DROP FOREIGN KEY FK_8A6960DD25576DBD;
        ALTER TABLE quiz_users_answers DROP FOREIGN KEY FK_8A6960DD1E27F6BF;
        ALTER TABLE quiz_questions_items DROP FOREIGN KEY FK_5F62C3601E27F6BF;
        ALTER TABLE quiz_questions_xref DROP FOREIGN KEY FK_13DB8DAC853CD175;
        ALTER TABLE quiz_questions_xref DROP FOREIGN KEY FK_13DB8DAC73B21E9C;
        ALTER TABLE quiz_questions_xref DROP FOREIGN KEY FK_13DB8DAC1E27F6BF;

        ALTER TABLE module DROP FOREIGN KEY FK_C242628853CD175;
        DROP TABLE quizes;
        DROP TABLE quiz_questions;
        DROP TABLE quiz_questions_xref;
        DROP TABLE quiz_steps;
        DROP TABLE quiz_questions_items;
        DROP TABLE quiz_users_played;
        DROP TABLE quiz_users_answers;


        // Re-add previous tables
        
--
-- Structure de la table `monkey_quiz_settings`
--

CREATE TABLE `monkey_quiz_settings` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `survey_monkey_id` int(11) DEFAULT NULL,
  `preview_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `survey_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edit_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `collect_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analyze_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `native_quiz_answer`
--

CREATE TABLE `native_quiz_answer` (
  `id` int(11) NOT NULL,
  `questions_id` int(11) DEFAULT NULL,
  `answer` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `native_quiz_answer`
--

INSERT INTO `native_quiz_answer` (`id`, `questions_id`, `answer`, `points`, `created`, `updated`) VALUES
(34, 10, 'Rendre vos formations plus efficaces', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(35, 10, 'Dynamiser vos formations', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(36, 10, 'Entraîner vos collaborateurs ', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(37, 10, 'Transformer/Digitaliser vos formations', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(38, 10, 'Diffuser facilement des modules à distance', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(39, 10, 'Organiser et délivrer des sessions individuelles de coaching', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(40, 10, 'Permettre à vos collaborateurs de se former Anywhere, Any time, Any device', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(41, 11, '1', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(42, 11, '2', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(43, 11, '3', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(44, 11, '4', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(45, 11, '5', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(46, 11, '6', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(47, 11, '7', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(48, 11, '8', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(49, 11, '9', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(50, 11, '10', 10, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(51, 12, 'OUI', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(52, 12, 'NON', 0, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(53, 13, 'OUI', 0, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(54, 13, 'NON', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(55, 14, 'OUI', 0, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(56, 14, 'NON', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(57, 15, 'Un mail client', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(58, 15, 'Un message vocal client', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(59, 15, 'L’appel d’un débiteur', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(60, 15, 'Un appel client entrant', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(61, 16, 'Se faire plaisir', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(62, 16, 'S\'adapter et survivre dans un environnement mouvant', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(63, 16, 'Faire plaisir à ses collaborateurs', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(64, 17, 'feedback d\'évaluation', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(65, 17, 'feedback constructif', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(66, 17, 'feedback d\'encouragement ', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(67, 17, 'Il existe qu\'une sorte de feedback', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(68, 18, 'C\'est que je vois de moi et que les autres ne voient pas', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(69, 18, 'C\'est ce que je ne vois pas de moi mais que les autres voient', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(70, 18, 'C\'est ce que je ne pouvais pas voir dans mon rétroviseur', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(71, 19, 'pour la réussite des salariés', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(72, 19, 'il contribue à la performance', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(73, 19, 'il permet un meilleur engagement des personnes ', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(74, 19, 'il contribue à l\'évolution des organisations et du management ', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(75, 20, 'développer les situations de feedback informelles ', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(76, 20, 'mettre l\'accent sur la demande de feedback', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(77, 20, 'construire sur les forces des personnes', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(78, 20, 'construire des plans de développement avec de nombreux axes d\'amélioration', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(79, 21, 'commencer par tous les points négatifs', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(80, 21, 'donner mes opinions ', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(81, 21, 'être factuel et précis', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(82, 21, 'faire participer l\'interlocuteur au feedback', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(83, 21, 'donner le liste à la Prévert des points forts et des axes d\'amélioration', 0, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(84, 22, 'A', 0, '2019-10-30 11:02:19', '2019-10-30 11:02:19'),
(85, 22, 'B', 0, '2019-10-30 11:02:19', '2019-10-30 11:02:19'),
(86, 23, 'Yes', 100, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(87, 23, 'non', 0, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(88, 24, 'Yes', 100, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(89, 24, 'Non', 0, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(90, 25, 'Ok', 20, '2019-11-21 21:24:57', '2019-11-21 21:24:57'),
(91, 25, 'Bad', 30, '2019-11-21 21:24:57', '2019-11-21 21:24:57'),
(92, 26, 'Ok', 35, '2019-11-21 21:24:57', '2019-11-21 21:24:57'),
(93, 26, 'Bad', 0, '2019-11-21 21:24:57', '2019-11-21 21:24:57');

-- --------------------------------------------------------

--
-- Structure de la table `native_quiz_question`
--

CREATE TABLE `native_quiz_question` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `text` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `native_quiz_question`
--

INSERT INTO `native_quiz_question` (`id`, `quiz_id`, `text`, `type`, `points`, `created`, `updated`) VALUES
(10, 4, 'Qu’imaginez-vous faire avec la plateforme Match 3.0 ?', 'multi_answer', 70, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(11, 4, 'Quelle est votre évaluation de l’application Match 3.0 ?', 'multi_answer', 100, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(12, 5, 'Le rebond et la VAD peuvent se pratiquer à chaque contact client?', 'single_answer', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(13, 5, 'Le rebond à chaud est à privilégier au rebond à froid?', 'single_answer', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(14, 5, 'L’objectif d’un rebond est une VAD immédiate?', 'single_answer', 25, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(15, 5, 'Quels sont les motifs propices au Rebond commercial?', 'multi_answer', 100, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(16, 6, 'à qui sert le feedback dans une entité ?', 'multi_answer', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(17, 6, 'Parmi les choix proposés, quels sont les sortes feedbacks connues ?', 'multi_answer', 300, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(18, 6, 'Concernant la pratique du feedback, comment définissez-vous votre angle mort  ?', 'multi_answer', 100, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(19, 6, 'Pourquoi mettre en place le feedback ?', 'multi_answer', 400, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(20, 6, 'Quelles sont les bonnes pratiques pour instaurer la pratique du feedback ?', 'multi_answer', 300, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(21, 6, 'Quand je donne un feedback de qualité, je dois :', 'multi_answer', 200, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(22, 8, '<p>TEST CREATION QUIZ</p>\r\n', 'single_answer', 0, '2019-10-30 11:02:19', '2019-10-30 11:02:19'),
(23, 9, '<p><img src=\"https://tse2.mm.bing.net/th?id=OIP.E0wVid9wssEZ__OfZjH9JAHaH_&amp;pid=Api&amp;P=0&amp;w=300&amp;h=300\" /></p>\r\n', 'multi_answer', 100, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(24, 9, '<p><a href=\"https://www.youtube.com/watch?v=OIjKTLZAbHE\">https://www.youtube.com/watch?v=OIjKTLZAbHE</a></p>\r\n', 'multi_answer', 100, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(25, 10, '<p>Test 1</p>\r\n', 'multi_answer', 50, '2019-11-21 21:24:57', '2019-11-21 21:24:57'),
(26, 10, '<p>Test 2</p>\r\n', 'single_answer', 35, '2019-11-21 21:24:57', '2019-11-21 21:24:57');

-- --------------------------------------------------------

--
-- Structure de la table `native_quiz_result`
--

CREATE TABLE `native_quiz_result` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `answers` longtext COLLATE utf8_unicode_ci,
  `passed` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `native_quiz_result`
--

INSERT INTO `native_quiz_result` (`id`, `quiz_id`, `module_id`, `person_id`, `date`, `score`, `answers`, `passed`, `created`, `updated`) VALUES
(11, 4, 243, 22, '2019-11-04 17:25:28', 160, '{\"10\":[\"34\",\"35\",\"36\",\"37\",\"38\",\"39\",\"40\"],\"11\":[\"41\",\"42\",\"44\",\"45\",\"46\",\"47\",\"48\",\"49\",\"50\"]}', 1, '2019-11-04 17:25:28', '2019-11-04 17:25:28'),
(12, 4, 243, 15, '2019-11-04 17:26:49', 70, '{\"10\":[\"34\",\"35\",\"36\",\"37\",\"38\",\"39\"],\"11\":[\"48\"]}', 1, '2019-11-04 17:26:49', '2019-11-04 17:26:49'),
(13, 4, 243, 15, '2019-11-04 17:27:02', 80, '{\"10\":[\"34\",\"35\",\"36\",\"37\",\"38\",\"39\",\"40\"],\"11\":[\"50\"]}', 1, '2019-11-04 17:27:02', '2019-11-04 17:27:02'),
(14, 4, 243, 15, '2019-11-04 17:27:28', 40, '{\"10\":[\"34\",\"35\",\"36\"],\"11\":[\"46\"]}', 1, '2019-11-04 17:27:28', '2019-11-04 17:27:28'),
(15, 4, 260, 29, '2019-11-05 02:28:23', 30, '{\"10\":[\"38\",\"39\"],\"11\":[\"44\"]}', 1, '2019-11-05 02:28:23', '2019-11-05 02:28:23'),
(16, 4, 260, 29, '2019-11-05 03:04:17', 30, '{\"10\":[\"37\",\"38\"],\"11\":[\"44\"]}', 1, '2019-11-05 03:04:17', '2019-11-05 03:04:17'),
(17, 4, 260, 29, '2019-11-05 05:05:29', 30, '{\"10\":[\"36\",\"37\"],\"11\":[\"45\"]}', 1, '2019-11-05 05:05:29', '2019-11-05 05:05:29'),
(18, 4, 260, 29, '2019-11-05 05:28:33', 30, '{\"10\":[\"37\",\"38\"],\"11\":[\"46\"]}', 1, '2019-11-05 05:28:33', '2019-11-05 05:28:33'),
(19, 4, 260, 29, '2019-11-05 06:29:16', 30, '{\"10\":[\"36\",\"37\"],\"11\":[\"42\"]}', 1, '2019-11-05 06:29:16', '2019-11-05 06:29:16'),
(20, 4, 260, 29, '2019-11-05 07:23:33', 30, '{\"10\":[\"37\",\"38\"],\"11\":[\"47\"]}', 1, '2019-11-05 07:23:33', '2019-11-05 07:23:33'),
(21, 4, 260, 29, '2019-11-05 07:54:54', 30, '{\"10\":[\"34\",\"36\"],\"11\":[\"49\"]}', 1, '2019-11-05 07:54:54', '2019-11-05 07:54:54'),
(22, 4, 260, 29, '2019-11-05 09:12:07', 20, '{\"10\":[\"36\"],\"11\":[\"42\"]}', 1, '2019-11-05 09:12:07', '2019-11-05 09:12:07'),
(24, 4, 260, 29, '2019-11-06 02:22:03', 20, '{\"10\":[\"35\"],\"11\":[\"42\"]}', 1, '2019-11-06 02:22:03', '2019-11-06 02:22:03'),
(25, 4, 260, 29, '2019-11-06 04:24:24', 30, '{\"10\":[\"37\",\"38\"],\"11\":[\"45\"]}', 1, '2019-11-06 04:24:24', '2019-11-06 04:24:24'),
(26, 9, 278, 29, '2019-11-07 11:56:30', 100, '{\"23\":[\"86\"],\"24\":[\"89\"]}', 1, '2019-11-07 11:56:30', '2019-11-07 11:56:30'),
(27, 4, 283, 42, '2019-11-20 22:19:06', 40, '{\"10\":[\"36\",\"37\"],\"11\":[\"41\",\"42\"]}', 1, '2019-11-20 22:19:06', '2019-11-20 22:19:06');

-- --------------------------------------------------------

--
-- Structure de la table `native_quiz_settings`
--

CREATE TABLE `native_quiz_settings` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `quizable` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_passing_score` int(11) NOT NULL,
  `passing_message` varchar(1023) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failing_message` varchar(1023) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `native_quiz_settings`
--

INSERT INTO `native_quiz_settings` (`id`, `quiz_id`, `published`, `quizable`, `minimum_passing_score`, `passing_message`, `failing_message`, `created`, `updated`) VALUES
(4, 4, 1, 1, 0, '', '', '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(5, 5, 1, 1, 0, '', '', '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(6, 6, 1, 1, 0, '', '', '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(7, 8, 1, 1, 0, '', '', '2019-10-30 11:02:19', '2019-10-30 11:02:19'),
(8, 9, 1, 1, 0, '', '', '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(9, 10, 1, 1, 0, '', '', '2019-11-21 21:24:57', '2019-11-21 21:24:57');

-- --------------------------------------------------------

--
-- Structure de la table `quiz`
--

CREATE TABLE `quiz` (
  `quiz_id` int(11) NOT NULL,
  `title` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `question_count` int(11) NOT NULL,
  `quizable` tinyint(1) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SURVEY_MONKEY',
  `total_points` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `title`, `create_date`, `question_count`, `quizable`, `type`, `total_points`, `created`, `updated`) VALUES
(4, 'Que pensez-vous de Match 3.0 ?', '2019-07-12', 2, 1, 'NATIVE', 170, '2019-07-12 08:19:00', '2019-07-12 08:19:00'),
(5, 'Quiz de validation ', '2019-07-12', 4, 1, 'NATIVE', 175, '2019-07-12 08:29:13', '2019-07-12 08:29:13'),
(6, 'Quiz de validation feedback', '2019-07-29', 6, 1, 'NATIVE', 1400, '2019-07-29 10:17:05', '2019-07-29 10:17:05'),
(8, 'TEST CREATION QUIZ', '2019-10-30', 1, 1, 'NATIVE', 0, '2019-10-30 11:02:19', '2019-10-30 11:02:19'),
(9, 'TEST IMAGE et video', '2019-11-07', 2, 1, 'NATIVE', 200, '2019-11-07 11:54:48', '2019-11-07 11:54:48'),
(10, 'Test Adrien', '2019-11-21', 2, 1, 'NATIVE', 85, '2019-11-21 21:24:57', '2019-11-21 21:24:57');

-- --------------------------------------------------------

--
-- Structure de la table `quiz_api_log`
--

CREATE TABLE `quiz_api_log` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `url` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `monkey_quiz_settings`
--
ALTER TABLE `monkey_quiz_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_F8460539853CD175` (`quiz_id`);

--
-- Index pour la table `native_quiz_answer`
--
ALTER TABLE `native_quiz_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_41257B98BCB134CE` (`questions_id`);

--
-- Index pour la table `native_quiz_question`
--
ALTER TABLE `native_quiz_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6420279853CD175` (`quiz_id`);

--
-- Index pour la table `native_quiz_result`
--
ALTER TABLE `native_quiz_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8892F0AE853CD175` (`quiz_id`),
  ADD KEY `IDX_8892F0AEAFC2B591` (`module_id`),
  ADD KEY `IDX_8892F0AE217BBB47` (`person_id`);

--
-- Index pour la table `native_quiz_settings`
--
ALTER TABLE `native_quiz_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E5F0EBF2853CD175` (`quiz_id`);

--
-- Index pour la table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quiz_id`);

--
-- Index pour la table `quiz_api_log`
--
ALTER TABLE `quiz_api_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DEE78351853CD175` (`quiz_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `monkey_quiz_settings`
--
ALTER TABLE `monkey_quiz_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `native_quiz_answer`
--
ALTER TABLE `native_quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT pour la table `native_quiz_question`
--
ALTER TABLE `native_quiz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `native_quiz_result`
--
ALTER TABLE `native_quiz_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `native_quiz_settings`
--
ALTER TABLE `native_quiz_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quiz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `quiz_api_log`
--
ALTER TABLE `quiz_api_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `monkey_quiz_settings`
--
ALTER TABLE `monkey_quiz_settings`
  ADD CONSTRAINT `FK_F8460539853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);

--
-- Contraintes pour la table `native_quiz_answer`
--
ALTER TABLE `native_quiz_answer`
  ADD CONSTRAINT `FK_41257B98BCB134CE` FOREIGN KEY (`questions_id`) REFERENCES `native_quiz_question` (`id`);

--
-- Contraintes pour la table `native_quiz_question`
--
ALTER TABLE `native_quiz_question`
  ADD CONSTRAINT `FK_B6420279853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);

--
-- Contraintes pour la table `native_quiz_result`
--
ALTER TABLE `native_quiz_result`
  ADD CONSTRAINT `FK_8892F0AE217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `FK_8892F0AE853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`),
  ADD CONSTRAINT `FK_8892F0AEAFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`);

--
-- Contraintes pour la table `native_quiz_settings`
--
ALTER TABLE `native_quiz_settings`
  ADD CONSTRAINT `FK_E5F0EBF2853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);

--
-- Contraintes pour la table `quiz_api_log`
--
ALTER TABLE `quiz_api_log`
  ADD CONSTRAINT `FK_DEE78351853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);
  

--
-- Contraintes pour la table `quiz_api_log`
--

UPDATE `module` SET `quiz_id` = NULL WHERE `quiz_id` > 0;

ALTER TABLE `module`
  ADD CONSTRAINT `FK_C242628853CD175` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);
");
    }

}
