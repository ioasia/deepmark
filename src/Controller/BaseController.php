<?php

namespace Controller;

use ApiBundle\Entity\ProfileVariety;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Traits\Controller\HasAjax;
use Traits\Controller\HasDownload;
use Traits\Controller\HasNavigator;
use Traits\Controller\HasUsers;

/**
 * Class BaseAdminController
 * @package ApiBundle\Controller
 */
class BaseController extends Controller
{
    use HasNavigator;
    use HasDownload;
    use HasAjax;
    use HasUsers;

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function getName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @param int $type
     * @return ProfileVariety|object|null
     */
    protected function getProfileVariety(int $type)
    {
        return $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(array('appId' => $type));
    }

    protected function getProfileFromVariety(ProfileVariety $profileType)
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array('profileType' => $profileType,));
    }
}
