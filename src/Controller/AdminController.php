<?php

namespace Controller;

use ApiBundle\Entity\BookingAgenda;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ListAdminController
 * @package ApiBundle\Controller
 */
abstract class AdminController extends BaseController
{
    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $actions = [];

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        return $this->render('list/index.html.twig', [
            'list' => $this->getData(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()

        ]);
    }

    abstract protected function getData();

    abstract protected function loadNavItems();
}
