<?php


namespace Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface AdminControllerInterface
 * @package Controller
 */
interface AdminControllerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request);
}
