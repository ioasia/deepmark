<?php

namespace TrainerBundle\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BadRequest extends BadRequestHttpException
{
}
