<?php

namespace TrainerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Profile;
use Traits\Controller\HasBestPractice;

/**
 * BestPracticesController controller.
 */
class BestPracticesController extends Controller
{
    use HasBestPractice;

    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    public function indexAction(Request $request)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $profile->getEntity()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }
        // get tags
        $searchParam['tags'] = [];
        $tags         = $this->container->get('doctrine')->getRepository('ApiBundle:Tag')->findAll();
        foreach ($tags as $tag){
            $searchParam['tags'][$tag->getId()] = $tag->getTagName();
        }


        return $this->render('TrainerBundle:bestPractices:index.html.twig', array(
            'searchParam' => $searchParam
        ));
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id)
    {
        $result = $this->getBestPractice($id, $this->getCurrentUser());
        return $this->render('molecules/best-practices/view.html.twig', array(
            'bestPractice'          => $result['bestPractice'],
            'resourcePath'          => $result['resourcePath'],
            'others'                => $result['others'],
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser()),
            'isReview'              => $result['isReview']
        ));
    }
}
