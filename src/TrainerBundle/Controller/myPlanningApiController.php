<?php

namespace TrainerBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\WorkingHours;
use AppBundle\Entity\Person;
use AppBundle\Service\CalendarApi;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Utils\Email;
use Traits\IsAvailable;

class myPlanningApiController extends RestController
{
    use IsAvailable;
    /**
     * @Rest\Get("calendars")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     *
     * @throws Exception
     * @Rest\QueryParam(name="dateStart", nullable=true, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=true, description="Date End")
     */
    public function getAvailabilitiesTrainerAction(ParamFetcher $paramFetcher)
    {
        $now = new DateTime();
        if ($paramFetcher->get('dateStart')) {
            $dateStart = new DateTime($paramFetcher->get('dateStart'));
        } else {
            // first day of current month
            $dateStart = clone $now;
            $dateStart = $dateStart->modify('first day of this month');
        }
        if ($paramFetcher->get('dateEnd')) {
            $dateEnd = new DateTime($paramFetcher->get('dateEnd'));
        } else {
            // last day of month
            $dateEnd = clone $now;
            $dateEnd = $dateEnd->modify('last day of this month');
        }

        $calendarApi = $this->get(CalendarApi::class);

        $person  = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        if (!$profile) {
            return $this->errorHandler();
        }

        $calendar_array = $calendarApi->getBookingsByTrainer($profile, $dateStart, $dateEnd);
        $calendar_array = array_merge($calendar_array,
            $calendarApi->getAvailabilityByTrainer($profile, $dateStart, $dateEnd));

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="startTimeAfternoon", nullable=true, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="endTimeAfternoon", nullable=true, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="startTime", nullable=true, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="endTime", nullable=true, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function postCalendarsAction(ParamFetcher $paramFetcher)
    {
        /** @var Person $user */
        $user = $this->getUser();

        $beginning = new DateTime($paramFetcher->get('beginning'), $user->getDateTimeZone());
        $ending    = new DateTime($paramFetcher->get('ending'), $user->getDateTimeZone());

        $startTime = $paramFetcher->get('startTime');
        $endTime   = $paramFetcher->get('endTimeAfternoon');

        $freezeTime = $paramFetcher->get('endTime') . '|' . $paramFetcher->get('startTimeAfternoon');

        $profileId = $paramFetcher->get('profileId') ?: null;

        if ($profileId) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'id' => $profileId,
            ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'person' => $user->getId(),
            ));
        }

        $availableCalendarExist = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($profile, $beginning, $ending);

        if ($availableCalendarExist) {
            return $this->alreadyExist('availability already exist');
        }

        $period = new DatePeriod(
            $beginning,
            DateInterval::createFromDateString('1 day'),
            $ending
        );

        $availableCalendars = [];

        foreach ($period as $value) {
            /** @var DateTime $dayBegin */
            $dayBegin = clone $value;
            list($hours, $minutes) = explode(':', $startTime);

            $dayBegin->add(DateInterval::createFromDateString("$hours hours $minutes minutes"));

            /** @var DateTime $dayEnd */
            $dayEnd = clone $value;
            list($hours, $minutes) = explode(':', $endTime);

            $dayEnd->add(DateInterval::createFromDateString("$hours hours $minutes minutes"));

            $availableCalendar = new AvailableCalendar();
            $availableCalendar->setBeginning($dayBegin);
            $availableCalendar->setEnding($dayEnd);
            $availableCalendar->setFreezeTime($freezeTime);
            $availableCalendar->setProfile($profile);

            $availableCalendars[] = $availableCalendar;
        }

        $view       = View::create();
        $returnData = [];
        if (count($availableCalendars) > 0) {
            $em = $this->getDoctrine()->getManager();
            foreach ($availableCalendars as $availableCalendar) {
                $em->persist($availableCalendar);
                $em->flush();

                array_push($returnData, array(
                    'id'          => $availableCalendar->getId(),
                    'title'       => '',
                    'start'       => $availableCalendar->getBeginning(),
                    'end'         => $availableCalendar->getEnding(),
                    'type'        => '',
                    'displayType' => 'availability',
                    'duration'    => 0,
                    'with'        => '',
                    'withLink'    => '#',
                ));
            }
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de l\'ajout de votre disponibilité, veuillez réessayer', []);
        }

        return $this->handleView($view);
    }

    /**
     * DELETE AvailableCalendar.
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning avaibility.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="profileId", nullable=true, strict=true, description="Profile ID.")
     *
     * @return Response
     */
    public function deleteCalendarsAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $user      = $this->getUser();
        $profileId = $paramFetcher->get('profileId') ?: null;

        if ($profileId) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'id' => $profileId,
            ));
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'person' => $user->getId(),
            ));
        }

        $availablesCalendar = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($profile, $beginning, $ending);

        $view               = View::create();
        $errors             = $this->get('validator')->validate($profile);
        $messages           = [];
        if (0 == count($errors)) {
            $em = $this->getDoctrine()->getManager();
            foreach ($availablesCalendar as $availableCalendar) {
                if ($this->checkAvailabilityDelete($profile, $availableCalendar->getBeginning(), $availableCalendar->getEnding())) {
                    $em->remove($availableCalendar);
                } else {
                    $messages[]= $availableCalendar->getBeginning()->format('d-m-Y');
                }
            }
            $em->flush();
            $returnData = ['status' => $messages ? $translator->trans('global.available_calendar_warning_delete', ['%days%' => implode(", ", $messages)]) : $translator->trans('global.available_calendar_delete_success')];
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de la supression de vos disponibilités, veuillez réessayer', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @Rest\Post("calendars/custom")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning availability.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     *
     * @return Response
     */
    public function postCalendarsCustomAction(ParamFetcher $paramFetcher)
    {
        /** @var Person $user */
        $user = $this->getUser();
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $days      = $paramFetcher->get('days');

        $interval = DateInterval::createFromDateString('1 day');
        $ending->add($interval);

        $period = new DatePeriod($beginning, $interval, $ending);

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));

        $availableCalendars = [];
        foreach ($period as $dayCurrent) {
            $dayName = date('l', $dayCurrent->getTimeStamp());

            foreach ($days as $day) {
                $dayAdded = $day['dayName'];

                if ($dayAdded === $dayName) {
                    $freezeTime = null;
                    $dayBegin = clone $dayCurrent;
                    $dayEnd   = clone $dayCurrent;

                    if ((!$day['start'] || !$day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        continue;
                    } else if ((!$day['start'] || !$day['end']) && ($day['start_afternoon'] && $day['end_afternoon'])) {
                        $start              = explode(':', $day['start_afternoon']);
                        $end                = explode(':', $day['end_afternoon']);

                    } else if (($day['start'] && $day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end']);
                    } else {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end_afternoon']);
                        $freezeTime         = $day['end'] . '|' . $day['start_afternoon'];
                    }

                    $beginTime = $dayBegin->setTime($start[0], $start[1]);
                    $endTime   = $dayEnd->setTime($end[0], $end[1]);

                    // Check if exist already
                    $existCalendar = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
                        ->findAvailabilityByLiveResource($profile, $beginTime, $endTime);

                    if (!$existCalendar) {
                        $availableCalendar = new AvailableCalendar();
                        $availableCalendar->setBeginning($beginTime);
                        $availableCalendar->setEnding($endTime);
                        $availableCalendar->setFreezeTime($freezeTime);
                        $availableCalendar->setProfile($profile);
                        $availableCalendars[] = $availableCalendar;
                    }
                }
            }
        }

        $view       = View::create();
        $returnData = [];
        if (count($availableCalendars) > 0) {
            $em = $this->getDoctrine()->getManager();
            foreach ($availableCalendars as $availableCalendar) {
                $em->persist($availableCalendar);
                $em->flush();

                array_push($returnData, array(
                    'id'          => $availableCalendar->getId(),
                    'title'       => '',
                    'start'       => $availableCalendar->getBeginning(),
                    'end'         => $availableCalendar->getEnding(),
                    'type'        => '',
                    'displayType' => 'availability',
                    'duration'    => 0,
                    'with'        => '',
                    'withLink'    => '#',
                ));
            }
            $view->setData($returnData);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de l\'ajout de votre disponibilité, veuillez réessayer', []);
        }

        return $this->handleView($view);
    }

    /**
     * POST AvailableCalendar.
     *
     * @Rest\Post("delete/custom")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="beginning", nullable=false, strict=true, description="Beginning availability.")
     * @Rest\RequestParam(name="ending", nullable=false, strict=true, description="Ending availability.")
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     *
     * @return Response
     */
    public function postDeleteCustomAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        /** @var Person $user */
        $user = $this->getUser();
        $beginning = new DateTime($paramFetcher->get('beginning'));
        $ending    = new DateTime($paramFetcher->get('ending'));
        $days      = $paramFetcher->get('days');

        $interval = DateInterval::createFromDateString('1 day');
        $ending->add($interval);

        $period = new DatePeriod($beginning, $interval, $ending);

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));
        $messages           = [];
        foreach ($period as $dayCurrent) {
            $dayName = date('l', $dayCurrent->getTimeStamp());

            foreach ($days as $day) {
                $dayAdded = $day['dayName'];

                if ($dayAdded === $dayName) {
                    $dayBegin = clone $dayCurrent;
                    $dayEnd   = clone $dayCurrent;

                    if ((!$day['start'] || !$day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        continue;
                    } else if ((!$day['start'] || !$day['end']) && ($day['start_afternoon'] && $day['end_afternoon'])) {
                        $start              = explode(':', $day['start_afternoon']);
                        $end                = explode(':', $day['end_afternoon']);

                    } else if (($day['start'] && $day['end']) && (!$day['start_afternoon'] || !$day['end_afternoon'])) {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end']);
                    } else {
                        $start              = explode(':', $day['start']);
                        $end                = explode(':', $day['end_afternoon']);
                    }

                    $beginTime = $dayBegin->setTime($start[0], $start[1]);
                    $endTime   = $dayEnd->setTime($end[0], $end[1]);

                    // Check if exist already
                    $existCalendars = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
                        ->findAvailabilityByLiveResource($profile, $beginTime, $endTime);

                    if ($existCalendars) {
                        foreach ($existCalendars as $existCalendar) {
                            if ($this->checkAvailabilityDelete($profile, $existCalendar->getBeginning(), $existCalendar->getEnding())) {
                                $em->remove($existCalendar);
                            } else {
                                $messages[]= $existCalendar->getBeginning()->format('d-m-Y');
                            }
                        }
                        $em->flush();
                    }
                }
            }
        }

        $view       = View::create();
        $returnData = ['status' => $messages ? $translator->trans('global.available_calendar_warning_delete', ['%days%' => implode(", ", $messages)]) : $translator->trans('global.available_calendar_delete_success')];
        $view->setData($returnData);
        $view->setStatusCode(200);
        return $this->handleView($view);
    }

    /**
     * POST workingHours.
     *
     * @Rest\Post("working/hours")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="days", nullable=true, strict=true, description="days.")
     * @Rest\RequestParam(name="lunchStartTime", nullable=true, strict=true, description="lunchStartTime.")
     * @Rest\RequestParam(name="lunchEndTime", nullable=true, strict=true, description="lunchEndTime.")
     * @return Response
     */
    public function postWorkingHoursAction(ParamFetcher $paramFetcher)
    {
        $now     = new DateTime('now');
        $em      = $this->getDoctrine()->getManager();
        $days    = $paramFetcher->get('days');
        $user    = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $user->getId(),
        ));

        if ($this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findOneBy(['profile' => $profile])) {
            $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
                ->deleteWorkingHoursByProfile($profile);
        }

        if ($paramFetcher->get('lunchStartTime') && $paramFetcher->get('lunchEndTime')) {
            $now            = new DateTime('now');
            $lunchStartTime = explode(":", $paramFetcher->get('lunchStartTime'));
            $lunchEndTime   = explode(":", $paramFetcher->get('lunchEndTime'));
            $profile->setLunchStartTime(clone $now->setTime($lunchStartTime[0], $lunchStartTime[1]));
            $profile->setLunchEndTime(clone $now->setTime($lunchEndTime[0], $lunchEndTime[1]));
        }

        if ($days) {
            foreach ($days as $day) {
                $working = new WorkingHours();
                $working->setName($day['dayName']);

                if ($day['start'] && $day['end']) {
                    $startTime = explode(":", $day['start']);
                    $endTime   = explode(":", $day['end']);
                    $working->setStart(clone $now->setTime($startTime[0], $startTime[1]));
                    $working->setEnd(clone $now->setTime($endTime[0], $endTime[1]));
                }

                $working->setProfile($profile);
                $em->persist($working);
                $em->flush();
            }
        }

        $view = View::create();
        $view->setData('OK');
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * POST requestReplacement.
     *
     * @Rest\Post("request/replacement")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="bookingId", nullable=true, strict=true, description="booking Id.")
     *
     * @return Response
     * @throws Exception
     */
    public function postRequestReplacementAction(ParamFetcher $paramFetcher)
    {
        $bookingId     = $paramFetcher->get('bookingId');
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->find($bookingId);

        $requestReplacement = $this->getDoctrine()->getRepository('ApiBundle:RequestReplacementResource')->findBy(['bookingAgenda' => $bookingAgenda]);

        if (!$requestReplacement) {
            $em            = $this->getDoctrine()->getManager();
            $requestAssign = new RequestReplacementResource();
            $requestAssign->setBookingAgenda($bookingAgenda);
            $requestAssign->setDateAdd(new DateTime());
            $requestAssign->setStatus(RequestReplacementResource::STATUS_UNKNOWN);
            $em->persist($requestAssign);
            $em->flush();

            $this->get(Email::class)->sendMailReplacementPending($bookingAgenda);
        }

        $view = View::create();
        $view->setData('OK');
        $view->setStatusCode(200);

        return $this->handleView($view);
    }
}
