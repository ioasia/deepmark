<?php

namespace TrainerBundle\Controller;

use ApiBundle\Controller\RestController;
use AppBundle\Service\CalendarApi;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;

class DashboardApiController extends RestController
{
    /**
     * @Rest\Get("calendars")
     * GET calendar
     * Display my bookings for an intervention
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @Rest\QueryParam(name="dateEnd", nullable=false, description="Date End")
     * @Rest\QueryParam(name="typeModule", nullable=false, description="Module Type")
     */
    public function getCalendarsTrainerAction(ParamFetcher $paramFetcher)
    {
        $dateStart = new \DateTime($paramFetcher->get('dateStart'));
        $dateEnd = new \DateTime($paramFetcher->get('dateEnd'));
        $moduleType = ($paramFetcher->get('typeModule') &&  $paramFetcher->get('typeModule') != 0)  ? $this->getDoctrine()->getRepository('ApiBundle:StoredModule')->findOneBy(['appId' =>  $paramFetcher->get('typeModule')]) : null;
        $calendarApi = $this->get(CalendarApi::class);
        $calendar_array= [];
        $person = $this->getUser();
        $roles   = $person->getRoles();

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        if (!$profile) {
            return $this->errorHandler();
        }
        $calendar_array = array_merge($calendar_array, $calendarApi->getBookingsByTrainer(in_array('ROLE_ADMIN', $roles) ? null : $profile, $dateStart, $dateEnd, in_array('ROLE_ADMIN', $roles) ? $moduleType : null));

        $view = View::create();

        $view->setData($calendar_array);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("dayActivities")
     * @Rest\QueryParam(name="day", description="Day to search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDayActivitiesTrainerAction(ParamFetcher $paramFetcher)
    {
        $date = new \DateTime($paramFetcher->get('day'));
        $person = $this->getUser();
        $roles   = $person->getRoles();
        if (in_array('ROLE_ADMIN', $roles)) {
            $listActivities = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBookingByProfileAndDate(null, $date);
        } else {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);

            $listActivities = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBookingByProfileAndDate($profile, $date);
        }

        $view = $this->renderView('TrainerBundle:Dashboard:trainer_activities.html.twig', [
            'bookings' => $listActivities
        ]);

        return new JsonResponse($view);
    }

    /**
     * @Rest\Post("activitiesByRange")
     * @Rest\RequestParam(name="startDate", description="Day to search")
     * @Rest\RequestParam(name="endDate", description="Day to search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTrainerActivitiesByRangeAction(ParamFetcher $paramFetcher)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $startDate = $paramFetcher->get('startDate');
        $endDate = $paramFetcher->get('endDate');
        $listActivities = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findModulesByRange($profile, $startDate, $endDate);

        $view = $this->renderView('TrainerBundle:Dashboard:trainer_activities.html.twig', [
            'bookings' => $listActivities
        ]);

        return new JsonResponse($view);
    }
}
