<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Status;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DashboardController
 * @package TrainerBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @var array
     */
    private $notificationsCount = array(
        'activities' => 0,
        'alerts'     => 0,
        'reports'    => 0,
        'exercices'  => 0,
    );

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);

        $reportsUnDone = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findTrainerReportByStatus($profile->getId(), $this->getDoctrine()->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::REPORT_UNDONE])->getId());

        $dateMin = new \DateTime('last saturday');
        $dateMin->modify('+1 day');
        $dtMax = clone $dateMin;
        $dtMax->modify('+6 days');
        $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
            'liveResource' => $profile,
        ));
        $coursesCreatedIds = [];
        $coursesCreated    = [];
        $courseIds         = [];
        $courses           = [];
        foreach ($assignmentTrainer as $assignment){
            $intervention = $assignment->getModule()->getIntervention();
            $today        = date("Y-m-d");
            $begin        = $intervention->getBeginning()->format("Y-m-d");
            $end          = $intervention->getEnding()->format("Y-m-d ");
            if (strtotime($begin) <= strtotime($today) && strtotime($end) >= strtotime($today)) {
                if (!in_array($intervention->getId(), $courseIds)) {
                    $courseIds[] = $intervention->getId();
                    $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
                    $times = [];

                    foreach ($courseModules as $module) {
                        if (!$duration = $module->getDuration()) {
                            continue;
                        }

                        $times[] = $duration->format('H:i');
                    }

                    $totalDuration = $this->AddPlayTime($times);
                    $courses[] = array('totalDuration' => $totalDuration, 'intervention' => $intervention);
                }
            } elseif (strtotime($begin) >= strtotime($today)) {
                if (!in_array($intervention->getId(), $coursesCreatedIds)) {
                    $coursesCreatedIds[] = $intervention->getId();
                    $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
                    $times = [];

                    foreach ($courseModules as $module) {
                        if (!$duration = $module->getDuration()) {
                            continue;
                        }

                        $times[] = $duration->format('H:i');
                    }

                    $totalDuration = $this->AddPlayTime($times);
                    $coursesCreated[] = array('totalDuration' => $totalDuration, 'intervention' => $intervention);
                }
            }
        }
        return $this->render('TrainerBundle:Dashboard:index.html.twig', array(
            'reportsToDo'    => $reportsUnDone,
            'coursesCreated' => $coursesCreated,
            'courses'        => $courses,
            'count'          => $this->notificationsCount,
            'modal'          => false,
            'bookings'       => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBookingByProfileAndDate($profile, new \DateTime(date("Y-m-d"))),
            'bookingsWeek'   => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findModulesByRange($profile, $dateMin, $dtMax),
            'bookingsMonth'  => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findModulesByRange(
                    $profile,
                    new \DateTime('first day of this month 00:00:00'),
                    new \DateTime('last day of this month 00:00:00')
                ),
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }

    function AddPlayTime($times)
    {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours   = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function alertsAction()
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);

        $repository = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda');

        return $this->render('TrainerBundle:Dashboard:alerts.html.twig', array(
            'count'                      => $this->notificationsCount,
            'alert_count'                => array(
                'plannings' => 0,
                'reports'   => 0,
                'exercices' => 0,
                'messages'  => 0,
                'billings'  => 0,
            ),
            // TODO : make it todays
            'bookings'                   => $repository->findBy(array('trainer' => $profile)),
            'messages'                   => [],
            'activities'                 => [],
            'count_reports_un_realised'  => $repository->countTrainerReport($profile->getId()),
            'reports_un_realised'        => $repository->findTrainerReport($profile->getId()),
            'count_reports_un_validated' => $repository->countTrainerReport($profile->getId()),
            'reports_un_validated'       => $repository->findTrainerReport($profile->getId()),
            'count_reports_rejected'     => $repository->countTrainerReport($profile->getId()),
            'reports_rejected'           => $repository->findTrainerReport($profile->getId()),
            'modal'                      => false,
            'count_invoice_overdelay'    => 0,
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exercicesAction()
    {
        return $this->render('TrainerBundle:Dashboard:exercices.html.twig', array(
            'count'     => $this->notificationsCount,
            'exercices' => [],
        ));
    }

    /**
     * Show the page before start a vision
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function beginVisioAction()
    {
        return $this->render('TrainerBundle:Dashboard:view_before_visio.html.twig');
    }
}
