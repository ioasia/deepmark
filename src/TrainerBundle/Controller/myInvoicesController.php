<?php

namespace TrainerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class myInvoicesController extends Controller
{
    public function indexAction()
    {
        return $this->render('TrainerBundle:myInvoices:index.html.twig', array(
          'invoices' => [],
        ));
    }
}
