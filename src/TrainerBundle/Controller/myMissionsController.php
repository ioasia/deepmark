<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\NativeQuizResult;
use ApiBundle\Entity\StoredModule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Status;

class myMissionsController extends Controller
{
    public function indexAction()
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);
        $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
            'liveResource' => $profile,
        ));
        $coursesFinished   = [];
        $coursesInProgress = [];
        $coursesCreated    = [];
        $courseIds         = [];
        foreach ($assignmentTrainer as $assignment){
            $intervention = $assignment->getModule()->getIntervention();
            $today        = date("Y-m-d");
            $begin        = $intervention->getBeginning()->format("Y-m-d");
            $end          = $intervention->getEnding()->format("Y-m-d ");
            if (!in_array($intervention->getId(), $courseIds)) {
                $courseIds[] = $intervention->getId();
                $courseModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
                $times = [];

                foreach ($courseModules as $module) {
                    if (!$duration = $module->getDuration()) {
                        continue;
                    }

                    $times[] = $duration->format('H:i');
                }

                $totalDuration = $this->AddPlayTime($times);
                if (strtotime($begin) <= strtotime($today) && strtotime($end) >= strtotime($today)) {
                    $coursesInProgress[] = array('totalDuration' => $totalDuration, 'intervention' => $intervention);
                }
                if (strtotime($end) < strtotime($today)) {
                    $coursesFinished[] = array('totalDuration' => $totalDuration, 'intervention' => $intervention);
                }
                if (strtotime($begin) > strtotime($today)) {
                    $coursesCreated[] = array('totalDuration' => $totalDuration, 'intervention' => $intervention);
                }
            }
        }


        return $this->render('TrainerBundle:myMissions:index.html.twig', array(
            'interventions_created'     => $coursesCreated,
            'interventions_in_progress' => $coursesInProgress,
            'interventions_finished'    => $coursesFinished,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)

        ));
    }

    function AddPlayTime($times)
    {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours   = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }


    /**
     * return the intervention which the user want.
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);
        $intervention        = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->find($id);

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findModulesByIntervention($intervention->getId());
        $statusModules = [];
        $bookings      = [];
        $times         = [];
        $moduleAssigns = [];
        $moduleHasAssessment = [];
        $moduleRating = [];
        $moduleTrainerRating = [];
        $moduleBookingAgendaDoneNum = $moduleBookingAgendaBookedNum = [];

        $moduleAssessment = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'intervention' => $intervention,
            'storedModule' => StoredModule::ASSESSMENT,
        ));
        $moduleList = [];
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $moduleList[] = $module;
            $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
                'liveResource' => $profile, 'module' => $module
            ));
            if ($assignmentTrainer) {
                $moduleAssigns[] = $module->getId();
                $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)->findOneBy(array(
                    'trainer' => $profile, 'module' =>$module
                ));
                if ($bookingAgenda){
                    $bookings[$module->getId()] = $bookingAgenda->getId();
                }

                $bookingAgendas = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                    'trainer' => $profile, 'module' =>$module
                ));
                $status = 1;
                $bookingAgendaDoneNum = $bookingAgendaBookedNum = 0;
                if ($bookingAgendas) {
                    foreach ($bookingAgendas as $row){
                        if($row->getStatus()->getId() !=  Status::DONE and $row->getStatus()->getId() !=  Status::EXPIRED ){
                            $status = 0;
                            break;
                        }
                    }
                    foreach ($bookingAgendas as $row){
                        if($row->getStatus()->getId() ==  Status::DONE ){
                            $bookingAgendaDoneNum++;
                        }
                        if($row->getStatus()->getId() ==  Status::BOOKED ){
                            $bookingAgendaBookedNum++;
                        }
                    }


                } else {
                    $status = 0;
                }
                $moduleBookingAgendaDoneNum[$module->getId()]   = $bookingAgendaDoneNum;
                $moduleBookingAgendaBookedNum[$module->getId()] = $bookingAgendaBookedNum;
                $statusModules[$module->getId()] = $status;
                $moduleRating[$module->getId()]  = $this->getRatingByModule($module);
                $moduleTrainerRating[$module->getId()] = $this->getRatingTrainerByModule($module, $profile);

            }

            $hasAssessment = $this->getDoctrine()->getRepository('ApiBundle:ModuleAssessments')
                ->findOneBy(array(
                    'assessment' => $module,
                    'module'  => $moduleAssessment,
                ));

            if ($hasAssessment) {
                $moduleHasAssessment[] = $module->getId();
            }

            $times[]       = $module->getDuration()->format('H:i');
            $totalDuration = $this->AddPlayTime($times);
        }

        return $this->render('TrainerBundle:myMissions:show.html.twig', array(
            'moduleBookingAgendaDoneNum'   => $moduleBookingAgendaDoneNum,
            'moduleBookingAgendaBookedNum' => $moduleBookingAgendaBookedNum,
            'moduleRating'        => $moduleRating,
            'moduleTrainerRating' => $moduleTrainerRating,
            'moduleAssessment'    => $moduleAssessment,
            'moduleHasAssessment' => $moduleHasAssessment,
            'statusModules'       => $statusModules,
            'intervention'        => $intervention,
            'bookings'            => $bookings,
            'moduleAssigns'       => $moduleAssigns,
            'modules'             => $moduleList,
            'isCourses'           => true,
            'totalDuration'       => $totalDuration,
            'nextSession'         => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions($profile),

        ));
    }

    public function getRatingByModule(Module $module){
        $rating = 0;
        $total = 0;
        $moduleResponse = $this->getDoctrine()->getRepository('ApiBundle:ModuleResponse')
            ->findBy(array('module' => $module));
        foreach ($moduleResponse as $moduleRating){
            $total++;
            $rating += $moduleRating->getNotation();
        }
        return  $result = ['totalResponse'=> $total, 'rating'=> $total == 0 || $rating == 0 ? 0 : round($rating/$total, 2)]  ;
    }

    public function getRatingTrainerByModule(Module $module, $trainer){
        $rating = 0;
        $total = 0;
        $moduleResponse = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
            ->findBy(array('module' => $module, 'trainer'=>$trainer));
        foreach ($moduleResponse as $moduleRating){
            $total++;
            $rating += $moduleRating->getNotation();
        }
        return  $result = ['totalResponse'=> $total, 'rating'=> $total == 0 || $rating == 0 ? 0 : round($rating/$total, 2)]  ;
    }

    /**
     * @param $id

     * @return Response
     * @throws \Exception
     */
    public function detailAction($id)
    {

        $moduleToDo   = $this->getDoctrine()->getRepository(Module::class)->find($id);
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);

        $intervention = $moduleToDo->getIntervention();

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findModulesByIntervention($intervention->getId());

        $bookingsTrainer  = [];
        $moduleAssessment = null;

        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                if (StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()) {
                    $moduleAssessment = $module;
                }
                continue;
            }
            $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
                'liveResource' => $profile, 'module' => $module
            ));
            if ($assignmentTrainer) {
                $moduleAssigns[] = $module->getId();
                if (StoredModule::ONLINE != $module->getStoredModule()->getAppId()) {
                    $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)->findOneBy(array(
                        'trainer' => $profile, 'module' =>$module
                    ));
                    if ($bookingAgenda){
                        if ($bookingAgenda){
                            $bookingsTrainer[$module->getId()] = $bookingAgenda->getId();
                        }
                    }

                }

            }
        }
        $moduleRating        = $this->getRatingByModule($moduleToDo);
        $moduleTrainerRating = $this->getRatingTrainerByModule($moduleToDo, $profile);
        $status       =  $this->getDoctrine()->getRepository(Status::class)->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
        $statusBooked =  $this->getDoctrine()->getRepository(Status::class)->findOneBy(['appId' => Status::WITH_BOOKING_BOOKED]);
        $sessions = [];
        $sessionDones = [];
        $sessionBooked = [];
        $bookingDones = [];
        $bookeds = [];
        if (StoredModule::PRESENTATION_ANIMATION === $moduleToDo->getStoredModule()->getAppId() || StoredModule::VIRTUAL_CLASS === $moduleToDo->getStoredModule()->getAppId()){
            $sessions = $moduleToDo->getSessions();
            if ($sessions) {
                foreach ($sessions as $session){
                    $bookingDones = [];
                    $place = '';
                    if(StoredModule::PRESENTATION_ANIMATION === $moduleToDo->getStoredModule()->getAppId()){
                        $place = $session->getSessionPlace()->getAddress();
                    }
                    $allBooking = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                        'trainer' => $profile, 'module' =>$moduleToDo, 'moduleSession' => $session->getSession()
                    ));


                    $bookeds = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                        'trainer' => $profile, 'module' =>$moduleToDo, 'status' => $statusBooked, 'moduleSession' => $session->getSession()
                    ));
                    if($bookeds){
                        $sessionBooked[$session->getSession()->getId()] = array('data'=> $session->getSession(), 'place'=> $place, 'num' => count($bookeds), 'bookingId' =>$bookeds[0]->getId(), 'entity' => $bookeds[0]->getLearner()->getEntity()->getDesignation(), 'org' => $bookeds[0]->getLearner()->getEntity()->getOrganisation()->getDesignation() );
                    }
                    foreach ($allBooking as $booking){
                        if($booking->getStatus() == $status ){
                            $bookingDones[] = $booking;
                        }
                    }

//                    $bookingDones = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
//                        'trainer' => $profile, 'module' =>$moduleToDo, 'status' => $status, 'moduleSession' => $session->getSession()
//                    ));
                    if($bookingDones){
                        $sessionDones[$session->getSession()->getId()] = array('data'=> $session->getSession(), 'place'=> $place,  'num' => count($bookingDones), 'bookingId' =>$bookingDones[0]->getId(), 'entity' => $bookingDones[0]->getLearner()->getEntity()->getDesignation(), 'org' => $bookingDones[0]->getLearner()->getEntity()->getOrganisation()->getDesignation() );
                    }

                }
            }

        } else {
            $bookingDones = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                'trainer' => $profile, 'module' =>$moduleToDo, 'status' => $status
            ));
            $bookeds = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                'trainer' => $profile, 'module' =>$moduleToDo, 'status' => $statusBooked
            ));
        }
        return $this->render('TrainerBundle:myMissions:detail.html.twig', array(
            'sessionDones' => $sessionDones,
            'sessionBookeds' => $sessionBooked,
            'moduleRating'        => $moduleRating,
            'moduleTrainerRating' => $moduleTrainerRating,
            'moduleAssessment'    => $moduleAssessment,
            'bookingDones'    => $bookingDones,
            'bookeds'         => $bookeds,
            'module'          => $moduleToDo,
            'moduleAssigns'   => $moduleAssigns,
            'bookingsTrainer' => $bookingsTrainer,
            'idModule'        => $id,
            'modules'         => $modules,
            'intervention'    => $intervention,
            'nextSession'     => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }
}
