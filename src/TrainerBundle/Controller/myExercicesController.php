<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\ModuleAttendanceReport;
use ApiBundle\Entity\ModuleCancel;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleNotes;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\ModuleTemplate;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Room;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Service\QuizServiceAPI;
use DateInterval;
use DateTime;
use Doctrine\Common\Persistence\ObjectRepository;
use LearnerBundle\Controller\HelperTrait;
use LearnerBundle\Controller\Noticeable;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Traits\Controller\HasUsers;
use Traits\HasAttendance;
use Traits\IsAvailable;
use Utils\Email;
use PDO;

class myExercicesController extends Controller
{
    use Noticeable;
    use HelperTrait;
    use IsAvailable;
    use HasAttendance;
    use HasUsers;

    private function getLearnerInterventionRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention');
    }

    private function getInterventionRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Intervention');
    }

    private function getModuleRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Module');
    }

    private function getAssignments(Module $module)
    {
        $return = null;
        if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
            $return = $this->getDoctrine()->getRepository(AssigmentResourceSystem::class)
                ->findBy(array(
                    'module' => $module,
                ));
        }

        return $return;
    }

    /**
     * return the profile object for the current user.
     *
     * @return Profile
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * return the profile object for the trainer user.
     *
     * @return Profile
     */
    private function getUserProfile($uid)
    {
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['id' => $uid]);
    }

    private function isModuleDone(Profile $profile, Module $module)
    {
        $isDone          = false;
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')->findOneBy(array(
            'learner' => $profile,
            'module'  => $module,
        ));
        $appIdStatus     = $moduleIteration->getStatus()->getAppId();
        if ($appIdStatus == Status::LEARNER_INTERVENTION_FINISHED || $appIdStatus == Status::LEARNER_INTERVENTION_EXPIRED) {
            $isDone = true;
        } else {
            $this->shouldUpdateIteration($profile, $module);
        }

        return $isDone;
    }

    private function shouldUpdateIteration(Profile $profile, Module $module)
    {
        $moduleIterationExist = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));
        if (!$moduleIterationExist) {
            $moduleAppId = $module->getStoredModule()->getAppId();
            switch ($moduleAppId) {
                case StoredModule::ONLINE:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $this->checkBookingModuleDone($profile, $module);
                    break;
                default:
                    // $this->createModuleIteration($profile, $module);
                    break;
            }
        }
    }

    private function checkIfModuleBooked(Module $module, $profile)
    {
        if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));

            if ($bookingModule) {
                $module->setIsBooked(true);
            } else {
                $module->setIsBooked(false);
            }
        }
    }

    private function checkBookingModuleDone(Profile $profile, Module $module)
    {
        $duration      = clone $module->getDuration();
        $now           = new DateTime();
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));
        if ($bookingModule) {
            $end     = clone $bookingModule->getBookingDate();
            $hours   = intval($duration->format('H'));
            $minutes = intval($duration->format('i'));

            $interval = new DateInterval('PT' . $hours . 'H' . $minutes . 'M');
            $end->add($interval);
            if ($now >= $end) {
                $this->createModuleIteration($profile, $module);
            }
        }
    }

    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    private function createModuleIteration(Profile $profile, Module $module)
    {
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = new ModuleIteration();
        $moduleIteration->setLearner($profile);
        $moduleIteration->setModule($module);
        $moduleIteration->setExecutionDate(new DateTime());

        $intervention        = $em->getRepository('ApiBundle:Intervention')
            ->find($module->getIntervention());
        $interventionLearner = $em->getRepository('ApiBundle:LearnerIntervention')
            ->findOneBy(['learner' => $profile, 'intervention' => $intervention]);

        if (Status::LEARNER_INTERVENTION_CREATED == $interventionLearner->getStatus()->getAppId()) {
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);

            $interventionLearner->setStatus($status);
        }

        $nbModule = $em->getRepository('ApiBundle:Module')
            ->countLearnerModuleByInterventionId($intervention->getId());

        $countIteration = $em->getRepository('ApiBundle:ModuleIteration')
                ->countLearnerModuleIterationByInterventionId($intervention->getId(), $profile->getId()) + 1;

        $progress = ($countIteration / $nbModule) * 100;
        if (100 === $progress) {
            $interventionLearner->setStatus(
                $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::LEARNER_INTERVENTION_FINISHED,
                ))
            );
        }
        $interventionLearner->setProgression($progress);

        $em->persist($moduleIteration);
        $em->persist($interventionLearner);
        $em->flush();
    }

    public function indexAction()
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        // get answers to assign
        $db = $this->getDoctrine()->getManager();
        // Write your raw SQL
        $query = "SELECT answer_id FROM `exercises` GROUP BY answer_id";
        // Prepare the query from $db
        $statementDB = $db->getConnection()->prepare($query);
        // Execute both queries
        $statementDB->execute();
        $answerIds = $statementDB->fetchAll(PDO::FETCH_COLUMN, 0);

        $exercisesToAssigns = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersToAssign('open', $answerIds);
        $exercisesToAssignUsers = [];
        foreach ($exercisesToAssigns as $exercisesToAssign) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToAssign->getPlay()->getUser_id()));
            $exercisesToAssignUsers[$exercisesToAssign->getId()] = $player;
        }

        // get answers to do
        $exercisesToDos = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_TODO, $profile);
        $exercisesToDoUsers = [];
        foreach ($exercisesToDos as $exercisesToDo) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToDo->getPlay()->getUser_id()));
            $exercisesToDoUsers[$exercisesToDo->getId()] = $player;
        }

        // get answers to validation
        $exercisesToValidations = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_SUBMITTED, $profile);
        $exercisesToValidationUsers = [];
        foreach ($exercisesToValidations as $exercisesToValidation) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesToValidation->getPlay()->getUser_id()));
            $exercisesToValidationUsers[$exercisesToValidation->getId()] = $player;
        }

        // get validated answers
        $exercisesValidateds = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_APPROVED, $profile);
        $exercisesToValidatedUsers = [];
        foreach ($exercisesValidateds as $exercisesValidated) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesValidated->getPlay()->getUser_id()));
            $exercisesToValidatedUsers[$exercisesValidated->getId()] = $player;
        }

        // get reject answers
        $exercisesRejects = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->getAnswersByStatus('open', Exercises::STATUS_REJECTED, $profile);
        $exercisesRejectedUsers = [];
        foreach ($exercisesRejects as $exercisesReject) {
            $player =   $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array('person' => $exercisesReject->getPlay()->getUser_id()));
            $exercisesRejectedUsers[$exercisesReject->getId()] = $player;
        }

        return $this->render('TrainerBundle:myExercices:index.html.twig', array(
            'exercisesToAssigns'        => $exercisesToAssigns,
            'exercisesToAssignUsers'    => $exercisesToAssignUsers,
            'exercisesToDos'        => $exercisesToDos,
            'exercisesToDoUsers'    => $exercisesToDoUsers,
            'exercisesToValidations'        => $exercisesToValidations,
            'exercisesToValidationUsers'    => $exercisesToValidationUsers,
            'exercisesValidateds'        => $exercisesValidateds,
            'exercisesToValidatedUsers'    => $exercisesToValidatedUsers,
            'exercisesRejects'              => $exercisesRejects,
            'exercisesRejectedUsers'        => $exercisesRejectedUsers,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function assignAction($id)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $answer = $this->getDoctrine()
            ->getRepository('ApiBundle:QuizUsersAnswers')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $exercises = $this->getDoctrine()->getRepository('ApiBundle:Exercises')->findOneBy(array(
            'answer' => $answer, 'trainer' => $profile
        ));

        if (!$exercises) {
            $exercises = new Exercises();
        }
        // save the Exercises object
        $exercises->setTrainer($profile);
        $exercises->setAnswer($answer);
        $exercises->setStatus(ModuleReports::STATUS_TODO);
        $exercises->setDateAdd(new DateTime());
        $em->persist($exercises);
        $em->flush();

        return $this->redirect($this->generateUrl('trainer_exercices_index'), 301);
    }

    /**
     * @param $id
     * @param $bid
     * @param $learer
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function bookingAgendaAction($id, $bid, $learerBook = null, Request $request)
    {
        $bookedModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($bid);
        $learner      = $bookedModule->getLearner();
        /** @var Module $moduleToDo */
        $moduleToDo   = $this->getModuleRepository()->find($id);
        $profile      = $this->getCurrentUser();
        $emailTrainer = $bookedModule->getTrainer()->getPerson()->getEmail();

        $intervention = $moduleToDo->getIntervention();

        $modules = $this->getDoctrine()->getRepository('ApiBundle:Module')->findModulesByIntervention($intervention->getId());

        $courses          = [];
        $moduleIterations = [];
        $counter          = 0;
        $isNext           = false;
        $isNextSet        = false;
        $moduleNext       = null;
        $expired          = null;
        $moduleAssigns    = [];
        $bookingsTrainer  = [];
        $moduleHasAssessment = [];
        $moduleAssessment = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'intervention' => $intervention,
            'storedModule' => StoredModule::ASSESSMENT,
        ));
        $moduleRatings = [];
        $moduleTrainerRatings = [];
        $moduleBookingAgendaDoneNum = $moduleBookingAgendaBookedNum = [];
        $status       =  $this->getDoctrine()->getRepository(Status::class)->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
        $statusBooked =  $this->getDoctrine()->getRepository(Status::class)->findOneBy(['appId' => Status::WITH_BOOKING_BOOKED]);
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {

                continue;
            }

            $hasAssessment = $this->getDoctrine()->getRepository('ApiBundle:ModuleAssessments')
                ->findOneBy(array(
                    'assessment' => $module,
                    'module'  => $moduleAssessment,
                ));

            if ($hasAssessment) {
                $moduleHasAssessment[] = $module->getId();
            }

            if ($isNext && !$isNextSet) {
                $moduleNext = $module;
                $isNextSet  = true;
            }

            ++$counter;
            if ($module->getId() === $moduleToDo->getId()) {
                $isNext = true;
            }

            //set status or checking status
            $this->updateStatusModule($learner, $module);
            $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));
            $status          = $moduleIteration->getStatus();
            //end set status or checking status

            $isDone = $this->isModuleDone($learner, $module);
            $module->setDone($isDone);

            if (!$isDone) {
                $bookingModule = $this->getDoctrine()
                    ->getRepository('ApiBundle:BookingAgenda')
                    ->findOneBy(array(
                        'learner' => $learner,
                        'module'  => $module,
                    ));

                $bookings = $this->getDoctrine()
                    ->getRepository('ApiBundle:BookingAgenda')
                    ->findBy(array(
                        'trainer' => $profile,
                        'module'  => $module,
                    ));

                // prepare report data for Online modules
                $reports = [];
                $min     = $max = $avg = null;
                if ($bookings) {
                    $grades = [];
                    $total  = 0;
                    $i      = 0;
                    foreach ($bookings as $k => $item) {
                        $grade                  = $item->getLearnerGrade() ? $item->getLearnerGrade() : 0;
                        $report                 = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->findOneBy(array(
                            'session' => $item, 'module' => $module, 'trainer' => $profile
                        ));
                        $reports[$k]['booking'] = $item;
                        $reports[$k]['report']  = $report;
                        if ($grade) {
                            $i++;
                            $grades[] = $grade;
                            $total    += $grade;
                        }
                    }
                    $max = $grades ? max($grades) : 'N/A';
                    $min = $grades ? min($grades) : 'N/A';
                    $avg = $total ? $total / $i : 'N/A';
                }
                // prepare report data for Online modules

                // prepare report data for Quiz modules
                $learnerQuizzes = [];
                if ($module->getQuiz()) {
                    $quizScores = [];
                    $quizTotal  = 0;
                    $i          = 0;
                    // todo: implement with new quiz
                    /*$leanerId          = $learner->getId();
                    if ('NATIVE' === $module->getQuiz()->getType()) {
                        $result  = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                            ->findOneBy([
                                'student' => $learner,
                                'quiz'    => $module->getQuiz(),
                            ]);

                        $score                         = $result ? round(($result->getScore() / $module->getQuiz()->getTotalPoints()) * 100, 2) : 0;
                        $learnerQuizzes[$leanerId]['student'] = $learner;
                        $learnerQuizzes[$leanerId]['quiz']    = $module->getQuiz();
                        $learnerQuizzes[$leanerId]['toPass']  = $module->getScoreQuiz();
                        $learnerQuizzes[$leanerId]['result']  = $result ? $result->getScore() : '-';
                        $learnerQuizzes[$leanerId]['score']   = $score;

                        if ($result) {
                            $i++;
                            $quizTotal    += $score;
                            $quizScores[] = $score;
                        }
                    }*/
                    $max = $quizScores ? max($quizScores) : 'N/A';
                    $min = $quizScores ? min($quizScores) : 'N/A';
                    $avg = $quizTotal ? $quizTotal / $i : 'N/A';
                }
                // prepare report data for Quiz modules
                array_push($moduleIterations, [
                    'module'  => $module,
                    'counter' => $counter,
                    'booking' => $bookingModule,
                    'status'  => $status,
                    'online'  => ['reports' => $reports, 'min' => $min, 'max' => $max, 'avg' => $avg],
                    'quizzes' => ['quizzes' => $learnerQuizzes, 'min' => $min, 'max' => $max, 'avg' => $avg]
                ]);
            }

            $notation = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleResponse')
                ->findOneBy(['module' => $module, 'learner' => $learner]);
            array_push($courses, ['module' => $module, 'notation' => $notation, 'status' => $status]);



            $assignmentTrainer = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(array(
                'liveResource' => $profile, 'module' => $module
            ));
            if ($assignmentTrainer) {
                $moduleAssigns[] = $module->getId();
                if ($learerBook) {
                    $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)->findOneBy(array(
                        'trainer' => $profile, 'module' =>$module, 'learner'=>$learerBook
                    ));


                } else {
                    $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)->findOneBy(array(
                        'trainer' => $profile, 'module' =>$module
                    ));

                }

                if ($bookingAgenda){
                    if ($bookingAgenda){
                        $bookingsTrainer[$module->getId()] = $bookingAgenda->getId();
                    }
                }


                $bookingDones = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                    'trainer' => $profile, 'module' =>$module, 'status' => $status
                ));
                $bookeds = $this->getDoctrine()->getRepository(BookingAgenda::class)->findBy(array(
                    'trainer' => $profile, 'module' =>$module, 'status' => $statusBooked
                ));

                $moduleBookingAgendaDoneNum[$module->getId()]   = count($bookingDones);
                $moduleBookingAgendaBookedNum[$module->getId()] = count($bookeds);
                $moduleRatings[$module->getId()]  = $this->getRatingByModule($module);
                $moduleTrainerRatings[$module->getId()] = $this->getRatingTrainerByModule($module, $profile);

            }
        }

        $moduleWording = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleWording')
            ->findOneBy(['module' => $moduleToDo]);

        $isBooked = $this->checkIfModuleBooked($moduleToDo, $learner);

        $bookingModule = null;
        if (in_array($moduleToDo->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            // the module will be expired when the today - the booking date + the duration time > 24 hrs
            $expired = $this->isAvailableBooking($moduleToDo, $bookedModule);
        }

        $groupEntities = [];
        // Set course status
        $today  = new DateTime();
        $module = $bookedModule->getModule();
        $end    = clone $bookedModule->getBookingDate();
        //variable time for countdown script
        $countDowntime    = clone $bookedModule->getBookingDate();
        $interval         = new DateInterval('PT15M');
        $interval->invert = 1;
        $countDowntime->add($interval);
        //end countdown time
        $hour    = $module->getDuration()->format('G') + $bookedModule->getBookingDate()->format('G');
        $minutes = $module->getDuration()->format('i') + $bookedModule->getBookingDate()->format('i');
        $end->setTime($hour, $minutes, 0);
        $isTrained = $today < $countDowntime ? Status::SESSION_NOT_START : ($bookedModule->getStatus()->getAppId() == Status::WITH_BOOKING_DONE ? Status::SESSION_DONE : Status::SESSION_IN_PROGRESS);

        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookedModule);
        } else {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $moduleToDo,
                'trainer' => $profile,
                'moduleSession' => $bookedModule->getModuleSession()
            ));
            foreach ($bookingAgendas as $bookingAgenda){
                $entity = $bookingAgenda->getLearner()->getEntity();
                if(!array_key_exists($entity->getId(), $groupEntities)){
                    $groupEntities[$entity->getId()]['entityName'] = $entity->getDesignation();
                    $groupEntities[$entity->getId()]['organisationName'] = $entity->getOrganisation()->getDesignation();
                }
                $groupEntities[$entity->getId()]['learners'][] = $bookingAgenda->getLearner();

            }
        }

        $moduleReports = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->findOneBy(array(
            'session' => $bookedModule, 'module' => $moduleToDo, 'trainer' => $profile, 'status' => [ModuleReports::STATUS_SUBMITTED, ModuleReports::STATUS_APPROVED]
        ));

        $moduleTemplate = $this->getDoctrine()->getRepository('ApiBundle:ModuleTemplate')->findOneBy(array(
            'module' => $moduleToDo, 'appId' => ModuleTemplate::INDIVIDUAL_REPORT_TEMPLATE
        ));

        $moduleAttendanceReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleAttendanceReport')->findOneBy(array(
            'module' => $moduleToDo, 'trainer' => $profile
        ));

        $moduleAttendanceTemplate = $this->getDoctrine()->getRepository('ApiBundle:ModuleTemplate')->findOneBy(array(
            'module' => $moduleToDo, 'appId' => ModuleTemplate::ATTENDANCE_REPORT_TEMPLATE
        ));
        $moduleNote      = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleNotes')
            ->findOneBy(['module' => $moduleToDo, 'trainer' => $profile]);

        $moduleRating        = $this->getRatingByModule($moduleToDo);
        $moduleTrainerRating = $this->getRatingTrainerByModule($moduleToDo, $profile);

        $replacement = $this->getDoctrine()
            ->getRepository('ApiBundle:RequestReplacementResource')
            ->findOneBy(['bookingAgenda' => $bookedModule]);

        return $this->render('TrainerBundle:myExercices:bookingAgenda.html.twig', array(
            'replacement'                   => $replacement,
            'moduleBookingAgendaDoneNum'   => $moduleBookingAgendaDoneNum,
            'moduleBookingAgendaBookedNum' => $moduleBookingAgendaBookedNum,
            'moduleRatings'          => $moduleRatings,
            'moduleTrainerRatings'   => $moduleTrainerRatings,
            'moduleRating'           => $moduleRating,
            'moduleTrainerRating'    => $moduleTrainerRating,
            'moduleAssessment'       => $moduleAssessment,
            'moduleHasAssessment'    => $moduleHasAssessment,
            'moduleAssigns'          => $moduleAssigns,
            'bookingsTrainer'        => $bookingsTrainer,
            'idModule'               => $id,
            'modulesIntervention'    => $modules,
            'modules'                => $courses,
            'note'                   => $moduleNote ? $moduleNote->getTrainerNotes() : '',
            'module'                 => $moduleToDo,
            'expired'                => !$expired,
            'next_module'            => $moduleNext,
            'intervention'           => $intervention,
            'observation'            => $moduleWording,
            'isBooked'               => $isBooked,
            'booked'                 => $bookedModule,
            'emailTrainer'           => $emailTrainer,
            'countDownTime'          => $countDowntime,
            'learner'                => $learner,
            'isTrained'              => $isTrained,
            'modules_iterations'     => $moduleIterations,
            'templateFile'           => $moduleTemplate ? $moduleTemplate : null,
            'reportFile'             => $moduleReports ? $moduleReports : null,
            'attendanceTemplateFile' => $moduleAttendanceTemplate ? $moduleAttendanceTemplate : null,
            'attendanceReportFile'   => $moduleAttendanceReport ? $moduleAttendanceReport : null,
            'bookingAgendas'         => $bookingAgendas,
            'groupEntities'          => $groupEntities,
            'canBeNotated'           => $this->canModuleBeNotated($moduleToDo, $profile),
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function viewAction($id, Request $request, QuizServiceAPI $quizServiceAPI)
    {
        $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($id);
        $questionItems = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'personalized_correction');
        $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
        $items = [];
        foreach ($questionItems as $k => $questionItem) {
            $items[$k]['item'] = $questionItem;
            $mark = $this->getDoctrine()->getRepository('ApiBundle:Marks')
                ->findBy(array(
                    'exercises' => $answer->getExercises(),
                    'questionsItem' => $questionItem,
                ));
            $items[$k]['mark'] = $mark;
        }

        // get step and total step
        $quizSteps = $quizServiceAPI->sanitizeQuiz($answer->getPlay()->getQuiz());
        $step = 1;
        foreach ($quizSteps as $k => $quizStep) {
            foreach ($quizStep['questions'] as $question) {
                if ($question->getId() == $answer->getQuestion()->getId()) {
                    $step = $k + 1;
                    break;
                }
            }
        }

        $tags = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findAll();

        return $this->render('TrainerBundle:myExercices:mark.html.twig', array(
            'totalStep'             => count($quizSteps),
            'step'                  => $step,
            'answer'                => $answer,
            'isSubmitted'           => 1,
            'questionMarkItems'     => $items,
            'defaultCorrection'     => $defaultCorrection,
            'templateFile'          => null,
            'reportFile'            => null,
            'tags'                  => $tags,
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser())
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function markAction($id, Request $request, QuizServiceAPI $quizServiceAPI)
    {
        $answer = $this->getDoctrine()->getRepository('ApiBundle:QuizUsersAnswers')->find($id);
        $questionItems = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'personalized_correction');
        $defaultCorrection = $this->getDoctrine()->getRepository('ApiBundle:QuizQuestionsItems')->getItemsByQuestionId($answer->getQuestion()->getId(), 'default_correction');
        $items = [];
        foreach ($questionItems as $k => $questionItem) {
            $items[$k]['item'] = $questionItem;
            $mark = $this->getDoctrine()->getRepository('ApiBundle:Marks')
                ->findBy(array(
                    'exercises' => $answer->getExercises(),
                    'questionsItem' => $questionItem,
                ));
            $items[$k]['mark'] = $mark;
        }

        // get step and total step
        $quizSteps = $quizServiceAPI->sanitizeQuiz($answer->getPlay()->getQuiz());
        $step = 1;
        foreach ($quizSteps as $k => $quizStep) {
            foreach ($quizStep['questions'] as $question) {
                if ($question->getId() == $answer->getQuestion()->getId()) {
                    $step = $k + 1;
                    break;
                }
            }
        }
        $allowSubmitToAdmin = false;
        if ($items) {
            if($answer->getExercises()->getGrade() != NULL){
                $allowSubmitToAdmin = true;
                foreach ($items as $k => $item) {
                    if (!$item['mark']) {
                        $allowSubmitToAdmin = false;
                        break;
                    }
                }
            }
        } else {
            if ($answer->getExercises()->getGrade() != NULL) {
                $allowSubmitToAdmin = true;
            }
        }

        $tags = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findAll();

        return $this->render('TrainerBundle:myExercices:mark.html.twig', array(
            'totalStep'             => count($quizSteps),
            'step'                  => $step,
            'answer'                => $answer,
            'isSubmitted'           => $answer->getExercises()->getStatus() == Exercises::STATUS_SUBMITTED || $answer->getExercises()->getStatus() == Exercises::STATUS_APPROVED ? 1 : 0,
            'questionMarkItems'     => $items,
            'defaultCorrection'     => $defaultCorrection,
            'templateFile'          => null,
            'reportFile'            => null,
            'tags'                  => $tags,
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $this->getCurrentUser()),
            'allowSubmitToAdmin'    => $allowSubmitToAdmin
        ));
    }

    public function getRatingByModule(Module $module){
        $rating = 0;
        $total = 0;
        $moduleResponse = $this->getDoctrine()->getRepository('ApiBundle:ModuleResponse')
            ->findBy(array('module' => $module));
        foreach ($moduleResponse as $moduleRating){
            $total++;
            $rating += $moduleRating->getNotation();
        }
        return  $result = ['totalResponse'=> $total, 'rating'=> $total == 0 || $rating == 0 ? 0 : round($rating/$total, 2)]  ;
    }

    public function getRatingTrainerByModule(Module $module, $trainer){
        $rating = 0;
        $total = 0;
        $moduleResponse = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
            ->findBy(array('module' => $module, 'trainer'=>$trainer));
        foreach ($moduleResponse as $moduleRating){
            $total++;
            $rating += $moduleRating->getNotation();
        }
        return  $result = ['totalResponse'=> $total, 'rating'=> $total == 0 || $rating == 0 ? 0 : round($rating/$total, 2)]  ;
    }

    private function updateStatusModule($profile, $module, $notShow = false)
    {
        $em              = $this->getDoctrine()->getManager();
        $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
            ->findOneBy(array(
                'learner' => $profile,
                'module'  => $module,
            ));

        if (!isset($moduleIteration)) {
            $moduleIteration = new ModuleIteration();
            $moduleIteration->setLearner($profile);
            $moduleIteration->setModule($module);
            $moduleIteration->setExecutionDate(new DateTime());
        }

        if (empty($moduleIteration->getStatus())) {
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_CREATED]);
            $moduleIteration->setStatus($status);
        }

        if ($notShow) {
            $moduleAppId = $module->getStoredModule()->getAppId();
            if ($moduleAppId == StoredModule::ELEARNING || $moduleAppId == StoredModule::QUIZ || $moduleAppId == StoredModule::EVALUATION) {
                //set expired status for module
                $endTime = $module->getEnding()->format('Y-m-d');
                if (date('Y-m-d') > $endTime) {
                    $status = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_EXPIRED]);
                    $moduleIteration->setStatus($status);
                } elseif ($moduleIteration->getStatus()->getAppId() == Status::LEARNER_INTERVENTION_CREATED) {
                    $status = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);
                    $moduleIteration->setStatus($status);
                }
                //end set expired status for module
            }
        }
        $em->persist($moduleIteration);
        $em->flush();
    }

    public function statusCompleteBookingAgendaAction($id, Request $request)
    {
        $currentTime     = new DateTime();
        $isReport = 0;
        $profile         = $this->getCurrentUser();
        $em              = $this->getDoctrine()->getManager();
        $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->find($id);
        $module          = $isBookingAgenda->getModule();
        if (StoredModule::VIRTUAL_CLASS === $module->getStoredModule()->getAppId() || StoredModule::ONLINE_WORKSHOP === $module->getStoredModule()->getAppId()) {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $module,
                'moduleSession'  => $isBookingAgenda->getModuleSession(),
                'trainer' => $profile
            ));
            foreach ($bookingAgendas as $booking) {
                $learnerIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $booking->getLearner(),
                        'module'  => $module,
                    ));
                if (Status::LEARNER_INTERVENTION_IN_PROGRESS == $learnerIteration->getStatus()->getAppId()) {
                    $diff       = $currentTime->getTimestamp() - $learnerIteration->getStartTime()->getTimestamp();
                    $statusDone = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::WITH_BOOKING_DONE,
                    ));
                    $learnerIteration->setStatus($statusDone);
                    $learnerIteration->setLearnedTime($diff);
                    $learnerIteration->setExecutionDate(new DateTime());
                    $this->updateProgressCourse($module, $booking->getLearner());
                    $booking->setStatus($statusDone);
                    $isReport = 1;
                } else {
                    $statusAbsent = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::WITH_BOOKING_LEARNER_ABSENT,
                    ));
                    $learnerIteration->setStatus($statusAbsent);
                    $booking->setStatus($statusAbsent);
                }
                $em->persist($learnerIteration);
                $em->flush();
            }
            // add trainer report if there is at least a learner is learning
            if ($isReport) {
                $this->addModuleReport($isBookingAgenda, $module, $isBookingAgenda->getTrainer(), ModuleReports::STATUS_TODO);
            }
        } else if (StoredModule::PRESENTATION_ANIMATION === $module->getStoredModule()->getAppId()) {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $module,
                'moduleSession'  => $isBookingAgenda->getModuleSession(),
                'trainer' => $profile
            ));
            foreach ($bookingAgendas as $booking) {
                $learnerIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $booking->getLearner(),
                        'module'  => $module,
                    ));
                if (Status::LEARNER_INTERVENTION_IN_PROGRESS == $learnerIteration->getStatus()->getAppId()) {
                    if ($learnerIteration->getStartTime()) {
                        $diff       = $currentTime->getTimestamp() - $learnerIteration->getStartTime()->getTimestamp();
                        $statusDone = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                            'appId' => Status::WITH_BOOKING_DONE,
                        ));
                        $learnerIteration->setStatus($statusDone);
                        $learnerIteration->setLearnedTime($diff);
                        $learnerIteration->setExecutionDate(new DateTime());
                        $this->updateProgressCourse($module, $booking->getLearner());
                        $booking->setStatus($statusDone);
                        $isReport = 1;
                    } else {
                        $statusAbsent = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                            'appId' => Status::WITH_BOOKING_LEARNER_ABSENT,
                        ));
                        $learnerIteration->setStatus($statusAbsent);
                        $booking->setStatus($statusAbsent);
                    }
                } else {
                    $statusAbsent = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::WITH_BOOKING_LEARNER_ABSENT,
                    ));
                    $learnerIteration->setStatus($statusAbsent);
                    $booking->setStatus($statusAbsent);
                }
                $em->persist($learnerIteration);
                $em->flush();
            }
            // add trainer report if there is at least a learner is learning
            if ($isReport) {
                $this->addModuleReport($isBookingAgenda, $module, $isBookingAgenda->getTrainer(), ModuleReports::STATUS_TODO);
            }
        } else {
            //1-1
            $moduleIteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $isBookingAgenda->getLearner(),
                    'module'  => $module,
                ));

            if (Status::LEARNER_INTERVENTION_IN_PROGRESS == $moduleIteration->getStatus()->getAppId()) {
                $diff = $currentTime->getTimestamp() - $moduleIteration->getStartTime()->getTimestamp();

                $statusDone = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_DONE,
                ));
                $isBookingAgenda->setStatus($statusDone);
                $moduleIteration->setStatus($statusDone);
                $moduleIteration->setLearnedTime($diff);
                $moduleIteration->setExecutionDate(new DateTime());

                $this->addModuleReport($isBookingAgenda, $module, $isBookingAgenda->getTrainer(), ModuleReports::STATUS_TODO);
            } else {
                $statusAbsent = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_LEARNER_ABSENT,
                ));
                $isBookingAgenda->setStatus($statusAbsent);
                $moduleIteration->setStatus($statusAbsent);
            }
            $em->persist($isBookingAgenda);
            $em->persist($moduleIteration);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('trainer_exercices_video_booking_agenda', ['id' => $module->getId(), 'bid' => $isBookingAgenda->getId()]), 301);
    }

    public function videoAction($id, Request $request)
    {
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);

        $moduleToDo    = $bookingModule->getModule();
        $profile       = $this->getCurrentUser();

        $moduleNotes = $this->getDoctrine()->getRepository('ApiBundle:ModuleNotes')->findOneBy(array(
            'session' => $bookingModule, 'module' => $moduleToDo, 'trainer' => $profile
        ));

        $moduleTemplate = $this->getDoctrine()->getRepository('ApiBundle:ModuleTemplate')->findOneBy(array(
            'module' => $moduleToDo, 'appId' => ModuleTemplate::INDIVIDUAL_REPORT_TEMPLATE
        ));

        $moduleReports = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->findOneBy(array(
            'session' => $bookingModule, 'module' => $moduleToDo, 'trainer' => $profile, 'status' => [ModuleReports::STATUS_SUBMITTED, ModuleReports::STATUS_APPROVED]
        ));

        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookingModule);
        } else {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $moduleToDo,
                'trainer' => $profile,
                'moduleSession' => $bookingModule->getModuleSession()
            ));
        }

        $moduleAttendanceReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleAttendanceReport')->findOneBy(array(
            'module' => $moduleToDo, 'trainer' => $profile
        ));

        $moduleAttendanceTemplate = $this->getDoctrine()->getRepository('ApiBundle:ModuleTemplate')->findOneBy(array(
            'module' => $moduleToDo, 'appId' => ModuleTemplate::ATTENDANCE_REPORT_TEMPLATE
        ));

        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $this->getDoctrine()->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $this->getDoctrine()->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $this->getEntityFromProfile()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        return $this->render('TrainerBundle:myExercices:video.html.twig', array(
            'module'                 => $moduleToDo,
            'isLearned'              => true,
            'booked'                 => $bookingModule,
            'bookingAgendas'         => $bookingAgendas,
            'notes'                  => $moduleNotes ? $moduleNotes->getTrainerNotes() : null,
            'templateFile'           => $moduleTemplate ? $moduleTemplate : null,
            'reportFile'             => $moduleReports ? $moduleReports : null,
            'attendanceTemplateFile' => $moduleAttendanceTemplate ? $moduleAttendanceTemplate : null,
            'attendanceReportFile'   => $moduleAttendanceReport ? $moduleAttendanceReport : null,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile),
            'searchParam'   => $searchParam,
            'intervention'  => $moduleToDo->getIntervention()
        ));
    }

    public function saveNotesAction(Request $request)
    {
        $data    = $request->request->all();
        $trainer = $this->getCurrentUser();
        $learner = $this->getUserProfile($data['learnerId']);

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $data['moduleId'],
        ));

        /** @var BookingAgenda $bookingAgenda */
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $data['bookingAgendaId'],
        ));

        $moduleNotes = $this->getDoctrine()->getRepository('ApiBundle:ModuleNotes')->findOneBy(array(
            'session' => $bookingAgenda, 'module' => $module, 'trainer' => $trainer
        ));

        $moduleNotes = $moduleNotes ? $moduleNotes : new ModuleNotes();
        $moduleNotes->setLearner($learner);
        $moduleNotes->setTrainer($trainer);
        $moduleNotes->setModule($module);
        $moduleNotes->setSession($bookingAgenda);
        $moduleNotes->setTrainerNotes($data['notes']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($moduleNotes);
        $em->flush();

        $response = new Response(json_encode($moduleNotes ? $moduleNotes->getId() : false));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function saveGradeAction(Request $request)
    {
        $data = $request->request->all();
        /** @var BookingAgenda $bookingAgenda */
        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $data['booking_agenda_id'],
        ));

        $bookingAgenda->setLearnerGrade($data['grade']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($bookingAgenda);
        $em->flush();

        $response = new Response(json_encode($bookingAgenda ? $bookingAgenda->getId() : false));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function saveAttendanceAction(Request $request)
    {
        $attendances = $request->request->get('attendances');
        $type = $request->request->get('type');
        $this->saveAttendance($attendances, $type);

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function saveRoomAction(Request $request)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $roomName = $request->request->get('roomName');
        $sessionId = $request->request->get('sessionId');

        if ($roomName && $sessionId) {
            $session = $this->getDoctrine()->getRepository('ApiBundle:Session')->findOneBy(array(
                'id' => $sessionId,
            ));
            if ($session) {
                if (($persistedRoom = $this->getDoctrine()->getRepository('ApiBundle:Room')->findOneBy(['roomName' => $roomName])) == null) {
                    $persistedRoom = new Room();
                    $persistedRoom->setRoomName($roomName);
                    $em->persist($persistedRoom);
                }
                $session->addRoom($persistedRoom);
                $em->persist($session); //persist the object
                $em->flush($session);
            }
        }

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function assignRoomAction(Request $request)
    {
        $bookings = $request->request->get('bookings');
        $roomId = $request->request->get('roomId');

        if ($bookings && $roomId) {
            $room = $this->getDoctrine()->getRepository('ApiBundle:Room')->findOneBy(['id' => $roomId]);
            if ($room) {
                foreach ($bookings as $booking) {
                    $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                        'id' => $booking['bookingId'],
                    ));
                    $bookingAgenda->setRoom($room);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($bookingAgenda);
                    $em->flush($bookingAgenda);
                }
            }
        }

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function allowSignaturesAction(Request $request)
    {
        $signatures = $request->request->get('signatures');
        $typeSignature = $request->request->get('typeSignature');
        $bookingId = $request->request->get('bookingId');
        $isOpen = $request->request->get('isOpen');
        $this->allowSignatures($signatures, $typeSignature);
        $this->allowSignaturesForTrainer($bookingId, $typeSignature, $isOpen);

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getAttendanceContentAction(Request $request)
    {
        $id = $request->request->get('id');
        $absent = $request->request->get('absent');
        $method = $request->request->get('method');
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo    = $bookingModule->getModule();
        $profile       = $this->getCurrentUser();

        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookingModule);
        } else {
            if ($absent == 2) { // room management: only show the online user
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findPresenceBooking($profile, $moduleToDo, $bookingModule->getModuleSession());
            } else {
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                    'module'  => $moduleToDo,
                    'trainer' => $profile,
                    'moduleSession' => $bookingModule->getModuleSession()
                ));
            }
        }
        $session = [];
        if($bookingModule->getModuleSession()){
            $moduleSession = $bookingModule->getModuleSession();
            $start = $moduleSession->getSession()->getSessionDate();
            $session["sessionId"] = $moduleSession->getId();
            $session["startDate"] = $start;
            $session["startTime"] = $start->format('G:i');
            $session['session']   = $moduleSession->getSession();
            $end = clone $start;
            $end->setTime($start->format('H') + $moduleToDo->getDuration()->format('H'), $start->format('i') + $moduleToDo->getDuration()->format('i'));

            $session["endTime"] = $end->format('G:i');
            $session["address"] = "";
            $session["totalLearners"] = 0;
            $session["learners"] = [];
            if ($moduleSession->getSessionPlace()) {
                $session["address"] = $moduleSession->getSessionPlace()->getAddress() . ' - ' . $moduleSession->getSessionPlace()->getPlace();
            }
            if($bookingAgendas){
                $learners = 0;
                $learnerIds = [];
                $learnersGroup = [];
                foreach ($bookingAgendas as $booking){
                    $learner = $booking->getLearner()->getPerson();
                    $lgroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getId()));
                    $groupNames = [];
                    if ($lgroups) {
                        foreach ($lgroups as $learnerGroup) {
                            $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                        }
                    }
                    $groupName  = implode(", ", $groupNames);
                    $learnerIds[] = $learner->getId();
                    $learnersGroup[$learner->getId()] = ['booked' => $booking, 'groupName' => $groupName];
                    $learners++;
                }
                $session["totalLearners"] = $learners;
                $profiles = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array(
                    'person' => $learnerIds,
                ));
                $session["learners"] = $profiles;
                $session['learnersGroup'] = $learnersGroup;
            }
        } else { // for Individual Remote Session
            $hours = $minutes = 0;
            $start  = $bookingModule->getBookingDate();
            $end    = clone $start;
            $duration   = clone $bookingModule->getModule()->getDuration();
            $hours      += intval($duration->format('H'));
            $minutes    += intval($duration->format('i'));
            $end->add(new \DateInterval('PT' . $hours . 'H' . $minutes . 'M'));

            $session["sessionId"] = $bookingModule->getId();
            $session["startDate"] = $start;
            $session["startTime"] = $start->format('G:i');
            $session['session']   = null;
            $end = clone $start;
            $end->setTime($start->format('H') + $moduleToDo->getDuration()->format('H'), $start->format('i') + $moduleToDo->getDuration()->format('i'));

            $session["endTime"] = $end->format('G:i');
            $session["address"] = "";
            $session["totalLearners"] = 0;
            $session["learners"] = [];

            if($bookingAgendas){
                $learners = 0;
                $learnerIds = [];
                $learnersGroup = [];
                foreach ($bookingAgendas as $booking){
                    $learner = $booking->getLearner()->getPerson();
                    $lgroups = $this->getDoctrine()->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getId()));
                    $groupNames = [];
                    if ($lgroups) {
                        foreach ($lgroups as $learnerGroup) {
                            $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                        }
                    }
                    $groupName  = implode(", ", $groupNames);
                    $learnerIds[] = $learner->getId();
                    $learnersGroup[$learner->getId()] = ['booked' => $booking, 'groupName' => $groupName];
                    $learners++;
                }
                $session["totalLearners"] = $learners;
                $profiles = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array(
                    'person' => $learnerIds,
                ));
                $session["learners"] = $profiles;
                $session['learnersGroup'] = $learnersGroup;
            }
        }

        if (!empty($method)) {
            $em = $this->getDoctrine()->getManager();
            $moduleToDo->setSignatureMethod($method);
            $em->persist($moduleToDo);
            $em->flush($moduleToDo);
        }

        if (!$absent) {
            if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
                // todo: implement for trainer signature
                $sessionSignatures = null;
            } else {
                $sessionSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                    'profile' => $profile,
                    'session' => $bookingModule->getModuleSession()->getSession()
                ));
            }

            return $this->render('TrainerBundle:myExercices:ajax-attendance-sheet.html.twig', array(
                'module'                 => $moduleToDo,
                'booked'                 => $bookingModule,
                'bookingAgendas'         => $bookingAgendas,
                'absent'                 => $absent,
                'profile'                => $profile,
                'sessionSignatures'      => $sessionSignatures
            ));
        } else {
            if ($absent == 1) {
                return $this->render('TrainerBundle:myExercices:ajax-absent.html.twig', array(
                    'module'                 => $moduleToDo,
                    'booked'                 => $bookingModule,
                    'bookingAgendas'         => $bookingAgendas,
                    'absent'                 => $absent,
                    'session'                => $session,
                    'profile'                => $profile
                ));
            } else if ($absent == 2) {
                return $this->render('TrainerBundle:myExercices:ajax-room-management.html.twig', array(
                    'module'                 => $moduleToDo,
                    'booked'                 => $bookingModule,
                    'bookingAgendas'         => $bookingAgendas,
                    'absent'                 => $absent,
                    'session'                => $session,
                    'profile'                => $profile
                ));
            }
        }
    }

    public function saveSignatureMethodAction(Request $request)
    {
        $id = $request->request->get('id');
        $method = $request->request->get('method');
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo    = $bookingModule->getModule();

        $em = $this->getDoctrine()->getManager();
        $moduleToDo->setSignatureMethod($method);
        $em->persist($moduleToDo);
        $em->flush($moduleToDo);

        $response = new Response(json_encode(array("status" => 'OK')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function printAttendanceSheetAction($id, Request $request)
    {
        $profile       = $this->getCurrentUser();
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->find($id);
        $moduleToDo    = $bookingModule->getModule();

        if (StoredModule::ONLINE === $moduleToDo->getStoredModule()->getAppId()) {
            $bookingAgendas = array($bookingModule);
            $trainerSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                'profile'  => $profile
            ), array('signatureType' => 'ASC'));
        } else {
            $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'  => $moduleToDo,
                'trainer' => $profile,
                'moduleSession' => $bookingModule->getModuleSession()
            ));
            $trainerSignatures = $this->getDoctrine()->getRepository('ApiBundle:SessionSignature')->findBy(array(
                'profile'  => $profile,
                'session' => $bookingModule->getModuleSession()->getSession()
            ), array('signatureType' => 'ASC'));
        }

        return $this->render('TrainerBundle:myExercices:print-attendance-sheet.html.twig', array(
            'booked'         => $bookingModule,
            'module'         => $moduleToDo,
            'bookingAgendas' => $bookingAgendas,
            'trainerSignatures' => $trainerSignatures
        ));
    }

    public function saveSignatureAction(Request $request)
    {
        if ($request->request->get('sessionSignature')){
            return $this->saveSessionSignature($request);
        }
        return $this->saveSignature($request);
    }

    public function saveReportAction(Request $request, Swift_Mailer $mailer)
    {
        $em         = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $targetDir  = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/files/reports/';
        $array      = explode('.', $_FILES['file']['name']);
        $extension  = end($array);
        $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;

        if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) { // file uploaded succeeded
            $data    = $request->request->all();
            $trainer = $this->getCurrentUser();

            /** @var Module $module */
            $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
                'id' => $data['moduleId'],
            ));

            /** @var BookingAgenda $bookingAgenda */
            $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                'id' => $data['bookingAgendaId'],
            ));

            $moduleReports = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')->findOneBy(array(
                'session' => $bookingAgenda, 'module' => $module, 'trainer' => $trainer, 'status' => [ModuleReports::STATUS_TODO, ModuleReports::STATUS_REJECTED]
            ));

            if (!$moduleReports) {
                $moduleReports = new ModuleReports();
            }

            // save the FileDescriptor object
            $fileDescriptor = new FileDescriptor();
            $fileDescriptor->setName($_FILES['file']['name']);
            $fileDescriptor->setPath($fileName);
            $fileDescriptor->setSize($_FILES['file']['size']);
            $fileDescriptor->setMimeType($_FILES['file']['type']);
            $fileDescriptor->setDirectory($targetDir);

            $em->persist($fileDescriptor);
            $em->flush();

            // save the ModuleReports object
            $moduleReports->setTrainer($trainer);
            $moduleReports->setModule($module);
            $moduleReports->setFileDescriptor($fileDescriptor);
            $moduleReports->setSession($bookingAgenda);
            $moduleReports->setStatus(ModuleReports::STATUS_SUBMITTED);
            $moduleReports->setDateAdd(new DateTime());

            $em->persist($moduleReports);
            $em->flush();

            // send email alert to admin/supervisor
            $admins   = [];
            $moduleSends = $this->getDoctrine()->getRepository('ApiBundle:ModuleSend')->findBy(array(
                'module' => $module,
            ));
            if ($moduleSends) {
                foreach ($moduleSends as $send) {
                    $admins[] = $send->getProfile()->getOtherEmail();
                    $send->setStatus($em->getRepository('ApiBundle:Status')->findOneBy(['appId' => Status::REPORT_DONE]));
                    $em->persist($send);
                    $em->flush();
                }

                if ($admins) {
                    $message = (new Swift_Message($translator->trans('trainers.alerts.reports.reports_has_been_upload')))
                        ->setFrom(array('team@deepmark.com' => 'Deepmark'))
                        ->setTo($admins)
                        ->setContentType('text/html')
                        ->setBody(
                            $this->render('ApiBundle:Email:warningUploadReport.html.twig', array(
                                'name'    => $module->getdesignation(),
                                'start'   => $module->getBeginning()->format('Y-m-d H:i:s'),
                                'end'     => $module->getEnding()->format('Y-m-d H:i:s'),
                                'profile' => $bookingAgenda->getLearner(),
                                'joinUrl' => $this->generateUrl('trainer_exercices_video', array('id' => $bookingAgenda->getId()), 0)
                            )));
                    $mailer->send($message);
                }
            }
            // end send email alert to admin/supervisor

            $response = new Response(json_encode(array("uploaded" => true, 'file_name' => $_FILES['file']['name'])));
        } else { // file upload failed
            $response = new Response(json_encode(array("uploaded" => false, 'file_name' => 'File upload failed')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function saveAttendanceReportAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $targetDir  = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/files/reports/';
        $array      = explode('.', $_FILES['file']['name']);
        $extension  = end($array);
        $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;

        if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) { // file uploaded succeeded
            $data    = $request->request->all();
            $trainer = $this->getCurrentUser();

            /** @var Module $module */
            $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
                'id' => $data['moduleId'],
            ));

            $moduleAttendanceReport = $this->getDoctrine()->getRepository('ApiBundle:ModuleAttendanceReport')->findOneBy(array(
                'module' => $module, 'trainer' => $trainer
            ));

            if ($moduleAttendanceReport) {
                $fileDescriptorModel = $em->getRepository('ApiBundle:FileDescriptor')->find($moduleAttendanceReport->getFileDescriptor()->getId());
                $em->remove($fileDescriptorModel);
                $em->persist($fileDescriptorModel);
                $em->flush();
            } else {
                $moduleAttendanceReport = new ModuleAttendanceReport();
            }

            // save the FileDescriptor object
            $fileDescriptor = new FileDescriptor();
            $fileDescriptor->setName($_FILES['file']['name']);
            $fileDescriptor->setPath($fileName);
            $fileDescriptor->setSize($_FILES['file']['size']);
            $fileDescriptor->setMimeType($_FILES['file']['type']);
            $fileDescriptor->setDirectory($targetDir);

            $em->persist($fileDescriptor);
            $em->flush();

            // save the module Attendance Report object
            $moduleAttendanceReport->setTrainer($trainer);
            $moduleAttendanceReport->setModule($module);
            $moduleAttendanceReport->setFileDescriptor($fileDescriptor);

            $em->persist($moduleAttendanceReport);
            $em->flush();

            try {
                $storage = $this->container->get('storage');
                $storage->uploadFile($targetDir . $fileName);
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

            $response = new Response(json_encode(array("uploaded" => true, 'file_name' => $_FILES['file']['name'])));
        } else { // file upload failed
            $response = new Response(json_encode(array("uploaded" => false, 'file_name' => 'File upload failed')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function sendAlertAction(Request $request, Swift_Mailer $mailer)
    {
        $data    = $request->request->all();
        $learner = $this->getUserProfile($data['learnerId']);
        $late = $data['late'];

        $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $data['bookingAgendaId'],
        ));

        $translator = $this->get('translator');
        $subject = ($late == 15) ? $translator->trans('email.warning_late_15m') : $translator->trans('email.warning_late').' : ' . $late . ' ' .strtoupper($translator->trans('email.minutes'));
        $this->get(Email::class)->sendMailNewHasEmailWarningLate($learner, $bookingAgenda->getModule(), $bookingAgenda, $learner->getEntity()->getOrganisation()->getDesignation() . ', ' . $this->trans('email.informs_you') . ': ' . $subject, $late);

        $response = new Response(json_encode(true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function sendVirtualClassAlertsAction(Request $request, Swift_Mailer $mailer)
    {
        $late = $request->request->get('late');
        $alerts = $request->request->get('alerts');
        if ($alerts) {
            foreach ($alerts as $alert) {
                if ($alert['isAlert']) {
                    $bookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                        'id' => $alert['bookingId'],
                    ));
                    $translator = $this->get('translator');
                    $subject = ($late == 15) ? $translator->trans('email.warning_late_15m') : $translator->trans('email.warning_late').' : ' . $late . ' ' .strtoupper($translator->trans('email.minutes'));
                    $this->get(Email::class)->sendMailNewHasEmailWarningLate($bookingAgenda->getLearner(), $bookingAgenda->getModule(), $bookingAgenda, $bookingAgenda->getLearner()->getEntity()->getOrganisation()->getDesignation() . ', ' . $translator->trans('email.informs_you') . ': ' . $subject, $late);
                }
            }
        }

        $response = new Response(json_encode('Done'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function cancelSessionAction(Request $request)
    {
        $data    = $request->request->all();
        $trainer = $this->getCurrentUser();
        $learner = $this->getUserProfile($data['learnerId']);

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $data['moduleId'],
        ));

        /** @var BookingAgenda $booking */
        $booking = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $data['bookingAgendaId'],
        ));

        $moduleCancel = new ModuleCancel();
        $moduleCancel->setCancelType($data['statusId']);
        $moduleCancel->setLearner($learner);
        $moduleCancel->setTrainer($trainer);
        $moduleCancel->setModule($module);
        $moduleCancel->setSession($booking);
        $em = $this->getDoctrine()->getManager();
        $em->persist($moduleCancel);
        $em->flush();

        if ($booking->getModule()->getStoredModule()->getAppId() == StoredModule::ONLINE) {
            $booking->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_CANCELED,
            )));
            $em->persist($booking);
            $em->flush();

            $this->get(Email::class)->sendMailBookingCancelForLearner($booking);
        } else {
            $bookings = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBy(['module' => $booking->getModule()]);
            foreach ($bookings as $k => $booked) {
                $booked->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_CANCELED,
                )));
                $em->persist($booked);
                $em->flush();
                $this->get(Email::class)->sendMailBookingCancelForLearner($booked);
            }
        }

        $response = new Response(json_encode($moduleCancel ? $moduleCancel->getId() : false));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deleteFileAction(Request $request)
    {
        $deleteId   = $request->request->get('deleteId');
        $deleteType = $request->request->get('deleteType');
        $url = $request->request->get('url');
        $em = $this->getDoctrine()->getManager();
        if ($deleteType == 'report') {
            $reportModule = $this->getDoctrine()->getRepository(ModuleReports::class)->find($deleteId);
            $fileDescriptor = $reportModule->getFileDescriptor();
            $em->remove($fileDescriptor);
            $em->remove($reportModule);
        }
        elseif ($deleteType == 'attendance') {
            $attendanceModule = $this->getDoctrine()->getRepository(ModuleAttendanceReport::class)->find($deleteId);
            $fileDescriptor = $attendanceModule->getFileDescriptor();
            $em->remove($fileDescriptor);
            $em->remove($attendanceModule);
        }
        $em->flush();
        return $this->redirect($url);
    }

    public function addModuleReport(BookingAgenda $bookingAgenda, Module $module, Profile $trainer, $status)
    {
        $em = $this->getDoctrine()->getManager();
        $existedReport = $this->getDoctrine()
            ->getRepository('ApiBundle:ModuleReports')
            ->findOneBy(['module' => $module, 'trainer' => $trainer, 'status' => $status]);
        if (!$existedReport) {
            // add report with begin status
            $report = new ModuleReports();
            $report->setStatus(ModuleReports::STATUS_TODO);
            $report->setTrainer($trainer);
            $report->setModule($module);
            $report->setSession($bookingAgenda);
            $report->setDateAdd(new DateTime());
            $em->persist($report);
            $em->flush();
        }
    }
}
