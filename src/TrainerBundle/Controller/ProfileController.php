<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\InterventionPerimeter;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\ModuleTrainerResponse;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileDocument;
use ApiBundle\Entity\Skill;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use AppBundle\Entity\PersonTimeZone;
use Controller\BaseController;
use DateTime;
use Doctrine\Common\Persistence\ObjectRepository;
use Exception;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TrainerBundle\Exception\BadRequest;

/**
 * Class ProfileController
 * @package TrainerBundle\Controller
 */
class ProfileController extends BaseController
{
    /**
     * @return ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * @return ObjectRepository
     */
    private function getSkillRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Skill');
    }

    /**
     * @return ObjectRepository
     */
    private function getSkillResourceVarietyProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:SkillResourceVarietyProfile');
    }

    /**
     * @param $appId
     *
     * @return LiveResourceVariety|object
     */
    private function getLiveResourceVarietyByAppId($appId)
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
            'appId' => $appId,
        ));
    }


    public function uploadProfileDocument(Profile $profile, $targetDir, $myFile, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $fileCount = count($myFile["name"]);
        for ($i = 0; $i < $fileCount; $i++) {
            $array      = explode('.', $myFile['name'][$i]);
            $extension  = end($array);
            $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
            $targetFile = $targetDir . $fileName;

            if (move_uploaded_file($myFile['tmp_name'][$i], $targetFile)) {

                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($myFile['name'][$i]);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize($myFile['size'][$i]);
                $fileDescriptor->setMimeType(pathinfo( $myFile['name'][$i], PATHINFO_EXTENSION));
                $fileDescriptor->setDirectory($myFile['tmp_name'][$i]);
                $em->persist($fileDescriptor);

                $profileDocument = new ProfileDocument();
                $profileDocument->setProfile($profile);
                $profileDocument->setFileDescriptor($fileDescriptor);
                $profileDocument->setDocumentType($type);
                $profileDocument->setCreated(new DateTime());
                $profileDocument->setUpdated(new DateTime());
                $em->persist($profileDocument);
                $em->flush();

            }
        }
    }

    public function saveDocumentsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profileId = $request->request->get('profileId');
        $profileEntity = $em->getRepository(Profile::class)->findOneBy(['id'=>$profileId]);
        $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile/';

        if (isset($_FILES['billing'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['billing'],
                ProfileDocument::BANK_ACCOUNT_STATEMENT);
        } elseif (isset($_FILES['identite'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['identite'],
                ProfileDocument::IDENTITY_DOCUMENT);
        } elseif (isset($_FILES['social'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['social'],
                ProfileDocument::SOCIAL_DOCUMENT);
        } elseif (isset($_FILES['assurance'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['assurance'],
                ProfileDocument::ASSURANCE_DOCUMENT);
        } elseif (isset($_FILES['contrat_cadre'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['contrat_cadre'],
                ProfileDocument::CONTRACT_DOCUMENT);
        } elseif (isset($_FILES['contrat_mission'])) {
            $this->uploadProfileDocument($profileEntity, $targetDir, $_FILES['contrat_mission'],
                ProfileDocument::MISSION_DOCUMENT);
        }
        $session = new Session();
        $session->set('documentTab', 2);
        return $response = new Response(json_encode(array("uploaded" => true, 'file_name' => 'File upload success')));
    }

    /**
     * Index view
     * @return Response
     */
    public function showAction()
    {
        $ranking = [];
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        $rankingGrades = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
            ->findBy(array('trainer' => $profile));

        if ($rankingGrades) {
            $grade = 0;
            foreach ($rankingGrades as $key => $item) {
                $grade += $item->getNotation();
            }
            $ranking['grade'] = round($grade / ($key + 1), 2);
        }

        $identityDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::IDENTITY_DOCUMENT, 'profile' => $this->getUser()
        ));
        $socialDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::SOCIAL_DOCUMENT, 'profile' => $this->getUser()
        ));
        $assuranceDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::ASSURANCE_DOCUMENT, 'profile' => $this->getUser()
        ));
        $contractDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::CONTRACT_DOCUMENT, 'profile' => $this->getUser()
        ));
        $missionDocs = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::MISSION_DOCUMENT, 'profile' => $this->getUser()
        ));

        $extIban = $profile->getIban() != null ? pathinfo($profile->getIban(), PATHINFO_EXTENSION) : null;

        return $this->render('TrainerBundle:Profile:show.html.twig', array(
            'user' => $user,
            'profile' => $profile,
            'mentor_skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                'profile' => $profile,
                'liveResourceVariety' => $this->getLiveResourceVarietyByAppId(1),
            )),
            'live_skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                'profile' => $profile,
                'liveResourceVariety' => $this->getLiveResourceVarietyByAppId(2),
            )),
            'ranking' => $ranking,
            'identityDoc' => $identityDoc ? $identityDoc : null,
            'socialDoc' => $socialDoc ? $socialDoc : null,
            'assuranceDoc' => $assuranceDoc ? $assuranceDoc : null,
            'contractDoc' => $contractDoc ? $contractDoc : null,
            'missionDocs' => $missionDocs ? $missionDocs : null,
            'extIban' => $extIban
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function skillsListAction(Request $request)
    {
        // khi thay đổi admin trong trainer get id profile
        $id = empty($request->query->get('id')) ? $this->getUser()->getId() : $request->query->get('id');
        if(empty($request->query->get('id'))) {
            return $this->render('TrainerBundle:Profile:skills-list.html.twig', array(
                'skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                    'profile' => $this->getProfileRepository()->findOneBy(array(
                        'person' => $id,
                    )),
                    'liveResourceVariety' => $this->getLiveResourceVarietyByAppId($request->query->get('type')),
                )),
                
            ));
        }else {
            return $this->render('TrainerBundle:Profile:skills-list.html.twig', array(
                'skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                    'profile' => $this->getProfileRepository()->findOneBy(array(
                        'person' => $id,
                    )),
                    'liveResourceVariety' => $this->getLiveResourceVarietyByAppId($request->query->get('type')),
                )),
                'editAdmin' => 1, // thay đổi trainer phía admin
                'id' => $id, // id cần thay đổi
            ));
        }
        
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        /*$workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);*/

        $rankingGrades = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->findBy(array(
            'trainer' => $profile,
        ));

        $status = $this->getDoctrine()->getRepository('ApiBundle:LiveResourceOrigin')->findAll();
        $perimeters = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->findAll();

        $ranking = [];

        if ($rankingGrades) {
            $grade = 0;
            foreach ($rankingGrades as $k => $item) {
                $grade += $item->getNotation();
            }
            $ranking['grade'] = round($grade / ($k + 1), 2);
        }
        $fileFormatCv = $fileFormatVideo = '';
        if($profile->getCv()){
            $cvInfo = pathinfo($profile->getCv());
            $fileFormatCv = $cvInfo['extension'];
        }
        if($profile->getVideo()){
            $videoInfo = pathinfo($profile->getVideo());
            $fileFormatVideo = $videoInfo['extension'];
        }

        $extIban = $profile->getIban() != null ? pathinfo($profile->getIban(), PATHINFO_EXTENSION) : null;

        $identityDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::IDENTITY_DOCUMENT, 'profile' => $profile
        ));

        $socialDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::SOCIAL_DOCUMENT, 'profile' => $profile
        ));
        $assuranceDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::ASSURANCE_DOCUMENT, 'profile' => $profile
        ));
        $contractDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::CONTRACT_DOCUMENT, 'profile' => $profile
        ));
        $missionDocs = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::MISSION_DOCUMENT, 'profile' => $profile
        ));

        $statement   = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'documentType' => ProfileDocument::BANK_ACCOUNT_STATEMENT,
            'profile'      => $profile,
        ));

        $cvTemplateDoc = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
            'documentType' => ProfileDocument::CV_DOCUMENT
        ));
        $timeZones = $this->getDoctrine()->getRepository(PersonTimeZone::class)->findAll();

        $alertCheckInfo = 0;
        $session = new Session();
        // set and get session attributes
        $checkFistLogin = $session->get('last_login');
        if ($checkFistLogin == -1) {
            $alertCheckInfo = 1;
            $session->set('last_login', null);
        }

        return $this->render('TrainerBundle:Profile:edit.html.twig', array(
            'statement'     => $statement,
            'fileFormatCv' => $fileFormatCv,
            'fileFormatVideo' => $fileFormatVideo,
            'timeZones' => $timeZones,
            'profile' => $profile,
            'person' => $user,
            //'workingHours' => $workingHours,
            'mentor_skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                'profile' => $profile,
                'liveResourceVariety' => $this->getLiveResourceVarietyByAppId(1),
            )),
            'live_skills' => $this->getSkillResourceVarietyProfileRepository()->findBy(array(
                'profile' => $profile,
                'liveResourceVariety' => $this->getLiveResourceVarietyByAppId(2),
            )),
            'available_skills' => $this->getSkillRepository()->findAll(),
            'ranking' => $ranking,
            'identityDoc' => $identityDoc ? $identityDoc : null,
            'socialDoc' => $socialDoc ? $socialDoc : null,
            'assuranceDoc' => $assuranceDoc ? $assuranceDoc : null,
            'contractDoc' => $contractDoc ? $contractDoc : null,
            'missionDocs' => $missionDocs ? $missionDocs : null,
            'cvTemplateDoc' => $cvTemplateDoc ? $cvTemplateDoc : null,
            'extIban' => $extIban,
            'rankingGrades' => $rankingGrades,
            'alertCheckInfo' => $alertCheckInfo,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile),
            'status' => $status,
            'perimeters' => $perimeters
        ));
    }

    public function savePasswordAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $entityManager = $this->getDoctrine()->getManager();
        $password = $request->request->get('password');
        $rpPassword = $request->request->get('rpPassword');


        $profile->setUpdateDate(new DateTime());
        $user->setPlainPassword($rpPassword);
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->updatePassword($user);
        $entityManager->persist($user);
        $entityManager->persist($profile);
        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return $this->render('TrainerBundle:Profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('trainer_profile_edit');
    }

    public function saveAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));
        $entityManager = $this->getDoctrine()->getManager();
        $lastName = $request->request->get('lastName');
        $firstName = $request->request->get('firstName');
        $fixedPhone = $request->request->get('fixedPhone');
        $mobilePhone = $request->request->get('mobilePhone');
        $employId = $request->request->get('employeeId');
        $otherEmail = $request->request->get('otherEmail');
//        $number = $request->request->get('number');
//        $street = $request->request->get('street');
//        $district = $request->request->get('district');
//        $city = $request->request->get('city');
//        $region = $request->request->get('region');

        $profile->setLastName($lastName);
        $profile->setFirstName($firstName);
        $profile->setFixedPhone($fixedPhone);

        $profile->setMobilePhone($mobilePhone);
        $profile->setOtherEmail($otherEmail);
        $profile->setEmployeeId($employId);

//        $profile->setNumber($number);
//        $profile->setStreet($street);
//        $profile->setDistrict($district);
//        $profile->setCity($city);
//        $profile->setRegion($region);
        $profile->setUpdateDate(new DateTime());
        $entityManager->persist($user);
        $entityManager->persist($profile);
        try {
            $entityManager->flush();
            $this->addFlash('succeed', 'Profile updated successfully');
        } catch (Exception $e) {
            return $this->render('TrainerBundle:profile:edit.html.twig', array(
                'error' => "all the required fields aren't specified",
            ));
        }
        return $this->redirectToRoute('trainer_profile_edit');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addSkillAction(Request $request)
    {
        try {
            $this->validateIsAjax($request);
            $idUser = empty($request->request->get('idUser')) ? $this->getUser()->getId() : $request->request->get('idUser');
            $type = $this->getLiveResourceVarietyFromRequest($request);

            $liveResourceSkill = $this->addSkillProfile(
                $this->getSkillRepository()->find($request->get('skill')),
                $this->getProfileRepository()->findOneBy(['person' => $idUser]),
                $type
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($liveResourceSkill);
            $entityManager->flush();

            return $this->json(['status' => 'success', 'type' => $type->getAppId()]);
        } catch (BadRequest $e) {
            $logger = $this->get('logger');
            $logger->error($e);

            return $this->json(['status' => 'error', 'msg' => $e->getMessage()])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            $logger = $this->get('logger');
            $logger->error($e);

            return $this->json(['status' => 'error'])->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteSkillAction(Request $request)
    {
        $skillId = $request->request->get('deleteId');
        $editAdmin = $request->request->get('editAdmin');
        $id = empty($request->request->get('id')) ? $this->getUser()->getId() : $request->request->get('id');

        $skill = $this->getSkillRepository()->find($skillId);
        $liveResourceSkill = $this->removeSkillProfile(
            $skill,
            $this->getProfileRepository()->findOneBy(['person' => $id]),
            LiveResourceVariety::LIVE_TRAINER
        );
        if($liveResourceSkill){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($liveResourceSkill[0]);
            $entityManager->flush();
        }
        if(!empty($editAdmin) && $editAdmin == 1) {
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }
        return $this->redirectToRoute('trainer_profile_edit');

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function removeSkillAction(Request $request)
    {
        try {
            $this->validateIsAjax($request);
            $id = empty($request->query->get('id')) ? $this->getUser()->getId() : $request->query->get('id');
            $skill = $this->getSkillRepository()->find($request->get('skill'));

            $liveResourceSkill = $this->removeSkillProfile(
                $skill,
                $this->getProfileRepository()->findOneBy(['person' => $id]),
                $this->getLiveResourceVarietyFromRequest($request)
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($liveResourceSkill);
            $entityManager->flush();

            return $this->json(['status' => 'success']);
        } catch (BadRequest $e) {
            $logger = $this->get('logger');
            $logger->error($e);

            return $this->json(['status' => 'error', 'msg' => $e->getMessage()])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            $logger = $this->get('logger');
            $logger->error($e);

            return $this->json(['status' => 'error'])->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailableUserSkillsAction(Request $request)
    {
        try {
            $this->validateIsAjax($request);
            $id = empty($request->query->get('id')) ? $this->getUser()->getId() : $request->query->get('id');
            
            $allSkillsArray = array_reduce(
                $this->getSkillRepository()->findAll(),
                function ($carry, Skill $item) {
                    $carry = $carry ?? [];
                    $carry[$item->getId()]['name'] = $item->getDesignation();
                    $carry[$item->getId()]['desc'] = $item->getSkillDescription();

                    return $carry;
                }
            );
            $list = [];
            if ($id != -1) {
                $list =  $this->getSkillResourceVarietyProfileRepository()
                    ->findBy([
                        'profile' => $this->getProfileRepository()
                            ->findOneBy(['person' => $id])->getId(),
                        'liveResourceVariety' => $this->getLiveResourceVarietyFromRequest($request),
                    ]);
            }
            $userSkillsData = array_reduce(
                $list,
                function ($carry, SkillResourceVarietyProfile $item) {
                    $carry = $carry ?? [];
                    array_push($carry, $item->getSkill()->getId());

                    return $carry;
                }
            );

            foreach ((array)$userSkillsData as $skillAlreadyAchievedId) {
                unset($allSkillsArray[$skillAlreadyAchievedId]);
            }
            return $this->json($allSkillsArray);
        } catch (Exception $e) {
            $logger = $this->get('logger');
            $logger->error($e);

            return $this->json([])->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @param $skill
     * @param $profile
     * @param $type
     * @return SkillResourceVarietyProfile
     * @throws Exception
     */
    private function addSkillProfile($skill, $profile, $type)
    {

        $skillResource = new SkillResourceVarietyProfile();
        $skillResource->setSkill($skill);
        $skillResource->setProfile($profile);
        $skillResource->setLiveResourceVariety($type);
        $skillResource->setScore(3);
        $skillResource->setDateAdd(new DateTime());

        return $skillResource;
    }

    /**
     * @param $skill
     * @param $profile
     * @param $type
     * @return BookingAgenda[]|ModuleTrainerResponse[]|object[]
     */
    private function removeSkillProfile($skill, $profile, $type)
    {
        return $this->getSkillResourceVarietyProfileRepository()
            ->findBy([
                'profile' => $profile->getId(),
                'skill' => $skill->getId(),
                'liveResourceVariety' => $type,
            ]);
    }

    /**
     * @param Request $request
     * @return LiveResourceVariety|object
     */
    private function getLiveResourceVarietyFromRequest(Request $request)
    {
        switch ($request->get('type')) {
            case 'trainer':
                return $this->getLiveResourceVarietyByAppId(LiveResourceVariety::LIVE_TRAINER);
                break;
            case 'mentor':
                return $this->getLiveResourceVarietyByAppId(LiveResourceVariety::MENTOR);
                break;
            default:
                throw new BadRequest('missing proper resource type');
        }
    }

    public function mediaFileUploadAction(Request $request)
    {
        $profileFolder = '/files/profile/';
        $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . $profileFolder;
        $fileType = isset($_FILES['avatarFile']) ? 'avatar' : (isset($_FILES['videoFile']) ? 'video' : (isset($_FILES['cvFile']) ? 'cv' : 'iban'));
        $fileTemp = isset($_FILES['avatarFile']) ? $_FILES['avatarFile'] : (isset($_FILES['videoFile']) ? $_FILES['videoFile'] : (isset($_FILES['cvFile']) ? $_FILES['cvFile'] : $_FILES['ibanFile']));
        $array = explode('.', $fileTemp['name']);
        $extension = end($array);
        $fileName = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;
        $profile = [];
        if (move_uploaded_file($fileTemp['tmp_name'], $targetFile)) {
            if ($this->getUser()->isRole('ROLE_ADMIN')) {
                $profile = $this->getProfileRepository()->findOneBy(array('id' => $_POST['id']));
            } else {
                $profile = $this->getProfileRepository()->findOneBy(array('person' => $this->getUser()));
            }
            $em = $this->getDoctrine()->getManager();
            if ($fileType == 'avatar') {
                $profile->setAvatar($profileFolder . $fileName);
            } elseif ($fileType == 'video') {
                $profile->setVideo($profileFolder . $fileName);
            } elseif ($fileType == 'iban') {
                $profile->setIban($profileFolder . $fileName);
            } else {
                $profile->setCv($profileFolder . $fileName);
            }
            $em->flush();
            $response = new Response(json_encode(array("uploaded" => $fileType, 'file_name' => $profileFolder . $fileName)));
        } else {
            $response = new Response(json_encode(array("uploaded" => false, 'file_name' => 'File upload failed')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deleteFileAction(Request $request)
    {
        $fileType = $request->request->get('fileProfileType') ? $request->request->get('fileProfileType') : null;

        if ($fileType) {
            $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
            $profile = $this->getProfileRepository()->findOneBy(array('id' => $request->request->get('profileId')));
            $em = $this->getDoctrine()->getManager();
            if ($fileType == 'avatar') {
                if (file_exists($targetDir . $profile->getAvatar())) {
                    unlink($targetDir . $profile->getAvatar());
                }
                $profile->setAvatar(null);
            } elseif ($fileType == 'video') {
                if (file_exists($targetDir . $profile->getVideo())) {
                    unlink($targetDir . $profile->getVideo());
                }
                $profile->setVideo(null);
            } elseif ($fileType == 'iban') {
                if (file_exists($targetDir . $profile->getIban())) {
                    unlink($targetDir . $profile->getIban());
                }
                $profile->setIban(null);
            } elseif ($fileType == 'cv') {
                if (file_exists($targetDir . $profile->getCv())) {
                    unlink($targetDir . $profile->getCv());
                }
                $profile->setCv(null);
            }
            $em->flush();

        }
        if ($this->getUser()->isRole('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_trainers_edit', array('id' => $request->request->get('profileId')));
        }
        return $this->redirectToRoute('trainer_profile_edit');
    }

    public function deleteDocAction(Request $request)
    {
        $profileId = $request->request->get('profileId');
        $docId = $request->request->get('docId');
        if ($docId) {
            $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile';
            $file = $this->getDoctrine()->getRepository('ApiBundle:FileDescriptor')->findOneBy(array(
                'id' => $docId,
            ));
            if (file_exists($targetDir . $file->getPath())) {
                unlink($targetDir . $file->getPath());
            }

            $documentTrainer = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findOneBy(array(
                'fileDescriptor' => $docId,
            ));

            $em = $this->getDoctrine()->getManager();
            $em->remove($documentTrainer);
            $em->remove($file);
            $em->flush();

        }
        if ($this->getUser()->isRole('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_trainers_edit', array('id' => $request->request->get('profileId')));
        }
        return $this->redirectToRoute('trainer_profile_edit');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveBillingAction(Request $request)
    {
        $bankName = $request->request->get('bankName');
        $accountCurrency = $request->request->get('accountCurrency');
        $iban = $request->request->get('iban');
        $bic = $request->request->get('bic');
        $bankCode = $request->request->get('bankCode');
        $agencyCode = $request->request->get('agencyCode');
        $accountNum = $request->request->get('accountNum');
        $ribKey = $request->request->get('ribKey');
        $domiciliationAgency = $request->request->get('domiciliationAgency');
        $profileId =  $request->request->get('profileId');
        $user = $this->getUser();
        $profile = $this->getProfileRepository()->find($profileId);
        $entityManager = $this->getDoctrine()->getManager();

        $profile->setBankName($bankName);
        $profile->setAccountCurrency($accountCurrency);
        $profile->setIban($iban);
        $profile->setBic($bic);
        $profile->setBankCode($bankCode);
        $profile->setAgencyCode($agencyCode);
        $profile->setAccountNum($accountNum);
        $profile->setRibKey($ribKey);
        $profile->setDomiciliationAgency($domiciliationAgency);

        $entityManager->persist($profile);
        $entityManager->flush();

        return $this->json(['status' => 'success']);

    }
    public function saveInterventionPerimeterAction(Request $request)
    {
        $interventionPerimeterId = $request->request->get('interventionPerimeter');
        $user = $this->getUser();
        $interventionPerimeter = $this->getDoctrine()->getRepository('ApiBundle:InterventionPerimeter')->find($interventionPerimeterId);
        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        $entityManager = $this->getDoctrine()->getManager();

        $profile->setInterventionPerimeter($interventionPerimeter);
        $entityManager->persist($profile);
        $entityManager->flush();

        return $this->json(['status' => 'success']);

    }

    public function getCourseLearnersBySearchAction(Request $request)
    {
        $adminLearnersController = $this->get('AdminLearners');
        return $adminLearnersController->getCourseLearnersBySearchAction($request);
    }

}
