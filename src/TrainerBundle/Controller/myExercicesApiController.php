<?php

namespace TrainerBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\Marks;
use ApiBundle\Entity\QuizQuestionsItems;
use ApiBundle\Service\QuizServiceAPI;
use ApiBundle\Service\UploadFile;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class myExercicesApiController extends RestController
{
    /**
     * @Rest\Get("save-record")
     *
     * Display save record
     */
    public function saveRecordAction(Request $request, UploadFile $fileApi, QuizServiceAPI $quizServiceAPI) {
        // Get params
        $params = $request->request->all();
        // Check type to get specs for upload
        $fileConf = $this->checkUploadType($params);
        if ($fileConf === false) {
            // Error upload not configured or bad params
            $datas = array(
                'status' => 'fail',
                'error' => 'Not configured or bad params');
        } else {
            // Upload can be executed
            $file = $request->files->get('file');
            $validation = true;
            $errors = [];

            // Validate type
            if (!in_array($file->getClientMimeType(), $this->upload_config['validation']['mimeTypes'])) {
                $validation = false;
                $errors[] = "<b>Bad file format. <i>Accepted :</i></b> " . implode(', ', $this->upload_config['validation']['mimeTypes']) . " / <b><i>Yours :</i></b> " . $file->getClientMimeType();
            }
            // Validate size
            if ($file->getClientSize() >= $this->upload_config['validation']['maxSize']) {
                $validation = false;
                $errors[] = "<b>Bad file size. <i>Accepted :</i></b> " . ($this->upload_config['validation']['maxSize'] / 1000000) . "Mo / <b><i>Yours :</i></b> " . number_format($file->getClientSize() / 1000000, 2) . 'Mo';
            }

            if ($validation === true) {
                // File API
                $fileApi->setTargetDirectory($this->upload_config['path']);
                if($file->getClientMimeType() == 'video/webm') {
                    $ext = 'webm';
                } else {
                    $ext = $file->getClientOriginalExtension();
                }
                $fileDescriptor = $fileApi->upload($file, $ext);

                // If recording, update answer in db
                $quizServiceAPI->updateRecordAnswerByTrainer($params['exercices_id'], $params['question_item_id'], $fileDescriptor->getPath());

                // Response
                $datas = array(
                    'status' => 'success',
                    'file' => $fileDescriptor);
            } else {
                // Response
                $datas = array(
                    'status' => 'error',
                    'errors' => implode('<br />', $errors)
                );
            }
        }

        // View
        $view = View::create();
        $view->setData($datas);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("save-free")
     *
     * Display save record
     */
    public function saveFreeAction(Request $request, UploadFile $fileApi, QuizServiceAPI $quizServiceAPI) {
        // Get params
        $params = $request->request->all();
        $quizServiceAPI->updateRecordAnswerByTrainer($params['exercices_id'], $params['question_item_id'], $params['editor_content']);

        $data = array('status' => 'success');
        // View
        $view = View::create();
        $view->setData($data);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("save-exercises-status")
     *
     * Display save record
     */
    public function saveExercisesStatusAction(Request $request, UploadFile $fileApi, QuizServiceAPI $quizServiceAPI) {
        // Get params
        $params = $request->request->all();
        $quizServiceAPI->saveExercisesStatus($params['exercices_id'], Exercises::STATUS_SUBMITTED);

        $data = array('status' => 'success');
        // View
        $view = View::create();
        $view->setData($data);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("save-grade")
     *
     * Display save grade
     */
    public function saveGradeAction(Request $request, UploadFile $fileApi, QuizServiceAPI $quizServiceAPI) {
        // Get params
        $params = $request->request->all();
        $quizServiceAPI->saveGradeByTrainer($params['exercices_id'], $params['grade']);

        $data = array('status' => 'success');
        // View
        $view = View::create();
        $view->setData($data);
        return $this->handleView($view);
    }

    /**
     * Check upload Type
     *
     * @param array $params
     * @return array
     */
    private function checkUploadType($params) {
        if (isset($params['type'])) {
            // Base Path Quiz
            $path = '/web/files/quiz_learner/';
            switch ($params['type']) {
                // Quiz CSV upload for each question
                case 'quiz_xls' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        // Add path
                        $path .= $this->quiz_id . '/xls/';
                        // Validation params
                        $accepted_type = array('application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz image upload for each question
                case 'quiz_img' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/img/';
                        // Validation params
                        $accepted_type = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_doc' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/doc/';
                        // Validation params
                        $accepted_type = array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-word', 'application/msword');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_ppt' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/ppt/';
                        // Validation params
                        $accepted_type = array(
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.ms-powerpoint');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz audio upload for each question
                case 'quiz_audio' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/audio/';
                        // Validation params
                        $accepted_type = array('audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/x-aac', 'audio/x-wav', 'audio/midi', 'audio/webm', 'audio/3gpp', 'audio/3gpp2', 'audio/aac', 'audio/wav');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz video upload for each question
                case 'quiz_video' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/video/';
                        // Validation params
                        $accepted_type = array('video/quicktime', 'video/3gpp2', 'video/3gpp', 'video/x-flv', 'video/mp4', 'video/x-msvideo', 'video/mpeg', 'video/ogg', 'video/webm');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz doc upload for each question
                case 'quiz_template' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];
                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/doc/';
                        // Validation params
                        $accepted_type = array(
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/pdf',
                            'application/msword',
                            'text/plain',
                            'text/csv');
                        $accepted_size = 100000000; // 100Mo
                    } else {
                        return false;
                    }
                    break;
                // Quiz recording upload for each question
                case 'quiz_recording' :
                    if (isset($params['quiz_id']) && isset($params['question_id'])) {
                        // Init Ids
                        $this->quiz_id = $params['quiz_id'];

                        $this->question_id = $params['question_id'];
                        // Add path
                        $path .= $this->quiz_id . '/recording/';
                        // Validation params
                        $accepted_type = array('video/webm', 'audio/webm');
                        $accepted_size = 150000000; // 150Mo
                    } else {
                        return false;
                    }
                    break;
                default :
                    return false;
                    break;
            }

            $this->upload_config = array(
                'path' => $path,
                'validation' => [
                    'maxSize' => $accepted_size,
                    'mimeTypes' => $accepted_type
                ],
            );
        } else {
            return false;
        }
    }
}
