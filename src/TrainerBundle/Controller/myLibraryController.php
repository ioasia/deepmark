<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class myLibraryController
 * @package TrainerBundle\Controller
 */
class myLibraryController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $bookedAgendas = $this->getDoctrine()
            ->getRepository(BookingAgenda::class)
            ->findModulesByProfile($this->getCurrentProfile());

        return $this->render('TrainerBundle:myLibrary:index.html.twig', array(
            'bookedAgendas' => $bookedAgendas,
            'nextSession'         => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null,$profile)

        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if (!$fileDescriptor->getPath() || (!$form->isSubmitted() && !!$form->isValid())) {
            return $this->render('TrainerBundle:myLibrary:upload.html.twig', array(
                'form' => $form->createView(),
            ));
        }

        $fileDescriptor = $fileApi->uploadPrivateFile($fileDescriptor->getPath());

        $em = $this->getDoctrine()->getManager();

        $em->persist($fileDescriptor);
        $document = new EducationalDocument();
        $document->setName($fileDescriptor->getName());
        $document->setFileDescriptor($fileDescriptor);

        $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

        switch ($mimeType) {
            case 'audio':
                $appId = EducationalDocVariety::AUDIO;
                break;
            case 'video':
                $appId = EducationalDocVariety::VIDEO;
                break;
            default:
                $appId = EducationalDocVariety::DOCUMENT;
                break;
        }
        $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                'appId' => $appId,
            )
        ));
        $em->persist($document);
        $em->flush();

        return $this->redirect($this->generateUrl('trainer_library_index'));
    }
}
