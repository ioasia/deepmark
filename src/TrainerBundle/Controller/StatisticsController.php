<?php

namespace TrainerBundle\Controller;

use AdminBundle\Controller\StatsController;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Entity;
use ApiBundle\Entity\Group;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\Organisation;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\Statistics\KeyNameConst;
use DateTime;

/**
 * Class StatisticsController
 * @package TrainerBundle\Controller
 */
class StatisticsController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $person        = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)
            ->findBy(array('trainer' => $person));

        $statisticsSearchParam = [];

        foreach ($bookingAgenda as $book) {
            $course                                             = $book->getModule()->getIntervention();
            $module                                             = $book->getModule();
            $statisticsSearchParam['courses'][$course->getId()] = $course;
            $statisticsSearchParam['modules'][$module->getId()] = $module;
        }

        $status                              = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findBy(['appId' => [Status::LEARNER_INTERVENTION_CREATED, Status::WITH_BOOKING_BOOKED, Status::WITH_BOOKING_DONE, Status::LEARNER_INTERVENTION_IN_PROGRESS]]);
        $statisticsSearchParam['status']     = $status;
        $moduleType                          = $this->getDoctrine()->getRepository('ApiBundle:StoredModule')
            ->findBy(
                ['appId' => [StoredModule::ONLINE, StoredModule::VIRTUAL_CLASS, StoredModule::PRESENTATION_ANIMATION]]
            );
        $statisticsSearchParam['moduleType'] = $moduleType;

        return $this->render('TrainerBundle:statistics:index.html.twig', array(
            'organisations'         => $this->getOrganisations($profile),
            'statisticsSearchParam' => $statisticsSearchParam,
            'nextSession'           => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null,$profile)

        ));
    }

    public function getOrganisations(Profile $profile)
    {
        $organisations = [];
        $interventions = [];
        $assigmentResourceSystems = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(['liveResource' => $profile]);
        foreach ($assigmentResourceSystems as $assigmentResourceSystem) {
            $intervention = $assigmentResourceSystem->getModule()->getIntervention();
            $learnerInterventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')->findBy(['intervention' => $intervention]);
            if (!in_array($assigmentResourceSystem->getModule()->getIntervention(), $interventions) && count($learnerInterventions)) {
                $interventions[] = $assigmentResourceSystem->getModule()->getIntervention();
                foreach ($learnerInterventions as $learnerIntervention) {
                    $organisation = $learnerIntervention->getLearner()->getEntity()->getOrganisation();
                    if ($organisation && !in_array($organisation, $organisations)) {
                        $organisations[] = $organisation;
                    }
                }
            }
        }
        return $organisations;
    }

    public function sectorWorkplaceAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $workplaces = [];
        $entityId = $request->request->get('entityId');
        $organizationId =  $request->request->get('organizationId');
        $regionalId = $request->request->get('regionalId');
        $sectorId = $request->request->get('sectorId');
        if($sectorId == 0){
            $sectors = [];

            if($regionalId == 0){
                $regions = [];
                if($entityId == 0){
                    $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                    $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                    foreach ($entityLists as $entity){
                        $regionalLists = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
                        $regions = array_merge($regions, $regionalLists != null ? $regionalLists : []);
                    }
                } else {
                    $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
                    $regions = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);

                }
                foreach($regions as $region){
                    $sectorLists     = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $region));
                    $sectors = array_merge($sectors, $sectorLists != null ? $sectorLists : []);

                }

            } else {
                $regional  = $em->getRepository('ApiBundle:Regional')->find($regionalId);
                $sectors   = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));

            }
            if ($sectors){
                foreach($sectors as $sector){
                    $workplaceLists = $em->getRepository('ApiBundle:Workplace')->findBy(array('groupOrganization' => $sector));
                    $workplaces = array_merge($workplaces, $workplaceLists != null ? $workplaceLists : []);
                }
            }

        } else {
            $sector  = $em->getRepository('ApiBundle:GroupOrganization')->find($sectorId);
            $workplaces = $em->getRepository('ApiBundle:Workplace')->findBy(array('groupOrganization' => $sector));

        }
        $result     = [];
        foreach ($workplaces as $workplace) {
            $result[] = array('id' => $workplace->getId(), 'designation' => $workplace->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }


    public function regionSectorAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $sectors = [];
        $entityId = $request->request->get('entityId');
        $organizationId =  $request->request->get('organizationId');
        $regionalId = $request->request->get('regionalId');

        if($regionalId == 0){
            $regions = [];
            if($entityId == 0){
                $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                foreach ($entityLists as $entity){
                    $regionalLists = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);
                    $regions = array_merge($regions, $regionalLists != null ? $regionalLists : []);
                }
            } else {
                $entity = $em->getRepository('ApiBundle:Entity')->find($entityId);
                $regions = $em->getRepository('ApiBundle:Regional')->findBy(['entity' => $entity]);

            }
            foreach($regions as $region){
                $sectorLists     = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $region));
                $sectors = array_merge($sectors, $sectorLists != null ? $sectorLists : []);
            }
        } else {
            $regional  = $em->getRepository('ApiBundle:Regional')->find($regionalId);
            $sectors     = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));
        }
        $result     = [];
        foreach ($sectors as $sector) {
            $result[] = array('id' => $sector->getId(), 'designation' => $sector->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    public function entityRegionAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $organizationId =  $request->request->get('organizationId');
        $regions = [];
        if ($entityId == 0) {
            $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity){
                $regionalLists = $entity->getRegionals();
                $regions = array_merge($regions, $regionalLists != null ? $regionalLists->toArray() : []);
            }
        } else {
            $entity   = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $regions     = $em->getRepository('ApiBundle:Regional')->findBy(array('entity' => $entity));
        }

        $result     = [];
        foreach ($regions as $region) {
            $result[] = array('id' => $region->getId(), 'designation' => $region->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function organizationEntityAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $organizationId = $request->request->get('organizationId');
        $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
        $entityLists     = $em->getRepository('ApiBundle:Entity')->findBy(array('organisation' => $organization));
        $result     = [];
        foreach ($entityLists as $entity) {
            $result[] = array('id' => $entity->getId(), 'designation' => $entity->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function entityCourseAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $courses = [];
        $organizationId =  $request->request->get('organizationId');
        if($entityId == 0){
            $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity){
                $courseLists = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
                $courses = array_merge($courses, $courseLists != null ? $courseLists : []);
            }
        } else {
            $entity   = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $courses  = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
        }
        $result     = [];
        foreach ($courses as $course) {
            $result[] = array('id' => $course->getId(), 'designation' => $course->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function courseModuleAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $modules = [];
        $organizationId =  $request->request->get('organizationId');
        $courseId = $request->request->get('courseId');
        $courses = [];
        if($courseId == 0){

            if ($entityId == 0) {
                $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
                $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
                foreach ($entityLists as $entity){
                    $courseLists = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
                    $courses = array_merge($courses, $courseLists != null ? $courseLists : []);
                }
            } else {
                $entity   = $em->getRepository('ApiBundle:Entity')->find($entityId);
                $courses  = $em->getRepository('ApiBundle:Intervention')->findBy(array('entity' => $entity));
            }

            foreach($courses as $course){
                $moduleList    = $em->getRepository('ApiBundle:Module')->findBy(array('intervention' => $course));
                $modules = array_merge($modules, $moduleList != null ? $moduleList : []);
            }

        } else {
            $course   = $em->getRepository('ApiBundle:Intervention')->find($courseId);
            $modules  = $em->getRepository('ApiBundle:Module')->findBy(array('intervention' => $course));
        }
        $result     = [];
        foreach ($modules as $module) {
            if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId() ||
                StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
            ) {
                continue;
            }
            $result[] = array('id' => $module->getId(), 'designation' => $module->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }


    public function entityGroupAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $entityId = $request->request->get('entityId');
        $groups = [];
        $organizationId =  $request->request->get('organizationId');
        if ($entityId == 0) {
            $organization   = $em->getRepository('ApiBundle:Organisation')->find($organizationId);
            $entityLists = $this->getDoctrine()->getRepository(Entity::class)->findBy(['organisation' => $organization]);
            foreach ($entityLists as $entity){
                $groupLists = $em->getRepository('ApiBundle:Group')->findBy(array('entity' => $entity));
                $groups = array_merge($groups, $groupLists != null ? $groupLists : []);
            }
        } else {
            $entity   = $em->getRepository('ApiBundle:Entity')->find($entityId);
            $groups     = $em->getRepository('ApiBundle:Group')->findBy(array('entity' => $entity));
        }
        $result     = [];
        foreach ($groups as $group) {
            $result[] = array('id' => $group->getId(), 'designation' => $group->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getStatisticByTypeAction(Request $request)
    {
        $type = $request->get('type');
        $options = $request->query->all();
        $from = $request->get('from') ? (new \DateTime($request->get('from'))) : (new \DateTime('first day of this month'));
        $to   = $request->get('to') ? (new \DateTime($request->get('to'))) : new \DateTime('last day of this month');
        $data = [];

        $organizationId = $request->get('organizationId');
        $organisation = $this->getDoctrine()->getRepository(Organisation::class)
            ->findOneBy([KeyNameConst::KEY_ID => $organizationId]);
        $options[KeyNameConst::KEY_ORGANISATION] = $organisation;
        //$options[KeyNameConst::KEY_ORGANISATION] = $this->getEntityFromProfile()->getOrganisation();
        switch ($type) {
            case StatsController::STATISTIC_BY_MODULE_RESULTS:
                $data = $this->getStatisticByModuleResultInRangeTime($from, $to, $options);
                break;
            case StatsController::STATISTIC_BY_GLOBAL:
                $data = $this->getStatisticByGlobal($from, $to, $options);
                if ($options['isExport'] == 'true') {
                    return $this->getExportData(reset($data), $options);
                }
                break;
            default:
                break;
        }
        $view = $this->renderView('statistics/module_table.html.twig', ['statisticType' => $type, 'statisticData' => $data]);
        return new JsonResponse($view);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array data of statistics by course module
     */
    private function getStatisticByGlobal(DateTime $from, DateTime $to, array $options = []){
        // call the AdminStats service
        $adminStatsController = $this->get('AdminStats');
        $data = [];
        $now = new \DateTime('now');

        $numActivePerson = 0;
        $totalSpentTime = 0;
        $averageSpentTime = 0;

        $averageTrainerGrade = 0;

        $numActiveSupervisor = 0;
        $numInactiveSupervisor = 0;
        $numNewSupervisor = 0;

        $totalCourses = 0;
        $numCourseInProgress = 0;
        $totalCourseGrade = 0;
        $numCourseEnd = 0;
        $totalSize = 0;

        $numOfIndividualClass = 0;
        $numOfVirtualClassroom = 0;
        $numOfWorkshop = 0;
        $numOfClassroom = 0;
        $numOfQuiz = 0;
        $numOfElearning = 0;
        $numOfAssessment = 0;
        $numOfIndividualClassBooked = 0;
        $numOfIndividualClassCancelled = 0;
        $numOfIndividualClassEnd = 0;

        $numOfVirtualClassroomBooked = 0;
        $numOfVirtualClassroomCancelled = 0;
        $numOfVirtualClassroomEnd = 0;

        $numOfWorkshopBooked = 0;
        $numOfWorkshopCancelled = 0;
        $numOfWorkshopEnd = 0;

        $numOfClassroomBooked = 0;
        $numOfClassroomCancelled = 0;
        $numOfClassroomEnd = 0;

        $numOfQuizEnd = 0;

        $numOfElearningEnd = 0;

        $numOfAssessmentEnd = 0;

        $numOfMicroLearning = 0;
        $numOfMicroLearningEnd = 0;

        $profileType  = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
            ->findOneBy(['appId' => ProfileVariety::SUPERVISOR]);

        // Learners
        $learnerType  = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
            ->findOneBy(['appId' => ProfileVariety::LEARNER]);

        $totalLearners = $this->getDoctrine()->getRepository(Profile::class)
            ->totalProfileByRole($to, $learnerType->getId(), $options);

        $totalActivePerson = $this->getDoctrine()->getRepository(Profile::class)
            ->totalActiveProfileByRole($to, $learnerType->getId(), $options);

        $totalInActivePerson = $this->getDoctrine()->getRepository(Profile::class)
            ->totalInActiveProfileByRole($to, $learnerType->getId(), $options);

        $totalNewPerson = $this->getDoctrine()->getRepository(Profile::class)
            ->totalNewProfileByRole($from, $to, $learnerType->getId(), $options);

        // Trainers
        $trainerType  = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
            ->findOneBy(['appId' => ProfileVariety::TRAINER]);

        $totalTrainers = $this->getDoctrine()->getRepository(Profile::class)
            ->totalProfileByRole($to, $trainerType->getId(), $options);

        $totalActiveTrainers = $this->getDoctrine()->getRepository(Profile::class)
            ->totalActiveProfileByRole($to, $trainerType->getId(), $options);

        $totalInActiveTrainers = $this->getDoctrine()->getRepository(Profile::class)
            ->totalInActiveProfileByRole($to, $trainerType->getId(), $options);

        $totalNewTrainers = $this->getDoctrine()->getRepository(Profile::class)
            ->totalNewProfileByRole($from, $to, $trainerType->getId(), $options);

        // Supervisor
        $supervisorType  = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')
            ->findOneBy(['appId' => ProfileVariety::SUPERVISOR]);

        $totalSupervisors = $this->getDoctrine()->getRepository(Profile::class)
            ->totalProfileByRole($to, $supervisorType->getId(), $options);

        $totalActiveSupervisors = $this->getDoctrine()->getRepository(Profile::class)
            ->totalActiveProfileByRole($to, $supervisorType->getId(), $options);

        $totalInActiveSupervisors = $this->getDoctrine()->getRepository(Profile::class)
            ->totalInActiveProfileByRole($to, $supervisorType->getId(), $options);

        $totalNewSupervisors = $this->getDoctrine()->getRepository(Profile::class)
            ->totalNewProfileByRole($from, $to, $supervisorType->getId(), $options);


        // Training Groups
        $totalTrainingGroups = $this->getDoctrine()->getRepository(Group::class)
            ->countTotalGroupByEntity($to, $options);

        $newTrainingGroups = $this->getDoctrine()->getRepository(Group::class)
            ->countNewGroup($from, $to, $options);

        $adminStatsController->getInterventionLists($from, $to, $options);

        $interventions = $adminStatsController->getInterventionLists($from, $to, $options);
        $numCourseNew = count($interventions);

        $interventionUntilTo = $adminStatsController->getInterventionLists($from, $to, $options, true);
        foreach ($interventionUntilTo as $intervention){
            // Modules
            $modules = $this->getDoctrine()->getRepository(Module::class)
                ->findBy(array(KeyNameConst::KEY_INTERVENTION => $intervention));
            foreach ($modules as $module){
                $moduleType = $module->getStoredModule()->getAppId();
                switch ($moduleType){
                    case StoredModule::ONLINE:
                        $numOfIndividualClass++;
                        $numOfIndividualClassBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfIndividualClassCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfIndividualClassEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::VIRTUAL_CLASS:
                        $numOfVirtualClassroom++;
                        $numOfVirtualClassroomBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfVirtualClassroomCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfVirtualClassroomEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ONLINE_WORKSHOP:
                        $numOfWorkshop++;
                        $numOfWorkshopBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfWorkshopCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfWorkshopEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::PRESENTATION_ANIMATION:
                        $numOfClassroom++;
                        $numOfClassroomBooked += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::BOOKED);
                        $numOfClassroomCancelled += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::CANCELED);
                        $numOfClassroomEnd += $this->getDoctrine()->getRepository(BookingAgenda::class)
                            ->countBookingByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::QUIZ:
                        $numOfQuiz++;
                        $numOfQuizEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                            ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ELEARNING:
                        $numOfElearning++;
                        $numOfElearningEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                            ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::ASSESSMENT:
                        $numOfAssessment++;
                        $numOfAssessmentEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                            ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    case StoredModule::EVALUATION:
                        $numOfMicroLearning++;
                        $numOfMicroLearningEnd += $this->getDoctrine()->getRepository(ModuleIteration::class)
                            ->countModuleByStatus($module->getId(), Status::DONE);
                        break;
                    default:
                        break;
                }
                // Stokage information
                $educationalDocuments = $module->getEducationalDocument();
                $trainerDocuments = $module->getTrainerDocument();
                $mediaDocument = $module->getMediaDocument();

                if ($educationalDocuments) {
                    foreach ($educationalDocuments as $educationalDocument) {
                        if ($educationalDocument) {
                            $totalSize += $educationalDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($trainerDocuments) {
                    foreach ($trainerDocuments as $trainerDocument) {
                        if ($trainerDocument) {
                            $totalSize += $trainerDocument->getFileDescriptor()->getSize();
                        }
                    }
                }
                if ($mediaDocument && !empty($mediaDocument->getFileDescriptor())) {
                    $totalSize += $mediaDocument->getFileDescriptor()->getSize();
                }
            }
            // Courses
            $totalCourses ++;
            $numCourseInProgress += ($intervention->getBeginning() <= $now && $intervention->getEnding() >= $now) ? 1 : 0;
            $numCourseEnd += ($now > $intervention->getEnding()) ? 1 : 0;

            $learnersNotation = $this->getDoctrine()->getRepository(ModuleResponse::class)
                ->getResultsByIntervention($intervention->getId());

            $nbNotation    = 0;
            $notation      = 0;
            $totalNotation = 0;
            foreach ($learnersNotation as $learnerNotation) {
                ++$nbNotation;
                $notation += $learnerNotation->getNotation();
            }

            if ($nbNotation) {
                $totalNotation = $notation / $nbNotation;
            }

            $intervention->setLearnersNotation($totalNotation);

            $totalCourseGrade += $intervention->getlearnersNotation();

            // Learners
            $numActivePerson += $this->getDoctrine()->getRepository(LearnerIntervention::class)
                ->countLearnerByStatus($intervention->getId(), 1);

            $totalSpentTime += $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->countTotalSpentTimeByInterventionId($intervention->getId());
            $averageSpentTime = $numActivePerson > 0 ? $totalSpentTime/$numActivePerson : 0;

            // calculation the average trainer grade for each intervention
            $assigmentResources = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
                ->getTrainersByInterventionId($intervention->getId());
            if ($assigmentResources) {
                $grade = 0;
                foreach ($assigmentResources as $assigmentResource) {
                    $trainer = $assigmentResource->getLiveResource();
                    if ($trainer) {
                        $grade += $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
                            ->getRankingByTrainer($trainer);
                    }
                }
                $averageTrainerGrade += $grade > 0 ? $grade / count($assigmentResources) : 0;
            }

            // Supervisors
            $numActiveSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 1);

            $numInactiveSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 0);

            $numNewSupervisor += $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->countProfileByRole($intervention->getEntity()->getId(), $profileType->getId(), 1, 1);
        }
        $averageTrainerGrade = $averageTrainerGrade > 0 ? $averageTrainerGrade / $totalTrainers : 0;
        $data[] = [
            'learners' => [
                'total'             => $totalLearners,
                'active'            => $totalActivePerson,
                'inactive'          => $totalInActivePerson,
                'new'               => $totalNewPerson,
                'totalSpentTime'    => number_format($totalSpentTime/3600, 2, '.', ','),
                'averageSpentTime'  => number_format($averageSpentTime/3600, 2, '.', ','),
            ],
            'trainers' => [
                'total'                 => $totalTrainers,
                'active'                => $totalActiveTrainers,
                'inactive'              => $totalInActiveTrainers,
                'new'                   => $totalNewTrainers,
                'averageTrainerGrade'   => $averageTrainerGrade,
            ],
            'supervisors' => [
                'total'             => $totalSupervisors,
                'active'            => $totalActiveSupervisors,
                'inactive'          => $totalInActiveSupervisors,
                'new'               => $totalNewSupervisors,
            ],
            'courses' => [
                'total'             => $totalCourses,
                'started'           => $numCourseInProgress,
                'ended'             => $numCourseEnd,
                'new'               => $numCourseNew,
                'averageGrade'      => $totalCourses > 0 ? number_format($totalCourseGrade/$totalCourses, 2, '.', ',') : 0,
            ],
            'groups' => [
                'total'         => $totalTrainingGroups,
                'new'           => $newTrainingGroups,
            ],
            'stokage' => [
                'total'         => '50 GB',
                'used'          => $adminStatsController->convertFromBytes($totalSize),
                'remaining'     => $adminStatsController->convertFromBytes(53687091200 - $totalSize),
            ],
            'modules' => [
                'eLearning' => ['total' => $numOfElearning, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfElearningEnd],
                'quiz' => ['total' => $numOfQuiz, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfQuizEnd],
                'microLearning' => ['total' => $numOfMicroLearning, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfMicroLearningEnd],
                'assignment' => ['total' => $numOfAssessment, 'booked' => 'N/A', 'cancelled' => 'N/A', 'ended' => $numOfAssessmentEnd],
                'individual' => ['total' => $numOfIndividualClass, 'booked' => $numOfIndividualClassBooked, 'cancelled' => $numOfIndividualClassCancelled, 'ended' => $numOfIndividualClassEnd],
                'virtual' => ['total' => $numOfVirtualClassroom, 'booked' => $numOfVirtualClassroomBooked, 'cancelled' => $numOfVirtualClassroomCancelled, 'ended' => $numOfVirtualClassroomEnd],
                'workshop' => ['total' => $numOfWorkshop, 'booked' => $numOfWorkshopBooked, 'cancelled' => $numOfWorkshopCancelled, 'ended' => $numOfWorkshopEnd],
                'presentation' => ['total' => $numOfClassroom, 'booked' => $numOfClassroomBooked, 'cancelled' => $numOfClassroomCancelled, 'ended' => $numOfClassroomEnd],
            ],
        ];
        return $data;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param array $options
     * @return array
     * @throws \Exception
     */
    private function getStatisticByModuleResultInRangeTime(DateTime $from, DateTime $to, array $options = []){
        // call the AdminStats service
        $adminStatsController = $this->get('AdminStats');
        $data = [];
        $interventions = $adminStatsController->getInterventionLists($from, new \DateTime('last day of December this year +1 years'), $options, true);
        foreach ($interventions as $intervention){
            $learnersInterventions = $intervention->getLearners();
            $modules = $adminStatsController->getModuleListByInterventionAndConditionFilter($intervention, $options);
            foreach ($learnersInterventions as $learnerIntervention){
                $learner = $learnerIntervention->getLearner();
                if ($adminStatsController->checkUserByConditionFilters($learner, $options)) {
                    foreach ($modules as $module) {
                        if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                            if ($adminStatsController->checkBookingStatusByConditionFilters($learner, $module, $options)) {
                                $bookingAgenda = $this->getDoctrine()->getRepository(BookingAgenda::class)
                                    ->findOneBy(array(KeyNameConst::KEY_MODULE => $module, KeyNameConst::KEY_LEARNER => $learner));
                                if ($bookingAgenda && ($from <= $bookingAgenda->getBookingDate() && $bookingAgenda->getBookingDate() <= $to)) {
                                    $learnerInfo = $adminStatsController->getLearnerInfo($learner);
                                    if (in_array($options[KeyNameConst::KEY_GROUP_ID], $learnerInfo[KeyNameConst::KEY_GROUP_ID]) || $options[KeyNameConst::KEY_GROUP_ID] == 0) {
                                        $status = $bookingAgenda != null && $bookingAgenda->getStatus() ? $bookingAgenda->getStatus()->getDesignation() : KeyNameConst::VALUE_N_A;
                                        $scoreArr = $adminStatsController->getScoreAndBookingInfoByLearnerAndModule($module, $learner);
                                        $moduleName = $module->getDesignation();
                                        $moduleType = $module->getStoredModule()->getDesignation();
                                        $moduleDuration   = $module->getDuration()->format('H:i:s');

                                        $end = clone $bookingAgenda->getBookingDate();
                                        $hour = (int)$module->getDuration()->format('G') + $bookingAgenda->getBookingDate()->format('G');
                                        $minutes = (int)$module->getDuration()->format('i') + $bookingAgenda->getBookingDate()->format('i');
                                        $end->setTime($hour, $minutes, 0);

                                        $dataRow = [
                                            KeyNameConst::KEY_COURSE_NAME => $intervention->getDesignation(),
                                            KeyNameConst::KEY_STATUS => $status,
                                            KeyNameConst::KEY_MODULE_NAME => $moduleName,
                                            KeyNameConst::KEY_MODULE_TYPE => $moduleType,
                                            KeyNameConst::KEY_TRAINER => !empty($bookingAgenda->getTrainer()) ? $bookingAgenda->getTrainer()->getDisplayName() : KeyNameConst::VALUE_N_A,
                                            KeyNameConst::KEY_DURATION => $moduleDuration,
                                            KeyNameConst::KEY_BOOKED_DATE => $bookingAgenda->getBookingDate(),
                                            KeyNameConst::KEY_BOOKING_SESSION => $bookingAgenda->getBookingDate()->format('H:i').' - '.$end->format('H:i'),
                                        ];
                                        $data[] = array_merge($dataRow, $learnerInfo, $scoreArr);
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        return $data;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $person                = $this->getUser();
        $bookingAgenda         = $this->getDoctrine()->getRepository(BookingAgenda::class)
            ->findBy(array('trainer' => $person));
        $statisticsSearchParam = [];

        foreach ($bookingAgenda as $agenda) {
            $course                                             = $agenda->getModule()->getIntervention();
            $module                                             = $agenda->getModule();
            $statisticsSearchParam['courses'][$course->getId()] = $course;
            $statisticsSearchParam['modules'][$module->getId()] = $module;
        }

        $status                              = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->findBy(
                [
                    'appId' => [
                        Status::LEARNER_INTERVENTION_CREATED,
                        Status::WITH_BOOKING_BOOKED,
                        Status::WITH_BOOKING_DONE,
                        Status::LEARNER_INTERVENTION_IN_PROGRESS
                    ]
                ]
            );
        $statisticsSearchParam['status']     = $status;
        $moduleType                          = $this->getDoctrine()->getRepository('ApiBundle:StoredModule')
            ->findBy(
                [
                    'appId' => [
                        StoredModule::ONLINE,
                        StoredModule::VIRTUAL_CLASS,
                        StoredModule::PRESENTATION_ANIMATION
                    ]
                ]
            );
        $statisticsSearchParam['moduleType'] = $moduleType;

        $statisticsModule  = [];
        $bookingStatistics = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findBookingStatistics($person, $request->request->all());

        foreach ($bookingStatistics as $book) {
            $module           = $book->getModule();
            $learner          = $book->getLearner();
            $score            = $book->getLearnerGrade() != null ? $book->getLearnerGrade() . '/100' : 'N/A';
            $firstName        = $learner->getFirstName();
            $lastName         = $learner->getLastName();
            $otherEmail       = $learner->getOtherEmail();
            $employeeId       = $learner->getEmployeeId();
            $courseName       = $module->getIntervention()->getDesignation();
            $moduleName       = $module->getDesignation();
            $moduleType       = $module->getStoredModule()->getDesignation();
            $status           = $book->getStatus() != null ? $book->getStatus()->getDesignation() : 'N/A';
            $moduleEnd        = $module->getEnding();
            $moduleDuration   = $module->getDuration();
            $moduleIterations = $module->getIteration();
            $realtime         = '';

            $iteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array('learner' => $learner, 'module' => $module));
            $realtime  = $iteration != null && $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : 'N/A';

//            $realTime = ;
            $moduleResponse = $this->getDoctrine()
                ->getRepository('ApiBundle:ModuleResponse')
                ->findOneBy(['module' => $module, 'learner' => $learner]);
            $moduleRating   = $moduleResponse != null ? $moduleResponse->getNotation() : 'N/A';
            $moduleComment  = $moduleResponse != null ? $moduleResponse->getDesignation() : 'N/A';

            $moduleTrainerResponse = $rankingGrades = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
                ->findOneBy(array('trainer' => $person, 'module' => $module));
            $moduleTrainerRating   = $moduleTrainerResponse != null ? $moduleTrainerResponse->getNotation() : 'N/A';
            $moduleTrainerComment  = $moduleTrainerResponse != null ? $moduleTrainerResponse->getDesignation() : 'N/A';
            $statisticsModule[]    = array('id'         => $module->getId(), 'firstName' => $firstName, 'lastName' => $lastName, 'otherEmail' => $otherEmail, 'employeeId' => $employeeId,
                                           'courseName' => $courseName, 'moduleName' => $moduleName, 'moduleType' => $moduleType, 'status' => $status, 'moduleEnd' => $moduleEnd, 'moduleDuration' => $moduleDuration, 'realtime' => $realtime,
                                           'score'      => $score, 'moduleRating' => $moduleRating, 'moduleComment' => $moduleComment, 'moduleTrainerRating' => $moduleTrainerRating, 'moduleTrainerComment' => $moduleTrainerComment);
        }

        return $this->render('TrainerBundle:statistics:index.html.twig', array(
            'statistics'            => $statisticsModule,
            'statisticsSearchParam' => $statisticsSearchParam
        ));
    }
}
