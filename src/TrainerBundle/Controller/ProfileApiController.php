<?php

namespace TrainerBundle\Controller;

use AdminBundle\Helper\Traits\HelperTrait;
use AdminBundle\Services\LearnerService;
use AdminBundle\Services\TrainerBooking\TrainerBookingService;
use ApiBundle\Controller\RestController;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use Traits\Controller\HasUsers;
use Utils\Email;

/**
 * ProfileApi controller.
 */
class ProfileApiController extends RestController
{
    use HasUsers;
    use \LearnerBundle\Controller\HelperTrait;


    /**
     * @Rest\Post("attachmentSession")
     *
     * @param ParamFetcher $paramFetcher
     * @param TranslatorInterface $translator
     * @param TrainerBookingService $trainerBookingService
     * @param LearnerService $learnerService
     * @Rest\RequestParam(name="moduleId", nullable=false, strict=true, description="module")
     * @Rest\RequestParam(name="sessionId", nullable=false, strict=true, description="session")
     * @Rest\RequestParam(name="selectedProfiles", nullable=false, strict=true, description="Learners.")
     *
     * @return Response
     */
    public function attachmentSessionAction(ParamFetcher $paramFetcher, TranslatorInterface $translator
        , TrainerBookingService $trainerBookingService, LearnerService $learnerService)
    {
        $moduleId  = $paramFetcher->get('moduleId');
        $sessionId = $paramFetcher->get('sessionId');
        $learnerIds = $paramFetcher->get('selectedProfiles');

        $learners = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->findBy(array(
                'id' => $learnerIds,
            ));

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->error('Not found module');
        }

        $storedModule = $module->getStoredModule()->getAppId();

        if ((StoredModule::VIRTUAL_CLASS !== $storedModule) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModule) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModule)
        ) {
            return $this->error('Not a session module');
        }

        /** @var ModuleSession $moduleSession */
        $moduleSessionRepository = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession');
        $moduleSession = $moduleSessionRepository->findOneBy(array(
            'id' => $sessionId,
        ));

        if (!$moduleSession) {
            return $this->error($translator->trans('booking.session_messages.no_session_module'));
        }

        $unAvailableTimesLearners = $learnerService->validateLearnersToSessionByTrainer($moduleSession, $learners, $module);
        if($unAvailableTimesLearners){
            return $this->error($translator->trans('booking.session_messages.learner_not_available_session', ['%learnerNames%' => implode(", ", $unAvailableTimesLearners)]));
        }

        $start   = $moduleSession->getSession()->getSessionDate();
        $end     = clone $start;
        $hour    = (int)$module->getDuration()->format('G') + (int)$moduleSession->getSession()->getSessionDate()->format('G');
        $minutes = (int)$module->getDuration()->format('i') + (int)$moduleSession->getSession()->getSessionDate()->format('i');
        $end->setTime($hour, $minutes, 0);

        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $moduleAssignment = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findOneBy([
                'module' => $module,
                'moduleSession' => $moduleSession->getSession()
            ]);
            if (!$moduleAssignment) {
                return $this->error($translator->trans('booking.session_messages.no_assignment_session'));
            }
            $trainer          = $moduleAssignment->getLiveResource();
            if (!$trainer){
                return $this->error($translator->trans('booking.session_messages.no_assignment_session'));
            }

            $em              = $this->getDoctrine()->getManager();
            foreach ($learners as $learner){
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));

                $isBooked = false;
                if($bookingAgendas) {
                    foreach ($bookingAgendas as $booked) {
                        if ($booked->getModuleSession()->getId() === $moduleSession->getId()) {
                            $isBooked = true;
                            $bookingAgenda = $booked;
                            continue;
                        }
                        $em->remove($booked);
                    }
                    $em->flush();
                }

                if ($isBooked == false) {
                    $bookingAgenda = new BookingAgenda();
                    $bookingAgenda->setBookingDate($moduleSession->getSession()->getSessionDate());
                    $bookingAgenda->setLearner($learner);
                    $bookingAgenda->setTrainer($trainer);
                    $bookingAgenda->setModule($module);

                    $bookingAgenda->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::WITH_BOOKING_BOOKED,
                    )));

                    $this->setStatusModuleIteration($learner, $module, Status::WITH_BOOKING_BOOKED);
                    $bookingAgenda->setModuleSession($moduleSession);
                    $em->persist($bookingAgenda);
                }
                $em->flush();
                // end set Status::WITH_BOOKING_CONFIRMED status for all booked with Status::WITH_BOOKING_BOOKED status
                $this->get(Email::class)->sendMailBookingConfirm($learner, $module, $bookingAgenda);
            }
            // send mail to trainer one time when learner booking
            if(!$trainerBookingService->hasSentMailBookingConfirm($module, $moduleSession)) {
                $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingAgenda);
            }
        } else{
            return $this->warningMissing('Warning :', $errors);
        }

        return $this->json(['error' => false, 'message' => 'Assign successful']);
    }


    private function warningMissing($message, $errors)
    {
        $schema = [];
        if ($errors) {
            foreach ($errors as $error) {
                $schema['errors'][$error->getPropertyPath()] = $error->getMessage();
            }
        }

        return $this->json(['error' => true, 'message' =>"$message ". json_encode($schema)]);
    }

    private function error($message){
        return $this->json([
            'error' => true,
            'message' => $message
        ]);
    }

}
