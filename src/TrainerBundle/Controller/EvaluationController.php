<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\DetailedEvaluation;
use ApiBundle\Entity\DetailedEvaluationLine;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use ApiBundle\Form\DetailedEvaluationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EvaluationController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $evaluations = $em->getRepository('ApiBundle:DetailedEvaluation')->findAll();

        return $this->render('TrainerBundle:Evaluation:index.html.twig', array(
            'evaluations' => $evaluations,
        ));
    }

    /**
     * Creation of an evaluation.
     *
     * @param Request $request
     *
     * @return FormInterface|Response
     */
    public function newAction()
    {
        return $this->render('TrainerBundle:Evaluation:new.html.twig');
    }

    /**
     * Create Evalution.
     *
     * @param Request $request
     */
    public function addAction(Request $request)
    {
        $reponse = new Response();
        $em = $this->getDoctrine()->getManager();

        $data = $request->request->get('evaluation');

        $evaluation = new DetailedEvaluation();
        $intervention = $em->getRepository(Intervention::class)
            ->find($data['intervention']);

        $admin = $em->getRepository(Profile::class)
            ->find($data['admin']);

        $learner = $em->getRepository(Profile::class)
            ->find($data['learner']);

        $evaluation->setIntervention($intervention);
        $evaluation->setAdmin($admin);
        $evaluation->setLearner($learner);

        $notation = 0;

        $em->persist($evaluation);

        foreach ($data['evalRows'] as $row) {
            $line = new DetailedEvaluationLine();
            $line->setDetailEvaluation($evaluation);
            $line->setTitle($row['title']);
            $line->setWording($row['wording']);
            $line->setNotation($row['notation']);

            $notation += $row['notation'];
            $em->persist($line);
        }

        $evaluation->setGlobalScore($notation);
        $em->flush();

        return $reponse->setContent($evaluation->getId());
    }

    /**
     * Fonction qui retourne les informations lié à un compte rendu.
     *
     * @param DetailedEvaluation $evaluation
     *
     * @return Response
     */
    public function showAction(DetailedEvaluation $evaluation, $id)
    {
        $evaluation = $this->getDoctrine()->getRepository(DetailedEvaluation::class)->find($id);
        $deleteForm = $this->createDeleteForm($evaluation);
        $lines = $this->getDoctrine()->getRepository('AppBundle:DetailedEvaluationLine')
            ->findBy(['detailEvaluation' => $evaluation]);

        return $this->render('TrainerBundle:Evaluation:show.html.twig', array(
            'evaluation' => $evaluation,
            'lines' => $lines,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Request            $request
     * @param DetailedEvaluation $eval
     *
     * @return object|\Symfony\Component\Form\FormInterface|JsonResponse|null
     */
    public function editAction(Request $request, DetailedEvaluation $eval)
    {
        $deleteForm = $this->createDeleteForm($eval);

        $editForm = $this->createForm(DetailedEvaluationType::class, $eval);
        $editForm->handleRequest($request);

        $lines = $this->getDoctrine()->getRepository(DetailedEvaluationLine::class)
            ->findBy(['detailEvaluation' => $eval]);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('trainer_evaluation_edit', array('id' => $eval->getId()));
        }

        return $this->render('TrainerBundle:Evaluation:edit.html.twig', array(
            'evaluation' => $eval,
            'lines' => $lines,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a person entity.
     *
     * @param Request            $request
     * @param DetailedEvaluation $evaluation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, DetailedEvaluation $evaluation)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createDeleteForm($evaluation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lines = $em->getRepository('ApiBundle:DetailedEvaluationLine')
                ->findBy(['detailEvaluation' => $evaluation]);

            foreach ($lines as $line) {
                $em->remove($line);
            }
            $em->remove($evaluation);
            $em->flush();
        }

        return $this->redirectToRoute('trainer_evaluation_index');
    }

    /**
     * @param DetailedEvaluation $eval
     *
     * @return FormInterface
     */
    private function createDeleteForm(DetailedEvaluation $eval)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trainer_evaluation_delete', array('id' => $eval->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
