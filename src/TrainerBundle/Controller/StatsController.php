<?php


namespace TrainerBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StatsController extends Controller
{

    public function organizationEntityAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->organizationEntityAction($request);
    }

    public function entityCourseAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->entityCourseAction($request);
    }

    public function entityGroupAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->entityGroupAction($request);
    }

    public function entityRegionAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->entityRegionAction($request);
    }

    public function regionSectorAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->regionSectorAction($request);
    }

    public function sectorWorkplaceAction(Request $request)
    {
        $adminStatsController = $this->get('AdminStats');
        return $adminStatsController->sectorWorkplaceAction($request);
    }
}