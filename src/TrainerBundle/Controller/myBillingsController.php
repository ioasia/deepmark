<?php

namespace TrainerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class myBillingsController extends Controller
{
    public function indexAction()
    {
        $invoices = $this->getInvoiceByMonth();

        return $this->render('TrainerBundle:myBillings:index.html.twig', $invoices);
    }

    public function invoiceByMonthAction(Request $request)
    {
        $month = $request->request->get('month');

        return $this->getInvoiceByMonth($month);
    }

    private function getLearnerByModule($module)
    {
        $booking = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
            ->findBy(['module' => $module]);

        $learners = [];

        foreach ($booking as $item) {
            array_push($learners, $item->getProfile());
        }

        return $learners;
    }

    private function getInvoiceByMonth($month = null)
    {
        if (is_null($month)) {
            $date = new \DateTime();
            $month = $date->format('m');
        }

        $billingValid = [];
        $billingUnValid = [];

        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $this->getUser()]);

        $resources = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy(['profile' => $profile]);

        foreach ($resources as $resource) {
            $module = $resource->getModule();
            $modInt = $this->getDoctrine()->getRepository('ApiBundle:ModuleIntervention')
                ->findOneBy(['module' => $module]);

            $learners = $this->getLearnerByModule($module);

            foreach ($learners as $learner) {
                $booking = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                    ->findBy(['module' => $module, 'profile' => $learner]);

                $monthActivity = $booking->getBookingDate()->format('m');

                if ($month != $monthActivity) {
                    if ('payed' === $module->getStatus()->getDesignation()) {
                        $row = [
                            'Org' => '',
                            'Entité' => $learner->getEntity()->getDesignation(),
                            'intervention' => $modInt->getIntervention()->getDesignation(),
                            'theme' => $module->getSubject()->getDesignation(),
                            'type' => $module->getStoredModule()->getDesignation(),
                            'date' => $booking->getBookingDate(),
                            'learner' => $learner->getDisplayName(),
                            'statut' => $module->getStatus()->getDesignation(),
                            'reportStatus' => $module->getReport()->getStatus()->getDesignation(),
                            'price' => $module->getPrice(),
                        ];
                        array_push($billingValid, $row);
                    } else {
                        $row = [
                            'Org' => '',
                            'Entité' => $learner->getEntity()->getDesignation(),
                            'intervention' => $modInt->getIntervention()->getDesignation(),
                            'theme' => $module->getSubject()->getDesignation(),
                            'type' => $module->getStoredModule()->getDesignation(),
                            'date' => $booking->getBookingDate(),
                            'learner' => $learner->getDisplayName(),
                            'statut' => $module->getStatus()->getDesignation(),
                            'reportStatus' => $module->getReport()->getStatus()->getDesignation(),
                            'price' => $module->getPrice(),
                        ];
                        array_push($billingUnValid, $row);
                    }
                }
            }
        }

        return [
            'billingValid' => $billingValid,
            'billingUnValid' => $billingUnValid,
        ];
    }
}
