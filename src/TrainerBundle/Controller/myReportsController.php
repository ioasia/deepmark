<?php

namespace TrainerBundle\Controller;

use ApiBundle\Entity\Report;
use ApiBundle\Entity\ReportLine;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\BookingAgenda;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class myReportsController extends Controller
{
    public function indexAction()
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);

        $reportsToDo = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus($profile, ModuleReports::STATUS_TODO);
        $reportsSubmitted = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus($profile, ModuleReports::STATUS_SUBMITTED);
        $reportsApproved = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus($profile, ModuleReports::STATUS_APPROVED);
        $reportsRejected = $this->getDoctrine()->getRepository('ApiBundle:ModuleReports')
            ->findReportByStatus($profile, ModuleReports::STATUS_REJECTED);

        return $this->render('TrainerBundle:myReports:index.html.twig', array(
            'reportsToDo' => $reportsToDo,
            'reportsSubmitted' => $reportsSubmitted,
            'reportsApproved' => $reportsApproved,
            'reportsRejected' => $reportsRejected,
            'nextSession'   => $this->getDoctrine()->getRepository(BookingAgenda::class)->findClosestSessions(null, $profile)
        ));
    }

    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isXmlHttpRequest()) {
            $data = $request->request->get('report');
            $reponse = new Response();

            $report = new Report();
            $report->setCreateDate(new \DateTime());

            $em->persist($report);

            $module = $em->getRepository('ApiBundle:Module')->find($data['module']);
            $module->setReport($report);

            $em->persist($module);

            foreach ($data['report'] as $line) {
                $reportLine = new ReportLine();
                $reportLine->setNotation($line['notation']);
                $reportLine->setWording($line['wording']);
                $reportLine->setTitle($line['title']);
                $reportLine->setReport($report);

                $em->persist($reportLine);
            }

            $em->flush();

            return new Response();
        }

        return $this->render('TrainerBundle:myReports:new.html.twig');
    }
}
