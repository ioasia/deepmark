<?php

namespace TrainerBundle\Utils;

use AdminBundle\Utils\Contracts\InterventionUpload;
use AdminBundle\Utils\FileUplaod\UploadFile;

class UploadFileTrainer extends UploadFile
{
    public const UPLOADER_ID = 'report';

    public function __construct(InterventionUpload $uploader)
    {
        parent::__construct($uploader);
        $this->dirToUpload = $uploader->getUploadFolder(self::UPLOADER_ID);
    }
}
