<?php

namespace TrainerBundle\Utils;

use AdminBundle\Utils\FileUplaod\FileUploadFactory;

class FileUploadReport extends FileUploadFactory
{
    public const FILE_DOR = __DIR__.'/../Files';

    protected $uploaders = [
        'report' => UploadFileTrainer::class,
    ];

    protected $uploadFolders = [
        'report' => self::FILE_DOR,
    ];
}
