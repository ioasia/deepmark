<?php

namespace ImportBundle\Controller;

use ImportBundle\Utils\traits\ImportTrait;
use ImportBundle\Utils\validation\LearnerFieldValidator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LearnerImportController extends Controller
{
    use ImportTrait;

    const USER = 'learner';

    protected $validator = LearnerFieldValidator::class;

    const EVENT_DISPATCH = 'file.upload.import.learner';
}
