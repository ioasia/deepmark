<?php

namespace ImportBundle\Controller;

use ImportBundle\Utils\traits\ImportTrait;
use ImportBundle\Utils\validation\TrainerFieldValidator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TrainerImportController extends Controller
{
    use ImportTrait;

    const USER = 'trainer';

    protected $validator = TrainerFieldValidator::class;

    const EVENT_DISPATCH = 'file.upload.import.trainer';
}
