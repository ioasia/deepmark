<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 15:50.
 */

namespace ImportBundle\Event;

use ImportBundle\Utils\validation\ImportValidator;
use Symfony\Component\EventDispatcher\Event;

class UserImportFileEvent extends Event
{
    private $relativeFilePath;

    private $role;

    private $reader;

    private $validator;

    private $response;

    private $importUserEntity;

    private $errors = [];

    private $importEntity;

    public function __construct(string $relativeFilePath, string $role)
    {
        $this->relativeFilePath = $relativeFilePath;

        $this->role = $role;

        $this->response = new \stdClass();
    }

    public function loadReader($reader)
    {
        $this->reader = $reader;
    }

    public function getReader()
    {
        return $this->reader;
    }

    public function getPath()
    {
        return $this->relativeFilePath;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function loadValidator(ImportValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getValidator(): ?ImportValidator
    {
        return $this->validator;
    }

    public function setResponse($errors, $message)
    {
        $this->response->errors = $errors;
        $this->response->message = $message;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setImportEntity($importEntity)
    {
        $this->importEntity = $importEntity;
    }

    public function getImportEntity()
    {
        return $this->importEntity;
    }

    public function setImportUserEntity($importUserEntity)
    {
        $this->importUserEntity = $importUserEntity;
    }

    public function getImportUserEntity()
    {
        return $this->importUserEntity;
    }

    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
