<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 20:50.
 */

namespace ImportBundle\Utils;

use Port\Excel\ExcelReader;

class ExcelReaderWrapper extends ExcelReader
{
    private $file;

    public function __construct(\SplFileObject $file, ?int $headerRowNumber = null, ?int $activeSheet = null, bool $readOnly = true, ?int $maxRows = null)
    {
        $this->file = $file;

        parent::__construct($file, $headerRowNumber, $activeSheet, $readOnly, $maxRows);
    }

    public function getFile(): ?\SplFileObject
    {
        return $this->file;
    }

    public function __toArray(): ?array
    {
        $totalNumber = $this->count();
        --$totalNumber;
        $heaers = $this->getRow(0);

        $filedsArray = [];
        for ($i = 1; $i <= $totalNumber; ++$i) {
            $filedsArray[$i] = array_combine($heaers, $this->getRow($i));
        }

        return $filedsArray;
    }
}
