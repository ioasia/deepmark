<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 20:34.
 */

namespace ImportBundle\Utils\validation;

interface ImportValidator
{
    public function validate();

    public function loadFieldsToValidateArray(array $fieldsToValidate);
}
