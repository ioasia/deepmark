<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 19:48.
 */

namespace ImportBundle\Utils\validation;

class LearnerFieldValidator extends ValidatorAbstract implements ImportValidator
{
    protected $configFile = 'learner_config.yml';
}
