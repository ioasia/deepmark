<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 19:48.
 */

namespace ImportBundle\Utils\validation;

class TrainerFieldValidator extends ValidatorAbstract implements ImportValidator
{
    protected $configFile = 'trainer_config.yml';
}
