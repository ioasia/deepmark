<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 19:49.
 */

namespace ImportBundle\Utils\validation;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Yaml\Yaml;

abstract class ValidatorAbstract
{
    protected $configuration;

    protected $fieldsToValidate;

    protected $valiation;

    protected $configFile = '';

    public function loadConfiguration()
    {
        $configFile = __DIR__.'/../../Resources/config/import/'.$this->configFile;
        $configuration = Yaml::parseFile($configFile);

        $this->configuration = $configuration;
    }

    public function loadFieldsToValidateArray(array $fieldsToValidate)
    {
        $this->fieldsToValidate = $fieldsToValidate;
    }

    public function validate(): array
    {
        $validationConfig = $this->configuration['import']['fields'];
        $constraints = [];
        foreach ($validationConfig as $fieldName => $validations) {
            $validators = $validations['validators'];
            foreach ($validators as $validatorType => $options) {
                $validatorStringClass = 'Symfony\Component\Validator\Constraints\\'.$validatorType;

                $validatorClass = !empty($options) ? new $validatorStringClass($options) : new $validatorStringClass();
                $constraints[$fieldName][] = $validatorClass;
            }
        }
        $constraints = new Assert\Collection($constraints);

        $groups = new Assert\GroupSequence(['Default', 'custom']);
        $validator = Validation::createValidator();

        $valiation = [];

        foreach ($this->fieldsToValidate as $rowNr => $fields) {
            $valiation[$rowNr] = $validator->validate($fields, $constraints, $groups);
        }

        $this->valiation = $valiation;

        return $valiation;
    }

    public function hasErrors()
    {
        foreach ($this->valiation as $validationList) {
            if (is_array($validationList)) {
                continue;
            }
            if ($validationList->count() > 0) {
                return true;
            }
        }

        return false;
    }
}
