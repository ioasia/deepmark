<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 19:08.
 */

namespace ImportBundle\Utils\traits;

use ImportBundle\Entity\ImportFile;
use ImportBundle\Entity\ImportUser;
use ImportBundle\Event\UserImportFileEvent;
use ImportBundle\Utils\ExcelReaderWrapper;
use ImportBundle\Utils\ImportConstant;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;

trait ImportTrait
{
    public function importUserAction($path)
    {
        /** @var EventDispatcher $dispatcher */
        $dispatcher = $this->get('event_dispatcher');

        $file = new \SplFileObject($path);
        $reader = new ExcelReaderWrapper($file);

        $validator = new $this->validator();
        $validator->loadConfiguration();

        $event = new UserImportFileEvent($path, self::USER);
        $event->loadReader($reader);
        $event->loadValidator($validator);
        $event->setImportEntity(new ImportFile());
        $event->setImportUserEntity(new ImportUser());

        $import = $dispatcher->dispatch(self::EVENT_DISPATCH, $event);

        $response = [
            'errors' => ImportConstant::NO_ERROR,
            'message' => ImportConstant::IMPORT_COMPLETED,
        ];

        if (!empty($event->getErrors())) {
            $response = [
                'errors' => $event->getErrors(),
                'message' => ImportConstant::ERROR_MESSAGE,
            ];
        }

        return new JsonResponse($response);
    }
}
