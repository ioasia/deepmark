<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 22.03.19
 * Time: 14:51.
 */

namespace ImportBundle\Utils\traits;

use ImportBundle\Entity\ImportFile;
use ImportBundle\Entity\ImportStatus;
use ImportBundle\Entity\ImportUser;
use ImportBundle\Utils\ExcelReaderWrapper;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Validator\ConstraintViolationList;

trait ImportUserListenerTrait
{
    public function importUser(Event $event)
    {
        /** @var ExcelReaderWrapper $reader */
        $reader = $event->getReader();

        $event->getValidator()->loadFieldsToValidateArray($reader->__toArray());
        $validations = $event->getValidator()->validate();

        /** @var ImportFile $importFileEntity */
        $importFileEntity = $event->getImportEntity();
        $importFileEntity->setFilePath($reader->getFile()->getPath());
        $importFileEntity->setImportType(self::USER);

        if ($event->getValidator()->hasErrors()) {
            $rowErrors = [];
            /** @var ConstraintViolationList $validation */
            foreach ($validations as $rowNr => $validation) {
                foreach ($validation->getIterator() as $value) {
                    $rowErrors[$rowNr] = [
                        'field' => $value->getPropertyPath(),
                        'value' => $value->getInvalidValue(),
                        'message' => $value->getMessage(), ];
                }
            }
            $importFileEntity->setErrors($rowErrors);
            $event->setErrors($rowErrors);

            $this->entityManager->flush();

            return;
        }
        $this->entityManager->persist($importFileEntity);

        foreach ($reader->__toArray() as $fields) {
            /** @var ImportUser $importUserEntity */
            $importUserEntity = new ImportUser();
            $importUserEntity->setImportType(self::USER);
            $importUserEntity->setEmail($fields['email']);
            $importUserEntity->setPhone($fields['phone']);
            $importUserEntity->setFirstName($fields['name']);
            $importUserEntity->setSurname($fields['surname']);

            $importStatus = $this->entityManager->getRepository(ImportStatus::class)->find(ImportStatus::IMPORTED);

            $importUserEntity->setImportStatus($importStatus);

            $this->entityManager->persist($importUserEntity);
        }
    }
}
