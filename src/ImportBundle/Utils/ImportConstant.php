<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 28.03.19
 * Time: 15:39.
 */

namespace ImportBundle\Utils;

class ImportConstant
{
    const NO_ERROR = 0;
    const IMPORT_COMPLETED = 'Import Completed';
    const ERROR_MESSAGE = 'There was an error with the import';
}
