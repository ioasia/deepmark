<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 21.03.19
 * Time: 15:55.
 */

namespace ImportBundle\EventListener;

use Doctrine\ORM\EntityManager;
use ImportBundle\Event\UserImportFileEvent;
use ImportBundle\Utils\traits\ImportUserListenerTrait;
use Symfony\Component\EventDispatcher\Event;

class LearnerImportListener
{
    private $entityManager;

    const USER = 'learner';

    use ImportUserListenerTrait;

    /**
     * LearnerImportListener constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param UserImportFileEvent|Event $event
     */
    public function onFileUploadImportLearner(Event $event)
    {
        $this->importUser($event);
        $this->entityManager->flush();
    }
}
