<?php

namespace ImportBundle\Entity;

/**
 * ImportStatus.
 */
class ImportStatus
{
    const IMPORTED = 1;
    const INVITED = 2;
    const REGISTERED = 3;
    const EMAIL_EXISTS = 4;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $statusName;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusName.
     *
     * @param string|null $statusName
     *
     * @return ImportStatus
     */
    public function setStatusName($statusName = null)
    {
        $this->statusName = $statusName;

        return $this;
    }

    /**
     * Get statusName.
     *
     * @return string|null
     */
    public function getStatusName()
    {
        return $this->statusName;
    }

    public function __toString()
    {
        return $this->getStatusName();
    }
}
