<?php

namespace ImportBundle\Entity;

/**
 * ImportFile.
 */
class ImportFile
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $importType;

    /**
     * @var string|null
     */
    private $filePath;

    /**
     * @var string|null
     */
    private $errors;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set importType.
     *
     * @param string|null $importType
     *
     * @return ImportFile
     */
    public function setImportType($importType = null)
    {
        $this->importType = $importType;

        return $this;
    }

    /**
     * Get importType.
     *
     * @return string|null
     */
    public function getImportType()
    {
        return $this->importType;
    }

    /**
     * Set filePath.
     *
     * @param string|null $filePath
     *
     * @return ImportFile
     */
    public function setFilePath($filePath = null)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath.
     *
     * @return string|null
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set errors.
     *
     * @param array|null $errors
     *
     * @return ImportFile
     */
    public function setErrors($errors = null)
    {
        $this->errors = serialize($errors);

        return $this;
    }

    /**
     * Get errors.
     *
     * @return array|null
     */
    public function getErrors()
    {
        return unserialize($this->errors);
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return ImportFile
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }
}
