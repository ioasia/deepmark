<?php

namespace ImportBundle\Entity;

/**
 * ImportUser.
 */
class ImportUser
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $surname;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $importType;

    /**
     * @var bool|null
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return ImportUser
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surname.
     *
     * @param string|null $surname
     *
     * @return ImportUser
     */
    public function setSurname($surname = null)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname.
     *
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return ImportUser
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return ImportUser
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set importType.
     *
     * @param string|null $importType
     *
     * @return ImportUser
     */
    public function setImportType($importType = null)
    {
        $this->importType = $importType;

        return $this;
    }

    /**
     * Get importType.
     *
     * @return string|null
     */
    public function getImportType()
    {
        return $this->importType;
    }

    /**
     * Set created.
     *
     * @param bool|null $created
     *
     * @return ImportUser
     */
    public function setCreated($created = null)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return bool|null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return ImportUser
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @var \ImportBundle\Entity\ImportStatus
     */
    private $importStatus;

    /**
     * Set importStatus.
     *
     * @param \ImportBundle\Entity\ImportStatus|null $importStatus
     *
     * @return ImportUser
     */
    public function setImportStatus(\ImportBundle\Entity\ImportStatus $importStatus = null)
    {
        $this->importStatus = $importStatus;

        return $this;
    }

    /**
     * Get importStatus.
     *
     * @return \ImportBundle\Entity\ImportStatus|null
     */
    public function getImportStatus()
    {
        return $this->importStatus;
    }
}
