<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InterventionsController extends Controller
{
    public function indexAction()
    {
        $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        return $this->render('ClientBundle:Interventions:index.html.twig', array(
            'interventions' => $interventions,
        ));
    }
}
