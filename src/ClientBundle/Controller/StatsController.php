<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StatsController extends Controller
{
    public function indexAction()
    {
        $intervention = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        return $this->render('ClientBundle:Stats:index.html.twig', array(
            'interventions' => $intervention,
        ));
    }
}
