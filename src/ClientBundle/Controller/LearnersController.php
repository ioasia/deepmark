<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LearnersController extends Controller
{
    /**
     * @return ProfileVariety|object
     */
    private function getLearnerProfileType()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(array(
            'designation' => 'TYPE_LEARNER',
        ));
    }

    public function indexAction()
    {
        $learners = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findBy(['profileType' => $this->getLearnerProfileType()]);

        foreach ($learners as $learner) {
            $learner->setNoteGlobal(0);
        }

        return $this->render('ClientBundle:Learners:index.html.twig', array(
            'learners' => $learners,
        ));
    }
}
