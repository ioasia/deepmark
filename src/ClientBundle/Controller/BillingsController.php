<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BillingsController extends Controller
{
    /**
     * return the profile of the current user.
     */
    private function getCurrentUser()
    {
        $person = $this->getUser();

        return $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
    }

    public function indexAction()
    {
        $customer = $this->getCurrentUser();

        $quotations = $this->getDoctrine()->getRepository('ApiBundle:Quotation')
            ->findBy(['customer' => $customer]);

        foreach ($quotations as $quotation) {
            $quotationLines = $this->getDoctrine()->getRepository('ApiBundle:QuotationLine')
            ->findBy(['quotation' => $quotation]);

            $price = 0;

            foreach ($quotationLines as $line) {
                $price = $price + ($line->getProduct()->getPrice() * $line->getQuantity());
            }

            $quotation->setTotalPrice($price);
        }

        return $this->render('ClientBundle:Billings:index.html.twig', array(
            'quotations' => $quotations,
        ));
    }
}
