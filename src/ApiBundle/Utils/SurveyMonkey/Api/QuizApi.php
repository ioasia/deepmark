<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 10.01.19
 * Time: 09:54.
 */

namespace ApiBundle\Utils\SurveyMonkey\Api;

/**
 * Class Quiz.
 */
class QuizApi
{
    /**
     * @var bool
     */
    public $is_quiz_mode;

    /**
     * What to reveal to the user when they complete the quiz:
     * disabled, results_only or results_and_answers.
     *
     * @var
     */
    public $show_results_type = 'disabled';

    /**
     * Text to show the user when they complete the quiz.
     *
     * @var
     */
    public $feedback;

    private $questionCount;

    public function __construct(bool $isQuizMode = false)
    {
        $this->is_quiz_mode = $isQuizMode;

        $this->feedback = new \stdClass();
    }

    public function isQuizzable(): bool
    {
        return $this->is_quiz_mode;
    }

    public function setRanges(int $min, int $max, string $message): void
    {
        $this->feedback->ranges = [
            [
                'min' => $min,
                'max' => $max,
                'message' => $message,
            ],
        ];
    }

    /**
     * What to reveal to the user when they complete the quiz
     * Allowed parameters:
     * disabled, results_only or results_and_answers.
     *
     * default value: disabled
     *
     * @param string $visible
     */
    public function setResultsVisibility(string $visible): void
    {
        $allowedParameters = [
            'disabled' => 'None', 'results_only' => 'Results only', 'results_and_answers' => 'Results with highligted answers',
        ];

        $this->show_results_type = in_array($visible, $allowedParameters) ? array_search($visible, $allowedParameters) : 'disabled';
    }

    /**
     * Configure whether the following parameters use percentage or points.
     * Note that these ranges are inclusive and may not overlap.
     *
     * @param string $rangeType
     */
    public function setRangeType(string $rangeType): void
    {
        $availableRanges = [
            'percentage', 'points',
        ];

        if (!in_array($rangeType, $availableRanges)) {
            $rangeType = 'percentage';
        }

        $this->feedback->ranges_type = $rangeType;
    }

    /**
     * Text to show the user when they complete the quiz.
     *
     * @param string $correctText
     * @param string $incorrectText
     * @param string $partialText
     */
    public function setFeedback(string $correctText, string $incorrectText, string $partialText): void
    {
        $this->feedback->correct_text = $correctText;
        $this->feedback->incorrect_text = $incorrectText;
        $this->feedback->partial_text = $partialText;
    }

    public function setQuestionCount(int $questionCount)
    {
        $this->questionCount = $questionCount;
    }

    /**
     * @return mixed
     */
    public function getQuestionCount(): int
    {
        return $this->questionCount;
    }
}
