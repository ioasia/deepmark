<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 09.01.19
 * Time: 11:10.
 */

namespace ApiBundle\Utils\Contract;

use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class QuizValidatorAbstract
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var
     */
    protected $quiz;

    /**
     * QuizValidatorAbstract constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $quiz
     */
    public function setQuizParams(array $quiz): void
    {
        $this->quiz = $quiz;
    }

    /**
     * Validates quiz params. Depending on request
     * apply different validator constraints.
     *
     * @return object
     */
    abstract public function validate();
}
