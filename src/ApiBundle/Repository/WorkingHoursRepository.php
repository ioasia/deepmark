<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Profile;
use Doctrine\ORM\QueryBuilder;

class WorkingHoursRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $profile
     *
     * @return mixed
     */
    public function deleteWorkingHoursByProfile(Profile $profile)
    {
        $query = $this->createQueryBuilder('p')
            ->delete()
            ->andWhere('p.profile = :profile')
            ->setParameter('profile', $profile)
            ->getQuery();

        return $query->execute();
    }
}
