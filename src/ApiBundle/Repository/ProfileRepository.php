<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\GroupOrganization;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Regional;
use ApiBundle\Entity\Skill;
use ApiBundle\Entity\Workplace;
use AppBundle\Entity\Person;
use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Utils\Statistics\KeyNameConst;

/**
 * Class ProfileRepository
 * @package ApiBundle\Repository
 */
class ProfileRepository extends EntityRepository
{
    /**
     * Get profile that have one availability for the session
     * @param Profile $profile
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getAvailableBySession(
        Profile $profile,
        \DateTime $startDate,
        \DateTime $endDate
    )
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->join('p.availableCalendar', 'a');

        $allConditions = $queryBuilder->expr()->andX();
        $allConditions->add($queryBuilder->expr()->eq('p.id', ':id'));
        $allConditions->add($queryBuilder->expr()->lte('a.beginning', ':start_date'));
        $allConditions->add($queryBuilder->expr()->gte('a.ending', ':end_date'));

        $queryBuilder->setParameter('id', $profile->getId());
        $queryBuilder->setParameter('start_date', $startDate, Type::DATETIME);
        $queryBuilder->setParameter('end_date', $endDate, Type::DATETIME);
        $queryBuilder->andWhere($allConditions);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get trainer that have one availability for the session
     * @param array $skills
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param null $skills_join_operator
     * @return mixed
     */
    public function getAvailableTrainersBySkillsSession(
        array $skills,
        \DateTime $startDate,
        \DateTime $endDate,
        $skills_join_operator = null
    )
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->join('p.availableCalendar', 'a');

        if (!is_null($skills_join_operator)) {
            $queryBuilder = $this->addSkills($queryBuilder, $skills, $skills_join_operator);
        }

        $allConditions = $queryBuilder->expr()->andX();
        $allConditions->add($queryBuilder->expr()->lte('a.beginning', ':start_date'));
        $allConditions->add($queryBuilder->expr()->gte('a.ending', ':end_date'));

        $queryBuilder->setParameter('start_date', $startDate, Type::DATETIME);
        $queryBuilder->setParameter('end_date', $endDate, Type::DATETIME);
        $queryBuilder->andWhere($allConditions);

        return $queryBuilder->getQuery()->getResult();
    }

    // Get trainers that have availabilities between the dates

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $skills
     * @param string $skills_join_operator
     *
     * @return mixed
     */
    private function addSkills($queryBuilder, $skills, $skills_join_operator): ?QueryBuilder
    {
        $queryBuilder->join('p.skills', 's');
        /** @var Skill $skill */
        foreach ($skills as $key => $skill) {
            if ('and' == $skills_join_operator) {
                $queryBuilder->andWhere('s.skill = :skills_' . $key)
                    ->setParameter('skills_' . $key, $skill);
            }
            if ('or' == $skills_join_operator) {
                $queryBuilder->orWhere('s.skill = :skills_' . $key)
                    ->setParameter('skills_' . $key, $skill);
            }
        }
        $queryBuilder->andWhere('s.status = \'APPROVED\'');
        return $queryBuilder;
    }

    /**
     * @param array $skills
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param null $skills_join_operator
     * @return mixed
     */
    public function getAvailableTrainersBySkillsOnline(array $skills, \DateTime $startDate, \DateTime $endDate, $skills_join_operator = null)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->join('p.availableCalendar', 'a');
        if (!is_null($skills_join_operator)) {
            $queryBuilder = $this->addSkills($queryBuilder, $skills, $skills_join_operator);
        }

        $allConditions = $queryBuilder->expr()->andX();

        $allConditions->add($queryBuilder->expr()->gte('a.beginning', ':start_date'));
        $allConditions->add($queryBuilder->expr()->lte('a.ending', ':end_date'));

        $queryBuilder->setParameter('start_date', $startDate, Type::DATETIME);
        $queryBuilder->setParameter('end_date', $endDate, Type::DATETIME);
        $queryBuilder->andWhere($allConditions);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $filters
     * @return mixed
     * @throws \Exception
     */
    public function findLearnerByCondition(array $filters = [])
    {
        $query = $this->createQueryBuilder('profile');

        $query->leftJoin('profile.person', 'person');

        if (isset($filters[KeyNameConst::KEY_ENTITY_ID]) && $filters[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->where('profile.entity = ' . (int)$filters[KeyNameConst::KEY_ENTITY_ID]);
        }
        //filter by organization
        if (isset($filters[KeyNameConst::KEY_ORGANISATION]) && !empty($filters[KeyNameConst::KEY_ORGANISATION])) {
            $query->leftJoin('profile.entity', 'entity')
                ->andWhere('entity.organisation = :organisation')
                ->setParameter(KeyNameConst::KEY_ORGANISATION, $filters[KeyNameConst::KEY_ORGANISATION]);
        }
        //filter by regional
        if (isset($filters[KeyNameConst::KEY_REGIONAL_ID]) && !empty($filters[KeyNameConst::KEY_REGIONAL_ID]) && $filters[KeyNameConst::KEY_REGIONAL_ID] != 0) {
            $query->leftJoin(Regional::class, 'regional', Expr\Join::WITH, 'profile.regional = regional')
                ->andWhere('regional.id = ' . (int)$filters[KeyNameConst::KEY_REGIONAL_ID]);
        }

        //filter by group organization new name is sectorId on UI
        if (isset($filters[KeyNameConst::KEY_SECTOR_ID]) && !empty($filters[KeyNameConst::KEY_SECTOR_ID]) && $filters[KeyNameConst::KEY_SECTOR_ID] != 0) {
            $query->leftJoin(GroupOrganization::class, 'sector', Expr\Join::WITH, 'profile.groupOrganization = sector')
                ->andwhere('sector.id = ' . (int)$filters[KeyNameConst::KEY_SECTOR_ID]);
        }
        //filter by workplace
        if (isset($filters[KeyNameConst::KEY_WORKPLACE_ID]) && !empty($filters[KeyNameConst::KEY_WORKPLACE_ID]) && $filters[KeyNameConst::KEY_WORKPLACE_ID] != 0) {
            $query->leftJoin(Workplace::class, 'workplace', Expr\Join::WITH, 'profile.workplace = workplace')
                ->andwhere('workplace.id = ' . (int)$filters[KeyNameConst::KEY_WORKPLACE_ID]);
        }
        // Filter by group
        if (isset($filters[KeyNameConst::KEY_GROUP_ID]) && !empty($filters[KeyNameConst::KEY_GROUP_ID])) {
            $query->leftJoin(LearnerGroup::class, 'learner_group', Expr\Join::WITH, 'learner_group.learner = profile')
                ->andwhere('learner_group.group = ' . (int)$filters[KeyNameConst::KEY_GROUP_ID]);
        }

        $query->andWhere('profile.profileType = ' . (int)$this->getEntityManager()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => ProfileVariety::LEARNER])->getId());

        if (isset($filters[KeyNameConst::KEY_STATUS_ID]) && $filters[KeyNameConst::KEY_STATUS_ID] != -1) {
            $query->andWhere('person.enabled =' . (int)$filters[KeyNameConst::KEY_STATUS_ID]);
        }

        if (isset($filters[KeyNameConst::KEY_CREATION_DATE]) && !empty($filters[KeyNameConst::KEY_CREATION_DATE])) {
            $cd_min = $filters[KeyNameConst::KEY_CREATION_DATE] instanceof DateTime ? $filters[KeyNameConst::KEY_CREATION_DATE] : new \DateTime(date($filters[KeyNameConst::KEY_CREATION_DATE]));
            if (isset($filters['creation_date_max'])){
                $cd_max = $filters['creation_date_max'] instanceof DateTime ? $filters['creation_date_max'] : new \DateTime(date($filters['creation_date_max']));
            } else {
                $cd_max = clone $cd_min;
                $cd_max->modify('+1 day');
            }

            $query->andWhere('profile.creationDate >= :cd_begin');
            $query->andWhere('profile.creationDate <= :cd_end');
            $query->setParameter('cd_begin', $cd_min, Type::DATETIME);
            $query->setParameter('cd_end', $cd_max, Type::DATETIME);
        }

        if (isset($filters[KeyNameConst::KEY_CONNECTION_DATE]) && !empty($filters[KeyNameConst::KEY_CONNECTION_DATE])) {
            $ll_min = new \DateTime(date($filters[KeyNameConst::KEY_CONNECTION_DATE]));
            $ll_max = clone $ll_min;
            $ll_max->modify('+1 day');

            $query->andWhere('person.lastLogin >= :ll_begin');
            $query->andWhere('person.lastLogin <= :ll_end');
            $query->setParameter('ll_begin', $ll_min);
            $query->setParameter('ll_end', $ll_max);
        }

        if(isset($filters['courseFilter'])){
            if(empty($filters['courseFilter'])){
                $query->andWhere('1 = 2'); // return 0 learner in case intervention is not assign any learner
            }else {
                $query->andWhere('profile.person in (' . implode(",", $filters['courseFilter']) . ')');
            }
        }

        $query->orderBy('profile.updateDate', 'ASC');
        return $query->getQuery()->getResult();
    }

    /**
     * @param null $role
     * @param array $conditions
     * @param string $orderBy
     * @param string $orderDir
     * @return mixed
     */
    public function getUsersByConditions($role = null, $conditions = [], $orderBy = 'profile.updateDate', $orderDir = 'ASC')
    {
        $qb = $this->createQueryBuilder('profile');

        if ($role) {
            if (!$profileType = $this->getEntityManager()->getRepository('ApiBundle:ProfileVariety')
                ->findOneBy(['appId' => $role])) {
                return [];
            }

            $qb->andWhere(
                'profile.profileType = ' . (int)$this->getEntityManager()->getRepository('ApiBundle:ProfileVariety')
                    ->findOneBy(['appId' => $role])->getId()
            );
        }

        if (isset($conditions['person'])) {
            $qb->innerJoin('profile.person', 'person');

            $fields = array_merge(
                $this->getEntityManager()->getClassMetadata(Person::class)->getFieldNames(),
                $this->getEntityManager()->getClassMetadata(Person::class)->getAssociationNames()
            );

            $this->buildQueries($qb, $fields, $conditions['person'], 'person');
        }

        if (isset($conditions['profile'])) {
            $fields = array_merge(
                $this->getClassMetadata()->getFieldNames(),
                $this->getClassMetadata()->getAssociationNames()
            );

            $this->buildQueries($qb, $fields, $conditions['profile']);
        }

        /**
         * Filter by group
         */

        if (isset($conditions['group'])) {
            $qb->innerJoin(LearnerGroup::class, 'learnerGroup', Expr\Join::WITH)
                ->where('learnerGroup.group = ' . (int)$conditions['group']);
        }

        return $qb->orderBy($orderBy, $orderDir)->getQuery()->getResult();
    }

    /**
     * @param $qb
     * @param $fields
     * @param $conditions
     * @param string $table
     */
    private function buildQueries(&$qb, $fields, $conditions, $table = 'profile')
    {
        foreach ($conditions as $key => $data) {
            if (!in_array($key, $fields)) {
                continue;
            }

            if (!isset($data['operate'])) {
                foreach ($data as $index => $subData) {
                    $qb->andWhere($table . '.' . $key . ' ' . $subData['operate'] . ':' . $key . $index)
                        ->setParameter($key . $index, $subData['value']);
                }

                continue;
            }

            $qb->andWhere($table . '.' . $key . ' ' . $data['operate'] . ':' . $key)
                ->setParameter($key, $data['value']);
        }
    }

    public function countProfileByRole($enityId, $profileTypeId, $status, $isNew = null)
    {
        $query = $this->createQueryBuilder('t');
        $query->select('count(t.id)');
        $query->where($query->expr()->eq('t.entity', $enityId));
        $query->andWhere($query->expr()->eq('t.profileType', $profileTypeId));
        $query->leftJoin('t.person', 'p');
        if ($isNew) {
            $query->andWhere($query->expr()->isNull('p.lastLogin'));
        } else {
            $query->andWhere($query->expr()->eq('p.enabled', $status));
            $query->andWhere($query->expr()->isNotNull('p.lastLogin'));
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function totalProfileByRole(DateTime $endDate, $profileTypeId, array $options = [])
    {
        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)');
        $query->where($query->expr()->eq('i.profileType', $profileTypeId));
        $query->leftJoin('i.person', 'p');
        $query->andWhere('p.createdAt <= :ending')
                ->setParameter('ending', $endDate, Type::DATE);

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function totalActiveProfileByRole(DateTime $endDate, $profileTypeId, array $options = [])
    {
        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)');
        $query->where($query->expr()->eq('i.profileType', $profileTypeId));
        $query->leftJoin('i.person', 'p');
        $query->andWhere('p.createdAt <= :ending')
                ->setParameter('ending', $endDate, Type::DATE);
        $query->andWhere($query->expr()->eq('p.enabled', 1));

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function totalInActiveProfileByRole(DateTime $endDate, $profileTypeId, array $options = [])
    {
        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)');
        $query->where($query->expr()->eq('i.profileType', $profileTypeId));
        $query->leftJoin('i.person', 'p');
        $query->andWhere('p.createdAt <= :ending')
            ->setParameter('ending', $endDate, Type::DATE);
        $query->andWhere($query->expr()->eq('p.enabled', 0));

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function totalNewProfileByRole(DateTime $startDate, DateTime $endDate, $profileTypeId, array $options = [])
    {
        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)');
        $query->where($query->expr()->eq('i.profileType', $profileTypeId));
        $query->leftJoin('i.person', 'p');
        $query->andWhere('p.createdAt >= :beginning AND p.createdAt <= :ending')
            ->setParameter('beginning', $startDate, Type::DATE)
            ->setParameter('ending', $endDate, Type::DATE);

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }
}
