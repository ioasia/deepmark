<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;

/**
 * ChapterModuleRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ChapterModuleRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByChapter($chapter)
    {
        $query = $this->createQueryBuilder('t');
        $query->where('t.chapter = :chapter');
        $query->setParameter('chapter', $chapter);
        $query->orderBy('t.position', 'ASC');

        return $query->getQuery()->getResult();
    }
}
