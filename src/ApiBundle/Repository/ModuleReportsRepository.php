<?php

namespace ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ModuleReportsRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ModuleReportsRepository extends EntityRepository
{
    public function findAdminReportByStatus($adminId, $status)
    {
        $query = $this->createQueryBuilder('t');
        $query->leftJoin('t.module', 'module');
        $query->where($query->expr()->isNotNull('module.isNeedReport'));

        if ($status) {
            $query->where('t.status = :status')->setParameter('status', $status);
        }

        if ($adminId) {
            $query->leftJoin('module.intervention', 'intervention');
            $query->andWhere($query->expr()->eq('intervention.admin', (int)$adminId));
        }

        return $query->getQuery()->getResult();
    }

    public function countAdminReportByStatus($adminId, $status)
    {
        $query = $this->createQueryBuilder('t');
        $query->select('count(t.id)');
        $query->leftJoin('t.module', 'module');
        $query->where($query->expr()->isNotNull('module.isNeedReport'));

        if ($status) {
            $query->where('t.status = :status')->setParameter('status', $status);
        }

        if ($adminId) {
            $query->leftJoin('module.intervention', 'intervention');
            $query->andWhere($query->expr()->eq('intervention.admin', (int)$adminId));
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function findReportByStatus($profile, $status)
    {
        $query = $this->createQueryBuilder('t');
        $query->leftJoin('t.module', 'module');
        $query->where($query->expr()->isNotNull('module.isNeedReport'));

        if ($profile) {
            $query->andWhere('t.trainer = :trainer')->setParameter('trainer', $profile);
        }

        if ($status) {
            $query->andWhere('t.status = :status')->setParameter('status', $status);
        }

        return $query->getQuery()->getResult();
    }
}
