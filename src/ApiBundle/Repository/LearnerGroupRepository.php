<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Group;
use Doctrine\ORM\QueryBuilder;

class LearnerGroupRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $profile
     *
     * @return mixed
     */
    public function deleteLearnerGroupByGroup(Group $group)
    {
        $query = $this->createQueryBuilder('p')
            ->delete()
            ->andWhere('p.group = :group')
            ->setParameter('group', $group)
            ->getQuery();

        return $query->execute();
    }
}
