<?php

namespace ApiBundle\Repository;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Utils\Statistics\KeyNameConst;

class GroupRepository extends EntityRepository
{
    /**
     * @param DateTime $endDate
     * @param  array options
     * @return mixed
     */
    public function countTotalGroupByEntity(DateTime $endDate, array $options = [])
    {

        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)')
            ->where('i.created <= :ending')
            ->setParameter('ending', $endDate, Type::DATE);

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    /**
     * @param DateTime $endDate
     * @param DateTime $startDate
     * @param  array options
     * @return mixed
     */
    public function countNewGroup(DateTime $startDate, DateTime $endDate, array $options = [])
    {

        $query = $this->createQueryBuilder('i');
        $query->select('count(i.id)')
            ->where('i.created >= :beginning AND i.created <= :ending')
            ->setParameter('beginning', $startDate, Type::DATE)
            ->setParameter('ending', $endDate, Type::DATE);

        if (key_exists(KeyNameConst::KEY_ENTITY_ID, $options) && $options[KeyNameConst::KEY_ENTITY_ID] != 0) {
            $query->andWhere('i.entity = :entity')
                ->setParameter('entity', $options[KeyNameConst::KEY_ENTITY_ID]);
        }
        return $query->getQuery()->getSingleScalarResult();
    }
}
