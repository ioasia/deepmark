<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\TrainerReservation;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class TrainerReservationRepository extends EntityRepository
{
    public function clearOld(): void
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where($qb->expr()->lt('t.reservation_ends', ':reservation_end'));

        $qb->setParameter('reservation_end', new \DateTime(), Type::DATETIME);

        $entitiesToDelete = $qb->getQuery()->getResult();

        foreach ($entitiesToDelete as $entityToDelete) {
            $this->getEntityManager()->remove($entityToDelete);
        }

        $this->getEntityManager()->flush();
    }

    public function isTrainerAvailable(Profile $trainer, \DateTime $startDate, \DateTime $endDate): bool
    {
        $this->clearOld();
        /*$qb = $this->createQueryBuilder('r');
        $allConditions = $qb->expr()->andX();
        $allConditions->add($qb->expr()->eq('r.profile', $trainer->getId()));
        $dateConditions = $qb->expr()->orX();
        $dateConditions->add($qb->expr()->between(':start_date', 'r.from_datetime', 'r.to_datetime'));
        $dateConditions->add($qb->expr()->between(':end_date', 'r.from_datetime', 'r.to_datetime'));
        $allConditions->add($dateConditions);

        $qb->setParameter('start_date', $startDate, Type::DATETIME);
        $qb->setParameter('end_date', $endDate, Type::DATETIME);

        $qb->where($allConditions);

        $results = $qb->getQuery()->getResult();
        return empty($results);*/
        // always allow assign trainer to module
        return true;
    }

    public function isTrainerAvailableSession(Profile $trainer, \DateTime $startDate, \DateTime $endDate): bool
    {
        $this->clearOld();
        $qb = $this->createQueryBuilder('r');
        $allConditions = $qb->expr()->andX();
        $allConditions->add($qb->expr()->eq('r.profile', $trainer->getId()));
        $allConditions->add($qb->expr()->isNotNull('r.session_unique'));
        $dateConditions = $qb->expr()->orX();
        $dateConditions->add($qb->expr()->between(':start_date', 'r.from_datetime', 'r.to_datetime'));
        $dateConditions->add($qb->expr()->between(':end_date', 'r.from_datetime', 'r.to_datetime'));
        $allConditions->add($dateConditions);

        $qb->setParameter('start_date', $startDate, Type::DATETIME);
        $qb->setParameter('end_date', $endDate, Type::DATETIME);

        $qb->where($allConditions);

        $results = $qb->getQuery()->getResult();

        return empty($results);
    }

    public function makeReservationOnline(Profile $trainer, \DateTime $startDate, \DateTime $endDate, string $moduleUnique): void
    {
        if (!$this->isTrainerAvailable($trainer, $startDate, $endDate)) {
            throw new \InvalidArgumentException('Trainer '.$trainer->getDisplayName().' is not available');
        }
        $reservation = new TrainerReservation();
        $reservation->setProfile($trainer);
        $reservation->setFromDatetime($startDate);
        $reservation->setToDatetime($endDate);
        $reservation->setReservationEnds(new \DateTime('+ 15 minutes'));
        $reservation->setModuleUnique($moduleUnique);

        $this->getEntityManager()->persist($reservation);
        $this->getEntityManager()->flush();
    }

    public function makeReservationSession(Profile $trainer, \DateTime $startDate, \DateTime $endDate, string $moduleUnique, string $sessionId): void
    {
        if (!$this->isTrainerAvailableSession($trainer, $startDate, $endDate)) {
            throw new \InvalidArgumentException('trainer is not avaible. Id: '.$trainer->getId());
        }
        $reservation = new TrainerReservation();
        $reservation->setProfile($trainer);
        $reservation->setFromDatetime($startDate);
        $reservation->setToDatetime($endDate);
        $reservation->setReservationEnds(new \DateTime('+ 15 minutes'));
        $reservation->setModuleUnique($moduleUnique);
        $reservation->setSessionUnique($sessionId);

        $this->getEntityManager()->persist($reservation);
        $this->getEntityManager()->flush();
    }

    public function delete(TrainerReservation $reservation)
    {
        $this->getEntityManager()->remove($reservation);
        $this->getEntityManager()->flush();
    }

    public function updateTrainer(TrainerReservation $reservation, $profile)
    {
        $reservation->setProfile($profile);
        $this->getEntityManager()->persist($reservation);
        $this->getEntityManager()->flush();
    }
}
