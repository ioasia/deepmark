<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 11:57.
 */

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileSupervisorEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('person')
            ->add('person', PersonPassType::class, array(
                'required' => true,
            ));
    }

    public function getParent()
    {
        return ProfileLiveResourceType::class;
    }
}
