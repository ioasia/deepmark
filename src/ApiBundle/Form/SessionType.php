<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 13:42.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Session;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('sessionDate', DateTimeType::class, array(
                //'data' => new \DateTime()
            ))
            ->add('participant', NumberType::class, array(
                //'data' => 1
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Session::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'session';
    }
}
