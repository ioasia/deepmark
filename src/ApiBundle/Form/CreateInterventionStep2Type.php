<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateInterventionStep2Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('modules', CollectionType::class, array(
                'entry_type' => ModuleStep2Type::class,
                'allow_add' => false,
                'allow_delete' => false,
                'attr' => array(
                    'class' => 'modules-selector',
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'createInterventionStep1';
    }
}
