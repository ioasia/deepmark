<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 15:08.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\SkillResourceVarietyProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillResourceVarietyProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('skill')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SkillResourceVarietyProfile::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_live_resource_skill';
    }
}
