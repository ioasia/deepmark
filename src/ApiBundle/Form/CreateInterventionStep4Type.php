<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateInterventionStep4Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'createInterventionStep1';
    }
}
