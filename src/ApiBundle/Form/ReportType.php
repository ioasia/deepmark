<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16/08/18
 * Time: 12:58.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Report;
use ApiBundle\Entity\Status;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('createDate', DateType::class, array('required' => true))
            ->add('updateDate', DateType::class, array('required' => true))
            ->add('admin', EntityType::class, array(
                'required' => true,
                'class' => Profile::class,
                'choice_label' => function ($profile) {
                    return $profile->displayName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.profileType = 1');
                },
            ))
            ->add('status', EntityType::class, array(
                'required' => true,
                'class' => Status::class,
                'choice_label' => 'designation',
            ))
            ->add('reportLine', ReportLineType::class, array(
                'mapped' => false,
                'required' => true,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Report::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_report';
    }
}
