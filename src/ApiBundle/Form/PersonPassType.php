<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 15:22.
 */

namespace ApiBundle\Form;

use AppBundle\Entity\PersonTimeZone;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonPassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username')
            ->add('timezone', EntityType::class, [
                'class' => PersonTimeZone::class,
                'mapped' => true,
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'mapped' => true,
            ])
        ;
    }

    public function getParent()
    {
        return PersonType::class;
    }
}
