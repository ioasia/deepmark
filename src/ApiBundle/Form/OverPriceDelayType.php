<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23/08/18
 * Time: 09:12.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\DelayUnit;
use ApiBundle\Entity\OverPriceDelay;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OverPriceDelayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('numberDelay', NumberType::class, array(
                'required' => true,
                'label' => false,
                'attr' => array('class' => 'col-md-3'),
            ))
            ->add('priceOverRun', NumberType::class, array(
                'required' => true,
                'label' => false,
                'attr' => array('class' => 'col-md-3'),
            ))
            ->add('delayUnit', EntityType::class, array(
                'required' => true,
                'label' => false,
                'choice_label' => 'designation',
                'class' => DelayUnit::class,
                'attr' => array('class' => 'col-md-2'),
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OverPriceDelay::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_overPriceDelay';
    }
}
