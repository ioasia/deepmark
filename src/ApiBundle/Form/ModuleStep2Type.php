<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 13:44.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\Skill;
use ApiBundle\Entity\StoredModule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleStep2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('designation')
            ->add('storedModule', EntityType::class, array(
                'mapped' => true,
                'required' => true,
                'class' => StoredModule::class,
                'choice_label' => function ($storedModule) {
                    return $storedModule->getDesignation();
                },
            ))
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $collectionTypeSession = 0;
                $ending = new \DateTime();
                $ending->modify('1 week');

                $duration = new \DateTime();
                $duration->setTime(0, 0, 0);
                $module = $event->getData();
                $form = $event->getForm();

                if ($module->getStoredModule()->getAppId() <= 2) {
                    $form->add('beginning', DateType::class, array(
                        'data' => new \DateTime(),
                    ));
                    $form->add('ending', DateType::class, array(
                        'data' => $ending,
                    ));
                    $form->add('duration', TimeType::class, array(
                        'required' => true,
                        'data' => $duration,
                    ));
                }

                if ($module->getStoredModule()->getAppId() > 2) {
                    $form->add('description');
                    $form->add('beginning', DateType::class, array(
                        //'data' => new \DateTime()
                    ));
                    $form->add('ending', DateType::class, array(
                        //'data' => $ending
                    ));
                    $form->add('duration', TimeType::class, array(
                        'required' => true,
                        //'data' => $duration
                    ));
                    $form->add('educationalDocument', EntityType::class, array(
                        'class' => EducationalDocument::class,
                        'required' => false,
                        'multiple' => true,
                        'choice_label' => function ($educationalDocument) {
                            return $educationalDocument->getName();
                        },
                    ));
                }

                if (StoredModule::ELEARNING === $module->getStoredModule()->getAppId()) {
                    $form->add('mediaDocument', EntityType::class, array(
                        'mapped' => true,
                        'required' => false,
                        'class' => EducationalDocument::class,
                        'choice_label' => function ($educationalDocument) {
                            return $educationalDocument->getName();
                        },
                    ));
                }

                if (StoredModule::QUIZ === $module->getStoredModule()->getAppId() || StoredModule::EVALUATION === $module->getStoredModule()->getAppId()) {
                    $form->add('quiz', EntityType::class, array(
                        'mapped' => true,
                        'required' => true,
                        'class' => Quizes::class,
                        'choice_label' => function ($quiz) {
                            return $quiz->getTitle();
                        },
                    ));
                }

                if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                    $form->add('skills', EntityType::class, array(
                        'mapped' => true,
                        'required' => true,
                        'multiple' => true,
                        'class' => Skill::class,
                        'choice_label' => function ($skill) {
                            return $skill->getDesignation();
                        },
                    ));
                }

                if (StoredModule::VIRTUAL_CLASS === $module->getStoredModule()->getAppId()) {
                    $form->add('skills', EntityType::class, array(
                        'mapped' => true,
                        'required' => true,
                        'multiple' => true,
                        'class' => Skill::class,
                        'choice_label' => function ($skill) {
                            return $skill->getDesignation();
                        },
                    ));
                    $form->add('sessions', CollectionType::class, array(
                        'entry_type' => ModuleSessionType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'attr' => array(
                            'class' => 'sessions-selector',
                            //'class' => 'sessions-' . $collectionTypeSession . '-selector'
                        ),
                    ));
                    ++$collectionTypeSession;
                }

                if (StoredModule::PRESENTATION_ANIMATION === $module->getStoredModule()->getAppId()) {
                    $form->add('skills', EntityType::class, array(
                        'mapped' => true,
                        'required' => true,
                        'multiple' => true,
                        'class' => Skill::class,
                        'choice_label' => function ($skill) {
                            return $skill->getDesignation();
                        },
                    ));
                    $form->add('sessions', CollectionType::class, array(
                        'entry_type' => ModuleSessionLocationType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'attr' => array(
                            'class' => 'sessions-selector',
                            //'class' => 'sessions-' . $collectionTypeSession . '-selector'
                        ),
                    ));
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Module::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'createModuleStep2';
    }
}
