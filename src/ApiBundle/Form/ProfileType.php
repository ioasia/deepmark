<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creationDate')
            ->add('updateDate')
            ->add('firstName')
            ->add('lastName')
            ->add('fixedPhone')
            ->add('mobilePhone')
            ->add('otherEmail')
            ->add('billingCode')
            ->add('street')
            ->add('avatar')
            ->add('organisationRegistrationNumber')
            ->add('pitch')
            ->add('video')
            ->add('curriculumVitae')
            ->add('scannedIban')
            ->add('iban')
            ->add('bicCode')
            ->add('restrictionAccess')
            ->add('person')
            ->add('companyPost')
            ->add('profileType')
            ->add('entity')
            ->add('managers')
            ->add('attendanceAttestation');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Profile',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile';
    }
}
