<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 15:27.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Quotation;
use ApiBundle\Entity\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuotationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('designation', TextType::class, array('required' => true))
            ->add('observation', TextType::class, array('required' => true))
            ->add('sentDate', DateTimeType::class, array('required' => true))
            ->add('statusDate', DateTimeType::class, array('required' => true))
            ->add('observation', TextType::class, array('required' => true))
            ->add('intervention', EntityType::class, array(
                'required' => true,
                'class' => Intervention::class,
                'choice_label' => 'designation',
            ))
            ->add('admin', EntityType::class, array(
                'required' => true,
                'class' => Profile::class,
                'choice_label' => 'username',
            ))
            ->add('customer', EntityType::class, array(
                'required' => true,
                'class' => Profile::class,
                'choice_label' => 'username',
            ))
            ->add('status', EntityType::class, array(
                'required' => true,
                'class' => Status::class,
                'choice_label' => 'designation',
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Quotation::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_quotation';
    }
}
