<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 13:44.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Module;
use ApiBundle\Entity\StoredModule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('designation')
            ->add('description')
            ->add('quantity')
            ->add('beginning')
            ->add('ending')
            ->add('storedModule', EntityType::class, array(
                'mapped' => true,
                'required' => true,
                'class' => StoredModule::class,
                'choice_label' => function ($storedModule) {
                    return $storedModule->getDesignation();
                },
            ))
            ->add('educationalDocument', CollectionType::class, array(
                'entry_type' => EducationalDocumentType::class,
                'allow_add' => true,
            ))
            ->add('price', MoneyType::class, array(
                'required' => true,
            ))
            ->add('duration', TimeType::class, array(
                'required' => true,
            ))
            /*->add('price_total', MoneyType::class, array(
                'required' => true,
                'mapped' => false,
                'disabled' => true
            ))*/
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Module::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_module';
    }
}
