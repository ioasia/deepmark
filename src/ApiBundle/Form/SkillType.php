<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\Skill as Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('designation', null, ['label' => 'skill', 'attr' => ['maxlength' => 255]])
            ->add('skill_description', TextareaType::class, [
                'required' => false,
                'attr' => ['id' => 'skillEditor', 'class' => 'skillEditor']])
            ->add('domains', null, [
                'required' => true,
                'label' => 'Domain',
                'attr' => ['id' => 'domains', 'style' => 'height: 100px;']]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entity::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_skill';
    }
}
