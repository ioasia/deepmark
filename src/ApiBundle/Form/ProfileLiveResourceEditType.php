<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\Profile;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ProfileLiveResourceEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('entity')
            ->remove('otherEmail')
            ->add('firstName', TextType::class, array(
                'required' => true,
            ))
            ->add('lastName', TextType::class, array(
                'required' => true,
            ))
            ->add('position', TextType::class, array(
                'required' => false,
            ))
            ->add('fixedPhone', TelType::class, array(
                'required' => false,
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\d]/',
                        'message' => 'Incorrect value. It should be numeric. Region/country prefix are allowed too.',
                    ]),
                ],
            ))
            ->add('mobilePhone', TelType::class, array(
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Regex([
                        'pattern' => '/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\d]/',
                        'message' => 'Incorrect value. It should be numeric. Region/country prefix are allowed too.',
                    ]),
                ],
            ))
            ->add('entity', EntityType::class, array(
                'class' => 'ApiBundle:Entity',
                'required' => true,
            ))
            ->add('person', PersonPassType::class, array(
                'required' => false,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Profile::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile_edit';
    }
}
