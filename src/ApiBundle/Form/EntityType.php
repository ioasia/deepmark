<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\Organisation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as SymfonyEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('street')
            ->add('expansionStreet')
            ->add('postalCode')
            ->add('socialReason')
            ->add('siret')
            ->add('declaration')
            ->add('city')
            ->add('country')
            ->add('organisation', SymfonyEntityType::class, array(
                'choice_label' => 'designation',
                'class' => Organisation::class,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Entity',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_entity';
    }
}
