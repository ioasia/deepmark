<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateInterventionStep1Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ending = new \DateTime();
        $ending->modify('1 week');
        $builder
            ->add('designation')
            ->add('logo', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class, array(
                'class' => FileDescriptor::class,
                'required' => false,
            ))
            ->add('comment', TextType::class, array(
                'required' => true,
            ))
            ->add('beginning', DateType::class, array(
                'data' => new \DateTime(),
            ))
            ->add('ending', DateType::class, array(
                'data' => $ending,
            ))
            ->add('customer', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class, array(
                'class' => Profile::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.profileType', 'type')
                        ->where('type.appId = ?1')
                        ->setParameter(1, ProfileVariety::CUSTOMER)
                        ;
                },
            ))
            ->add('modules', CollectionType::class, array(
                'entry_type' => ModuleStep1Type::class,
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => array(
                    'class' => 'modules-selector',
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'createInterventionStep1';
    }
}
