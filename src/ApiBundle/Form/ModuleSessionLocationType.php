<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 13:42.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\ModuleSession;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleSessionLocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('session', SessionType::class)
            ->add('sessionPlace', SessionPlaceType::class, array(
                'required' => true,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ModuleSession::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'module_session';
    }
}
