<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 11:57.
 */

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileManagerEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('person')
            ->add('firstName', TextType::class, array(
                'required' => true,
            ))
            ->add('lastName', TextType::class, array(
                'required' => true,
            ))
            ->add('fixedPhone', TelType::class, array(
                'required' => false,
            ))
            ->add('mobilePhone', TelType::class, array(
                'required' => false,
            ))
            ->add('person', PersonPassType::class, array(
                'required' => false,
            ))
            ->add('otherEmail', EmailType::class, array(
                'required' => false,
            ));
    }

    public function getParent()
    {
        return ProfileLiveResourceType::class;
    }
}
