<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/08/18
 * Time: 11:31.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\DetailedEvaluationLine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetailedEvaluationLineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('title', CheckboxType::class, array(
                'required' => false,
                'label' => 'Est-ce le titre ?',
            ))
            ->add('notation', TextType::class, array('required' => true))
            ->add('wording', TextType::class, array('required' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DetailedEvaluationLine::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_detailedEvaluationLine';
    }
}
