<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/08/18
 * Time: 11:31.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\ReportLine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportLineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('title', CheckboxType::class, array(
                'required' => true,
                'label' => 'Est-ce le titre ?',
            ))
            ->add('wording', TextType::class, array('required' => true))
            ->add('notation', NumberType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ReportLine::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_reportLine';
    }
}
