<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16/08/18
 * Time: 14:20.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('beginning', DateType::class, array(
                'required' => true,
            ))
            ->add('ending', DateType::class, array('required' => true))
            ->add('designation', TextType::class, array('required' => true))
            ->add('customer', EntityType::class, array(
                'mapped' => true,
                'required' => true,
                'class' => Profile::class,
                'choice_label' => function ($profile) {
                    return $profile->getDisplayName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->leftJoin('p.profileType', 'profileVariety')
                        ->where('profileVariety.appId = 4');
                },
            ))
            ->add('admin', EntityType::class, array(
                'mapped' => true,
                'required' => true,
                'class' => Profile::class,
                'choice_label' => function ($profile) {
                    return $profile->getDisplayName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->leftJoin('p.profileType', 'profileVariety')
                        ->where('profileVariety.appId = 1');
                },
            ))
            ->add('modules', CollectionType::class, array(
                'entry_type' => ModuleType::class,
                'allow_add' => true,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Intervention::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_intervention';
    }
}
