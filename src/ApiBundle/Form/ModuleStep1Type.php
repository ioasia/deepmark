<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20/08/18
 * Time: 13:44.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\Module;
use ApiBundle\Entity\StoredModule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleStep1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $duration = new \DateTime();
        $duration->setTime(0, 0, 0);

        $builder
            ->add('designation')
            ->add('quantity', NumberType::class, array(
                'required' => true,
            ))
            ->add('storedModule', EntityType::class, array(
                'mapped' => true,
                'required' => true,
                'class' => StoredModule::class,
                'choice_label' => function (StoredModule $storedModule) {
                    return $storedModule->getDesignation();
                },
            ))
            ->add('price', MoneyType::class, array(
                'required' => true,
                'data' => 0,
            ))
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Module::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'createModuleStep1';
    }
}
