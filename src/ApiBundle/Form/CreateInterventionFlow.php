<?php

namespace ApiBundle\Form;

use Craue\FormFlowBundle\Form\FormFlow;

class CreateInterventionFlow extends FormFlow
{
    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'Step 1',
                'form_type' => 'ApiBundle\Form\CreateInterventionStep1Type',
            ),
            array(
                'label' => 'Step 2',
                'form_type' => 'ApiBundle\Form\CreateInterventionStep2Type',
            ),
            array(
                'label' => 'Step 3',
                'form_type' => 'ApiBundle\Form\CreateInterventionStep3Type',
            ),
            array(
                'label' => 'confirmation',
            ),
        );
    }
}
