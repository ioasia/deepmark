<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 10:49.
 */

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username')
            ->remove('plainPassword');
    }

    public function getParent()
    {
        return PersonType::class;
    }
}
