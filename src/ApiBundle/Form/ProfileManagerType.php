<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 11:57.
 */

namespace ApiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileManagerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('person')
            ->remove('companyPost')
            //->remove('entity')
            ->add('firstName', TextType::class, array(
                'required' => true,
            ))
            ->add('lastName', TextType::class, array(
                'required' => true,
            ))
            ->add('fixedPhone', TelType::class, array(
                'required' => false,
            ))
            ->add('mobilePhone', TelType::class, array(
                'required' => false,
            ))
            ->add('entity', EntityType::class, array(
                'class' => 'ApiBundle:Entity',
                'required' => true,
            ))
            ->add('person', PersonPassType::class, array(
                'required' => true,
            ));
    }

    public function getParent()
    {
        return ProfileLiveResourceType::class;
    }
}
