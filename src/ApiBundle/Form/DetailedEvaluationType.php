<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16/08/18
 * Time: 12:45.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\DetailedEvaluation;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Profile;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetailedEvaluationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $option
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('intervention', EntityType::class, array(
                'required' => true,
                'class' => Intervention::class,
                'choice_label' => 'designation',
            ))
            ->add('admin', EntityType::class, array(
                'required' => true,
                'class' => Profile::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.profileType = 1');
                },
                'choice_label' => function ($profile) {
                    return $profile->getFirstName().' '.$profile->getLastName();
                },
            ))
            ->add('learner', EntityType::class, array(
                'required' => true,
                'class' => Profile::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.profileType = 3');
                },
                'choice_label' => function ($profile) {
                    return $profile->getFirstName().' '.$profile->getLastName();
                },
            ))
            ->add('globalScore', TextType::class, array('required' => true));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DetailedEvaluation::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_detailedEvaluation';
    }
}
