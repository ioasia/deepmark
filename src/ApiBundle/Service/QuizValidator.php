<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 09.01.19
 * Time: 10:56.
 */

namespace ApiBundle\Service;

use ApiBundle\Utils\Contract\QuizValidatorAbstract;
use Spliced\SurveyMonkey\SurveyMonkeyApiException;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class QuizValidator.
 */
class QuizValidator extends QuizValidatorAbstract
{
    /**
     * @var array
     */
    private $mandatoryFields = ['title'];

    /**
     * @var array
     */
    private $constraintsStore = [];

    /**
     * @var array
     */
    private $fieldsToValidate = [];

    /**
     *  Here you define rules that apply for the form request
     *  if the type will not be present in the request then it will
     *  be removed for the duration of the request.
     */
    private function setValidationRules(): void
    {
        $this->constraintsStore = [
            'title' => new NotBlank(),
            'type' => new All([new Regex('/^[a-zA-z]+$/i')]),
            'subtype' => new All([new Regex('/^[a-zA-z]+$/i')]),
            'questions' => new All([new NotBlank()]),
            'quizzable' => new All([new NotBlank()]),
            'options' => new All([new NotBlank()]),
            'range' => new All([new NotBlank()]),
            'quiz_options' => new All([new NotBlank()]),
        ];
    }

    /**
     * Validates quiz params. Depending on request
     * apply different validator constraints.
     *
     * @throws SurveyMonkeyApiException
     *
     * @return object
     */
    public function validate(): ConstraintViolationList
    {
        foreach ($this->mandatoryFields as $field) {
            if (!array_key_exists($field, $this->quiz)) {
                throw new SurveyMonkeyApiException('At least field title has to be set');
            }
        }

        $this->aggregateValuesForValidation();
        $this->setValidationRules();
        $this->excludeNonExistentFields();

        $constraint = new Constraints\Collection($this->constraintsStore);
        $violations = $this->validator->validate($this->fieldsToValidate, $constraint);

        if ($violations->count() > 0) {
            throw new SurveyMonkeyApiException('Constraint violation. One of the fields has not passed validation');
        }

        return $violations;
    }

    /**
     *   Gets all values from the form fields. These
     *   would be ready to validate later. Array has to be flatten one
     *   if value exists under the same key it doesn't matter as we are checking
     *   against value not logic.
     */
    private function aggregateValuesForValidation(): void
    {
        $toValidate['title'] = $this->quiz['title'];

        if (isset($this->quiz['questions']) && count($this->quiz['questions']) > 0) {
            foreach ($this->quiz['questions'] as $question) {
                $toValidate['questions'][] = $question['question'];
                $toValidate['type'][] = $question['type'];
                $toValidate['subtype'][] = $question['type'];

                if (isset($question['options'])) {
                    foreach ($question['options'] as $option) {
                        $toValidate['options'][] = $option;
                    }
                }

                if ('' != $this->quiz['quizable']) {
                    if (isset($this->quiz['quiz_options'])) {
                        foreach ($this->quiz['quiz_options']['feedback'] as $key => $value) {
                            if (!is_array($value)) {
                                $toValidate['quiz_options'][$key] = $value;
                            } else {
                                $toValidate[$key] = $value;
                            }
                        }
                    }
                }
            }
        }

        $this->fieldsToValidate = $toValidate;
    }

    /**
     *  If a key doesn't exist in the input array
     *  then the assigned validator will be deleted
     *  from the queue.
     */
    private function excludeNonExistentFields(): void
    {
        foreach ($this->constraintsStore as $key => $val) {
            if (!array_key_exists($key, $this->fieldsToValidate)) {
                unset($this->constraintsStore[$key]);
            }
        }
    }
}
