<?php

namespace ApiBundle\Service;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class NexmoSMS
{
    private $url;
    private $apiKey;
    private $apiSecret;

    public function __construct($url, $apiKey, $apiSecret)
    {
        $this->url = $url;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    public function sendSMS($destNumber, $text, $from = 'DeepMark')
    {
        $params = [
            'api_key' => $this->apiKey,
            'api_secret' => $this->apiSecret,
            'to' => $destNumber,
            'from' => $from,
            'text' => $text,
        ];

        return $this->callService('POST', '/sms/json', $params);
    }

    private function callService($method, $url, $params)
    {
        $client = new Client();

        $headers = [
            'apiKey' => $this->apiKey,
            'apiSecret' => $this->apiSecret,
        ];

        $parameters['http_errors'] = false;
        //$parameters['headers'] = $headers;

        //array_merge($headers, $params);

        if ('POST' == $method) {
            $parameters[RequestOptions::JSON] = $params;
        }

        $res = $client->request($method, $this->url.$url, $parameters);

        return json_decode($res->getBody(), true);
    }
}
