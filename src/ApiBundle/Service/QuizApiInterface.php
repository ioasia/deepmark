<?php
namespace ApiBundle\Service;

use ApiBundle\Entity\Quizes;

interface QuizApiInterface
{
    /**
     * todo instead of array use some DTO.
     *
     * @param array $quizData
     *
     * @throws SurveyMonkeyApiException
     */
    public function create(array $quizData = []): Quizes;

    /**
     * @param Quizes $quizes
     */
    public function publish(Quizes $quiz): void;

    /**
     * mostly to force each service to keep.
     *
     * @return string
     */
    public function getQuizesType(): string;
}
