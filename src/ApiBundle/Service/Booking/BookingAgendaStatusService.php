<?php

namespace ApiBundle\Service\Booking;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;

class BookingAgendaStatusService
{
    private $entityManager;

    private $bookingAgendaRepository;

    private $isSet = false;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->bookingAgendaRepository = $entityManager->getRepository(BookingAgenda::class);
    }

    public function setStatus($bookingAgendaId, $status): void
    {
        $bookingAgenda = $this->bookingAgendaRepository->find($bookingAgendaId);

        if (empty($bookingAgenda)) {
            return;
        }

        $status = $this->entityManager->getRepository(STATUS::class)->findOneBy(['appId' => $status]);

        if (empty($status) || $status == $bookingAgenda->getStatus()) {
            return;
        }

        $bookingAgenda->setStatus($status);

        $this->entityManager->persist($bookingAgenda);
        $this->entityManager->flush();

        $this->isSet = true;
    }

    public function isStatusSet(): bool
    {
        return $this->isSet;
    }
}
