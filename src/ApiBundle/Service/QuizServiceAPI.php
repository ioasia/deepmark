<?php

namespace ApiBundle\Service;

use AdminBundle\Services\QuizService;
use ApiBundle\Entity\Exercises;
use ApiBundle\Entity\Marks;
use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\QuizQuestions;
use ApiBundle\Entity\QuizQuestionsItems;
use ApiBundle\Entity\QuizQuestionsXref;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\QuizUsersAnswers;
use ApiBundle\Entity\QuizUsersPlayed;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QuizServiceAPI {

    private $answer;
    private $store;
    private $container;
    private $quizService;
    private $em;
    private $module;
    private $quiz;
    private $question;
    private $validating;
    private $play_id;
    private $play;
    private $items_ids = [];

    /**
     * Construct
     */
    public function __construct(ContainerInterface $container, QuizService $quizService) {
        // Init
        $this->container = $container;
        $this->quizService = $quizService;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->store = [];
    }

    /**
     * Sanitize quiz to get pre-formated object for views
     * 
     * @param Quizes $quiz
     */
    public function sanitizeQuiz(Quizes $quiz, $treatParams = false) {
        $quizSteps = [];
        if ($quiz) {
            // Init 
            $isFirst = true;
            // Foreach on questions of this quiz
            foreach ($quiz->getQuestions() as $question) {
                if ($question->getStep()->getStatus() != -1 && $question->getQuestion()->getStatus() != -1) {
                    // Get step id of this question
                    $step_id = $question->getStep()->getId();
                    if (is_string($question->getStep()->getParams())) {
                        $step_pos = json_decode($question->getStep()->getParams())->position;
                    } else {
                        $step_pos = $question->getStep()->getParams()->position;
                    }

                    // Json decode params step & question
                    if ($treatParams === true) {
                        if (is_string($question->getStep()->getParams())) {
                            $question->getStep()->setParams(json_decode($question->getStep()->getParams()));
                        }
                        $question->getQuestion()->setParams(json_decode($question->getQuestion()->getParams()));
                    }
                    $questionE = $question->getQuestion();
                    // Get items for current question
                    $items = $this->quizService->getItems($questionE->getId());

                    if ($treatParams === true) {
                        // Get specifics
                        foreach ($items as $key => $item) {     
                            // Json decode specifics
                            $specifics = json_decode($item->getSpecifics());

                            // Set specifics
                            $items[$key]->setSpecifics($specifics);

                            // Disorder for text to fill
                            if ($question->getQuestion()->getType() == 'text_to_fill') {
                                if ($specifics && $specifics->options) {
                                    $options = $specifics->options;
                                    if (isset($specifics->options_free)) {
                                        $options = array_merge($options, $specifics->options_free);
                                    }
                                    shuffle($options);
                                    $items[$key]->specifics_disordered = $options;
                                }
                            }
                        }
                    }
                    // Modify items
                    $questionE->items_disordered = $items;
                    shuffle($questionE->items_disordered);
                    $questionE->items = $items;

                    // Question params
                    if (is_string($question->getQuestion()->getParams())) {
                        $questionParams = json_decode($question->getQuestion()->getParams());
                    } else {
                        $questionParams = $question->getQuestion()->getParams();
                    }

                    // Check if already in array
                    if (!isset($quizSteps[$step_pos])) {
                        // Create array with first question
                        $quizSteps[$step_pos] = array(
                            'step_id' => $step_id,
                            'step_pos' => $step_pos,
                            'step' => $question->getStep(),
                            'is_first' => $isFirst,
                            'questions' => []
                        );
                        // Set question position
                        $quizSteps[$step_pos]['questions'][$questionParams->position] = $questionE;
                    } else {
                        // Add other question to this step
                        $quizSteps[$step_pos]['questions'][$questionParams->position] = $questionE;
                    }

                    $isFirst = false;
                }
            }
        }

        // Order steps
        ksort($quizSteps);
        foreach ($quizSteps as $key => $step) {
            if ($key == 0) {
                $quizSteps[$key]['is_first'] = 1;
            } else {
                $quizSteps[$key]['is_first'] = 0;
            }
        }

        // Order questions
        foreach ($quizSteps as $key => $step) {
            // Check questions positions
            if ($quizSteps[$key]['step']->getOrdering() === 1 && $treatParams === true) {
                // Shuffle if step disorderly
                shuffle($quizSteps[$key]['questions']);
            } else {
                // If no, ksort by position
                ksort($quizSteps[$key]['questions']);
            }

            // Check number questions to show
            if ($treatParams === true) {
                $questionToShow = $quizSteps[$key]['step']->getParams()->questions_to_show;
                if ($questionToShow != 0) {
                    $quizSteps[$key]['questions'] = array_slice($quizSteps[$key]['questions'], 0, $questionToShow);
                }
            }
        }
        return $quizSteps;
    }

    /**
     * Validate answer
     * 
     * @param array $answer
     */
    public function validateQuestion(array $answer = null) {

        // Validating array init
        $this->validating = array(
            'status' => 'fail',
            'points' => 0,
            'points_max' => 0
        );
        $this->answer = $answer;

        // Check question_id & quiz_id & module_id sended
        if ($answer['question_id'] && $answer['quiz_id'] && $answer['module_id'] && $answer['play_id']) {
            // Init
            $this->play_id = $answer['play_id'];
            $moduleId = $answer['module_id'];
            $quizId = $answer['quiz_id'];
            $questionId = $answer['question_id'];

            // Get Module
            $this->module = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository(Module::class)
                    ->findOneBy(['id' => $moduleId]);

            // Get Quiz
            $this->quiz = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository(Quizes::class)
                    ->findOneBy(['id' => $quizId]);

            // Get Question
            $this->question = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository(QuizQuestions::class)
                    ->findOneBy(['id' => $questionId]);


            // Check question_id exists
            if ($this->module && $this->quiz && $this->question) {
                $this->validating['points_max'] = $this->question->getScore_to_pass();
                // Get question params
                $questionParams = json_decode($this->question->getParams());
                // Get items
                $items = $this->getItems($questionId);
                foreach ($items as $item) {
                    $this->items_ids[] = $item->getId();
                }
                $this->validating['answers'] = [];
                $this->validating['is_survey'] = $this->quiz->getIsSurvey();

                // Check items exists
                if ($items) {

                    // Manage validation for each type of question
                    switch ($this->question->getType()) {
                        // TYPE QCU
                        case 'qcu' :
                            // Total to be checked
                            $totalToCheck = 1;
                            $totalChecked = 0;

                            // Good Answer
                            foreach ($items as $item) {
                                if ($item->getValue()) {
                                    $this->validating['answers'][]['correct'] = $item->getName();
                                } else {
                                    $this->validating['answers'][]['fail'] = $item->getName();
                                }
                            }
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value'])) {
                                foreach ($items as $item) {
                                    // If id selected match with id selected by admin = success
                                    if ($item->getId() == $answer['answer']['value'] && $item->getValue() == 1) {
                                        $totalChecked = 1;
                                    }
                                }
                            }
                            break;
                        // TYPE QCM
                        case 'qcm' :// Total to be checked
                            $totalToCheck = 0;
                            // Add to totalToCheck
                            foreach ($items as $item) {
                                if ($item->getValue() == 1) {
                                    $totalToCheck++;
                                    // Good answers
                                    $this->validating['answers'][]['correct'] = $item->getName();
                                } else {
                                    $this->validating['answers'][]['fail'] = $item->getName();
                                }
                            }
                            // Total checked +1 = good answer / -1 = bad answer
                            $totalChecked = 0;
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value']) && is_array($answer['answer']['value'])) {

                                foreach ($answer['answer']['value'] as $valueToCheck) {
                                    $passed = false;
                                    foreach ($items as $item) {
                                        // If id selected match with id selected by admin = add to totalChecked
                                        if ($item->getId() == $valueToCheck && $item->getValue() == 1) {
                                            $passed = true;
                                        }
                                    }
                                    // If good = +1 / bad = -1
                                    if ($passed == true) {
                                        $totalChecked++;
                                    } elseif($this->question->getIsSurvey() == 0) {
                                        $totalChecked--;
                                    }
                                }
                            }
                            break;
                        // TYPE DRAG_AND_DROP
                        case 'drag_and_drop' :
                            $totalToCheck = count($items);
                            $totalChecked = 0;
                            $itemsOrder = [];
                            // Create first array to create matchs from DB
                            foreach ($items as $item) {
                                $itemSpecifics = json_decode($item->getSpecifics());
                                $itemsOrder[] = $item->getValue() . $itemSpecifics->match;
                                // Good answers
                                $this->validating['answers'][] = [$item->getName(), $item->getValue(), $itemSpecifics->match];
                            }
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value']) && is_array($answer['answer']['value'])) {
                                // Check match OK
                                foreach ($answer['answer']['value'] as $key => $value) {
                                    foreach ($itemsOrder as $item) {
                                        if ($value[0] . $value[1] == $item) {
                                            $totalChecked++;
                                        }
                                    }
                                }
                            }
                            break;
                        // TYPE ORDERING
                        case 'ordering' :
                            $totalToCheck = count($items);
                            $totalChecked = 0;
                            // Good answer
                            foreach ($items as $item) {
                                $this->validating['answers'][] = $item->getName();
                            }
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value']) && is_array($answer['answer']['value'])) {
                                $itemsOrder = [];
                                // Create first array to check same order
                                foreach ($items as $item) {
                                    $itemsOrder[] = $item->getName();
                                }
                                // Check same order
                                foreach ($answer['answer']['value'] as $key => $value) {
                                    if (isset($itemsOrder[$key]) && $value == $itemsOrder[$key]) {
                                        $totalChecked++;
                                    } else {
                                        // To check if better with or without, each work, but depend of specs expected about this
                                        //$totalChecked--;
                                    }
                                }
                            }
                            break;
                        // TYPE CATEGORY
                        case 'category' :
                            $totalToCheck = count($items);
                            $totalChecked = 0;
                            // Good answer
                            foreach ($items as $key => $item) {
                                $this->validating['answers'][] = [$item->getName(), $item->getValue()];
                            }
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value']) && is_array($answer['answer']['value'])) {
                                // Create first array to check same order
                                foreach ($items as $key => $item) {
                                    if ($answer['answer']['value'][$key] == $item->getValue()) {
                                        $totalChecked++;
                                    }
                                }
                            }
                            break;
                        // TYPE TEXT_TO_FILL
                        case 'text_to_fill' :
                            $totalToCheck = 0;
                            $totalChecked = 0;
                            // Good answer
                            $itemSpecifics = json_decode($items[0]->getSpecifics());
                            $text = $items[0]->getValue();
                            if ($itemSpecifics) {
                                foreach ($itemSpecifics->options as $key => $item) {
                                    $this->validating['answers'][] = $item;
                                    $text = str_replace('<span class="marker" data-pos="' . $key . '"></span>', '<span class="marker" data-pos="' . $key . '"><strong>' . $item . '</strong></span>', $text);
                                }
                            }
                            $this->validating['answers_question'] = $text;
                            // Check answer exists
                            if (isset($answer['answer']) && isset($answer['answer']['value']) && is_array($answer['answer']['value'])) {
                                if ($itemSpecifics) {
                                    foreach ($itemSpecifics->options as $key => $item) {
                                        $totalToCheck++;
                                        foreach ($answer['answer']['value'] as $value) {
                                            if ($value[0] == $key && $value[1] == $item) {
                                                $totalChecked++;
                                            }
                                        }
                                    }
                                }
                            }

                            break;

                        case 'open' :
                            $totalToCheck = 0;
                            $totalChecked = 0;
                            break;


                        case 'scoring' :
                            if ($this->validating['is_survey'] == 0) {
                                // Question
                                $totalToCheck = 1;
                                $totalChecked = 0;
                                foreach ($items as $item) {
                                    if ($item->getValue()) {
                                        $this->validating['answers'] = $item->getValue();
                                        $this->validating['specifics'] = json_decode($item->getSpecifics());
                                        $this->validating['type'] = $item->getName();
                                    }
                                    if ($item->getValue() == $answer['answer']['value']) {
                                        $totalChecked = 1;
                                    }
                                }
                            } else {
                                // Survey
                                $totalToCheck = 0;
                                $totalChecked = 0;
                            }
                            break;
                    }

                    // Is survey
                    if($this->question->getIsSurvey() == 1) {
                        $totalToCheck = 0;
                    }
                    
                    // Result
                    if ($totalChecked == 0) {
                        if ($this->question->getType() == 'open') {
                            $this->validating['status'] = 'wait';
                            $this->validating['points'] = 0;
                        } else {
                            $this->validating['status'] = 'fail';
                        }
                    } else {
                        // Check if partial accepted or not
                        if ($questionParams->answer_type == 2) {
                            // PARTIAL accepted
                            // Check totalToCheck / totalChecked & score to pass global
                            if($totalToCheck <= 0) {
                                $this->validating['points'] = 0;
                            } else {
                                $pourcent = ($totalChecked / $totalToCheck) * 100;
                                // Calcul pourcent OK Round to upper or lower int 
                                $this->validating['points'] = max(round($this->question->getScore_to_pass() * ($pourcent / 100)), 0);
                            }
                            if ($this->validating['points'] == 0) {
                                $this->validating['status'] = 'fail';
                            } else {
                                $this->validating['status'] = 'success';
                            }
                        } else {
                            // PARTIAL NOT accepted
                            if ($totalChecked == $totalToCheck) {
                                $this->validating['status'] = 'success';
                                $this->validating['points'] = $this->question->getScore_to_pass();
                            } else {
                                $this->validating['status'] = 'fail';
                            }
                        }
                    }
                }
            }
            // Save answer
            if ($this->saveAnswer() === false) {
                // Error status to block the quiz if too late or error with timer
                $this->validating['status'] = 'error';
            }
            // Type = 2 Learning (1 - Test = No answers in modals)
            if ($this->module->getTypeQuiz() != 2 && isset($this->validating['answers'])) {
                $this->validating['answers'] = false;
            }
        }
        return $this->validating;
    }

    /**
     * Get Items
     * @param int $questionId
     */
    public function getItems($questionId, $status = 1) {
        return $this->container
                        ->get('doctrine')
                        ->getManager()
                        ->getRepository(QuizQuestionsItems::class)
                        ->findBy(['question_id' => $questionId, 'status' => $status]);
    }

    /**
     * Create play_id
     * 
     * @param int $module_id
     * @param int $quiz_id
     * @param int $user_id
     */
    public function savePlay($module_id, $quiz_id, $user_id) {

        // Get Module
        $this->module = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(Module::class)
                ->findOneBy(['id' => $module_id]);


        // Get Quiz
        $this->quiz = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(Quizes::class)
                ->findOneBy(['id' => $quiz_id]);


        // Get Previous Play
        $played = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(QuizUsersPlayed::class)
                ->findBy(['user_id' => $user_id, 'module' => $this->module, 'quiz' => $this->quiz]);

        
        if ($played && count($played) >= $this->module->getIteration()) {
            // Too much play
            return false;
        } else {
            $play = new QuizUsersPlayed();
            $play->setUser_id($user_id);
            $play->setQuiz($this->quiz);
            $play->setModule($this->module);

            // Save
            $this->em->persist($play);
            $this->em->flush();

            return array('play_count' => count($played), 'play_id' => $play->getId());
        }
    }

    /**
     * Update an answer
     * 
     * @param int $play_id
     * @param int $question_id
     * @param int $file
     */
    public function updateRecordAnswer($play_id, $question_id, $file) {

        $play = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(QuizUsersPlayed::class)
                ->findOneBy(['id' => $play_id]);
        $answers = $play->getAnswers();
        if ($answers) {
            foreach ($answers as $key => $answer) {
                if ($answer->getQuestionId() == $question_id) {
                    $params = json_decode($answer->getParams());

                    if ($params) {
                        $params->answer->record = $file;
                    }
                    $answer->setParams(json_encode($params));

                    $this->em->persist($answer);
                    $this->em->flush();
                }
            }
        }
    }

    /**
     * Save answers 
     */
    private function saveAnswer() {
        if ($this->play_id) {
            // Get Play
            $this->play = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository(QuizUsersPlayed::class)
                    ->findOneBy(['id' => $this->play_id]);
            if ($this->play) {
                if ($this->checkTimer() === true) {
                    // Set answer
                    $answer = new QuizUsersAnswers();
                    $answer->setPlay($this->play);
                    $answer->setQuestion($this->question);
                    $answer->setScores($this->validating['points']);
                    $answer->setStatus($this->validating['status'] === 'success' ? 1 : 0);
                    $answer->setParams(json_encode(array(
                        'answer' => $this->answer['answer'],
                        'items_ids' => $this->items_ids,
                        'timer_total' => $this->answer['timer_total'],
                        'timer' => $this->answer['timer'],
                        'file_descriptor' => $this->answer['file_descriptor']
                    )));

                    // Save
                    $this->em->persist($answer);
                    $this->em->flush();

                    // -- Save score total quiz
                    // Check score needed
                    $scoreNeeded = 0;
                    $questions = $this->quiz->getQuestions();
                    if ($questions) {
                        foreach ($questions as $question) {
                            $scoreNeeded += $question->getQuestion()->getScore_to_pass();
                        }
                    }
                    // Check score realized
                    $previousAnswers = $this->getAnswers($this->play, false);
                    $score = 0;
                    if ($previousAnswers) {
                        foreach ($previousAnswers['answers'] as $currAnswer) {
                            $currAnswer->setParams(json_encode($currAnswer->getParams()));
                            $score += $currAnswer->getScores();
                        }
                    }

                    // Set score for this game
                    $this->play->setScores($score > 0 ? round($score / $scoreNeeded * 100) : 0);

                    // Save
                    $this->em->persist($this->play);
                    $this->em->flush();

                    return $answer->getId();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * Check timer
     */

    private function checkTimer() {
        // Check if timer active
        $durationActive = $this->module->getDurationActive();
        if ($durationActive) {
            // Get timer value
            $unix = strtotime('1970-01-01 00:00:00');
            $timer = $this->module->getDuration()->getTimestamp();
            $diff_accepted = abs($timer - $unix);

            // Get time started quiz
            $created = $this->play->getCreated()->getTimestamp();
            $now = time();

            // Get the difference between creation and now
            $diff = abs($now - $created);
            /*
              var_dump($this->module->getDuration());
              var_dump($this->play->getCreated());
              var_dump('timer', $timer);
              var_dump('$unix', $unix);
              var_dump('$diff_accepted', $diff_accepted);
              var_dump('$now', $now);
              var_dump('$created', $created);
              var_dump('$diff', $diff); */
            if ($diff < $diff_accepted) {
                return true;
            } else {
                return false;
            }
        } else {
            // No timer active for this module
            return true;
        }
    }

    /**
     * Get results
     * 
     * @param ApiBundle\Entity\Quizes $quiz
     * @param int $user_id
     */
    public function getResults(Module $module, Quizes $quiz, $user_id = false, $withAnswers = false) {
        $this->quiz = $quiz;
        $this->module = $module;
        // Get Previous Play
        $search = array('quiz' => $this->quiz, 'module' => $this->module);
        if ($user_id != false) {
            $search['user_id'] = $user_id;
        }
        $played = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(QuizUsersPlayed::class)
                ->findBy($search);

        if (!$withAnswers) {
            return $played;
        } else {
            $allPlayed = [];
            foreach ($played as $play) {
                $allPlayed[] = array(
                    'quizPlayed' => $play,
                    'quizAnswers' => $this->getAnswers($play, false)
                );
            }
            return $allPlayed;
        }
    }

    /**
     * Get answers
     * 
     * @param QuizUsersPlayed $play
     * @return type
     */
    public function getAnswers(QuizUsersPlayed $play, $checkItems = true) {
        if ($play) {
            // Get Answers
            $answers = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository(QuizUsersAnswers::class)
                    ->findBy(['play' => $play]);

            // Totalize answer by questionId
            $totalAnswer = [];

            // Scores & status
            $scores = 0;
            $status = ['success' => 0, 'fail' => 0];
            $questions = [];
            if ($answers) {
                foreach ($answers as $answer) {
                    $scores += $answer->getScores();
                    if ($answer->getStatus() === 1) {
                        $status['success'] ++;
                    } else {
                        $status['fail'] ++;
                    }
                    if ($answer->getParams() && is_string($answer->getParams())) {
                        $paramsElement = json_decode($answer->getParams());
                        if(isset($paramsElement->file_descriptor)  && is_string($paramsElement->file_descriptor)){
                            $paramsElement->file_descriptor = json_decode($paramsElement->file_descriptor);
                        }
                        $answer->setParams($paramsElement);
                    }

                    $questions[] = $answer->getQuestion();

                    if (!isset($totalAnswer[$answer->getQuestion()->getId()])) {
                        $totalAnswer[$answer->getQuestion()->getId()] = 1;
                    } else {
                        $totalAnswer[$answer->getQuestion()->getId()] ++;
                    }
                }
            }


            // Get step
            $steps = [];
            $totalPoints = 0;
            $count = 0;
            if ($questions) {
                foreach ($questions as $question) {
                    // Question Id Step id
                    $step_id = $question->getQuestionXref()[0]->getStep()->getId();
                    $question_id = $question->getId();
                    if ($checkItems) {
                        // Get all items for answer (status 0, 1) if admin change the quiz after learner plays
                        $items = $this->getItems($question_id, [0, 1]);
                        if ($items) {
                            foreach ($items as $key => $item) {
                                if (is_string($item->getSpecifics())) {
                                    $specifics = json_decode($item->getSpecifics());
                                    $items[$key]->setSpecifics($specifics);
                                }
                                $question->setItem($item);
                            }
                        }
                        if (is_string($question->getParams())) {
                            $question->setParams(json_decode($question->getParams()));
                        }
                        $question->items = $items;
                        shuffle($items);
                        $question->items_disordered = $items;
                    }
                    if (!isset($steps[$step_id])) {
                        $steps[$step_id] = array(
                            'step_id' => $step_id,
                            'step_pos' => $step_id,
                            'step' => $question->getQuestionXref()[0]->getStep(),
                            'is_first' => ($count == 0 ? true : false),
                            'questions' => [$question]
                        );
                    } else {
                        $steps[$step_id]['questions'][] = $question;
                    }
                    $totalPoints += $question->getScore_to_pass();
                    $count++;
                }
            }

            return array(
                'steps' => $steps,
                'questions' => $questions,
                'answers' => $answers,
                'scores' => $scores,
                'status' => $status,
                'total_points' => $totalPoints,
                'total_questions' => $count,
                'total_answers' => $totalAnswer
            );
        }
    }

    /**
     * Update an answer of the trainer
     *
     * @param int $exercices_id
     * @param int $question_item_id
     * @param int $file
     */
    public function updateRecordAnswerByTrainer($exercices_id, $question_item_id, $file) {
        $marks = $this->container
            ->get('doctrine')
            ->getManager()
            ->getRepository(Marks::class)
            ->findOneBy(['exercises' => $exercices_id, 'questionsItem' => $question_item_id]);

        if (!$marks) {
            $marks = new Marks();
        }
        $exercices = $this->container
            ->get('doctrine')
            ->getManager()
            ->getRepository(Exercises::class)
            ->find($exercices_id);

        $questionsItem = $this->container
            ->get('doctrine')
            ->getManager()
            ->getRepository(QuizQuestionsItems::class)
            ->find($question_item_id);

        $marks->setExercises($exercices);
        $marks->setQuestionsItem($questionsItem);
        $marks->setSpecifics($file);
        $this->em->persist($marks);
        $this->em->flush();
    }

    /**
     * Update an answer of the trainer
     *
     * @param int $exercices_id
     */
    public function saveGradeByTrainer($exercices_id, $grade) {
        $exercices = $this->container
            ->get('doctrine')
            ->getManager()
            ->getRepository(Exercises::class)
            ->find($exercices_id);

        if (!$exercices) {
            $exercices = new Exercises();
        }

        $exercices->setGrade($grade);
        $this->em->persist($exercices);
        $this->em->flush();
    }

    /**
     * Update an status of the Exercises
     *
     * @param int $exercices_id
     * @param string $status
     */
    public function saveExercisesStatus($exercices_id, $status) {
        $exercices = $this->container
            ->get('doctrine')
            ->getManager()
            ->getRepository(Exercises::class)
            ->find($exercices_id);

        if (!$exercices) {
            $exercices = new Exercises();
        }

        $exercices->setStatus($status);
        $this->em->persist($exercices);
        $this->em->flush();
    }
}
