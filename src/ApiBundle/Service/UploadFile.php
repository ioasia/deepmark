<?php

namespace ApiBundle\Service;

use ApiBundle\Entity\FileDescriptor;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadFile
{
    private $targetDir;
    private $rootPath;

    public function __construct($targetDir, $rootPath)
    {
        $this->targetDir = $targetDir;
        $this->rootPath = $rootPath;
    }

    public function setTargetDirectory($targetDir)
    {
        $this->targetDir = $this->rootPath.'/..'.$targetDir;
    }

    public function getTargetDirectory()
    {
        return $this->targetDir;
    }

    public function getRootPath()
    {
        return $this->rootPath;
    }

    public function saveTempFile(\SplFileInfo $file)
    {
        $fileName = sha1(uniqid(mt_rand(), true)).'.'.$file->getExtension();
        $fileDescriptor = new FileDescriptor();
        $fileDescriptor->setName($file->getFilename());
        $fileDescriptor->setPath($fileName);
        $fileDescriptor->setSize($file->getSize());
        $fileDescriptor->setMimeType($file->getExtension());
        $fileDescriptor->setDirectory($this->targetDir);
        //rename($file->getPathname(), $this->targetDir.$fileName);
        copy($file->getPathname(), $this->targetDir.$fileName);

        return $fileDescriptor;
    }

    public function upload(UploadedFile $file, $extension = null): FileDescriptor
    {
        $fileName = sha1(uniqid(mt_rand(), true)).'.'.($extension !== null ? $extension : $file->guessExtension());
        $file->move($this->targetDir, $fileName);

        $fileDescriptor = new FileDescriptor();
        $fileDescriptor->setName($file->getClientOriginalName());
        $fileDescriptor->setPath($fileName);
        $fileDescriptor->setSize($file->getClientSize());
        $fileDescriptor->setMimeType($file->getClientMimeType());
        $fileDescriptor->setDirectory($this->targetDir);

        return $fileDescriptor;
    }

    public function uploadEditorFile(UploadedFile $file, $extension = null): FileDescriptor
    {
        $fileExtension = $extension !== null ? $extension : $file->guessExtension();
        $fileName = sha1(uniqid(mt_rand(), true)).'.'.$fileExtension;
        $file->move($this->targetDir, $fileName);

        if (!file_exists($this->targetDir.'/_thumbs')) {
            mkdir($this->targetDir.'/_thumbs');
        }

        $fileDescriptor = new FileDescriptor();
        $fileDescriptor->setName($file->getClientOriginalName());
        $fileDescriptor->setPath($fileName);
        $fileDescriptor->setSize($file->getClientSize());
        $fileDescriptor->setMimeType($fileExtension);
        $fileDescriptor->setDirectory($this->targetDir);

        $thumbnailPath = null;
        switch ($fileExtension) {
            case 'ppt':
            case 'pptx':
                $thumbnailPath = $this->targetDir.'/_thumbs/'.$fileName.'.png';
                @copy($this->rootPath.'/../web/img/icons/ppt.png', $thumbnailPath);
                break;
            case 'pdf':
                $thumbnailPath = $this->targetDir.'/_thumbs/'.$fileName.'.png';
                @copy($this->rootPath.'/../web/img/icons/pdf.png', $thumbnailPath);
                break;
            case 'xlsx':
                $thumbnailPath = $this->targetDir.'/_thumbs/'.$fileName.'.png';
                @copy($this->rootPath.'/../web/img/icons/xlsx.png', $thumbnailPath);
                break;
            case 'doc':
            case 'docx':
                $thumbnailPath = $this->targetDir.'/_thumbs/'.$fileName.'.png';
                @copy($this->rootPath.'/../web/img/icons/docx.png', $thumbnailPath);
                break;
            case 'txt':
            case 'rtf':
                $thumbnailPath = $this->targetDir.'/_thumbs/'.$fileName.'.png';
                @copy($this->rootPath.'/../web/img/icons/text.png', $thumbnailPath);
                break;
        }

        if ($thumbnailPath) {
            $fileDescriptor->setThumbnail($thumbnailPath);
        }

        return $fileDescriptor;
    }

    public function delete($filePath)
    {
        if (file_exists($this->targetDir.$filePath)) {
            unlink($this->targetDir.$filePath);
        }
    }
}
