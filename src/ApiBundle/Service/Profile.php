<?php

namespace ApiBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class Profile
 * @package ApiBundle\Service
 */
class Profile
{
    /** @var EntityManager */
    private $entityManager;
    private $security;

    public function __construct(EntityManager $entityManager, TokenStorage $security)
    {
        $this->entityManager = $entityManager;
        $this->security      = $security;
    }

    /**
     * @return array
     */
    public function getProfile()
    {
        $person = $this->security->getToken()->getUser();

        $profile = $this->entityManager->getRepository(\ApiBundle\Entity\Profile::class)
            ->findOneBy([
                'person' => $person,
            ]);
        $profile = $profile ?? new \ApiBundle\Entity\Profile();

        return array(
            'id' => $profile->getId(),
            'firstName' => $profile->getFirstName(),
            'lastName'  => $profile->getLastName(),
            'avatar'  => $profile->getAvatar(),
            'person'  => $profile->getPerson(),
            'entity'  => $profile->getEntity(),
        );
    }
}
