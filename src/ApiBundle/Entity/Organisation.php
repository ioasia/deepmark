<?php

namespace ApiBundle\Entity;

/**
 * Organisation.
 */
class Organisation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string|null
     */
    private $expansionStreet;

    /**
     * @var string|null
     */
    private $logo;

    /**
     * @var string
     */
    private $workingHours;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Organisation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Organisation
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set expansionStreet.
     *
     * @param string|null $expansionStreet
     *
     * @return Organisation
     */
    public function setExpansionStreet($expansionStreet = null)
    {
        $this->expansionStreet = $expansionStreet;

        return $this;
    }

    /**
     * Get expansionStreet.
     *
     * @return string|null
     */
    public function getExpansionStreet()
    {
        return $this->expansionStreet;
    }

    /**
     * @return string|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string|null $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Organisation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Organisation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var string
     */
    private $country;

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Organisation
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return Organisation
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return Organisation
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set workingHours.
     *
     * @param string $workingHours
     *
     * @return Organisation
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = serialize($workingHours);

        return $this;
    }

    /**
     * Get workingHours.
     *
     * @return array
     */
    public function getWorkingHours()
    {
        return unserialize($this->workingHours);
    }
}
