<?php

namespace ApiBundle\Entity;

/**
 * BookingAgenda.
 */
class BookingAgenda
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $bookingDate;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\Room
     */
    private $room;

    /**
     * @var int
     */
    private $learnerGrade;

    /**
     * @var int
     */
    private $alertSent;

    /**
     * @var int
     */
    private $reminderSent;

    /**
     * @var \DateTime
     */
    private $attendanceDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bookingSignatures;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleReports;

    /**
     * @var string
     */
    private $signatureParams;

    /**
     * @var int
     */
    private $joinStatus;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookingDate.
     *
     * @param \DateTime $bookingDate
     *
     * @return BookingAgenda
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    /**
     * Get bookingDate.
     *
     * @return \DateTime
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return BookingAgenda
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return BookingAgenda
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return BookingAgenda
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set trainer.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return BookingAgenda
     */
    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set room.
     *
     * @param \ApiBundle\Entity\Room|null $room
     *
     * @return BookingAgenda
     */
    public function setRoom(\ApiBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room.
     *
     * @return \ApiBundle\Entity\Room|null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @var string|null
     */
    private $webinarKey;

    /**
     * @var string|null
     */
    private $joinUrl;

    /**
     * Set webinarKey.
     *
     * @param string|null $webinarKey
     *
     * @return BookingAgenda
     */
    public function setWebinarKey($webinarKey = null)
    {
        $this->webinarKey = $webinarKey;

        return $this;
    }

    /**
     * Get webinarKey.
     *
     * @return string|null
     */
    public function getWebinarKey()
    {
        return $this->webinarKey;
    }

    /**
     * Set joinUrl.
     *
     * @param string|null $joinUrl
     *
     * @return BookingAgenda
     */
    public function setJoinUrl($joinUrl = null)
    {
        $this->joinUrl = $joinUrl;

        return $this;
    }

    /**
     * Get joinUrl.
     *
     * @return string|null
     */
    public function getJoinUrl()
    {
        return $this->joinUrl;
    }

    /**
     * @var \ApiBundle\Entity\Visio
     */
    private $visio;

    /**
     * Set visio.
     *
     * @param \ApiBundle\Entity\Visio|null $visio
     *
     * @return BookingAgenda
     */
    public function setVisio(\ApiBundle\Entity\Visio $visio = null)
    {
        $this->visio = $visio;

        return $this;
    }

    /**
     * Get visio.
     *
     * @return \ApiBundle\Entity\Visio|null
     */
    public function getVisio()
    {
        return $this->visio;
    }

    /**
     * @var \ApiBundle\Entity\ModuleSession
     */
    private $moduleSession;

    /**
     * Set moduleSession.
     *
     * @param \ApiBundle\Entity\ModuleSession|null $moduleSession
     *
     * @return BookingAgenda
     */
    public function setModuleSession(\ApiBundle\Entity\ModuleSession $moduleSession = null)
    {
        $this->moduleSession = $moduleSession;

        return $this;
    }

    /**
     * Get moduleSession.
     *
     * @return \ApiBundle\Entity\ModuleSession|null
     */
    public function getModuleSession()
    {
        return $this->moduleSession;
    }

    /**
     * @return int
     */
    public function getLearnerGrade()
    {
        return $this->learnerGrade;
    }

    /**
     * @param int $learnerGrade
     */
    public function setLearnerGrade(int $learnerGrade): void
    {
        $this->learnerGrade = $learnerGrade;
    }

    /**
     * @return int
     */
    public function getAlertSent()
    {
        return $this->alertSent;
    }

    /**
     * @param int $alertSent
     */
    public function setAlertSent(int $alertSent): void
    {
        $this->alertSent = $alertSent;
    }

    /**
     * @return int
     */
    public function getReminderSent()
    {
        return $this->reminderSent;
    }

    /**
     * @param int $reminderSent
     */
    public function setReminderSent(int $reminderSent): void
    {
        $this->reminderSent = $reminderSent;
    }

    /**
     * @return \DateTime
     */
    public function getAttendanceDate()
    {
        return $this->attendanceDate;
    }

    /**
     * @param \DateTime $attendanceDate
     */
    public function setAttendanceDate($attendanceDate)
    {
        $this->attendanceDate = $attendanceDate;
    }

    /**
     * Add bookingSignature.
     *
     * @param \ApiBundle\Entity\BookingSignature $bookingSignature
     *
     * @return BookingAgenda
     */
    public function addBookingSignature(\ApiBundle\Entity\BookingSignature $bookingSignature)
    {
        $this->bookingSignature[] = $bookingSignature;

        return $this;
    }

    /**
     * Remove bookingSignature.
     *
     * @param \ApiBundle\Entity\BookingSignature $bookingSignature
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBookingSignature(\ApiBundle\Entity\BookingSignature $bookingSignature)
    {
        return $this->bookingSignatures->removeElement($bookingSignature);
    }

    /**
     * Get bookingSignatures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingSignatures()
    {
        return $this->bookingSignatures;
    }

    /**
     * Add moduleReport.
     *
     * @param \ApiBundle\Entity\ModuleReports $moduleReport
     *
     * @return BookingAgenda
     */
    public function addModuleReport(\ApiBundle\Entity\ModuleReports $moduleReport)
    {
        $this->moduleReports[] = $moduleReport;

        return $this;
    }

    /**
     * Remove moduleReport.
     *
     * @param \ApiBundle\Entity\ModuleReports $moduleReport
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleReport(\ApiBundle\Entity\ModuleReports $moduleReport)
    {
        return $this->moduleReports->removeElement($moduleReport);
    }

    /**
     * Get moduleReports.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleReports()
    {
        return $this->moduleReports;
    }

    /**
     * Set signatureParams.
     *
     * @param array $signatureParams
     *
     * @return BookingAgenda
     */
    public function setSignatureParams($signatureParams)
    {
        $this->signatureParams = serialize($signatureParams);

        return $this;
    }

    /**
     * Get signatureParams.
     *
     * @return array
     */
    public function getSignatureParams()
    {
        return unserialize($this->signatureParams);
    }

    /**
     * Get joinStatus.
     *
     * @return int
     */
    public function getJoinStatus()
    {
        return $this->joinStatus;
    }

    /**
     * @param int $joinStatus
     * @return BookingAgenda
     */
    public function setJoinStatus($joinStatus)
    {
        $this->joinStatus = $joinStatus;

        return $this;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BookingAgenda
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BookingAgenda
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
