<?php

namespace ApiBundle\Entity;

/**
 * SkillResourceVarietyProfile.
 */
class SkillResourceVarietyProfile
{
    const STATUS_UNKNOWN = 'UNKNOWN';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_DENY = 'DENY';
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $score;

    /**
     * @var float
     */
    private $isWorking;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var \DateTime|null
     */
    private $validationDate;

    /**
     * @var string
     */
    private $status = 'UNKNOWN';

    /**
     * @var \ApiBundle\Entity\LiveResourceVariety
     */
    private $liveResourceVariety;

    /**
     * @var \ApiBundle\Entity\Skill
     */
    private $skill;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score.
     *
     * @param float $score
     *
     * @return SkillResourceVarietyProfile
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score.
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set isWorking.
     *
     * @param int $isWorking
     *
     * @return SkillResourceVarietyProfile
     */
    public function setIsWorking($isWorking)
    {
        $this->isWorking = $isWorking;

        return $this;
    }

    /**
     * Get score.
     *
     * @return float
     */
    public function getIsWorking()
    {
        return $this->isWorking;
    }

    /**
     * Set dateAdd.
     *
     * @param \DateTime $dateAdd
     *
     * @return SkillResourceVarietyProfile
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd.
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set validationDate.
     *
     * @param \DateTime|null $validationDate
     *
     * @return SkillResourceVarietyProfile
     */
    public function setValidationDate($validationDate = null)
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * Get validationDate.
     *
     * @return \DateTime|null
     */
    public function getValidationDate()
    {
        return $this->validationDate;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return SkillResourceVarietyProfile
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set liveResourceVariety.
     *
     * @param \ApiBundle\Entity\LiveResourceVariety|null $liveResourceVariety
     *
     * @return SkillResourceVarietyProfile
     */
    public function setLiveResourceVariety(\ApiBundle\Entity\LiveResourceVariety $liveResourceVariety = null)
    {
        $this->liveResourceVariety = $liveResourceVariety;

        return $this;
    }

    /**
     * Get liveResourceVariety.
     *
     * @return \ApiBundle\Entity\LiveResourceVariety|null
     */
    public function getLiveResourceVariety()
    {
        return $this->liveResourceVariety;
    }

    /**
     * Set skill.
     *
     * @param \ApiBundle\Entity\Skill|null $skill
     *
     * @return SkillResourceVarietyProfile
     */
    public function setSkill(\ApiBundle\Entity\Skill $skill = null)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill.
     *
     * @return \ApiBundle\Entity\Skill|null
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return SkillResourceVarietyProfile
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return SkillResourceVarietyProfile
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return SkillResourceVarietyProfile
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
