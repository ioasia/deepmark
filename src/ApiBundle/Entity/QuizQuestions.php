<?php

namespace ApiBundle\Entity;
/**
 * QuizQuestions.
 */
class QuizQuestions {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var text
     */
    private $question;

    /**
     * @var int
     */
    private $score_to_pass;

    /**
     * @var int
     */
    private $limit_attempt;

    /**
     * @var int
     */
    private $limit_users;

    /**
     * @var string
     */
    private $type;
    
    /**
     * @var int
     */
    private $status;
    
    /**
     * @var int
     */
    private $is_survey;

    /**
     * @var int
     */
    private $isLearnMore;

    /**
     * @var text
     */
    private $params;

    /**
     * @var \ApiBundle\Entity\QuizQuestionsXref
     */
    private $question_xref;
    

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    public $items;
    
    /**
     * @var \ApiBundle\Entity\QuizUsersAnswers
     */
    private $answers;

     /**
     * Construct
     */
    public function __construct() {
        // Init
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Module
     */
    public function setTitle($title = null) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get question.
     *
     * @return string
     */
    public function getQuestion() {
        return $this->question;
    }

    /**
     * Set question.
     *
     * @param string|null $question
     *
     * @return Module
     */
    public function setQuestion($question = null) {
        $this->question = $question;

        return $this;
    }

    /**
     * Get score_to_pass.
     *
     * @return string
     */
    public function getScore_to_pass() {
        return $this->score_to_pass;
    }

    /**
     * Set score_to_pass.
     *
     * @param string|null $score_to_pass
     *
     * @return Module
     */
    public function setScore_to_pass($score_to_pass = null) {
        $this->score_to_pass = $score_to_pass;

        return $this;
    }

    /**
     * Get limit_attempt.
     *
     * @return string
     */
    public function getLimit_attempt() {
        return $this->limit_attempt;
    }

    /**
     * Set limit_attemt.
     *
     * @param string|null $limit_attempt
     *
     * @return Module
     */
    public function setLimit_attempt($limit_attempt = null) {
        $this->limit_attempt = $limit_attempt;

        return $this;
    }

    /**
     * Get limit_users.
     *
     * @return string
     */
    public function getLimit_users() {
        return $this->limit_users;
    }

    /**
     * Set limit_users.
     *
     * @param string|null $limit_users
     *
     * @return Module
     */
    public function setLimit_users($limit_users = null) {
        $this->limit_users = $limit_users;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return Module
     */
    public function setType($type = null) {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Module
     */
    public function setStatus($status = null) {
        $this->status = $status;

        return $this;
    }
    
    /**
     * Get is_survey.
     *
     * @return int
     */
    public function getIsSurvey() {
        return $this->is_survey;
    }

    /**
     * Set is_survey.
     *
     * @param int|null $is_survey
     *
     * @return Module
     */
    public function setIsSurvey($is_survey = null) {
        $this->is_survey = $is_survey;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Set params.
     *
     * @param string|null $params
     *
     * @return Module
     */
    public function setParams($params = null) {
        $this->params = $params;

        return $this;
    }    
    
    /**
     * Set item.
     *
     * @param \ApiBundle\Entity\QuizQuestionsItems|null $item
     *
     * @return QuizQuestions
     */
    public function setItem(\ApiBundle\Entity\QuizQuestionsItems $item = null) {
        $item->setQuestion_id($this->id);
        $this->items[] = $item;

        return $this;
    }
    
    /**
     * Set items.
     *
     * @return \Doctrine\Common\Collections\Collection
     *
     * @return QuizQuestions
     */
    public function setItems(\Doctrine\Common\Collections\Collection $items = null) {
        $this->items = $items;

        return $this;
    }
    
    /**
     * Get items
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * Get question_xref
     * @return \ApiBundle\Entity\QuizQuestionsXref
     */
    public function getQuestionXref() {
        return $this->question_xref;
    }

    /**
     * @return int
     */
    public function getIsLearnMore()
    {
        return $this->isLearnMore;
    }

    /**
     * @param null $isLearnMore
     * @return $this
     */

    public function setIsLearnMore($isLearnMore = null)
    {
        $this->isLearnMore = $isLearnMore;
        return $this;
    }
}
