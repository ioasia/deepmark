<?php

namespace ApiBundle\Entity;

/**
 * TrainerReservation.
 */
class TrainerReservation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $module_unique;

    /**
     * @var \DateTime
     */
    private $reservation_ends;

    /**
     * @var \DateTime
     */
    private $from_datetime;

    /**
     * @var \DateTime
     */
    private $to_datetime;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moduleUnique.
     *
     * @param string $moduleUnique
     *
     * @return TrainerReservation
     */
    public function setModuleUnique($moduleUnique)
    {
        $this->module_unique = $moduleUnique;

        return $this;
    }

    /**
     * Get moduleUnique.
     *
     * @return string
     */
    public function getModuleUnique()
    {
        return $this->module_unique;
    }

    /**
     * Set reservationEnds.
     *
     * @param \DateTime $reservationEnds
     *
     * @return TrainerReservation
     */
    public function setReservationEnds($reservationEnds)
    {
        $this->reservation_ends = $reservationEnds;

        return $this;
    }

    /**
     * Get reservationEnds.
     *
     * @return \DateTime
     */
    public function getReservationEnds()
    {
        return $this->reservation_ends;
    }

    /**
     * Set fromDatetime.
     *
     * @param \DateTime $fromDatetime
     *
     * @return TrainerReservation
     */
    public function setFromDatetime($fromDatetime)
    {
        $this->from_datetime = $fromDatetime;

        return $this;
    }

    /**
     * Get fromDatetime.
     *
     * @return \DateTime
     */
    public function getFromDatetime()
    {
        return $this->from_datetime;
    }

    /**
     * Set toDatetime.
     *
     * @param \DateTime $toDatetime
     *
     * @return TrainerReservation
     */
    public function setToDatetime($toDatetime)
    {
        $this->to_datetime = $toDatetime;

        return $this;
    }

    /**
     * Get toDatetime.
     *
     * @return \DateTime
     */
    public function getToDatetime()
    {
        return $this->to_datetime;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return TrainerReservation
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @var string|null
     */
    private $session_unique;

    /**
     * Set sessionUnique.
     *
     * @param string|null $sessionUnique
     *
     * @return TrainerReservation
     */
    public function setSessionUnique($sessionUnique = null)
    {
        $this->session_unique = $sessionUnique;

        return $this;
    }

    /**
     * Get sessionUnique.
     *
     * @return string|null
     */
    public function getSessionUnique()
    {
        return $this->session_unique;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return TrainerReservation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return TrainerReservation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
