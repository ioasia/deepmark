<?php

namespace ApiBundle\Entity;

/**
 * ProfileVariety.
 */
class ProfileVariety
{
    public const SUPER_ADMIN = 0;
    public const ADMIN = 1;
    public const TRAINER = 2;
    public const LEARNER = 3;
    public const CUSTOMER = 4;
    public const SUPERVISOR = 5;
    public const MANAGER = 6;
    public const CLIENT = 7;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var int|null
     */
    private $appId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ProfileVariety
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set appId.
     *
     * @param int|null $appId
     *
     * @return ProfileVariety
     */
    public function setAppId($appId = null)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId.
     *
     * @return int|null
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ProfileVariety
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ProfileVariety
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
