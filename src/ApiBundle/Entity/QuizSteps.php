<?php

namespace ApiBundle\Entity;

/**
 * QuizSteps.
 */
class QuizSteps
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;
    
    /**
     * @var text
     */
    private $description;
    
    /**
     * @var int
     */
    private $scores;
    
    /**
     * @var int
     */
    private $score_to_pass;
    
    /**
     * @var int
     */
    private $limit_attempt;
    
    /**
     * @var int
     */
    private $limit_users;
    
    /**
     * @var int
     */
    private $ordering;
    
    /**
     * @var int
     */
    private $status;
    
    /**
     * @var text
     */
    private $params;

    /**
     * @var \ApiBundle\Entity\QuizSteps
     */
    private $step_xref;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Module
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }
    
    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Module
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }
    
    /**
     * Get scores.
     *
     * @return string
     */
    public function getScores()
    {
        return $this->scores;
    }
    
    /**
     * Set scores.
     *
     * @param string|null $score_to_pass
     *
     * @return Module
     */
    public function setScores($scores = null)
    {
        $this->scores = $scores;

        return $this;
    }
    
    /**
     * Get score_to_pass.
     *
     * @return string
     */
    public function getScore_to_pass()
    {
        return $this->score_to_pass;
    }
    
    /**
     * Set score_to_pass.
     *
     * @param string|null $score_to_pass
     *
     * @return Module
     */
    public function setScore_to_pass($score_to_pass = null)
    {
        $this->score_to_pass = $score_to_pass;

        return $this;
    }
    
    /**
     * Get limit_attempt.
     *
     * @return string
     */
    public function getLimit_attempt()
    {
        return $this->limit_attempt;
    }
    
    /**
     * Set limit_attemt.
     *
     * @param string|null $limit_attempt
     *
     * @return Module
     */
    public function setLimit_attempt($limit_attempt = null)
    {
        $this->limit_attempt = $limit_attempt;

        return $this;
    }
    
    /**
     * Get limit_users.
     *
     * @return string
     */
    public function getLimit_users() {
        return $this->limit_users;
    }

    /**
     * Set limit_users.
     *
     * @param string|null $limit_users
     *
     * @return Module
     */
    public function setLimit_users($limit_users = null) {
        $this->limit_users = $limit_users;

        return $this;
    }
    
    /**
     * Get ordering.
     *
     * @return string
     */
    public function getOrdering()
    {
        return $this->ordering;
    }
    
    /**
     * Set limit_attemt.
     *
     * @param string|null $ordering
     *
     * @return Module
     */
    public function setOrdering($ordering = null)
    {
        $this->ordering = $ordering;

        return $this;
    }
    
    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Module
     */
    public function setStatus($status = null) {
        $this->status = $status;

        return $this;
    }
    
    /**
     * Get params.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }
    
    /**
     * Set limit_attemt.
     *
     * @param string|null $params
     *
     * @return Module
     */
    public function setParams($params = null)
    {
        $this->params = $params;

        return $this;
    }
}
