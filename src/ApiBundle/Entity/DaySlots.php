<?php

namespace ApiBundle\Entity;

use DateTime;

/**
 * DaySlots.
 */
class DaySlots
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dayId;

    /**
     * @var DateTime
     */
    private $startMorningTime;

    /**
     * @var DateTime
     */
    private $endMorningTime;

    /**
     * @var DateTime
     */
    private $startAfternoonTime;

    /**
     * @var DateTime
     */
    private $endAfternoonTime;

    /**
     * @var \ApiBundle\Entity\Intervention
     */
    private $intervention;

    /**
     * @var DateTime
     */
    private $created;

    /**
     * @var DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDayId()
    {
        return $this->dayId;
    }

    /**
     * @param string $dayId
     */
    public function setDayId(string $dayId)
    {
        $this->dayId = $dayId;
    }

    /**
     * @return DateTime
     */
    public function getStartMorningTime()
    {
        return $this->startMorningTime;
    }

    /**
     * Set startMorningTime.
     *
     * @param DateTime $startMorningTime
     *
     * @return DaySlots
     */
    public function setStartMorningTime($startMorningTime)
    {
        $this->startMorningTime = $startMorningTime;

        return $this;
    }


    /**
     * @return DateTime
     */
    public function getEndMorningTime()
    {
        return $this->endMorningTime;
    }

    /**
     * Set endMorningTime.
     *
     * @param DateTime $endMorningTime
     *
     * @return DaySlots
     */
    public function setEndMorningTime($endMorningTime)
    {
        $this->endMorningTime = $endMorningTime;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartAfternoonTime()
    {
        return $this->startAfternoonTime;
    }

    /**
     * Set startAfternoonTime.
     *
     * @param DateTime $startAfternoonTime
     *
     * @return DaySlots
     */
    public function setStartAfternoonTime($startAfternoonTime)
    {
        $this->startAfternoonTime = $startAfternoonTime;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndAfternoonTime()
    {
        return $this->endAfternoonTime;
    }

    /**
     * Set endAfternoonTime.
     *
     * @param DateTime $endAfternoonTime
     *
     * @return DaySlots
     */
    public function setEndAfternoonTime($endAfternoonTime)
    {
        $this->endAfternoonTime = $endAfternoonTime;

        return $this;
    }

    /**
     * Set intervention.
     *
     * @param \ApiBundle\Entity\Intervention|null $intervention
     *
     * @return DaySlots
     */
    public function setIntervention(\ApiBundle\Entity\Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get intervention.
     *
     * @return \ApiBundle\Entity\Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Set created.
     *
     * @param DateTime $created
     *
     * @return AvailableCalendar
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param DateTime $updated
     *
     * @return AvailableCalendar
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
