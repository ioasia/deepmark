<?php

namespace ApiBundle\Entity;


/**
 * Chapter.
 */
class Chapter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $chapterName;

    /**
     * @var int
     */
    private $position;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chapterModules;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->chapterModules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param ChapterModule $chapterModule
     * @return $this
     */
    public function addChapterModule(\ApiBundle\Entity\ChapterModule $chapterModule)
    {
        $this->chapterModules[] = $chapterModule;

        return $this;
    }

    /**
     * @param ChapterModule $chapterModule
     * @return bool
     */
    public function removeChapterModule(\ApiBundle\Entity\ChapterModule $chapterModule)
    {
        return $this->chapterModules->removeElement($chapterModule);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getChapterModules()
    {
        return $this->chapterModules;
    }

    /**
     * @param $chapterName
     * @return $this
     */
    public function setChapterName($chapterName)
    {
        $this->chapterName = $chapterName;

        return $this;
    }

    /**
     * @return string
     */
    public function getChapterName()
    {
        return $this->chapterName;
    }

    /**
     * @param $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function __toString()
    {
        return $this->chapterName;
    }


    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Skill
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

}
