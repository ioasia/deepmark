<?php

namespace ApiBundle\Entity;

/**
 * LearnerIntervention.
 */
class LearnerIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $progression = '0';

    /**
     * @var Intervention
     */
    private $intervention;

    /**
     * @var Profile
     */
    private $learner;

    /**
     * @var Status
     */
    private $status;

    private $interventionNotation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getInterventionNotation()
    {
        return $this->interventionNotation;
    }

    /**
     * @param mixed $interventionNotation
     */
    public function setInterventionNotation($interventionNotation): void
    {
        $this->interventionNotation = $interventionNotation;
    }

    /**
     * Set progression.
     *
     * @param int|null $progression
     *
     * @return LearnerIntervention
     */
    public function setProgression($progression = null)
    {
        $this->progression = $progression;

        return $this;
    }

    /**
     * Get progression.
     *
     * @return int|null
     */
    public function getProgression()
    {
        return $this->progression;
    }

    /**
     * Set intervention.
     *
     * @param Intervention|null $intervention
     *
     * @return LearnerIntervention
     */
    public function setIntervention(Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get intervention.
     *
     * @return Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Set learner.
     *
     * @param Profile|null $learner
     *
     * @return LearnerIntervention
     */
    public function setLearner(Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set status.
     *
     * @param Status|null $status
     *
     * @return LearnerIntervention
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return LearnerIntervention
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return LearnerIntervention
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return LearnerIntervention
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
