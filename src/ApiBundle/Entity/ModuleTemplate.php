<?php

namespace ApiBundle\Entity;

/**
 * ModuleNotes.
 */
class ModuleTemplate
{
    public const INDIVIDUAL_REPORT_TEMPLATE = 1;
    public const ATTENDANCE_REPORT_TEMPLATE = 2;
    public const CERTIFICATE_TEMPLATE = 3;
    /**
     * @var int
     */
    private $id;
    private $module;
    private $fileDescriptor;
    private $appId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param mixed $module
     *
     * @return ModuleTemplate
     */
    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param mixed $appId
     *
     * @return ModuleTemplate
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
        return $this;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return ModuleTemplate
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

}
