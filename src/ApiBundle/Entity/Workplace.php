<?php

namespace ApiBundle\Entity;

/**
 * Workplace.
 */
class Workplace
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \ApiBundle\Entity\GroupOrganization
     */
    private $groupOrganization;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Workplace
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set groupOrganization.
     *
     * @param \ApiBundle\Entity\GroupOrganization|null $groupOrganization
     *
     * @return Workplace
     */
    public function setGroupOrganization(\ApiBundle\Entity\GroupOrganization $groupOrganization = null)
    {
        $this->groupOrganization = $groupOrganization;

        return $this;
    }

    /**
     * Get groupOrganization.
     *
     * @return \ApiBundle\Entity\GroupOrganization|null
     */
    public function getGroupOrganization()
    {
        return $this->groupOrganization;
    }

    public function __toString()
    {
        $name = '';
        if ($this->getGroupOrganization()) {
            $name .= $this->getGroupOrganization()->getDesignation().' ';
        }
        if ($this->getDesignation()) {
            $name .= $this->getDesignation();
        }

        return $name;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Workplace
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Workplace
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
