<?php

namespace ApiBundle\Entity;

/**
 * EchoModule.
 */
class EchoModule
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $echo_id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEchoId()
    {
        return $this->echo_id;
    }

    /**
     * Set echo_id.
     *
     * @param int $echo_id
     *
     * @return EchoModule
     */
    public function setEchoId(int $echo_id)
    {
        $this->echo_id = $echo_id;

        return $this;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return EchoModule
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return EchoModule
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return EchoModule
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return EchoModule
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
