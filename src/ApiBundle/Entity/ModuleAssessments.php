<?php

namespace ApiBundle\Entity;

/**
 * ModuleAssessments.
 */
class ModuleAssessments
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $assessment;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var int|null
     */
    private $completed;

    /**
     * @var string|null
     */
    private $operationOne;

    /**
     * @var int|null
     */
    private $scoreToPass;

    /**
     * @var string|null
     */
    private $operationTwo;

    /**
     * @var int|null
     */
    private $timeSpent;

    /**
     * @var int|null
     */
    private $timeSpentCheck;

    /**
     * @var int|null
     */
    private $weight;

    /**
     * @var string|null
     */
    private $certificationTemplate;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Module
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * @param \ApiBundle\Entity\Module $assessment
     * @return ModuleAssessments
     */
    public function setAssessment(\ApiBundle\Entity\Module $assessment)
    {
        $this->assessment = $assessment;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param int|null $completed
     * @return ModuleAssessments
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimeSpentCheck()
    {
        return $this->timeSpentCheck;
    }

    /**
     * @param int|null $timeSpentCheck
     * @return ModuleAssessments
     */
    public function setTimeSpentCheck($timeSpentCheck)
    {
        $this->timeSpentCheck = $timeSpentCheck;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return ModuleAssessments
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCertificationTemplate()
    {
        return $this->certificationTemplate;
    }

    /**
     * @param string|null $certificationTemplate
     * @return ModuleAssessments
     */
    public function setCertificationTemplate($certificationTemplate)
    {
        $this->certificationTemplate = $certificationTemplate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperationOne()
    {
        return $this->operationOne;
    }

    /**
     * @param string|null $operationOne
     * @return ModuleAssessments
     */
    public function setOperationOne($operationOne)
    {
        $this->operationOne = $operationOne;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getScoreToPass()
    {
        return $this->scoreToPass;
    }

    /**
     * @param int|null $scoreToPass
     * @return ModuleAssessments
     */
    public function setScoreToPass($scoreToPass)
    {
        $this->scoreToPass = $scoreToPass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperationTwo()
    {
        return $this->operationTwo;
    }

    /**
     * @param string|null $operationTwo
     */
    public function setOperationTwo($operationTwo)
    {
        $this->operationTwo = $operationTwo;
    }

    /**
     * @return int|null
     */
    public function getTimeSpent()
    {
        return $this->timeSpent;
    }

    /**
     * @param int|null $timeSpent
     * @return ModuleAssessments
     */
    public function setTimeSpent($timeSpent)
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleNotes
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Session
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Session
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
