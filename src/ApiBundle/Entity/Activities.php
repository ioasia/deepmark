<?php

namespace ApiBundle\Entity;

/**
 * Activities.
 */
class Activities
{
    // GENERAL
    public const LATEST_LEARNED_MODULE = 'LATEST_LEARNED_MODULE';
    public const NUMBER_OF_LOGIN = 'NUMBER_OF_LOGIN';

    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $activityName;

    /**
     * @var string
     */
    private $activityValue;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return Activities
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return string
     */
    public function getActivityName()
    {
        return $this->activityName;
    }

    /**
     * @param string $activityName
     *
     * @return Activities
     */
    public function setActivityName($activityName)
    {
        $this->activityName = $activityName;

        return $this;
    }

    /**
     * @return string
     */
    public function getActivityValue()
    {
        return $this->activityValue;
    }

    /**
     * @param string $activityValue
     *
     * @return Activities
     */
    public function setActivityValue($activityValue)
    {
        $this->activityValue = $activityValue;

        return $this;
    }
}
