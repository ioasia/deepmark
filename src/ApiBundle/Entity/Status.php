<?php

namespace ApiBundle\Entity;

/**
 * Status.
 */
class Status
{
    // GENERAL
    public const GENERAL_UNKNOWN = 0;

    // INTERVENTION
    public const INTERVENTION_CREATED = 61;
    public const INTERVENTION_IN_PROGRESS = 62;
    public const INTERVENTION_FINISHED = 63;

    // INTERVENTION LEARNER
    public const LEARNER_INTERVENTION_CREATED = 71;
    public const LEARNER_INTERVENTION_IN_PROGRESS = 72;
    public const LEARNER_INTERVENTION_FINISHED = 73;
    public const LEARNER_INTERVENTION_EXPIRED = 74;

    // WITH_BOOKING AssigmentResourceSystem
    public const WITH_BOOKING_SCHEDULED = 21;
    public const WITH_BOOKING_BOOKED = 22;
    public const WITH_BOOKING_IN_PROGRESS = 23;
    public const WITH_BOOKING_DONE = 24;
    public const WITH_BOOKING_REPORTED = 25;
    public const WITH_BOOKING_CANCELED = 26;
    public const WITH_BOOKING_LEARNER_ABSENT = 27;
    public const WITH_BOOKING_TRAINER_ABSENT = 28;
    public const WITH_BOOKING_TECHNICAL_PROBLEM = 29;
    public const WITH_BOOKING_CONFIRMED = 30;

    // WITHOUT_BOOKING
    public const WITHOUT_BOOKING_UNDONE = 3;
    public const WITHOUT_BOOKING_CORRECTED = 32;
    public const WITHOUT_BOOKING_VALIDATED = 33;
    public const WITHOUT_BOOKING_DONE = 34;

    // PROFILE
    public const PROFILE_WAITING_APPROVAL = 41;
    public const PROFILE_VALIDATED = 42;
    public const PROFILE_INVALID = 43;
    public const PROFILE_UNAUTHORIZED = 44;

    // REPORT
    public const REPORT_UNDONE = 51;
    public const REPORT_DONE = 52;
    public const REPORT_VALIDATED = 53;

    // Trainer status
    public const SESSION_NOT_START = 1;
    public const SESSION_IN_PROGRESS = 2;
    public const SESSION_DONE= 3;

    // GENERAL
    const BOOKED = 10;
    const IN_PROGRESS = 6;
    const CREATED = 6;
    const DONE = 12;
    const CANCELED = 14;
    const EXPIRED = 8;
    const LEARNER_ABSENT = 15;
    const CONFIRMED = 29;
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;
    /**
     * @var int
     */
    private $appId;

    /**
     * @var \ApiBundle\Entity\StatusFamily
     */
    private $statusFamily;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Status
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set appId.
     *
     * @param int $appId
     *
     * @return Status
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId.
     *
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set statusFamily.
     *
     * @param \ApiBundle\Entity\StatusFamily|null $statusFamily
     *
     * @return Status
     */
    public function setStatusFamily(\ApiBundle\Entity\StatusFamily $statusFamily = null)
    {
        $this->statusFamily = $statusFamily;

        return $this;
    }

    /**
     * Get statusFamily.
     *
     * @return \ApiBundle\Entity\StatusFamily|null
     */
    public function getStatusFamily()
    {
        return $this->statusFamily;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Status
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Status
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
