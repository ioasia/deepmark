<?php

namespace ApiBundle\Entity;

/**
 * EducationalDocument.
 */
class EducationalDocument
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $fileDescriptor;

    /**
     * @var \ApiBundle\Entity\EducationalDocVariety
     */
    private $educationalDocVariety;
    /**
     * @var int
     */
    private $isDownload;

    /**
     * @return int
     */
    public function getIsDownload()
    {
        return $this->isDownload;
    }

    /**
     * @param int $isDownload
     */
    public function setIsDownload(int $isDownload)
    {
        $this->isDownload = $isDownload;
        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return EducationalDocument
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return EducationalDocument
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return EducationalDocument
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return EducationalDocument
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

    /**
     * Set educationalDocVariety.
     *
     * @param \ApiBundle\Entity\EducationalDocVariety|null $educationalDocVariety
     *
     * @return EducationalDocument
     */
    public function setEducationalDocVariety(\ApiBundle\Entity\EducationalDocVariety $educationalDocVariety = null)
    {
        $this->educationalDocVariety = $educationalDocVariety;

        return $this;
    }

    /**
     * Get educationalDocVariety.
     *
     * @return \ApiBundle\Entity\EducationalDocVariety|null
     */
    public function getEducationalDocVariety()
    {
        return $this->educationalDocVariety;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $modules;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->modules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add module.
     *
     * @param \ApiBundle\Entity\Module $module
     *
     * @return EducationalDocument
     */
    public function addModule(\ApiBundle\Entity\Module $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Remove module.
     *
     * @param \ApiBundle\Entity\Module $module
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModule(\ApiBundle\Entity\Module $module)
    {
        return $this->modules->removeElement($module);
    }

    /**
     * Get modules.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @var string|null
     */
    private $url;

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return EducationalDocument
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return EducationalDocument
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return EducationalDocument
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $trainerModules;

    /**
     * Add trainerModule.
     *
     * @param \ApiBundle\Entity\Module $trainerModule
     *
     * @return EducationalDocument
     */
    public function addTrainerModule(\ApiBundle\Entity\Module $trainerModule)
    {
        $this->trainerModules[] = $trainerModule;

        return $this;
    }

    /**
     * Remove trainerModule.
     *
     * @param \ApiBundle\Entity\Module $trainerModule
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTrainerModule(\ApiBundle\Entity\Module $trainerModule)
    {
        return $this->trainerModules->removeElement($trainerModule);
    }

    /**
     * Get trainerModules.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainerModules()
    {
        return $this->trainerModules;
    }
}
