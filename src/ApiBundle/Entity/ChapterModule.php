<?php

namespace ApiBundle\Entity;

/**
 * ChapterModule.
 */
class ChapterModule
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Chapter
     */
    private $chapter;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var int
     */
    private $position;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Chapter.
     *
     * @param \ApiBundle\Entity\Chapter|null $chapter
     *
     * @return ChapterModule
     */
    public function setChapter(\ApiBundle\Entity\Chapter $chapter = null)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * Get Chapter.
     *
     * @return \ApiBundle\Entity\Chapter|null
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ChapterModule
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ChapterModule
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ChapterModule
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return ChapterModule
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }
}
