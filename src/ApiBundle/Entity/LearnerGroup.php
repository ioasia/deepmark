<?php

namespace ApiBundle\Entity;

/**
 * LearnerGroup.
 */
class LearnerGroup
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Group
     */
    private $group;

    /**
     * @var Profile
     */
    private $learner;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group.
     *
     * @param Group|null $group
     *
     * @return LearnerGroup
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return Group|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set learner.
     *
     * @param Profile|null $learner
     *
     * @return LearnerGroup
     */
    public function setLearner(Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return LearnerGroup
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return LearnerGroup
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
