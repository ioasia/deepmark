<?php

namespace ApiBundle\Entity;

/**
 * Quizes.
 */
class Quizes {

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $title;

    /**
     * @var text
     */
    private $description;

    /**
     * @var float
     */
    private $score_to_pass;

    /**
     * @var smallint
     */
    private $limit_attempt;
    
    /**
     * @var int
     */
    private $duration;
    

    /**
     * @var int
     */
    private $correction_duration;
    
    
    /**
     * @var int
     */
    private $is_survey;
    

    /**
     * @var int
     */
    private $skills_or_and;

    /**
     * @var int
     */
    private $limit_users;

    /**
     * @var date
     */
    private $created;

    /**
     * @var int
     */
    private $created_by;

    /**
     * @var text
     */
    private $params;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $questions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    public $steps;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    public $questions_xref;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    public $played;
    
    /**
     * @var int
     */
    public $questions_count;
    
     /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $skills;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->questions_xref = new \Doctrine\Common\Collections\ArrayCollection();
        $this->steps = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Module
     */
    public function setStatus($status = null) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Module
     */
    public function setTitle($title = null) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Module
     */
    public function setDescription($description = null) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get score_to_pass.
     *
     * @return string
     */
    public function getScore_to_pass() {
        return $this->score_to_pass;
    }

    /**
     * Set score_to_pass.
     *
     * @param string|null $score_to_pass
     *
     * @return Module
     */
    public function setScore_to_pass($score_to_pass = null) {
        $this->score_to_pass = $score_to_pass;

        return $this;
    }

    /**
     * Get limit_attempt.
     *
     * @return string
     */
    public function getLimit_attempt() {
        return $this->limit_attempt;
    }

    /**
     * Set limit_attemt.
     *
     * @param string|null $limit_attempt
     *
     * @return Module
     */
    public function setLimit_attempt($limit_attempt = null) {
        $this->limit_attempt = $limit_attempt;

        return $this;
    }
    
    
    /**
     * Get duration.
     *
     * @return int
     */
    public function getDuration() {
        return $this->duration;
    }

    /**
     * Set duration.
     *
     * @param int|null $duration
     *
     * @return Module
     */
    public function setDuration($duration = null) {
        $this->duration = $duration;

        return $this;
    }
    
    /**
     * Get skills_or_and.
     *
     * @return int
     */
    public function getSkills_or_and() {
        return $this->skills_or_and;
    }

    /**
     * Set skills_or_and.
     *
     * @param int|null $skills_or_and
     *
     * @return Module
     */
    public function setSkills_or_and($skills_or_and = null) {
        $this->skills_or_and = $skills_or_and;

        return $this;
    }
    
    /**
     * Get correction_duration.
     *
     * @return int
     */
    public function getCorrection_duration() {
        return $this->correction_duration;
    }

    /**
     * Set correction_duration.
     *
     * @param int|null $correction_duration
     *
     * @return Module
     */
    public function setCorrection_duration($correction_duration = null) {
        $this->correction_duration = $correction_duration;

        return $this;
    }
    
    
    
    /**
     * Get is_survey.
     *
     * @return int
     */
    public function getIsSurvey() {
        return $this->is_survey;
    }

    /**
     * Set is_survey.
     *
     * @param int|null $is_survey
     *
     * @return Module
     */
    public function setIsSurvey($is_survey = null) {
        $this->is_survey = $is_survey;

        return $this;
    }
    

    /**
     * Get limit_users.
     *
     * @return string
     */
    public function getLimit_users() {
        return $this->limit_users;
    }

    /**
     * Set limit_users.
     *
     * @param string|null $limit_users
     *
     * @return Module
     */
    public function setLimit_users($limit_users = null) {
        $this->limit_users = $limit_users;

        return $this;
    }

    /**
     * Get created.
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set created.
     *
     * @param string|null $created
     *
     * @return Module
     */
    public function setCreated($created = null) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created_by.
     *
     * @return string
     */
    public function getCreated_by() {
        return $this->created_by;
    }

    /**
     * Set created_by.
     *
     * @param string|null $created_by
     *
     * @return Module
     */
    public function setCreated_by($created_by = null) {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Set params.
     *
     * @param string|null $params
     *
     * @return Module
     */
    public function setParams($params = null) {
        $this->params = $params;

        return $this;
    }

    /**
     * Add step.
     *
     * @param \ApiBundle\Entity\QuizSteps $step
     *
     * @return Quizes
     */
    public function addStep(\ApiBundle\Entity\QuizSteps $step) {
        $this->steps[] = $step;

        return $this;
    }

    /**
     * Remove step.
     *
     * @param \ApiBundle\Entity\QuizSteps $step
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeStep(\ApiBundle\Entity\QuizSteps $step) {
        return $this->steps->removeElement($step);
    }

    /**
     * Get steps.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSteps() {
        return $this->steps;
    }

    /**
     * Add question.
     *
     * @param \ApiBundle\Entity\QuizQuestions $question
     *
     * @return Quizes
     */
    public function addQuestion(\ApiBundle\Entity\QuizQuestions $question) {
        $this->question[] = $question;

        return $this;
    }

    /**
     * Remove question.
     *
     * @param \ApiBundle\Entity\QuizQuestions $question
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeQuestion(\ApiBundle\Entity\QuizQuestions $question) {
        return $this->question->removeElement($question);
    }

    /**
     * Get questions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions() {
        return $this->questions;
    }

    /**
     * Add question Xref.
     *
     * @param \ApiBundle\Entity\QuizQuestionsXref $question_xref
     *
     * @return Quizes
     */
    public function addQuestionXref(\ApiBundle\Entity\QuizQuestionsXref $question_xref) {
        $this->questions_xref[] = $question_xref;

        return $this;
    }

    /**
     * Remove question Xref.
     *
     * @param \ApiBundle\Entity\QuizQuestionsXref $question_xref
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeQuestionXref(\ApiBundle\Entity\QuizQuestionsXref $question_xref) {
        return $this->questions_xref->removeElement($question_xref);
    }
    
    /**
     * Get questions_xref.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsXref() {
        return $this->questions_xref;
    }

    /**
     * Get scores total
     * 
     * @return int
     */
    public function getScores() {
        $questions = $this->getQuestions();
        $scores = 0;
        if($questions) {
            foreach($questions as $question) {
                $scores += $question->getQuestion()->getScore_to_pass();
            }
        }
        
        return $scores;
    }
    
     /**
     * Add skill.
     *
     * @param \ApiBundle\Entity\Skill $skill
     *
     * @return Module
     */
    public function addSkill(\ApiBundle\Entity\Skill $skill)
    {
        $this->skills[] = $skill;

        return $this;
    }

    /**
     * Remove skill.
     *
     * @param \ApiBundle\Entity\Skill $skill
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeSkill(\ApiBundle\Entity\Skill $skill)
    {
        return $this->skills->removeElement($skill);
    }

    /**
     * Get skills.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkills()
    {
        return $this->skills;
    }
}
