<?php

namespace ApiBundle\Entity;

/**
 * StoredModule.
 */
class StoredModule
{
    public const CONCEPTION = 1;
    public const PILOTAGE = 2;
    public const TEAM_PREPARATION = 3;
    public const ELEARNING = 4;
    public const QUIZ = 5;
    public const ONLINE = 6;
    public const VIRTUAL_CLASS = 7;
    public const ONLINE_WORKSHOP = 8;
    public const PRESENTATION_ANIMATION = 9;
    public const EVALUATION = 10;
    public const ASSESSMENT = 11;
    public const CHAPTER = 12;

    public const MODULE_REQUIRE_BOOKING = [
        self::ONLINE, self::VIRTUAL_CLASS,
        self::ONLINE_WORKSHOP, self::PRESENTATION_ANIMATION,
    ];

    private const COLORS = [
        self::CONCEPTION => 'green',
        self::PILOTAGE => 'darkgreen',
        self::ASSESSMENT => 'darkgreen',
        self::CHAPTER => 'lightblue',
        self::ELEARNING => 'orangelight',
        self::QUIZ => 'gold',
        self::ONLINE => 'navy',
        self::VIRTUAL_CLASS => 'blue',
        self::ONLINE_WORKSHOP => 'cadetblue',
        self::PRESENTATION_ANIMATION => 'lightblue',
        self::EVALUATION => 'lightmaroon',
    ];

    private const ICONS = [
        self::CONCEPTION => 'cogs',
        self::PILOTAGE => 'car',
        self::ASSESSMENT => 'car',
        self::CHAPTER => 'desktop',
        self::ELEARNING => 'box-open',
        self::QUIZ => 'question-square',
//        self::EXERCICE => 'question-square',
        self::ONLINE => 'globe',
        self::VIRTUAL_CLASS => 'desktop',
        self::ONLINE_WORKSHOP => 'desktop',
        self::PRESENTATION_ANIMATION => 'desktop',
        self::EVALUATION => 'pen-square',
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var int
     */
    private $appId;

    /**
     * @var bool
     */
    private $visibilityTraining = '0';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return StoredModule
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set appId.
     *
     * @param int $appId
     *
     * @return StoredModule
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId.
     *
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set visibilityTraining.
     *
     * @param bool $visibilityTraining
     *
     * @return StoredModule
     */
    public function setVisibilityTraining($visibilityTraining)
    {
        $this->visibilityTraining = $visibilityTraining;

        return $this;
    }

    /**
     * Get visibilityTraining.
     *
     * @return bool
     */
    public function getVisibilityTraining()
    {
        return $this->visibilityTraining;
    }

    /**
     * Color to be used in form for editing it.
     *
     * @return string
     */
    public function getColor(): string
    {
        return self::COLORS[$this->getAppId()] ?? '';
    }

    /**
     * Icon to be used in form for editing it.
     *
     * @return string
     */
    public function getIcon(): string
    {
        return self::ICONS[$this->getAppId()] ?? '';
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StoredModule
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return StoredModule
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
