<?php

namespace ApiBundle\Entity;

/**
 * LiveResourceVariety.
 */
class LiveResourceVariety
{
    const MENTOR = 1;
    const LIVE_TRAINER = 2;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $designation;

    /**
     * @var int|null
     */
    private $appId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string|null $designation
     *
     * @return LiveResourceVariety
     */
    public function setDesignation($designation = null)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string|null
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set appId.
     *
     * @param int|null $appId
     *
     * @return LiveResourceVariety
     */
    public function setAppId($appId = null)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId.
     *
     * @return int|null
     */
    public function getAppId()
    {
        return $this->appId;
    }

    public function __toString()
    {
        return $this->designation;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return LiveResourceVariety
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return LiveResourceVariety
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
