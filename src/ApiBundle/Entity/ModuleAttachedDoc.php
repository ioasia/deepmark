<?php

namespace ApiBundle\Entity;

/**
 * ModuleAttachedDoc.
 */
class ModuleAttachedDoc
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $link;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $profileVariety;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->profileVariety = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return ModuleAttachedDoc
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleAttachedDoc
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Add profileVariety.
     *
     * @param \ApiBundle\Entity\ProfileVariety $profileVariety
     *
     * @return ModuleAttachedDoc
     */
    public function addProfileVariety(\ApiBundle\Entity\ProfileVariety $profileVariety)
    {
        $this->profileVariety[] = $profileVariety;

        return $this;
    }

    /**
     * Remove profileVariety.
     *
     * @param \ApiBundle\Entity\ProfileVariety $profileVariety
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProfileVariety(\ApiBundle\Entity\ProfileVariety $profileVariety)
    {
        return $this->profileVariety->removeElement($profileVariety);
    }

    /**
     * Get profileVariety.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfileVariety()
    {
        return $this->profileVariety;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleAttachedDoc
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleAttachedDoc
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
