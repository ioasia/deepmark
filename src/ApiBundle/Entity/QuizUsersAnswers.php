<?php

namespace ApiBundle\Entity;

/**
 * QuizUsersAnswers.
 */
class QuizUsersAnswers
{
    /**
     * @var int
     */
    private $id;

   /**
     * @var int
     */
    private $play_id;
    
    /**
     * @var int
     */
    private $question_id;
    
    /**
     * @var text
     */
    private $answer;
    
    /**
     * @var int
     */
    private $scores;
    
    /**
     * @var int
     */
    private $status;
    
    /**
     * @var text
     */
    private $params;
    
     
    /**
     * @var \ApiBundle\Entity\QuizUsersPlayed
     */
    private $play;
    
    
    /**
     * @var \ApiBundle\Entity\QuizQuestions
     */
    private $question;

    /**
     * @var \ApiBundle\Entity\Exercises
     */
    private $exercises;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Get play.
     *
     * @return string
     */
    public function getPlay()
    {
        return $this->play;
    }

    /**
     * Set play.
     *
     * @param \ApiBundle\Entity\QuizUsersPlayed|null $play
     *
     * @return Module
     */
    public function setPlay($play = null)
    {
        $this->play = $play;

        return $this;
    }

    /**
     * Get exercises.
     *
     * @return string
     */
    public function getExercises()
    {
        return $this->exercises;
    }

    /**
     * Set exercises.
     *
     * @param \ApiBundle\Entity\Exercises|null $exercises
     *
     * @return QuizUsersAnswers
     */
    public function setExercises($exercises = null)
    {
        $this->exercises = $exercises;

        return $this;
    }
    
    /**
     * Get questionId.
     *
     * @return string
     */
    public function getQuestionId()
    {
        return $this->question->getId();
    }
    
    
     /**
     * Get question.
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }
    
    /**
     * Set question_id.
     *
     * @param \ApiBundle\Entity\QuizQuestions|null $question
     *
     * @return Module
     */
    public function setQuestion($question = null)
    {
        $this->question = $question;

        return $this;
    }
    
     /**
     * Get answer.
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }
    
    /**
     * Set answer.
     *
     * @param string|null $answer
     *
     * @return Module
     */
    public function setAnswer($answer = null)
    {
        $this->answer = $answer;

        return $this;
    }
    
     /**
     * Get scores.
     *
     * @return string
     */
    public function getScores()
    {
        return $this->scores;
    }
    
    /**
     * Set scores.
     *
     * @param string|null $scores
     *
     * @return Module
     */
    public function setScores($scores = null)
    {
        $this->scores = $scores;

        return $this;
    }
    
    
    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Module
     */
    public function setStatus($status = null) {
        $this->status = $status;

        return $this;
    }
    
     /**
     * Get params.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }
    
    /**
     * Set params.
     *
     * @param string|null $params
     *
     * @return Module
     */
    public function setParams($params = null)
    {
        $this->params = $params;

        return $this;
    }
}
