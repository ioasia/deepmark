<?php

namespace ApiBundle\Entity;

/**
 * ReportLine.
 */
class ReportLine
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $wording;

    /**
     * @var float|null
     */
    private $notation;

    /**
     * @var bool
     */
    private $title = '0';

    /**
     * @var \ApiBundle\Entity\Report
     */
    private $report;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wording.
     *
     * @param string $wording
     *
     * @return ReportLine
     */
    public function setWording($wording)
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * Get wording.
     *
     * @return string
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * Set notation.
     *
     * @param float|null $notation
     *
     * @return ReportLine
     */
    public function setNotation($notation = null)
    {
        $this->notation = $notation;

        return $this;
    }

    /**
     * Get notation.
     *
     * @return float|null
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set title.
     *
     * @param bool $title
     *
     * @return ReportLine
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return bool
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set report.
     *
     * @param \ApiBundle\Entity\Report|null $report
     *
     * @return ReportLine
     */
    public function setReport(\ApiBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report.
     *
     * @return \ApiBundle\Entity\Report|null
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ReportLine
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ReportLine
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
