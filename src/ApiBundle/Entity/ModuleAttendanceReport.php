<?php

namespace ApiBundle\Entity;

/**
 * ModuleAttendanceReport.
 */
class ModuleAttendanceReport
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $fileDescriptor;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleAttendanceReport
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return ModuleAttendanceReport
     */

    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return TrainerDocument
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleAttendanceReport
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleAttendanceReport
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
