<?php

namespace ApiBundle\Entity;

/**
 * AssigmentResourceSystem.
 */
class AssigmentResourceSystem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $minBookingDate;

    /**
     * @var \DateTime
     */
    private $maxBookingDate;

    /**
     * @var bool
     */
    private $hide = '0';

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $liveResource;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set minBookingDate.
     *
     * @param \DateTime $minBookingDate
     *
     * @return AssigmentResourceSystem
     */
    public function setMinBookingDate($minBookingDate)
    {
        $this->minBookingDate = $minBookingDate;

        return $this;
    }

    /**
     * Get minBookingDate.
     *
     * @return \DateTime
     */
    public function getMinBookingDate()
    {
        return $this->minBookingDate;
    }

    /**
     * Set maxBookingDate.
     *
     * @param \DateTime $maxBookingDate
     *
     * @return AssigmentResourceSystem
     */
    public function setMaxBookingDate($maxBookingDate)
    {
        $this->maxBookingDate = $maxBookingDate;

        return $this;
    }

    /**
     * Get maxBookingDate.
     *
     * @return \DateTime
     */
    public function getMaxBookingDate()
    {
        return $this->maxBookingDate;
    }

    /**
     * Set hide.
     *
     * @param bool $hide
     *
     * @return AssigmentResourceSystem
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide.
     *
     * @return bool
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set liveResource.
     *
     * @param \ApiBundle\Entity\Profile|null $liveResource
     *
     * @return AssigmentResourceSystem
     */
    public function setLiveResource(\ApiBundle\Entity\Profile $liveResource = null)
    {
        $this->liveResource = $liveResource;

        return $this;
    }

    /**
     * Get liveResource.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLiveResource()
    {
        return $this->liveResource;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return AssigmentResourceSystem
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return AssigmentResourceSystem
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \ApiBundle\Entity\ModuleSession
     */
    private $moduleSession;

    /**
     * Set moduleSession.
     *
     * @param \ApiBundle\Entity\ModuleSession|null $moduleSession
     *
     * @return AssigmentResourceSystem
     */
    public function setModuleSession(\ApiBundle\Entity\ModuleSession $moduleSession = null)
    {
        $this->moduleSession = $moduleSession;

        return $this;
    }

    /**
     * Get moduleSession.
     *
     * @return \ApiBundle\Entity\ModuleSession|null
     */
    public function getModuleSession()
    {
        return $this->moduleSession;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return AssigmentResourceSystem
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return AssigmentResourceSystem
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
