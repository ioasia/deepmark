<?php

namespace ApiBundle\Entity;

/**
 * StatsExportTemplate.
 */
class StatsExportTemplate {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $tab;
    
    /**
     * @var int
     */
    private $col;

    /**
     * @var text
     */
    private $params;
    
    /**
     * @var date
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $configurations;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->configurations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return Module
     */
    public function setType($type = null) {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Get tab.
     *
     * @return int
     */
    public function getTab() {
        return $this->tab;
    }

    /**
     * Set tab.
     *
     * @param int|null $tab
     *
     * @return Module
     */
    public function setTab($tab = null) {
        $this->tab = $tab;

        return $this;
    }
    
    /**
     * Get col.
     *
     * @return int
     */
    public function getCol() {
        return $this->col;
    }

    /**
     * Set col.
     *
     * @param int|null $col
     *
     * @return Module
     */
    public function setCol($col = null) {
        $this->col = $col;

        return $this;
    }

    /**
     * Get params.
     *
     * @return string
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Set params.
     *
     * @param string|null $params
     *
     * @return Module
     */
    public function setParams($params = null) {
        $this->params = $params;

        return $this;
    }
    
    /**
     * Get created.
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set created.
     *
     * @param string|null $created
     *
     * @return Module
     */
    public function setCreated($created = null) {
        $this->created = $created;

        return $this;
    }

    /**
     * Add configuration.
     *
     * @param \ApiBundle\Entity\StatsExportTemplateXref $configuration
     *
     * @return Quizes
     */
    public function addConfiguration(\ApiBundle\Entity\StatsExportTemplateXref $configuration) {
        $this->configurations[] = $configuration;

        return $this;
    }

    /**
     * Remove configurations.
     *
     * @param \ApiBundle\Entity\StatsExportTemplateXref $configuration
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeConfiguration(\ApiBundle\Entity\StatsExportTemplateXref $configuration) {
        return $this->configurations->removeElement($configuration);
    }

    /**
     * Get configurations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigurations() {
        return $this->configurations;
    }

}
