<?php

namespace ApiBundle\Entity;

/**
 * Scormvars.
 */
class Scormvars
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $SCOInstanceID;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $varName;

    /**
     * @var text|null
     */
    private $varValue;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSCOInstanceID()
    {
        return $this->SCOInstanceID;
    }

    /**
     * @param int $SCOInstanceID
     */
    public function setSCOInstanceID($SCOInstanceID)
    {
        $this->SCOInstanceID = $SCOInstanceID;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return Scormvars
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return string
     */
    public function getVarName()
    {
        return $this->varName;
    }

    /**
     * @param string $varName
     *
     * @return Scormvars
     */
    public function setVarName($varName)
    {
        $this->varName = $varName;

        return $this;
    }

    /**
     * @return text|null
     */
    public function getVarValue()
    {
        return $this->varValue;
    }

    /**
     * @param text|null $varValue
     *
     * @return Scormvars
     */
    public function setVarValue($varValue)
    {
        $this->varValue = $varValue;

        return $this;
    }
}
