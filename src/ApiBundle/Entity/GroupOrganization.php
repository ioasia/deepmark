<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * GroupOrganization.
 */
class GroupOrganization
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \ApiBundle\Entity\Regional
     */
    private $regional;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workplaces;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->workplaces = new ArrayCollection();
    }

    /**
     * Add workplace.
     *
     * @param \ApiBundle\Entity\Workplace $workplace
     *
     * @return GroupOrganization
     */
    public function addWorkplace(\ApiBundle\Entity\Workplace $workplace)
    {
        $this->workplaces[] = $workplace;

        return $this;
    }

    /**
     * Remove workplace.
     *
     * @param \ApiBundle\Entity\Workplace $workplace
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeWorkplace(\ApiBundle\Entity\Workplace $workplace)
    {
        return $this->workplaces->removeElement($workplace);
    }

    /**
     * Get workplaces.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkplaces()
    {
        return $this->workplaces;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return GroupOrganization
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set regional.
     *
     * @param \ApiBundle\Entity\Regional|null $regional
     *
     * @return GroupOrganization
     */
    public function setRegional(\ApiBundle\Entity\Regional $regional = null)
    {
        $this->regional = $regional;

        return $this;
    }

    /**
     * Get regional.
     *
     * @return \ApiBundle\Entity\Regional|null
     */
    public function getRegional()
    {
        return $this->regional;
    }

    public function __toString()
    {
        $name = '';
        if ($this->getRegional()) {
            $name .= $this->getRegional()->getDesignation().' ';
        }
        if ($this->getDesignation()) {
            $name .= $this->getDesignation();
        }

        return $name;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return GroupOrganization
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return GroupOrganization
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
