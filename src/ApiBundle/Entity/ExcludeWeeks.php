<?php

namespace ApiBundle\Entity;

use DateTime;

/**
 * ExcludeWeeks.
 */
class ExcludeWeeks
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var DateTime
     */
    private $start;

    /**
     * @var DateTime
     */
    private $end;

    /**
     * @var \ApiBundle\Entity\Intervention
     */
    private $intervention;

    /**
     * @var DateTime
     */
    private $created;

    /**
     * @var DateTime
     */
    private $updated;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set start.
     *
     * @param DateTime $start
     *
     * @return ExcludeWeeks
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end.
     *
     * @param DateTime $end
     *
     * @return ExcludeWeeks
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Set intervention.
     *
     * @param \ApiBundle\Entity\Intervention|null $intervention
     *
     * @return ExcludeWeeks
     */
    public function setIntervention(\ApiBundle\Entity\Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get intervention.
     *
     * @return \ApiBundle\Entity\Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Set created.
     *
     * @param DateTime $created
     *
     * @return AvailableCalendar
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param DateTime $updated
     *
     * @return AvailableCalendar
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
