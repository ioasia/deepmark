<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Skill.
 */
class Skill
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var string
     */
    private $skill_description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $modules;
    
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quizes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $domains;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->domains = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Domain $domain
     * @return $this
     */
    public function addDomain(\ApiBundle\Entity\Domain $domain)
    {
        $this->domains[] = $domain;

        return $this;
    }

    /**
     * @param Domain $domain
     * @return bool
     */
    public function removeDomain(\ApiBundle\Entity\Domain $domain)
    {
        return $this->domains->removeElement($domain);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Skill
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    public function __toString()
    {
        return $this->designation;
    }

    /**
     * Add module.
     *
     * @param \ApiBundle\Entity\Module $module
     *
     * @return Skill
     */
    public function addModule(\ApiBundle\Entity\Module $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Remove module.
     *
     * @param \ApiBundle\Entity\Module $module
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModule(\ApiBundle\Entity\Module $module)
    {
        return $this->modules->removeElement($module);
    }

    /**
     * @return string
     */
    public function getSkillDescription()
    {
        return $this->skill_description;
    }

    /**
     * @param string $skill_description
     */
    public function setSkillDescription(string $skill_description): void
    {
        $this->skill_description = $skill_description;
    }

    /**
     * Get modules.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }
    
    
    /**
     * Get quizes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizes()
    {
        return $this->modules;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Skill
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Skill
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
