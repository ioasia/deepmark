<?php

namespace ApiBundle\Entity;

/**
 * ModuleIteration.
 */
class ModuleIteration
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $executionDate;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $startTime;

    /**
     * @var int
     */
    private $learnedTime;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set executionDate.
     *
     * @param \DateTime $executionDate
     *
     * @return ModuleIteration
     */
    public function setExecutionDate($executionDate)
    {
        $this->executionDate = $executionDate;

        return $this;
    }

    /**
     * Get executionDate.
     *
     * @return \DateTime
     */
    public function getExecutionDate()
    {
        return $this->executionDate;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return ModuleIteration
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set status.
     *
     * @param Status|null $status
     *
     * @return ModuleIteration
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleIteration
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleIteration
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleIteration
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     */
    public function setStartTime(\DateTime $startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return int
     */
    public function getLearnedTime()
    {
        return $this->learnedTime;
    }

    /**
     * Set learnedTime.
     *
     * @param int $learnedTime
     *
     * @return ModuleIteration
     */
    public function setLearnedTime($learnedTime)
    {
        $this->learnedTime = $learnedTime;
        return $this;
    }
}
