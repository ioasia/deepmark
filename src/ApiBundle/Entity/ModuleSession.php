<?php

namespace ApiBundle\Entity;

/**
 * ModuleSession.
 */
class ModuleSession
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Session
     */
    private $session;

    /**
     * @var \ApiBundle\Entity\SessionPlace
     */
    private $sessionPlace;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set session.
     *
     * @param \ApiBundle\Entity\Session|null $session
     *
     * @return ModuleSession
     */
    public function setSession(\ApiBundle\Entity\Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return \ApiBundle\Entity\Session|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set sessionPlace.
     *
     * @param \ApiBundle\Entity\SessionPlace|null $sessionPlace
     *
     * @return ModuleSession
     */
    public function setSessionPlace(\ApiBundle\Entity\SessionPlace $sessionPlace = null)
    {
        $this->sessionPlace = $sessionPlace;

        return $this;
    }

    /**
     * Get sessionPlace.
     *
     * @return \ApiBundle\Entity\SessionPlace|null
     */
    public function getSessionPlace()
    {
        return $this->sessionPlace;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleSession
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleSession
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
