<?php

namespace ApiBundle\Entity;


/**
 * Tag.
 */
class Tag
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $tagName;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $tagName
     * @return $this
     */
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    public function __toString()
    {
        return $this->tagName;
    }


    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Skill
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

}
