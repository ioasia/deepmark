<?php

namespace ApiBundle\Entity;

/**
 * Exercises.
 */
class Exercises
{
    const STATUS_TODO = 'TODO';
    const STATUS_SUBMITTED = 'SUBMITTED';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\QuizUsersAnswers
     */
    private $answer;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var \DateTime|null
     */
    private $validationDate;

    /**
     * @var string
     */
    private $status = 'TODO';

    /**
     * @var int
     */
    private $grade;

    /**
     * @var text
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer.
     *
     * @param \ApiBundle\Entity\QuizUsersAnswers|null $answer
     *
     * @return Exercises
     */
    public function setAnswer(\ApiBundle\Entity\QuizUsersAnswers $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return \ApiBundle\Entity\QuizUsersAnswers|null
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set trainer.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return Exercises
     */

    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set dateAdd.
     *
     * @param \DateTime $dateAdd
     *
     * @return Exercises
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd.
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set validationDate.
     *
     * @param \DateTime|null $validationDate
     *
     * @return Exercises
     */
    public function setValidationDate($validationDate = null)
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * Get validationDate.
     *
     * @return \DateTime|null
     */
    public function getValidationDate()
    {
        return $this->validationDate;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Exercises
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set grade.
     *
     * @param int $grade
     *
     * @return Exercises
     */
    public function setGrade(int $grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return Exercises
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Exercises
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Exercises
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
