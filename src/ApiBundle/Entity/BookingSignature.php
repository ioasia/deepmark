<?php

namespace ApiBundle\Entity;

/**
 * BookingSignature.
 */
class BookingSignature
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var \ApiBundle\Entity\BookingAgenda
     */
    private $session;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $signature;

    /**
     * @var int
     */
    private $signatureType;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return BookingSignature
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set session.
     *
     * @param \ApiBundle\Entity\BookingAgenda|null $session
     *
     * @return BookingSignature
     */
    public function setSession(\ApiBundle\Entity\BookingAgenda $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return \ApiBundle\Entity\BookingAgenda|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set signature.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $signature
     *
     * @return BookingSignature
     */
    public function setSignature(\ApiBundle\Entity\FileDescriptor $signature = null)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return int
     */
    public function getSignatureType()
    {
        return $this->signatureType;
    }

    /**
     * @param int $signatureType
     */
    public function setSignatureType(int $signatureType)
    {
        $this->signatureType = $signatureType;
    }

    /**
     * Get signature.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BookingSignature
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BookingSignature
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
