<?php

namespace ApiBundle\Entity;

/**
 * ModuleNotes.
 */
class ModuleReports
{
    const STATUS_TODO = 'TODO';
    const STATUS_SUBMITTED = 'SUBMITTED';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\BookingAgenda
     */
    private $session;


    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $fileDescriptor;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var \DateTime|null
     */
    private $validationDate;

    /**
     * @var string
     */
    private $status = 'TODO';

    /**
     * @var text
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set session.
     *
     * @param \ApiBundle\Entity\BookingAgenda|null $session
     *
     * @return ModuleReports
     */
    public function setSession(\ApiBundle\Entity\BookingAgenda $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return \ApiBundle\Entity\BookingAgenda|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleReports
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return ModuleReports
     */

    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return TrainerDocument
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

    /**
     * Set dateAdd.
     *
     * @param \DateTime $dateAdd
     *
     * @return ModuleReports
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd.
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set validationDate.
     *
     * @param \DateTime|null $validationDate
     *
     * @return ModuleReports
     */
    public function setValidationDate($validationDate = null)
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * Get validationDate.
     *
     * @return \DateTime|null
     */
    public function getValidationDate()
    {
        return $this->validationDate;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return ModuleReports
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return ModuleReports
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleReports
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleReports
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
