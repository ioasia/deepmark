<?php

namespace ApiBundle\Entity;

/**
 * Class Configurations
 * @package ApiBundle\Entity
 */
class Configurations
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
