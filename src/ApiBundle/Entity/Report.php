<?php

namespace ApiBundle\Entity;

/**
 * Report.
 */
class Report
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var \DateTime|null
     */
    private $updateDate;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $admin;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return Report
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Report
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set admin.
     *
     * @param \ApiBundle\Entity\Profile|null $admin
     *
     * @return Report
     */
    public function setAdmin(\ApiBundle\Entity\Profile $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return Report
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Report
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Report
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
