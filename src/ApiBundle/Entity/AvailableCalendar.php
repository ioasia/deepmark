<?php

namespace ApiBundle\Entity;

/**
 * AvailableCalendar.
 */
class AvailableCalendar
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $beginning;

    /**
     * @var \DateTime
     */
    private $ending;

    /**
     * @var string
     */
    private $freezeTime;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set beginning.
     *
     * @param \DateTime $beginning
     *
     * @return AvailableCalendar
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;

        return $this;
    }

    /**
     * Get beginning.
     *
     * @return \DateTime
     */
    public function getBeginning()
    {
        return $this->beginning;
    }

    /**
     * Set ending.
     *
     * @param \DateTime $ending
     *
     * @return AvailableCalendar
     */
    public function setEnding($ending)
    {
        $this->ending = $ending;

        return $this;
    }

    /**
     * Get ending.
     *
     * @return \DateTime
     */
    public function getEnding()
    {
        return $this->ending;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return AvailableCalendar
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set Freeze time.
     *
     * @param string $freezeTime
     *
     * @return AvailableCalendar
     */
    public function setFreezeTime($freezeTime)
    {
        $this->freezeTime = $freezeTime;

        return $this;
    }

    /**
     * Get Freeze time.
     *
     * @return string
     */
    public function getFreezeTime()
    {
        return $this->freezeTime;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return AvailableCalendar
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return AvailableCalendar
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
