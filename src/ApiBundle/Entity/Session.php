<?php

namespace ApiBundle\Entity;

/**
 * Session.
 */
class Session
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $sessionDate;

    /**
     * @var int
     */
    private $participant = '1';

    /**
     * @var int
     */
    private $participantMax = '1';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rooms;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Room $room
     * @return $this
     */
    public function addRoom(\ApiBundle\Entity\Room $room)
    {
        $this->rooms[] = $room;

        return $this;
    }

    /**
     * @param Room $room
     * @return bool
     */
    public function removeRoom(\ApiBundle\Entity\Room $room)
    {
        return $this->rooms->removeElement($room);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set sessionDate.
     *
     * @param \DateTime $sessionDate
     *
     * @return Session
     */
    public function setSessionDate($sessionDate)
    {
        $this->sessionDate = $sessionDate;

        return $this;
    }

    /**
     * Get sessionDate.
     *
     * @return \DateTime
     */
    public function getSessionDate()
    {
        return $this->sessionDate;
    }

    /**
     * Set participant.
     *
     * @param int $participant
     *
     * @return Session
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;

        return $this;
    }

    /**
     * Get participant.
     *
     * @return int
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Session
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Session
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return int
     */
    public function getParticipantMax(): int
    {
        return $this->participantMax;
    }

    /**
     * @param int $participantMax
     */
    public function setParticipantMax(int $participantMax)
    {
        $this->participantMax = $participantMax;
        return $this;
    }


}
