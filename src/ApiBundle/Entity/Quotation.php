<?php

namespace ApiBundle\Entity;

/**
 * Quotation.
 */
class Quotation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var string|null
     */
    private $observation;

    /**
     * @var \DateTime|null
     */
    private $sentDate;

    /**
     * @var \DateTime|null
     */
    private $statusDate;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $admin;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * @var \ApiBundle\Entity\Intervention
     */
    private $intervention;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $customer;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Quotation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set observation.
     *
     * @param string|null $observation
     *
     * @return Quotation
     */
    public function setObservation($observation = null)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation.
     *
     * @return string|null
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set sentDate.
     *
     * @param \DateTime|null $sentDate
     *
     * @return Quotation
     */
    public function setSentDate($sentDate = null)
    {
        $this->sentDate = $sentDate;

        return $this;
    }

    /**
     * Get sentDate.
     *
     * @return \DateTime|null
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set statusDate.
     *
     * @param \DateTime|null $statusDate
     *
     * @return Quotation
     */
    public function setStatusDate($statusDate = null)
    {
        $this->statusDate = $statusDate;

        return $this;
    }

    /**
     * Get statusDate.
     *
     * @return \DateTime|null
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set admin.
     *
     * @param \ApiBundle\Entity\Profile|null $admin
     *
     * @return Quotation
     */
    public function setAdmin(\ApiBundle\Entity\Profile $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return Quotation
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set intervention.
     *
     * @param \ApiBundle\Entity\Intervention|null $intervention
     *
     * @return Quotation
     */
    public function setIntervention(\ApiBundle\Entity\Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get intervention.
     *
     * @return \ApiBundle\Entity\Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Set customer.
     *
     * @param \ApiBundle\Entity\Profile|null $customer
     *
     * @return Quotation
     */
    public function setCustomer(\ApiBundle\Entity\Profile $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Quotation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Quotation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
