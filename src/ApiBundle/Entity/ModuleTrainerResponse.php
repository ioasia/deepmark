<?php

namespace ApiBundle\Entity;

/**
 * ModuleResponse.
 */
class ModuleTrainerResponse
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \DateTime
     */
    private $dateResponse;

    /**
     * @var float
     */
    private $notation;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ModuleResponse
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set dateResponse.
     *
     * @param \DateTime $dateResponse
     *
     * @return ModuleResponse
     */
    public function setDateResponse($dateResponse)
    {
        $this->dateResponse = $dateResponse;

        return $this;
    }

    /**
     * Get dateResponse.
     *
     * @return \DateTime
     */
    public function getDateResponse()
    {
        return $this->dateResponse;
    }

    /**
     * Set notation.
     *
     * @param float $notation
     *
     * @return ModuleResponse
     */
    public function setNotation($notation)
    {
        $this->notation = $notation;

        return $this;
    }

    /**
     * Get notation.
     *
     * @return float
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set trainer.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return ModuleResponse
     */
    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return ModuleResponse
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleResponse
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleResponse
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleResponse
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
