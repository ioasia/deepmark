<?php

namespace ApiBundle\Entity;

/**
 * QuizQuestionsItems.
 */
class QuizQuestionsItems {

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $question_id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var text
     */
    private $value;

    /**
     * @var text
     */
    public $specifics;

    /**
     * @var int
     */
    private $scores;
    
    /**
     * @var int
     */
    private $status;
    
    /**
     * @var date
     */
    private $created;
    
    /**
     * @var \ApiBundle\Entity\QuizQuestionsItems
     */
    public $item;
    
    /**
     * @var \ApiBundle\Entity\QuizQuestions
     */
    private $question;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Get question_id.
     *
     * @return string
     */
    public function getQuestion_id()
    {
        return $this->question_id;
    }
    
    /**
     * Set question_id.
     *
     * @param string|null $question_id
     *
     * @return Module
     */
    public function setQuestion_id($question_id = null)
    {
        $this->question_id = $question_id;

        return $this;
    }
    
    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return Module
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Set name.
     *
     * @param string|null $name
     *
     */
    public function setName($name = null)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

     /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Module
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }
    
    /**
     * Get specifics.
     *
     * @return string
     */
    public function getSpecifics()
    {
        return $this->specifics;
    }
    
    /**
     * Set specifics.
     *
     * @param string|null $specifics
     *
     * @return Module
     */
    public function setSpecifics($specifics = null)
    {
        $this->specifics = $specifics;

        return $this;
    }
    
    /**
     * Get scores.
     *
     * @return string
     */
    public function getScores()
    {
        return $this->scores;
    }
    
    /**
     * Set scores.
     *
     * @param string|null $scores
     *
     * @return Module
     */
    public function setScores($scores = null)
    {
        $this->scores = $scores;

        return $this;
    }
    
    
    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Module
     */
    public function setStatus($status = null) {
        $this->status = $status;

        return $this;
    }
    
     /**
     * Get created.
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }
    
    /**
     * Set created.
     *
     * @param string|null $created
     *
     * @return Module
     */
    public function setCreated($created = null)
    {
        $this->created = $created;

        return $this;
    }
    
    /**
     * Set question.
     *
     * @param \ApiBundle\Entity\QuizQuestions|null $question
     *
     * @return QuizQuestions
     */
    public function setQuestion(\ApiBundle\Entity\QuizQuestions $question = null) {
        $this->setQuestion_id($question->getId());
        $this->question = $question;

        return $this;
    }
    
    /**
     * Get step.
     *
     * @return int
     */
    public function getQuestion() {
        return $this->question;
    }
}
