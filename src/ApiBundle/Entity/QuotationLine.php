<?php

namespace ApiBundle\Entity;

/**
 * QuotationLine.
 */
class QuotationLine
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $quantity;

    /**
     * @var \ApiBundle\Entity\Product
     */
    private $product;

    /**
     * @var \ApiBundle\Entity\Quotation
     */
    private $quotation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param string|null $quantity
     *
     * @return QuotationLine
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set product.
     *
     * @param \ApiBundle\Entity\Product|null $product
     *
     * @return QuotationLine
     */
    public function setProduct(\ApiBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \ApiBundle\Entity\Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set quotation.
     *
     * @param \ApiBundle\Entity\Quotation|null $quotation
     *
     * @return QuotationLine
     */
    public function setQuotation(\ApiBundle\Entity\Quotation $quotation = null)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation.
     *
     * @return \ApiBundle\Entity\Quotation|null
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return QuotationLine
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return QuotationLine
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
