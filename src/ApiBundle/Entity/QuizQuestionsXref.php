<?php

namespace ApiBundle\Entity;

/**
 * QuizQuestionsXref.
 */
class QuizQuestionsXref {

    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Quiz
     */
    private $quiz;
    
    
    /**
     * @var \ApiBundle\Entity\QuizSteps
     */
    private $step;
    
    
    /**
     * @var \ApiBundle\Entity\QuizQuestions
     */
    private $question;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set quiz.
     *
     * @param \ApiBundle\Entity\Quizes|null $quiz
     *
     * @return Quizes
     */
    public function setQuiz(\ApiBundle\Entity\Quizes $quiz = null) {
        $this->quiz = $quiz;

        return $this;
    }
    
    /**
     * Get quiz.
     *
     * @return int
     */
    public function getQuiz() {
        return $this->quiz;
    }

    /**
     * Set step.
     *
     * @param \ApiBundle\Entity\QuizSteps|null $step
     *
     * @return QuizSteps
     */
    public function setStep(\ApiBundle\Entity\QuizSteps $step = null) {
        $this->step = $step;

        return $this;
    }
    
    /**
     * Get step.
     *
     * @return int
     */
    public function getStep() {
        return $this->step;
    }

    
    /**
     * Set question.
     *
     * @param \ApiBundle\Entity\QuizQuestions|null $question
     *
     * @return QuizQuestions
     */
    public function setQuestion(\ApiBundle\Entity\QuizQuestions $question = null) {
        $this->question = $question;

        return $this;
    }
    
    /**
     * Get question.
     *
     * @return int
     */
    public function getQuestion() {
        return $this->question;
    }

}
