<?php

namespace ApiBundle\Entity;

/**
 * ScormSynthesis.
 */
class ScormSynthesis
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $SCOInstanceID;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var string
     */
    private $lessonStatus;

    /**
     * @var string
     */
    private $score;

    /**
     * @var string
     */
    private $timeSpent;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSCOInstanceID()
    {
        return $this->SCOInstanceID;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return ScormSynthesis
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param int $SCOInstanceID
     */
    public function setSCOInstanceID($SCOInstanceID)
    {
        $this->SCOInstanceID = $SCOInstanceID;
    }

    /**
     * @return string
     */
    public function getLessonStatus()
    {
        return $this->lessonStatus;
    }

    /**
     * @param string $lessonStatus
     *
     * @return ScormSynthesis
     */
    public function setLessonStatus($lessonStatus)
    {
        $this->lessonStatus = $lessonStatus;

        return $this;
    }

    /**
     * @return string
     *
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param string $score
     * @return ScormSynthesis
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSpent()
    {
        return $this->timeSpent;
    }

    /**
     * @param string $timeSpent
     *
     * @return ScormSynthesis
     */
    public function setTimeSpent($timeSpent)
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ScormSynthesis
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ScormSynthesis
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
