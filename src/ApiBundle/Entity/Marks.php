<?php

namespace ApiBundle\Entity;

/**
 * Marks.
 */
class Marks
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Exercises
     */
    private $exercises;

    /**
     * @var \ApiBundle\Entity\QuizQuestionsItems
     */
    private $questionsItem;

    /**
     * @var text
     */
    private $specifics;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercises.
     *
     * @param \ApiBundle\Entity\Exercises|null $exercises
     *
     * @return Marks
     */
    public function setExercises(\ApiBundle\Entity\Exercises $exercises = null)
    {
        $this->exercises = $exercises;

        return $this;
    }

    /**
     * Get exercises.
     *
     * @return \ApiBundle\Entity\Exercises|null
     */
    public function getExercises()
    {
        return $this->exercises;
    }

    /**
     * Set questionsItem.
     *
     * @param \ApiBundle\Entity\QuizQuestionsItems|null $questionsItem
     *
     * @return Marks
     */

    public function setQuestionsItem(\ApiBundle\Entity\QuizQuestionsItems $questionsItem = null)
    {
        $this->questionsItem = $questionsItem;

        return $this;
    }

    /**
     * Get questionsItem.
     *
     * @return \ApiBundle\Entity\QuizQuestionsItems|null
     */
    public function getQuestionsItem()
    {
        return $this->questionsItem;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Module
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get specifics.
     *
     * @return string
     */
    public function getSpecifics()
    {
        return $this->specifics;
    }

    /**
     * Set specifics.
     *
     * @param string|null $specifics
     *
     * @return Marks
     */
    public function setSpecifics($specifics = null)
    {
        $this->specifics = $specifics;

        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Exercises
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Exercises
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
