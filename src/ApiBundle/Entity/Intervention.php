<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Intervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $beginning;

    /**
     * @var \DateTime
     */
    private $ending;

    /**
     * @var int
     */
    private $ready = 0;

    /**
     * @var string|null
     */
    private $openDays;

    /**
     * @var bool
     */
    private $sequential = '1';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $modules;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $excludeWeeks;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $learners;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tags;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chapters;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $myFavorites;

    /**
     * @var FileDescriptor
     */
    private $logo;

    /**
     * @var Priority
     */
    private $priority;

    /**
     * @var Profile
     */
    private $admin;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var Profile
     */
    private $customer;

    /**
     * @var DetailedEvaluation
     */
    private $detailEvaluation;

    private $learnersProgress;

    private $learnersNotation;

    private $nbLearner;

    private $entity;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->modules = new ArrayCollection();
        $this->learners = new ArrayCollection();
        $this->excludeWeeks = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->chapters = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getNbLearner()
    {
        return $this->nbLearner;
    }

    /**
     * @param mixed $nbLearner
     */
    public function setNbLearner($nbLearner): void
    {
        $this->nbLearner = $nbLearner;
    }

    /**
     * @return mixed
     */
    public function getLearnersProgress()
    {
        return $this->learnersProgress;
    }

    /**
     * @param mixed $learnersProgress
     */
    public function setLearnersProgress($learnersProgress): void
    {
        $this->learnersProgress = $learnersProgress;
    }

    /**
     * @return mixed
     */
    public function getLearnersNotation()
    {
        return $this->learnersNotation;
    }

    /**
     * @param mixed $learnersNotation
     */
    public function setLearnersNotation($learnersNotation): void
    {
        $this->learnersNotation = $learnersNotation;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(\ApiBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function removeTag(\ApiBundle\Entity\Tag $tag)
    {
        return $this->tags->removeElement($tag);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Chapter $chapter
     * @return $this
     */
    public function addChapter(\ApiBundle\Entity\Chapter $chapter)
    {
        $this->chapters[] = $chapter;

        return $this;
    }

    /**
     * @param Chapter $chapter
     * @return bool
     */
    public function removeChapter(\ApiBundle\Entity\Chapter $chapter)
    {
        return $this->chapters->removeElement($chapter);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * @param MyFavorite $myFavorite
     * @return $this
     */
    public function addFavorite(\ApiBundle\Entity\MyFavorite $myFavorite)
    {
        $this->myFavorites[] = $myFavorite;

        return $this;
    }

    /**
     * @param MyFavorite $myFavorite
     * @return bool
     */
    public function removeFavorite(\ApiBundle\Entity\MyFavorite $myFavorite)
    {
        return $this->myFavorites->removeElement($myFavorite);
    }


    public function getFavorites()
    {
        return $this->myFavorites;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Intervention
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return Intervention
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set beginning.
     *
     * @param \DateTime $beginning
     *
     * @return Intervention
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;

        return $this;
    }

    /**
     * Get beginning.
     *
     * @return \DateTime
     */
    public function getBeginning()
    {
        return $this->beginning;
    }

    /**
     * Set ending.
     *
     * @param \DateTime $ending
     *
     * @return Intervention
     */
    public function setEnding($ending)
    {
        $this->ending = $ending;

        return $this;
    }

    /**
     * Get ending.
     *
     * @return \DateTime
     */
    public function getEnding()
    {
        return $this->ending;
    }

    /**
     * Set ready.
     *
     * @param int $ready
     *
     * @return Intervention
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready.
     *
     * @return int
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set sequential.
     *
     * @param bool $sequential
     *
     * @return Intervention
     */
    public function setSequential($sequential)
    {
        $this->sequential = $sequential;

        return $this;
    }

    /**
     * Get sequential.
     *
     * @return bool
     */
    public function getSequential()
    {
        return $this->sequential;
    }

    /**
     * Add module.
     *
     * @param Module $module
     *
     * @return Intervention
     */
    public function addModule(Module $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Remove module.
     *
     * @param Module $module
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModule(Module $module)
    {
        return $this->modules->removeElement($module);
    }

    /**
     * Get modules.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Add excludeWeek.
     *
     * @param ExcludeWeeks $excludeWeek
     *
     * @return Intervention
     */
    public function addExcludeWeek(ExcludeWeeks $excludeWeek)
    {
        $this->excludeWeeks[] = $excludeWeek;

        return $this;
    }

    /**
     * Remove excludeWeek.
     *
     * @param ExcludeWeeks $excludeWeek
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeExcludeWeek(ExcludeWeeks $excludeWeek)
    {
        return $this->excludeWeeks->removeElement($excludeWeek);
    }

    /**
     * Get excludeWeeks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcludeWeeks()
    {
        return $this->excludeWeeks;
    }

    /**
     * Add learner.
     *
     * @param LearnerIntervention $learner
     *
     * @return Intervention
     */
    public function addLearner(LearnerIntervention $learner)
    {
        $this->learners[] = $learner;

        return $this;
    }

    /**
     * Remove learner.
     *
     * @param LearnerIntervention $learner
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeLearner(LearnerIntervention $learner)
    {
        return $this->learners->removeElement($learner);
    }

    /**
     * Get learners.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLearners()
    {
        return $this->learners;
    }

    /**
     * Set logo.
     *
     * @param FileDescriptor|null $logo
     *
     * @return Intervention
     */
    public function setLogo(FileDescriptor $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return FileDescriptor|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set priority.
     *
     * @param Priority|null $priority
     *
     * @return Intervention
     */
    public function setPriority(Priority $priority = null)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority.
     *
     * @return Priority|null
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set admin.
     *
     * @param Profile|null $admin
     *
     * @return Intervention
     */
    public function setAdmin(Profile $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return Profile|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set status.
     *
     * @param Status|null $status
     *
     * @return Intervention
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set customer.
     *
     * @param Profile|null $customer
     *
     * @return Intervention
     */
    public function setCustomer(Profile $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer.
     *
     * @return Profile|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set detailEvaluation.
     *
     * @param DetailedEvaluation|null $detailEvaluation
     *
     * @return Intervention
     */
    public function setDetailEvaluation(DetailedEvaluation $detailEvaluation = null)
    {
        $this->detailEvaluation = $detailEvaluation;

        return $this;
    }

    /**
     * Get detailEvaluation.
     *
     * @return DetailedEvaluation|null
     */
    public function getDetailEvaluation()
    {
        return $this->detailEvaluation;
    }

    /**
     * Set openDays.
     *
     * @param string|null $openDays
     *
     * @return Intervention
     */
    public function setOpenDays($openDays = null)
    {
        $this->openDays = $openDays;

        return $this;
    }

    /**
     * Get openDays.
     *
     * @return string|null
     */
    public function getOpenDays()
    {
        return $this->openDays;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Intervention
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Intervention
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     *
     * @return Intervention
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }


}
