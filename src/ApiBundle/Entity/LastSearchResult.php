<?php

namespace ApiBundle\Entity;

/**
 * LastSearchResult.
 */
class LastSearchResult
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $params;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Constructor.
     */
    public function __construct()
    {

    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get params.
     *
     * @return string|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param null $params
     * @return $this
     */
    public function setParams($params = null)
    {
        $this->params = $params;

        return $this;
    }


    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return LastSearchResult
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BestPractice
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BestPractice
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
