<?php

namespace ApiBundle\Entity;

/**
 * OverPriceDelay.
 */
class OverPriceDelay
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $numberDelay;

    /**
     * @var \ApiBundle\Entity\PriceOverRun
     */
    private $priceOverRun;

    /**
     * @var \ApiBundle\Entity\DelayUnit
     */
    private $delayUnit;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberDelay.
     *
     * @param int $numberDelay
     *
     * @return OverPriceDelay
     */
    public function setNumberDelay($numberDelay)
    {
        $this->numberDelay = $numberDelay;

        return $this;
    }

    /**
     * Get numberDelay.
     *
     * @return int
     */
    public function getNumberDelay()
    {
        return $this->numberDelay;
    }

    /**
     * Set priceOverRun.
     *
     * @param \ApiBundle\Entity\PriceOverRun|null $priceOverRun
     *
     * @return OverPriceDelay
     */
    public function setPriceOverRun(\ApiBundle\Entity\PriceOverRun $priceOverRun = null)
    {
        $this->priceOverRun = $priceOverRun;

        return $this;
    }

    /**
     * Get priceOverRun.
     *
     * @return \ApiBundle\Entity\PriceOverRun|null
     */
    public function getPriceOverRun()
    {
        return $this->priceOverRun;
    }

    /**
     * Set delayUnit.
     *
     * @param \ApiBundle\Entity\DelayUnit|null $delayUnit
     *
     * @return OverPriceDelay
     */
    public function setDelayUnit(\ApiBundle\Entity\DelayUnit $delayUnit = null)
    {
        $this->delayUnit = $delayUnit;

        return $this;
    }

    /**
     * Get delayUnit.
     *
     * @return \ApiBundle\Entity\DelayUnit|null
     */
    public function getDelayUnit()
    {
        return $this->delayUnit;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return OverPriceDelay
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return OverPriceDelay
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return OverPriceDelay
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
