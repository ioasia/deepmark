<?php

namespace ApiBundle\Entity;

/**
 * TrainerDocument.
 */
class TrainerDocument
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $file;

    /**
     * @var \ApiBundle\Entity\EducationalDocVariety
     */
    private $educationalDocVariety;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file.
     *
     * @param string $file
     *
     * @return TrainerDocument
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set educationalDocVariety.
     *
     * @param \ApiBundle\Entity\EducationalDocVariety|null $educationalDocVariety
     *
     * @return TrainerDocument
     */
    public function setEducationalDocVariety(\ApiBundle\Entity\EducationalDocVariety $educationalDocVariety = null)
    {
        $this->educationalDocVariety = $educationalDocVariety;

        return $this;
    }

    /**
     * Get educationalDocVariety.
     *
     * @return \ApiBundle\Entity\EducationalDocVariety|null
     */
    public function getEducationalDocVariety()
    {
        return $this->educationalDocVariety;
    }

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $fileDescriptor;

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return TrainerDocument
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set trainer.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return TrainerDocument
     */
    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return TrainerDocument
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return TrainerDocument
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return TrainerDocument
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
