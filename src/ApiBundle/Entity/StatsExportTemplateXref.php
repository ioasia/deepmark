<?php

namespace ApiBundle\Entity;

/**
 * StatsExportTemplateXref.
 */
class StatsExportTemplateXref {

    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\StatsExportTemplate
     */
    private $template;
    
    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $admin;
   

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set template.
     *
     * @param \ApiBundle\Entity\StatsExportTemplate|null $template
     *
     * @return StatsExportTemplate
     */
    public function setTemplate(\ApiBundle\Entity\StatsExportTemplate $template = null) {
        $this->template = $template;

        return $this;
    }
    
    /**
     * Get template.
     *
     * @return int
     */
    public function getTemplate() {
        return $this->template;
    }

    
    /**
     * Set admin.
     *
     * @param \ApiBundle\Entity\Profile|null $admin
     *
     * @return DetailedEvaluation
     */
    public function setAdmin(\ApiBundle\Entity\Profile $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }

}
