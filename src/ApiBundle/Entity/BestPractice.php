<?php

namespace ApiBundle\Entity;

/**
 * BestPractice.
 */
class BestPractice
{
    const STATUS_TODO = 'TODO';
    const STATUS_SUBMITTED = 'SUBMITTED';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    const RESOURCE_TYPE_EXERCISE_TRAINER_ANSWER = 'EXERCISE_TRAINER_ANSWER';
    const RESOURCE_TYPE_MODULE_TRAINER_HAS_DOCUMENT = 'MODULE_TRAINER_HAS_DOCUMENT';
    const RESOURCE_TYPE_MODULE_LEARNER_HAS_DOCUMENT = 'MODULE_LEARNER_HAS_DOCUMENT';
    const RESOURCE_TYPE_QUIZ_LEARNER_ANSWER = 'QUIZ_LEARNER_ANSWER';
    const RESOURCE_TYPE_QUIZ_DEFAULT_CORRECTION = 'QUIZ_DEFAULT_CORRECTION';
    const RESOURCE_TYPE_EXTERNAL = 'EXTERNAL';

    const DOCUMENT_TYPE_VIDEO = 'VIDEO';
    const DOCUMENT_TYPE_AUDIO = 'AUDIO';
    const DOCUMENT_TYPE_DOCUMENT = 'DOCUMENT';
    const DOCUMENT_TYPE_FILE = 'FILE';
    const DOCUMENT_TYPE_URL = 'URL';
    const DOCUMENT_TYPE_TEXT = 'TEXT';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $designation;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Entity
     */
    private $entity;

    /**
     * @var string
     */
    private $status = 'TODO';

    /**
     * @var int
     */
    private $resourceId;

    /**
     * @var int
     */
    private $draft;

    /**
     * @var string
     */
    private $resourceType;

    /**
     * @var string
     */
    private $documentType;

    /**
     * @var string
     */
    private $resourceParam;

    /**
     * @var float
     */
    private $rating;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tags;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fileDescriptors;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bestPracticeResponses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $myFavorites;

    /**
     * @var \DateTime
     */
    private $validateDate;

    /**
     * @var \DateTime
     */
    private $publishDate;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fileDescriptors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(\ApiBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function removeTag(\ApiBundle\Entity\Tag $tag)
    {
        return $this->tags->removeElement($tag);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param FileDescriptor $fileDescriptor
     * @return $this
     */
    public function addFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor)
    {
        $this->fileDescriptors[] = $fileDescriptor;

        return $this;
    }

    /**
     * @param FileDescriptor $fileDescriptor
     * @return bool
     */
    public function removeFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor)
    {
        return $this->fileDescriptors->removeElement($fileDescriptor);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getFileDescriptors()
    {
        $fileDescriptorArray =[];
        foreach ($this->fileDescriptors as $fileDescriptor) {
            $fileDescriptorArray[] = $fileDescriptor;
        }
        if ($fileDescriptorArray) {
            usort($fileDescriptorArray,function($first,$second){
                return $first->getPosition() > $second->getPosition();
            });
        }
        return $fileDescriptorArray;
    }

    public function getNumberOfFileDescriptors()
    {
        $total = 0;
        foreach ($this->fileDescriptors as $item) {
            $total++;
        }
        return $total;
    }

    /**
     * Add bestPracticeResponses.
     *
     * @param \ApiBundle\Entity\BestPracticeResponse $bestPracticeResponse
     *
     * @return BestPractice
     */
    public function addBestPracticeResponse(\ApiBundle\Entity\BestPracticeResponse $bestPracticeResponse)
    {
        $this->bestPracticeResponses[] = $bestPracticeResponse;

        return $this;
    }

    /**
     * Remove bestPracticeResponse.
     *
     * @param \ApiBundle\Entity\BestPracticeResponse $bestPracticeResponse
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBestPracticeResponse(\ApiBundle\Entity\BestPracticeResponse $bestPracticeResponse)
    {
        return $this->bestPracticeResponses->removeElement($bestPracticeResponse);
    }

    /**
     * Get bestPracticeResponses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBestPracticeResponses()
    {
        if ($this->bestPracticeResponses) {
            $voted = 0;
            foreach ($this->bestPracticeResponses as $bestPracticeResponse) {
                $voted ++;
            }
            return ['rating' => round($this->rating, 1), 'voted' => $voted];
        }
        return ['rating' => 0, 'voted' => 0];
    }


    /**
     * @param MyFavorite $myFavorite
     * @return $this
     */
    public function addBestPracticeFavorite(\ApiBundle\Entity\MyFavorite $myFavorite)
    {
        $this->myFavorites[] = $myFavorite;

        return $this;
    }

    /**
     * @param MyFavorite $myFavorite
     * @return bool
     */
    public function removeBestPracticeFavorite(\ApiBundle\Entity\MyFavorite $myFavorite)
    {
        return $this->myFavorites->removeElement($myFavorite);
    }


    public function getBestPracticeFavorites()
    {
        return $this->myFavorites;
    }

    /**
     * Set designation.
     *
     * @param string|null $designation
     *
     * @return BestPractice
     */
    public function setDesignation($designation = null)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string|null
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return BestPractice
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return BestPractice
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return BestPractice
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return BestPractice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set entity.
     *
     * @param \ApiBundle\Entity\Entity|null $entity
     *
     * @return BestPractice
     */
    public function setEntity(\ApiBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return \ApiBundle\Entity\Entity|null
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return int
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * @param int $resourceId
     */
    public function setResourceId(int $resourceId): void
    {
        $this->resourceId = $resourceId;
    }

    /**
     * @return int
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * @param int $draft
     */
    public function setDraft(int $draft): void
    {
        $this->draft = $draft;
    }

    /**
     * Set resourceType.
     *
     * @param string|null $resourceType
     *
     * @return BestPractice
     */
    public function setResourceType($resourceType = null)
    {
        $this->resourceType = $resourceType;

        return $this;
    }

    /**
     * Get resourceType.
     *
     * @return string|null
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }

    /**
     * Set documentType.
     *
     * @param string|null $documentType
     *
     * @return BestPractice
     */
    public function setDocumentType($documentType = null)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType.
     *
     * @return string|null
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Set resourceParam.
     *
     * @param string|null $resourceParam
     *
     * @return BestPractice
     */
    public function setResourceParam($resourceParam = null)
    {
        $this->resourceParam = $resourceParam;

        return $this;
    }

    /**
     * Get resourceParam.
     *
     * @return string|null
     */
    public function getResourceParam()
    {
        return $this->resourceParam;
    }

    /**
     * @param $publish
     * @return $this
     */
    public function setPublishDate($publish)
    {
        $this->publishDate = $publish;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidateDate()
    {
        return $this->validateDate;
    }

    /**
     * @param $validate
     * @return $this
     */
    public function setValidateDate($validate)
    {
        $this->validateDate = $validate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return BestPractice
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BestPractice
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BestPractice
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
