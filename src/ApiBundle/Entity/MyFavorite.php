<?php

namespace ApiBundle\Entity;

/**
 * MyFavorite.
 */
class MyFavorite
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\BestPractice
     */
    private $bestPractice;

    /**
     * @var \ApiBundle\Entity\Intervention
     */
    private $intervention;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return MyFavorite
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return MyFavorite
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set bestPractice.
     *
     * @param \ApiBundle\Entity\BestPractice|null $bestPractice
     *
     * @return MyFavorite
     */
    public function setBestPractice(\ApiBundle\Entity\BestPractice $bestPractice = null)
    {
        $this->bestPractice = $bestPractice;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\BestPractice|null
     */
    public function getBestPractice()
    {
        return $this->bestPractice;
    }

    /**
     * Set intervention.
     *
     * @param \ApiBundle\Entity\Intervention|null $intervention
     *
     * @return MyFavorite
     */
    public function setIntervention(\ApiBundle\Entity\Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get Intervention.
     *
     * @return \ApiBundle\Entity\Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return MyFavorite
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return MyFavorite
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
