<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity.
 */
class Entity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string|null
     */
    private $expansionStreet;

    /**
     * @var string|null
     */
    private $postalCode;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $country;

    /**
     * @var \ApiBundle\Entity\Organisation
     */
    private $organisation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $regionals;

    /**
     * @var string|null
     */
    private $logo;

    /**
     * @var string|null
     */
    private $signature;

    /**
     * @var string
     */
    private $workingHours;

    /**
     * @var string|null
     */
    private $socialReason;

    /**
     * @var string|null
     */
    private $siret;

    /**
     * @var string|null
     */
    private $declaration;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->regionals = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add regional.
     *
     * @param \ApiBundle\Entity\Regional $regional
     *
     * @return Entity
     */
    public function addRegional(\ApiBundle\Entity\Regional $regional)
    {
        $this->regionals[] = $regional;

        return $this;
    }

    /**
     * Remove regional.
     *
     * @param \ApiBundle\Entity\Regional $regional
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRegional(\ApiBundle\Entity\Regional $regional)
    {
        return $this->regionals->removeElement($regional);
    }

    /**
     * Get regionals.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegionals()
    {
        return $this->regionals;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Entity
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Entity
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return Entity
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return Entity
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Set expansionStreet.
     *
     * @param string|null $expansionStreet
     *
     * @return Entity
     */
    public function setExpansionStreet($expansionStreet = null)
    {
        $this->expansionStreet = $expansionStreet;

        return $this;
    }

    /**
     * Get expansionStreet.
     *
     * @return string|null
     */
    public function getExpansionStreet()
    {
        return $this->expansionStreet;
    }

    /**
     * Set organisation.
     *
     * @param \ApiBundle\Entity\Organisation|null $organisation
     *
     * @return Entity
     */
    public function setOrganisation(\ApiBundle\Entity\Organisation $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation.
     *
     * @return \ApiBundle\Entity\Organisation|null
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function __toString()
    {
        $name = '';
        if ($this->getOrganisation()) {
            $name .= $this->getOrganisation()->getDesignation().' ';
        }
        if ($this->getDesignation()) {
            $name .= $this->getDesignation();
        }

        return $name;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Entity
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Entity
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string|null $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string|null
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string|null $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set workingHours.
     *
     * @param string $workingHours
     *
     * @return Organisation
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = serialize($workingHours);

        return $this;
    }

    /**
     * Get workingHours.
     *
     * @return array
     */
    public function getWorkingHours()
    {
        return unserialize($this->workingHours);
    }

    /**
     * @return string|null
     */
    public function getSocialReason()
    {
        return $this->socialReason;
    }

    /**
     * @param string|null $socialReason
     */
    public function setSocialReason($socialReason)
    {
        $this->socialReason = $socialReason;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string|null $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeclaration()
    {
        return $this->declaration;
    }

    /**
     * @param string|null $declaration
     */
    public function setDeclaration($declaration)
    {
        $this->declaration = $declaration;
        return $this;
    }


}
