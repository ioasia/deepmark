<?php

namespace ApiBundle\Entity;

/**
 * ModuleSend
 */
class ModuleSend
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group.
     *
     * @param Module|null $module
     *
     * @return ModuleSend
     */
    public function setModule(Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set profile.
     *
     * @param Profile|null $profile
     *
     * @return ModuleSend
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return ModuleSend
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleSend
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return LearnerGroup
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
