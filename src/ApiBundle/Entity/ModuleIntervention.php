<?php

namespace ApiBundle\Entity;

/**
 * ModuleIntervention.
 */
class ModuleIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantity = '1';

    /**
     * @var \ApiBundle\Entity\Intervention
     */
    private $intervention;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return ModuleIntervention
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set intervention.
     *
     * @param \ApiBundle\Entity\Intervention|null $intervention
     *
     * @return ModuleIntervention
     */
    public function setIntervention(\ApiBundle\Entity\Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Get intervention.
     *
     * @return \ApiBundle\Entity\Intervention|null
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleIntervention
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleIntervention
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleIntervention
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
