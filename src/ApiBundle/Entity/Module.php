<?php

namespace ApiBundle\Entity;

use DateTime;

class Module
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $designation;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var int
     */
    private $isNeedScoreToPass;

    /**
     * @var int
     */
    private $isPersonalBooking;

    /**
     * @var int
     */
    private $isNeedReport;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int|null
     */
    private $participantNumber;

    /**
     * @var int|null
     */
    private $minScore;

    /**
     * @var int|null
     */
    private $bestPracticeScore;


    /**
     * @var int|null
     */
    private $signature;

    /**
     * @var int|null
     */
    private $signatureNumber;

    /**
     * @var int|null
     */
    private $signatureAfternoon;

    /**
     * @var int|null
     */
    private $participantMinNumber;

    /**
     * @var int|null
     */
    private $participantMaxNumber;

    /**
     * @var DateTime
     */
    private $beginning;

    /**
     * @var DateTime
     */
    private $ending;

    /**
     * @var bool|null
     */
    private $countBeginning = '0';

    /**
     * @var bool|null
     */
    private $countEnding = '0';

    /**
     * @var int
     */
    private $iteration;

    /**
     * @var int
     */
    private $validatingScore;

    /**
     * @var DateTime
     */
    private $duration;
    
    /**
     * @var int
     */
    private $durationActive;
    
    /**
     * @var int
     */
    private $correctionDuration;

    /**
     * @var int
     */
    private $correctionDurationDays;

    /**
     * @var DateTime
     */
    private $correctionDurationMax;
    
    /**
     * @var int
     */
    private $poolTrainers;
    
    /**
     * @var int
     */
    private $showCorrection;

    /**
     * @var bool
     */
    private $requireReport;

    /**
     * @var bool
     */
    private $sendParticipant;

    /**
     * @var bool
     */
    private $sendManager;

    /**
     * @var bool
     */
    private $sendManagerOther;

    /**
     * @var bool
     */
    private $cumulativeSkill;

    /**
     * @var int|null
     */
    private $locationProcess;

    /**
     * @var DateTime
     */
    private $conceptionDate;

    /**
     * @var string|null
     */
    private $eLearningObs;

    /**
     * @var DateTime|null
     */
    private $dateAtelier;

    /**
     * @var int|null
     */
    private $typeQuiz;
    
    /**
     * @var int|null
     */
    private $scoreQuiz;

    /**
     * @var string|null
     */
    private $externalLinkQuiz;

    /**
     * @var string|null
     */
    private $snapshotQuiz;

    /**
     * @var bool|null
     */
    private $freeRegistration;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assigments;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assessments;

    /**
     * @var \ApiBundle\Entity\EducationalDocument
     */
    private $mediaDocument;

    /**
     * @var Intervention
     */
    private $intervention;

    /**
     * @var \ApiBundle\Entity\Subject
     */
    private $subject;

    /**
     * @var \ApiBundle\Entity\LiveResourceOrigin
     */
    private $liveResourceOrigin;

    /**
     * @var \ApiBundle\Entity\AttendanceAttestation
     */
    private $attendanceAttestation;

    /**
     * @var \ApiBundle\Entity\Report
     */
    private $report;

    /**
     * @var \ApiBundle\Entity\Status
     */
    private $status;

    /**
     * @var \ApiBundle\Entity\DetailedEvaluation
     */
    private $detailEvaluation;

    /**
     * @var \ApiBundle\Entity\StoredModule
     */
    private $storedModule;

    /**
     * @var \ApiBundle\Entity\PriceVariety
     */
    private $priceVariety;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $educationalDocument;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $trainerDocument;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleTemplates;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $skills;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sessions;

    /**
     * @var bool
     */
    private $done;

    /**
     * @var bool
     */
    private $isBooked;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quiz;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleResponses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleTrainerResponses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleNotes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attendanceReports;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $echoModule;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleIterations;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bookingAgenda;


    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;
    
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    public $played;

    /**
     * @var text|null
     */

    private $signatureParameters;

    /**
     * @var int|null
     */

    private $signatureMethod;

    /**
     * @return int
     */
    public function getSignatureMethod()
    {
        return $this->signatureMethod;
    }

    /**
     * @param int $signatureMethod
     * @return Module
     */
    public function setSignatureMethod($signatureMethod)
    {
        $this->signatureMethod = $signatureMethod;
        return $this;
    }

    /**
     * @return int
     */
    public function getisNeedScoreToPass()
    {
        return $this->isNeedScoreToPass;
    }

    /**
     * @param int $isNeedScoreToPass
     * @return Module
     */
    public function setIsNeedScoreToPass($isNeedScoreToPass)
    {
        $this->isNeedScoreToPass = $isNeedScoreToPass;
        return $this;

    }

    /**
     * @return int
     */
    public function getIsNeedReport()
    {
        return $this->isNeedReport;
    }

    /**
     * @param int $isNeedReport
     * @return Module
     */
    public function setIsNeedReport($isNeedReport)
    {
        $this->isNeedReport = $isNeedReport;
        return $this;

    }

    /**
     * @return int
     */
    public function getIsPersonalBooking()
    {
        return $this->isPersonalBooking;
    }

    /**
     * @param int $isPersonalBooking
     * @return Module
     */
    public function setIsPersonalBooking($isPersonalBooking)
    {
        $this->isPersonalBooking = $isPersonalBooking;
        return $this;
    }

    /**
     * Set done.
     *
     * @param bool|null $isBooked
     *
     * @return Module
     */
    public function setIsBooked($isBooked = null)
    {
        $this->isBooked = $isBooked;

        return $this;
    }

    /**
     * Get done.
     *
     * @return bool|null
     */
    public function isBooked()
    {
        return $this->isBooked;
    }

    /**
     * Set done.
     *
     * @param bool|null $done
     *
     * @return Module
     */
    public function setDone($done = null)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done.
     *
     * @return bool|null
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->assigments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->assesments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->educationalDocument = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trainerDocument = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sessions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->moduleTemplates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->played = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setInProgressParam()
    {
        $this->setIteration(0);
        $this->setValidatingScore(0);
        //$this->setParcoursSequentiel(false);
        $this->setRequireReport(false);
        $this->setSendParticipant(false);
        $this->setSendManager(false);
        $this->setSendManagerOther(false);
        $this->setCumulativeSkill(0);
        // $this->setSendParticipantDelay(new DateTime());
        $this->setConceptionDate(new DateTime());
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string|null $designation
     *
     * @return Module
     */
    public function setDesignation($designation = null)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string|null
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Module
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return Module
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return Module
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get signature.
     *
     * @return int|null
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set signature.
     *
     * @param int|null $signature
     *
     * @return Module
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Set signatureNumber.
     *
     * @param int|null $signatureNumber
     *
     * @return Module
     */
    public function setSignatureNumber($signatureNumber = null)
    {
        $this->signatureNumber = $signatureNumber;

        return $this;
    }

    /**
     * Get signatureNumber.
     *
     * @return int|null
     */
    public function getSignatureNumber()
    {
        return $this->signatureNumber;
    }

    /**
     * @return int|null
     */
    public function getMinScore()
    {
        return $this->minScore;
    }

    /**
     * @param int|null $minScore
     */
    public function setMinScore($minScore)
    {
        $this->minScore = $minScore;
    }

    /**
     * @return int|null
     */
    public function getBestPracticeScore()
    {
        return $this->bestPracticeScore;
    }

    /**
     * @param int|null $bestPracticeScore
     */
    public function setBestPracticeScore($bestPracticeScore)
    {
        $this->bestPracticeScore = $bestPracticeScore;
    }


    /**
     * @return int|null
     */
    public function getSignatureAfternoon()
    {
        return $this->signatureAfternoon;
    }

    /**
     * @param int|null $signatureAfternoon
     */
    public function setSignatureAfternoon($signatureAfternoon)
    {
        $this->signatureAfternoon = $signatureAfternoon;
    }

    /**
     * Set participantNumber.
     *
     * @param int|null $participantNumber
     *
     * @return Module
     */
    public function setParticipantNumber($participantNumber = null)
    {
        $this->participantNumber = $participantNumber;

        return $this;
    }

    /**
     * Get participantNumber.
     *
     * @return int|null
     */
    public function getParticipantNumber()
    {
        return $this->participantNumber;
    }

    /**
     * Set participantMinNumber.
     *
     * @param int|null $participantMinNumber
     *
     * @return Module
     */
    public function setParticipantMinNumber($participantMinNumber = null)
    {
        $this->participantMinNumber = $participantMinNumber;

        return $this;
    }

    /**
     * Get participantMinNumber.
     *
     * @return int|null
     */
    public function getParticipantMinNumber()
    {
        return $this->participantMinNumber;
    }

    /**
     * Set participantMaxNumber.
     *
     * @param int|null $participantMaxNumber
     *
     * @return Module
     */
    public function setParticipantMaxNumber($participantMaxNumber = null)
    {
        $this->participantMaxNumber = $participantMaxNumber;

        return $this;
    }

    /**
     * Get participantMaxNumber.
     *
     * @return int|null
     */
    public function getParticipantMaxNumber()
    {
        return $this->participantMaxNumber;
    }

    /**
     * Set beginning.
     *
     * @param DateTime $beginning
     *
     * @return Module
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;

        return $this;
    }

    /**
     * Get beginning.
     *
     * @return DateTime
     */
    public function getBeginning()
    {
        return $this->beginning;
    }

    public function getBeginningWithTime(): DateTime
    {
        $endingWithTime = clone $this->getBeginning();
        $endingWithTime->setTime(0, 0, 0);

        return $endingWithTime;
    }

    /**
     * Set ending.
     *
     * @param DateTime $ending
     *
     * @return Module
     */
    public function setEnding($ending)
    {
        $this->ending = $ending;

        return $this;
    }

    /**
     * Get ending.
     *
     * @return DateTime
     */
    public function getEnding()
    {
        return $this->ending;
    }

    public function getEndingWithTime(): DateTime
    {
        $endingWithTime = clone $this->getEnding();
        $endingWithTime->setTime(23, 59, 59);

        return $endingWithTime;
    }

    /**
     * Set countBeginning.
     *
     * @param bool|null $countBeginning
     *
     * @return Module
     */
    public function setCountBeginning($countBeginning = null)
    {
        $this->countBeginning = $countBeginning;

        return $this;
    }

    /**
     * Get countBeginning.
     *
     * @return bool|null
     */
    public function getCountBeginning()
    {
        return $this->countBeginning;
    }

    /**
     * Set countEnding.
     *
     * @param bool|null $countEnding
     *
     * @return Module
     */
    public function setCountEnding($countEnding = null)
    {
        $this->countEnding = $countEnding;

        return $this;
    }

    /**
     * Get countEnding.
     *
     * @return bool|null
     */
    public function getCountEnding()
    {
        return $this->countEnding;
    }

    /**
     * Set iteration.
     *
     * @param int $iteration
     *
     * @return Module
     */
    public function setIteration($iteration)
    {
        $this->iteration = $iteration;

        return $this;
    }

    /**
     * Get iteration.
     *
     * @return int
     */
    public function getIteration()
    {
        return $this->iteration;
    }

    /**
     * Set validatingScore.
     *
     * @param int $validatingScore
     *
     * @return Module
     */
    public function setValidatingScore($validatingScore)
    {
        $this->validatingScore = $validatingScore;

        return $this;
    }

    /**
     * Get validatingScore.
     *
     * @return int
     */
    public function getValidatingScore()
    {
        return $this->validatingScore;
    }

    /**
     * Set duration.
     *
     * @param DateTime $duration
     *
     * @return Module
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return DateTime
     */
    public function getDuration()
    {
        return $this->duration;
    }
    
    /**
     * Set durationActive.
     *
     * @param int $durationActive
     *
     * @return Module
     */
    public function setDurationActive($durationActive)
    {
        $this->durationActive = $durationActive;

        return $this;
    }

    /**
     * Get durationActive.
     *
     * @return int
     */
    public function getDurationActive()
    {
        return $this->durationActive;
    }
    
     /**
     * Set correctionDuration.
     *
     * @param int $correctionDuration
     *
     * @return Module
     */
    public function setCorrectionDuration($correctionDuration)
    {
        $this->correctionDuration = $correctionDuration;

        return $this;
    }

    /**
     * Get correctionDuration.
     *
     * @return int
     */
    public function getCorrectionDuration()
    {
        return $this->correctionDuration;
    }

    /**
     * Set correctionDurationDays.
     *
     * @param int $correctionDurationDays
     *
     * @return Module
     */
    public function setCorrectionDurationDays($correctionDurationDays)
    {
        $this->correctionDurationDays = $correctionDurationDays;

        return $this;
    }

    /**
     * Get correctionDurationDays.
     *
     * @return int
     */
    public function getCorrectionDurationDays()
    {
        return $this->correctionDurationDays;
    }
    
     /**
     * Set correctionDurationMax.
     *
     * @param DateTime $correctionDurationMax
     *
     * @return Module
     */
    public function setCorrectionDurationMax($correctionDurationMax)
    {
        $this->correctionDurationMax = $correctionDurationMax;

        return $this;
    }

    /**
     * Get correctionDurationMax.
     *
     * @return DateTime
     */
    public function getCorrectionDurationMax()
    {
        return $this->correctionDurationMax;
    }
    
     /**
     * Set poolTrainers.
     *
     * @param int $poolTrainers
     *
     * @return Module
     */
    public function setPoolTrainers($poolTrainers)
    {
        $this->poolTrainers = $poolTrainers;

        return $this;
    }

    /**
     * Get poolTrainers.
     *
     * @return int
     */
    public function getPoolTrainers()
    {
        return $this->poolTrainers;
    }
    
     /**
     * Set showCorrection.
     *
     * @param int $showCorrection
     *
     * @return Module
     */
    public function setShowCorrection($showCorrection)
    {
        $this->showCorrection = $showCorrection;

        return $this;
    }

    /**
     * Get showCorrection.
     *
     * @return int
     */
    public function getShowCorrection()
    {
        return $this->showCorrection;
    }

    /**
     * Set requireReport.
     *
     * @param bool $requireReport
     *
     * @return Module
     */
    public function setRequireReport($requireReport)
    {
        $this->requireReport = $requireReport;

        return $this;
    }

    /**
     * Get requireReport.
     *
     * @return bool
     */
    public function getRequireReport()
    {
        return $this->requireReport;
    }

    /**
     * Set sendParticipant.
     *
     * @param bool $sendParticipant
     *
     * @return Module
     */
    public function setSendParticipant($sendParticipant)
    {
        $this->sendParticipant = $sendParticipant;

        return $this;
    }

    /**
     * Get sendParticipant.
     *
     * @return bool
     */
    public function getSendParticipant()
    {
        return $this->sendParticipant;
    }

    /**
     * Set sendManager.
     *
     * @param bool $sendManager
     *
     * @return Module
     */
    public function setSendManager($sendManager)
    {
        $this->sendManager = $sendManager;

        return $this;
    }

    /**
     * Get sendManager.
     *
     * @return bool
     */
    public function getSendManager()
    {
        return $this->sendManager;
    }

    /**
     * Set sendManagerOther.
     *
     * @param bool $sendManagerOther
     *
     * @return Module
     */
    public function setSendManagerOther($sendManagerOther)
    {
        $this->sendManagerOther = $sendManagerOther;

        return $this;
    }

    /**
     * Get sendManagerOther.
     *
     * @return bool
     */
    public function getSendManagerOther()
    {
        return $this->sendManagerOther;
    }

    /**
     * Set cumulativeSkill.
     *
     * @param bool $cumulativeSkill
     *
     * @return Module
     */
    public function setCumulativeSkill($cumulativeSkill)
    {
        $this->cumulativeSkill = $cumulativeSkill;

        return $this;
    }

    /**
     * Get cumulativeSkill.
     *
     * @return bool
     */
    public function getCumulativeSkill()
    {
        return $this->cumulativeSkill;
    }

    /**
     * Set locationProcess.
     *
     * @param int|null $locationProcess
     *
     * @return Module
     */
    public function setLocationProcess($locationProcess = null)
    {
        $this->locationProcess = $locationProcess;

        return $this;
    }

    /**
     * Get locationProcess.
     *
     * @return int|null
     */
    public function getLocationProcess()
    {
        return $this->locationProcess;
    }

    /**
     * Set conceptionDate.
     *
     * @param DateTime $conceptionDate
     *
     * @return Module
     */
    public function setConceptionDate($conceptionDate)
    {
        $this->conceptionDate = $conceptionDate;

        return $this;
    }

    /**
     * Get conceptionDate.
     *
     * @return DateTime
     */
    public function getConceptionDate()
    {
        return $this->conceptionDate;
    }

    /**
     * Set eLearningObs.
     *
     * @param string|null $eLearningObs
     *
     * @return Module
     */
    public function setELearningObs($eLearningObs = null)
    {
        $this->eLearningObs = $eLearningObs;

        return $this;
    }

    /**
     * Get eLearningObs.
     *
     * @return string|null
     */
    public function getELearningObs()
    {
        return $this->eLearningObs;
    }

    /**
     * Set dateAtelier.
     *
     * @param DateTime|null $dateAtelier
     *
     * @return Module
     */
    public function setDateAtelier($dateAtelier = null)
    {
        $this->dateAtelier = $dateAtelier;

        return $this;
    }

    /**
     * Get dateAtelier.
     *
     * @return DateTime|null
     */
    public function getDateAtelier()
    {
        return $this->dateAtelier;
    }

    /**
     * Set typeQuiz.
     *
     * @param int|null $typeQuiz
     *
     * @return Module
     */
    public function setTypeQuiz($typeQuiz = null)
    {
        $this->typeQuiz = $typeQuiz;

        return $this;
    }

    /**
     * Get typeQuiz.
     *
     * @return int|null
     */
    public function getTypeQuiz()
    {
        return $this->typeQuiz;
    }
    
    /**
     * Set scoreQuiz.
     *
     * @param int|null $scoreQuiz
     *
     * @return Module
     */
    public function setScoreQuiz($scoreQuiz = null)
    {
        $this->scoreQuiz = $scoreQuiz;

        return $this;
    }

    /**
     * Get scoreQuiz.
     *
     * @return int|null
     */
    public function getScoreQuiz()
    {
        return $this->scoreQuiz;
    }

    /**
     * Set externalLinkQuiz.
     *
     * @param string|null $externalLinkQuiz
     *
     * @return Module
     */
    public function setExternalLinkQuiz($externalLinkQuiz = null)
    {
        $this->externalLinkQuiz = $externalLinkQuiz;

        return $this;
    }

    /**
     * Get externalLinkQuiz.
     *
     * @return string|null
     */
    public function getExternalLinkQuiz()
    {
        return $this->externalLinkQuiz;
    }

    /**
     * Set snapshotQuiz.
     *
     * @param string|null $snapshotQuiz
     *
     * @return Module
     */
    public function setSnapshotQuiz($snapshotQuiz = null)
    {
        $this->snapshotQuiz = $snapshotQuiz;

        return $this;
    }

    /**
     * Get snapshotQuiz.
     *
     * @return string|null
     */
    public function getSnapshotQuiz()
    {
        return $this->snapshotQuiz;
    }

    /**
     * Set freeRegistration.
     *
     * @param bool|null $freeRegistration
     *
     * @return Module
     */
    public function setFreeRegistration($freeRegistration = null)
    {
        $this->freeRegistration = $freeRegistration;

        return $this;
    }

    /**
     * Get freeRegistration.
     *
     * @return bool|null
     */
    public function getFreeRegistration()
    {
        return $this->freeRegistration;
    }

    /**
     * Add assigment.
     *
     * @param \ApiBundle\Entity\AssigmentResourceSystem $assigment
     *
     * @return Module
     */
    public function addAssigment(\ApiBundle\Entity\AssigmentResourceSystem $assigment)
    {
        $this->assigments[] = $assigment;

        return $this;
    }

    /**
     * Remove assigment.
     *
     * @param \ApiBundle\Entity\AssigmentResourceSystem $assigment
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAssigment(\ApiBundle\Entity\AssigmentResourceSystem $assigment)
    {
        return $this->assigments->removeElement($assigment);
    }

    /**
     * Get assigments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssigments()
    {
        return $this->assigments;
    }

    /**
     * Add assessment.
     *
     * @param \ApiBundle\Entity\ModuleAssessments $assessment
     *
     * @return Module
     */
    public function addAssessment(\ApiBundle\Entity\ModuleAssessments $assessment)
    {
        $this->assessments[] = $assessment;

        return $this;
    }

    /**
     * Remove assessment.
     *
     * @param \ApiBundle\Entity\ModuleAssessments $assessment
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAssessment(\ApiBundle\Entity\ModuleAssessments $assessment)
    {
        return $this->assessments->removeElement($assessment);
    }

    /**
     * Get assessment.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssessments()
    {
        return $this->assessments;
    }

    /**
     * Add moduleTemplates.
     *
     * @param \ApiBundle\Entity\AssigmentResourceSystem $template
     *
     * @return Module
     */
    public function addModuleTemplates(ModuleTemplate $template)
    {
        $this->moduleTemplates[] = $template;

        return $this;
    }

    /**
     * Remove moduleTemplates.
     *
     * @param \ApiBundle\Entity\ModuleTemplate $template
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleTemplates(ModuleTemplate $template)
    {
        return $this->moduleTemplates->removeElement($template);
    }

    /**
     * Get moduleTemplates.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleTemplates()
    {
        return $this->moduleTemplates;
    }

    /**
     * Add played.
     *
     * @param \ApiBundle\Entity\QuizUsersPlayed $played
     *
     * @return Module
     */
    public function addPlayed(QuizUsersPlayed $played)
    {
        $this->played[] = $played;

        return $this;
    }

    /**
     * Remove played.
     *
     * @param \ApiBundle\Entity\QuizUsersPlayed $played
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removePlayed(QuizUsersPlayed $played)
    {
        return $this->played->removeElement($played);
    }

    /**
     * Get played.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayed()
    {
        return $this->played;
    }

    /**
     * Set mediaDocument.
     *
     * @param \ApiBundle\Entity\EducationalDocument|null $mediaDocument
     *
     * @return Module
     */
    public function setMediaDocument(\ApiBundle\Entity\EducationalDocument $mediaDocument = null)
    {
        $this->mediaDocument = $mediaDocument;

        return $this;
    }

    /**
     * Get mediaDocument.
     *
     * @return \ApiBundle\Entity\EducationalDocument|null
     */
    public function getMediaDocument()
    {
        return $this->mediaDocument;
    }

    /**
     * Set intervention.
     *
     * @param Intervention|null $intervention
     *
     * @return Module
     */
    public function setIntervention(Intervention $intervention = null)
    {
        $this->intervention = $intervention;

        return $this;
    }

    public function getIntervention(): Intervention
    {
        return $this->intervention;
    }

    /**
     * Set subject.
     *
     * @param \ApiBundle\Entity\Subject|null $subject
     *
     * @return Module
     */
    public function setSubject(\ApiBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return \ApiBundle\Entity\Subject|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set liveResourceOrigin.
     *
     * @param \ApiBundle\Entity\LiveResourceOrigin|null $liveResourceOrigin
     *
     * @return Module
     */
    public function setLiveResourceOrigin(\ApiBundle\Entity\LiveResourceOrigin $liveResourceOrigin = null)
    {
        $this->liveResourceOrigin = $liveResourceOrigin;

        return $this;
    }

    /**
     * Get liveResourceOrigin.
     *
     * @return \ApiBundle\Entity\LiveResourceOrigin|null
     */
    public function getLiveResourceOrigin()
    {
        return $this->liveResourceOrigin;
    }

    /**
     * Set attendanceAttestation.
     *
     * @param \ApiBundle\Entity\AttendanceAttestation|null $attendanceAttestation
     *
     * @return Module
     */
    public function setAttendanceAttestation(\ApiBundle\Entity\AttendanceAttestation $attendanceAttestation = null)
    {
        $this->attendanceAttestation = $attendanceAttestation;

        return $this;
    }

    /**
     * Get attendanceAttestation.
     *
     * @return \ApiBundle\Entity\AttendanceAttestation|null
     */
    public function getAttendanceAttestation()
    {
        return $this->attendanceAttestation;
    }

    /**
     * Set report.
     *
     * @param \ApiBundle\Entity\Report|null $report
     *
     * @return Module
     */
    public function setReport(\ApiBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report.
     *
     * @return \ApiBundle\Entity\Report|null
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set status.
     *
     * @param \ApiBundle\Entity\Status|null $status
     *
     * @return Module
     */
    public function setStatus(\ApiBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \ApiBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set detailEvaluation.
     *
     * @param \ApiBundle\Entity\DetailedEvaluation|null $detailEvaluation
     *
     * @return Module
     */
    public function setDetailEvaluation(\ApiBundle\Entity\DetailedEvaluation $detailEvaluation = null)
    {
        $this->detailEvaluation = $detailEvaluation;

        return $this;
    }

    /**
     * Get detailEvaluation.
     *
     * @return \ApiBundle\Entity\DetailedEvaluation|null
     */
    public function getDetailEvaluation()
    {
        return $this->detailEvaluation;
    }

    /**
     * Set storedModule.
     *
     * @param \ApiBundle\Entity\StoredModule|null $storedModule
     *
     * @return Module
     */
    public function setStoredModule(\ApiBundle\Entity\StoredModule $storedModule = null)
    {
        $this->storedModule = $storedModule;

        return $this;
    }

    /**
     * @return StoredModule
     */
    public function getStoredModule(): StoredModule
    {
        return $this->storedModule;
    }

    /**
     * Set priceVariety.
     *
     * @param \ApiBundle\Entity\PriceVariety|null $priceVariety
     *
     * @return Module
     */
    public function setPriceVariety(\ApiBundle\Entity\PriceVariety $priceVariety = null)
    {
        $this->priceVariety = $priceVariety;

        return $this;
    }

    /**
     * Get priceVariety.
     *
     * @return \ApiBundle\Entity\PriceVariety|null
     */
    public function getPriceVariety()
    {
        return $this->priceVariety;
    }

    /**
     * Add educationalDocument.
     *
     * @param \ApiBundle\Entity\EducationalDocument $educationalDocument
     *
     * @return Module
     */
    public function addEducationalDocument(\ApiBundle\Entity\EducationalDocument $educationalDocument)
    {
        $this->educationalDocument[] = $educationalDocument;

        return $this;
    }

    /**
     * Remove educationalDocument.
     *
     * @param \ApiBundle\Entity\EducationalDocument $educationalDocument
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeEducationalDocument(\ApiBundle\Entity\EducationalDocument $educationalDocument)
    {
        return $this->educationalDocument->removeElement($educationalDocument);
    }

    /**
     * Get educationalDocument.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEducationalDocument()
    {
        return $this->educationalDocument;
    }

    /**
     * Add trainerDocument.
     *
     * @param \ApiBundle\Entity\EducationalDocument $trainerDocument
     *
     * @return Module
     */
    public function addTrainerDocument(\ApiBundle\Entity\EducationalDocument $trainerDocument)
    {
        $this->trainerDocument[] = $trainerDocument;

        return $this;
    }

    /**
     * Remove trainerDocument.
     *
     * @param \ApiBundle\Entity\EducationalDocument $trainerDocument
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTrainerDocument(\ApiBundle\Entity\EducationalDocument $trainerDocument)
    {
        return $this->trainerDocument->removeElement($trainerDocument);
    }

    /**
     * Get trainerDocument.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainerDocument()
    {
        return $this->trainerDocument;
    }

    /**
     * Add skill.
     *
     * @param \ApiBundle\Entity\Skill $skill
     *
     * @return Module
     */
    public function addSkill(\ApiBundle\Entity\Skill $skill)
    {
        $this->skills[] = $skill;

        return $this;
    }

    /**
     * Remove skill.
     *
     * @param \ApiBundle\Entity\Skill $skill
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeSkill(\ApiBundle\Entity\Skill $skill)
    {
        return $this->skills->removeElement($skill);
    }

    /**
     * Get skills.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Add quiz.
     *
     * @param \ApiBundle\Entity\Quizes $quiz
     *
     * @return Module
     */
    public function addQuiz(\ApiBundle\Entity\Quizes $quiz)
    {
        $this->quiz[] = $quiz;
    }

    /**
     * Remove quiz.
     *
     * @param \ApiBundle\Entity\Quizes $quiz
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeQuiz(\ApiBundle\Entity\Quizes $quiz)
    {
        return $this->quiz->removeElement($quiz);
    }

    /**
     * Get quiz.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Set quiz.
     *
     * @param \ApiBundle\Entity\Quiz|null $quizes
     *
     * @return Module
     */
    public function setQuiz(\ApiBundle\Entity\Quizes $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Add echoModule.
     *
     * @param \ApiBundle\Entity\EchoModule $echoModule
     *
     * @return Module
     */
    public function addEchoModule(\ApiBundle\Entity\EchoModule $echoModule)
    {
        $this->echoModule[] = $echoModule;
    }

    /**
     * Remove echoModule.
     *
     * @param \ApiBundle\Entity\EchoModule $echoModule
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeEchoModule(\ApiBundle\Entity\EchoModule $echoModule)
    {
        return $this->echoModule->removeElement($echoModule);
    }

    /**
     * Get echoModule.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEchoModule()
    {
        return $this->echoModule;
    }

    /**
     * Set echoModule.
     *
     * @param \ApiBundle\Entity\EchoModule|null $echoModule
     *
     * @return Module
     */
    public function setEchoModule(\ApiBundle\Entity\EchoModule $echoModule = null)
    {
        $this->echoModule = $echoModule;

        return $this;
    }

    /**
     * Add session.
     *
     * @param \ApiBundle\Entity\ModuleSession $session
     *
     * @return Module
     */
    public function addSession(\ApiBundle\Entity\ModuleSession $session)
    {
        $this->sessions[] = $session;

        return $this;
    }

    /**
     * Remove session.
     *
     * @param \ApiBundle\Entity\ModuleSession $session
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeSession(\ApiBundle\Entity\ModuleSession $session)
    {
        return $this->sessions->removeElement($session);
    }

    /**
     * Get sessions.
     *
     * @return ModuleSession[]
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Add moduleIteration.
     *
     * @param \ApiBundle\Entity\ModuleIteration $moduleIteration
     *
     * @return Module
     */
    public function addModuleIteration(\ApiBundle\Entity\ModuleIteration $moduleIteration)
    {
        $this->moduleIterations[] = $moduleIteration;

        return $this;
    }

    /**
     * Remove moduleIteration.
     *
     * @param \ApiBundle\Entity\ModuleIteration $moduleIteration
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleIteration(\ApiBundle\Entity\ModuleIteration $moduleIteration)
    {
        return $this->moduleIterations->removeElement($moduleIteration);
    }

    /**
     * Get moduleIterations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleIterations()
    {
        return $this->moduleIterations;
    }

    /**
     * Add moduleResponse.
     *
     * @param \ApiBundle\Entity\ModuleResponse $moduleResponse
     *
     * @return Module
     */
    public function addModuleResponse(\ApiBundle\Entity\ModuleResponse $moduleResponse)
    {
        $this->moduleResponses[] = $moduleResponse;

        return $this;
    }

    /**
     * Remove moduleResponse.
     *
     * @param \ApiBundle\Entity\ModuleResponse $moduleResponse
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleResponse(\ApiBundle\Entity\ModuleResponse $moduleResponse)
    {
        return $this->moduleResponses->removeElement($moduleResponse);
    }

    /**
     * Get moduleResponses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleResponses()
    {
        return $this->moduleResponses;
    }

    /**
     * Add moduleTrainerResponse.
     *
     * @param \ApiBundle\Entity\ModuleTrainerResponse $moduleTrainerResponse
     *
     * @return Module
     */
    public function addModuleTrainerResponse(\ApiBundle\Entity\ModuleTrainerResponse $moduleTrainerResponse)
    {
        $this->moduleTrainerResponses[] = $moduleTrainerResponse;

        return $this;
    }

    /**
     * Remove moduleTrainerResponse.
     *
     * @param \ApiBundle\Entity\ModuleTrainerResponse $moduleResponse
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleTrainerResponse(\ApiBundle\Entity\ModuleTrainerResponse $moduleTrainerResponse)
    {
        return $this->moduleTrainerResponses->removeElement($moduleTrainerResponse);
    }

    /**
     * Get moduleTrainerResponses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleTrainerResponses()
    {
        return $this->moduleResponses;
    }

    /**
     * Add moduleNote.
     *
     * @param \ApiBundle\Entity\ModuleNotes $moduleNote
     *
     * @return Module
     */
    public function addModuleNote(\ApiBundle\Entity\ModuleNotes $moduleNote)
    {
        $this->moduleNotes[] = $moduleNote;

        return $this;
    }

    /**
     * Remove moduleNote.
     *
     * @param \ApiBundle\Entity\ModuleNotes $moduleNote
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModuleNote(\ApiBundle\Entity\ModuleNotes $moduleNote)
    {
        return $this->moduleNotes->removeElement($moduleNote);
    }

    /**
     * Get moduleNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleNotes()
    {
        return $this->moduleNotes;
    }

   

    /**
     * Add attendanceReport.
     *
     * @param \ApiBundle\Entity\ModuleAttendanceReport $attendanceReport
     *
     * @return Module
     */
    public function addAttendanceReport(\ApiBundle\Entity\ModuleAttendanceReport $attendanceReport)
    {
        $this->attendanceReports[] = $attendanceReport;

        return $this;
    }

    /**
     * Remove attendanceReport.
     *
     * @param \ApiBundle\Entity\ModuleAttendanceReport $attendanceReport
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAttendanceReport(\ApiBundle\Entity\ModuleAttendanceReport $attendanceReport)
    {
        return $this->attendanceReports->removeElement($attendanceReport);
    }

    /**
     * Get attendanceReports.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendanceReports()
    {
        return $this->attendanceReports;
    }

    /**
     * Add bookingAgenda.
     *
     * @param \ApiBundle\Entity\BookingAgenda $bookingAgenda
     *
     * @return Module
     */
    public function addBookingAgenda(\ApiBundle\Entity\BookingAgenda $bookingAgenda)
    {
        $this->bookingAgenda[] = $bookingAgenda;

        return $this;
    }

    /**
     * Remove bookingAgenda.
     *
     * @param \ApiBundle\Entity\BookingAgenda $bookingAgenda
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBookingAgenda(\ApiBundle\Entity\BookingAgenda $bookingAgenda)
    {
        return $this->bookingAgenda->removeElement($bookingAgenda);
    }

    /**
     * Get bookingAgenda.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingAgenda()
    {
        return $this->bookingAgenda;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Module
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Module
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return text|null
     */
    public function getSignatureParameters()
    {
        return unserialize($this->signatureParameters);
    }

    /**
     * @param text|null $signatureParameters
     */
    public function setSignatureParameters($signatureParameters)
    {
        $this->signatureParameters = serialize($signatureParameters);
        return $this;
    }
}
