<?php

namespace ApiBundle\Entity;

/**
 * AttendanceAttestation.
 */
class AttendanceAttestation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $attendanceDate;

    /**
     * @var string
     */
    private $file;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $liveResource;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $profile;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->profile = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attendanceDate.
     *
     * @param \DateTime $attendanceDate
     *
     * @return AttendanceAttestation
     */
    public function setAttendanceDate($attendanceDate)
    {
        $this->attendanceDate = $attendanceDate;

        return $this;
    }

    /**
     * Get attendanceDate.
     *
     * @return \DateTime
     */
    public function getAttendanceDate()
    {
        return $this->attendanceDate;
    }

    /**
     * Set file.
     *
     * @param string $file
     *
     * @return AttendanceAttestation
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set liveResource.
     *
     * @param \ApiBundle\Entity\Profile|null $liveResource
     *
     * @return AttendanceAttestation
     */
    public function setLiveResource(\ApiBundle\Entity\Profile $liveResource = null)
    {
        $this->liveResource = $liveResource;

        return $this;
    }

    /**
     * Get liveResource.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLiveResource()
    {
        return $this->liveResource;
    }

    /**
     * Add profile.
     *
     * @param \ApiBundle\Entity\Profile $profile
     *
     * @return AttendanceAttestation
     */
    public function addProfile(\ApiBundle\Entity\Profile $profile)
    {
        $this->profile[] = $profile;

        return $this;
    }

    /**
     * Remove profile.
     *
     * @param \ApiBundle\Entity\Profile $profile
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProfile(\ApiBundle\Entity\Profile $profile)
    {
        return $this->profile->removeElement($profile);
    }

    /**
     * Get profile.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return AttendanceAttestation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return AttendanceAttestation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
