<?php

namespace ApiBundle\Entity;

/**
 * QuizUsersPlayed.
 */
class QuizUsersPlayed {

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $user_id;

    /**
     * @var int
     */
    private $quiz_id;

    /**
     * @var int
     */
    private $scores;

    /**
     * @var date
     */
    private $created;

    /**
     * @var date
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * @var \ApiBundle\Entity\Quizes
     */
    private $quiz;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get user_id.
     *
     * @return string
     */
    public function getUser_id() {
        return $this->user_id;
    }

    /**
     * Set user_id.
     *
     * @param string|null $user_id
     *
     * @return QuizUsersPlayed
     */
    public function setUser_id($user_id = null) {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get quiz.
     *
     * @return string
     */
    public function getQuiz() {
        return $this->quiz;
    }

    /**
     * Set quiz.
     *
     * @param string|null $quiz
     *
     * @return QuizUsersPlayed
     */
    public function setQuiz($quiz = null) {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get module.
     *
     * @return string
     */
    public function getModule() {
        return $this->module;
    }

    /**
     * Set module.
     *
     * @param string|null $module
     *
     * @return QuizUsersPlayed
     */
    public function setModule($module = null) {
        $this->module = $module;

        return $this;
    }

    /**
     * Get scores.
     *
     * @return string
     */
    public function getScores() {
        return $this->scores;
    }

    /**
     * Set scores.
     *
     * @param string|null $scores
     *
     * @return QuizUsersPlayed
     */
    public function setScores($scores = null) {
        $this->scores = $scores;

        return $this;
    }

    /**
     * Get created.
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set created.
     *
     * @param string|null $created
     *
     * @return QuizUsersPlayed
     */
    public function setCreated($created = null) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return string
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set updated.
     *
     * @param string|null $updated
     *
     * @return QuizUsersPlayed
     */
    public function setUpdated($updated = null) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setAnswers($answers) {
        $this->answers = $answers;
        
        return $this;
    }



    /**
     * Add answers.
     *
     * @param \ApiBundle\Entity\QuizUsersAnswers $answer
     *
     * @return QuizUsersPlayed
     */
    public function addAnswers(QuizUsersAnswers $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer.
     *
     * @param \ApiBundle\Entity\QuizUsersAnswers $answer
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnswers(QuizUsersAnswers $answer)
    {
        return $this->answers->removeElement($answer);
    }

    /**
     * Get answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
