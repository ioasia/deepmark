<?php

namespace ApiBundle\Entity;

/**
 * DetailedEvaluation.
 */
class DetailedEvaluation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $globalScore;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $admin;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set globalScore.
     *
     * @param int $globalScore
     *
     * @return DetailedEvaluation
     */
    public function setGlobalScore($globalScore)
    {
        $this->globalScore = $globalScore;

        return $this;
    }

    /**
     * Get globalScore.
     *
     * @return int
     */
    public function getGlobalScore()
    {
        return $this->globalScore;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return DetailedEvaluation
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set admin.
     *
     * @param \ApiBundle\Entity\Profile|null $admin
     *
     * @return DetailedEvaluation
     */
    public function setAdmin(\ApiBundle\Entity\Profile $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return DetailedEvaluation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return DetailedEvaluation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
