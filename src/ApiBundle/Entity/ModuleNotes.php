<?php

namespace ApiBundle\Entity;

/**
 * ModuleNotes.
 */
class ModuleNotes
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\BookingAgenda
     */
    private $session;


    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $learner;

    /**
     * @var string
     */
    private $learner_notes;

    /**
     * @var string
     */
    private $trainer_notes;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set session.
     *
     * @param \ApiBundle\Entity\BookingAgenda|null $session
     *
     * @return ModuleNotes
     */
    public function setSession(\ApiBundle\Entity\BookingAgenda $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return \ApiBundle\Entity\BookingAgenda|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return ModuleNotes
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $learner
     *
     * @return ModuleNotes
     */
    public function setLearner(\ApiBundle\Entity\Profile $learner = null)
    {
        $this->learner = $learner;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getLearner()
    {
        return $this->learner;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return ModuleNotes
     */

    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * @return string
     */
    public function getLearnerNotes()
    {
        return $this->learner_notes;
    }

    /**
     * @param string $learner_notes
     */
    public function setLearnerNotes(string $learner_notes): void
    {
        $this->learner_notes = $learner_notes;
    }

    /**
     * @return string
     */
    public function getTrainerNotes()
    {
        return $this->trainer_notes;
    }

    /**
     * @param string $trainer_notes
     */
    public function setTrainerNotes(string $trainer_notes): void
    {
        $this->trainer_notes = $trainer_notes;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ModuleNotes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return ModuleNotes
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
