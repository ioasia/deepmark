<?php

namespace ApiBundle\Entity;


/**
 * Tag.
 */
class Room
{
    const STATUS_CLOSE = 'CLOSE';
    const STATUS_OPEN = 'OPEN';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $roomName;

    /**
     * @var string
     */
    private $status = 'CLOSE';

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $roomName
     * @return $this
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    public function __toString()
    {
        return $this->roomName;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Room
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Room
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
