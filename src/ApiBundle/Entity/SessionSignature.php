<?php

namespace ApiBundle\Entity;

/**
 * SessionSignature.
 */
class SessionSignature
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\Session
     */
    private $session;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $signature;

    /**
     * @var int
     */
    private $signatureType;

    /**
     * @var int
     */
    private $isOpen;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Profile.
     *
     * @param \ApiBundle\Entity\Profile|null $prpfile
     *
     * @return SessionSignature
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get Profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set session.
     *
     * @param \ApiBundle\Entity\Session|null $session
     *
     * @return SessionSignature
     */
    public function setSession(\ApiBundle\Entity\Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session.
     *
     * @return \ApiBundle\Entity\Session|null
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set signature.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $signature
     *
     * @return SessionSignature
     */
    public function setSignature(\ApiBundle\Entity\FileDescriptor $signature = null)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return int
     */
    public function getSignatureType()
    {
        return $this->signatureType;
    }

    /**
     * @param int $signatureType
     */
    public function setSignatureType(int $signatureType)
    {
        $this->signatureType = $signatureType;
    }

    /**
     * Get signature.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @return int
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }

    /**
     * @param int $isOpen
     *
     * @return SessionSignature
     */
    public function setIsOpen(int $isOpen)
    {
        $this->isOpen = $isOpen;
        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return SessionSignature
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return SessionSignature
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
