<?php

namespace ApiBundle\Entity;

/**
 * DetailedEvaluationLine.
 */
class DetailedEvaluationLine
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $wording;

    /**
     * @var float|null
     */
    private $notation;

    /**
     * @var bool
     */
    private $title = '0';

    /**
     * @var \ApiBundle\Entity\DetailedEvaluation
     */
    private $detailEvaluation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wording.
     *
     * @param string $wording
     *
     * @return DetailedEvaluationLine
     */
    public function setWording($wording)
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * Get wording.
     *
     * @return string
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * Set notation.
     *
     * @param float|null $notation
     *
     * @return DetailedEvaluationLine
     */
    public function setNotation($notation = null)
    {
        $this->notation = $notation;

        return $this;
    }

    /**
     * Get notation.
     *
     * @return float|null
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set title.
     *
     * @param bool $title
     *
     * @return DetailedEvaluationLine
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return bool
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set detailEvaluation.
     *
     * @param \ApiBundle\Entity\DetailedEvaluation|null $detailEvaluation
     *
     * @return DetailedEvaluationLine
     */
    public function setDetailEvaluation(\ApiBundle\Entity\DetailedEvaluation $detailEvaluation = null)
    {
        $this->detailEvaluation = $detailEvaluation;

        return $this;
    }

    /**
     * Get detailEvaluation.
     *
     * @return \ApiBundle\Entity\DetailedEvaluation|null
     */
    public function getDetailEvaluation()
    {
        return $this->detailEvaluation;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return DetailedEvaluationLine
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return DetailedEvaluationLine
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
