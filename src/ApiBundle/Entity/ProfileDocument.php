<?php

namespace ApiBundle\Entity;

/**
 * ProfileDocument.
 */
class ProfileDocument
{
    public const IDENTITY_DOCUMENT = 1;
    public const SOCIAL_DOCUMENT = 2;
    public const ASSURANCE_DOCUMENT = 3;
    public const CONTRACT_DOCUMENT = 4;
    public const MISSION_DOCUMENT = 5;
    public const CV_DOCUMENT = 6;
    public const BANK_ACCOUNT_STATEMENT = 7;
    public const EDITOR_FILE = 8;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\FileDescriptor
     */
    private $fileDescriptor;

    /**
     * @var int
     */
    private $documentType;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set learner.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return ModuleNotes
     */

    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return int
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param int $documentType
     */
    public function setDocumentType(int $documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * Set fileDescriptor.
     *
     * @param \ApiBundle\Entity\FileDescriptor|null $fileDescriptor
     *
     * @return ProfileDocument
     */
    public function setFileDescriptor(\ApiBundle\Entity\FileDescriptor $fileDescriptor = null)
    {
        $this->fileDescriptor = $fileDescriptor;

        return $this;
    }

    /**
     * Get fileDescriptor.
     *
     * @return \ApiBundle\Entity\FileDescriptor|null
     */
    public function getFileDescriptor()
    {
        return $this->fileDescriptor;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Session
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Session
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}