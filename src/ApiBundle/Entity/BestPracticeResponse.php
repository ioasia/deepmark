<?php

namespace ApiBundle\Entity;

/**
 * BestPracticeResponse.
 */
class BestPracticeResponse
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \DateTime
     */
    private $dateResponse;

    /**
     * @var float
     */
    private $notation;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * @var \ApiBundle\Entity\BestPractice
     */
    private $bestPractice;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return BestPracticeResponse
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set dateResponse.
     *
     * @param \DateTime $dateResponse
     *
     * @return BestPracticeResponse
     */
    public function setDateResponse($dateResponse)
    {
        $this->dateResponse = $dateResponse;

        return $this;
    }

    /**
     * Get dateResponse.
     *
     * @return \DateTime
     */
    public function getDateResponse()
    {
        return $this->dateResponse;
    }

    /**
     * Set notation.
     *
     * @param float $notation
     *
     * @return BestPracticeResponse
     */
    public function setNotation($notation)
    {
        $this->notation = $notation;

        return $this;
    }

    /**
     * Get notation.
     *
     * @return float
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return BestPracticeResponse
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get learner.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->learner;
    }

    /**
     * Set bestPractice.
     *
     * @param \ApiBundle\Entity\BestPractice|null $bestPractice
     *
     * @return BestPracticeResponse
     */
    public function setBestPractice(\ApiBundle\Entity\BestPractice $bestPractice = null)
    {
        $this->bestPractice = $bestPractice;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\BestPractice|null
     */
    public function getBestPractice()
    {
        return $this->bestPractice;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BestPracticeResponse
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BestPracticeResponse
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
