<?php

namespace ApiBundle\Entity;

/**
 * StatusFamily.
 */
class StatusFamily
{
    public const GENERAL = 1;
    public const INTERVENTION = 6;
    public const LEARNER_INTERVENTION = 7;
    public const PROFILE = 2;
    public const WITHOUT_BOOKING = 3;
    public const WITH_BOOKING = 4;
    public const REPORT = 5;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return StatusFamily
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @var int
     */
    private $appId;

    /**
     * Set appId.
     *
     * @param int $appId
     *
     * @return StatusFamily
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId.
     *
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StatusFamily
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return StatusFamily
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
