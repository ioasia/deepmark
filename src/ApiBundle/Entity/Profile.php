<?php

namespace ApiBundle\Entity;

use AppBundle\Entity\Person;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class Profile
 * @package ApiBundle\Entity
 */
class Profile
{
    // lunch time
    public const LUNCH_START_TIME = '12:00';
    public const LUNCH_END_TIME = '14:00';

    // default password
    public const DEFAULT_PASSWORD = 'PleaseChangeMe';

    /**
     * @var int
     */
    private $id;

    /**
     * @var text|null
     */

    private $workingHours;

    /**
     * @var DateTime
     */
    private $lunchStartTime;

    /**
     * @var DateTime
     */
    private $lunchEndTime;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var DateTime
     */
    private $updateDate;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string|null
     */
    private $fixedPhone;

    /**
     * @var string|null
     */
    private $mobilePhone;

    /**
     * @var string|null
     */
    private $employeeId;

    /**
     * @var string|null
     */
    private $otherEmail;

    /**
     * @var string
     */
    private $billingCode;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string|null
     */
    private $avatar;

    /**
     * @var string|null
     */
    private $organisationRegistrationNumber;

    /**
     * @var string|null
     */
    private $pitch;

    /**
     * @var string|null
     */
    private $video;

    /**
     * @var string|null
     */
    private $curriculumVitae;

    /**
     * @var string|null
     */
    private $scannedIban;

    /**
     * @var string|null
     */
    private $iban;

    /**
     * @var string|null
     */
    private $cv;

    /**
     * @var string|null
     */
    private $bicCode;

    /**
     * @var Person
     */
    private $person;

    /**
     * @var CompanyPost
     */
    private $companyPost;

    /**
     * @var RestrictionAccess
     */
    private $restrictionAccess;

    /**
     * @var Entity
     */
    private $entity;

    /**
     * @var ProfileVariety
     */
    private $profileType;

    /**
     * @var LiveResourceOrigin
     */
    private $liveResourceOrigin;

    /**
     * @var InterventionPerimeter
     */
    private $interventionPerimeter;

    /**
     * @var Collection
     */
    private $manager;

    /**
     * @var Collection
     */
    private $attendanceAttestation;

    private $billingAddress;

    private $noteGlobal;

    private $nbActivitiesExecuted;

    /**
     * @var Regional
     */
    private $regional;

    /**
     * @var GroupOrganization
     */
    private $groupOrganization;

    /**
     * @var Workplace
     */
    private $workplace;

    /**
     * @var string|null
     */
    private $bankName;

    /**
     * @var string|null
     */
    private $accountCurrency;

    /**
     * @var string|null
     */
    private $bic;

    /**
     * @var string|null
     */
    private $bankCode;

    /**
     * @var string|null
     */
    private $agencyCode;

    /**
     * @var string|null
     */
    private $accountNum;

    /**
     * @var string|null
     */
    private $ribKey;

    /**
     * @var string|null
     */
    private $domiciliationAgency;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->manager               = new ArrayCollection();
        $this->attendanceAttestation = new ArrayCollection();
    }

    /**
     * @return InterventionPerimeter
     */
    public function getInterventionPerimeter()
    {
        return $this->interventionPerimeter;
    }

    /**
     * @param InterventionPerimeter $interventionPerimeter
     */
    public function setInterventionPerimeter(InterventionPerimeter $interventionPerimeter = null)
    {
        $this->interventionPerimeter = $interventionPerimeter;
        return $this;
    }


    /**
     * @return LiveResourceOrigin
     */
    public function getLiveResourceOrigin()
    {
        return $this->liveResourceOrigin;
    }

    /**
     * @param LiveResourceOrigin $liveResourceOrigin
     */
    public function setLiveResourceOrigin(LiveResourceOrigin $liveResourceOrigin = null)
    {
        $this->liveResourceOrigin = $liveResourceOrigin;
        return $this;
    }


    /**
     * @return null|string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param null|string $bankName
     */
    public function setBankName($bankName = null)
    {
        $this->bankName = $bankName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAccountCurrency()
    {
        return $this->accountCurrency;
    }

    /**
     * @param null|string $accountCurrency
     */
    public function setAccountCurrency($accountCurrency = null)
    {
        $this->accountCurrency = $accountCurrency;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param null|string $bic
     */
    public function setBic($bic = null)
    {
        $this->bic = $bic;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @param null|string $bankCode
     */
    public function setBankCode($bankCode = null)
    {
        $this->bankCode = $bankCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAgencyCode()
    {
        return $this->agencyCode;
    }

    /**
     * @param null|string $agencyCode
     */
    public function setAgencyCode($agencyCode = null)
    {
        $this->agencyCode = $agencyCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAccountNum()
    {
        return $this->accountNum;
    }

    /**
     * @param null|string $accountNum
     */
    public function setAccountNum($accountNum = null)
    {
        $this->accountNum = $accountNum;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getRibKey()
    {
        return $this->ribKey;
    }

    /**
     * @param null|string $ribKey
     */
    public function setRibKey($ribKey = null)
    {
        $this->ribKey = $ribKey;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDomiciliationAgency()
    {
        return $this->domiciliationAgency;
    }

    /**
     * @param null|string $domiciliationAgency
     */
    public function setDomiciliationAgency($domiciliationAgency = null)
    {
        $this->domiciliationAgency = $domiciliationAgency;
        return $this;
    }



    /**
     * @return GroupOrganization
     */
    public function getGroupOrganization()
    {
        return $this->groupOrganization;
    }

    /**
     * @param GroupOrganization $groupOrganization
     */
    public function setGroupOrganization(GroupOrganization $groupOrganization = null)
    {
        $this->groupOrganization = $groupOrganization;
        return $this;
    }

    /**
     * @return Regional
     */
    public function getRegional()
    {
        return $this->regional;
    }

    /**
     * @param Regional $regional
     */
    public function setRegional(Regional $regional = null)
    {
        $this->regional = $regional;
        return $this;
    }

    /**
     * @return Workplace
     */
    public function getWorkplace()
    {
        return $this->workplace;
    }

    /**
     * @param Workplace $workplace
     *
     * @return Profile
     */
    public function setWorkplace(Workplace $workplace = null)
    {
        $this->workplace = $workplace;

        return $this;
    }

    /**
     * @return text|null
     */
    public function getWorkingHours()
    {
        return unserialize($this->workingHours);
    }

    /**
     * @param text|null $workingHours
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = serialize($workingHours);
        return $this;
    }

    public function getDisplayName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get lunchStartTime.
     *
     * @return DateTime
     */
    public function getLunchStartTime()
    {
        return $this->lunchStartTime;
    }

    /**
     * Set lunchStartTime.
     *
     * @param DateTime $lunchStartTime
     *
     * @return Profile
     */
    public function setLunchStartTime($lunchStartTime)
    {
        $this->lunchStartTime = $lunchStartTime;

        return $this;
    }

    /**
     * Get lunchEndTime.
     *
     * @return DateTime
     */
    public function getLunchEndTime()
    {
        return $this->lunchEndTime;
    }

    /**
     * Set lunchEndTime.
     *
     * @param DateTime $lunchStartTime
     *
     * @return Profile
     */
    public function setLunchEndTime($lunchEndTime)
    {
        $this->lunchEndTime = $lunchEndTime;

        return $this;
    }

    /**
     * Set creationDate.
     *
     * @param DateTime $creationDate
     *
     * @return Profile
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate.
     *
     * @return DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate.
     *
     * @param DateTime $updateDate
     *
     * @return Profile
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return Profile
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return Profile
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set fixedPhone.
     *
     * @param string|null $fixedPhone
     *
     * @return Profile
     */
    public function setFixedPhone($fixedPhone = null)
    {
        $this->fixedPhone = $fixedPhone;

        return $this;
    }

    /**
     * Get fixedPhone.
     *
     * @return string|null
     */
    public function getFixedPhone()
    {
        return $this->fixedPhone;
    }

    /**
     * Set mobilePhone.
     *
     * @param string|null $mobilePhone
     *
     * @return Profile
     */
    public function setMobilePhone($mobilePhone = null)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone.
     *
     * @return string|null
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set otherEmail.
     *
     * @param string|null $otherEmail
     *
     * @return Profile
     */
    public function setOtherEmail($otherEmail = null)
    {
        $this->otherEmail = $otherEmail;

        return $this;
    }

    /**
     * Get otherEmail.
     *
     * @return string|null
     */
    public function getOtherEmail()
    {
        return $this->otherEmail;
    }

    /**
     * Set billingCode.
     *
     * @param string $billingCode
     *
     * @return Profile
     */
    public function setBillingCode($billingCode)
    {
        $this->billingCode = $billingCode;

        return $this;
    }

    /**
     * Get billingCode.
     *
     * @return string
     */
    public function getBillingCode()
    {
        return $this->billingCode;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Profile
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region)
    {
        $this->region = $region;
    }

    /**
     * Set avatar.
     *
     * @param string|null $avatar
     *
     * @return Profile
     */
    public function setAvatar($avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar.
     *
     * @return string|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set organisationRegistrationNumber.
     *
     * @param string|null $organisationRegistrationNumber
     *
     * @return Profile
     */
    public function setOrganisationRegistrationNumber($organisationRegistrationNumber = null)
    {
        $this->organisationRegistrationNumber = $organisationRegistrationNumber;

        return $this;
    }

    /**
     * Get organisationRegistrationNumber.
     *
     * @return string|null
     */
    public function getOrganisationRegistrationNumber()
    {
        return $this->organisationRegistrationNumber;
    }

    /**
     * Set pitch.
     *
     * @param string|null $pitch
     *
     * @return Profile
     */
    public function setPitch($pitch = null)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * Get pitch.
     *
     * @return string|null
     */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * Set video.
     *
     * @param string|null $video
     *
     * @return Profile
     */
    public function setVideo($video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video.
     *
     * @return string|null
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set curriculumVitae.
     *
     * @param string|null $curriculumVitae
     *
     * @return Profile
     */
    public function setCurriculumVitae($curriculumVitae = null)
    {
        $this->curriculumVitae = $curriculumVitae;

        return $this;
    }

    /**
     * Get curriculumVitae.
     *
     * @return string|null
     */
    public function getCurriculumVitae()
    {
        return $this->curriculumVitae;
    }

    /**
     * Set scannedIban.
     *
     * @param string|null $scannedIban
     *
     * @return Profile
     */
    public function setScannedIban($scannedIban = null)
    {
        $this->scannedIban = $scannedIban;

        return $this;
    }

    /**
     * Get scannedIban.
     *
     * @return string|null
     */
    public function getScannedIban()
    {
        return $this->scannedIban;
    }

    /**
     * Set iban.
     *
     * @param string|null $iban
     *
     * @return Profile
     */
    public function setIban($iban = null)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get cv.
     *
     * @return string|null
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Set iban.
     *
     * @param string|null $cv
     *
     * @return Profile
     */
    public function setCv($cv = null)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get iban.
     *
     * @return string|null
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bicCode.
     *
     * @param string|null $bicCode
     *
     * @return Profile
     */
    public function setBicCode($bicCode = null)
    {
        $this->bicCode = $bicCode;

        return $this;
    }

    /**
     * Get bicCode.
     *
     * @return string|null
     */
    public function getBicCode()
    {
        return $this->bicCode;
    }

    /**
     * Set person.
     *
     * @param Person|null $person
     *
     * @return Profile
     */
    public function setPerson(Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return Person|null
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set companyPost.
     *
     * @param CompanyPost|null $companyPost
     *
     * @return Profile
     */
    public function setCompanyPost(CompanyPost $companyPost = null)
    {
        $this->companyPost = $companyPost;

        return $this;
    }

    /**
     * Get companyPost.
     *
     * @return CompanyPost|null
     */
    public function getCompanyPost()
    {
        return $this->companyPost;
    }

    /**
     * Set restrictionAccess.
     *
     * @param RestrictionAccess|null $restrictionAccess
     *
     * @return Profile
     */
    public function setRestrictionAccess(RestrictionAccess $restrictionAccess = null)
    {
        $this->restrictionAccess = $restrictionAccess;

        return $this;
    }

    /**
     * Get restrictionAccess.
     *
     * @return RestrictionAccess|null
     */
    public function getRestrictionAccess()
    {
        return $this->restrictionAccess;
    }

    /**
     * Set entity.
     *
     * @param Entity|null $entity
     *
     * @return Profile
     */
    public function setEntity(Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return Entity|null
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set profileType.
     *
     * @param ProfileVariety|null $profileType
     *
     * @return Profile
     */
    public function setProfileType(ProfileVariety $profileType = null)
    {
        $this->profileType = $profileType;

        return $this;
    }

    /**
     * Get profileType.
     *
     * @return ProfileVariety|null
     */
    public function getProfileType()
    {
        return $this->profileType;
    }

    /**
     * Add manager.
     *
     * @param Manager $manager
     *
     * @return Profile
     */
    public function addManager(Manager $manager)
    {
        $this->manager[] = $manager;

        return $this;
    }

    /**
     * Remove manager.
     *
     * @param Manager $manager
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeManager(Manager $manager)
    {
        return $this->manager->removeElement($manager);
    }

    /**
     * Get manager.
     *
     * @return Collection
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Add attendanceAttestation.
     *
     * @param AttendanceAttestation $attendanceAttestation
     *
     * @return Profile
     */
    public function addAttendanceAttestation(AttendanceAttestation $attendanceAttestation)
    {
        $this->attendanceAttestation[] = $attendanceAttestation;

        return $this;
    }

    /**
     * Remove attendanceAttestation.
     *
     * @param AttendanceAttestation $attendanceAttestation
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAttendanceAttestation(AttendanceAttestation $attendanceAttestation)
    {
        return $this->attendanceAttestation->removeElement($attendanceAttestation);
    }

    /**
     * Get attendanceAttestation.
     *
     * @return Collection
     */
    public function getAttendanceAttestation()
    {
        return $this->attendanceAttestation;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param mixed $billingAddress
     */
    public function setBillingAddress($billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return mixed
     */
    public function getNoteGlobal()
    {
        return $this->noteGlobal;
    }

    /**
     * @param mixed $noteGlobal
     */
    public function setNoteGlobal($noteGlobal): void
    {
        $this->noteGlobal = $noteGlobal;
    }

    /**
     * @return mixed
     */
    public function getNbActivitiesExecuted()
    {
        return $this->nbActivitiesExecuted;
    }

    /**
     * @param mixed $nbActivitiesExecuted
     */
    public function setNbActivitiesExecuted($nbActivitiesExecuted): void
    {
        $this->nbActivitiesExecuted = $nbActivitiesExecuted;
    }

    /**
     * @var string|null
     */
    private $managerEmail;

    /**
     * Set managerEmail.
     *
     * @param string|null $managerEmail
     *
     * @return Profile
     */
    public function setManagerEmail($managerEmail = null)
    {
        $this->managerEmail = $managerEmail;

        return $this;
    }

    /**
     * Get managerEmail.
     *
     * @return string|null
     */
    public function getManagerEmail()
    {
        return $this->managerEmail;
    }

    /**
     * @var Collection
     */
    private $skills;

    /**
     * Add skill.
     *
     * @param SkillResourceVarietyProfile $skill
     *
     * @return Profile
     */
    public function addSkill(SkillResourceVarietyProfile $skill)
    {
        $this->skills[] = $skill;

        return $this;
    }

    /**
     * Remove skill.
     *
     * @param SkillResourceVarietyProfile $skill
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeSkill(SkillResourceVarietyProfile $skill)
    {
        return $this->skills->removeElement($skill);
    }

    /**
     * Get skills.
     *
     * @return Collection
     */
    public function getSkills()
    {
        return $this->skills;
    }

    public function __toString()
    {
        return $this->getDisplayName();
    }

    /**
     * @var string
     */
    private $position;

    /**
     * Set position.
     *
     * @param string $position
     *
     * @return Profile
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @var Collection
     */
    private $availableCalendar;

    /**
     * Add availableCalendar.
     *
     * @param AvailableCalendar $availableCalendar
     *
     * @return Profile
     */
    public function addAvailableCalendar(AvailableCalendar $availableCalendar)
    {
        $this->availableCalendar[] = $availableCalendar;

        return $this;
    }

    /**
     * Remove availableCalendar.
     *
     * @param AvailableCalendar $availableCalendar
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAvailableCalendar(AvailableCalendar $availableCalendar)
    {
        return $this->availableCalendar->removeElement($availableCalendar);
    }

    /**
     * Get availableCalendar.
     *
     * @return Collection
     */
    public function getAvailableCalendar()
    {
        return $this->availableCalendar;
    }

    /**
     * @var Collection
     */
    private $trainerReservation;

    /**
     * Add trainerReservation.
     *
     * @param TrainerReservation $trainerReservation
     *
     * @return Profile
     */
    public function addTrainerReservation(TrainerReservation $trainerReservation)
    {
        $this->trainerReservation[] = $trainerReservation;

        return $this;
    }

    /**
     * Remove trainerReservation.
     *
     * @param TrainerReservation $trainerReservation
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTrainerReservation(TrainerReservation $trainerReservation)
    {
        return $this->trainerReservation->removeElement($trainerReservation);
    }

    /**
     * Get trainerReservation.
     *
     * @return Collection
     */
    public function getTrainerReservation()
    {
        return $this->trainerReservation;
    }

    public function __toArray(): array
    {
        return [
            'id'         => $this->getId(),
            'first_name' => $this->getFirstName(),
            'last_name'  => $this->getLastName(),
            'city'  => $this->getCity(),
            'avatar'  => $this->getAvatar(),
        ];
    }

    /**
     * @var DateTime
     */
    private $created;

    /**
     * @var DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param DateTime $created
     *
     * @return Profile
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param DateTime $updated
     *
     * @return Profile
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return null|string
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @param null|string $employeeId
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }
}