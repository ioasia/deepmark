<?php

namespace ApiBundle\Entity;

use Ramsey\Uuid\Uuid;

/**
 * InterventionStep.
 */
class InterventionStep
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $interventionId;

    /**
     * @var string
     */
    private $step_1;

    /**
     * @var string
     */
    private $step_2;

    /**
     * @var string
     */
    private $step_3;

    /**
     * @var string
     */
    private $step_4;

    /**
     * @var int
     */
    private $published = 0;

    /**
     * @var Entity
     */
    private $entity;

    public function __construct()
    {
        $this->createDate = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set interventionId.
     *
     * @param string $interventionId
     *
     * @return InterventionStep
     */
    public function setInterventionId($interventionId)
    {
        $this->interventionId = $interventionId;

        return $this;
    }

    /**
     * Get interventionId.
     *
     * @return string
     */
    public function getInterventionId()
    {
        return $this->interventionId;
    }

    /**
     * Set step1.
     *
     * @param array $step1
     *
     * @return InterventionStep
     */
    public function setStep1($step1)
    {
        $this->step_1 = serialize($step1);

        return $this;
    }

    /**
     * Get step1.
     *
     * @return array
     */
    public function getStep1()
    {
        return unserialize($this->step_1);
    }

    /**
     * Set step2.
     *
     * @param string $step2
     *
     * @return InterventionStep
     */
    public function setStep2($step2)
    {
        $this->step_2 = serialize($step2);

        return $this;
    }

    /**
     * Get step2.
     *
     * @return array
     */
    public function getStep2()
    {
        return unserialize($this->step_2);
    }

    /**
     * Set step3.
     *
     * @param string $step3
     *
     * @return InterventionStep
     */
    public function setStep3($step3)
    {
        $this->step_3 = serialize($step3);

        return $this;
    }

    /**
     * Get step3.
     *
     * @return array
     */
    public function getStep3()
    {
        return unserialize($this->step_3);
    }

    /**
     * Set step4.
     *
     * @param string $step4
     *
     * @return InterventionStep
     */
    public function setStep4($step4)
    {
        $this->step_4 = serialize($step4);

        return $this;
    }

    /**
     * Get step4.
     *
     * @return array
     */
    public function getStep4()
    {
        return unserialize($this->step_4);
    }

    /**
     * Set published.
     *
     * @param int $published
     *
     * @return InterventionStep
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published.
     *
     * @return int
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @var string|null
     */
    private $sessionId;

    /**
     * Set sessionId.
     *
     * @return InterventionStep
     *
     * @throws \Exception
     */
    public function setSessionId()
    {
        if (empty($this->sessionId)) {
            $unique = Uuid::uuid1();
            $this->sessionId = $unique->toString();
        }

        return $this;
    }

    /**
     * Get sessionId.
     *
     * @return string|null
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function __toArray()
    {
        return [
            'sessionId' => $this->getSessionId(),
            'step' => [
                1 => $this->getStep1(),
                2 => $this->getStep2(),
                3 => $this->getStep3(),
                4 => $this->getStep4(),
            ],
        ];
    }

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return InterventionStep
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @var string|null
     */
    private $stepErrors;

    /**
     * Set stepErrors.
     *
     * @param string|null $stepErrors
     *
     * @return InterventionStep
     */
    public function setStepErrors($stepErrors = null)
    {
        $this->stepErrors = $stepErrors;

        return $this;
    }

    /**
     * Get stepErrors.
     *
     * @return string|null
     */
    public function getStepErrors()
    {
        return $this->stepErrors;
    }

    public function getCombinedSteps()
    {
        $setToMerge = $this->getStep1()['module'];
        $combined = [];
        foreach ($this->getStep1()['module'] as $uniqueId => $module) {
            if (array_key_exists($uniqueId, $setToMerge) && array_key_exists($uniqueId, $this->getStep2()['module'])) {
                $combined[$uniqueId] = array_merge($setToMerge[$uniqueId], $this->getStep2()['module'][$uniqueId]);
            } elseif (array_key_exists($uniqueId, $this->getStep2()['module'])) {
                $combined[$uniqueId] = $this->getStep2()['module'][$uniqueId];
            } else {
                $combined[$uniqueId] = $setToMerge[$uniqueId];
            }
        }
        $stepMain = $this->getStep1();
        $stepMain['module'] = $combined;
        $stepMain['constraints'] = $this->getStep3();

        return $stepMain;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return InterventionStep
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return InterventionStep
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     *
     * @return InterventionStep
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
        return $this;
    }
}
