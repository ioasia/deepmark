<?php

namespace ApiBundle\Entity;

/**
 * Visio.
 */
class Visio
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $webinarKey;

    /**
     * @var string|null
     */
    private $joinUrl;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $trainer;

    /**
     * @var \ApiBundle\Entity\Module
     */
    private $module;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set webinarKey.
     *
     * @param string|null $webinarKey
     *
     * @return Visio
     */
    public function setWebinarKey($webinarKey = null)
    {
        $this->webinarKey = $webinarKey;

        return $this;
    }

    /**
     * Get webinarKey.
     *
     * @return string|null
     */
    public function getWebinarKey()
    {
        return $this->webinarKey;
    }

    /**
     * Set joinUrl.
     *
     * @param string|null $joinUrl
     *
     * @return Visio
     */
    public function setJoinUrl($joinUrl = null)
    {
        $this->joinUrl = $joinUrl;

        return $this;
    }

    /**
     * Get joinUrl.
     *
     * @return string|null
     */
    public function getJoinUrl()
    {
        return $this->joinUrl;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Visio
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return Visio
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set trainer.
     *
     * @param \ApiBundle\Entity\Profile|null $trainer
     *
     * @return Visio
     */
    public function setTrainer(\ApiBundle\Entity\Profile $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set module.
     *
     * @param \ApiBundle\Entity\Module|null $module
     *
     * @return Visio
     */
    public function setModule(\ApiBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \ApiBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bookings;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add booking.
     *
     * @param \ApiBundle\Entity\BookingAgenda $booking
     *
     * @return Visio
     */
    public function addBooking(\ApiBundle\Entity\BookingAgenda $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking.
     *
     * @param \ApiBundle\Entity\BookingAgenda $booking
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBooking(\ApiBundle\Entity\BookingAgenda $booking)
    {
        return $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Visio
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Visio
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
