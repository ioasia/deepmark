<?php

namespace ApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ApiExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        if (!isset($config['nexmo']['url'])) {
            throw new \InvalidArgumentException(
                'The option "api.nexmo.url" must be set.'
            );
        }
        if (!isset($config['nexmo']['api_key'])) {
            throw new \InvalidArgumentException(
                'The option "api.nexmo.api_key" must be set.'
            );
        }
        if (!isset($config['nexmo']['api_secret'])) {
            throw new \InvalidArgumentException(
                'The option "api.nexmo.api_secret" must be set.'
            );
        }

        $container->setParameter(
            'api.nexmo.url',
            $config['nexmo']['url']
        );
        $container->setParameter(
            'api.nexmo.api_key',
            $config['nexmo']['api_key']
        );
        $container->setParameter(
            'api.nexmo.api_secret',
            $config['nexmo']['api_secret']
        );

        if (!isset($config['webinar']['url'])) {
            throw new \InvalidArgumentException(
                'The option "api.webinar.url" must be set.'
            );
        }
        if (!isset($config['webinar']['api_key'])) {
            throw new \InvalidArgumentException(
                'The option "api.webinar.api_key" must be set.'
            );
        }
        if (!isset($config['webinar']['api_secret'])) {
            throw new \InvalidArgumentException(
                'The option "api.webinar.api_secret" must be set.'
            );
        }

        $container->setParameter(
            'api.webinar.url',
            $config['webinar']['url']
        );
        $container->setParameter(
            'api.webinar.api_key',
            $config['webinar']['api_key']
        );
        $container->setParameter(
            'api.webinar.api_secret',
            $config['webinar']['api_secret']
        );
    }

    public function getAlias()
    {
        return 'api';
    }
}
