<?php

namespace ApiBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class NotStartCourses
 * @package ApiBundle\Command
 */
class LearnerNotCompletedCourses extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('alerts:learner-not-complete-courses')
            ->setDescription('Using for send the alert email to participants who have not started their journey')
            ->addArgument(
                'days',
                InputArgument::REQUIRED,
                'Warnings in what days? Example: 15, 30,... (days)'
            )
            ->addArgument(
                'domain',
                InputArgument::REQUIRED,
                'The domain of website'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $stopwatch = new Stopwatch();
        $stopwatch->start('alerts');
        $service = $this->getContainer()->get('alerts_service');
        $logger->info('Alert not complete the journey: ');
        $service->learnerNotCompletedCourses($this->getContainer(), $input->getArgument('days'), $input->getArgument('domain'));
        $event = $stopwatch->stop('alerts');

        $dt = new DateTime('NOW');
        $output->writeln('<comment>' . $dt->format('Y-m-d H:i:s')
            . ' Alert run in ' . $event->getDuration() / 1000 . ' seconds' . '</comment>');
    }
}
