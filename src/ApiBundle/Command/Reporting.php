<?php

namespace ApiBundle\Command;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use Doctrine\Persistence\ObjectManager;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Traits\HasSpreadsheet;

/**
 * Class Reporting
 * @package ApiBundle\Command
 */
abstract class Reporting extends ContainerAwareCommand
{
    use HasSpreadsheet;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->writeHeaders();
        $reports = $this->getReports();

        $worksheet   = $this->spreadsheet->getActiveSheet();
        $progressBar = new ProgressBar($output, count($reports));
        $progressBar->start();

        foreach ($reports as $rowIndex => $course) {
            $this->processReport($rowIndex, $course, $worksheet);
            $progressBar->advance();
        }

        $this->writeToFile(__DIR__ . DIRECTORY_SEPARATOR . strtolower((new \ReflectionClass($this))->getShortName()));
        $output->writeln('');

        return 0;
    }

    /**
     * @return ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return array
     */
    abstract protected function getReports(): array;

    /**
     * @param int $rowIndex
     * @param array $report
     * @param Worksheet $worksheet
     * @return mixed
     */
    abstract protected function processReport(int $rowIndex, array $report, Worksheet $worksheet);

    /**
     * @param array $conditions
     * @return AssigmentResourceSystem[]|Intervention[]|LearnerIntervention[]|Module[]|array|object[]
     */
    protected function getCourses(array $conditions = [])
    {
        return $this->getEntityManager()->getRepository(Intervention::class)->findBy($conditions);
    }

    /**
     * @param array $conditions
     * @return AssigmentResourceSystem[]|Intervention[]|LearnerIntervention[]|Module[]|array|object[]
     */
    protected function getModules(array $conditions = [])
    {
        return $this->getEntityManager()->getRepository(Module::class)->findBy($conditions);
    }

    /**
     * @param array $conditions
     * @return AssigmentResourceSystem[]|Intervention[]|LearnerIntervention[]|Module[]|array|object[]
     */
    protected function getModuleTrainers(array $conditions = [])
    {
        return $this->getEntityManager()->getRepository(AssigmentResourceSystem::class)->findBy($conditions);
    }

    /**
     * @param array $conditions
     * @return AssigmentResourceSystem[]|Intervention[]|LearnerIntervention[]|Module[]|array|object[]
     */
    protected function getCourseLearners(array $conditions = [])
    {
        return $this->getEntityManager()->getRepository(LearnerIntervention::class)->findBy($conditions);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function writeHeaders()
    {
        $headers     = array_keys($this->headers);
        $activeSheet = $this->spreadsheet->getActiveSheet();

        foreach ($headers as $index => $header) {
            $activeSheet->getCellByColumnAndRow((int)$index + 1, 1)->setValue($header);
        }
    }
}
