<?php

namespace ApiBundle\Command;

use ApiBundle\Entity\Profile;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Utils\User;

/**
 * Class Profiles
 * @package ApiBundle\Command
 */
class Profiles extends ContainerAwareCommand
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('profiles:default')
            ->setDescription('Update default data for profiles');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em       = $this->getContainer()->get('doctrine')->getManager();
        $profiles = $em->getRepository(Profile::class)->findAll();

        foreach ($profiles as $profile) {
            User::setDefaultWorkingHours($em, $profile);
            $em->flush();
        }
    }
}
