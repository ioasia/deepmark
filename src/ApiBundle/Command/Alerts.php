<?php

namespace ApiBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class Alerts
 * @package ApiBundle\Command
 */
class Alerts extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('alerts:alerts')
            ->setDescription('Using for all alert: report upload, validation skills, validated trainers')
            ->addArgument(
                'domain',
                InputArgument::REQUIRED,
                'The domain of website'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('alerts');

        $service = $this->getContainer()->get('alerts_service');
        $service->alertReportUpload($this->getContainer());
        $service->alertCount($this->getContainer(), $input->getArgument('domain'));

        $event = $stopwatch->stop('alerts');

        $dt = new DateTime('NOW');
        $output->writeln('<comment>' . $dt->format('Y-m-d H:i:s')
            . 'Alert run in ' . $event->getDuration() / 1000 . ' seconds' . '</comment>');
    }
}
