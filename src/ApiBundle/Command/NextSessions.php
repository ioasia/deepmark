<?php

namespace ApiBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class NextSessions
 * @package ApiBundle\Command
 */
class NextSessions extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('alerts:next-sessions')
            ->setDescription('Using for send alert email with the next session in x hours')
            ->addArgument(
                'hours',
                InputArgument::REQUIRED,
                'Warnings in what hours? Example: 48, 24,... (hours)'
            )
            ->addArgument(
                'minutes',
                InputArgument::REQUIRED,
                'Warnings in what minutes? Example: 1, 59,... (minutes)'
            )
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Type of user'
            )
            ->addArgument(
                'domain',
                InputArgument::REQUIRED,
                'The domain of website'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $stopwatch = new Stopwatch();
        $stopwatch->start('alerts');
        $service = $this->getContainer()->get('alerts_service');
        $logger->info('Alert next session: ' . $input->getArgument('hours') . ' hours ' . $input->getArgument('minutes') . ' minutes and type as' . $input->getArgument('type'));
        $service->alertNextSessions($this->getContainer(), $input->getArgument('hours'), $input->getArgument('minutes'), $input->getArgument('type'), $input->getArgument('domain'));
        $event = $stopwatch->stop('alerts');

        $dt = new DateTime('NOW');
        $output->writeln('<comment>' . $dt->format('Y-m-d H:i:s')
            . ' Alert run in ' . $event->getDuration() / 1000 . ' seconds' . '</comment>');
    }
}
