<?php

namespace ApiBundle\Command\Reporting;

use ApiBundle\Command\Reporting;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class Courses
 * @package ApiBundle\Command\Reporting
 */
class Courses extends Reporting
{
    /**
     * @var array
     */
    protected $headers = [
        'ID'                              => 'id',
        'Course name'                     => 'designation',
        'Created by'                      => 'creatorName',
        'Beginning'                       => 'beginning',
        'Ending'                          => 'ending',
        'Total modules'                   => 'totalModules',
        //'Total modules with rated'        => 'totalRatedModules',
        //'Module with highest rated count' => '',
        'Total expired modules'           => 'totalExpiredModules',
        'Total personal booking modules'  => 'totalModulesPersonalBooking',
        'Total learners'                  => 'totalLearners',
        'Total trainers'                  => 'totalTrainers',
        'Is course expired'               => 'isCourseExpired',
        'Have documents'                  => 'haveDocument',
    ];

    /**
     *
     */
    protected function configure()
    {
        $this->setName('reporting:courses');
    }

    /**
     * @return array
     */
    protected function getReports(): array
    {
        $em         = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement  = $connection->prepare(
            'SELECT
                intervention.*,
                CONCAT( creator.first_name, \' \', creator.last_name ) AS creatorName,
                COUNT( learners.id ) AS totalLearners,
                COUNT( module.id ) AS totalModules,                
                IF ( intervention.ending > CURDATE(), \'expired\', \'\' ) AS isCourseExpired,
                IF ( intervention.fileDescriptor_id IS NULL OR  intervention.fileDescriptor_id = \'\', 0, 1 ) AS haveDocument 
            FROM
                intervention AS intervention
            LEFT JOIN `profile` AS creator ON creator.id = intervention.admin_id
            LEFT JOIN `module` AS `module` ON module.intervention_id = intervention.id 
            LEFT JOIN learner_intervention AS learners ON learners.intervention_id = intervention.id
            GROUP BY
            intervention.id'
        );

        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * @param int $rowIndex
     * @param array $course
     * @param Worksheet $worksheet
     * @return mixed|void
     */
    protected function processReport(int $rowIndex, array $course, Worksheet $worksheet)
    {
        $colIndex = 0;
        foreach ($this->headers as $colName => $fieldName) {
            if (empty($fieldName)) {
                continue;
            }
            $colIndex++;
            if (isset($course[$fieldName])) {
                $worksheet->setCellValueByColumnAndRow($colIndex, $rowIndex + 2, $course[$fieldName]);
                continue;
            }

            if (!method_exists($this, 'get' . ucfirst($fieldName))) {
                $worksheet->setCellValueByColumnAndRow($colIndex, $rowIndex + 2, null);
                continue;
            }

            $values = call_user_func_array([$this, 'get' . ucfirst($fieldName)], [$course]);

            if (is_array($values)) {
                foreach ($values as $index => $value) {
                    $worksheet->setCellValueByColumnAndRow(
                        $colIndex,
                        $rowIndex + 2,
                        $value
                    );

                    $colIndex++;
                }
                continue;
            }

            $worksheet->setCellValueByColumnAndRow(
                $colIndex,
                $rowIndex + 2,
                $values
            );
        }
    }

    /**
     * @param array $course
     * @return \ApiBundle\Entity\AssigmentResourceSystem[]|\ApiBundle\Entity\Intervention[]|\ApiBundle\Entity\LearnerIntervention[]|\ApiBundle\Entity\Module[]|array|int|object[]
     */
    private function getTotalTrainers(array $course)
    {
        $modules       = $this->getModules(['intervention' => $course['id']]);
        $totalTrainers = 0;

        foreach ($modules as $module) {
            $totalTrainers += count($this->getModuleTrainers(['module' => $module->getId()]));
        }

        return $totalTrainers;
    }

    /**
     * @param array $course
     * @return int|void
     */
    private function getTotalModulesPersonalBooking(array $course)
    {
        return count($this->getModules(['intervention' => $course['id'], 'isPersonalBooking' => 1]));
    }

    /**
     * @param array $course
     * @return false|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getTotalExpiredModules(array $course)
    {
        $em = $this->getEntityManager();
        /**
         * @var \Doctrine\DBAL\Connection $connection
         */
        $connection = $em->getConnection();
        $statement  = $connection->prepare(
            'SELECT
                COUNT(`module`.`id`)                 
            FROM
                `module` AS `module`
            WHERE
                `module`.`ending` > CURDATE()
                AND `module`.`intervention_id` = :courseId'
        );
        $statement->bindValue('courseId', $course['id']);
        $statement->execute();

        return $statement->fetchColumn();
    }

    private function getTotalRatedModules(array $course)
    {
        $em = $this->getEntityManager();
        /**
         * @var \Doctrine\DBAL\Connection $connection
         */
        $connection = $em->getConnection();
        $statement  = $connection->prepare(
            '
                SELECT
                        module.designation AS moduleName,
                        COUNT(module_response.module_id) as totalRated,
                        SUM(module_response.notation) AS ratedScores,
                        AVG(module_response.notation) AS avgScores
                FROM
                    `module` AS `module`
                INNER JOIN module_response AS module_response
                    ON
                        module_response.module_id = module.id
                WHERE module.intervention_id  = :courseId 
                    GROUP BY module.id
                    ORDER BY `totalRated`  DESC
               '
        );
        $statement->bindValue('courseId', $course['id']);
        $statement->execute();

        $modulesScores = $statement->fetchAll();

        if (!$modulesScores) {
            return;
        }

        return [
            'totalModulesWithRated'   => count($modulesScores),
            'highestTotalRatedModuleId' => reset($modulesScores)['moduleName'],
        ];
    }
}
