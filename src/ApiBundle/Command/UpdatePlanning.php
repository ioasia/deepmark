<?php

namespace ApiBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class UpdatePlanning
 * @package ApiBundle\Command
 */
class UpdatePlanning extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('alerts:update-planning')
            ->setDescription('Using for send alert email to remind the update planning')
            ->addArgument(
                'days',
                InputArgument::REQUIRED,
                'Warnings in what days? Example: 30, 60,... (days)'
            )
            ->addArgument(
                'domain',
                InputArgument::REQUIRED,
                'The domain of website'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('alerts');

        $service = $this->getContainer()->get('alerts_service');
        $service->alertUpdatePlanning($this->getContainer(), $input->getArgument('days'),
            $input->getArgument('domain'));

        $event = $stopwatch->stop('alerts');

        $dt = new DateTime('NOW');
        $output->writeln('<comment>' . $dt->format('Y-m-d H:i:s')
            . ' Alert run in ' . $event->getDuration() / 1000 . ' seconds' . '</comment>');
    }
}
