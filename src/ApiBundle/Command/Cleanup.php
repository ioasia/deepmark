<?php

namespace ApiBundle\Command;

use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileDocument;
use ApiBundle\Entity\ScormSynthesis;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\WorkingHours;
use AppBundle\Entity\Person;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Cleanup
 * @package ApiBundle\Command
 */
class Cleanup extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this->setName('cleanup:default');
        $this
            ->addOption(
                'keyword',
                null,
                InputOption::VALUE_OPTIONAL
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em    = $this->getContainer()->get('doctrine')->getManager();
        $users = [];

        if ($keyword = explode(",",$input->getOption('keyword'))) {
            /**
             * @var QueryBuilder $qb
             */
            $qb    = $em->getRepository(Person::class)->createQueryBuilder('p');
            $users = $qb
                ->where('p.id > :id')
                ->setParameter('id', $keyword)
                ->getQuery()->getResult();
        }

        if (empty($users)) {
            return;
        }

        foreach ($users as $user) {
            try {
                $profile               = $em->getRepository(Profile::class)->findOneBy(['person' => $user]);
                $learnerInterventions  = $em->getRepository(LearnerIntervention::class)->findBy(['learner' => $profile]);
                $workHours             = $em->getRepository(WorkingHours::class)->findBy(['profile' => $profile]);
                $calendars             = $em->getRepository(AvailableCalendar::class)->findBy(['profile' => $profile]);
                $learnerGroups         = $em->getRepository(LearnerGroup::class)->findBy(['learner' => $profile]);
                $learnerVerieties      = $em->getRepository(SkillResourceVarietyProfile::class)->findBy(['profile' => $profile]);
                $learnerDocuments      = $em->getRepository(ProfileDocument::class)->findBy(['profile' => $profile]);
                $learnerScormSynthesis = $em->getRepository(ScormSynthesis::class)->findBy(['profile' => $profile]);
                $learnerActivities     = $em->getRepository(Activities::class)->findBy(['profile' => $profile]);
                $learnerRespones       = $em->getRepository(ModuleResponse::class)->findBy(['learner' => $profile]);
                $moduleIterations      = $em->getRepository(ModuleIteration::class)->findBy(['learner' => $profile]);

                foreach ($learnerInterventions as $learnerIntervention) {
                    if ($learnerIntervention) {
                        $em->remove($learnerIntervention);
                    }
                }

                foreach ($workHours as $workHour) {
                    if ($workHour) {
                        $em->remove($workHour);
                    }
                }

                foreach ($calendars as $calendar) {
                    if ($calendar) {
                        $em->remove($calendar);
                    }
                }

                foreach ($learnerGroups as $learnerGroup) {
                    if ($learnerGroup) {
                        $em->remove($learnerGroup);
                    }
                }

                foreach ($learnerVerieties as $learnerVeriety) {
                    if ($learnerVeriety) {
                        $em->remove($learnerVeriety);
                    }
                }

                foreach ($learnerDocuments as $learnerDocument) {
                    if ($learnerDocument) {
                        $em->remove($learnerDocument);
                    }
                }

                foreach ($learnerScormSynthesis as $learnerScormSynthesi) {
                    if ($learnerScormSynthesi) {
                        $em->remove($learnerScormSynthesi);
                    }
                }

                foreach ($learnerActivities as $learnerActivity) {
                    if ($learnerActivity) {
                        $em->remove($learnerActivity);
                    }
                }

                foreach ($learnerRespones as $learnerRespone) {
                    if ($learnerRespone) {
                        $em->remove($learnerRespone);
                    }
                }

                foreach ($moduleIterations as $moduleIteration) {
                    if ($moduleIteration) {
                        $em->remove($moduleIteration);
                    }
                }

                $em->remove($user);
                $em->remove($profile);
                $em->flush();
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    }
}
