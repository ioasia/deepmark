<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\ProfileVariety;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EmailController extends Controller
{
    public function interventionAction()
    {
        $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        return $this->render('ApiBundle:Email:intervention.html.twig', array(
            'intervention' => $interventions[0],
        ));
    }

    public function learnerBookingAction()
    {
        $profileType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );

        $trainers = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array(
            'profileType' => $profileType,
        ));

        return $this->render('ApiBundle:Email:learnerBooking.html.twig', array(
            'name' => 'TEST',
            'start' => new \DateTime(),
            'end' => new \DateTime(),
            'profile' => $trainers[0],
            'joinUrl' => 'https://google.com',
        ));
    }

    public function trainerReservationAction()
    {
        $profileType = $this->getDoctrine()->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::LEARNER,
            )
        );

        $learners = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findBy(array(
            'profileType' => $profileType,
        ));

        return $this->render('ApiBundle:Email:learnerBooking.html.twig', array(
            'name' => 'TEST',
            'start' => new \DateTime(),
            'end' => new \DateTime(),
            'profile' => $learners[0],
            'joinUrl' => 'https://google.com',
        ));
    }
}
