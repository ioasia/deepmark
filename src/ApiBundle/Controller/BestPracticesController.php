<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\BestPractice;
use ApiBundle\Entity\BestPracticeResponse;
use ApiBundle\Entity\LastSearchResult;
use ApiBundle\Entity\MyFavorite;
use ApiBundle\Entity\Tag;
use ApiBundle\Entity\FileDescriptor;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Traits\IsAvailable;
use DateTime;

class BestPracticesController extends RestController
{
    use IsAvailable;
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchBestPracticesAction(Request $request)
    {
        $now = new DateTime('now');
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $options = $request->request->all();

        if($options['realClick'] === "true"){
            $lastSearch = $this->getDoctrine()->getRepository('ApiBundle:LastSearchResult')->findOneBy(['profile' => $profile]);
            if(!$lastSearch) {
                $lastSearch = new LastSearchResult();
            }
            $lastSearch->setProfile($profile);
            $lastSearch->setParams(json_encode($options));
            $this->getDoctrine()->getManager()->persist($lastSearch);
            $this->getDoctrine()->getManager()->flush();
        }
        $bestPractices = [];
        if(!empty($options['tab']) && $options['tab'] == "2") {
            // favorite best practices
            $favorites = $this->getDoctrine()->getRepository('ApiBundle:MyFavorite')->searchFavoriteBestPractices($options, $profile, $now);
            foreach ($favorites as $favorite){
                $bestPractices[] = $favorite->getBestPractice();
            }
        } elseif(!empty($options['tab']) && $options['tab'] == "1") {
            $bestPractices = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->searchBestPractices($options, $profile, $now);
        } else {
            $bestPractices['aSubmitted'] = $bestPractices['bValidated'] = $bestPractices['cRejected'] = [];
            $myBestPractices = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->searchBestPractices($options, $profile, $now);
            foreach ($myBestPractices as $bestPractice) {
                if ($bestPractice->getStatus() == BestPractice::STATUS_SUBMITTED) {
                    $bestPractices['aSubmitted'][] = $bestPractice;
                } elseif ($bestPractice->getStatus() == BestPractice::STATUS_APPROVED) {
                    $bestPractices['bValidated'][] = $bestPractice;
                } elseif ($bestPractice->getStatus() == BestPractice::STATUS_REJECTED) {
                    $bestPractices['cRejected'][] = $bestPractice;
                }
            }
            ksort($bestPractices);
        }

        $view = $this->renderView('molecules/best-practices/search_best_practices.html.twig', [
            'bestPractices' => $bestPractices
        ]);
        return new JsonResponse($view);
    }

    public function getLastSearchBestPracticesAction(Request $request)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $lastSearch = $this->getDoctrine()->getRepository('ApiBundle:LastSearchResult')->findOneBy(['profile' => $profile]);

        return new JsonResponse($lastSearch ? json_decode($lastSearch->getParams()) : null);
    }

    /**
     * POST bestPractice.
     *
     * @Rest\Post("review")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="bestPracticeId", nullable=false, strict=true, description="best practice Id.")
     * @Rest\RequestParam(name="notation", nullable=false, strict=true, description="Raing star")
     * @Rest\RequestParam(name="designation", nullable=true, strict=true, description="comment review")
     *
     * @return Response
     */
    public function reviewAction(ParamFetcher $paramFetcher)
    {
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $id = $paramFetcher->get('bestPracticeId');
        $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->find($id);
        $notation = $paramFetcher->get('notation');
        $designation = $paramFetcher->get('designation');
        $em = $this->getDoctrine()->getManager();

        if($bestPractice) {
            $response = new BestPracticeResponse();
            $response->setBestPractice($bestPractice);
            $response->setDateResponse(new \DateTime());
            $response->setProfile($profile);
            $response->setNotation($notation);
            $response->setDesignation($designation);
            $em->persist($response);
            $responses = $this->getDoctrine()->getRepository('ApiBundle:BestPracticeResponse')->findBy([
                'bestPractice' => $bestPractice
            ]);
            $total = $notation;
            $num = 1;
            foreach ($responses as $res){
                $total += $res->getNotation();
                $num++;
            }
            $average = round($total/$num, 1);
            $bestPractice->setRating($average);
            $em->persist($bestPractice);
            $em->flush();
        }

        return new JsonResponse('OK');

    }

    /**
     * POST bestPractice.
     *
     * @Rest\Post("add-external")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="titleBestPractice", nullable=true, strict=true, description="best Practice Title.")
     * @Rest\RequestParam(name="descriptionBestPractice", nullable=true, strict=true, description="best Practice Desc.")
     * @Rest\FileParam(name="fileUploadBestPractice", nullable=true, strict=true, description="file upload")
     * @Rest\RequestParam(name="urlBestPractice", nullable=true, strict=true, description="url")
     * @Rest\RequestParam(name="textBestPractice", nullable=true, strict=true, description="text editor")
     * @Rest\FileParam(name="audioBestPractice", nullable=true, strict=true, description="audio")
     * @Rest\FileParam(name="videoBestPractice", nullable=true, strict=true, description="video")
     * @Rest\RequestParam(name="tagsBestPractice", nullable=true, strict=true, description="tags")
     * @Rest\RequestParam(name="draft", nullable=true, strict=true, description="Draft")
     * @Rest\RequestParam(name="orderBestPractice", nullable=true, strict=true, description="tags")
     *
     * @return Response
     */
    public function addExternalAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $draft = $paramFetcher->get('draft');
        $bestPracticeTitle = $paramFetcher->get('titleBestPractice');
        $bestPracticeDesc = $paramFetcher->get('descriptionBestPractice');
        $tags = $paramFetcher->get('tagsBestPractice');
        $orders = (explode(",",$paramFetcher->get('orderBestPractice')));

        try {
            $person = $this->getUser();
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);
            $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')
                ->find($profile->getEntity());
            // remove the draft vision
            $draftBestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')
                ->findOneBy(['draft' => $profile->getId()]);
            if ($draftBestPractice) {
                $filesOfBestPractice = $draftBestPractice->getFileDescriptors();
                foreach ($filesOfBestPractice as $fileDescriptor) {
                    @unlink($fileDescriptor->getDirectory().$fileDescriptor->getPath());
                    $em->remove($fileDescriptor);
                    $em->flush($fileDescriptor);
                }
                $em->remove($draftBestPractice);
                $em->flush($draftBestPractice);
            }
            // end remove the draft vision
            $error = false;
            $bestPractice = new BestPractice();
            $bestPractice->setDesignation($bestPracticeTitle);
            $bestPractice->setDescription($bestPracticeDesc);
            $bestPractice->setProfile($profile);
            if ($draft) {
                $bestPractice->setDraft($draft);
            }
            $bestPractice->setEntity($entity);
            $bestPractice->setResourceId(0);
            $bestPractice->setResourceType(BestPractice::RESOURCE_TYPE_EXTERNAL);
            $bestPractice->setStatus(BestPractice::STATUS_SUBMITTED);
            if ($tags) {
                $tags = explode(',', $tags);
                foreach ($tags as $tag) {
                    if (($persistedTag = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findOneBy(['tagName' => $tag])) == null) {
                        $persistedTag = new Tag();
                        $persistedTag->setTagName($tag);
                        $em->persist($persistedTag);
                    }
                    $bestPractice->addTag($persistedTag);
                }
            }
            $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/bestPracticeFiles/';

            if($fileUpload = $paramFetcher->get('fileUploadBestPractice')){
                $originalName = $fileUpload->getClientOriginalName();
                $arrFileName = explode('.', $originalName);
                $extension  = end($arrFileName);
                $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($originalName);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize($fileUpload->getSize());
                $fileDescriptor->setMimeType(BestPractice::DOCUMENT_TYPE_FILE);
                $fileDescriptor->setDirectory($targetDir);
                $order = array_search('fileUploadBestPractice', $orders);
                $fileDescriptor->setPosition($order);

                $fileUpload->move($targetDir, $fileName);
                $em->persist($fileDescriptor);
                $bestPractice->addFileDescriptor($fileDescriptor);
            }

            if($paramFetcher->get('urlBestPractice')){
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($paramFetcher->get('urlBestPractice'));
                $fileDescriptor->setPath($paramFetcher->get('urlBestPractice'));
                $fileDescriptor->setMimeType(BestPractice::DOCUMENT_TYPE_URL);
                $fileDescriptor->setDirectory('/');
                $order = array_search('urlBestPractice', $orders);
                $fileDescriptor->setPosition($order);

                $em->persist($fileDescriptor);
                $bestPractice->addFileDescriptor($fileDescriptor);
            }

            if($paramFetcher->get('textBestPractice')){
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($paramFetcher->get('textBestPractice'));
                $fileDescriptor->setPath('text');
                $fileDescriptor->setMimeType(BestPractice::DOCUMENT_TYPE_TEXT);
                $fileDescriptor->setDirectory('/');
                $order = array_search('textBestPractice', $orders);
                $fileDescriptor->setPosition($order);

                $em->persist($fileDescriptor);
                $bestPractice->addFileDescriptor($fileDescriptor);
            }

            if($fileUpload = $paramFetcher->get('audioBestPractice')){
                $originalName = $fileUpload->getClientOriginalName();
                $extension  = 'webm';
                $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($originalName);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize($fileUpload->getSize());
                $fileDescriptor->setMimeType(BestPractice::DOCUMENT_TYPE_AUDIO);
                $fileDescriptor->setDirectory($targetDir);
                $order = array_search('audioBestPractice', $orders);
                $fileDescriptor->setPosition($order);

                $fileUpload->move($targetDir, $fileName);
                $em->persist($fileDescriptor);
                $bestPractice->addFileDescriptor($fileDescriptor);
            }

            if($fileUpload = $paramFetcher->get('videoBestPractice')){
                $originalName = $fileUpload->getClientOriginalName();
                $extension  = 'webm';
                $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
                $fileDescriptor = new FileDescriptor();
                $fileDescriptor->setName($originalName);
                $fileDescriptor->setPath($fileName);
                $fileDescriptor->setSize($fileUpload->getSize());
                $fileDescriptor->setMimeType(BestPractice::DOCUMENT_TYPE_VIDEO);
                $fileDescriptor->setDirectory($targetDir);
                $order = array_search('videoBestPractice', $orders);
                $fileDescriptor->setPosition($order);

                $fileUpload->move($targetDir, $fileName);
                $em->persist($fileDescriptor);
                $bestPractice->addFileDescriptor($fileDescriptor);
            }

            $em->persist($bestPractice); //persist the object
            $em->flush(); //save it to the db

        } catch (Exception $e) {
            $error = true;
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = [
            'status'    => $error ? 1 : 0,
            'url'       => isset($bestPractice) ? $this->generateUrl('learner_best_practices_view', ['id' => $bestPractice->getId()], 0) : 0,
            'message'   => $error ? $translator->trans('global.best_practice_has_added', ['%best_practice%' => $bestPracticeTitle]) : $translator->trans('global.best_practice_added_success', ['%best_practice%' => $bestPracticeTitle])];
        return new JsonResponse($returnData);
    }

    /**
     * POST bestPractice.
     *
     * @Rest\Post("add")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="bestPracticeType", nullable=true, strict=true, description="best Practice Type.")
     * @Rest\RequestParam(name="bestPracticeTitle", nullable=true, strict=true, description="best Practice Title.")
     * @Rest\RequestParam(name="bestPracticeDesc", nullable=true, strict=true, description="best Practice Desc.")
     * @Rest\RequestParam(name="resourceType", nullable=true, strict=true, description="resource Type.")
     * @Rest\RequestParam(name="resourceId", nullable=true, strict=true, description="resource Id.")
     * @Rest\RequestParam(name="moduleId", nullable=true, strict=true, description="module Id.")
     * @Rest\RequestParam(name="entityId", nullable=true, strict=true, description="entity Id.")
     * @Rest\RequestParam(name="tags", nullable=true, strict=true, description="tags.")
     *
     * @return Response
     */
    public function addAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $bestPracticeType = $paramFetcher->get('bestPracticeType');
        $bestPracticeTypeArray = explode("|",$bestPracticeType);
        $bestPracticeTitle = $paramFetcher->get('bestPracticeTitle');
        $bestPracticeDesc = $paramFetcher->get('bestPracticeDesc');
        $resourceType = $paramFetcher->get('resourceType');
        $resourceId = $paramFetcher->get('resourceId');
        $moduleId = $paramFetcher->get('moduleId');
        $entityId = $paramFetcher->get('entityId');
        $tags = $paramFetcher->get('tags');

        try {
            $person = $this->getUser();
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                ->findOneBy(['person' => $person]);
            $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')
                ->find($entityId);
            $module = $this->getDoctrine()->getRepository('ApiBundle:Module')
                ->find($moduleId);
            $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->findBy([
                //'profile' => $profile,
                'module' => $module,
                'entity' => $entity,
                'resourceId' => $resourceId,
                'resourceType' => $resourceType,
                'resourceParam' => $bestPracticeTypeArray[0],
                'documentType' => $bestPracticeTypeArray[1],
            ]);
            $error = false;
            if (!$bestPractice) {
                $bestPractice = new BestPractice();
                $bestPractice->setDesignation($bestPracticeTitle);
                $bestPractice->setDescription($bestPracticeDesc);
                $bestPractice->setProfile($profile);
                $bestPractice->setModule($module);
                $bestPractice->setEntity($entity);
                $bestPractice->setResourceId($resourceId);
                $bestPractice->setResourceType($resourceType);
                $bestPractice->setResourceParam($bestPracticeTypeArray[0]);
                $bestPractice->setDocumentType($bestPracticeTypeArray[1]);
                $bestPractice->setStatus(BestPractice::STATUS_SUBMITTED);
                if ($tags) {
                    $tags = explode(',', $tags);
                    foreach ($tags as $tag) {
                        if (($persistedTag = $this->getDoctrine()->getRepository('ApiBundle:Tag')->findOneBy(['tagName' => $tag])) == null) {
                            $persistedTag = new Tag();
                            $persistedTag->setTagName($tag);
                            $em->persist($persistedTag);
                        }
                        $bestPractice->addTag($persistedTag);
                    }
                }
                $em->persist($bestPractice); //persist the object
                $em->flush($bestPractice); //save it to the db
            } else {
                $error = true;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = [
            'status'    => $error ? 1 : 0,
            'message'   => $error ? $translator->trans('global.best_practice_has_added', ['%best_practice%' => $bestPracticeTitle]) : $translator->trans('global.best_practice_added_success', ['%best_practice%' => $bestPracticeTitle])];
        return new JsonResponse($returnData);
    }

    /**
     * POST favorite.
     *
     * @Rest\Post("add-favorite")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="bestPracticeId", nullable=true, strict=true, description="best Practice Id.")
     * @Rest\RequestParam(name="favoriteId", nullable=true, strict=true, description="favorite Id.")
     *
     * @return Response
     */
    public function addFavoriteAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $bestPracticeId = $paramFetcher->get('bestPracticeId');
        $favoriteId = $paramFetcher->get('favoriteId');
        $message = '';
        $status = 0;
        try {
            if (!$favoriteId) {
                $person = $this->getUser();
                $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->findOneBy(['person' => $person]);
                $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->find($bestPracticeId);
                $favorite = new MyFavorite();
                $favorite->setProfile($profile);
                $favorite->setBestPractice($bestPractice);
                $favorite->setDesignation('Testing');
                $em->persist($favorite); //persist the object
                $em->flush($favorite); //save it to the db
                $message = 'Added';
                $favoriteId = $favorite->getId();
                $status = 1;
            } else {
                $myFavorite = $this->getDoctrine()->getRepository('ApiBundle:MyFavorite')->find($favoriteId);
                if ($myFavorite) {
                    $em->remove($myFavorite); //persist the object
                    $em->flush($myFavorite); //save it to the db
                    $message = 'Removed';
                    $status = 1;
                }
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = ['status' => $status, 'message' => $message, 'favoriteId' => $favoriteId];
        return new JsonResponse($returnData);
    }

    /**
     * POST favorite.
     *
     * @Rest\Post("publish")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="id", nullable=true, strict=true, description="Id.")
     * @Rest\RequestParam(name="entityId", nullable=true, strict=true, description="entity Id.")
     * @Rest\RequestParam(name="publishDate", nullable=true, strict=true, description="publish Date.")
     *
     * @return Response
     */
    public function publishAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $paramFetcher->get('id');
        $entityId = $paramFetcher->get('entityId');
        $publishDate    = new \DateTime($paramFetcher->get('publishDate'));
        $message = '';
        $status = 0;
        try {
            if ($id && $entityId && $publishDate) {
                $bestPractice = $this->getDoctrine()->getRepository('ApiBundle:BestPractice')->find($id);
                $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')->find($entityId);
                $bestPractice->setPublishDate($publishDate);
                $bestPractice->setEntity($entity);
                $em->persist($bestPractice); //persist the object
                $em->flush($bestPractice); //save it to the db
                $message = 'Added';
                $status = 1;
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = ['status' => $status, 'message' => $message];
        return new JsonResponse($returnData);
    }

    public function getUserInfoAction(Request $request)
    {
        $profileId = $request->get('profileId');
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->find($profileId);
        $ranking      = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($profile);

        $view = $this->renderView('molecules/modal/best-practice-user-info.html.twig', [
            'profile' => $profile,
            'ranking' => $ranking
        ]);
        return new JsonResponse($view);
    }
}
