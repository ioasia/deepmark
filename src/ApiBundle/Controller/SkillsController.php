<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;

class SkillsController extends RestController
{
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getSkillRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Skill');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * @param $designation
     *
     * @return LiveResourceVariety|object
     */
    private function getLiveResourceVarietyByDesignation($designation)
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
            'designation' => $designation,
        ));
    }

    /**
     * GET Skill.
     *
     * @param int          $id           skillId
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSkillAction($id)
    {
        $skill = $this->getSkillRepository()->findOneBy(array(
            'id' => $id,
        ));

        if (!$skill) {
            return $this->notFound('Skill not found');
        }

        $view = View::create();

        $view->setData($skill);

        return $this->handleView($view);
    }

    /**
     * GET Subjects.
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSubjectsAction()
    {
        $skill = $this->getSubjectRepository()->findAll();
        if (!$skill) {
            return $this->notFound('Skill not found');
        }
        $view = View::create();
        //$context = new Context();
        //$context->addGroups(['skill']);
        //$view->setContext($context);
        $view->setData($skill);

        return $this->handleView($view);
    }

    /**
     * GET list skills.
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @QueryParam(name="page", requirements="\d+", default="0", description="Page Number ?page=1")
     * @QueryParam(name="limit", requirements="\d+", default="10", description="Limit result ?limit=10")
     */
    public function getSkillsAction(ParamFetcher $paramFetcher)
    {
        $page = $paramFetcher->get('page');
        $limit = $paramFetcher->get('limit');

        $skills = $this->getSkillRepository()->findSkills($page, $limit);

        $view = View::create();
        //$context = new Context();
        //$context->addGroups(['skills']);
        //$view->setContext($context);

        $view->setData($skills);

        return $this->handleView($view);
    }

    /**
     * POST SkillResourceVarietyProfile.
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="skill_id", nullable=false, strict=true, description="Skill Id.")
     * @RequestParam(name="score", nullable=false, strict=true, description="Score float.")
     * @RequestParam(name="type", nullable=false, strict=true, description="MENTOR | LIVE_TRAINER.")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postSkillsAction(ParamFetcher $paramFetcher)
    {
        $skillId = $paramFetcher->get('skill_id');
        $score = $paramFetcher->get('score');
        $type = $paramFetcher->get('type');

        $skill = $this->getSkillRepository()->findOneBy(array(
            'id' => $skillId,
        ));

        if (!$skill) {
            return $this->notFound('Skill not found');
        }

        if ('MENTOR' !== $type && 'LIVE_TRAINER' !== $type) {
            return $this->forbid('Provide good type please');
        }

        $user = $this->getUser();

        $profile = $this->getProfileRepository()->findOneBy(array(
            'person' => $user->getId(),
        ));

        $skillResource = new SkillResourceVarietyProfile();
        $skillResource->setSkill($skill);
        $skillResource->setProfile($profile);
        $skillResource->setLiveResourceVariety($this->getLiveResourceVarietyByDesignation($type));
        $skillResource->setScore($score);
        $skillResource->setDateAdd(new \DateTime());

        $view = View::create();
        $errors = $this->get('validator')->validate($skillResource);
        if (0 == count($errors)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($skillResource);
            $em->flush();
            $view->setData($skillResource);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Erreur lors de l\'ajout de votre compétences, veuillez réessayer', $errors);
        }

        return $this->handleView($view);
    }

    private function getSubjectRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Subject');
    }

    /**
     * Return the overall user list.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSkillformAction()
    {
        $skills = $this->getDoctrine()->getRepository('ApiBundle:Skill')->getSkills();
        $response = new JsonResponse($skills);

        return $response;
    }
}
