<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\MyFavorite;
use ApiBundle\Entity\Status;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Traits\IsAvailable;
use Utils\Statistics\BaseStatistics;
use DateTime;

class CatalogController extends RestController
{
    use IsAvailable;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchCatalogAction(Request $request)
    {
        $courses = [];
        $person = $this->getUser();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->findOneBy(['person' => $person]);
        $options = $request->request->all();
        $sort = $request->query->get('sort');

        $now = new DateTime('now');
        $doneStatus = $this->getDoctrine()->getRepository('ApiBundle:Status')
            ->find(Status::DONE);

        if (empty($options['tab']) or $options['tab'] == "1") {
            // all courses
            $interventions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Intervention')->searchInterventions($options, $now, $sort);
            foreach ($interventions as $intervention) {
                // filter by start
                if (isset($options['rateNumber']) && $options['rateNumber'] > 0) {
                    $grade = $this->getDoctrine()->getManager()->getRepository('ApiBundle:ModuleResponse')->findInterventionGrade($intervention->getId());
                    if ($grade[0]['grade'] < $options['rateNumber']) continue;
                }
                $interventionTime = $this->get(BaseStatistics::class)->processInterventionTime($intervention, $profile, $doneStatus);
                // filter by duration
                if ($options['duration'] != '') {
                    $durationArray = explode("|", $options['duration']);
                    if ($interventionTime['totalMinutes'] < $durationArray[0]*60 || $interventionTime['totalMinutes'] > $durationArray[1]*60) continue;
                }
                $totalModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->countModuleByInterventionId($intervention->getId());
                $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($interventionTime['times']);
                $courses[] = array('remainMinutes' => ($interventionTime['totalMinutes'] - $interventionTime['doneMinutes']), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'totalModules' => $totalModules);
            }
        } else if (!empty($options['tab']) && $options['tab'] == "2") {
            // new courses
            $interval = new \DateInterval('P3D'); // 1 week
            $begin  = clone $now;
            $end    = clone $now;
            $begin->sub($interval);
            $end->add($interval);
            $options['new_course']['begin'] = $begin;
            $options['new_course']['end'] = $end;
            $interventions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Intervention')->searchInterventions($options, $now, $sort);
            foreach ($interventions as $intervention) {
                // filter by start
                if (isset($options['rateNumber']) && $options['rateNumber'] > 0) {
                    $grade = $this->getDoctrine()->getManager()->getRepository('ApiBundle:ModuleResponse')->findInterventionGrade($intervention->getId());
                    if ($grade[0]['grade'] < $options['rateNumber']) continue;
                }
                $interventionTime = $this->get(BaseStatistics::class)->processInterventionTime($intervention, $profile, $doneStatus);
                // filter by duration
                if ($options['duration'] != '') {
                    $durationArray = explode("|", $options['duration']);
                    if ($interventionTime['totalMinutes'] < $durationArray[0]*60 || $interventionTime['totalMinutes'] > $durationArray[1]*60) continue;
                }
                $totalModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->countModuleByInterventionId($intervention->getId());
                $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($interventionTime['times']);
                $courses[] = array('remainMinutes' => ($interventionTime['totalMinutes'] - $interventionTime['doneMinutes']), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'totalModules' => $totalModules);
            }
        } else if (!empty($options['tab']) && $options['tab'] == "4") {
            // Popular courses
            $interventions = $this->getDoctrine()->getManager()->getRepository('ApiBundle:Intervention')->searchInterventions($options, $now, $sort);
            foreach ($interventions as $intervention) {
                // filter by start
                $grade = $this->getDoctrine()->getManager()->getRepository('ApiBundle:ModuleResponse')->findInterventionGrade($intervention->getId());
                // Popular course must have the grade > 4
                if ($grade[0]['grade'] < 4 || ($options['rateNumber'] > 0 && $grade[0]['grade'] < $options['rateNumber'] )) continue;

                $interventionTime = $this->get(BaseStatistics::class)->processInterventionTime($intervention, $profile, $doneStatus);
                // filter by duration
                if ($options['duration'] != '') {
                    $durationArray = explode("|", $options['duration']);
                    if ($interventionTime['totalMinutes'] < $durationArray[0]*60 || $interventionTime['totalMinutes'] > $durationArray[1]*60) continue;
                }
                $totalModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->countModuleByInterventionId($intervention->getId());
                $numOfLearner = count($this->getDoctrine()->getManager()->getRepository('ApiBundle:LearnerIntervention')->findBy(array('intervention' => $intervention)));
                $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($interventionTime['times']);
                $courses[] = array('remainMinutes' => ($interventionTime['totalMinutes'] - $interventionTime['doneMinutes']), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'numOfLearner' => $numOfLearner, 'grade' => $grade[0]['grade'], 'totalModules' => $totalModules);
            }
            array_multisort(array_column($courses, 'numOfLearner'), SORT_DESC, $courses);
        } else if (!empty($options['tab']) && $options['tab'] == "5") {
            // favorite intervention
            $favorites = $this->getDoctrine()->getRepository('ApiBundle:MyFavorite')->searchFavoriteInterventions($options, $profile, $sort);
            foreach ($favorites as $favorite){
                $intervention = $favorite->getIntervention();
                // filter by start
                if (isset($options['rateNumber']) && $options['rateNumber'] > 0) {
                    $grade = $this->getDoctrine()->getManager()->getRepository('ApiBundle:ModuleResponse')->findInterventionGrade($intervention->getId());
                    if ($grade[0]['grade'] < $options['rateNumber']) continue;
                }
                if ($intervention->getEnding() > $now) {
                    $interventionTime = $this->get(BaseStatistics::class)->processInterventionTime($intervention, $profile, $doneStatus);
                    // filter by duration
                    if ($options['duration'] != '') {
                        $durationArray = explode("|", $options['duration']);
                        if ($interventionTime['totalMinutes'] < $durationArray[0]*60 || $interventionTime['totalMinutes'] > $durationArray[1]*60) continue;
                    }
                    $totalModules = $this->getDoctrine()->getRepository('ApiBundle:Module')->countModuleByInterventionId($intervention->getId());
                    $totalDuration = $this->get(BaseStatistics::class)->AddPlayTime($interventionTime['times']);
                    $courses[] = array('remainMinutes' => ($interventionTime['totalMinutes'] - $interventionTime['doneMinutes']), 'totalDuration' => $totalDuration, 'intervention' => $intervention, 'totalModules' => $totalModules);
                }
            }
        }

        $view = $this->renderView('molecules/catalog/search_catalog.html.twig', [
            'courses' => $courses,
            'options' => $options
        ]);
        return new JsonResponse($view);
    }

    /**
     * POST favorite.
     *
     * @Rest\Post("add-favorite")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="interventionId", nullable=true, strict=true, description="intervention Id.")
     * @Rest\RequestParam(name="favoriteId", nullable=true, strict=true, description="favorite Id.")
     *
     * @return Response
     */
    public function addFavoriteAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $interventionId = $paramFetcher->get('interventionId');
        $favoriteId = $paramFetcher->get('favoriteId');
        $message = '';
        $status = 0;
        try {
            if (!$favoriteId) {
                $person = $this->getUser();
                $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
                    ->findOneBy(['person' => $person]);
                $intervention = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->find($interventionId);
                $favorite = new MyFavorite();
                $favorite->setProfile($profile);
                $favorite->setIntervention($intervention);
                $favorite->setDesignation('Testing');
                $em->persist($favorite); //persist the object
                $em->flush($favorite); //save it to the db
                $message = 'Added';
                $favoriteId = $favorite->getId();
                $status = 1;
            } else {
                $myFavorite = $this->getDoctrine()->getRepository('ApiBundle:MyFavorite')->find($favoriteId);
                if ($myFavorite) {
                    $em->remove($myFavorite); //persist the object
                    $em->flush($myFavorite); //save it to the db
                    $message = 'Removed';
                    $status = 1;
                }
            }
        } catch (Exception $e) {
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = ['status' => $status, 'message' => $message, 'favoriteId' => $favoriteId];
        return new JsonResponse($returnData);
    }

    public function getUserInfoAction(Request $request)
    {
        $profileId = $request->get('profileId');
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')
            ->find($profileId);
        $ranking = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')->getRankingByTrainer($profile);

        $view = $this->renderView('molecules/modal/best-practice-user-info.html.twig', [
            'profile' => $profile,
            'ranking' => $ranking
        ]);
        return new JsonResponse($view);
    }
}
