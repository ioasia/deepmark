<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Room;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Traits\IsAvailable;
use DateTime;

class UtilityController extends RestController
{
    use IsAvailable;

    /**
     * POST favorite.
     *
     * @Rest\Post("update-room-status")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="roomId", nullable=true, strict=true, description="room Id.")
     * @Rest\RequestParam(name="statusId", nullable=true, strict=true, description="status Id.")
     *
     * @return Response
     */
    public function updateRoomStatusAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $roomId = $paramFetcher->get('roomId');
        $statusId = $paramFetcher->get('statusId');
        $message = '';
        $status = 0;
        try {
            if ($roomId) {
                $room = $this->getDoctrine()->getRepository('ApiBundle:Room')->find($roomId);
                $room->setStatus($statusId ? Room::STATUS_OPEN : Room::STATUS_CLOSE);
                $em->persist($room); //persist the object
                $em->flush($room); //save it to the db
                $message = $statusId ? $translator->trans('buttons.opened') : $translator->trans('buttons.closed');
                $status = 1;
            }
        } catch (Exception $e) {
            $status = 0;
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = ['status' => $status, 'message' => mb_strtoupper($message), 'roomId' => $roomId];
        return new JsonResponse($returnData);
    }

    /**
     * POST favorite.
     *
     * @Rest\Post("change-session-date")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @Rest\RequestParam(name="sessionId", nullable=true, strict=true, description="session Id.")
     * @Rest\RequestParam(name="moduleId", nullable=true, strict=true, description="module Id.")
     * @Rest\RequestParam(name="startDate", nullable=true, strict=true, description="start Date.")
     * @Rest\RequestParam(name="startTime", nullable=true, strict=true, description="start Time.")
     *
     * @return Response
     */
    public function changeSessionDateAction(ParamFetcher $paramFetcher)
    {
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $sessionId = $paramFetcher->get('sessionId');
        $moduleId = $paramFetcher->get('moduleId');
        $startDate = $paramFetcher->get('startDate');
        $startTime = $paramFetcher->get('startTime');

        $message = '';
        $status = 0;
        try {
            if ($sessionId) {
                $session = $this->getDoctrine()->getRepository('ApiBundle:Session')->find($sessionId);
                // process the session date time
                $dateSession = DateTime::createFromFormat('d/m/Y', $startDate);
                $startTime   = new DateTime($startTime);
                $dateSession->setTime($startTime->format('H'), $startTime->format('i'));
                $session->setSessionDate($dateSession);
                $em->persist($session);

                $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->find($moduleId);
                $moduleSession = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession')->findOneBy(['session' => $session]);
                $bookingAgendas = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(
                    [
                        'moduleSession' => $moduleSession,
                        'module' => $module
                    ]);

                if ($bookingAgendas) {
                    foreach ($bookingAgendas as $bookingAgenda) {
                        $bookingAgenda->setBookingDate($session->getSessionDate());
                        $em->persist($bookingAgenda); //persist the object
                    }
                }

                $assignments = $this->getDoctrine()->getRepository('ApiBundle:AssigmentResourceSystem')->findBy(
                    [
                        'module' => $module
                    ]);

                if ($assignments) {
                    $start = $session->getSessionDate();
                    $end = clone $start;
                    $end->setTime($start->format('H') + $module->getDuration()->format('H'), $start->format('i') + $module->getDuration()->format('i'));

                    foreach ($assignments as $assignment) {
                        $assignment->setMinBookingDate($start);
                        $assignment->setMaxBookingDate($end);
                        $em->persist($assignment);
                    }
                }

                $em->flush(); //save it to the db
                $message = $session ? $translator->trans('buttons.opened') : $translator->trans('buttons.closed');
                $status = 1;
            }
        } catch (Exception $e) {
            $status = 0;
            //echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        $returnData = ['status' => $status, 'message' => mb_strtoupper($message), 'sessionId' => $sessionId];
        return new JsonResponse($returnData);
    }

    /**
     * @Rest\Get("embed-checking")
     * GET check allow embed
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @Rest\QueryParam(name="url", nullable=true, description="Url")
     */
    public function embedCheckingAction(ParamFetcher $paramFetcher)
    {
        $url = $paramFetcher->get('url');
        if ($this->isValidURL($url)) {
            $status = $this->allowEmbed($url);
        } else {
            $status = -1;
        }

        $returnData = ['status' => $status];
        return new JsonResponse($returnData);
    }
}
