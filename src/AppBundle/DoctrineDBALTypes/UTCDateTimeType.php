<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 06.03.19
 * Time: 10:37.
 */

namespace AppBundle\DoctrineDBALTypes;

use AppBundle\Utils\UserTimeZone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType;

class UTCDateTimeType extends DateTimeType
{
    /**
     * @var \DateTimeZone
     */
    private static $utc;

    /**
     * @param \DateTime        $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ('CURRENT_TIMESTAMP' === $value) {
            $value = new \DateTime();
        }

        if ($value instanceof \DateTime) {
            $value->setTimezone(new \DateTimeZone('UTC'));
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    /**
     * @param \DateTime        $value
     * @param AbstractPlatform $platform
     *
     * @return \DateTime
     *
     * @throws ConversionException::conversionFailedFormat
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof \DateTime) {
            return $value;
        }

        $converted = \DateTime::createFromFormat(
            $platform->getDateTimeFormatString(), $value, self::$utc ? self::$utc : self::$utc = new \DateTimeZone('UTC')
        );

        if (!$converted) {
            throw ConversionException::conversionFailedFormat(
                $value, $this->getName(), $platform->getDateTimeFormatString()
            );
        }

        $converted->setTimeZone(new \DateTimeZone(UserTimeZone::getTimeZone()));

        return $converted;
    }
}
