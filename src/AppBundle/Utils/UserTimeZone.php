<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 06.03.19
 * Time: 14:28.
 */

namespace AppBundle\Utils;

use AppBundle\Entity\Person;

class UserTimeZone
{
    /** @var Person $user */
    private static $user;

    /** @var string */
    private static $defaultTimeZone = 'UTC';

    public static function setUser(Person $person)
    {
        self::$user = $person;
    }

    public static function setTimeZone(string $timeZone)
    {
        $user = self::$user;

        self::$defaultTimeZone = empty($user->getTimeZone()) ? $timeZone : $user->getTimeZone()->getTimeZone();
    }

    public static function getTimeZone(): string
    {
        return self::$defaultTimeZone;
    }

    public static function getTimeZoneObject(): \DateTimeZone
    {
        return new \DateTimeZone(self::getTimeZone());
    }
}
