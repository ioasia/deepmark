<?php
namespace AppBundle\Helper;

use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\Templating\EngineInterface;

class SocialBarHelper extends Helper
{
    protected $templating;

    public function __construct(EngineInterface $templating)
    {
        $this->templating  = $templating;
    }


    public function socialButtons($parameters)
    {
        return $this->templating->render('AppBundle:helper:socialButtons.html.twig', $parameters);
    }

    public function facebookButton($parameters)
    {
        return $this->templating->render('AppBundle:helper:facebookButton.html.twig', $parameters);
    }

    public function linkedinButton($parameters)
    {
        return $this->templating->render('AppBundle:helper:linkedinButton.html.twig', $parameters);
    }


    public function getName()
    {
        return 'socialButtons';
    }
}