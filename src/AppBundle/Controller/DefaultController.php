<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('fos_user_login', array(), 301);
    }

    /**
     * @Route("/viewer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function viewerAction(Request $request)
    {
        $url = $request->get('url');
        return $this->render('AppBundle:viewer:index.html.twig', array(
            'url'       => $url
        ));
    }
}
