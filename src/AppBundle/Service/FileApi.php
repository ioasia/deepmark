<?php

namespace AppBundle\Service;

use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Profile;
use ApiBundle\Service\UploadFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileApi
{
    private $uploadFile;

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    public function uploadPublicFile(UploadedFile $file)
    {
        return $this->uploadFile->upload($file);
    }

    public function uploadPrivateFile(UploadedFile $file)
    {
        $this->uploadFile->setTargetDirectory('/src/ApiBundle/Resources/files');

        return $this->uploadFile->upload($file);
    }

    /**
     * @param UploadedFile $file
     * @param Profile $profile
     * @return FileDescriptor
     */
    public function uploadEditorFile(UploadedFile $file, Profile $profile)
    {
        $this->uploadFile->setTargetDirectory('/web/files/editor/' . $profile->getId());

        return $this->uploadFile->uploadEditorFile($file);
    }

    public function downloadPrivateFile(FileDescriptor $fileDescriptor)
    {
        $path = $fileDescriptor->getDirectory().'/'.$fileDescriptor->getPath();
        $file = new \SplFileInfo($path);

        return $file;
    }
}
