<?php

namespace AppBundle\Service;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use Doctrine\ORM\EntityManagerInterface;

class Scheduler
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $designation
     *
     * @return LiveResourceVariety|object
     */
    private function getLiveResourceVarietyByDesignation($designation)
    {
        return $this->em->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
            'designation' => $designation,
        ));
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getSkillResourceVarietyProfileRepository()
    {
        return $this->em->getRepository('ApiBundle:SkillResourceVarietyProfile');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getAvailableCalendarRepository()
    {
        return $this->em->getRepository('ApiBundle:AvailableCalendar');
    }

    public function getLiveResourcesBySkill($skill)
    {
        return $this->getSkillResourceVarietyProfileRepository()->findBy(array(
            'skill' => $skill,
            'liveResourceVariety' => $this->getLiveResourceVarietyByDesignation('LIVE_TRAINER'),
        ));
    }

    public function getLiveResourcesAvailableBySkills($skills, \DateTime $startTime, \DateTime $endTime)
    {
        $liveResourcesBySkills = [];
        $availabilities = [];

        foreach ($skills as $skill) {
            $liveResourcesBySkill = $this->getLiveResourcesBySkill($skill);

            foreach ($liveResourcesBySkill as $liveResource) {
                array_push($availabilities, $this->getAvailableCalendarRepository()->getAvailabilityByLiveResource($liveResource, $startTime, $endTime));
            }
        }

        return $availabilities;
    }

    public function updateInterventionLearnerProgress(Intervention $intervention, Profile $learner)
    {
        $interventionLearner = $this->em->getRepository('ApiBundle:LearnerIntervention')->findOneBy(array(
            'intervention' => $intervention,
            'learner' => $learner,
        ));

        return $interventionLearner;
    }

    public function assignInterventionLearner(Intervention $intervention, Profile $learner)
    {
        $interventionLearner = new LearnerIntervention();
        $interventionLearner->setProgression(0);
        $interventionLearner->setLearner($learner);
        $interventionLearner->setIntervention($intervention);
        $status = $this->em->getRepository('ApiBundle:Status')->findOneBy(array(
            'designation' => 'scheduled',
        ));
        $interventionLearner->setStatus($status);
        $this->em->persist($interventionLearner);
        $this->em->flush();

        return $interventionLearner;
    }

    public function assignModuleLiveResource(Profile $liveResource, Module $module)
    {
        $assignmentResource = new AssigmentResourceSystem();
        $assignmentResource->setModule($module);
        $assignmentResource->setLiveResource($liveResource);
        //TODO: Ajout du time module au beginning
        $assignmentResource->setMinBookingDate(new \DateTime());
        $assignmentResource->setMaxBookingDate($module->getBeginning());
        $statusScheduled = $this->em->getRepository('ApiBundle:Status')->findOneBy(array(
                'designation' => 'scheduled',
            )
        );
        $assignmentResource->setStatus($statusScheduled);
        $assignmentResource->setHide(false);
        $this->em->persist($assignmentResource);
        $this->em->flush();

        return $assignmentResource;
    }

    public function bookingModuleLearner(Profile $learner, \DateTime $executionDate, Module $module)
    {
        $moduleIteration = new BookingAgenda();
        $status = $this->em->getRepository('ApiBundle:Status')->findOneBy(['designation' => 'reserved']);

        $moduleIteration->setBookingDate($executionDate);
        $moduleIteration->setLearner($learner);
        $moduleIteration->setModule($module);
        $moduleIteration->setStatus($status);

        //TODO Casser la dispo du live trainer en 2

        $this->em->persist($moduleIteration);
        $this->em->flush();

        return $moduleIteration;
    }

    public function updateAssigmentResource(Profile $profile, BookingAgenda $bookingAgenda)
    {
        $module = $bookingAgenda->getModule();

        $assigment = $this->em->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy(['liveResource' => $profile, 'module' => $module]);

        $firstBegin = $assigment->getMinBookingDate();
        $firstEnd = $bookingAgenda->getBookingDate();

        $end = clone $bookingAgenda->getBookingDate();
        $hour = intval($module->getDuration()->format('H')) + intval($bookingAgenda->getBookingDate()->format('H'));
        $minutes = intval($module->getDuration()->format('m')) + intval($bookingAgenda->getBookingDate()->format('m'));
        $end->setTime($hour, $minutes, 0);

        $secondBegin = $bookingAgenda->getBookingDate()->setTime($end);
        $secondEnd = $assigment->getMaxBookingDate();

        $assigment->setHide(true);

        $firstPart = new AssigmentResourceSystem();
        $firstPart->setModule($assigment->getModule());
        $firstPart->setMinBookingDate($firstBegin);
        $firstPart->setMaxBookingDate($firstEnd);
        $firstPart->setLiveResource($assigment->getLiveResource());
        $firstPart->setHide(false);
        $firstPart->setStatus($assigment->getStatus());

        $secondtPart = new AssigmentResourceSystem();
        $secondtPart->setModule($assigment->getModule());
        $secondtPart->setMinBookingDate($secondBegin);
        $secondtPart->setMaxBookingDate($secondEnd);
        $secondtPart->setLiveResource($assigment->getLiveResource());
        $secondtPart->setHide(false);
        $secondtPart->setStatus($assigment->getStatus());

        $this->em->persist($firstPart);
        $this->em->persist($secondtPart);
        $this->em->persist($assigment);
        $this->em->flush();
    }
}
