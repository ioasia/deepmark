<?php

namespace AppBundle\Service;

use ApiBundle\Entity\Profile;

class EmailApi
{
    private $mailer;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendMailBookingTrainer(Profile $trainer, $name, \DateTime $start, \DateTime $end, Profile $learner)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[DeepMark] Une nouvelle séance de formation a été reservé avec vous')
            ->setFrom(array('team@deepmark.com' => 'Deepmark'))
            ->setTo($trainer->getPerson()->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->twig->render(
                    'ApiBundle:Email:trainerBooking.html.twig', array(
                        'name' => $name,
                        'start' => $start,
                        'end' => $end,
                        'profile' => $learner,
                    )
                )
            )
        ;

        return $this->mailer->send($message);
    }

    public function sendMailBookingLearner(Profile $trainer, $name, \DateTime $start, \DateTime $end, Profile $learner, $files = [], $joinUrl = '')
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[DeepMark] Votre réservation a bien été enregistré')
            ->setFrom(array('team@deepmark.com' => 'Deepmark'))
            ->setTo($learner->getPerson()->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->twig->render(
                    'ApiBundle:Email:learnerBooking.html.twig', array(
                        'name' => $name,
                        'start' => $start,
                        'end' => $end,
                        'profile' => $trainer,
                        'joinUrl' => $joinUrl,
                    )
                )
            )
        ;

        return $this->mailer->send($message);
    }

    public function sendMailInterventionCreation($email, $intervention)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[DeepMark] Nouvelle intervention')
            ->setFrom(array('team@deepmark.com' => 'Deepmark'))
            ->setTo($email)
            ->setContentType('text/html')
            ->setBody(
                $this->twig->render(
                    'ApiBundle:Email:intervention.html.twig', array(
                        'intervention' => $intervention,
                    )
                )
            )
        ;

        return $this->mailer->send($message);
    }
}
