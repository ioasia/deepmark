<?php

namespace AppBundle\Service;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\StoredModule;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Traits\IsAvailable;

class CalendarApi
{
    use IsAvailable;
    private $em;
    private $translator;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    public function getBookingsByTrainer(Profile $trainer = null, \DateTime $dateStart, \DateTime $dateEnd, StoredModule $storedodule = null)
    {
        $booking_array = [];
        $allIndividualBooking = [];

        $assignements = $this->em->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findAssigmentByTrainer($trainer ? $trainer->getId() : null, $dateStart, $dateEnd);
        if($storedodule){
            foreach ($assignements as $assignement) {
                $module = $assignement->getModule();
                if ($storedodule != null && ($module->getStoredModule()->getAppId() == $storedodule->getAppId()) ){
                    $booking = $this->getBookingsByProfile($trainer, $module, $dateStart, $dateEnd);
                    if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                        $allIndividualBooking = array_merge($allIndividualBooking, $booking);
                    } else {
                        $booking_array = $booking_array + $booking;
                    }
                }
            }
        } else {
            foreach ($assignements as $assignement) {
                $module = $assignement->getModule();
                $booking = $this->getBookingsByProfile($trainer, $module, $dateStart, $dateEnd);
                if (StoredModule::ONLINE === $module->getStoredModule()->getAppId()) {
                    $allIndividualBooking = array_merge($allIndividualBooking, $booking);
                    // remove duplicate item by bookingId field in an array
                    $temp = array_unique(array_column($allIndividualBooking, 'bookingId'));
                    $allIndividualBooking = array_intersect_key($allIndividualBooking, $temp);
                } else {
                    $booking_array = $booking_array + $booking;
                }
            }
        }
        return array_merge($allIndividualBooking, $booking_array);
    }

    public function getSessionsByModule(AssigmentResourceSystem $assignement)
    {
        $sessions_array = [];
        $session = $assignement->getModuleSession();
        $module = $assignement->getModule();

        $sessionDate = $session->getSession()->getSessionDate();
        $end = clone $sessionDate;
        $hour = $module->getDuration()->format('G') + $sessionDate->format('G');
        $minutes = $module->getDuration()->format('i') + $sessionDate->format('i');
        $end->setTime($hour, $minutes, 0);
        $color = '#68d2f1';

        switch ($module->getStoredModule()->getAppId()) {
            case StoredModule::ONLINE:
                $color = '#003B5C';
                break;
            case StoredModule::VIRTUAL_CLASS:
                $color = '#045f7f';
                break;
            case StoredModule::ONLINE_WORKSHOP:
                $color = '#68d2f1';
                break;
            case StoredModule::PRESENTATION_ANIMATION:
                $color = '#68d2f1';
                break;
            default:
                break;
        }

        array_push($sessions_array, array(
            'id' => $session->getId(),
            'title' => $sessionDate->format('H:i').' - '.$end->format('H:i').' '.$module->getStoredModule()->getDesignation(),
            'start' => $sessionDate,
            'end' => $end,
            'type' => $module->getStoredModule()->getAppId(),
            'displayType' => 'session',
            'duration' => $module->getDuration()->format('G:i'),
            'backgroundColor' => $color,
            'className' => ['event-booking'],
        ));

        return $sessions_array;
    }

    public function getBookingsByModule(Module $module, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $booking_array = [];

        $bookings = $this->em->getRepository('ApiBundle:BookingAgenda')->findBookingByModule($module->getId(), $dateStart, $dateEnd);

        foreach ($bookings as $booking) {
            $module = $booking->getModule();
            $end = clone $booking->getBookingDate();
            $hour = $module->getDuration()->format('G') + $booking->getBookingDate()->format('G');
            $minutes = $module->getDuration()->format('i') + $booking->getBookingDate()->format('i');
            $end->setTime($hour, $minutes, 0);
            $color = '#68d2f1';

            switch ($module->getStoredModule()->getAppId()) {
                case StoredModule::ONLINE:
                    $color = '#003B5C';
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $color = '#045f7f';
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $color = '#68d2f1';
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $color = '#68d2f1';
                    break;
                default:
                    break;
            }

            array_push($booking_array, array(
                'id' => $booking->getModule()->getId(),
                'title' => $booking->getBookingDate()->format('H:i').' - '.$end->format('H:i').' '.$booking->getModule()->getStoredModule()->getDesignation(),
                'start' => $booking->getBookingDate(),
                'end' => $end,
                'type' => $booking->getModule()->getStoredModule()->getAppId(),
                'displayType' => 'booked',
                'duration' => $booking->getModule()->getDuration()->format('G:i'),
                'backgroundColor' => $color,
                'heading' => $booking->getModule()->getStoredModule()->getDesignation(),
                'startEndDate' => "Booked for the " . $booking->getBookingDate()->format('d/m/Y') . " from " . $booking->getBookingDate()->format('H:i').' to '.$end->format('H:i'),
                'bookingId' => $booking->getId(),
                'bookingStatus' => $booking->getStatus()->getDesignation(),
                'mobile' => $booking->getTrainer()->getMobilePhone(),
                'email' => $booking->getTrainer()->getPerson()->getEmail(),
                'trainer' => $booking->getTrainer()->getDisplayName(),
                'className' => ['event-booking'],
            ));
        }

        return $booking_array;
    }

    public function getBookingsByProfile(Profile $trainer = null, Module $module, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $booking_array = [];
        $bookings = $this->em->getRepository('ApiBundle:BookingAgenda')->findBookingByProfile($trainer ? $trainer : null, $module->getId(), $dateStart, $dateEnd);

        foreach ($bookings as $booking) {
            $sessionId = $booking->getModuleSession() != null ? $booking->getModuleSession()->getId() : 0;

            $module = $booking->getModule();
            $end = clone $booking->getBookingDate();
            $hour = $module->getDuration()->format('G') + $booking->getBookingDate()->format('G');
            $minutes = $module->getDuration()->format('i') + $booking->getBookingDate()->format('i');
            $end->setTime($hour, $minutes, 0);
            $color = '#68d2f1';
            //check session get number learner
            $learnerName = $learnerMobile = $learnerEmail = '';
            $numberLearner = 0;
            if ($module->getStoredModule()->getAppId() != StoredModule::ONLINE) {
                foreach ($bookings as $bookingSession){
                    if($bookingSession->getModuleSession()->getId()  == $sessionId){
                        $numberLearner++;
                    }
                }
            } else {
                $learner = $booking->getLearner();
                $learnerName = $learner->getDisplayName();
                $learnerMobile = $learner->getMobilePhone();
                $learnerEmail = $learner->getPerson()->getEmail();
                $numberLearner = 1;
            }
            $moduleTypeName = $booking->getModule()->getStoredModule()->getDesignation();

            switch ($module->getStoredModule()->getAppId()) {
                case StoredModule::ONLINE:
                    $color = '#003B5C';
                    $moduleTypeName =$this->translator->trans('admin.interventions.mods.onlineseance');
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $color = '#045f7f';
                    $moduleTypeName =$this->translator->trans('admin.interventions.mods.virtualclass');
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $color = '#68d2f1';
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $color = '#68d2f1';
                    $moduleTypeName =$this->translator->trans('admin.interventions.mods.animation');
                    break;
                default:
                    break;
            }
            $available = $this->isAvailableModule($booking->getModule());
            // the module will be expired when the today - the booking date + the duration time > 24 hrs
            $isAvailableBooking = $this->isAvailableBooking($module, $booking);

            // Get street from module Session Place
            $address = '';
            if ($booking->getModuleSession()) {
                $moduleSession = $booking->getModuleSession();
                if ($moduleSession->getSessionPlace()) {
                    $address = $moduleSession->getSessionPlace()->getAddress() . ' - ' . $moduleSession->getSessionPlace()->getPlace();
                }
            }
            $bookingRow = array(
                'id' => $booking->getModule()->getId(),
                'title' => $booking->getBookingDate()->format('H:i').' - '.$end->format('H:i').' '.$moduleTypeName,
                'start' => $booking->getBookingDate(),
                'end' => $end,
                'type' => $booking->getModule()->getStoredModule()->getAppId(),
                'displayType' => 'booked',
                'duration' => $booking->getModule()->getDuration()->format('G:i'),
                'backgroundColor' => $color,
                'heading' => $moduleTypeName,
                'startEndDate' => $booking->getBookingDate()->format('H:i').' '.$this->translator->trans('global.to_txt').' '.$end->format('H:i'),
                'bookingId' => $booking->getId(),
                'bookingStatus' => $booking->getStatus()->getDesignation(),
                'mobile' => $booking->getTrainer()->getMobilePhone(),
                'email' => $booking->getTrainer()->getPerson()->getEmail(),
                'trainer' => $booking->getTrainer()->getDisplayName(),
                'moduleName' => $booking->getModule()->getDesignation(),
                'moduleAddress' => $address,
                'intervention' => $booking->getModule()->getIntervention()->getDesignation(),
                'entity' =>$booking->getLearner()->getEntity()->getOrganisation()->getDesignation().' - '.$booking->getLearner()->getEntity()->getDesignation(),
                'available' => $available && $isAvailableBooking,
                'className' => ['event-booking'],
                'numberLearner' => $numberLearner,
                'learnerName' => $learnerName,
                'learnerMobile' => $learnerMobile,
                'learnerEmail' => $learnerEmail,
            );
            // fixed duplicate module in calendar trainer/learner
            if ($sessionId != 0) {
                $booking_array[$sessionId] = $bookingRow;
            } else {
                $booking_array[] = $bookingRow;
            }
        }
        return $booking_array;
    }

    public function getAvailabilityByTrainer(Profile $trainer, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $availability_data = [];

        $availabilities = $this->em->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($trainer, $dateStart, $dateEnd);

        // remove the working hours and only get available calendar
        /*$workingHours = $this->em->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $trainer]);
        $workingHoursByDate = [];
        if($workingHours){
            foreach ($workingHours as $v){
                if ($v->getStart()){
                    $workingHoursByDate[$v->getName()]['start'] = $v->getStart()->format('H:i');

                }
                if ($v->getEnd()){
                    $workingHoursByDate[$v->getName()]['end'] = $v->getEnd()->format('H:i');
                }
            }
        }*/

        foreach ($availabilities as $available) {
            $freezeStartTime = $freezeEndTime = null;
            $startTime =  $available->getBeginning();
            $endTime = $available->getEnding();
            $availableStartTime = $available->getBeginning()->format('H:i');
            $availableEndTime = $available->getEnding()->format('H:i');

            // get freezeTime to replace the lunch time
            $freezeTime = $available->getFreezeTime();
            if ($freezeTime) {
                $freezeTimeArray = $freezeTime != null ? explode("|", $freezeTime) : null;
                $freezeStartTime = $freezeTimeArray != null ? $freezeTimeArray[0] : null;
                $freezeEndTime = $freezeTimeArray != null ? $freezeTimeArray[1] : null;
            }

            if($freezeStartTime && $freezeEndTime){
                // remove the working hours and only get available calendar
                /*if($workingHoursByDate){
                    $dayOfWeek = $startTime->format('l');
                    if(array_key_exists($dayOfWeek, $workingHoursByDate) && !empty($workingHoursByDate[$dayOfWeek]['start'])){
                        $availableStartTime = strtotime($availableStartTime) < strtotime($workingHoursByDate[$dayOfWeek]['start']) ? $workingHoursByDate[$dayOfWeek]['start'] : $availableStartTime;
                    }
                    if(array_key_exists($dayOfWeek, $workingHoursByDate) && !empty($workingHoursByDate[$dayOfWeek]['end'])){
                        $availableEndTime = strtotime($availableEndTime) > strtotime($workingHoursByDate[$dayOfWeek]['end']) ? $workingHoursByDate[$dayOfWeek]['end'] : $availableEndTime;
                    }
                }*/

                $timeSlot = $availableStartTime . ' - ' .$availableEndTime;

                if(strtotime($availableStartTime) >= strtotime($freezeEndTime) || strtotime($availableEndTime) <= strtotime($freezeEndTime)){
                    $timeSlot = $availableStartTime . ' - ' .$availableEndTime;
                } elseif(strtotime($availableStartTime) < strtotime($freezeStartTime) && strtotime($availableEndTime) > strtotime($freezeEndTime)){
                    $timeSlot = $availableStartTime . ' - ' . $freezeStartTime . ' | ' . $freezeEndTime . ' - ' .$availableEndTime;
                } elseif(strtotime($availableStartTime) >= strtotime($freezeStartTime) && strtotime($availableStartTime) <= strtotime($freezeEndTime) && strtotime($availableEndTime) > strtotime($freezeEndTime)){
                    $timeSlot = $freezeEndTime . ' - ' . $availableEndTime;
                    $startTime =  new DateTime($available->getBeginning()->format("Y-m-d ". $freezeEndTime));
                } elseif(strtotime($availableEndTime) >= strtotime($freezeStartTime) && strtotime($availableEndTime) <= strtotime($freezeEndTime) && strtotime($availableStartTime) < strtotime($freezeStartTime)){
                    $timeSlot = $availableStartTime . ' - ' . $freezeStartTime;
                    $endTime =  new DateTime($available->getBeginning()->format("Y-m-d ". $freezeStartTime));
                }

                array_push($availability_data, array(
                    'id' => $available->getId(),
                    'title' => $timeSlot,
                    'start' => $startTime,
                    'end' => $endTime,
                    'type' => '',
                    'displayType' => 'availability',
                    'duration' => 0,
                    'with' => '',
                    'withLink' => '#',
                    'className' => ['event-availability'],
                ));

            } else {
                // remove the working hours and only get available calendar
                /*if($workingHoursByDate){
                    $dayOfWeek = $startTime->format('l');
                    if(array_key_exists($dayOfWeek, $workingHoursByDate) && !empty($workingHoursByDate[$dayOfWeek]['start'])){
                        $availableStartTime = strtotime($availableStartTime) < strtotime($workingHoursByDate[$dayOfWeek]['start']) ? $workingHoursByDate[$dayOfWeek]['start'] : $availableStartTime;
                    }
                    if(array_key_exists($dayOfWeek, $workingHoursByDate) && !empty($workingHoursByDate[$dayOfWeek]['end'])){
                        $availableEndTime = strtotime($availableEndTime) > strtotime($workingHoursByDate[$dayOfWeek]['end']) ? $workingHoursByDate[$dayOfWeek]['end'] : $availableEndTime;
                    }
                }*/
                array_push($availability_data, array(
                    'id' => $available->getId(),
                    'title' => $availableStartTime.' - '.$availableEndTime,
                    'start' => $startTime,
                    'end' => $endTime,
                    'type' => '',
                    'displayType' => 'availability',
                    'duration' => 0,
                    'with' => '',
                    'withLink' => '#',
                    'className' => ['event-availability'],
                ));
            }
        }

        return $availability_data;
    }
    // todo: this function will be replace the current function
    /*public function getAvailabilityByTrainer(Profile $trainer, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $availability_data = [];

        $availabilities = $this->em->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilityByLiveResource($trainer, $dateStart, $dateEnd);

        foreach ($availabilities as $available) {
            $startTime =  $available->getBeginning();
            $endTime = $available->getEnding();
            $availableStartTime = $available->getBeginning()->format('H:i');
            $availableEndTime = $available->getEnding()->format('H:i');
            array_push($availability_data, array(
                'id' => $available->getId(),
                'title' => $availableStartTime.' - '.$availableEndTime,
                'start' => $startTime,
                'end' => $endTime,
                'type' => '',
                'displayType' => 'availability',
                'duration' => 0,
                'with' => '',
                'withLink' => '#',
                'className' => ['event-availability'],
            ));
        }

        return $availability_data;
    }*/

    public function getAvailabilitySession(Module $module)
    {
        $availability_data = [];

        $sessions = $module->getSessions();

        foreach ($sessions as $session) {
            $assignement = $this->em->getRepository('ApiBundle:AssigmentResourceSystem')
                ->findOneBy(array(
                    'module' => $module,
                    'moduleSession' => $session,
                ));
            $start = $session->getSession()->getSessionDate();
            $end = clone $start;
            $hour = $module->getDuration()->format('G') + $start->format('G');
            $minutes = $module->getDuration()->format('i') + $start->format('i');
            $end->setTime($hour, $minutes, 0);

            array_push($availability_data, array(
                'id' => $module->getId(),
                'title' => $module->getDesignation(),
                'start' => $start,
                'end' => $end,
                'type' => $module->getStoredModule()->getAppId(),
                'displayType' => 'availability',
                'duration' => $module->getDuration()->format('G:i'),
                'with' => $assignement->getLiveResource(),
                'withLink' => '#',
            ));
        }

        return $availability_data;
    }

    public function getAvailabilityOnline(Module $module, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $now = new \DateTime();
        if ($dateStart < $now) {
            $dateStart = $now;
        }
        $availability_data = [];
        // Get Assignements to see which trainers are concerned
        $assignementResources = $this->em
            ->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy(array(
                'module' => $module,
            ));

        // Foreach Trainer assigned get when available
        foreach ($assignementResources as $assignementResource) {
            $liveResource = $assignementResource->getLiveResource();

            $availabilities = $this->em->getRepository('ApiBundle:AvailableCalendar')
                ->findAvailabilityByLiveResource($liveResource, $dateStart, $dateEnd);

            foreach ($availabilities as $available) {
                array_push($availability_data, array(
                    'id' => $module->getId(),
                    'title' => $module->getDesignation(),
                    'start' => $available->getBeginning(),
                    'end' => $available->getEnding(),
                    'type' => $module->getStoredModule()->getAppId(),
                    'displayType' => 'availability',
                    'duration' => $module->getDuration()->format('G:i'),
                    'with' => $liveResource,
                    'withLink' => '#',
                ));
            }
        }

        return $availability_data;
    }

    public function getMyBookingsByModule(Profile $learner, Module $module, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $booking_array = [];
        $bookings = $this->em->getRepository('ApiBundle:BookingAgenda')->findMyBookingByModule($learner, $module->getId(), $dateStart, $dateEnd);
        foreach ($bookings as $booking) {
            $module = $booking->getModule();
            $end = clone $booking->getBookingDate();
            $hour = $module->getDuration()->format('G') + $booking->getBookingDate()->format('G');
            $minutes = $module->getDuration()->format('i') + $booking->getBookingDate()->format('i');
            $end->setTime($hour, $minutes, 0);

            $color = '#ffad17';
            $moduleType = $booking->getModule()->getStoredModule()->getDesignation();

            switch ($module->getStoredModule()->getAppId()) {
                case StoredModule::ONLINE:
                    $color = '#003B5C';
                    $moduleType = $this->translator->trans('admin.interventions.mods.onlineseance');
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $color = '#045f7f';
                    $moduleType = $this->translator->trans('admin.interventions.mods.virtualclass');
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $color = '#659eb1';
                    $moduleType = $this->translator->trans('admin.interventions.mods.workshop');
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $color = '#68d2f1';
                    $moduleType = $this->translator->trans('admin.interventions.mods.animation');
                    break;
                case StoredModule::QUIZ:
                    $color = '#ffa322';
                    $moduleType = $this->translator->trans('admin.interventions.mods.quiz');
                    break;
                case StoredModule::EVALUATION:
                    $color = '#957013';
                    $moduleType = $this->translator->trans('admin.interventions.mods.evaluation');
                    break;
                default:
                    break;
            }
            $available = $this->isAvailableModule($module);
            // the module will be expired when the today - the booking date + the duration time > 24 hrs
            $bookingModule = $this->em->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $learner,
                    'module'  => $module,
                ));
            $isAvailableBooking = $this->isAvailableBooking($module, $bookingModule);

            // Get street from module Session Place
            $address = '';
            if ($booking->getModuleSession()) {
                $moduleSession = $booking->getModuleSession();
                if ($moduleSession->getSessionPlace()) {
                    $address = $moduleSession->getSessionPlace()->getAddress() . ' - ' . $moduleSession->getSessionPlace()->getPlace();
                }
            }

            $bookingRow = array(
                'id' => $booking->getModule()->getId(),
                'title' => $booking->getModule()->getStoredModule()->getDesignation() . ': ' . $booking->getModule()->getDesignation(),
                'start' => $booking->getBookingDate(),
                'end' => $end,
                'type' => $booking->getModule()->getStoredModule()->getAppId(),
                'displayType' => $moduleType,
                'duration' => $booking->getModule()->getDuration()->format('G:i'),
                'trainerName' => $booking->getTrainer()->getDisplayName(),
                'trainerMobile' => $booking->getTrainer()->getMobilePhone(),
                'trainerEmail' => $booking->getTrainer()->getPerson()->getEmail(),
                'moduleName' => $booking->getModule()->getDesignation(),
                'moduleAddress' => $address,
                'bookingStatus' => $booking->getStatus()->getDesignation(),
                'startEndDate' => $booking->getBookingDate()->format('H:i').' '.$this->translator->trans('global.to_txt').' '.$end->format('H:i'),
                'intervention' => $booking->getModule()->getIntervention()->getDesignation(),
                'entity' => $booking->getLearner()->getEntity()->getOrganisation()->getDesignation(),
                'available' => $available && $isAvailableBooking,
                'backgroundColor' => $color,
            );
            if (in_array('ROLE_ADMIN', $learner->getPerson()->getRoles())) {
                $sessionId = $booking->getModuleSession() != null ? $booking->getModuleSession()->getId() : 0;
                if ($sessionId == 0) {
                    $booking_array[] = $bookingRow;
                } else {
                    $booking_array[$sessionId] = $bookingRow;
                }
            } else {
                array_push($booking_array, $bookingRow);
            }
        }

        return $booking_array;
    }
}