<?php

declare(strict_types=1);

namespace AppBundle\Service;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Record\Location;
use Psr\Container\ContainerInterface;

/**
 * Class LocalisationReader
 * @package AppBundle\Service
 */
class LocalisationReader implements LocalisationInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $geoLitePath = '';

    /**
     * @var LocalisationReader
     */
    private $response;

    /**
     * LocalisationReader constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->loadSettings();
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     *
     */
    private function loadSettings(): void
    {
        $this->geoLitePath = $this->container->get('kernel')->getRootDir() . '/geolite_city/GeoLite2-City.mmdb';
    }

    /**
     * @return LocalisationReader|null
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    public function getRequestLocalisation(): ?LocalisationReader
    {
        $reader = new Reader($this->geoLitePath);

        try {
            $record         = $reader->city($this->ip);
            $this->response = $record;
        } catch (AddressNotFoundException $e) {

        }

        return $this;
    }

    /**
     * @return Location|null
     */
    public function getLocation(): ?Location
    {
        return !empty($this->response) ? $this->response->location : null;
    }

    /**
     * @return string|null
     */
    public function getTimeZone(): ?string
    {
        return !empty($this->response) ? $this->response->location->timeZone : null;
    }
}
