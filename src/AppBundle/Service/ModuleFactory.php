<?php

namespace AppBundle\Service;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\Visio;
use Doctrine\ORM\EntityManagerInterface;

class ModuleFactory
{
    private $em;
    private $webinarApi;

    public function __construct(EntityManagerInterface $em, WebinarApi $webinarApi)
    {
        $this->em = $em;
        $this->webinarApi = $webinarApi;
    }

    private function getLiveResourceByModuleSkill($skills, $start, $end)
    {
        $liveResources = [];
        // TODO: We must take all of the skills
        $skill = $skills[0];
        $liveResourcesAvailableBySkill = $this->em
            ->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(array(
                'skill' => $skill, // TODO : skills
                'liveResourceVariety' => $this->em->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(array(
                    'appId' => LiveResourceVariety::LIVE_TRAINER,
                )),
            ));

        foreach ($liveResourcesAvailableBySkill as $liveResourceSkill) {
            $liveResource = $liveResourceSkill->getProfile();

            $isAvailable = $this->em->getRepository('ApiBundle:AvailableCalendar')
                ->findAvailabilityByLiveResource($liveResource, $start, $end);
            // Condition has availability within range
            if ($isAvailable && count($isAvailable) > 0) {
                if (!in_array($liveResource, $liveResources)) {
                    array_push($liveResources, $liveResource);
                }
            }
        }

        return $liveResources;
    }

    private function getLiveResourceByModule($start, $end)
    {
        $liveResources = [];

        $availabilities = $this->em->getRepository('ApiBundle:AvailableCalendar')
            ->findAvailabilities($start, $end);

        foreach ($availabilities as $availability) {
            $liveResource = $availability->getProfile();

            if (!in_array($liveResource, $liveResources)) {
                array_push($liveResources, $liveResource);
            }
        }

        return $liveResources;
    }

    private function nbDays($start, $end)
    {
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);

        return iterator_count($period) ? iterator_count($period) : 1;
    }

    private function getHours(\DateTime $duration)
    {
        $hours = (float) $duration->format('h');
        $minutes = (float) $duration->format('i');

        return $hours + ($minutes / 60);
    }

    public function getLiveResources(Module $module)
    {
        $appId = $module->getStoredModule()->getAppId();

        if (in_array(
            $appId,
            [StoredModule::PRESENTATION_ANIMATION, StoredModule::ONLINE_WORKSHOP, StoredModule::VIRTUAL_CLASS])
        ) {
            return $this->getLiveResourceForWorkshopTypeModule($module);
        }

        if (StoredModule::ONLINE) {
            return $this->getLiveResourceOnlineModule($module);
        }

        return $this->getLiveResourceDefault($module);
    }

    private function getLiveResourceDefault(Module $module)
    {
        $liveResources = [];
        $skills = [];

        $liveResourceRequiredNumber = 0;

        return array(
            'module' => $module,
            'skills' => $skills,
            'liveResources' => $liveResources,
            'liveResourceRequiredNumber' => $liveResourceRequiredNumber,
            'liveResourceAvailable' => count($liveResources),
        );
    }

    private function getLiveResourceOnlineModule(Module $module)
    {
        $start = $module->getBeginning();
        $end = $module->getEnding();

        $nbDays = $this->nbDays($start, $end);
        $hourNeeded = $module->getQuantity() * $this->getHours($module->getDuration());
        $hourPerDay = $hourNeeded / $nbDays;
        $liveResourceRequiredNumber = ceil($hourPerDay / 5);

        $skills = $module->getSkills();
        if ($skills) {
            $liveResources = $this->getLiveResourceByModuleSkill($skills, $start, $end);
        } else {
            $liveResources = $this->getLiveResourceByModule($start, $end);
        }

        return array(
            'module' => $module,
            'skills' => $skills,
            'liveResources' => $liveResources,
            'liveResourceRequiredNumber' => $liveResourceRequiredNumber,
            'liveResourceAvailable' => count($liveResources),
        );
    }

    private function getLiveResourceForWorkshopTypeModule(Module $module)
    {
        $start = $module->getBeginning();
        $end = $module->getEnding();
        $nbDays = $this->nbDays($start, $end);
        $nbSession = count($module->getSessions());
        $hourNeeded = $nbSession * $module->getQuantity() * $this->getHours($module->getDuration());
        $hourPerDay = $hourNeeded / $nbDays;
        $liveResourceRequiredNumber = ceil($hourPerDay / 5);

        $skills = $module->getSkills();
        if ($skills && count($skills) > 0) {
            $liveResources = $this->getLiveResourceByModuleSkill($skills, $start, $end);
        } else {
            $liveResources = $this->getLiveResourceByModule($start, $end);
        }

        return array(
            'module' => $module,
            'skills' => $skills,
            'liveResources' => $liveResources,
            'liveResourceRequiredNumber' => $liveResourceRequiredNumber,
            'liveResourceAvailable' => count($liveResources),
        );
    }

    private function assignByModule($module)
    {
        $assigments = [];
        $result = $this->getLiveResources($module);

        $liveResources = $result['liveResources'];
        $liveResourceRequiredNumber = $result['liveResourceRequiredNumber'];
        $status = $this->em->getRepository('ApiBundle:Status')->findOneBy(array(
            'appId' => Status::WITH_BOOKING_SCHEDULED,
        ));

        $liveResourceCounter = 0;
        foreach ($liveResources as $liveResource) {
            if ($liveResourceCounter >= $liveResourceRequiredNumber) {
                break;
            }
            $newAssigment = new AssigmentResourceSystem();
            $newAssigment->setModule($module);
            $newAssigment->setStatus($status);
            $newAssigment->setMinBookingDate($module->getBeginning());
            $newAssigment->setMaxBookingDate($module->getEnding());
            $newAssigment->setHide(false);
            $newAssigment->setLiveResource($liveResource);
            array_push($assigments, $newAssigment);
            ++$liveResourceCounter;
        }

        return $assigments;
    }

    private function assignBySession($module)
    {
        $assigments = [];
        $sessions = $module->getSessions();

        foreach ($sessions as $session) {
            $result = $this->getLiveResources($module);

            $liveResources = $result['liveResources'];
            $liveResourceRequiredNumber = 1;
            $status = $this->em->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_SCHEDULED,
            ));

            $liveResourceCounter = 0;
            foreach ($liveResources as $liveResource) {
                if ($liveResourceCounter >= $liveResourceRequiredNumber) {
                    break;
                }
                $newAssigment = new AssigmentResourceSystem();
                $newAssigment->setModule($module);
                $newAssigment->setStatus($status);
                $newAssigment->setMinBookingDate($module->getBeginning());
                $newAssigment->setMaxBookingDate($module->getEnding());
                $newAssigment->setHide(false);
                $newAssigment->setLiveResource($liveResource);
                $newAssigment->setModuleSession($session);
                array_push($assigments, $newAssigment);
                ++$liveResourceCounter;
            }
        }

        return $assigments;
    }

    public function assignLiveResource($modules)
    {
        $assigments = [];
        foreach ($modules as $module) {
            $storedModule = $module->getStoredModule()->getAppId();
            switch ($storedModule) {
                case StoredModule::VIRTUAL_CLASS:
                    $assigments = array_merge($assigments, $this->assignBySession($module));
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $assigments = array_merge($assigments, $this->assignBySession($module));
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $assigments = array_merge($assigments, $this->assignBySession($module));
                    break;
                default:
                    $assigments = array_merge($assigments, $this->assignByModule($module));
                    break;
            }
        }

        return $assigments;
    }

    private function createWebinar(AssigmentResourceSystem $assigmentResourceSystem)
    {
        $module = $assigmentResourceSystem->getModule();
        $trainer = $assigmentResourceSystem->getLiveResource();

        $sessions = $module->getSessions();

        if (count($sessions) > 0) {
            foreach ($sessions as $session) {
                $start = $session->getSession()->getSessionDate();
                $end = clone $start;
                $hour = $module->getDuration()->format('G') + $start->format('G');
                $minutes = $module->getDuration()->format('i') + $start->format('i');
                $end->setTime($hour, $minutes, 0);
                $name = 'Webinar with '.$assigmentResourceSystem->getLiveResource()->getDisplayName().' for module '.$module->getDesignation();
                $webinar = $this->webinarApi->createWebinar($name, 'Deepmark', $start, $end);

                if ($webinar) {
                    $visio = new Visio();
                    $visio->setWebinarKey($webinar->webinarKey);
                    $visio->setTrainer($trainer);
                    $visio->setModule($module);
                    $visio->setStartDate($start);
                    $visio->setEndDate($end);

                    $registrant = $this->webinarApi->createRegistrant($webinar->webinarKey, $trainer);
                    $joinUrl = $registrant->joinUrl;
                    $visio->setJoinUrl($joinUrl);

                    return $visio;
                }
            }
        }

        return false;
    }

    public function createWebinars($assigments)
    {
        $visios = [];

        foreach ($assigments as $assigment) {
            $module = $assigment->getModule();
            $storedModule = $module->getStoredModule()->getAppId();

            switch ($storedModule) {
                case StoredModule::VIRTUAL_CLASS:
                    $visio = $this->createWebinar($assigment);
                    if ($visio) {
                        array_push($visios, $visio);
                    }
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $visio = $this->createWebinar($assigment);
                    if ($visio) {
                        array_push($visios, $visio);
                    }
                    break;
                default:
                    break;
            }
        }

        return $visios;
    }
}
