<?php

namespace AppBundle\Service;

use ApiBundle\Entity\Profile;
use ApiBundle\Service\GoToWebinar;

class WebinarApi
{
    private $webinar;

    public function __construct(GoToWebinar $goToWebinar)
    {
        $this->webinar = $goToWebinar;
        // $this->webinar->authenticate();
    }

    public function createWebinar($name, $description, $startTime, $endTime)
    {
        $webinar = $this->webinar->createWebinar(
            $name,
            $description,
            [
                array(
                    'startTime' => $startTime->format('Y-m-d\TH:i:s\Z'),
                    'endTime' => $endTime->format('Y-m-d\TH:i:s\Z'),
                ),
            ],
            'Europe/Paris',
            'single_session',
            false
        );

        return $webinar;
    }

    // Return array
    // joinUrl
    // asset
    // registrantKey
    // status
    public function createRegistrant($webinarKey, Profile $profile)
    {
        $registrant = $this->webinar->createRegistrant(
            $webinarKey,
            $profile->getFirstName(),
            $profile->getLastName(),
            $profile->getPerson()->getEmail(),
            true
        );

        return $registrant;
    }

    public function cancelWebinar($webinarKey)
    {
        return $this->webinar->cancelWebinar($webinarKey);
    }
}
