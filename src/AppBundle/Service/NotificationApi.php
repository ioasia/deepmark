<?php

namespace AppBundle\Service;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Module;
use ApiBundle\Service\NexmoSMS;

class NotificationApi
{
    private $nexmo;

    public function __construct(NexmoSMS $nexmoSMS)
    {
        $this->nexmo = $nexmoSMS;
    }

    public function sendNotificationTrainerSMS($destNumber, Intervention $intervention, Module $module)
    {
        $text = "Bonjour,\r\n
                Un apprennant vient de s'inscrire à la formation".$module->getDesignation()."- de l'intervention "
                .$intervention->getDesignation()."\r\n
                A bientôt,";

        return $this->nexmo->sendSMS($destNumber, $text);
    }

    public function sendNotificationApprenantSMS($destNumber, BookingAgenda $booking)
    {
        $text = "Bonjour,\r\n
                Votre réservation pour le module ".$booking->getModule()->getDesignation()." a bien été prise en compte.\r\n
                Rappel : \r\n
                Date : ".$booking->getBookingDate()->format('Y-m-d G:i')."\r\n
                Durée : ".$booking->getModule()->getDuration()->format('G.i')."min\r\n
                A bientôt,";

        return $this->nexmo->sendSMS($destNumber, $text);
    }

    public function sendNotificationAttachementApprennantSMS($destNumber, Intervention $intervention, $joinUrl)
    {
        $text = "Bonjour,\r\n
                Un administrateur vous a inscrit à la formation".$intervention->getDesignation().'- '
            .$intervention->getCustomer()->getEntity()->getDesignation()."\r\n"
            .$joinUrl."pour accéder à la formation.\r\n
            A bientôt,";

        return $this->nexmo->sendSMS($destNumber, $text);
    }
}
