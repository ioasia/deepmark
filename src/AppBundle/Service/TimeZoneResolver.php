<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 07.03.19
 * Time: 09:33.
 */
declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Entity\Person;
use AppBundle\Entity\PersonTimeZone;
use Psr\Container\ContainerInterface;

/**
 * Class TimeZoneResolver
 * @package AppBundle\Service
 */
class TimeZoneResolver
{
    public const LOCALHOST = '127.0.0.1';

    public const EXCLUDED_IPS = [
        self::LOCALHOST, '172.16.66.1',
    ];

    /** @var LocalisationReader $localisationReader */
    private $localisationReader;

    /** @var ContainerInterface $container */
    private $container;

    /** @var Person $user */
    private $user;

    /** @var string */
    private $timeZone;

    /**
     * TimeZoneResolver constructor.
     * @param LocalisationInterface $LocalisationReader
     * @param ContainerInterface $container
     */
    public function __construct(LocalisationInterface $LocalisationReader, ContainerInterface $container)
    {
        $this->localisationReader = $LocalisationReader;
        $this->container          = $container;
    }

    /**
     * @param Person $user
     */
    public function setUser(Person $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Person|null
     */
    public function getUser(): ?Person
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    /**
     *
     */
    public function __invoke(): void
    {
        if (empty($this->user) || !empty($this->user->getTimeZone())) {
            return;
        }

        $this->setTimeZone();
    }

    public function getTimeZoneForUser()
    {
        $this->setTimeZone();
    }

    /**
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    private function setTimeZone()
    {
        $ip = $this->container->get('request_stack')->getCurrentRequest()->getClientIp();

        $this->localisationReader->setIp($ip);
        $this->timeZone = !in_array($ip, self::EXCLUDED_IPS)
            ? $this->localisationReader->getRequestLocalisation()->getTimeZone()
            : $this->container->getParameter('time_zone_default');

        if (!$this->timeZone) {
            $this->timeZone = $this->container->getParameter('time_zone_default');
        }

        $this->setTimeZoneForUser();
    }

    /**
     *
     */
    private function setTimeZoneForUser()
    {
        $timeZoneRepository = $this->container->get('doctrine')->getRepository(PersonTimeZone::class);
        $timeZone           = $timeZoneRepository->findOneBy(['timeZone' => $this->timeZone]);

        $em = $this->container->get('doctrine')->getManager();

        if (empty($timeZone)) {
            $timeZone = new PersonTimeZone();
            $timeZone->setTimeZone($this->timeZone);

            $em->persist($timeZone);
        }

        $this->user->setTimeZone($timeZone);

        $em->persist($this->user);
        $em->flush();
    }
}
