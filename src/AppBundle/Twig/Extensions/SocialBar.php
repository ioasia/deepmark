<?php
namespace AppBundle\Twig\Extensions;

use Psr\Container\ContainerInterface;
use Twig\TwigFunction;

class SocialBar extends \Twig_Extension {

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * SocialBar constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getName()
    {
        return 'social_bar';
    }

    public function getFunctions()
    {
        return array(
             new TwigFunction('socialButtons', [$this, 'getSocialButtons'],  array('is_safe' => array('html'))),
             new TwigFunction('facebookButton', [$this, 'getFacebookShareButton'], array('is_safe' => array('html'))),
            new TwigFunction('linkedinButton', [$this, 'getLinkedinShareButton'], array('is_safe' => array('html'))),
        );
    }

    public function getSocialButtons($parameters = array())
    {
        // no parameters were defined, keeps default values
        if (!array_key_exists('facebook', $parameters)){
            $render_parameters['facebook'] = array();
            // parameters are defined, overrides default values
        }else if(is_array($parameters['facebook'])){
            $render_parameters['facebook'] = $parameters['facebook'];
            // the button is not displayed
        }else{
            $render_parameters['facebook'] = false;
        }

        if (!array_key_exists('linkedin', $parameters)){
            $render_parameters['linkedin'] = array();
        }else if(is_array($parameters['linkedin'])){
            $render_parameters['linkedin'] = $parameters['linkedin'];
        }else{
            $render_parameters['linkedin'] = false;
        }

        // get the helper service and display the template
        return $this->container->get('socialBarHelper')->socialButtons($render_parameters);
    }

    // https://developers.facebook.com/docs/reference/plugins/like/
    public function getFacebookShareButton($parameters = array())
    {
        // default values, you can override the values by setting them
        $parameters = $parameters + array(
                'url' => null,
                'locale' => 'en_US',
                'send' => false,
                'width' => 300,
                'showFaces' => false,
                'layout' => 'button',
            );

        return $this->container->get('socialBarHelper')->facebookButton($parameters);
    }

    public function getLinkedinShareButton($parameters = array())
    {
        // default values, you can override the values by setting them
        $parameters = $parameters + array(
                'url' => null,
                'locale' => 'en_US',
            );

        return $this->container->get('socialBarHelper')->linkedinButton($parameters);
    }
}