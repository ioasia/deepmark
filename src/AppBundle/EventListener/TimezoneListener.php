<?php

namespace AppBundle\EventListener;

use AppBundle\DoctrineDBALTypes\UTCDateTimeType;
use AppBundle\Entity\Person;
use AppBundle\Service\TimeZoneResolver;
use AppBundle\Utils\UserTimeZone;
use Doctrine\DBAL\Types\Type;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Twig\Environment;
use Twig\Extension\CoreExtension;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class TimezoneListener
{
    /** @var TokenStorageInterface */
    private $token_storage;

    /** @var \Twig_Environment */
    private $twig;

    /** @var ContainerInterface|Container */
    private $container;
    /**
     * @var TimeZoneResolver
     */
    private $timeZoneResolver;

    public function __construct(TokenStorageInterface $token_storage, Environment $twig, ContainerInterface $container, TimeZoneResolver $timeZoneResolver)
    {
        $this->token_storage = $token_storage;
        $this->twig = $twig;
        $this->container = $container;
        $this->timeZoneResolver = $timeZoneResolver;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest()
    {
        /** @var UsernamePasswordToken $token */
        $token = $this->token_storage->getToken();
        // override doctrine datetime class
        Type::overrideType('datetime', UTCDateTimeType::class);
        Type::overrideType('datetimetz', UTCDateTimeType::class);
        if (!empty($token)) {
            /** @var Person $user */
            $user = $token->getUser();

            if (!$user instanceof Person) {
                return;
            }

            $this->timeZoneResolver->setUser($user);
            $this->timeZoneResolver->__invoke();

            $user = $this->timeZoneResolver->getUser();

            UserTimeZone::setUser($user);
            $parameterTimeZone = $this->container->getParameter('time_zone_default');
            UserTimeZone::setTimeZone($parameterTimeZone);
            date_default_timezone_set($user->getDateTimeZone()->getName());

            $this->twig->getExtension(CoreExtension::class)->setTimezone($user->getDateTimeZone()->getName());
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $cookieValue = $request->cookies->get('hl');
        $hasChangedValue = $request->cookies->get('hcc');
        if ($cookieValue && !$hasChangedValue) {
            // set new cookie
            // please set the secure parameter as false for dev site
            $cookie = new Cookie('hl', $cookieValue, time() + 31536000, null, null,true, true);
            $hasChangedCookie = new Cookie('hcc', $cookieValue, time() + 31536000, null, null,true, true);
            $response->headers->setCookie($cookie);
            $response->headers->setCookie($hasChangedCookie);
        }
    }
}
