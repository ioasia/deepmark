<?php

namespace AppBundle\Entity;

use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 *
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="createdAt",
 *          column=@ORM\Column(
 *              name = "created",
 *              type = "datetime",
 *              options = {"default" = "CURRENT_TIMESTAMP"}
 *          )
 *      ),
 *
 *     @ORM\AttributeOverride(name="updatedAt",
 *          column=@ORM\Column(
 *              name = "updated",
 *              type = "datetime",
 *              options = {"default" = "CURRENT_TIMESTAMP"}
 *          )
 *      )
 * })
 */
class Person extends BaseUser
{
    use TimestampableEntity;
    const DEFAULT_TIMEZONE = 'Europe/Paris';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var PersonTimeZone
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PersonTimeZone")
     * @ORM\JoinColumn(name="time_zone_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $timeZone;

    /**
     * @return PersonTimeZone|null
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param PersonTimeZone|null $timeZone
     *
     * @return Person
     */
    public function setTimeZone(PersonTimeZone $timeZone = null)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    public function getDateTimeZone(): DateTimeZone
    {
        $timezoneString = $this->getTimeZone() ? $this->getTimeZone()->getTimeZone() : self::DEFAULT_TIMEZONE;

        return new DateTimeZone($timezoneString);
    }

    /**
     * @param $role
     * @return boolean
     */
    public function isRole($role)
    {
        return in_array($role, $this->getRoles());
    }
}
