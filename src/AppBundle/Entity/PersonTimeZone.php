<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="person_time_zone")
 *
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="createdAt",
 *          column=@ORM\Column(
 *              name = "created",
 *              type = "datetime",
 *              options = {"default" = "CURRENT_TIMESTAMP"}
 *          )
 *      ),
 *
 *     @ORM\AttributeOverride(name="updatedAt",
 *          column=@ORM\Column(
 *              name = "updated",
 *              type = "datetime",
 *              options = {"default" = "CURRENT_TIMESTAMP"}
 *          )
 *      )
 * })
 */
class PersonTimeZone
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="short_name", nullable=true)
     */
    private $shortName;

    /**
     * @return mixed
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param mixed $shortName
     */
    public function setShortName($shortName): void
    {
        $this->shortName = $shortName;
    }

    /**
     * @ORM\Column(type="string", name="time_zone"))
     */
    private $timeZone;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeZone.
     *
     * @param string $timeZone
     *
     * @return PersonTimeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone.
     *
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    public function __toString()
    {
        return $this->getTimeZone();
    }
}
