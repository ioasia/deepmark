<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Profile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;

/**
 * Class BaseFixture
 * @package AppBundle\DataFixtures
 */
class BaseFixture extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->om = $manager;
    }

    /**
     * @param $type
     * @param bool $reload
     * @return mixed
     */
    protected function getProfile($type, $reload = false)
    {
        static $profiles;

        if (isset($profiles[$type]) && $reload === false) {
            return $profiles[$type];
        }

        $profiles[$type] = $this->om->getRepository(Profile::class)->findOneBy(
            [
                'profileType' => $this->om->getRepository('ApiBundle:ProfileVariety')
                    ->findOneBy(['appId' => $type])
            ]
        );

        return $profiles[$type];
    }
}
