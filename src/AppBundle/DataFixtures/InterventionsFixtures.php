<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\Session;
use ApiBundle\Entity\SessionPlace;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use DateTime;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;

/**
 * Class InterventionsFixtures
 * @package AppBundle\DataFixtures
 */
class InterventionsFixtures extends BaseFixture implements DependentFixtureInterface
{
    const INTERVENTION_REFERENCE = 'intervention';

    const MODULE_NAME_MAPPING = [
        'Conception'                 => 'Bushido',
        'Pilotage'                   => 'Basic Sword usage',
        'Module e-learning'          => 'Sun Tzu',
        'Quiz'                       => 'who is your enemy',
        'Séance en ligne'            => 'katagana and hiragana',
        'Classe virtuelle'           => 'Katas',
        'Atelier collectif en ligne' => 'Sword mastery',
        'Animation présentielle'     => 'Kumite',
        'Evaluation'                 => 'Kumite with your sen sei',
    ];

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            ImportantProfileFixtures::class,
            StoredModuleFixtures::class,
            PriorityFixtures::class,
            LiveResourceOriginFixtures::class,
        ];
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->om = $manager;
        $this->createOneInterventionWithAllModuleTypes($manager);
        $this->createAnInterventionForEachModuleType($manager);
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    private function createAnInterventionForEachModuleType(ObjectManager $manager)
    {
        $storedModules = $manager->getRepository('ApiBundle:StoredModule')->findAll();
        $profileAdmin  = $this->getProfile(ProfileVariety::ADMIN);
        $profileClient = $this->getProfile(ProfileVariety::CUSTOMER);

        /** @var StoredModule $storedModule */
        foreach ($storedModules as $storedModule) {
            $status = $manager->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::INTERVENTION_CREATED]);

            $priority = $manager->getRepository('ApiBundle:Priority')->findOneBy(['appId' => rand(1, 3)]);

            $intervention = new Intervention();
            $intervention->setCustomer($profileClient);
            $intervention->setAdmin($profileAdmin);
            $intervention->setBeginning(new DateTime('2019-02-18'));
            $intervention->setEnding(new DateTime('2024-03-18'));
            $intervention->setAdmin($profileAdmin);
            $intervention->setPriority($priority);
            $intervention->setStatus($status);
            $intervention->setDesignation(
                self::MODULE_NAME_MAPPING[$storedModule->getDesignation()] ?? uniqid($storedModule->getDesignation())
            );
            $intervention->setComment('Comment');

            $module = $this->createModule($manager, $storedModule);
            $intervention->addModule($module);
            $module->setIntervention($intervention);

            $manager->persist($intervention);
            $manager->flush();
            $this->addReference(self::INTERVENTION_REFERENCE . '_' . $storedModule->getAppId(), $intervention);
        }
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    private function createOneInterventionWithAllModuleTypes(ObjectManager $manager)
    {
        $status = $manager->getRepository('ApiBundle:Status')->findOneBy(
            ['appId' => Status::INTERVENTION_CREATED]
        );

        $priority = $manager->getRepository('ApiBundle:Priority')->findOneBy(
            ['appId' => rand(1, 3)]
        );

        $now       = new DateTime();
        $dateStart = clone $now;
        $dateStart = $dateStart->modify('first day of this month');
        $dateEnd   = clone $now;
        $dateEnd   = $dateEnd->modify('first day of next month');

        $profileAdmin  = $this->getProfile(ProfileVariety::ADMIN);
        $profileClient = $this->getProfile(ProfileVariety::CUSTOMER);

        $intervention = new Intervention();

        $intervention->setCustomer($profileClient);
        $intervention->setAdmin($profileAdmin);
        $intervention->setBeginning($dateStart);
        $intervention->setEnding($dateEnd);
        $intervention->setAdmin($profileAdmin);
        $intervention->setPriority($priority);
        $intervention->setStatus($status);
        $intervention->setDesignation('Rebond et Vente à distance');
        $intervention->setComment('Comment');

        $manager->persist($intervention);

        $storedModules = $manager->getRepository('ApiBundle:StoredModule')->findAll();

        foreach ($storedModules as $storedModule) {
            $module = $this->createModule($manager, $storedModule);
            $intervention->addModule($module);
            $module->setIntervention($intervention);
        }

        $manager->flush();

        $this->assignTrainers($manager, $intervention);
        $this->assignLearner($manager, $intervention);
        $this->addReference(self::INTERVENTION_REFERENCE, $intervention);
    }

    /**
     * @param ObjectManager $manager
     * @param StoredModule $storedModule
     * @return Module
     * @throws Exception
     */
    private function createModule(ObjectManager $manager, StoredModule $storedModule)
    {
        $now       = new DateTime();
        $dateStart = clone $now;
        $dateStart = $dateStart->modify('first day of this month');
        $dateEnd   = clone $now;
        $dateEnd   = $dateEnd->modify('first day of next month');
        $duration  = clone $now;
        $duration->setTime(rand(0, 2), rand(0, 59));

        $module = new Module();
        $module->setBeginning($dateStart);
        $module->setEnding($dateEnd);
        $module->setDuration($duration);
        $module->setDesignation(
            self::MODULE_NAME_MAPPING[$storedModule->getDesignation()] ?? uniqid($storedModule->getDesignation())
        );
        $module->setDescription('Path of samurai');
        $module->setPrice(rand(10, 500));
        $module->setStoredModule($storedModule);
        $module->setQuantity(rand(1, 5));

        // For Quiz
        $module->setIteration(1);
        $module->setValidatingScore(30);

        // Not used
        $module->setConceptionDate($now);
        $module->setRequireReport(false);
        $module->setSendParticipant(false);
        $module->setSendManager(false);
        $module->setSendManagerOther(false);
        $module->setCumulativeSkill(false);
        $module->setFreeRegistration(true);

        $this->hydrateDependingOnModuleType($manager, $module);
        $manager->persist($module);

        return $module;
    }

    /**
     * @param ObjectManager $manager
     * @param Module $module
     * @throws Exception
     */
    private function hydrateDependingOnModuleType(ObjectManager $manager, Module $module)
    {
        switch ($module->getStoredModule()->getAppId()) {
            case StoredModule::EVALUATION:
            case StoredModule::QUIZ:
            case StoredModule::ONLINE:
                break;
            case StoredModule::ELEARNING:
                $this->hydrateELearning($module);
                break;
            case StoredModule::VIRTUAL_CLASS:
            case StoredModule::ONLINE_WORKSHOP:
            case StoredModule::PRESENTATION_ANIMATION:
                $this->createSessions($manager, $module);
                break;
        }
    }

    /**
     * @param Module $module
     */
    private function hydrateELearning(Module $module)
    {
        $eduDoc = new EducationalDocument();
        $eduDoc->setName('Education document');
        $eduDoc->setModule($module);
    }

    /**
     * @param ObjectManager $manager
     * @param Module $module
     * @throws Exception
     */
    private function createSessions(ObjectManager $manager, Module $module)
    {
        $isPresentation = StoredModule::PRESENTATION_ANIMATION === $module->getStoredModule()->getAppId();

        for ($nbQty = 0; $nbQty < $module->getQuantity(); ++$nbQty) {
            $dateSession   = $this->getRandomDateRange($module->getBeginning(), $module->getEnding());
            $moduleSession = new ModuleSession();
            $session       = new Session();
            $session->setParticipant(rand(1, 10));
            $session->setSessionDate($dateSession);
            $moduleSession->setSession($session);

            if ($isPresentation) {
                $sessionPlace = new SessionPlace();
                $sessionPlace->setStreet('Street 1');
                $sessionPlace->setCity('Puteaux');
                $sessionPlace->setZipCode('92000');
                $sessionPlace->setCountry('France');
                $manager->persist($sessionPlace);
                $moduleSession->setSessionPlace();
            }

            $manager->persist($session);
            $manager->persist($moduleSession);
            $module->addSession($moduleSession);
        }

        $manager->flush();
    }

    /**
     * @param $date1
     * @param $date2
     * @return DateTime
     * @throws Exception
     */
    public function getRandomDateRange($date1, $date2)
    {
        if (!is_a($date1, 'DateTime')) {
            $date1 = new DateTime((ctype_digit((string)$date1) ? '@' : '') . $date1);
            $date2 = new DateTime((ctype_digit((string)$date2) ? '@' : '') . $date2);
        }
        $random_u    = random_int($date1->format('U'), $date2->format('U'));
        $random_date = new DateTime();
        $random_date->setTimestamp($random_u);
        $random_date->setTime(rand(7, 19), rand(0, 59));

        return $random_date;
    }

    /**
     * @param ObjectManager $manager
     * @param Profile $profile
     * @param DateTime $date
     * @return AvailableCalendar
     */
    private function createAvailability(ObjectManager $manager, Profile $profile, DateTime $date)
    {
        $startDate = clone $date;
        $startDate->setTime(rand(7, 11), rand(0, 59));
        $endDate = clone $startDate;
        $endDate->setTime(rand(13, 19), rand(0, 59));
        $entity = new AvailableCalendar();
        $entity->setProfile($profile);
        $entity->setBeginning($startDate);
        $entity->setEnding($endDate);

        $manager->persist($entity);

        return $entity;
    }

    /**
     * @param ObjectManager $manager
     * @param Module $module
     * @throws Exception
     */
    private function assignTrainerOnline(ObjectManager $manager, Module $module)
    {
        $trainer = $this->getProfile(ProfileVariety::TRAINER);

        for ($nbQty = 0; $nbQty < $module->getQuantity(); ++$nbQty) {
            $date = $this->getRandomDateRange($module->getBeginning(), $module->getEnding());
            $this->createAvailability($manager, $trainer, $date);
        }

        $statusScheduled = $manager->getRepository('ApiBundle:Status')
            ->findOneBy(['appId' => Status::WITH_BOOKING_SCHEDULED]);

        $assigment = new AssigmentResourceSystem();
        $assigment->setModule($module);
        $assigment->setMinBookingDate($module->getBeginning());
        $assigment->setMaxBookingDate($module->getEnding());
        $assigment->setLiveResource($trainer);
        $assigment->setStatus($statusScheduled);

        $manager->persist($assigment);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param Module $module
     */
    private function assignTrainerSessions(ObjectManager $manager, Module $module)
    {
        $sessions        = $module->getSessions();
        $statusScheduled = $manager->getRepository('ApiBundle:Status')->findOneBy(
            ['appId' => Status::WITH_BOOKING_SCHEDULED]
        );

        $trainer = $this->getProfile(ProfileVariety::TRAINER);

        foreach ($sessions as $session) {
            $date = $session->getSession()->getSessionDate();

            $this->createAvailability($manager, $trainer, $date);

            $assigment = new AssigmentResourceSystem();
            $assigment->setModule($module);
            $assigment->setMinBookingDate($module->getBeginning());
            $assigment->setMaxBookingDate($module->getEnding());
            $assigment->setLiveResource($trainer);
            $assigment->setModuleSession($session);
            $assigment->setStatus($statusScheduled);

            $manager->persist($assigment);
        }
    }

    /**
     * @param ObjectManager $manager
     * @param Module $module
     * @throws Exception
     */
    private function assignTrainersModule(ObjectManager $manager, Module $module)
    {

        switch ($module->getStoredModule()->getAppId()) {
            case StoredModule::ONLINE:
                $this->assignTrainerOnline($manager, $module);
                break;
            case StoredModule::VIRTUAL_CLASS:
            case StoredModule::ONLINE_WORKSHOP:
            case StoredModule::PRESENTATION_ANIMATION:
                $this->assignTrainerSessions($manager, $module);
                break;
        }
    }

    /**
     * @param ObjectManager $manager
     * @param Intervention $intervention
     * @throws Exception
     */
    private function assignTrainers(ObjectManager $manager, Intervention $intervention)
    {
        $modules = $intervention->getModules();

        foreach ($modules as $module) {
            $this->assignTrainersModule($manager, $module);
        }
    }

    /**
     * @param ObjectManager $manager
     * @param Intervention $intervention
     */
    private function assignLearner(ObjectManager $manager, Intervention $intervention)
    {
        $status = $manager->getRepository('ApiBundle:Status')
            ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_CREATED]);

        $learnerIntervention = new LearnerIntervention();
        $learnerIntervention->setIntervention($intervention);
        $learnerIntervention->setLearner($this->getProfile(ProfileVariety::LEARNER));
        $learnerIntervention->setProgression(0);
        $learnerIntervention->setStatus($status);
        $manager->persist($learnerIntervention);
    }
}
