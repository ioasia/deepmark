<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\EducationalDocVariety;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class EducationalDocVarietyFixtures
 * @package AppBundle\DataFixtures
 */
class EducationalDocVarietyFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $docVariety = array(
            EducationalDocVariety::DOCUMENT => 'Document',
            EducationalDocVariety::AUDIO    => 'Audio',
            EducationalDocVariety::VIDEO    => 'Video',
        );

        foreach ($docVariety as $key => $variety) {
            $newDocVariety = new EducationalDocVariety();
            $newDocVariety->setAppId($key);
            $newDocVariety->setDesignation($variety);
            $manager->persist($newDocVariety);
        }

        $manager->flush();
    }
}
