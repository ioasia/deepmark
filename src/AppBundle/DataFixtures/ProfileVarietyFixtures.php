<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\ProfileVariety;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ProfileVarietyFixtures
 * @package AppBundle\DataFixtures
 */
class ProfileVarietyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $profileVariety = array(
            ProfileVariety::SUPER_ADMIN => 'Super Admin',
            ProfileVariety::ADMIN       => 'Admin',
            ProfileVariety::TRAINER     => 'Trainer',
            ProfileVariety::LEARNER     => 'Learner',
            ProfileVariety::CUSTOMER    => 'Customer',
        );

        foreach ($profileVariety as $key => $type) {
            $profileVariety = new ProfileVariety();
            $profileVariety->setDesignation($type);
            $profileVariety->setAppId($key);
            $manager->persist($profileVariety);
        }

        $manager->flush();
    }
}
