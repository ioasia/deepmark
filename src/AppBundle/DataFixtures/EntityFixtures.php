<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Entity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class EntityFixtures
 * @package AppBundle\DataFixtures
 */
class EntityFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $organisation = $manager->getRepository('ApiBundle:Organisation')
            ->findOneBy(['designation' => 'DeepMark']);

        $entity = new Entity();
        $entity->setDesignation('Direction');
        $entity->setStreet('Street 1');
        $entity->setOrganisation($organisation);

        $manager->persist($entity);
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [OrganisationFixtures::class];
    }
}
