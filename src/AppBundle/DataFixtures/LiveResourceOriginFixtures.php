<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\LiveResourceOrigin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LiveResourceOriginFixtures
 * @package AppBundle\DataFixtures
 */
class LiveResourceOriginFixtures extends Fixture
{
    const LIVE_RESOURCE_ORIGIN = ['internal', 'external'];

    const LIVE_RESOURCE_ORIGIN_PREFIX = 'origin_live_resource_';

    public function load(ObjectManager $manager)
    {
        foreach (self::LIVE_RESOURCE_ORIGIN as $type) {
            $typeLiveResource = new LiveResourceOrigin();
            $typeLiveResource->setDesignation($type);
            $manager->persist($typeLiveResource);
            $this->setReference(
                self::LIVE_RESOURCE_ORIGIN_PREFIX . $type,
                $typeLiveResource
            );
        }

        $manager->flush();
    }
}
