<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\PersonTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class PersonTimeZoneFixtures
 * @package AppBundle\DataFixtures
 */
class PersonTimeZoneFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $timeZones = timezone_identifiers_list();

        foreach ($timeZones as $timeZone) {
            $newTimeZone = new PersonTimeZone();
            $newTimeZone->setTimeZone($timeZone);
            $manager->persist($newTimeZone);
        }

        $manager->flush();
    }
}
