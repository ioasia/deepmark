<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Organisation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class OrganisationFixtures
 * @package AppBundle\DataFixtures
 */
class OrganisationFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $organisation = new Organisation();
        $organisation->setExpansionStreet('');
        $organisation->setStreet('12 place de la Défense');
        $organisation->setDesignation('DeepMark');
        $organisation->setCity('Puteaux');
        $organisation->setZipCode('92000');
        $organisation->setCountry('France');

        $manager->persist($organisation);
        $manager->flush();
    }
}
