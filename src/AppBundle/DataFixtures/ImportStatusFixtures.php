<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use ImportBundle\Entity\ImportStatus;

/**
 * Class ImportStatusFixtures
 * @package AppBundle\DataFixtures
 */
class ImportStatusFixtures extends Fixture
{
    const STATUSES = ['imported', 'invited', 'registered'];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::STATUSES as $status) {
            $importStatus = new ImportStatus();
            $importStatus->setStatusName($status);
            $manager->persist($importStatus);
        }

        $manager->flush();
    }
}
