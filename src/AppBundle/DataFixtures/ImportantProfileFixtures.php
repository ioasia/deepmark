<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\CompanyPost;
use ApiBundle\Entity\LiveResourceVariety;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use AppBundle\Entity\Person;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Faker\Factory;

/**
 * Class ImportantProfileFixtures
 * @package AppBundle\DataFixtures
 */
class ImportantProfileFixtures extends Fixture implements DependentFixtureInterface
{
    const DEFAULT_SCORE = 3;

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var string
     */
    private $defaultPassword = Profile::DEFAULT_PASSWORD;

    /**
     * @var string
     */
    private $defaultDomain = '@deepmark.fr';

    private $users = [
        // Super Admin
        [
            'person'  => [
                'email'              => null,
                'username'           => 'sadmin',
                'username_canonical' => 'sadmin',
                'enable'             => true,
                'password'           => null,
                'roles'              => 'super_admin'
            ],
            'profile' => [
                'other_email'  => '',
                'firstName'    => 'Super',
                'lastName'     => 'Admin',
                'billing_code' => '',
                'street'       => '',
                'companyPost'  => null,
                'profile_type' => null,
                'person'       => null,
                'entity'       => null
            ]
        ],
        // Admin
        [
            'person'  => [
                'email'              => null,
                'username'           => 'admin',
                'username_canonical' => 'admin',
                'enable'             => true,
                'password'           => null,
                'roles'              => 'admin'
            ],
            'profile' => [
                'other_email'  => '',
                'firstName'    => 'Admin',
                'lastName'     => 'Admin',
                'billing_code' => '',
                'street'       => '',
                'companyPost'  => null,
                'profile_type' => null,
                'person'       => null,
                'entity'       => null
            ]
        ],
        // Trainer
        [
            'person'  => [
                'email'              => null,
                'username'           => 'trainer',
                'username_canonical' => 'trainer',
                'enable'             => true,
                'password'           => null,
                'roles'              => 'trainer'
            ],
            'profile' => [
                'other_email'  => '',
                'firstName'    => 'Trainer',
                'lastName'     => 'Trainer',
                'billing_code' => '',
                'street'       => '',
                'companyPost'  => null,
                'profile_type' => null,
                'person'       => null,
                'entity'       => null
            ]
        ],
        // Learner
        [
            'person'  => [
                'email'              => null,
                'username'           => 'learner',
                'username_canonical' => 'learner',
                'enable'             => true,
                'password'           => null,
                'roles'              => 'learner'
            ],
            'profile' => [
                'other_email'  => '',
                'firstName'    => 'Learner',
                'lastName'     => 'Learner',
                'billing_code' => '',
                'street'       => '',
                'companyPost'  => null,
                'profile_type' => null,
                'person'       => null,
                'entity'       => null
            ]
        ],
        // Client
        [
            'person'  => [
                'email'              => null,
                'username'           => 'client',
                'username_canonical' => 'client',
                'enable'             => true,
                'password'           => null,
                'roles'              => 'client'
            ],
            'profile' => [
                'other_email'  => '',
                'firstName'    => 'Client',
                'lastName'     => 'Client',
                'billing_code' => '',
                'street'       => '',
                'companyPost'  => null,
                'profile_type' => null,
                'person'       => null,
                'entity'       => null
            ]
        ]
    ];

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->om              = $manager;
        $profileTypeSuperAdmin = $this->getProfileVarity(ProfileVariety::SUPER_ADMIN);
        $profileTypeAdmin      = $this->getProfileVarity(ProfileVariety::ADMIN);
        $profileTypeTrainer    = $this->getProfileVarity(ProfileVariety::TRAINER);
        $profileTypeLearner    = $this->getProfileVarity(ProfileVariety::LEARNER);
        $profileTypeCustomer   = $this->getProfileVarity(ProfileVariety::CUSTOMER);

        $liveTrainerType = $this->om->getRepository('ApiBundle:LiveResourceVariety')->findOneBy(
            ['appId' => LiveResourceVariety::LIVE_TRAINER]
        );

        $entity = $this->om->getRepository('ApiBundle:Entity')->findOneBy(['designation' => 'Direction']);
        $skills = $this->om->getRepository('ApiBundle:Skill')->findAll();

        $companyPost = new CompanyPost();
        $companyPost->setDesignation('default');

        $this->om->persist($companyPost);

        $faker = Factory::create();
        /**
         * Generate random Admin
         */

        $this->users[] =
            [
                'person'  => [
                    'email'              => $faker->email,
                    'username'           => $faker->userName,
                    'username_canonical' => null,
                    'enable'             => true,
                    'password'           => null,
                    'roles'              => 'admin'
                ],
                'profile' => [
                    'other_email'  => '',
                    'firstName'    => $faker->firstName,
                    'lastName'     => $faker->lastName,
                    'billing_code' => '',
                    'street'       => $faker->streetAddress,
                    'companyPost'  => null,
                    'profile_type' => null,
                    'person'       => null,
                    'entity'       => null
                ]
            ];


        foreach ($this->users as $user) {
            $personData  = $user['person'];
            $profileData = $user['profile'];

            $person = new Person();
            $email  = $personData['email'] ? $personData['email'] : $personData['username'] . $this->defaultDomain;
            $person->setEmail($email);
            $person->setUsername($personData['username']);
            $person->setUsernameCanonical($personData['username']);
            $person->setEnabled($personData['enable']);
            $person->setPlainPassword(
                $personData['password'] ? $personData['password'] : $this->defaultPassword
            );
            switch ($personData['roles']) {
                case 'super_admin':
                    $person->setRoles(['ROLE_SUPER_ADMIN']);
                    break;
                case 'admin':
                    $person->setRoles(['ROLE_ADMIN']);
                    break;
                case 'trainer':
                    $person->setRoles(['ROLE_TRAINER']);
                    break;
                case 'learner':
                    $person->setRoles(['ROLE_LEARNER']);
                    break;
                case 'client':
                    $person->setRoles(['ROLE_CLIENT']);
                    break;
            }

            $this->om->persist($person);

            $profile = new Profile();
            $profile->setOtherEmail($profileData['other_email'] ? $profileData['other_email'] : $email);
            $profile->setCreationDate(new DateTime());
            $profile->setUpdateDate(new DateTime());
            $profile->setFirstName($profileData['firstName']);
            $profile->setLastName($profileData['lastName']);
            $profile->setBillingCode($profileData['billing_code']);
            $profile->setStreet($profileData['street']);
            $profile->setCompanyPost($companyPost);
            switch ($personData['roles']) {
                case 'super_admin':
                    $profile->setProfileType($profileTypeSuperAdmin);
                    break;
                case 'admin':
                    $profile->setProfileType($profileTypeAdmin);
                    break;
                case 'trainer':
                    $profile->setProfileType($profileTypeTrainer);
                    $profile->addSkill($this->addSkillProfile($skills[0], $profile, $liveTrainerType));
                    break;
                case 'learner':
                    $profile->setProfileType($profileTypeLearner);
                    break;
                case 'client':
                    $profile->setProfileType($profileTypeCustomer);
                    break;
            }
            $profile->setPerson($person);
            $profile->setEntity($entity);

            $this->om->persist($profile);
        }

        $this->om->flush();
    }

    /**
     * @param $appId
     * @return ProfileVariety|object|null
     */
    private function getProfileVarity($appId)
    {
        return $this->om->getRepository('ApiBundle:ProfileVariety')->findOneBy(['appId' => (int)$appId]);
    }

    /**
     * @param $skill
     * @param $profile
     * @param $type
     * @return SkillResourceVarietyProfile
     * @throws Exception
     */
    private function addSkillProfile($skill, $profile, $type)
    {
        $skillResource = new SkillResourceVarietyProfile();
        $skillResource->setSkill($skill);
        $skillResource->setProfile($profile);
        $skillResource->setLiveResourceVariety($type);
        $skillResource->setScore(self::DEFAULT_SCORE);
        $skillResource->setDateAdd(new DateTime());

        return $skillResource;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ProfileVarietyFixtures::class,
            PersonTimeZoneFixtures::class,
            EntityFixtures::class,
            SkillFixtures::class,
        );
    }
}
