<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\StatusFamily;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class StatusFamilyFixtures
 * @package AppBundle\DataFixtures
 */
class StatusFamilyFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $statusFamily = array(
            StatusFamily::GENERAL              => 'General',
            StatusFamily::INTERVENTION         => 'Intervention',
            StatusFamily::LEARNER_INTERVENTION => 'Learner_Intervention',
            StatusFamily::PROFILE              => 'Profile',
            StatusFamily::WITHOUT_BOOKING      => 'Without reservation',
            StatusFamily::WITH_BOOKING         => 'With reservation',
            StatusFamily::REPORT               => 'Report',
        );

        foreach ($statusFamily as $key => $status) {
            $newStatusFamily = new StatusFamily();
            $newStatusFamily->setAppId($key);
            $newStatusFamily->setDesignation($status);
            $manager->persist($newStatusFamily);
        }
        $manager->flush();
    }
}
