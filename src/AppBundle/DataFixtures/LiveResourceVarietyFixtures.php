<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\LiveResourceVariety;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LiveResourceVarietyFixtures
 * @package AppBundle\DataFixtures
 */
class LiveResourceVarietyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $liveResourceVariety = array(
            LiveResourceVariety::MENTOR       => 'Mentor',
            LiveResourceVariety::LIVE_TRAINER => 'Live Trainer',
        );

        foreach ($liveResourceVariety as $key => $type) {
            $typeLiveResource = new LiveResourceVariety();
            $typeLiveResource->setAppId($key);
            $typeLiveResource->setDesignation($type);
            $manager->persist($typeLiveResource);
        }
        $manager->flush();
    }
}
