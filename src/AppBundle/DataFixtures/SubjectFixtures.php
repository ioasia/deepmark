<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class SubjectFixtures
 * @package AppBundle\DataFixtures
 */
class SubjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $subjects = array(
            Subject::SUBJECT_ONE   => 'sell ancillary products',
            Subject::SUBJECT_TWO   => 'Announce a difficult decision',
            Subject::SUBJECT_THREE => 'Manage a team in times of change',
            Subject::SUBJECT_FOUR  => 'Improve your leadership',
        );

        foreach ($subjects as $key => $subject) {
            $newSubject = new Subject();
            $newSubject->setDesignation($subject);
            $newSubject->setAppId($key);
            $manager->persist($newSubject);
        }

        $manager->flush();
    }
}
