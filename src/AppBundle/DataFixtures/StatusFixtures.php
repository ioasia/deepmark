<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01/09/18
 * Time: 11:14.
 */

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Status;
use ApiBundle\Entity\StatusFamily;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StatusFixtures extends Fixture
{
    /**
     * @param $manager
     * @param $appId
     *
     * @return mixed
     */
    private function getStatusFamilyByAppId(ObjectManager $manager, $appId)
    {
        return $manager->getRepository('ApiBundle:StatusFamily')->findOneBy(array(
            'appId' => $appId,
        ));
    }

    public function load(ObjectManager $manager)
    {
        $statusGeneral = array(
            Status::GENERAL_UNKNOWN => 'Unknown',
        );

        foreach ($statusGeneral as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::GENERAL));
            $manager->persist($newStatus);
        }

        $statusIntervention = array(
            Status::INTERVENTION_CREATED     => 'Created',
            Status::INTERVENTION_IN_PROGRESS => 'In Progress',
            Status::INTERVENTION_FINISHED    => 'Finished',
        );

        foreach ($statusIntervention as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::INTERVENTION));
            $manager->persist($newStatus);
        }

        $statusLearnerIntervention = array(
            Status::LEARNER_INTERVENTION_CREATED     => 'Created',
            Status::LEARNER_INTERVENTION_IN_PROGRESS => 'In Progress',
            Status::LEARNER_INTERVENTION_FINISHED    => 'Finished',
            Status::LEARNER_INTERVENTION_EXPIRED     => 'Expired',
        );

        foreach ($statusLearnerIntervention as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::LEARNER_INTERVENTION));
            $manager->persist($newStatus);
        }

        $statusWithBooking = array(
            Status::WITH_BOOKING_SCHEDULED         => 'Scheduled',
            Status::WITH_BOOKING_BOOKED            => 'Booked',
            Status::WITH_BOOKING_IN_PROGRESS       => 'in Progress',
            Status::WITH_BOOKING_DONE              => 'Done',
            Status::WITH_BOOKING_REPORTED          => 'Reported',
            Status::WITH_BOOKING_CANCELED          => 'Canceled',
            Status::WITH_BOOKING_LEARNER_ABSENT    => 'Learner absent',
            Status::WITH_BOOKING_TRAINER_ABSENT    => 'Trainer absent',
            Status::WITH_BOOKING_TECHNICAL_PROBLEM => 'Technical problem',
        );

        foreach ($statusWithBooking as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::WITH_BOOKING));
            $manager->persist($newStatus);
        }

        $statusWithoutBooking = array(
            Status::WITHOUT_BOOKING_UNDONE    => 'Not done yet',
            Status::WITHOUT_BOOKING_CORRECTED => 'Corrected',
            Status::WITHOUT_BOOKING_VALIDATED => 'Validated',
            Status::WITHOUT_BOOKING_DONE      => 'Done',
        );

        foreach ($statusWithoutBooking as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::WITHOUT_BOOKING));
            $manager->persist($newStatus);
        }

        $statusProfile = array(
            Status::PROFILE_WAITING_APPROVAL => 'Not done yet',
            Status::PROFILE_VALIDATED        => 'Corrected',
            Status::PROFILE_INVALID          => 'Validated',
            Status::PROFILE_UNAUTHORIZED     => 'Done',
        );

        foreach ($statusProfile as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::PROFILE));
            $manager->persist($newStatus);
        }

        $statusReport = array(
            Status::REPORT_UNDONE    => 'Not done yet',
            Status::REPORT_DONE      => 'Done',
            Status::REPORT_VALIDATED => 'Validated',
        );

        foreach ($statusReport as $key => $status) {
            $newStatus = new Status();
            $newStatus->setDesignation($status);
            $newStatus->setAppId($key);
            $newStatus->setStatusFamily(
                $this->getStatusFamilyByAppId($manager, statusFamily::REPORT));
            $manager->persist($newStatus);
        }

        $manager->flush();
    }
}
