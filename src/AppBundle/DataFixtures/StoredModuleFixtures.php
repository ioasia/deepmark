<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\StoredModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class StoredModuleFixtures
 * @package AppBundle\DataFixtures
 */
class StoredModuleFixtures extends Fixture
{
    public const CONCEPTION_STORED_MODULE_REFERENCE = 'conception';
    public const PILOTAGE_STORED_MODULE_REFERENCE = 'pilotage';
    public const ELEARNING_STORED_MODULE_REFERENCE = 'elearning';
    public const QUIZZ_STORED_MODULE_REFERENCE = 'quizz';
    public const EXERCICE_STORED_MODULE_REFERENCE = 'exercice';
    public const ONLINE_STORED_MODULE_REFERENCE = 'online';
    public const VIRTUEL_STORED_MODULE_REFERENCE = 'virtuel';
    public const COLLECTIF_STORED_MODULE_REFERENCE = 'collectif';
    public const PRESENTIELLE_STORED_MODULE_REFERENCE = 'presentielle';
    public const EVALUATION_STORED_MODULE_REFERENCE = 'evaluation';

    public function load(ObjectManager $manager)
    {
        $conception = new StoredModule();
        $conception->setAppId(StoredModule::CONCEPTION);
        $conception->setDesignation('Conception');
        $conception->setVisibilityTraining(false);

        $pilotage = new StoredModule();
        $pilotage->setAppId(StoredModule::PILOTAGE);
        $pilotage->setDesignation('Pilotage');
        $pilotage->setVisibilityTraining(false);

        $teamPreparation = new StoredModule();
        $teamPreparation->setAppId(StoredModule::TEAM_PREPARATION);
        $teamPreparation->setDesignation('Team Preparation');
        $teamPreparation->setVisibilityTraining(false);

        $elearning = new StoredModule();
        $elearning->setAppId(StoredModule::ELEARNING);
        $elearning->setDesignation('E-Learning');
        $elearning->setVisibilityTraining(false);

        $quizz = new StoredModule();
        $quizz->setAppId(StoredModule::QUIZ);
        $quizz->setDesignation('Quiz');
        $quizz->setVisibilityTraining(false);

        $online = new StoredModule();
        $online->setAppId(StoredModule::ONLINE);
        $online->setDesignation('Individual Remote Session');
        $online->setVisibilityTraining(false);

        $virtuel = new StoredModule();
        $virtuel->setAppId(StoredModule::VIRTUAL_CLASS);
        $virtuel->setDesignation('Virtual Classroom');
        $virtuel->setVisibilityTraining(false);

        $workshop = new StoredModule();
        $workshop->setAppId(StoredModule::ONLINE_WORKSHOP);
        $workshop->setDesignation('Online Workshop');
        $workshop->setVisibilityTraining(false);

        $presentielle = new StoredModule();
        $presentielle->setAppId(StoredModule::PRESENTATION_ANIMATION);
        $presentielle->setDesignation('Classroom Training');
        $presentielle->setVisibilityTraining(false);

        $evaluation = new StoredModule();
        $evaluation->setAppId(StoredModule::EVALUATION);
        $evaluation->setDesignation('Evaluation');
        $evaluation->setVisibilityTraining(false);

        $manager->persist($conception);
        $manager->persist($pilotage);
        $manager->persist($teamPreparation);
        $manager->persist($elearning);
        $manager->persist($quizz);
        $manager->persist($online);
        $manager->persist($virtuel);
        $manager->persist($workshop);
        $manager->persist($presentielle);
        $manager->persist($evaluation);

        $this->addReference(self::CONCEPTION_STORED_MODULE_REFERENCE, $conception);
        $this->addReference(self::PILOTAGE_STORED_MODULE_REFERENCE, $pilotage);
        $this->addReference(self::ELEARNING_STORED_MODULE_REFERENCE, $elearning);
        $this->addReference(self::QUIZZ_STORED_MODULE_REFERENCE, $quizz);
        $this->addReference(self::ONLINE_STORED_MODULE_REFERENCE, $online);
        $this->addReference(self::VIRTUEL_STORED_MODULE_REFERENCE, $virtuel);
        $this->addReference(self::COLLECTIF_STORED_MODULE_REFERENCE, $workshop);
        $this->addReference(self::PRESENTIELLE_STORED_MODULE_REFERENCE, $presentielle);
        $this->addReference(self::EVALUATION_STORED_MODULE_REFERENCE, $evaluation);

        $manager->flush();
    }
}
