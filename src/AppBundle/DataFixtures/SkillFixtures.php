<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class SkillFixtures
 * @package AppBundle\DataFixtures
 */
class SkillFixtures extends Fixture
{
    const SKILLS = ['management', 'consulting', 'recrutement', 'vente'];
    const REFERENCE_SKILL_PREFIX = 'skill_';

    public function load(ObjectManager $manager)
    {
        foreach (self::SKILLS as $skill) {
            $newSkill = new Skill();
            $newSkill->setDesignation($skill);
            $manager->persist($newSkill);
            $this->setReference(self::REFERENCE_SKILL_PREFIX.$skill, $newSkill);
        }

        $manager->flush();
    }
}
