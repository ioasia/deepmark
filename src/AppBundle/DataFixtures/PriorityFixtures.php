<?php

namespace AppBundle\DataFixtures;

use ApiBundle\Entity\Priority;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class PriorityFixtures
 * @package AppBundle\DataFixtures
 */
class PriorityFixtures extends Fixture
{
    private $priority = [
        'LOW',
        'NORMAL',
        'HIGH',
        'CRITICAL'
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->priority as $index => $priorityName) {
            $priority = new Priority;
            $priority->setAppId($index);
            $priority->setDesignation($priorityName);
            $manager->persist($priority);
        }
        $manager->flush();
    }
}
