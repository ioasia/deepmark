#!/bin/sh

echo "Update latest code"
git checkout -f develop
git pull

echo "Update latest vendor"
composer update

echo "Migrate database"
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:schema:update --force

echo "Reset database"
php bin/console doctrine:fixtures:load --no-interaction

echo "Clear cache"
php bin/console cache:clear --env=dev
php bin/console cache:clear --env=prod